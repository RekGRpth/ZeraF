.class public Lcom/google/android/velvet/Query;
.super Ljava/lang/Object;
.source "Query.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/Query$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/velvet/Query;",
            ">;"
        }
    .end annotation
.end field

.field public static final EMPTY:Lcom/google/android/velvet/Query;

.field private static final SEARCHABLE_EMPTY_STRING:Ljava/lang/String;

.field private static final sBuilder:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/google/android/velvet/Query$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private static sLatestCommitId:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final mAlternateSpans:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;"
        }
    .end annotation
.end field

.field private final mCannedAudio:Ljava/lang/String;

.field private final mCommitId:J

.field private final mCorpus:Lcom/google/android/velvet/Corpus;

.field private final mExtras:Landroid/os/Bundle;

.field private final mFlags:I

.field private final mGogglesDisclosedCapability:I

.field private final mGogglesQueryBitmap:Landroid/graphics/Bitmap;

.field private final mLocationOverride:Landroid/location/Location;

.field private final mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

.field private final mPersistCgiParameters:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mQuery:Ljava/lang/String;

.field public mRequestId:Ljava/lang/String;

.field private final mResultIndex:I

.field private final mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

.field private final mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    sput-object v0, Lcom/google/android/velvet/Query;->SEARCHABLE_EMPTY_STRING:Ljava/lang/String;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lcom/google/android/velvet/Query;->sBuilder:Ljava/lang/ThreadLocal;

    new-instance v0, Lcom/google/android/velvet/Query;

    const/4 v1, 0x0

    const-string v2, ""

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/velvet/PendingCorpus;->WEB:Lcom/google/android/velvet/PendingCorpus;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v0 .. v17}, Lcom/google/android/velvet/Query;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/Corpus;ILjava/util/Map;JLjava/util/List;Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;Landroid/location/Location;Ljava/lang/String;Landroid/graphics/Bitmap;ILcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)V

    sput-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    new-instance v0, Lcom/google/android/velvet/Query$1;

    invoke-direct {v0}, Lcom/google/android/velvet/Query$1;-><init>()V

    sput-object v0, Lcom/google/android/velvet/Query;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    sput-object v0, Lcom/google/android/velvet/Query;->sLatestCommitId:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method constructor <init>()V
    .locals 18

    const/4 v1, 0x0

    const-string v2, ""

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/velvet/PendingCorpus;->WEB:Lcom/google/android/velvet/PendingCorpus;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v17}, Lcom/google/android/velvet/Query;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/Corpus;ILjava/util/Map;JLjava/util/List;Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;Landroid/location/Location;Ljava/lang/String;Landroid/graphics/Bitmap;ILcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)V

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/Corpus;ILjava/util/Map;JLjava/util/List;Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;Landroid/location/Location;Ljava/lang/String;Landroid/graphics/Bitmap;ILcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/velvet/Corpus;
    .param p5    # I
    .param p7    # J
    .param p10    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p11    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;
    .param p12    # Landroid/location/Location;
    .param p13    # Ljava/lang/String;
    .param p14    # Landroid/graphics/Bitmap;
    .param p15    # I
    .param p16    # Lcom/google/android/velvet/presenter/UiMode;
    .param p17    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/Corpus;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            "Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;",
            "Landroid/location/Location;",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            "I",
            "Lcom/google/android/velvet/presenter/UiMode;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/velvet/Query;->mFlags:I

    invoke-static {p2}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/velvet/Query;->mRequestId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    iput p5, p0, Lcom/google/android/velvet/Query;->mResultIndex:I

    if-nez p6, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->of()Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/Query;->mPersistCgiParameters:Lcom/google/common/collect/ImmutableMap;

    :goto_0
    iput-wide p7, p0, Lcom/google/android/velvet/Query;->mCommitId:J

    iput-object p9, p0, Lcom/google/android/velvet/Query;->mAlternateSpans:Ljava/util/List;

    iput-object p10, p0, Lcom/google/android/velvet/Query;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    iput-object p11, p0, Lcom/google/android/velvet/Query;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    iput-object p12, p0, Lcom/google/android/velvet/Query;->mLocationOverride:Landroid/location/Location;

    iput-object p13, p0, Lcom/google/android/velvet/Query;->mCannedAudio:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/velvet/Query;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/velvet/Query;->mGogglesDisclosedCapability:I

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/velvet/Query;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/velvet/Query;->mExtras:Landroid/os/Bundle;

    return-void

    :cond_0
    instance-of v1, p6, Lcom/google/common/collect/ImmutableMap;

    if-eqz v1, :cond_1

    check-cast p6, Lcom/google/common/collect/ImmutableMap;

    iput-object p6, p0, Lcom/google/android/velvet/Query;->mPersistCgiParameters:Lcom/google/common/collect/ImmutableMap;

    goto :goto_0

    :cond_1
    invoke-static {p6}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/Query;->mPersistCgiParameters:Lcom/google/common/collect/ImmutableMap;

    goto :goto_0
.end method

.method synthetic constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/Corpus;ILjava/util/Map;JLjava/util/List;Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;Landroid/location/Location;Ljava/lang/String;Landroid/graphics/Bitmap;ILcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;Lcom/google/android/velvet/Query$1;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/velvet/Corpus;
    .param p5    # I
    .param p6    # Ljava/util/Map;
    .param p7    # J
    .param p9    # Ljava/util/List;
    .param p10    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p11    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;
    .param p12    # Landroid/location/Location;
    .param p13    # Ljava/lang/String;
    .param p14    # Landroid/graphics/Bitmap;
    .param p15    # I
    .param p16    # Lcom/google/android/velvet/presenter/UiMode;
    .param p17    # Landroid/os/Bundle;
    .param p18    # Lcom/google/android/velvet/Query$1;

    invoke-direct/range {p0 .. p17}, Lcom/google/android/velvet/Query;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/Corpus;ILjava/util/Map;JLjava/util/List;Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;Landroid/location/Location;Ljava/lang/String;Landroid/graphics/Bitmap;ILcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/Query;Landroid/os/Parcel;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/Query;->fromParcel(Landroid/os/Parcel;)Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/velvet/Query;)Landroid/location/Location;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mLocationOverride:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/velvet/Query;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCannedAudio:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/velvet/Query;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/velvet/Query;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget v0, p0, Lcom/google/android/velvet/Query;->mGogglesDisclosedCapability:I

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/UiMode;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/velvet/Query;)Landroid/os/Bundle;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$1600()Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    sget-object v0, Lcom/google/android/velvet/Query;->sLatestCommitId:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/Query;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/velvet/Query;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Corpus;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/Query;)J
    .locals 2
    .param p0    # Lcom/google/android/velvet/Query;

    iget-wide v0, p0, Lcom/google/android/velvet/Query;->mCommitId:J

    return-wide v0
.end method

.method static synthetic access$600(Lcom/google/android/velvet/Query;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget v0, p0, Lcom/google/android/velvet/Query;->mResultIndex:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/velvet/Query;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mAlternateSpans:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    return-object v0
.end method

.method private assertSearchable()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not searchable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    return-void
.end method

.method private buildUpon()Lcom/google/android/velvet/Query$Builder;
    .locals 2

    sget-object v1, Lcom/google/android/velvet/Query;->sBuilder:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/Query$Builder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/velvet/Query$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/velvet/Query$Builder;-><init>(Lcom/google/android/velvet/Query$1;)V

    sget-object v1, Lcom/google/android/velvet/Query;->sBuilder:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0, p0}, Lcom/google/android/velvet/Query$Builder;->buildUpon(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v1

    return-object v1
.end method

.method public static equivalentForSearchDisregardingParams(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z
    .locals 2
    .param p0    # Lcom/google/android/velvet/Query;
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/Util;->equalsIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/Query;->mResultIndex:I

    iget v1, p1, Lcom/google/android/velvet/Query;->mResultIndex:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mLocationOverride:Landroid/location/Location;

    iget-object v1, p1, Lcom/google/android/velvet/Query;->mLocationOverride:Landroid/location/Location;

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static equivalentForSuggest(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z
    .locals 2
    .param p0    # Lcom/google/android/velvet/Query;
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/Util;->equalsIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private fromParcel(Landroid/os/Parcel;)Lcom/google/android/velvet/Query;
    .locals 19

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/velvet/PendingCorpus;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v2, v6}, Lcom/google/android/velvet/PendingCorpus;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/searchcommon/util/Util;->bundleToStringMap(Landroid/os/Bundle;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    sget-object v2, Lcom/google/android/velvet/Query;->sLatestCommitId:Ljava/util/concurrent/atomic/AtomicLong;

    sget-object v10, Lcom/google/android/velvet/Query;->sLatestCommitId:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v10

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    const-class v2, Landroid/location/Location;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Landroid/location/Location;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    const-class v2, Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Landroid/graphics/Bitmap;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v16

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v17

    check-cast v17, Lcom/google/android/velvet/presenter/UiMode;

    const-class v2, Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v18

    const/high16 v2, 0x1000000

    or-int/2addr v2, v1

    new-instance v1, Lcom/google/android/velvet/Query;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct/range {v1 .. v18}, Lcom/google/android/velvet/Query;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/Corpus;ILjava/util/Map;JLjava/util/List;Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;Landroid/location/Location;Ljava/lang/String;Landroid/graphics/Bitmap;ILcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)V

    return-object v1
.end method

.method private getSelectionString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getSelection()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "unchanged"

    goto :goto_0

    :sswitch_1
    const-string v0, "start"

    goto :goto_0

    :sswitch_2
    const-string v0, "end"

    goto :goto_0

    :sswitch_3
    const-string v0, "all"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x100 -> :sswitch_1
        0x200 -> :sswitch_2
        0x300 -> :sswitch_3
    .end sparse-switch
.end method

.method private getSuggestionType()I
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    const v1, 0xf000

    and-int/2addr v0, v1

    return v0
.end method

.method private getSuggestionTypeString()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getSuggestionType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "none"

    goto :goto_0

    :sswitch_1
    const-string v0, "clicked"

    goto :goto_0

    :sswitch_2
    const-string v0, "refined"

    goto :goto_0

    :sswitch_3
    const-string v0, "prefetch"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1000 -> :sswitch_1
        0x2000 -> :sswitch_2
        0x3000 -> :sswitch_3
    .end sparse-switch
.end method

.method private getTrigger()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    and-int/lit16 v0, v0, 0xf0

    return v0
.end method

.method private getTriggerString()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getTrigger()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "user"

    goto :goto_0

    :sswitch_1
    const-string v0, "intent"

    goto :goto_0

    :sswitch_2
    const-string v0, "prefetch"

    goto :goto_0

    :sswitch_3
    const-string v0, "predictive"

    goto :goto_0

    :sswitch_4
    const-string v0, "webview"

    goto :goto_0

    :sswitch_5
    const-string v0, "hotword"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x20 -> :sswitch_2
        0x30 -> :sswitch_3
        0x40 -> :sswitch_4
        0x80 -> :sswitch_5
    .end sparse-switch
.end method

.method private getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    and-int/lit8 v0, v0, 0xf

    return v0
.end method

.method private getTypeString()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "text"

    goto :goto_0

    :pswitch_1
    const-string v0, "voice"

    goto :goto_0

    :pswitch_2
    const-string v0, "goggles"

    goto :goto_0

    :pswitch_3
    const-string v0, "sound"

    goto :goto_0

    :pswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sentinel["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getSentinelMode()Lcom/google/android/velvet/presenter/UiMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getWebCorpus()Lcom/google/android/velvet/WebCorpus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    check-cast v0, Lcom/google/android/velvet/WebCorpus;

    return-object v0
.end method

.method public static isSameQueryType(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z
    .locals 2
    .param p0    # Lcom/google/android/velvet/Query;
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getType()I

    move-result v0

    invoke-direct {p1}, Lcom/google/android/velvet/Query;->getType()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public canCommit()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->isTextSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canUseToSearch()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/velvet/Query;->SEARCHABLE_EMPTY_STRING:Ljava/lang/String;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    instance-of v0, v0, Lcom/google/android/velvet/WebCorpus;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearCommit()Lcom/google/android/velvet/Query;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/Query$Builder;->setCommitId(J)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/16 v1, 0xf00

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->clearFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public committed()Lcom/google/android/velvet/Query;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->committed()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public expectActionFromGws()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->shouldShowCards()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->isSoundSearch()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fromClickedSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/16 v1, 0x1000

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/velvet/Query$Builder;->setSuggestion(ILcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public fromIntent()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setTrigger(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public fromIntent(Landroid/content/Intent;)Lcom/google/android/velvet/Query;
    .locals 4
    .param p1    # Landroid/content/Intent;

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->getQueryString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->shouldSelectAllQuery(Landroid/content/Intent;)Z

    move-result v1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v3

    if-eqz v1, :cond_0

    const/16 v2, 0x300

    :goto_0
    invoke-virtual {v3, v2}, Lcom/google/android/velvet/Query$Builder;->setSelection(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/Query$Builder;->setTrigger(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v2

    return-object v2

    :cond_0
    const/16 v2, 0x200

    goto :goto_0
.end method

.method public fromPredictive(Ljava/lang/String;Landroid/location/Location;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/location/Location;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setTrigger(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/Query$Builder;->setLocationOverride(Landroid/location/Location;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public fromQueryRefinement(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/velvet/Query$Builder;->setSuggestion(ILcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public fromSuggestionToPrefetch(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/16 v1, 0x3000

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/velvet/Query$Builder;->setSuggestion(ILcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public fromWebView()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setTrigger(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public getAlternateSpans()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mAlternateSpans:Ljava/util/List;

    return-object v0
.end method

.method public getCannedAudio()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCannedAudio:Ljava/lang/String;

    return-object v0
.end method

.method public getClickedSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getSuggestionType()I

    move-result v0

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCommitId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/velvet/Query;->mCommitId:J

    return-wide v0
.end method

.method public getCorpus()Lcom/google/android/velvet/Corpus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    return-object v0
.end method

.method public getCorpusIdentifier()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->assertSearchable()V

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/WebCorpus;->getIdentifier()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method public getGogglesDisclosedCapability()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/Query;->mGogglesDisclosedCapability:I

    return v0
.end method

.method public getGogglesQueryImage()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getLocationOverride()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mLocationOverride:Landroid/location/Location;

    return-object v0
.end method

.method public getPersistCgiParameters()Lcom/google/common/collect/ImmutableMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mPersistCgiParameters:Lcom/google/common/collect/ImmutableMap;

    return-object v0
.end method

.method getPrefetchPattern()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->assertSearchable()V

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/WebCorpus;->getPrefetchPattern()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrefetchSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getSuggestionType()I

    move-result v0

    const/16 v1, 0x3000

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getQueryString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryStringForSearch()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/common/base/CharMatcher;->WHITESPACE:Lcom/google/common/base/CharMatcher;

    iget-object v1, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/CharMatcher;->trimAndCollapseFrom(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getQueryStringForSuggest()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/common/base/CharMatcher;->WHITESPACE:Lcom/google/common/base/CharMatcher;

    sget-object v1, Lcom/google/common/base/CharMatcher;->WHITESPACE:Lcom/google/common/base/CharMatcher;

    iget-object v2, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Lcom/google/common/base/CharMatcher;->collapseFrom(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/base/CharMatcher;->trimLeadingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRefinementSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getSuggestionType()I

    move-result v0

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mRequestId:Ljava/lang/String;

    return-object v0
.end method

.method public getResultIndex()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/Query;->mResultIndex:I

    return v0
.end method

.method public getSelection()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    and-int/lit16 v0, v0, 0xf00

    return v0
.end method

.method public getSentinelMode()Lcom/google/android/velvet/presenter/UiMode;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

    return-object v0
.end method

.method public getSuggestionClickLogInfo()Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    return-object v0
.end method

.method public getWebSearchPattern()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->assertSearchable()V

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->isPrefetch()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getPrefetchPattern()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/WebCorpus;->getWebSearchPattern()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public goggles(I)Lcom/google/android/velvet/Query;
    .locals 4
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setGogglesQueryBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    const-string v2, "web"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/Corpus;->getCorpus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/Corpus;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/Query$Builder;->setCorpus(Lcom/google/android/velvet/Corpus;Z)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->setGogglesDisclosedCapability(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public isEmptySuggestQuery()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isGogglesSearch()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIntentQuery()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getTrigger()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrefetch()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getTrigger()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isQueryTextFromGoggles()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isQueryTextFromVoice()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRestoredState()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecondarySearchQuery()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSentinel()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSoundSearch()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTextSearch()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getType()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTriggeredFromHotword()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getTrigger()I

    move-result v0

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUserQuery()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getTrigger()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVoiceSearch()Z
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWebViewQuery()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getTrigger()I

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public offlineGoggles()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setGogglesQueryBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setGogglesDisclosedCapability(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public sentinel(Lcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/Query$Builder;->setSentinel(Lcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public shouldPlayTts()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldResendLastRecording()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldShowCards()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->shouldShowCards()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/Query;->mResultIndex:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldSuppressAnswers()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public soundSearchFromAction()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public soundSearchFromPromotedQuery()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->clearFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public text()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Query["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getTypeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getTriggerString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/Query;->mResultIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " CID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/velvet/Query;->mCommitId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sel-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getSelectionString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sug-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->getSuggestionTypeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->shouldSuppressAnswers()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ", suppress-answers"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->shouldPlayTts()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ", play-tts"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->shouldResendLastRecording()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ", resend-last-recording"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mLocationOverride:Landroid/location/Location;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", LocationOverride:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/velvet/Query;->mLocationOverride:Landroid/location/Location;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCannedAudio:Ljava/lang/String;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", CannedAutio:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/velvet/Query;->mCannedAudio:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", mPersistCgiParameters: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/Query;->mPersistCgiParameters:Lcom/google/common/collect/ImmutableMap;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_2

    :cond_3
    const-string v0, ""

    goto :goto_3

    :cond_4
    const-string v0, ""

    goto :goto_4
.end method

.method public voiceSearch()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->clearFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setCannedAudio(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public voiceSearchForPinholeWithCorpus(Lcom/google/android/velvet/WebCorpus;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Lcom/google/android/velvet/WebCorpus;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/velvet/Query;->SEARCHABLE_EMPTY_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/velvet/Query$Builder;->setCorpus(Lcom/google/android/velvet/Corpus;Z)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->clearFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setCannedAudio(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public voiceSearchFromHotword()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setTrigger(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->clearFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setCannedAudio(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public voiceSearchWithCannedAudio(Ljava/lang/String;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->clearFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->setCannedAudio(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public voiceSearchWithLastRecording()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setCannedAudio(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method voiceSearchWithRecognizedQuery(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/velvet/Query;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/velvet/Query$Builder;->setRequestId(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withCommitIdFrom(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Query;
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getCommitId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/Query$Builder;->setCommitId(J)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withCorpus(Lcom/google/android/velvet/Corpus;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Lcom/google/android/velvet/Corpus;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/velvet/Query$Builder;->setCorpus(Lcom/google/android/velvet/Corpus;Z)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withGogglesQueryImage(Landroid/graphics/Bitmap;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->setGogglesQueryBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withGogglesTextAndCorpus(Ljava/lang/String;Lcom/google/android/velvet/Corpus;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Corpus;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/android/velvet/Query$Builder;->setCorpus(Lcom/google/android/velvet/Corpus;Z)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x200000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withPersistCgiParameters(Ljava/util/Map;)Lcom/google/android/velvet/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/velvet/Query;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->setPersistCgiParameters(Ljava/util/Map;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setTrigger(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object p0

    goto :goto_0
.end method

.method public withQueryStringAndIndex(Ljava/lang/String;I)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setTrigger(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/Query$Builder;->setResultIndex(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withQueryStringCorpusIndexAndPersistCgiParameters(Ljava/lang/String;Lcom/google/android/velvet/Corpus;ILjava/util/Map;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Corpus;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/Corpus;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/velvet/Query;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcom/google/android/velvet/Query$Builder;->setCorpus(Lcom/google/android/velvet/Corpus;Z)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/velvet/Query$Builder;->setResultIndex(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/velvet/Query$Builder;->setPersistCgiParameters(Ljava/util/Map;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withRecognizedText(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/velvet/Query;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setSelection(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x100000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/velvet/Query$Builder;->setRequestId(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withSecondarySearchQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/Query;->withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-direct {v1}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v1

    return-object v1
.end method

.method public withSuppressedAnswers()Lcom/google/android/velvet/Query;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query$Builder;->setFlag(I)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public withUpdatedCorpora(Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpora;

    invoke-direct {p0}, Lcom/google/android/velvet/Query;->buildUpon()Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query$Builder;->updateCorpora(Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query$Builder;->build()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/velvet/Query;->mFlags:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mRequestId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->getIdentifier()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mPersistCgiParameters:Lcom/google/common/collect/ImmutableMap;

    invoke-static {v0}, Lcom/google/android/searchcommon/util/Util;->stringMapToBundle(Ljava/util/Map;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    iget v0, p0, Lcom/google/android/velvet/Query;->mResultIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/google/android/velvet/Query;->mCommitId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mLocationOverride:Landroid/location/Location;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mCannedAudio:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/google/android/velvet/Query;->mGogglesDisclosedCapability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/google/android/velvet/Query;->mExtras:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    return-void
.end method
