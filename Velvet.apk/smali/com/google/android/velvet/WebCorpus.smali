.class public Lcom/google/android/velvet/WebCorpus;
.super Lcom/google/android/velvet/Corpus;
.source "WebCorpus.java"


# instance fields
.field private final mNeedLocation:Z

.field private final mPrefetchPattern:Ljava/lang/String;

.field private final mQueryParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mShowCards:Z

.field private final mSupportedLocales:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUrlAuthority:Ljava/lang/String;

.field private final mUrlParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUrlPath:Ljava/lang/String;

.field private final mWebSearchPattern:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZZLcom/google/android/velvet/Corpus;Lcom/google/android/velvet/Corpora;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # Landroid/net/Uri;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p11    # Z
    .param p12    # Z
    .param p13    # Lcom/google/android/velvet/Corpus;
    .param p14    # Lcom/google/android/velvet/Corpora;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZZ",
            "Lcom/google/android/velvet/Corpus;",
            "Lcom/google/android/velvet/Corpora;",
            ")V"
        }
    .end annotation

    const-string v2, "web"

    const v6, 0x7f040023

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v7, p13

    move-object/from16 v8, p14

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/google/android/velvet/Corpus;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;ILcom/google/android/velvet/Corpus;Lcom/google/android/velvet/Corpora;Ljava/util/Map;)V

    iput-object p4, p0, Lcom/google/android/velvet/WebCorpus;->mWebSearchPattern:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/velvet/WebCorpus;->mPrefetchPattern:Ljava/lang/String;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mUrlPath:Ljava/lang/String;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mUrlAuthority:Ljava/lang/String;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mUrlParams:Ljava/util/Map;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mQueryParams:Ljava/util/Map;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mSupportedLocales:Ljava/util/List;

    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/velvet/WebCorpus;->mNeedLocation:Z

    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/google/android/velvet/WebCorpus;->mShowCards:Z

    return-void
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)Lcom/google/android/velvet/WebCorpus;
    .locals 15
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/google/android/velvet/WebCorpus;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/velvet/WebCorpus;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v1, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v9, p3

    move/from16 v12, p4

    invoke-direct/range {v0 .. v14}, Lcom/google/android/velvet/WebCorpus;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZZLcom/google/android/velvet/Corpus;Lcom/google/android/velvet/Corpora;)V

    return-object v0
.end method

.method public static createWebCorpus(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;Lcom/google/android/velvet/WebCorpus;Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/WebCorpus;
    .locals 15
    .param p0    # Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .param p1    # Lcom/google/android/velvet/WebCorpus;
    .param p2    # Lcom/google/android/velvet/Corpora;

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getPrefetchPattern()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/velvet/WebCorpus;->getPrefetchPattern()Ljava/lang/String;

    move-result-object v5

    :cond_0
    new-instance v0, Lcom/google/android/velvet/WebCorpus;

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getCorpusIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getIcon()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getWebSearchPattern()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getUrlPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getUrlAuthority()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getUrlParamsList()Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/velvet/WebCorpus;->parameterMapFromList(Ljava/util/List;)Ljava/util/Map;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getQueryParamsList()Ljava/util/List;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/velvet/WebCorpus;->parameterMapFromList(Ljava/util/List;)Ljava/util/Map;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getSupportedLocaleList()Ljava/util/List;

    move-result-object v10

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getRequiresLocation()Z

    move-result v11

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getShowCards()Z

    move-result v12

    move-object/from16 v13, p1

    move-object/from16 v14, p2

    invoke-direct/range {v0 .. v14}, Lcom/google/android/velvet/WebCorpus;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZZLcom/google/android/velvet/Corpus;Lcom/google/android/velvet/Corpora;)V

    return-object v0
.end method

.method private static parameterMapFromList(Ljava/util/List;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public getPrefetchPattern()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mPrefetchPattern:Ljava/lang/String;

    return-object v0
.end method

.method public getWebSearchPattern()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mWebSearchPattern:Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mSupportedLocales:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mSupportedLocales:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/WebCorpus;->mSupportedLocales:Ljava/util/List;

    invoke-static {}, Lcom/google/android/searchcommon/util/Util;->getLocaleString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public matchesUrl(Landroid/net/Uri;Z)Z
    .locals 6
    .param p1    # Landroid/net/Uri;
    .param p2    # Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/velvet/WebCorpus;->mUrlPath:Ljava/lang/String;

    invoke-static {v2, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/velvet/WebCorpus;->mUrlAuthority:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    if-eqz p2, :cond_3

    iget-object v2, p0, Lcom/google/android/velvet/WebCorpus;->mUrlParams:Ljava/util/Map;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/WebCorpus;->mUrlParams:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v4

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_0
.end method

.method public shouldShowCards()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/WebCorpus;->mShowCards:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WebCorpus["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/WebCorpus;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/WebCorpus;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/WebCorpus;->getWebSearchPattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PATH:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/WebCorpus;->mUrlPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", AUTH:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/WebCorpus;->mUrlAuthority:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PARAMS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/WebCorpus;->mUrlParams:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
