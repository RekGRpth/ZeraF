.class Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;
.super Landroid/os/AsyncTask;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateAccountConfigurationsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mAccounts:[Landroid/accounts/Account;

.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;[Landroid/accounts/Account;)V
    .locals 0
    .param p2    # [Landroid/accounts/Account;

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->mAccounts:[Landroid/accounts/Account;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 14
    .param p1    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x1

    iget-object v11, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->mAccounts:[Landroid/accounts/Account;

    array-length v11, v11

    invoke-static {v11}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->mAccounts:[Landroid/accounts/Account;

    array-length v10, v4

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v10, :cond_4

    aget-object v0, v4, v9

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_1
    const/4 v11, 0x3

    if-ge v8, v11, :cond_0

    if-nez v7, :cond_0

    iget-object v11, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;
    invoke-static {v11}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2400(Lcom/google/android/velvet/tg/FirstRunActivity;)Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v11

    invoke-interface {v11, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->fetchAccountConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    move-result-object v7

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_0
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;->hasConfiguration()Z

    move-result v11

    if-nez v11, :cond_2

    :cond_1
    const-string v11, "FirstRunActivity"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "config was null for: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;->getConfiguration()Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v5

    iget-object v11, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;
    invoke-static {v11}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2400(Lcom/google/android/velvet/tg/FirstRunActivity;)Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v11

    invoke-interface {v11, v5, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->userCanRunTheGoogle(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_3

    const/4 v6, 0x1

    :goto_3
    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    invoke-direct {v1, v0, v6}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;-><init>(Landroid/accounts/Account;Z)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v11, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;
    invoke-static {v11}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2400(Lcom/google/android/velvet/tg/FirstRunActivity;)Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v11

    invoke-interface {v11, v5, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->saveConfiguration(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)V

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    goto :goto_3

    :cond_4
    return-object v3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # setter for: Lcom/google/android/velvet/tg/FirstRunActivity;->mFetchingConfigs:Z
    invoke-static {v0, v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2302(Lcom/google/android/velvet/tg/FirstRunActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # setter for: Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;
    invoke-static {v0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2502(Lcom/google/android/velvet/tg/FirstRunActivity;Ljava/util/List;)Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->mAccounts:[Landroid/accounts/Account;

    array-length v1, v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity;->mSkipToOptIn:Z
    invoke-static {v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2600(Lcom/google/android/velvet/tg/FirstRunActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity;->loggedInUserCanRunMariner()Z
    invoke-static {v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2700(Lcom/google/android/velvet/tg/FirstRunActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V
    invoke-static {v0, v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$900(Lcom/google/android/velvet/tg/FirstRunActivity;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity;->startMarinerOrSearch()V
    invoke-static {v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2800(Lcom/google/android/velvet/tg/FirstRunActivity;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v1, 0x7f0d00cb

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V
    invoke-static {v0, v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$900(Lcom/google/android/velvet/tg/FirstRunActivity;Z)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/velvet/tg/FirstRunActivity;->mFetchingConfigs:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2302(Lcom/google/android/velvet/tg/FirstRunActivity;Z)Z

    return-void
.end method
