.class Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;
.super Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SampleCardScreen"
.end annotation


# instance fields
.field private final mSampleCardLayout:I

.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/tg/FirstRunActivity$1;)V

    iput p6, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;->mSampleCardLayout:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;ILcom/google/android/velvet/tg/FirstRunActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # Lcom/google/android/velvet/tg/FirstRunActivity$1;

    invoke-direct/range {p0 .. p6}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method setup(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->setup(Landroid/view/View;)V

    const v3, 0x7f1000cd

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;->mSampleCardLayout:I

    invoke-virtual {v2, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    :cond_0
    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v4, 0x7f1000c4

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;

    const/4 v3, 0x0

    invoke-direct {v0, p1, v3}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;-><init>(Landroid/view/View;Lcom/google/android/velvet/tg/FirstRunActivity$1;)V

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v4, 0x7f1000c6

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    new-instance v4, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen$1;

    invoke-direct {v4, p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen$1;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen$2;

    invoke-direct {v3, p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen$2;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;Landroid/view/View;)V

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->dealSoon(Ljava/lang/Runnable;)V
    invoke-static {v0, v3}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;->access$1600(Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;Ljava/lang/Runnable;)V

    return-void
.end method
