.class Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;
.super Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FirstRunScreenWithNext"
.end annotation


# instance fields
.field private final mContentText:Ljava/lang/String;

.field private final mTitleText:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V

    iput-object p4, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->mTitleText:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->mContentText:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/tg/FirstRunActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/velvet/tg/FirstRunActivity$1;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method setup(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const v3, 0x7f1000c6

    const/4 v2, 0x0

    const v0, 0x7f100059

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->mTitleText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f1000ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->mContentText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-virtual {v0, v3}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext$1;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v1, 0x7f1000c7

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-virtual {v0, v3}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v1, 0x7f1000c4

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected show()Landroid/view/View;
    .locals 3

    invoke-super {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->show()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v2, 0x7f1000c6

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    return-object v0
.end method
