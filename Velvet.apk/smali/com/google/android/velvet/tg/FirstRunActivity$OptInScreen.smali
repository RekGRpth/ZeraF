.class Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;
.super Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OptInScreen"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;II)V
    .locals 1
    .param p2    # I
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/google/android/velvet/tg/FirstRunActivity$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;II)V

    return-void
.end method


# virtual methods
.method setup(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v4, 0x7f1000c9

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    const-string v4, "KR"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f0d01c4

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    const v3, 0x7f1000cb

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v4, 0x7f0d0016

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v4, 0x7f0d01c5

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    const v3, 0x7f1000b2

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    new-instance v4, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen$1;

    invoke-direct {v4, p0}, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen$1;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen$2;

    invoke-direct {v3, p0}, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen$2;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v4, 0x7f1000c8

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    new-instance v4, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen$3;

    invoke-direct {v4, p0}, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen$3;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v4, 0x7f1000c4

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v4, 0x7f1000c6

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v4, 0x7f1000c7

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    return-void
.end method
