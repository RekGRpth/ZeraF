.class public Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SuggestionGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;
    }
.end annotation


# instance fields
.field public animationIndex:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public appearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

.field public canDismiss:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public canDrag:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public column:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public disappearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

.field public fillViewport:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public inheritPadding:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public noPadding:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public removeOnDismiss:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field


# direct methods
.method public constructor <init>(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->inheritPadding:Z

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->fillViewport:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    iput p3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iput-boolean v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->inheritPadding:Z

    iput-boolean v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->fillViewport:Z

    iput-boolean v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    sget-object v1, Lcom/google/android/googlequicksearchbox/R$styleable;->SuggestionGridLayout_Layout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDrag:Z

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->inheritPadding:Z

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->fillViewport:Z

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->getAnimationType(Landroid/content/res/TypedArray;ILcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->getAnimationType(Landroid/content/res/TypedArray;ILcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->disappearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 5
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {p0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    iput-boolean v4, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v4, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iput-boolean v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->inheritPadding:Z

    iput-boolean v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->fillViewport:Z

    iput-boolean v4, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    instance-of v1, p1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    iget v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    iput v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    iget-boolean v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iget-boolean v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iget-boolean v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iget-boolean v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->inheritPadding:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->inheritPadding:Z

    iget-object v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iput-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iget-object v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->disappearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iput-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->disappearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iget v1, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->animationIndex:I

    iput v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->animationIndex:I

    :cond_0
    return-void
.end method

.method private getAnimationType(Landroid/content/res/TypedArray;ILcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I
    .param p3    # Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    const/4 v1, -0x1

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-static {}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->values()[Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    move-result-object v1

    aget-object p3, v1, v0

    :cond_0
    return-object p3
.end method
