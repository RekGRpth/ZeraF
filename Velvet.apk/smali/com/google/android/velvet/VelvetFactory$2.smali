.class Lcom/google/android/velvet/VelvetFactory$2;
.super Ljava/lang/Object;
.source "VelvetFactory.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/VelvetFactory;->createBackgroundTask(Ljava/lang/String;Z)Ljava/util/concurrent/Callable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/VelvetFactory;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/VelvetFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/VelvetFactory$2;->this$0:Lcom/google/android/velvet/VelvetFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetFactory$2;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory$2;->this$0:Lcom/google/android/velvet/VelvetFactory;

    # getter for: Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;
    invoke-static {v0}, Lcom/google/android/velvet/VelvetFactory;->access$000(Lcom/google/android/velvet/VelvetFactory;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->flushEvents()V

    const/4 v0, 0x0

    return-object v0
.end method
