.class public Lcom/google/android/velvet/gallery/VelvetImage;
.super Ljava/lang/Object;
.source "VelvetImage.java"


# instance fields
.field private mDomain:Ljava/lang/String;

.field private mHeight:I

.field private mId:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mNavigationUri:Ljava/lang/String;

.field private mSnippet:Ljava/lang/String;

.field private mSourceUri:Ljava/lang/String;

.field private mThumbnailHeight:I

.field private mThumbnailUri:Ljava/lang/String;

.field private mThumbnailWidth:I

.field private mUri:Ljava/lang/String;

.field private mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/velvet/gallery/VelvetImage;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/velvet/gallery/VelvetImage;

    invoke-virtual {v0}, Lcom/google/android/velvet/gallery/VelvetImage;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mHeight:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNavigationUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mNavigationUri:Ljava/lang/String;

    return-object v0
.end method

.method public getSnippet()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mSnippet:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mSourceUri:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mThumbnailHeight:I

    return v0
.end method

.method public getThumbnailUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mThumbnailUri:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mThumbnailWidth:I

    return v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mUri:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mWidth:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public setDomain(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mDomain:Ljava/lang/String;

    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mHeight:I

    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mId:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mName:Ljava/lang/String;

    return-void
.end method

.method public setNavigationUri(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mNavigationUri:Ljava/lang/String;

    return-void
.end method

.method public setSnippet(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mSnippet:Ljava/lang/String;

    return-void
.end method

.method public setSourceUri(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mSourceUri:Ljava/lang/String;

    return-void
.end method

.method public setThumbnailHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mThumbnailHeight:I

    return-void
.end method

.method public setThumbnailUri(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mThumbnailUri:Ljava/lang/String;

    return-void
.end method

.method public setThumbnailWidth(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mThumbnailWidth:I

    return-void
.end method

.method public setUri(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mUri:Ljava/lang/String;

    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/gallery/VelvetImage;->mWidth:I

    return-void
.end method
