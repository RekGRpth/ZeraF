.class Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity$1;
.super Ljava/lang/Object;
.source "NavigatingPhotoViewActivity.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->updateNavItem(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;

.field final synthetic val$navigateUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity$1;->this$0:Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;

    iput-object p2, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity$1;->val$navigateUri:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity$1;->val$navigateUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity$1;->this$0:Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    return v1
.end method
