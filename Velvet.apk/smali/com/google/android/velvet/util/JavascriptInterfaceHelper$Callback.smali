.class public Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;
.super Ljava/lang/Object;
.source "JavascriptInterfaceHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/util/JavascriptInterfaceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Callback"
.end annotation


# instance fields
.field private final mArgCount:I

.field private final mCallBuidler:Ljava/lang/StringBuilder;

.field private mExtraArgs:[Ljava/lang/Object;

.field private final mName:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # [Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mName:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mArgCount:I

    iput-object p3, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mExtraArgs:[Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    return-void
.end method

.method private toString(Ljava/lang/StringBuilder;)V
    .locals 3
    .param p1    # Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mArgCount:I

    if-ge v1, v2, :cond_1

    if-lez v1, :cond_0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v2, "a"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "function("

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v2, "){"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mExtraArgs:[Ljava/lang/Object;

    array-length v2, v2

    if-lez v2, :cond_2

    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mExtraArgs:[Ljava/lang/Object;

    # invokes: Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->appendArgsTo(Ljava/lang/StringBuilder;[Ljava/lang/Object;)V
    invoke-static {p1, v2}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->access$000(Ljava/lang/StringBuilder;[Ljava/lang/Object;)V

    :cond_2
    const-string v2, ")}"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method


# virtual methods
.method public varargs callNow([Ljava/lang/Object;)Ljava/lang/String;
    .locals 3
    .param p1    # [Ljava/lang/Object;

    const/4 v1, 0x0

    array-length v0, p1

    iget v2, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mArgCount:I

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->toString(Ljava/lang/StringBuilder;)V

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    const-string v1, ")("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    # invokes: Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->appendArgsTo(Ljava/lang/StringBuilder;[Ljava/lang/Object;)V
    invoke-static {v0, p1}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->access$000(Ljava/lang/StringBuilder;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->toString(Ljava/lang/StringBuilder;)V

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->mCallBuidler:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
