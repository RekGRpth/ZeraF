.class Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;
.super Lcom/google/android/velvet/presenter/QueryState$LoadState;
.source "QueryState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/QueryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TtsActionLoadState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/velvet/presenter/QueryState$LoadState",
        "<",
        "Lcom/google/android/velvet/presenter/Action;",
        ">;"
    }
.end annotation


# instance fields
.field private mTtsAvailable:Z


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected setState(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->setState(I)V

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->mTtsAvailable:Z

    :cond_1
    return-void
.end method

.method public takeTtsAvailable()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->mTtsAvailable:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->mTtsAvailable:Z

    return v0
.end method

.method ttsAvailable()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$TtsActionLoadState;->mTtsAvailable:Z

    return-void
.end method
