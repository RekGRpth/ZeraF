.class Lcom/google/android/velvet/presenter/SearchController$2;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/SearchController;->makeImageUniversalEventHandler(Lcom/google/android/velvet/gallery/ImageMetadataController;Ljava/lang/String;)Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchController;

.field final synthetic val$controller:Lcom/google/android/velvet/gallery/ImageMetadataController;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/SearchController;Ljava/lang/String;Lcom/google/android/velvet/gallery/ImageMetadataController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController$2;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/SearchController$2;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/SearchController$2;->val$controller:Lcom/google/android/velvet/gallery/ImageMetadataController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleEvents(Ljava/util/Map;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$2;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mWebViewActive:Z
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$700(Lcom/google/android/velvet/presenter/SearchController;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    const-string v1, "gsais"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "gsais"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$2;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # invokes: Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$800(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$2;->val$packageName:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->createPhotoViewIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivity(Landroid/content/Intent;)Z

    :cond_0
    const-string v1, "gsaim"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$2;->val$controller:Lcom/google/android/velvet/gallery/ImageMetadataController;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$2;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v3

    const-string v1, "gsaim"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/velvet/gallery/ImageMetadataController;->setQueryWithJson(Lcom/google/android/velvet/Query;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
