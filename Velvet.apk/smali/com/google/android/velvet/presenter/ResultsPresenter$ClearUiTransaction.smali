.class Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "ResultsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/ResultsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClearUiTransaction"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/ResultsPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/ResultsPresenter;Lcom/google/android/velvet/presenter/ResultsPresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/ResultsPresenter;
    .param p2    # Lcom/google/android/velvet/presenter/ResultsPresenter$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;-><init>(Lcom/google/android/velvet/presenter/ResultsPresenter;)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    const/4 v2, 0x5

    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->removeAllViews()V

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->setScrollY(I)V

    invoke-interface {p1, v1}, Lcom/google/android/velvet/presenter/MainContentUi;->setWhiteBackgroundState(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsTextLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$300(Lcom/google/android/velvet/presenter/ResultsPresenter;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$400(Lcom/google/android/velvet/presenter/ResultsPresenter;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$500(Lcom/google/android/velvet/presenter/ResultsPresenter;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$400(Lcom/google/android/velvet/presenter/ResultsPresenter;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$400(Lcom/google/android/velvet/presenter/ResultsPresenter;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # setter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewVisible:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$602(Lcom/google/android/velvet/presenter/ResultsPresenter;Z)Z

    :cond_0
    return-void
.end method
