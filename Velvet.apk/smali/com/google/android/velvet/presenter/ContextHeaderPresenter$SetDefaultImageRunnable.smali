.class Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;
.super Ljava/lang/Object;
.source "ContextHeaderPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/ContextHeaderPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetDefaultImageRunnable"
.end annotation


# instance fields
.field private volatile mCanceled:Z

.field final synthetic this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->mCanceled:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;Lcom/google/android/velvet/presenter/ContextHeaderPresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter;
    .param p2    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;-><init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->mCanceled:Z

    return v0
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->mCanceled:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    # invokes: Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->getDefaultImage()Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->access$600(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->access$800(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v1

    new-instance v2, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;-><init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setCanceled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->mCanceled:Z

    return-void
.end method
