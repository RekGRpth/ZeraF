.class public Lcom/google/android/velvet/presenter/WebSearchConnectionError;
.super Lcom/google/android/velvet/presenter/SearchError;
.source "WebSearchConnectionError.java"


# instance fields
.field private mHttpErrorCode:I

.field private mHttpErrorMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchError;-><init>()V

    iput p1, p0, Lcom/google/android/velvet/presenter/WebSearchConnectionError;->mHttpErrorCode:I

    iput-object p2, p0, Lcom/google/android/velvet/presenter/WebSearchConnectionError;->mHttpErrorMessage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getErrorMessage()Ljava/lang/CharSequence;
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/presenter/SearchError;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getErrorMessageResId()I
    .locals 1

    const v0, 0x7f0d0390

    return v0
.end method
