.class Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;
.super Ljava/lang/Object;
.source "InAppWebPagePresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->tryAgain()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mVisible:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$100(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mErrorDumpMessage:Lcom/google/android/searchcommon/util/LazyString;
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$202(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;Lcom/google/android/searchcommon/util/LazyString;)Lcom/google/android/searchcommon/util/LazyString;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$300(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->reset()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$400(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/ui/InAppWebPageActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->hideError()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$400(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/ui/InAppWebPageActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$500(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->loadUri(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method
