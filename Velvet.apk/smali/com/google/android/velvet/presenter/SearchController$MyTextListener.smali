.class Lcom/google/android/velvet/presenter/SearchController$MyTextListener;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Lcom/google/android/speech/callback/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyTextListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/Callback",
        "<",
        "Landroid/util/Pair",
        "<",
        "Lcom/google/android/velvet/Query;",
        "Lcom/google/majel/proto/MajelProtos$MajelResponse;",
        ">;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mQuery:Lcom/google/android/velvet/Query;

.field private final mQueryState:Lcom/google/android/velvet/presenter/QueryState;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/QueryState;
    .param p2    # Lcom/google/android/velvet/Query;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;->mQuery:Lcom/google/android/velvet/Query;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/QueryState;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Lcom/google/android/velvet/presenter/SearchController$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;-><init>(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;->onError(Ljava/lang/Void;)V

    return-void
.end method

.method public onError(Ljava/lang/Void;)V
    .locals 3
    .param p1    # Ljava/lang/Void;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;->mQuery:Lcom/google/android/velvet/Query;

    sget-object v2, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    return-void
.end method

.method public onResult(Landroid/util/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/majel/proto/MajelProtos$MajelResponse;",
            ">;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;->mQuery:Lcom/google/android/velvet/Query;

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-static {v0}, Lcom/google/android/velvet/ActionServerResult;->listFromMajelResponse(Lcom/google/majel/proto/MajelProtos$MajelResponse;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/presenter/Action;->fromActionServerResults(Ljava/util/List;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    return-void
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/util/Pair;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;->onResult(Landroid/util/Pair;)V

    return-void
.end method
