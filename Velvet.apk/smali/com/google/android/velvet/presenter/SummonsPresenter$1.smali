.class Lcom/google/android/velvet/presenter/SummonsPresenter$1;
.super Ljava/lang/Object;
.source "SummonsPresenter.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/SummonsPresenter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/searchcommon/summons/Source;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SummonsPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/SummonsPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$1;->this$0:Lcom/google/android/velvet/presenter/SummonsPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/presenter/SummonsPresenter$1;->consume(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public consume(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$1;->this$0:Lcom/google/android/velvet/presenter/SummonsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SummonsPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->access$100(Lcom/google/android/velvet/presenter/SummonsPresenter;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/SummonsPresenter$1$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/velvet/presenter/SummonsPresenter$1$1;-><init>(Lcom/google/android/velvet/presenter/SummonsPresenter$1;Ljava/util/List;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    return v0
.end method
