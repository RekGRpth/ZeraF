.class public Lcom/google/android/velvet/presenter/FooterPresenter;
.super Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
.source "FooterPresenter.java"


# instance fields
.field private final mCorpora:Lcom/google/android/velvet/Corpora;

.field private final mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

.field private mSelectedCorpus:Lcom/google/android/velvet/Corpus;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/Corpora;Lcom/google/android/velvet/presenter/FooterUi;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpora;
    .param p2    # Lcom/google/android/velvet/presenter/FooterUi;

    const-string v0, "footer"

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    return-void
.end method

.method private updateCorpora(Lcom/google/android/velvet/Corpus;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/Corpus;

    if-eqz p1, :cond_2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v3}, Lcom/google/android/velvet/Corpora;->getSummonsCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v3

    if-ne p1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    iget-object v3, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v3, p1}, Lcom/google/android/velvet/presenter/FooterUi;->removeCorpusSelectors(Lcom/google/android/velvet/Corpus;)V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v3, p1, v1}, Lcom/google/android/velvet/presenter/FooterUi;->addCorpusSelector(Lcom/google/android/velvet/Corpus;Z)V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v3, p1}, Lcom/google/android/velvet/Corpora;->getSubCorpora(Lcom/google/android/velvet/Corpus;)Ljava/lang/Iterable;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/Corpus;

    invoke-virtual {v2}, Lcom/google/android/velvet/Corpus;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v3, v2, v1}, Lcom/google/android/velvet/presenter/FooterUi;->addCorpusSelector(Lcom/google/android/velvet/Corpus;Z)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public getSelectedCorpus()Lcom/google/android/velvet/Corpus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mSelectedCorpus:Lcom/google/android/velvet/Corpus;

    return-object v0
.end method

.method public onCorpusClicked(Lcom/google/android/velvet/Corpus;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpus;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/FooterUi;->setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/FooterPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onCorpusSelected(Lcom/google/android/velvet/Corpus;)V

    return-void
.end method

.method public onMenuButtonClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/FooterPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onMenuButtonClick(Landroid/view/View;)V

    return-void
.end method

.method protected onPostAttach(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method protected onPreDetach()V
    .locals 0

    return-void
.end method

.method public onTgFooterButtonPressed()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/FooterPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onTgFooterButtonPressed()V

    return-void
.end method

.method public resetShowMoreCorpora()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/FooterUi;->isCorpusBarLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/FooterUi;->resetShowMoreCorpora()V

    :cond_0
    return-void
.end method

.method public setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpus;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mSelectedCorpus:Lcom/google/android/velvet/Corpus;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/FooterUi;->isCorpusBarLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/FooterUi;->setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V

    :cond_0
    return-void
.end method

.method public setShowCorpusBar(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/FooterUi;->setShowCorpusBar(Z)V

    return-void
.end method

.method public setShowTgFooterButton(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/FooterUi;->setShowTgFooterButton(Z)V

    return-void
.end method

.method public setTgFooterButtonEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/FooterUi;->setTgFooterButtonEnabled(Z)V

    return-void
.end method

.method setTgFooterButtonText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/FooterUi;->setTgFooterButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updateCorpora()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mFooterUi:Lcom/google/android/velvet/presenter/FooterUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/FooterUi;->isCorpusBarLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpora;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/FooterPresenter;->updateCorpora(Lcom/google/android/velvet/Corpus;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/FooterPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpora;->getSummonsCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/FooterPresenter;->updateCorpora(Lcom/google/android/velvet/Corpus;)V

    :cond_0
    return-void
.end method
