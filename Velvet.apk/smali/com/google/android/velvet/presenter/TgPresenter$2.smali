.class Lcom/google/android/velvet/presenter/TgPresenter$2;
.super Ljava/lang/Object;
.source "TgPresenter.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/TgPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/TgPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter$2;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$2;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/TgPresenter;->access$000(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/ViewActionRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->recordViewEndTimes()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$2;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isPredictiveOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$2;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/TgPresenter;->access$000(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/ViewActionRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->recordViewStartTimes()V

    :cond_0
    return-void
.end method
