.class public Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;
.super Lcom/google/android/velvet/presenter/MainContentPresenter;
.source "GogglesDisambiguationPresenter.java"


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)V"
        }
    .end annotation

    const-string v0, "gogglesdisambig"

    invoke-direct {p0, v0, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentFragment;)V

    return-void
.end method

.method private appendFifeThumbnailSizes(Ljava/lang/String;II)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s=w%d-h%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createDownloadListener(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/ui/WebImageView$Listener;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private getResultItemView(Ljava/lang/String;ILcom/google/bionics/goggles/api2/GogglesProtos$Result;ZII)Landroid/view/View;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/bionics/goggles/api2/GogglesProtos$Result;
    .param p4    # Z
    .param p5    # I
    .param p6    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p0, v7}, Lcom/google/android/velvet/VelvetFactory;->createGogglesDisambiguationItem(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v6, 0x7f1000fb

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f1000fc

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getSubtitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f1000fa

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {p3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getFifeImageUrl()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, p5, p6}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->appendFifeThumbnailSizes(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->createDownloadListener(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/ui/WebImageView$Listener;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/ui/WebImageView;->setOnDownloadListener(Lcom/google/android/velvet/ui/WebImageView$Listener;)V

    invoke-virtual {v2, v1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;

    invoke-direct {v6, p0, p3, p1, p2}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;-><init>(Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;Lcom/google/bionics/goggles/api2/GogglesProtos$Result;Ljava/lang/String;I)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p4, :cond_0

    const v6, 0x7f100066

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-object v3
.end method


# virtual methods
.method protected onPostAttach(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0c0054

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0c0055

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v0, p0, v4}, Lcom/google/android/velvet/VelvetFactory;->createGogglesDisambiguation(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->getGogglesResults()Lcom/google/android/goggles/ResultSet;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/goggles/ResultSet;->getResultSetNumber()I

    move-result v2

    invoke-virtual {v11}, Lcom/google/android/goggles/ResultSet;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    if-ne v3, v10, :cond_0

    const/4 v4, 0x1

    :goto_1
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getResultItemView(Ljava/lang/String;ILcom/google/bionics/goggles/api2/GogglesProtos$Result;ZII)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v4, 0x0

    aput-object v8, v0, v4

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->postAddViews([Landroid/view/View;)V

    return-void
.end method

.method protected onPreDetach()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->postRemoveAllViews()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->postResetScroll()V

    return-void
.end method

.method public onViewsDismissed(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter;->onViewsDismissed(Ljava/lang/Iterable;)V

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/TraceTracker;->newSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v1, v2}, Lcom/google/android/goggles/TraceTracker$Session;->addUserEventStartSearch(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getGogglesDisclosedCapability()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/velvet/Query;->goggles(I)Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    return-void
.end method
