.class Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;
.super Ljava/lang/Object;
.source "GogglesDisambiguationPresenter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getResultItemView(Ljava/lang/String;ILcom/google/bionics/goggles/api2/GogglesProtos$Result;ZII)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;

.field final synthetic val$result:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

.field final synthetic val$resultSetNumber:I

.field final synthetic val$sessionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;Lcom/google/bionics/goggles/api2/GogglesProtos$Result;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->this$0:Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->val$result:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->val$sessionId:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->val$resultSetNumber:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->val$result:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getResultId()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->val$sessionId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/goggles/TraceTracker;->getSession(Ljava/lang/String;)Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v1

    iget v2, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->val$resultSetNumber:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/goggles/TraceTracker$Session;->addUserEventDisambigClick(ILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->val$resultSetNumber:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/goggles/TraceTracker$Session;->addImpressionSingleResult(ILjava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    move-result-object v2

    new-instance v3, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;

    invoke-direct {v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;->addResultInfos(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->this$0:Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$2;->val$result:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/presenter/Action;->selectGogglesResult(Lcom/google/bionics/goggles/api2/GogglesProtos$Result;)V

    return-void
.end method
