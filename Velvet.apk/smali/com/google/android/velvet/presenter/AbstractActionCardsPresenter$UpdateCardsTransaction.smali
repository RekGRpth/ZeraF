.class Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "AbstractActionCardsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateCardsTransaction"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;
    }
.end annotation


# instance fields
.field private final mAction:Lcom/google/android/velvet/presenter/Action;

.field private mLastShownController:Lcom/google/android/voicesearch/fragments/AbstractCardController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;"
        }
    .end annotation
.end field

.field private mNumPreparedControllers:I

.field private mNumShownControllers:I

.field private mPendingLayoutView:Landroid/view/View;

.field private mStaleControllers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final mUpdates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;Lcom/google/android/velvet/presenter/Action;)V
    .locals 1
    .param p2    # Lcom/google/android/velvet/presenter/Action;

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    iput v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumPreparedControllers:I

    iput v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumShownControllers:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mPendingLayoutView:Landroid/view/View;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/Action;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getCardControllers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mUpdates:Ljava/util/List;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;
    invoke-static {p1}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mStaleControllers:Ljava/util/Set;

    return-void
.end method

.method private getCardColumn()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->shouldCenterResultCardAndMatchPortraitWidthInLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 11
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    const/4 v8, 0x0

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v9, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;
    invoke-static {v9}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$100(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v9

    if-ne v7, v9, :cond_8

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mStaleControllers:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v7

    if-gtz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mUpdates:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_8

    :cond_0
    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    invoke-virtual {v7, p1}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->onBeforeCardsShown(Lcom/google/android/velvet/presenter/MainContentUi;)V

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mStaleControllers:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-virtual {v0, v7}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mPendingLayoutView:Landroid/view/View;

    if-eqz v7, :cond_3

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_4

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    :goto_2
    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mUpdates:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;

    iget-boolean v7, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mCreate:Z

    if-eqz v7, :cond_5

    if-eqz v4, :cond_2

    iget-object v7, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mView:Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mView:Landroid/view/View;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v7, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mView:Landroid/view/View;

    iget v9, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mIndex:I

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->getCardColumn()I

    move-result v10

    invoke-virtual {v0, v7, v9, v10}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addViewWithIndexAndColumn(Landroid/view/View;II)V

    iget-object v7, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    iput-boolean v8, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;

    move-result-object v7

    iget-object v9, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    iget-object v10, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mView:Landroid/view/View;

    invoke-interface {v7, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_3
    move v4, v8

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    :cond_5
    iget-object v7, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mView:Landroid/view/View;

    invoke-virtual {v0, v7}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;

    move-result-object v7

    iget-object v9, v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-interface {v7, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_6
    if-eqz v4, :cond_7

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mPendingLayoutView:Landroid/view/View;

    invoke-interface {p1, v7, v2}, Lcom/google/android/velvet/presenter/MainContentUi;->showViewsPendingLayout(Landroid/view/View;Ljava/util/List;)V

    :cond_7
    iget-object v8, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;

    move-result-object v7

    iget-object v9, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mLastShownController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-virtual {v8, v7, p1}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->setLastVisibleCard(Landroid/view/View;Lcom/google/android/velvet/presenter/MainContentUi;)V

    :cond_8
    return-void
.end method

.method public prepare()Z
    .locals 15

    const/4 v11, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->getCardControllers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v14

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$100(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v1

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    const/4 v12, 0x1

    :goto_1
    if-eqz v12, :cond_6

    iget v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumPreparedControllers:I

    if-ge v0, v14, :cond_6

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->getCardControllers()Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumPreparedControllers:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->shouldShowCard()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCardFactory:Lcom/google/android/voicesearch/CardFactory;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$200(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Lcom/google/android/voicesearch/CardFactory;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/CardFactory;->createCard(Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mUpdates:Ljava/util/List;

    new-instance v0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;

    iget v4, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumShownControllers:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;-><init>(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;Lcom/google/android/voicesearch/fragments/AbstractCardController;Landroid/view/View;IZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->getPendingLayoutView()Landroid/view/View;

    move-result-object v13

    if-eqz v13, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mPendingLayoutView:Landroid/view/View;

    if-nez v0, :cond_2

    iput-object v13, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mPendingLayoutView:Landroid/view/View;

    :cond_2
    const/4 v12, 0x0

    :cond_3
    iget v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumShownControllers:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumShownControllers:I

    iput-object v2, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mLastShownController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mStaleControllers:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumPreparedControllers:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumPreparedControllers:I

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mUpdates:Ljava/util/List;

    new-instance v6, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    iget v10, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumShownControllers:I

    move-object v7, p0

    move-object v8, v2

    invoke-direct/range {v6 .. v11}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;-><init>(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;Lcom/google/android/voicesearch/fragments/AbstractCardController;Landroid/view/View;IZ)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    iget v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;->mNumPreparedControllers:I

    if-eq v0, v14, :cond_0

    move v5, v11

    goto/16 :goto_0
.end method
