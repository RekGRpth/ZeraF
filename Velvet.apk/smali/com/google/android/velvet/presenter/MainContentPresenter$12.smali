.class Lcom/google/android/velvet/presenter/MainContentPresenter$12;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "MainContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/MainContentPresenter;->postSetLayoutTransitionStartDelay(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

.field final synthetic val$delay:J

.field final synthetic val$transitionType:I


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/MainContentPresenter;IJ)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$12;->this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

    iput p2, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$12;->val$transitionType:I

    iput-wide p3, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$12;->val$delay:J

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$12;->val$transitionType:I

    iget-wide v2, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$12;->val$delay:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setLayoutTransitionStartDelay(IJ)V

    return-void
.end method
