.class Lcom/google/android/velvet/presenter/TgPresenter$1;
.super Ljava/lang/Object;
.source "TgPresenter.java"

# interfaces
.implements Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/TgPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/TgPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter$1;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollAnimationFinished()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$1;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/TgPresenter;->access$000(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/ViewActionRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->recordViewEndTimes()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$1;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/TgPresenter;->access$000(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/ViewActionRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->recordViewStartTimes()V

    return-void
.end method

.method public onScrollChanged(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    return-void
.end method

.method public onScrollFinished()V
    .locals 0

    return-void
.end method

.method public onScrollMarginConsumed(Landroid/view/View;II)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    return-void
.end method
