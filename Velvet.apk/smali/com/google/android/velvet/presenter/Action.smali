.class public Lcom/google/android/velvet/presenter/Action;
.super Landroid/database/DataSetObservable;
.source "Action.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/velvet/presenter/Action;",
            ">;"
        }
    .end annotation
.end field

.field static final KEY_CONTROLLER_CLASS:Ljava/lang/String; = "velvet:action:controller_class"

.field static final KEY_CONTROLLER_STATE:Ljava/lang/String; = "velvet:action:controller_state"

.field public static final NONE:Lcom/google/android/velvet/presenter/Action;


# instance fields
.field private final mActionServerResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ActionServerResult;",
            ">;"
        }
    .end annotation
.end field

.field private mCardControllers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;>;"
        }
    .end annotation
.end field

.field private mControllerBundles:[Landroid/os/Parcelable;

.field private final mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

.field private mFlags:I

.field private final mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

.field private mModifiedQuery:Lcom/google/android/velvet/Query;

.field private final mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

.field private final mRecognizeException:Lcom/google/android/speech/exception/RecognizeException;

.field private mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/velvet/presenter/Action;

    const/16 v6, 0x40

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/Action;-><init>(Ljava/util/List;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/goggles/ResultSet;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Lcom/google/android/speech/exception/RecognizeException;I[Landroid/os/Parcelable;)V

    sput-object v0, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    new-instance v0, Lcom/google/android/velvet/presenter/Action$1;

    invoke-direct {v0}, Lcom/google/android/velvet/presenter/Action$1;-><init>()V

    sput-object v0, Lcom/google/android/velvet/presenter/Action;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/Action;-><init>(Ljava/util/List;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/goggles/ResultSet;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Lcom/google/android/speech/exception/RecognizeException;I[Landroid/os/Parcelable;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/goggles/ResultSet;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Lcom/google/android/speech/exception/RecognizeException;I[Landroid/os/Parcelable;)V
    .locals 2
    .param p2    # Lcom/google/android/speech/embedded/TaggerResult;
    .param p3    # Lcom/google/android/goggles/ResultSet;
    .param p4    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;
    .param p5    # Lcom/google/android/speech/exception/RecognizeException;
    .param p6    # I
    .param p7    # [Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ActionServerResult;",
            ">;",
            "Lcom/google/android/speech/embedded/TaggerResult;",
            "Lcom/google/android/goggles/ResultSet;",
            "Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            "I[",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/Action;->mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/Action;->mRecognizeException:Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    :goto_0
    iput p6, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    iput-object p7, p0, Lcom/google/android/velvet/presenter/Action;->mControllerBundles:[Landroid/os/Parcelable;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    goto :goto_0
.end method

.method constructor <init>(Ljava/util/List;[Landroid/os/Parcelable;)V
    .locals 8
    .param p2    # [Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ActionServerResult;",
            ">;[",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/Action;-><init>(Ljava/util/List;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/goggles/ResultSet;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Lcom/google/android/speech/exception/RecognizeException;I[Landroid/os/Parcelable;)V

    return-void
.end method

.method static synthetic access$000(Landroid/os/Parcel;)Lcom/google/android/velvet/presenter/Action;
    .locals 1
    .param p0    # Landroid/os/Parcel;

    invoke-static {p0}, Lcom/google/android/velvet/presenter/Action;->fromParcel(Landroid/os/Parcel;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    return-object v0
.end method

.method private canBeHandled()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->getFirstActionServerResult()Lcom/google/android/velvet/ActionServerResult;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mRecognizeException:Lcom/google/android/speech/exception/RecognizeException;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkControllerReadiness()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->isHandlingDone()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->isReady()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    iget v3, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->notifyChanged()V

    :cond_2
    return-void
.end method

.method public static final debugFlagsToString(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    and-int/lit16 v1, p0, 0x100

    if-eqz v1, :cond_0

    const-string v1, "accept"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    and-int/lit16 v1, p0, 0x100

    if-eqz v1, :cond_1

    const-string v1, "accept-timer"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    and-int/lit16 v1, p0, 0x400

    if-eqz v1, :cond_2

    const-string v1, "bailout"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    and-int/lit16 v1, p0, 0x800

    if-eqz v1, :cond_3

    const-string v1, "reject-swipe"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    and-int/lit16 v1, p0, 0x1000

    if-eqz v1, :cond_4

    const-string v1, "reject-timer"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    and-int/lit16 v1, p0, 0x2000

    if-eqz v1, :cond_5

    const-string v1, "reject-back"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    and-int/lit16 v1, p0, 0x4000

    if-eqz v1, :cond_6

    const-string v1, "reject-scroll"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    const v1, 0x8000

    and-int/2addr v1, p0

    if-eqz v1, :cond_7

    const-string v1, "card-above-srp"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_8

    const-string v1, "handling"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    and-int/lit8 v1, p0, 0x4

    if-eqz v1, :cond_9

    const-string v1, "ready"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    and-int/lit8 v1, p0, 0x8

    if-eqz v1, :cond_a

    const-string v1, "countdown"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static fromActionServerResults(Ljava/util/List;)Lcom/google/android/velvet/presenter/Action;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ActionServerResult;",
            ">;)",
            "Lcom/google/android/velvet/presenter/Action;"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/velvet/presenter/Action;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/Action;-><init>(Ljava/util/List;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/goggles/ResultSet;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Lcom/google/android/speech/exception/RecognizeException;I[Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public static fromEarsResponse(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)Lcom/google/android/velvet/presenter/Action;
    .locals 8
    .param p0    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/velvet/presenter/Action;

    const/4 v6, 0x0

    move-object v2, v1

    move-object v3, v1

    move-object v4, p0

    move-object v5, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/Action;-><init>(Ljava/util/List;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/goggles/ResultSet;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Lcom/google/android/speech/exception/RecognizeException;I[Landroid/os/Parcelable;)V

    return-object v0
.end method

.method public static fromGogglesResultSet(Lcom/google/android/goggles/ResultSet;)Lcom/google/android/velvet/presenter/Action;
    .locals 8
    .param p0    # Lcom/google/android/goggles/ResultSet;

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/velvet/presenter/Action;

    const/4 v6, 0x0

    move-object v2, v1

    move-object v3, p0

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/Action;-><init>(Ljava/util/List;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/goggles/ResultSet;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Lcom/google/android/speech/exception/RecognizeException;I[Landroid/os/Parcelable;)V

    return-object v0
.end method

.method private static fromParcel(Landroid/os/Parcel;)Lcom/google/android/velvet/presenter/Action;
    .locals 10

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-lez v4, :cond_3

    invoke-static {v4}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    :try_start_0
    new-instance v5, Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {v5}, Lcom/google/majel/proto/PeanutProtos$Peanut;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v6

    array-length v2, v6

    if-lez v2, :cond_2

    new-instance v2, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    invoke-direct {v2}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;-><init>()V

    invoke-virtual {v2, v6}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    :goto_1
    new-instance v6, Lcom/google/android/velvet/ActionServerResult;

    invoke-direct {v6, v5, v2}, Lcom/google/android/velvet/ActionServerResult;-><init>(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "Velvet.Action"

    const-string v4, "Error restoring action server protos"

    invoke-static {v3, v4, v2}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_2
    const-class v2, Lcom/google/android/speech/embedded/TaggerResult;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/speech/embedded/TaggerResult;

    const-class v3, Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/goggles/ResultSet;

    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v8

    invoke-virtual {p0}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/google/android/speech/exception/RecognizeException;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    and-int/lit8 v6, v4, -0x7

    const-class v4, Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v7

    array-length v4, v8

    if-eqz v4, :cond_1

    new-instance v4, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    invoke-direct {v4}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;-><init>()V

    :try_start_1
    invoke-virtual {v4, v8}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_1
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    new-instance v0, Lcom/google/android/velvet/presenter/Action;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/Action;-><init>(Ljava/util/List;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/goggles/ResultSet;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Lcom/google/android/speech/exception/RecognizeException;I[Landroid/os/Parcelable;)V

    return-object v0

    :catch_1
    move-exception v0

    const-string v8, "Velvet.Action"

    const-string v9, "Error restoring ears proto"

    invoke-static {v8, v9, v0}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_1
    move-object v4, v0

    goto :goto_3

    :cond_2
    move-object v2, v0

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_2
.end method

.method public static fromPumpkinTaggerResult(Lcom/google/android/speech/embedded/TaggerResult;)Lcom/google/android/velvet/presenter/Action;
    .locals 8
    .param p0    # Lcom/google/android/speech/embedded/TaggerResult;

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/velvet/presenter/Action;

    const/16 v6, 0x40

    move-object v2, p0

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/Action;-><init>(Ljava/util/List;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/goggles/ResultSet;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Lcom/google/android/speech/exception/RecognizeException;I[Landroid/os/Parcelable;)V

    return-object v0
.end method

.method private getFirstActionServerResult()Lcom/google/android/velvet/ActionServerResult;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ActionServerResult;

    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private haveMultipleGogglesResults()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v1}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addCardController(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public canPlayTts()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelCardCountDown()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->haveCards()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->cancelCountDown()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public cancelCardCountDownByUser()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->haveCards()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->cancelCountDownByUser()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getActionParserLog()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v0, :cond_0

    const/16 v0, 0x5d

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x5e

    goto :goto_0
.end method

.method public getAndSetCountdownStarted()Z
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getCardControllers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    return-object v0
.end method

.method public getCardMetadata()Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->getFirstActionServerResult()Lcom/google/android/velvet/ActionServerResult;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/google/android/velvet/ActionServerResult;->mMetadata:Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    goto :goto_0
.end method

.method public getCountdownDurationMs()J
    .locals 4

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->getFirstActionServerResult()Lcom/google/android/velvet/ActionServerResult;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2Count()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, v1, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2(I)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasSuggestedDelayMs()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getSuggestedDelayMs()I

    move-result v2

    int-to-long v2, v2

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getEarsResponse()Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    return-object v0
.end method

.method public getEffectOnWebResults()Lcom/google/android/voicesearch/EffectOnWebResults;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->getPeanut()Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/voicesearch/EffectOnWebResults;->getPeanutEffect(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    invoke-static {v1}, Lcom/google/android/voicesearch/EffectOnWebResults;->getPumpkinEffect(Lcom/google/android/speech/embedded/TaggerResult;)Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/voicesearch/EffectOnWebResults;->NO_WEB_RESULTS:Lcom/google/android/voicesearch/EffectOnWebResults;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/voicesearch/EffectOnWebResults;->NO_EFFECT:Lcom/google/android/voicesearch/EffectOnWebResults;

    goto :goto_0
.end method

.method public getGogglesResults()Lcom/google/android/goggles/ResultSet;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    return-object v0
.end method

.method public getModifiedCommit(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/Query;
    .locals 4
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/Corpora;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getSearchQuery()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getSearchCorpus()Ljava/lang/String;

    move-result-object v0

    const-string v2, "web"

    invoke-virtual {p2, v2, v0}, Lcom/google/android/velvet/Corpora;->getCorpus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/Corpus;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/velvet/Query;->withGogglesTextAndCorpus(Ljava/lang/String;Lcom/google/android/velvet/Corpus;)Lcom/google/android/velvet/Query;

    move-result-object p1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    const-string v2, ""

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/android/velvet/Query;->withGogglesTextAndCorpus(Ljava/lang/String;Lcom/google/android/velvet/Corpus;)Lcom/google/android/velvet/Query;

    move-result-object p1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mModifiedQuery:Lcom/google/android/velvet/Query;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mModifiedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v2, p2}, Lcom/google/android/velvet/Query;->withUpdatedCorpora(Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/Query;

    move-result-object p1

    goto :goto_0
.end method

.method public getModifiedQuery(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    invoke-virtual {v0}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getResultList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/ears/EarsResultParser;->getQueryForSearch(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/Query;->withSecondarySearchQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public getNumCardsToShow()I
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->shouldShowCard()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public getPeanut()Lcom/google/majel/proto/PeanutProtos$Peanut;
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getPeanut()Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->getFirstActionServerResult()Lcom/google/android/velvet/ActionServerResult;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    goto :goto_0
.end method

.method public getPumpkinTaggerResult()Lcom/google/android/speech/embedded/TaggerResult;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    return-object v0
.end method

.method public getRecognizeException()Lcom/google/android/speech/exception/RecognizeException;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mRecognizeException:Lcom/google/android/speech/exception/RecognizeException;

    return-object v0
.end method

.method public hasSimilarImageResults()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->hasPeanut()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getPeanut()Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponseCount()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public haveCards()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->getNumCardsToShow()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCardActionComplete()Z
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->haveMultipleGogglesResults()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->canBeHandled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isHandlingDone()Z
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReady()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->canBeHandled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needDisambiguation()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->haveMultipleGogglesResults()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCardActionComplete(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->notifyChanged()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Velvet.Action"

    const-string v1, "onCardActionComplete from unrecognized or un-ready controller"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCardControllerReady(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->checkControllerReadiness()V

    return-void
.end method

.method public onHandlingDone()V
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->checkControllerReadiness()V

    return-void
.end method

.method public reambiguate()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->haveMultipleGogglesResults()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->notifyChanged()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeCardController(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->notifyChanged()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->checkControllerReadiness()V

    goto :goto_0
.end method

.method public restoreControllers(Lcom/google/android/voicesearch/fragments/ControllerFactory;)V
    .locals 8
    .param p1    # Lcom/google/android/voicesearch/fragments/ControllerFactory;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/Action;->mControllerBundles:[Landroid/os/Parcelable;

    if-eqz v7, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mControllerBundles:[Landroid/os/Parcelable;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    move-object v1, v5

    check-cast v1, Landroid/os/Bundle;

    const-string v7, "velvet:action:controller_state"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v7, "velvet:action:controller_class"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->getController(Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->restoreInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->restoreStart()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/velvet/presenter/Action;->mControllerBundles:[Landroid/os/Parcelable;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->onHandlingDone()V

    :cond_1
    return-void
.end method

.method public selectGogglesResult(Lcom/google/bionics/goggles/api2/GogglesProtos$Result;)V
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    iput-object p1, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->notifyChanged()V

    return-void
.end method

.method public setGwsLoggableEvent(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    if-eqz v0, :cond_0

    and-int/lit16 v0, p1, 0x7f00

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    and-int/lit16 v0, p1, -0x7f01

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/2addr v0, p1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->notifyChanged()V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public setModifiedQuery(Lcom/google/android/velvet/Query;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/Action;->mModifiedQuery:Lcom/google/android/velvet/Query;

    return-void
.end method

.method public setPumpkinLoggableEvent(I)V
    .locals 1
    .param p1    # I

    const v0, 0x8000

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    :cond_0
    return-void
.end method

.method public shouldAutoExecute()Z
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->getFirstActionServerResult()Lcom/google/android/velvet/ActionServerResult;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2Count()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, v0, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {v2, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2(I)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getExecute()Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public suppressGwsLoggableEvent(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    if-eqz v0, :cond_0

    and-int/lit16 v0, p1, 0x7f00

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    and-int/lit16 v0, p1, -0x7f01

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit16 v1, p1, 0x7f00

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public takeGwsUnloggedEvents()I
    .locals 4

    iget v2, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    ushr-int/lit8 v2, v2, 0x10

    and-int/lit16 v0, v2, 0x7f00

    iget v2, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit16 v2, v2, 0x7f00

    xor-int/lit8 v3, v0, -0x1

    and-int v1, v2, v3

    iget v2, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    iget v3, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit16 v3, v3, 0x7f00

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    return v1
.end method

.method public takePumpkinUnloggedEvents()I
    .locals 5

    const v4, 0x8000

    iget v2, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    ushr-int/lit8 v2, v2, 0x10

    and-int v0, v2, v4

    iget v2, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/2addr v2, v4

    xor-int/lit8 v3, v0, -0x1

    and-int v1, v2, v3

    iget v2, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    iget v3, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/2addr v3, v4

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    return v1
.end method

.method public takeStartHandling()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/Action;->canBeHandled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public takeSuppressCorpora()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->getEffectOnWebResults()Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/EffectOnWebResults;->shouldSuppressWebResults()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Action["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Identity="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    if-eqz v3, :cond_0

    const-string v3, "ACTIONSERVER("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v3, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v3, :cond_1

    const-string v3, "PUMPKIN("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    invoke-virtual {v4}, Lcom/google/android/speech/embedded/TaggerResult;->getActionName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v3, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    if-eqz v3, :cond_2

    const-string v3, "GOGGLES("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {v4}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/Action;->mSelectedGogglesResult:Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    if-eqz v3, :cond_2

    const-string v3, "(SELECTED)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v3, p0, Lcom/google/android/velvet/presenter/Action;->mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    if-eqz v3, :cond_3

    const-string v3, "EARS"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v3, " FX="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/Action;->getEffectOnWebResults()Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " Flags"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    invoke-static {v4}, Lcom/google/android/velvet/presenter/Action;->debugFlagsToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " CARDS["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    const-string v3, "]]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 12
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v11, 0x0

    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {p1, v7}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v7, :cond_2

    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mActionServerResults:Ljava/util/List;

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ActionServerResult;

    iget-object v9, v0, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {v9}, Lcom/google/majel/proto/PeanutProtos$Peanut;->toByteArray()[B

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->writeByteArray([B)V

    iget-object v9, v0, Lcom/google/android/velvet/ActionServerResult;->mMetadata:Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    if-eqz v9, :cond_0

    iget-object v9, v0, Lcom/google/android/velvet/ActionServerResult;->mMetadata:Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    invoke-virtual {v9}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;->toByteArray()[B

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->writeByteArray([B)V

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    new-array v9, v11, [B

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v11}, Landroid/os/Parcel;->writeInt(I)V

    :cond_2
    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mPumpkinTaggerResult:Lcom/google/android/speech/embedded/TaggerResult;

    invoke-virtual {p1, v9, v11}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mGogglesResultSet:Lcom/google/android/goggles/ResultSet;

    invoke-virtual {p1, v9, v11}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mEarsResponse:Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    invoke-virtual {v9}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->toByteArray()[B

    move-result-object v9

    :goto_2
    invoke-virtual {p1, v9}, Landroid/os/Parcel;->writeByteArray([B)V

    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mRecognizeException:Lcom/google/android/speech/exception/RecognizeException;

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget v9, p0, Lcom/google/android/velvet/presenter/Action;->mFlags:I

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    new-array v3, v9, [Landroid/os/Bundle;

    const/4 v4, 0x0

    iget-object v9, p0, Lcom/google/android/velvet/presenter/Action;->mCardControllers:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getInstanceState()Landroid/os/Bundle;

    move-result-object v8

    if-eqz v8, :cond_3

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v9, "velvet:action:controller_class"

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "velvet:action:controller_state"

    invoke-virtual {v1, v9, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    add-int/lit8 v5, v4, 0x1

    aput-object v1, v3, v4

    move v4, v5

    goto :goto_3

    :cond_4
    new-array v9, v11, [B

    goto :goto_2

    :cond_5
    array-length v9, v3

    if-ge v4, v9, :cond_6

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/os/Bundle;

    :cond_6
    invoke-virtual {p1, v3, v11}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    return-void
.end method
