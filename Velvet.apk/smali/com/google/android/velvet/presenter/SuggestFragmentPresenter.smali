.class public Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
.super Lcom/google/android/velvet/presenter/TgPresenter;
.source "SuggestFragmentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$1;,
        Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;,
        Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;
    }
.end annotation


# instance fields
.field private mNoResultsView:Landroid/view/View;

.field private final mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

.field private mShowPredictive:Z

.field private mShowSampleCards:Z

.field private mShowSuggest:Z

.field private mShowSummons:Z

.field private mSuggestionsInitPosted:Z

.field private mSummonsPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

.field private mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

.field private mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

.field private mWebSuggestPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

.field private mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/google/UserInteractionLogger;Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/velvet/presenter/ViewActionRecorder;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 10
    .param p1    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p2    # Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .param p3    # Lcom/google/android/searchcommon/google/UserInteractionLogger;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .param p6    # Lcom/google/android/velvet/presenter/ViewActionRecorder;
    .param p7    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
    .param p8    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p9    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            "Lcom/google/android/searchcommon/google/SearchBoxLogging;",
            "Lcom/google/android/searchcommon/google/UserInteractionLogger;",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;",
            "Lcom/google/android/searchcommon/util/Clock;",
            "Lcom/google/android/velvet/presenter/ViewActionRecorder;",
            "Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;",
            "Lcom/google/android/apps/sidekick/inject/NetworkClient;",
            "Lcom/google/android/apps/sidekick/inject/ActivityHelper;",
            ")V"
        }
    .end annotation

    const-string v1, "suggest"

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/velvet/presenter/TgPresenter;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/google/UserInteractionLogger;Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/velvet/presenter/ViewActionRecorder;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    iput-object p2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSuggestionsInitPosted:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSummons:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowPredictive:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSuggest:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/velvet/ui/widget/SuggestionListView;)Lcom/google/android/velvet/ui/widget/SuggestionListView;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    .param p1    # Lcom/google/android/velvet/ui/widget/SuggestionListView;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/searchcommon/suggest/CachingPromoter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    .param p1    # Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/velvet/ui/widget/SuggestionListView;)Lcom/google/android/velvet/ui/widget/SuggestionListView;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    .param p1    # Lcom/google/android/velvet/ui/widget/SuggestionListView;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/searchcommon/suggest/CachingPromoter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    .param p1    # Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    return-object v0
.end method

.method private prepareSuggestionsUi()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSuggestionsInitPosted:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->resetChildDismissState(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSuggestionsInitPosted:Z

    new-instance v0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;-><init>(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$1;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    goto :goto_0
.end method

.method private updateSuggestionTypesToShow()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSuggest:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSummons:Z

    if-eqz v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->prepareSuggestionsUi()V

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowPredictive:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->setPredictiveMode(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->WEB_SUGGESTIONS:Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSuggest:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->setSuggestionsViewEnabled(Ljava/lang/Object;Z)V

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->SUMMONS:Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSummons:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->setSuggestionsViewEnabled(Ljava/lang/Object;Z)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    # invokes: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->maybePost()V
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->access$100(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;)V

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowPredictive:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSampleCards:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->setPredictiveMode(I)V

    :cond_3
    return-void

    :cond_4
    const/4 v1, 0x2

    goto :goto_0
.end method


# virtual methods
.method protected isNonPredictiveCardView(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->isNonPredictiveCardView(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPostAttach(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->onPostAttach(Landroid/os/Bundle;)V

    new-instance v1, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;-><init>(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$1;)V

    iput-object v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->addListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->updateSuggestionTypesToShow()V

    return-void
.end method

.method protected onPreDetach()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->onPreDetach()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->removeListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->WEB_SUGGESTIONS:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->removeSuggestionsView(Ljava/lang/Object;)V

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->SUMMONS:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->removeSuggestionsView(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSuggestionsInitPosted:Z

    return-void
.end method

.method onPreModeSwitch(Lcom/google/android/velvet/presenter/UiModeManager;Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;Z)Z
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/UiModeManager;
    .param p2    # Lcom/google/android/velvet/presenter/UiMode;
    .param p3    # Lcom/google/android/velvet/presenter/UiMode;
    .param p4    # Z

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSuggest:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1, p3}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldSuggestFragmentShowSuggestInMode(Lcom/google/android/velvet/presenter/UiMode;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/searchcommon/suggest/SuggestionsController;->WEB_SUGGESTIONS:Ljava/lang/Object;

    invoke-interface {v0, v2, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->setSuggestionsViewEnabled(Ljava/lang/Object;Z)V

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSummons:Z

    if-eqz v2, :cond_1

    invoke-virtual {p1, p3, p4}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldSuggestFragmentShowSummonsInMode(Lcom/google/android/velvet/presenter/UiMode;Z)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/searchcommon/suggest/SuggestionsController;->SUMMONS:Ljava/lang/Object;

    invoke-interface {v0, v2, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->setSuggestionsViewEnabled(Ljava/lang/Object;Z)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    # invokes: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->maybePost()V
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->access$100(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;)V

    if-nez p4, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public onViewsDismissed(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->onViewsDismissed(Ljava/lang/Iterable;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onWebSuggestionsDismissed()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method updateMode(Lcom/google/android/velvet/presenter/UiModeManager;Lcom/google/android/velvet/presenter/UiMode;Z)V
    .locals 5
    .param p1    # Lcom/google/android/velvet/presenter/UiModeManager;
    .param p2    # Lcom/google/android/velvet/presenter/UiMode;
    .param p3    # Z

    invoke-virtual {p1, p2, p3}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldUsePredictiveInMode(Lcom/google/android/velvet/presenter/UiMode;Z)Z

    move-result v0

    invoke-virtual {p1, p2}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldSuggestFragmentShowSuggestInMode(Lcom/google/android/velvet/presenter/UiMode;)Z

    move-result v2

    invoke-virtual {p1, p2, p3}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldSuggestFragmentShowSummonsInMode(Lcom/google/android/velvet/presenter/UiMode;Z)Z

    move-result v3

    invoke-virtual {p1, p2}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldShowCardList(Lcom/google/android/velvet/presenter/UiMode;)Z

    move-result v1

    iget-boolean v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowPredictive:Z

    if-ne v0, v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSuggest:Z

    if-ne v2, v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSummons:Z

    if-ne v3, v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSampleCards:Z

    if-eq v1, v4, :cond_1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowPredictive:Z

    iput-boolean v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSuggest:Z

    iput-boolean v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSummons:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSampleCards:Z

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    # invokes: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->updateHaveNoResults()V
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->access$000(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->updateSuggestionTypesToShow()V

    :cond_1
    return-void
.end method
