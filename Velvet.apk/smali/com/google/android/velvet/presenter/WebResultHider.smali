.class public Lcom/google/android/velvet/presenter/WebResultHider;
.super Ljava/lang/Object;
.source "WebResultHider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/WebResultHider$WebResultHiderClient;,
        Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;
    }
.end annotation


# instance fields
.field private final mClient:Lcom/google/android/velvet/presenter/WebResultHider$WebResultHiderClient;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mDelayedSuccessTask:Ljava/lang/Runnable;

.field private final mJsCallbackWatchdogTask:Ljava/lang/Runnable;

.field private final mJsHelper:Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;

.field private mLastId:I

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private mWaitForCallbackId:I

.field private final mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Landroid/webkit/WebView;Lcom/google/android/velvet/presenter/WebResultHider$WebResultHiderClient;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p3    # Landroid/webkit/WebView;
    .param p4    # Lcom/google/android/velvet/presenter/WebResultHider$WebResultHiderClient;

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I

    iput v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mLastId:I

    new-instance v0, Lcom/google/android/velvet/presenter/WebResultHider$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/WebResultHider$1;-><init>(Lcom/google/android/velvet/presenter/WebResultHider;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mDelayedSuccessTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/presenter/WebResultHider$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/WebResultHider$2;-><init>(Lcom/google/android/velvet/presenter/WebResultHider;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mJsCallbackWatchdogTask:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWebView:Landroid/webkit/WebView;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mClient:Lcom/google/android/velvet/presenter/WebResultHider$WebResultHiderClient;

    new-instance v0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWebView:Landroid/webkit/WebView;

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;-><init>(Lcom/google/android/velvet/presenter/WebResultHider;Landroid/webkit/WebView;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mJsHelper:Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/WebResultHider;Z)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/WebResultHider;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/WebResultHider;->complete(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/WebResultHider;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/WebResultHider;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/WebResultHider;->onJsCallbackTimeout()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/velvet/presenter/WebResultHider;IZI)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/WebResultHider;
    .param p1    # I
    .param p2    # Z
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/presenter/WebResultHider;->onJsCallbackInternal(IZI)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/velvet/presenter/WebResultHider;IZI)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/WebResultHider;
    .param p1    # I
    .param p2    # Z
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/presenter/WebResultHider;->onJsCallback(IZI)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/velvet/presenter/WebResultHider;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/WebResultHider;

    iget v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I

    return v0
.end method

.method private complete(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mClient:Lcom/google/android/velvet/presenter/WebResultHider$WebResultHiderClient;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWebView:Landroid/webkit/WebView;

    invoke-interface {v0, v1, p1}, Lcom/google/android/velvet/presenter/WebResultHider$WebResultHiderClient;->onHidingComplete(Landroid/webkit/WebView;Z)V

    return-void
.end method

.method private onJsCallback(IZI)V
    .locals 2
    .param p1    # I
    .param p2    # Z
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v1, Lcom/google/android/velvet/presenter/WebResultHider$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/velvet/presenter/WebResultHider$3;-><init>(Lcom/google/android/velvet/presenter/WebResultHider;IZI)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private onJsCallbackInternal(IZI)V
    .locals 4
    .param p1    # I
    .param p2    # Z
    .param p3    # I

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mJsCallbackWatchdogTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I

    if-eqz p2, :cond_3

    if-lez p3, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mDelayedSuccessTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mDelayedSuccessTask:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getResultHidingDelay()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p3, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/WebResultHider;->complete(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/velvet/presenter/WebResultHider;->complete(Z)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v2}, Lcom/google/android/velvet/presenter/WebResultHider;->complete(Z)V

    goto :goto_0
.end method

.method private onJsCallbackTimeout()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/WebResultHider;->complete(Z)V

    return-void
.end method

.method private startJsCallbackWatchdog()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mJsCallbackWatchdogTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mJsCallbackWatchdogTask:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getResultHidingJsTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private tag()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Velvet.WebResultHider["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] ID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public hideResult(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mDelayedSuccessTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mJsCallbackWatchdogTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mLastId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mLastId:I

    iput v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mJsHelper:Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/velvet/presenter/WebResultHider;->mWaitForCallbackId:I

    # invokes: Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->googleAgsaHideAll(Ljava/lang/String;ZI)V
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->access$200(Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;Ljava/lang/String;ZI)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/WebResultHider;->startJsCallbackWatchdog()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/WebResultHider;->tag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
