.class Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;
.super Lcom/google/android/velvet/presenter/QueryState$LoadState;
.source "QueryState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/QueryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WebviewLoadState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/velvet/presenter/QueryState$LoadState",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mCorporaSuppressed:Z

.field private mPendingCorporaShow:Z

.field private mPintoModuleLoaded:Z

.field private mReadyToShow:Z

.field private mResultHidingState:I


# direct methods
.method constructor <init>()V
    .locals 1

    const-string v0, "webview"

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;I)I
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPintoModuleLoaded:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPintoModuleLoaded:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mReadyToShow:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mReadyToShow:Z

    return p1
.end method


# virtual methods
.method isWebResultHidden()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method loadStarted()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->setState(I)V

    return-void
.end method

.method reset()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->reset()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I

    return-void
.end method

.method resultHidingComplete()V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I

    return-void
.end method

.method protected setState(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x2

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    if-eq p1, v3, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->setState(I)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPintoModuleLoaded:Z

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->shouldShowCards()Z

    move-result v0

    if-eqz v0, :cond_2

    iput v2, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I

    goto :goto_0

    :cond_2
    iput v3, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/google/android/velvet/presenter/QueryState$LoadState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":H="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mResultHidingState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPintoModuleLoaded:Z

    if-eqz v0, :cond_0

    const-string v0, ", pinto module loaded"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mReadyToShow:Z

    if-eqz v0, :cond_1

    const-string v0, ", ready to show"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mCorporaSuppressed:Z

    if-eqz v0, :cond_2

    const-string v0, ", corpora suppressed"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/QueryState$WebviewLoadState;->mPendingCorporaShow:Z

    if-eqz v0, :cond_3

    const-string v0, ", pending corpora show"

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_2

    :cond_3
    const-string v0, ""

    goto :goto_3
.end method
