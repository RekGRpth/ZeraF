.class Lcom/google/android/velvet/presenter/TgPresenter$3;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "TgPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/TgPresenter;->postRemovePredictiveCards()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/TgPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter$3;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-direct {p0, p2}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildCount()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    :goto_0
    if-ltz v2, :cond_1

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/TgPresenter$3;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v3, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->isNonPredictiveCardView(Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method
