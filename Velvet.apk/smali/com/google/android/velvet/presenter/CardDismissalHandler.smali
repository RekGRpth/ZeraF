.class public Lcom/google/android/velvet/presenter/CardDismissalHandler;
.super Ljava/lang/Object;
.source "CardDismissalHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/CardDismissalHandler$1;,
        Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;,
        Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;
    }
.end annotation


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mPrefs:Landroid/content/SharedPreferences;

.field private final mPresenter:Lcom/google/android/velvet/presenter/TgPresenter;

.field private final mShowedDismissTrailForEntryTypes:Landroid/util/SparseBooleanArray;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPresenter:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mClock:Lcom/google/android/searchcommon/util/Clock;

    new-instance v2, Landroid/util/SparseBooleanArray;

    invoke-direct {v2}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v2, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mShowedDismissTrailForEntryTypes:Landroid/util/SparseBooleanArray;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/CardDismissalHandler;)Lcom/google/android/velvet/presenter/TgPresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/CardDismissalHandler;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPresenter:Lcom/google/android/velvet/presenter/TgPresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/CardDismissalHandler;)Landroid/util/SparseBooleanArray;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/CardDismissalHandler;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mShowedDismissTrailForEntryTypes:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/velvet/presenter/CardDismissalHandler;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/CardDismissalHandler;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->getDismissedEntryTypes()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private getDismissedEntryTypes()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "dismissed_entry_types"

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->of()Lcom/google/common/collect/ImmutableSet;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDismissTrailFactory(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Lcom/google/android/velvet/tg/DismissTrailFactory;
    .locals 3
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPresenter:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;

    invoke-direct {v1, p0, p1, v2}, Lcom/google/android/velvet/presenter/CardDismissalHandler$TgDismissTrailFactory;-><init>(Lcom/google/android/velvet/presenter/CardDismissalHandler;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/velvet/presenter/CardDismissalHandler$1;)V

    :goto_0
    return-object v1

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method

.method public recordDismissedEntryType(I)V
    .locals 5
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->getDismissedEntryTypes()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v2

    :goto_0
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "dismissed_entry_types"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void

    :cond_1
    move-object v2, v0

    goto :goto_0
.end method

.method public recordDismissedFirstUseCard(Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;->getDismissedPrefKey()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public shouldShowFirstUseCard(Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;)Z
    .locals 10
    .param p1    # Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;

    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;->getDismissedPrefKey()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_0
    return v5

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;->getFirstViewPrefKey()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v6, v2, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iget-object v6, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v6, v0, v8

    if-nez v6, :cond_1

    move-wide v0, v3

    iget-object v6, p0, Lcom/google/android/velvet/presenter/CardDismissalHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6, v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    sub-long v6, v3, v0

    const-wide/32 v8, 0x5265c00

    cmp-long v6, v6, v8

    if-lez v6, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->recordDismissedFirstUseCard(Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;)V

    goto :goto_0

    :cond_2
    const/4 v5, 0x1

    goto :goto_0
.end method
