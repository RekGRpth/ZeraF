.class public Lcom/google/android/velvet/presenter/SearchController;
.super Lcom/google/android/velvet/ui/RetainedFragmentPresenter;
.source "SearchController.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/searchcommon/google/LocationSettings$Observer;
.implements Lcom/google/android/searchcommon/util/ForceableLock$Owner;
.implements Lcom/google/android/velvet/presenter/QueryState$Observer;
.implements Lcom/google/android/velvet/presenter/WebResultHider$WebResultHiderClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;,
        Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;,
        Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;,
        Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;,
        Lcom/google/android/velvet/presenter/SearchController$MyTextListener;,
        Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;,
        Lcom/google/android/velvet/presenter/SearchController$MyAdClickHandlerClient;,
        Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;
    }
.end annotation


# static fields
.field private static final PREFERENCE_KEYS:Lcom/google/common/collect/ImmutableSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAdClickHandler:Lcom/google/android/velvet/presenter/AdClickHandler;

.field private mCardController:Lcom/google/android/voicesearch/VelvetCardController;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;

.field private final mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

.field private final mCorpora:Lcom/google/android/velvet/Corpora;

.field private mCurrentQuery:Lcom/google/android/velvet/Query;

.field private mDestroyed:Z

.field private final mFactory:Lcom/google/android/velvet/VelvetFactory;

.field private mGogglesCallback:Lcom/google/android/goggles/GogglesController$Callback;

.field private final mGoogleCookiesLockObserver:Landroid/database/DataSetObserver;

.field private mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

.field private mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

.field private mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

.field private mInitialized:Z

.field private mLocale:Ljava/lang/String;

.field private final mPackageName:Ljava/lang/String;

.field private mPumpkinAvailable:Z

.field private mPumpkinCallback:Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;

.field private mPumpkinTagger:Lcom/google/android/speech/embedded/PumpkinTagger;

.field private final mQueryState:Lcom/google/android/velvet/presenter/QueryState;

.field private mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

.field private mResultHider:Lcom/google/android/velvet/presenter/WebResultHider;

.field private mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

.field private mSearchResultPageFetcher:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mSrpCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

.field private final mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

.field private mTextRecognizer:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

.field private mWaitingForCookieRefresh:Z

.field private mWebView:Landroid/webkit/WebView;

.field private mWebViewActive:Z

.field private mWebViewBound:Z

.field private final mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

.field private final mWebViewControllerClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

.field private final mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const-string v0, "use_google_com"

    const-string v1, "search_domain"

    const-string v2, "debug_search_scheme_override"

    const-string v3, "debug_search_domain_override"

    const-string v4, "search_domain_scheme"

    const-string v5, "search_domain_country_code"

    const/16 v6, 0x9

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "google_account"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "signed_out"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "personalized_search_bool"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "personalized_search"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "safe_search_settings"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "web_corpora_config"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "gservices_overrides"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string v8, "webview_logged_in_account"

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "webview_logged_in_domain"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/velvet/presenter/SearchController;->PREFERENCE_KEYS:Lcom/google/common/collect/ImmutableSet;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/lang/String;Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/CoreSearchServices;
    .param p3    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p4    # Lcom/google/android/velvet/VelvetFactory;
    .param p5    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mPumpkinAvailable:Z

    new-instance v0, Lcom/google/android/velvet/presenter/SearchController$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/SearchController$1;-><init>(Lcom/google/android/velvet/presenter/SearchController;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mGoogleCookiesLockObserver:Landroid/database/DataSetObserver;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/SearchController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/SearchController;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/SearchController;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getCorpora()Lcom/google/android/velvet/Corpora;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-interface {p2}, Lcom/google/android/searchcommon/CoreSearchServices;->getWebViewGoogleCookiesLock()Lcom/google/android/searchcommon/util/ForceableLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    iput-object p6, p0, Lcom/google/android/velvet/presenter/SearchController;->mPackageName:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/velvet/presenter/SearchController;->mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    new-instance v0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/prefetch/SearchResultPageCache;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSrpCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    new-instance v0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;

    invoke-direct {v0, p0, v3}, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;-><init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewControllerClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    invoke-virtual {p4}, Lcom/google/android/velvet/VelvetFactory;->createQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewControllerClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {p4, v0, v1}, Lcom/google/android/velvet/VelvetFactory;->createWebViewController(Lcom/google/android/velvet/webview/WebViewControllerClient;Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/velvet/webview/GsaWebViewController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    new-instance v0, Lcom/google/android/velvet/presenter/SearchController$MyAdClickHandlerClient;

    invoke-direct {v0, p0, v3}, Lcom/google/android/velvet/presenter/SearchController$MyAdClickHandlerClient;-><init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V

    invoke-virtual {p4, v0}, Lcom/google/android/velvet/VelvetFactory;->createAdClickHandler(Lcom/google/android/velvet/presenter/AdClickHandler$Client;)Lcom/google/android/velvet/presenter/AdClickHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mAdClickHandler:Lcom/google/android/velvet/presenter/AdClickHandler;

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/SearchController;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->checkWebViewAccessAllowed()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/AdClickHandler;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mAdClickHandler:Lcom/google/android/velvet/presenter/AdClickHandler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/CoreSearchServices;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/searchcommon/util/UriRequest;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SearchController;->openUrlInBrowser(Lcom/google/android/searchcommon/util/UriRequest;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->getSearchResultPageFetcher()Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/SearchConfig;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/SearchSettings;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/velvet/presenter/SearchController;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->forceRefreshCookies()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/prefetch/SearchResultPageCache;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSrpCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/voicesearch/VoiceSearchServices;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/SearchPlatePresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/velvet/presenter/SearchController;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewActive:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/velvet/presenter/SearchController;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewActive:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    return-object v0
.end method

.method private checkWebViewAccessAllowed()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mInitialized:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->haveBadCookies()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->setWebViewAccessAllowed(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-virtual {v0, p0}, Lcom/google/android/searchcommon/util/ForceableLock;->release(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    iput-boolean v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mWaitingForCookieRefresh:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v0

    const-string v1, "refresh_search_domain_and_cookies"

    invoke-interface {v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasks;->forceRunInterruptingOngoing(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-virtual {v0, p0}, Lcom/google/android/searchcommon/util/ForceableLock;->tryObtain(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWaitingForCookieRefresh:Z

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mWaitingForCookieRefresh:Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->maybeForceReload()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/webview/GsaWebViewController;->setWebViewAccessAllowed(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->setWebViewAccessAllowed(Z)V

    goto :goto_0
.end method

.method private forceRefreshCookies()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->setWebViewAccessAllowed(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    const-wide/16 v1, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/SearchSettings;->setRefreshWebViewCookiesAt(J)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWaitingForCookieRefresh:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v0

    const-string v1, "refresh_search_domain_and_cookies"

    invoke-interface {v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasks;->forceRunInterruptingOngoing(Ljava/lang/String;)V

    return-void
.end method

.method private getSearchResultPageFetcher()Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchResultPageFetcher:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController;->mSrpCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/VelvetFactory;->createSearchResultPageFetcher(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/webview/GsaWebViewController;Lcom/google/android/velvet/prefetch/SearchResultPageCache;)Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchResultPageFetcher:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchResultPageFetcher:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->addObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchResultPageFetcher:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    return-object v0
.end method

.method private makeImageUniversalEventHandler(Lcom/google/android/velvet/gallery/ImageMetadataController;Ljava/lang/String;)Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;
    .locals 1
    .param p1    # Lcom/google/android/velvet/gallery/ImageMetadataController;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/velvet/presenter/SearchController$2;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/velvet/presenter/SearchController$2;-><init>(Lcom/google/android/velvet/presenter/SearchController;Ljava/lang/String;Lcom/google/android/velvet/gallery/ImageMetadataController;)V

    return-object v0
.end method

.method private maybeForceReload()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWaitingForCookieRefresh:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSrpCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->clear()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->forceReloadIfPossible()V

    :cond_0
    return-void
.end method

.method private maybeInitPumpkinTagger()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mPumpkinTagger:Lcom/google/android/speech/embedded/PumpkinTagger;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mLocale:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mLocale:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController;->mLocale:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/VoiceSearchServices;->createPumpkinTagger(Ljava/lang/String;)Lcom/google/android/speech/embedded/PumpkinTagger;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mPumpkinTagger:Lcom/google/android/speech/embedded/PumpkinTagger;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mPumpkinTagger:Lcom/google/android/speech/embedded/PumpkinTagger;

    if-nez v2, :cond_1

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mPumpkinAvailable:Z

    :goto_0
    return v1

    :cond_1
    new-instance v1, Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;-><init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V

    iput-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mPumpkinCallback:Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private openUrlInBrowser(Lcom/google/android/searchcommon/util/UriRequest;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->openUrlInBrowser(Lcom/google/android/searchcommon/util/UriRequest;)V

    :cond_0
    return-void
.end method

.method private unbindWebView()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewBound:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getScrollingContainer()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewBound:Z

    :cond_0
    return-void
.end method

.method private updateHotwordDetector()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mInitialized:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->shouldListenForHotword()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getHotwordDetector()Lcom/google/android/voicesearch/hotword/HotwordDetector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    new-instance v0, Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;-><init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->start(Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->stop()V

    goto :goto_0
.end method


# virtual methods
.method public clearWebViewsHistory()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-virtual {v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->clearWebViewsHistory()V

    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v3

    const-string v2, "SearchController state:"

    aput-object v2, v1, v4

    invoke-static {p2, v1}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v3

    const-string v2, "mWebViewActive: "

    aput-object v2, v1, v4

    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewActive:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p2, v1}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/velvet/presenter/QueryState;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/velvet/webview/GsaWebViewController;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/presenter/Action;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    :cond_0
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mSrpCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    return-void
.end method

.method public forceReleaseLock()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mInitialized:Z

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mDestroyed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->setWebViewAccessAllowed(Z)V

    :cond_0
    return-void
.end method

.method public getCardController()Lcom/google/android/voicesearch/VelvetCardController;
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCardController:Lcom/google/android/voicesearch/VelvetCardController;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/VelvetFactory;->createVelvetCardController(Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/voicesearch/VelvetCardController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCardController:Lcom/google/android/voicesearch/VelvetCardController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCardController:Lcom/google/android/voicesearch/VelvetCardController;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/VelvetCardController;->setPresenter(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCardController:Lcom/google/android/voicesearch/VelvetCardController;

    return-object v0
.end method

.method getGogglesCallback()Lcom/google/android/goggles/GogglesController$Callback;
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mGogglesCallback:Lcom/google/android/goggles/GogglesController$Callback;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;-><init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mGogglesCallback:Lcom/google/android/goggles/GogglesController$Callback;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mGogglesCallback:Lcom/google/android/goggles/GogglesController$Callback;

    return-object v0
.end method

.method public getQueryState()Lcom/google/android/velvet/presenter/QueryState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    return-object v0
.end method

.method getRecognizerController()Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .locals 5

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    const-class v2, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    new-instance v3, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;-><init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V

    invoke-static {v0, v2, v3}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/VelvetFactory;->createRecognizerController(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    return-object v0
.end method

.method public getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    return-object v0
.end method

.method getTextRecognizer()Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mTextRecognizer:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getTextRecognizer()Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mTextRecognizer:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mTextRecognizer:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    return-object v0
.end method

.method public getWebView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method haveBadCookies()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v2}, Lcom/google/android/searchcommon/SearchSettings;->getWebViewLoggedInAccount()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v2}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method init()V
    .locals 7

    const/4 v6, 0x0

    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mInitialized:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v2}, Lcom/google/android/velvet/Corpora;->initializeDelayed()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetFactory;->createResultsWebView()Landroid/webkit/WebView;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    const-string v3, "RESULTS"

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setTag(Ljava/lang/Object;)V

    new-instance v2, Lcom/google/android/velvet/presenter/WebResultHider;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v5, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    invoke-direct {v2, v3, v4, v5, p0}, Lcom/google/android/velvet/presenter/WebResultHider;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Landroid/webkit/WebView;Lcom/google/android/velvet/presenter/WebResultHider$WebResultHiderClient;)V

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mResultHider:Lcom/google/android/velvet/presenter/WebResultHider;

    new-instance v2, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v5, p0, Lcom/google/android/velvet/presenter/SearchController;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;-><init>(Landroid/webkit/WebView;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->areNativeImagesFromWebEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getImageMetadataController()Lcom/google/android/velvet/gallery/ImageMetadataController;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController;->mPackageName:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/google/android/velvet/presenter/SearchController;->makeImageUniversalEventHandler(Lcom/google/android/velvet/gallery/ImageMetadataController;Ljava/lang/String;)Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->registerHandler(Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    new-instance v3, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;

    invoke-direct {v3, p0, v6}, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;-><init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V

    invoke-virtual {v2, v3, v6}, Lcom/google/android/velvet/VelvetFactory;->createJavascriptExtensionsForSearchResults(Lcom/google/android/searchcommon/util/IntentStarter;Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;)Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    const-string v3, "agsa_ext"

    invoke-virtual {v2, v0, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->getSearchResultPageFetcher()Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->isNativeIgEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v2}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->addListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/UserAgentHelper;->onWebViewCreated(Landroid/webkit/WebView;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController;->mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/velvet/webview/GsaWebViewController;->setWebViewAndGsaCommunicationJsHelper(Landroid/webkit/WebView;Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mInitialized:Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->updateHotwordDetector()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->maybeBindWebView()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->initialize()V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->checkWebViewAccessAllowed()V

    return-void
.end method

.method maybeBindWebView()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mInitialized:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewBound:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getScrollingContainer()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewBound:Z

    :cond_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-virtual {v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/google/LocationSettings;->addUseLocationObserver(Lcom/google/android/searchcommon/google/LocationSettings$Observer;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/SearchSettings;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mDestroyed:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/google/LocationSettings;->removeUseLocationObserver(Lcom/google/android/searchcommon/google/LocationSettings$Observer;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/SearchSettings;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->unbindWebView()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-virtual {v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->dispose()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->stop()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchResultPageFetcher:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchResultPageFetcher:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->removeListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchResultPageFetcher:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->removeObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    :cond_2
    return-void
.end method

.method public onHidingComplete(Landroid/webkit/WebView;Z)V
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Z

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->resultHidingComplete()V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/QueryState;->removeObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->scheduleSendEvents(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/voicesearch/util/AccountHelperService;->start(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->stop()V

    :cond_1
    return-void
.end method

.method public onPostAttach(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    const/16 v0, 0x2b

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isTestPlatformLoggingEnabled()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/speech/test/TestPlatformLog;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCardController:Lcom/google/android/voicesearch/VelvetCardController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCardController:Lcom/google/android/voicesearch/VelvetCardController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/VelvetCardController;->setPresenter(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    :cond_0
    return-void
.end method

.method public onPreDetach()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->unbindWebView()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCardController:Lcom/google/android/voicesearch/VelvetCardController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCardController:Lcom/google/android/voicesearch/VelvetCardController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/VelvetCardController;->setPresenter(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    :cond_0
    return-void
.end method

.method public onQueryStateChanged()V
    .locals 11

    const/4 v10, 0x0

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v6, v7}, Lcom/google/android/velvet/presenter/QueryState;->shouldCancel(Lcom/google/android/velvet/Query;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v6}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v6}, Lcom/google/android/velvet/Query;->isSoundSearch()Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getRecognizerController()Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancel()V

    :cond_1
    :goto_0
    sget-object v6, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    iput-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    :cond_2
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->updateHotwordDetector()V

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/QueryState;->takeQueryToCommitToMajel()Lcom/google/android/velvet/Query;

    move-result-object v2

    if-eqz v2, :cond_3

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->isTextSearch()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getTextRecognizer()Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    move-result-object v6

    new-instance v7, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;

    iget-object v8, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-direct {v7, v8, v2, v10}, Lcom/google/android/velvet/presenter/SearchController$MyTextListener;-><init>(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchController$1;)V

    iget-object v8, p0, Lcom/google/android/velvet/presenter/SearchController;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-virtual {v6, v2, v7, v8}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->start(Lcom/google/android/velvet/Query;Lcom/google/android/speech/callback/Callback;Ljava/util/concurrent/Executor;)V

    :cond_3
    :goto_1
    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/QueryState;->takeQueryToCommitToPumpkin()Lcom/google/android/velvet/Query;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-boolean v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mPumpkinAvailable:Z

    if-eqz v6, :cond_f

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->maybeInitPumpkinTagger()Z

    move-result v6

    if-eqz v6, :cond_f

    iput-object v2, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mPumpkinTagger:Lcom/google/android/speech/embedded/PumpkinTagger;

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/velvet/presenter/SearchController;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v9, p0, Lcom/google/android/velvet/presenter/SearchController;->mPumpkinCallback:Lcom/google/android/velvet/presenter/SearchController$MyPumpkinListener;

    invoke-virtual {v6, v7, v8, v9}, Lcom/google/android/speech/embedded/PumpkinTagger;->tagAsync(Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/google/android/speech/callback/SimpleCallback;)V

    :cond_4
    :goto_2
    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/QueryState;->takeActionToHandle()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getCardController()Lcom/google/android/voicesearch/VelvetCardController;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v7

    invoke-virtual {v6, v7, v0}, Lcom/google/android/voicesearch/VelvetCardController;->handleAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    :cond_5
    iget-boolean v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewActive:Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/QueryState;->takeResultTypeToHide()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mResultHider:Lcom/google/android/velvet/presenter/WebResultHider;

    invoke-virtual {v6, v3}, Lcom/google/android/velvet/presenter/WebResultHider;->hideResult(Ljava/lang/String;)V

    :cond_6
    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v6}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v6

    if-eqz v6, :cond_7

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->takeGwsUnloggedEvents()I

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->getCardMetadata()Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->getCardMetadata()Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;->getLoggingUrls()Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/velvet/presenter/SearchController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v6, v1, v5, v10, v7}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventsToGws(ILcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    :cond_7
    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/QueryState;->takeUnusedActionToLog()Lcom/google/android/velvet/presenter/Action;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getCardController()Lcom/google/android/voicesearch/VelvetCardController;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v7

    invoke-virtual {v6, v7, v4}, Lcom/google/android/voicesearch/VelvetCardController;->logUnusedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    :cond_8
    return-void

    :cond_9
    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v6}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->isAttached()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getGogglesController()Lcom/google/android/goggles/GogglesController;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/goggles/GogglesController;->cancelRecognition()V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->isRestoredState()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->shouldResendLastRecording()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getRecognizerController()Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->resendVoiceSearch()V

    goto/16 :goto_1

    :cond_b
    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->getCannedAudio()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_c

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAudioController()Lcom/google/android/speech/audio/AudioController;

    move-result-object v6

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->getCannedAudio()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/speech/audio/AudioController;->setCannedAudio(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getRecognizerController()Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startVoiceSearch()V

    goto/16 :goto_1

    :cond_d
    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->isSoundSearch()Z

    move-result v6

    if-eqz v6, :cond_e

    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->showSoundSearchListening()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getRecognizerController()Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v6

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->shouldPlayTts()Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startSoundSearch(Z)V

    goto/16 :goto_1

    :cond_e
    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "Velvet.SearchController"

    const-string v7, "Unregonize query type from takeQueryToCommitToMajel"

    invoke-static {v6, v7}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_f
    iget-object v6, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    sget-object v7, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v6, v2, v7}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedPumpkinResult(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    goto/16 :goto_2
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/QueryState;->addObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->cancelSendEvents(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :cond_1
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/velvet/presenter/SearchController;->PREFERENCE_KEYS:Lcom/google/common/collect/ImmutableSet;

    invoke-virtual {v0, p2}, Lcom/google/common/collect/ImmutableSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->maybeForceReload()V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mGoogleCookiesLockObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/ForceableLock;->registerObserver(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->checkWebViewAccessAllowed()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->connectToIcing()V

    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mGoogleCookiesLockObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/ForceableLock;->unregisterObserver(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-virtual {v0, p0}, Lcom/google/android/searchcommon/util/ForceableLock;->release(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isSoundSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->onRecognitionPaused(Lcom/google/android/velvet/Query;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->disconnectFromIcing()V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->freeMemory()V

    :cond_0
    return-void
.end method

.method public onUseLocationChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchController;->maybeForceReload()V

    return-void
.end method

.method public setSearchPlatePresenter(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    return-void
.end method
