.class Lcom/google/android/velvet/presenter/QueryState$CombinedError;
.super Lcom/google/android/velvet/presenter/SearchError;
.source "QueryState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/QueryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CombinedError"
.end annotation


# instance fields
.field private final mFirst:Lcom/google/android/velvet/presenter/SearchError;

.field private final mSecond:Lcom/google/android/velvet/presenter/SearchError;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/SearchError;Lcom/google/android/velvet/presenter/SearchError;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SearchError;
    .param p2    # Lcom/google/android/velvet/presenter/SearchError;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchError;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/QueryState$CombinedError;->mFirst:Lcom/google/android/velvet/presenter/SearchError;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/QueryState$CombinedError;->mSecond:Lcom/google/android/velvet/presenter/SearchError;

    return-void
.end method


# virtual methods
.method public getErrorMessage()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$CombinedError;->mFirst:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchError;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getErrorMessageResId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$CombinedError;->mFirst:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchError;->getErrorMessageResId()I

    move-result v0

    return v0
.end method

.method public retry(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/QueryState;
    .param p2    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$CombinedError;->mFirst:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/presenter/SearchError;->retry(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/QueryState$CombinedError;->mSecond:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/presenter/SearchError;->retry(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V

    return-void
.end method
