.class public interface abstract Lcom/google/android/velvet/presenter/VelvetUi;
.super Ljava/lang/Object;
.source "VelvetUi.java"


# virtual methods
.method public abstract areFragmentsRunningLayoutTransitions()Z
.end method

.method public abstract clearIntent()V
.end method

.method public abstract closeOptionsMenu()V
.end method

.method public abstract doABarrelRoll()V
.end method

.method public abstract dumpActivityState(Ljava/lang/String;Ljava/io/PrintWriter;)V
.end method

.method public abstract findFragmentByTag(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract finish()V
.end method

.method public abstract getActivity()Landroid/app/Activity;
.end method

.method public abstract getBackFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
.end method

.method public abstract getFrontFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
.end method

.method public abstract getIntent()Landroid/content/Intent;
.end method

.method public abstract getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;
.end method

.method public abstract getLayoutInflater()Landroid/view/LayoutInflater;
.end method

.method public abstract getResultsAreaSizeDp()Landroid/graphics/Point;
.end method

.method public abstract getScrollingContainer()Lcom/google/android/velvet/ui/util/CoScrollContainer;
.end method

.method public abstract getSearchPlateHeight()I
.end method

.method public abstract indicateRemoveFromHistoryFailed()V
.end method

.method public abstract isChangingConfigurations()Z
.end method

.method public abstract openUrlInBrowser(Lcom/google/android/searchcommon/util/UriRequest;)V
.end method

.method public abstract setFooterStickiness(IZ)V
.end method

.method public abstract setFragments(Lcom/google/android/velvet/ui/VelvetFragment;Lcom/google/android/velvet/ui/VelvetFragment;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;)V"
        }
    .end annotation
.end method

.method public abstract setSearchPlateStickiness(IZZ)V
.end method

.method public abstract setShowContextHeader(ZZ)V
.end method

.method public abstract showCardInDialog(Lcom/google/geo/sidekick/Sidekick$Entry;Z)V
.end method

.method public abstract showDebugDialog(Ljava/lang/String;)V
.end method

.method public abstract showFooter()V
.end method

.method public abstract showOptionsMenu(Landroid/view/View;)V
.end method

.method public abstract startActivity(Landroid/content/Intent;)V
.end method
