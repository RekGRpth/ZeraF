.class Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "TgPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/TgPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddPredictiveCardsTransaction"
.end annotation


# instance fields
.field private final mDismissTrailFactories:[Lcom/google/android/velvet/tg/DismissTrailFactory;

.field private final mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstCardView:Landroid/view/View;

.field private final mLoadingCardToRemove:Landroid/view/View;

.field private final mNumEntries:I

.field private mPreparedEntries:I

.field private mPreparingStack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final mScrollToCards:Z

.field private final mScrollToEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private final mScrollToPosition:I

.field private final mStacks:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private final mViews:[Landroid/view/View;

.field final synthetic this$0:Lcom/google/android/velvet/presenter/TgPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;Ljava/util/List;ZLjava/util/Map;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;I)V
    .locals 2
    .param p3    # Z
    .param p5    # Landroid/view/View;
    .param p6    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ">;",
            "Landroid/view/View;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            "I)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    const-string v0, "addPredictiveCards"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>(Ljava/lang/String;I)V

    iput-object p2, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mEntries:Ljava/util/List;

    iput-boolean p3, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mScrollToCards:Z

    iput-object p6, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mScrollToEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput p7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mScrollToPosition:I

    iput-object p4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mState:Ljava/util/Map;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mLoadingCardToRemove:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mNumEntries:I

    iget v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mNumEntries:I

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mViews:[Landroid/view/View;

    iget v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mNumEntries:I

    new-array v0, v0, [Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mStacks:[Ljava/util/ArrayList;

    iget v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mNumEntries:I

    new-array v0, v0, [Lcom/google/android/velvet/tg/DismissTrailFactory;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mDismissTrailFactories:[Lcom/google/android/velvet/tg/DismissTrailFactory;

    return-void
.end method

.method private restoreCardState(Landroid/view/View;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v9, -0x1

    const v7, 0x7f100013

    invoke-virtual {p1, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mState:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    if-eqz v0, :cond_2

    const-string v7, "card:views"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {p1, v6}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    instance-of v7, p1, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    if-eqz v7, :cond_1

    const-string v7, "card_expanded"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v7, v8, p2, v1}, Lcom/google/android/velvet/VelvetFactory;->createCardSettingsForAdapter(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;)Landroid/view/View;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v1, v7, v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->showSettingsView(Lcom/google/android/velvet/presenter/TgPresenter;Landroid/view/View;)V

    :cond_1
    const-string v7, "card:focusedViewId"

    invoke-virtual {v0, v7, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-eq v3, v9, :cond_2

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    :cond_2
    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 10
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    const v9, 0x7f0c0066

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getColumnCount()I

    move-result v3

    const/4 v2, 0x0

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mLoadingCardToRemove:Landroid/view/View;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mLoadingCardToRemove:Landroid/view/View;

    invoke-virtual {v1, v7}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    :cond_0
    const/4 v4, 0x0

    :goto_0
    iget v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mNumEntries:I

    if-ge v4, v7, :cond_3

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mViews:[Landroid/view/View;

    aget-object v7, v7, v4

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mViews:[Landroid/view/View;

    aget-object v7, v7, v4

    iget-object v8, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mDismissTrailFactories:[Lcom/google/android/velvet/tg/DismissTrailFactory;

    aget-object v8, v8, v4

    invoke-virtual {v1, v7, v2, v8}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addViewToColumn(Landroid/view/View;ILcom/google/android/velvet/tg/DismissTrailFactory;)V

    :goto_1
    add-int/lit8 v7, v2, 0x1

    rem-int v2, v7, v3

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mStacks:[Ljava/util/ArrayList;

    aget-object v7, v7, v4

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mStacks:[Ljava/util/ArrayList;

    aget-object v6, v7, v4

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mDismissTrailFactories:[Lcom/google/android/velvet/tg/DismissTrailFactory;

    aget-object v7, v7, v4

    invoke-virtual {v1, v6, v2, v7}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addStackToColumn(Ljava/util/ArrayList;ILcom/google/android/velvet/tg/DismissTrailFactory;)V

    goto :goto_1

    :cond_3
    iget v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mScrollToPosition:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_7

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v7

    iget v8, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mScrollToPosition:I

    invoke-interface {v7, v8}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->smoothScrollToY(I)V

    :cond_4
    :goto_2
    iget-boolean v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mScrollToCards:Z

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mFirstCardView:Landroid/view/View;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mFirstCardView:Landroid/view/View;

    invoke-interface {v7, v8, v5}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->scrollToView(Landroid/view/View;I)V

    :cond_5
    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isPredictiveOnlyMode()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;
    invoke-static {v7}, Lcom/google/android/velvet/presenter/TgPresenter;->access$000(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/ViewActionRecorder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->recordViewStartTimes()V

    :cond_6
    return-void

    :cond_7
    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mScrollToEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v8, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mScrollToEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    # invokes: Lcom/google/android/velvet/presenter/TgPresenter;->findCardForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    invoke-static {v7, v8}, Lcom/google/android/velvet/presenter/TgPresenter;->access$700(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v7

    invoke-interface {v7, v0, v5}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->scrollToView(Landroid/view/View;I)V

    goto :goto_2
.end method

.method public prepare()Z
    .locals 10

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    iget v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mNumEntries:I

    if-ge v4, v7, :cond_4

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparingStack:Ljava/util/List;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparingStack:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparingStack:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mStacks:[Ljava/util/ArrayList;

    iget v9, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    aget-object v8, v8, v9

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    sub-int/2addr v7, v8

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/TgPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v8, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
    invoke-static {v8}, Lcom/google/android/velvet/presenter/TgPresenter;->access$600(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v8

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getIdForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v9}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v9

    invoke-virtual {v4, v7, v0, v8, v9}, Lcom/google/android/velvet/VelvetFactory;->createPredictiveCardForAdapter(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mState:Ljava/util/Map;

    if-eqz v4, :cond_1

    invoke-direct {p0, v1, v0}, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->restoreCardState(Landroid/view/View;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mFirstCardView:Landroid/view/View;

    if-nez v4, :cond_2

    iput-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mFirstCardView:Landroid/view/View;

    :cond_2
    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparingStack:Ljava/util/List;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mStacks:[Ljava/util/ArrayList;

    iget v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    aget-object v4, v4, v7

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mStacks:[Ljava/util/ArrayList;

    iget v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    aget-object v4, v4, v7

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparingStack:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ne v4, v7, :cond_5

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparingStack:Ljava/util/List;

    :cond_3
    :goto_1
    iget v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    :cond_4
    iget v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    iget v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mNumEntries:I

    if-ne v4, v7, :cond_9

    move v4, v5

    :goto_2
    move v6, v4

    :cond_5
    return v6

    :cond_6
    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mEntries:Ljava/util/List;

    iget v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntriesToShow()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mDismissTrailFactories:[Lcom/google/android/velvet/tg/DismissTrailFactory;

    iget v8, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/TgPresenter;->access$500(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/CardDismissalHandler;

    move-result-object v9

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-virtual {v9, v4}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->getDismissTrailFactory(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Lcom/google/android/velvet/tg/DismissTrailFactory;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v5, :cond_7

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    goto/16 :goto_0

    :cond_7
    iput-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparingStack:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mStacks:[Ljava/util/ArrayList;

    iget v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mViews:[Landroid/view/View;

    iget v7, p0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;->mPreparedEntries:I

    aput-object v1, v4, v7

    goto :goto_1

    :cond_9
    move v4, v6

    goto :goto_2
.end method
