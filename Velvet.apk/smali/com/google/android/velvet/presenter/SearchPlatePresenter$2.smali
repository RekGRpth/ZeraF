.class Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;
.super Ljava/lang/Object;
.source "SearchPlatePresenter.java"

# interfaces
.implements Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchPlatePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraCreated(Lcom/google/android/goggles/camera/CameraManager;)V
    .locals 1
    .param p1    # Lcom/google/android/goggles/camera/CameraManager;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    # getter for: Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->access$100(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/goggles/GogglesController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/GogglesController;->onCameraCreated(Lcom/google/android/goggles/camera/CameraManager;)V

    :cond_0
    return-void
.end method

.method public onCameraOpened(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    # getter for: Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->access$100(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/goggles/GogglesController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/GogglesController;->onCameraOpened(Z)V

    :cond_0
    return-void
.end method

.method public onCameraReleased()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    # getter for: Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->access$100(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/goggles/GogglesController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/GogglesController;->onCameraReleased()V

    :cond_0
    return-void
.end method

.method public onCaptureButtonClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    # getter for: Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->access$100(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/goggles/GogglesController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/GogglesController;->onCaptureButtonClicked()V

    :cond_0
    return-void
.end method

.method public onFlashButtonClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$2;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    # getter for: Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->access$100(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/goggles/GogglesController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/GogglesController;->onFlashButtonClicked()V

    :cond_0
    return-void
.end method
