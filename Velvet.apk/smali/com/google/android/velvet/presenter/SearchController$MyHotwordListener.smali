.class Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHotwordListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchController;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/SearchController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SearchController;
    .param p2    # Lcom/google/android/velvet/presenter/SearchController$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;-><init>(Lcom/google/android/velvet/presenter/SearchController;)V

    return-void
.end method


# virtual methods
.method public onHotword(J)V
    .locals 2
    .param p1    # J

    const/16 v0, 0x41

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->voiceSearchFromHotword()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    return-void
.end method

.method public onHotwordDetectorStarted()V
    .locals 2

    const-string v0, "Velvet.SearchController"

    const-string v1, "#onHotwordDetectorStarted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->onListeningForHotwordChanged(Z)V

    return-void
.end method

.method public onHotwordDetectorStopped()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->onListeningForHotwordChanged(Z)V

    return-void
.end method

.method public onMusicDetected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyHotwordListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->onMusicDetected()V

    return-void
.end method
