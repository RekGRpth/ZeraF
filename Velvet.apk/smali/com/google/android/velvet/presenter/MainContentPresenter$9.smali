.class Lcom/google/android/velvet/presenter/MainContentPresenter$9;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "MainContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/MainContentPresenter;->postScrollToView(Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

.field final synthetic val$offsetFromTop:I

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;ILandroid/view/View;I)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;
    .param p4    # I

    iput-object p1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$9;->this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$9;->val$view:Landroid/view/View;

    iput p6, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$9;->val$offsetFromTop:I

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>(Ljava/lang/String;Ljava/lang/Object;I)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$9;->val$view:Landroid/view/View;

    iget v2, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$9;->val$offsetFromTop:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->scrollToView(Landroid/view/View;I)V

    return-void
.end method
