.class Lcom/google/android/velvet/presenter/TgPresenter$4;
.super Ljava/lang/Object;
.source "TgPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/TgPresenter;->showLoadingCard()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/TgPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/TgPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/VelvetFactory;->createLoadingCard(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->access$302(Lcom/google/android/velvet/presenter/TgPresenter;Landroid/view/View;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/TgPresenter;->access$300(Lcom/google/android/velvet/presenter/TgPresenter;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/TgPresenter;->access$300(Lcom/google/android/velvet/presenter/TgPresenter;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/velvet/presenter/TgPresenter;->postAddAndScrollToView(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    # getter for: Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/TgPresenter;->access$300(Lcom/google/android/velvet/presenter/TgPresenter;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/google/android/velvet/presenter/TgPresenter;->postSetVisibility(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$4;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->access$402(Lcom/google/android/velvet/presenter/TgPresenter;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method
