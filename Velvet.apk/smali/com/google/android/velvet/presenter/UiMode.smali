.class public final enum Lcom/google/android/velvet/presenter/UiMode;
.super Ljava/lang/Enum;
.source "UiMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/UiMode$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/velvet/presenter/UiMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum CONNECTION_ERROR:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum GOGGLES_CAPTURE:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum GOGGLES_DISAMBIGUATION:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum GOGGLES_FALLBACK:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum NONE:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum PREDICTIVE:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum PREDICTIVE_CARD_LIST:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum RESULTS:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum RESULTS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum SOUND_SEARCH:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum SUMMONS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

.field public static final enum VOICESEARCH:Lcom/google/android/velvet/presenter/UiMode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "PREDICTIVE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "VOICESEARCH"

    invoke-direct {v0, v1, v5}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->VOICESEARCH:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "SUGGEST"

    invoke-direct {v0, v1, v6}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "RESULTS"

    invoke-direct {v0, v1, v7}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "RESULTS_SUGGEST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->RESULTS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "SUMMONS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "CONNECTION_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->CONNECTION_ERROR:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "PREDICTIVE_CARD_LIST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE_CARD_LIST:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "SUMMONS_SUGGEST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "GOGGLES_CAPTURE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_CAPTURE:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "GOGGLES_DISAMBIGUATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_DISAMBIGUATION:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "GOGGLES_FALLBACK"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_FALLBACK:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/UiMode;

    const-string v1, "SOUND_SEARCH"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->SOUND_SEARCH:Lcom/google/android/velvet/presenter/UiMode;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/android/velvet/presenter/UiMode;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->VOICESEARCH:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->RESULTS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->CONNECTION_ERROR:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE_CARD_LIST:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_CAPTURE:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_DISAMBIGUATION:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_FALLBACK:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->SOUND_SEARCH:Lcom/google/android/velvet/presenter/UiMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/velvet/presenter/UiMode;->$VALUES:[Lcom/google/android/velvet/presenter/UiMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/velvet/presenter/UiMode;
    .locals 1

    const-class v0, Lcom/google/android/velvet/presenter/UiMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/UiMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/velvet/presenter/UiMode;
    .locals 1

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->$VALUES:[Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0}, [Lcom/google/android/velvet/presenter/UiMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/velvet/presenter/UiMode;

    return-object v0
.end method


# virtual methods
.method public canShowSpinner()Z
    .locals 1

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBackFragmentTag()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode$1;->$SwitchMap$com$google$android$velvet$presenter$UiMode:[I

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/UiMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "suggest"

    goto :goto_0

    :pswitch_1
    const-string v0, "goggleshistorymsg"

    goto :goto_0

    :pswitch_2
    const-string v0, "gogglesdisambig"

    goto :goto_0

    :pswitch_3
    const-string v0, "results"

    goto :goto_0

    :pswitch_4
    const-string v0, "summons"

    goto :goto_0

    :pswitch_5
    const-string v0, "error"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getFrontFragmentTag()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode$1;->$SwitchMap$com$google$android$velvet$presenter$UiMode:[I

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/UiMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "suggest"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method public getSummonsQueryStrategy()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/UiMode;->isSuggestMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/UiMode;->isSummonsMode()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isHotwordSupported()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/UiMode;->isSuggestMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPredictiveMode()Z
    .locals 1

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE_CARD_LIST:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSuggestMode()Z
    .locals 1

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSummonsMode()Z
    .locals 1

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isViewAndEditMode()Z
    .locals 1

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public usesSuggestions()Z
    .locals 1

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->RESULTS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->CONNECTION_ERROR:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
