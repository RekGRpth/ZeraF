.class public Lcom/google/android/velvet/presenter/VelvetPresenter;
.super Ljava/lang/Object;
.source "VelvetPresenter.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/QueryState$Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/VelvetPresenter$13;,
        Lcom/google/android/velvet/presenter/VelvetPresenter$BrowserDimensionsSupplier;,
        Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;
    }
.end annotation


# instance fields
.field private final mChangeModeTask:Ljava/lang/Runnable;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;

.field private mContextHeaderPresenter:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

.field private final mCookies:Lcom/google/android/velvet/Cookies;

.field private final mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

.field private final mCorpora:Lcom/google/android/velvet/Corpora;

.field private final mCorporaObserver:Landroid/database/DataSetObserver;

.field private mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

.field private final mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

.field private final mDelayedInitializeTask:Ljava/lang/Runnable;

.field private mFocused:Z

.field private mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

.field private final mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

.field private mGogglesController:Lcom/google/android/goggles/GogglesController;

.field private final mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;

.field private final mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private mLocationOracleLock:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

.field private final mLogIdleTask:Ljava/lang/Runnable;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

.field private final mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

.field private mNewIntent:Landroid/content/Intent;

.field private final mNotifyFragmentsAndChangeModeTask:Ljava/lang/Runnable;

.field private mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

.field private mQueryState:Lcom/google/android/velvet/presenter/QueryState;

.field private mRestoredInstance:Z

.field private mResumed:Z

.field private mSearchController:Lcom/google/android/velvet/presenter/SearchController;

.field private mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

.field private mSuggestionLauncher:Lcom/google/android/searchcommon/suggest/SuggestionLauncher;

.field private mSuggestionViewRecycler:Lcom/google/android/velvet/ui/util/ViewRecycler;

.field private final mSuggestionsController:Lcom/google/android/searchcommon/suggest/SuggestionsController;

.field private final mSuggestionsUi:Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;

.field private final mUi:Lcom/google/android/velvet/presenter/VelvetUi;

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

.field private final mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

.field private mWaitingForLayoutTransitionToSwitchMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/velvet/presenter/VelvetUi;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/searchcommon/AsyncServices;Lcom/google/android/searchcommon/GlobalSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/velvet/ActivityLifecycleObserver;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/debug/DebugFeatures;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/velvet/presenter/VelvetUi;
    .param p3    # Lcom/google/android/searchcommon/CoreSearchServices;
    .param p4    # Lcom/google/android/searchcommon/AsyncServices;
    .param p5    # Lcom/google/android/searchcommon/GlobalSearchServices;
    .param p6    # Lcom/google/android/velvet/VelvetFactory;
    .param p7    # Lcom/google/android/velvet/ActivityLifecycleObserver;
    .param p8    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p9    # Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/velvet/presenter/VelvetPresenter$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$1;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mDelayedInitializeTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/presenter/VelvetPresenter$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$2;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLogIdleTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/presenter/VelvetPresenter$3;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$3;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorporaObserver:Landroid/database/DataSetObserver;

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    new-instance v0, Lcom/google/android/velvet/presenter/VelvetPresenter$4;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$4;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNotifyFragmentsAndChangeModeTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/presenter/VelvetPresenter$5;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$5;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mChangeModeTask:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    iput-object p6, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getCorpora()Lcom/google/android/velvet/Corpora;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-interface {p4}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getCookies()Lcom/google/android/velvet/Cookies;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p9, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    new-instance v0, Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/VelvetPresenter$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsUi:Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetFactory;->createSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsController:Lcom/google/android/searchcommon/suggest/SuggestionsController;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p7, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;

    invoke-interface {p3}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    new-instance v0, Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {v0, v1}, Lcom/google/android/velvet/presenter/UiModeManager;-><init>(Lcom/google/android/searchcommon/SearchConfig;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    new-instance v0, Lcom/google/android/velvet/presenter/VelvetFragments;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/velvet/presenter/VelvetFragments;-><init>(Lcom/google/android/velvet/presenter/VelvetUi;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    iput-object p8, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->initializeDelayed()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->logIdle()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/UiMode;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/UiModeManager;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isReady(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/SearchController;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/searchcommon/suggest/SuggestionsController;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsController:Lcom/google/android/searchcommon/suggest/SuggestionsController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateCorpora()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->notifyFragmentsAndEnterPendingMode()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->enterPendingMode()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/VelvetUi;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/QueryState;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/searchcommon/google/UserInteractionLogger;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    return-object v0
.end method

.method private addFeedbackMenuItem(Landroid/view/Menu;)V
    .locals 2

    const v0, 0x7f0d042d

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/VelvetPresenter$12;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$12;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    return-void
.end method

.method private addGogglesMenuItem(Landroid/view/Menu;)V
    .locals 2

    const v0, 0x7f0d0039

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/VelvetPresenter$8;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$8;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    return-void
.end method

.method private addTheGoogleMenuItems(Landroid/view/Menu;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isMarinerEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE_CARD_LIST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq v0, v1, :cond_0

    const v0, 0x7f0d00d7

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/VelvetPresenter$9;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$9;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE_CARD_LIST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq v0, v1, :cond_1

    const v0, 0x7f0d00d8

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/VelvetPresenter$10;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$10;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d0321

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/VelvetPresenter$11;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$11;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_2
    return-void
.end method

.method private cancelIdleTasks()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mDelayedInitializeTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLogIdleTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private cancelModeChangeTasks()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNotifyFragmentsAndChangeModeTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mChangeModeTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mWaitingForLayoutTransitionToSwitchMode:Z

    return-void
.end method

.method private enterMode(Lcom/google/android/velvet/presenter/UiMode;)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->cancelModeChangeTasks()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mResumed:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNotifyFragmentsAndChangeModeTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private enterPendingMode()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/VelvetUi;->finish()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    iput-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateMode(Lcom/google/android/velvet/presenter/UiMode;)V

    return-void
.end method

.method private getQueryFromIntent(Landroid/content/Intent;)Lcom/google/android/velvet/Query;
    .locals 4

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isSearchIntent(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isSendTextIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->hasQueryStringExtra(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Query;->fromIntent(Landroid/content/Intent;)Lcom/google/android/velvet/Query;

    move-result-object v0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isBeamIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;Z)Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->fromIntent()Lcom/google/android/velvet/Query;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v1, "Velvet.Presenter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not get query from beam URL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isGogglesIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query;->goggles(I)Lcom/google/android/velvet/Query;

    move-result-object v0

    goto :goto_0

    :cond_5
    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isZXingIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->offlineGoggles()Lcom/google/android/velvet/Query;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isVoiceSearchIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isResumeFromHistory(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_9

    const/16 v1, 0x15

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->isCannedAudioEnabled()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->getVoiceCannedAudio(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    :cond_7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->voiceSearch()Lcom/google/android/velvet/Query;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/Query;->voiceSearchWithCannedAudio(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isTheGoogleIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isMarinerEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/Query;->sentinel(Lcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)Lcom/google/android/velvet/Query;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private initializeDelayed()V
    .locals 6

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/velvet/VelvetStrictMode;->onStartupPoint(I)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->delayWrites()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v3, Lcom/google/android/velvet/presenter/VelvetPresenter$6;

    invoke-direct {v3, p0, v1}, Lcom/google/android/velvet/presenter/VelvetPresenter$6;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/searchcommon/GsaPreferenceController;)V

    const-wide/16 v4, 0x7d0

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/UiModeManager;->setStartupComplete()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateFragments()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateSuggestFragment()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->isBeamEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getBeamHelper()Lcom/google/android/velvet/util/BeamHelper;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/util/BeamHelper;->initialize(Landroid/app/Activity;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/SearchController;->init()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/velvet/VelvetBackgroundTasks;->notifyUiLaunched()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/velvet/VelvetBackgroundTasks;->maybeStartTasks()V

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->maybeRegisterSidekickAlarms()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->maybeExecuteUpgradeTasks()V

    const/16 v2, 0x1c

    invoke-static {v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    const/4 v2, 0x7

    invoke-static {v2}, Lcom/google/android/velvet/VelvetStrictMode;->onStartupPoint(I)V

    return-void
.end method

.method private isReady(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logCurrentMode()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/UiMode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logView(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private logIdle()V
    .locals 1

    const/16 v0, 0x20

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    return-void
.end method

.method private logStartIntent(Landroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x0

    const-string v0, "UNKNOWN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.ASSIST"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isLaunchFromFirstRunActivity(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "FIRST_RUN"

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v2, "START"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->typeFromIntent(Landroid/content/Intent;)Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v1

    const-string v0, "notificationEntriesKey"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "notificationEntriesKey"

    invoke-static {p1, v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntriesFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v4, "NOTIFICATION_CLICK"

    invoke-virtual {v3, v4, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logMetricsAction(Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v0, "assist_intent_source"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "assist_intent_source"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    const-string v1, "PREDICTIVE_WIDGET"

    const-string v0, "target_entry"

    invoke-static {p1, v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-result-object v2

    invoke-interface {v2, v0, v5, v5}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "target_group_entry_tree"

    invoke-static {p1, v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryTreeNodeFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->createForGroup(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    :cond_4
    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v3, "WIDGET_PRESS"

    invoke-virtual {v2, v3, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapter(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    :cond_5
    move-object v0, v1

    goto :goto_0

    :cond_6
    const-string v0, "UNKNOWN_ASSIST_SOURCE"

    goto :goto_0

    :cond_7
    const-string v0, "ASSIST_GESTURE"

    goto/16 :goto_0

    :cond_8
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v0, "LAUNCHER"

    goto/16 :goto_0

    :cond_9
    const-string v2, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v0, "SYSTEM_WEB_SEARCH"

    goto/16 :goto_0

    :cond_a
    const-string v2, "android.search.action.GLOBAL_SEARCH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v0, "SEARCH_WIDGET"

    goto/16 :goto_0

    :cond_b
    const-string v2, "android.intent.action.SEARCH_LONG_PRESS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v0, "SEARCH_LONG_PRESS"

    goto/16 :goto_0

    :cond_c
    const-string v2, "android.speech.action.WEB_SEARCH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v0, "VOICE_SEARCH"

    goto/16 :goto_0

    :cond_d
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "SHARE_INTENT"

    goto/16 :goto_0
.end method

.method private maybeExecuteUpgradeTasks()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getLastRunVersion()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode(Landroid/content/Context;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v2

    const-string v3, "update_gservices_config"

    invoke-interface {v2, v3}, Lcom/google/android/velvet/VelvetBackgroundTasks;->forceRunInterruptingOngoing(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v2

    const-string v3, "refresh_search_domain_and_cookies"

    invoke-interface {v2, v3}, Lcom/google/android/velvet/VelvetBackgroundTasks;->forceRunInterruptingOngoing(Ljava/lang/String;)V

    :cond_0
    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setLastRunVersion(I)V

    :cond_1
    return-void
.end method

.method private maybeInsertQueryIntoLocalHistory(Lcom/google/android/velvet/Query;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;->insertLocalHistory(Ljava/lang/String;J)V

    :cond_0
    return-void
.end method

.method private maybeRefreshSearchHistory()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->takeNewlyLoadedWebQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v0

    const-string v1, "refresh_search_history"

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getRefreshSearchHistoryDelay()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/velvet/VelvetBackgroundTasks;->forceRun(Ljava/lang/String;J)V

    :cond_0
    return-void
.end method

.method private maybeShowMarinerFirstRunScreens(Landroid/content/Intent;)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAllAccounts()[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v3, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isAccountOptedIn(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "assist_intent_source"

    const/4 v3, -0x1

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v3}, Lcom/google/android/searchcommon/MarinerOptInSettings;->userHasSeenFirstRunScreens()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isLaunchFromFirstRunActivity(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isVoiceSearchIntent(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isBeamIntent(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->shouldDisableMarinerOptIn(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    :cond_2
    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v2, 0x10008000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v2, v0}, Lcom/google/android/velvet/presenter/VelvetUi;->startActivity(Landroid/content/Intent;)V

    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method private notifyFragmentsAndEnterPendingMode()V
    .locals 8

    const/4 v5, 0x1

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v6, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    if-ne v4, v6, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v6, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    if-ne v4, v6, :cond_3

    :cond_0
    move v4, v5

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "current mode:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; pending:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v6, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    if-eq v4, v6, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v4}, Lcom/google/android/velvet/presenter/VelvetUi;->getFrontFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v4}, Lcom/google/android/velvet/presenter/VelvetUi;->getBackFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v3

    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v6, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v1, v4, v6, v7, v3}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onPreModeSwitch(Lcom/google/android/velvet/presenter/UiModeManager;Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;Z)Z

    move-result v4

    or-int/2addr v2, v4

    :cond_1
    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v6, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0, v4, v6, v7, v3}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onPreModeSwitch(Lcom/google/android/velvet/presenter/UiModeManager;Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;Z)Z

    move-result v4

    or-int/2addr v2, v4

    :cond_2
    if-eqz v2, :cond_4

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v4}, Lcom/google/android/velvet/presenter/VelvetUi;->areFragmentsRunningLayoutTransitions()Z

    move-result v4

    if-eqz v4, :cond_4

    iput-boolean v5, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mWaitingForLayoutTransitionToSwitchMode:Z

    :goto_1
    return-void

    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->enterPendingMode()V

    goto :goto_1
.end method

.method private setSourceParams(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getSearchSourceParam()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/velvet/util/IntentUtils;->getSourceParam(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->setSource(Ljava/lang/String;)V

    return-void
.end method

.method private setSuggestionsActive(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getShouldQueryStrategy()Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/UiMode;->getSummonsQueryStrategy()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->setSummonsQueryStrategy(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsUi:Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->start(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsUi:Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->stop(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;)V

    goto :goto_0
.end method

.method private setupFromIntent(Landroid/content/Intent;Z)V
    .locals 5
    .param p1    # Landroid/content/Intent;
    .param p2    # Z

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->logStartIntent(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->cancelModeChangeTasks()V

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    iput-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    iput-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->reset()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchController;->clearWebViewsHistory()V

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->maybeShowMarinerFirstRunScreens(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getQueryFromIntent(Landroid/content/Intent;)Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->shouldCommitQueryFromIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    :goto_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isReady(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->resetView(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/googlequicksearchbox/DebugIntent;->handleIntent(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v2

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/velvet/Query;->sentinel(Lcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v2

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/velvet/Query;->sentinel(Lcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchController;->getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->setForceSuggestionFetch()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->resetSearchPlate()V

    goto :goto_1
.end method

.method private shouldCommitQueryFromIntent(Landroid/content/Intent;)Z
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-static {p1}, Lcom/google/android/velvet/util/IntentUtils;->isGlobalSearchIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateCorpora()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/FooterPresenter;->updateCorpora()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->updateCorpora(Lcom/google/android/velvet/Corpora;)V

    return-void
.end method

.method private updateFooterStickiness(Z)V
    .locals 4
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->shouldSuppressCorpora()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/presenter/UiModeManager;->getFooterStickiness(Lcom/google/android/velvet/presenter/UiMode;Z)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v1, v0, p1}, Lcom/google/android/velvet/presenter/VelvetUi;->setFooterStickiness(IZ)V

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->takeShowCorporaRequest()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/VelvetUi;->showFooter()V

    :cond_1
    return-void
.end method

.method private updateFragments()V
    .locals 6

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/UiMode;->getFrontFragmentTag()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/UiMode;->getBackFragmentTag()Ljava/lang/String;

    move-result-object v1

    if-nez v3, :cond_2

    move-object v2, v0

    :goto_0
    if-nez v1, :cond_3

    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/VelvetFragment;->isAttached()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/VelvetFragment;->isCurrentFrontFragment()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/VelvetFragment;->getVelvetTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/presenter/VelvetFragments;->createNewFragment(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v2

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/VelvetFragment;->isAttached()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/VelvetFragment;->isCurrentBackFragment()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/VelvetFragment;->getVelvetTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/presenter/VelvetFragments;->createNewFragment(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v0

    :cond_1
    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v4, v2, v0}, Lcom/google/android/velvet/presenter/VelvetUi;->setFragments(Lcom/google/android/velvet/ui/VelvetFragment;Lcom/google/android/velvet/ui/VelvetFragment;)V

    return-void

    :cond_2
    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    invoke-virtual {v4, v3}, Lcom/google/android/velvet/presenter/VelvetFragments;->getFragment(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v2

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    invoke-virtual {v4, v1}, Lcom/google/android/velvet/presenter/VelvetFragments;->getFragment(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v0

    goto :goto_1
.end method

.method private updateHintText()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isReady(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    const v1, 0x7f0d036e

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->setHintTextResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->isHotwordActive()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    const v1, 0x7f0d0394

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->setHintTextResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    const v1, 0x7f0d0395

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->setHintTextResource(I)V

    goto :goto_0
.end method

.method private updateKeyboard()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mResumed:Z

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFocused:Z

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->takeEditingQueryChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterQueryEditMode()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->hideKeyboard()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->isViewAndEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->resetSearchPlate()V

    goto :goto_0
.end method

.method private updateMode(Lcom/google/android/velvet/presenter/UiMode;)V
    .locals 18
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;

    const/16 v13, 0x19

    invoke-static {v13}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-static {v13}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/UiModeManager;->isStartupComplete()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateFragments()V

    :cond_0
    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v3, 0x0

    sget-object v13, Lcom/google/android/velvet/presenter/VelvetPresenter$13;->$SwitchMap$com$google$android$velvet$presenter$UiMode:[I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v14}, Lcom/google/android/velvet/presenter/UiMode;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Invalid mode "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    :goto_0
    :pswitch_0
    sget-object v13, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    move-object/from16 v0, p1

    if-ne v0, v13, :cond_8

    const/4 v12, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSearchPlateStickiness()I

    move-result v14

    const/4 v15, 0x1

    invoke-interface {v13, v14, v12, v15}, Lcom/google/android/velvet/presenter/VelvetUi;->setSearchPlateStickiness(IZZ)V

    if-nez v12, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/QueryState;->shouldSuppressCorpora()Z

    move-result v13

    if-eqz v13, :cond_9

    :cond_1
    const/4 v13, 0x1

    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateFooterStickiness(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v13, v14}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldShowContextHeader(Lcom/google/android/velvet/presenter/UiMode;)Z

    move-result v4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContextHeaderPresenter:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    invoke-virtual {v13, v4}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v13, v4, v12}, Lcom/google/android/velvet/presenter/VelvetUi;->setShowContextHeader(ZZ)V

    if-eqz v11, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v13, v12}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterVoiceSearchMode(Z)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    move-object/from16 v0, p1

    invoke-virtual {v13, v14, v0}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldResetSearchBox(Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;)Z

    move-result v13

    if-eqz v13, :cond_e

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->resetSearchPlate()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v14, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-ne v13, v14, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterQueryEditMode()V

    :cond_2
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsController:Lcom/google/android/searchcommon/suggest/SuggestionsController;

    invoke-interface {v13}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->getSummonsFetchState()I

    move-result v13

    const/4 v14, 0x3

    if-ne v13, v14, :cond_10

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    move-object/from16 v0, p1

    invoke-virtual {v13, v14, v0, v2}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldShowCorpusBarInMode(Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;Z)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v13, v5}, Lcom/google/android/velvet/presenter/FooterPresenter;->setShowCorpusBar(Z)V

    if-nez v5, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/FooterPresenter;->resetShowMoreCorpora()V

    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateSuggestFragment()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isMarinerEnabled()Z

    move-result v17

    invoke-virtual/range {v14 .. v17}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldShowTgFooterButton(Lcom/google/android/velvet/presenter/UiMode;ZZ)Z

    move-result v14

    invoke-virtual {v13, v14}, Lcom/google/android/velvet/presenter/FooterPresenter;->setShowTgFooterButton(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/UiMode;->usesSuggestions()Z

    move-result v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setSuggestionsActive(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateSpinnerState()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->logCurrentMode()V

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v13}, Lcom/google/android/searchcommon/google/LocationSettings;->maybeShowLegacyOptIn()V

    :cond_4
    const/16 v13, 0x1a

    invoke-static {v13}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    :pswitch_1
    return-void

    :pswitch_2
    const/4 v3, 0x1

    goto/16 :goto_0

    :pswitch_3
    const/4 v11, 0x1

    goto/16 :goto_0

    :pswitch_4
    const/4 v6, 0x1

    goto/16 :goto_0

    :pswitch_5
    const/4 v1, 0x1

    const/4 v7, 0x1

    goto/16 :goto_0

    :pswitch_6
    const/4 v1, 0x4

    const/4 v7, 0x1

    goto/16 :goto_0

    :pswitch_7
    const/4 v10, 0x1

    goto/16 :goto_0

    :pswitch_8
    const/4 v3, 0x1

    goto/16 :goto_0

    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v13

    if-eqz v13, :cond_5

    const/4 v9, 0x1

    goto/16 :goto_0

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v13

    if-eqz v13, :cond_7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/Action;->hasSimilarImageResults()Z

    move-result v13

    if-eqz v13, :cond_6

    const/4 v1, 0x3

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x2

    goto/16 :goto_0

    :cond_7
    const/4 v8, 0x1

    goto/16 :goto_0

    :pswitch_a
    const/4 v8, 0x1

    goto/16 :goto_0

    :cond_8
    const/4 v12, 0x0

    goto/16 :goto_1

    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_2

    :cond_a
    if-eqz v10, :cond_b

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v13, v12}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterSoundSearchMode(Z)V

    goto/16 :goto_3

    :cond_b
    if-eqz v6, :cond_c

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v13, v12}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterGogglesMode(Z)V

    goto/16 :goto_3

    :cond_c
    if-eqz v7, :cond_d

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v13, v12, v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterGogglesResponseMode(ZI)V

    goto/16 :goto_3

    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v13, v12}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterTextMode(Z)V

    goto/16 :goto_3

    :cond_e
    if-eqz v9, :cond_f

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->showVoiceQueryComitted()V

    goto/16 :goto_4

    :cond_f
    if-eqz v8, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v13}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->showQueryComitted()V

    goto/16 :goto_4

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method private updateSpinnerState()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->shouldShowSpinner()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->canShowSpinner()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->showProgress()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->hideProgress()V

    goto :goto_0
.end method

.method private updateSuggestFragment()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isReady(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->updateMode(Lcom/google/android/velvet/presenter/UiModeManager;Lcom/google/android/velvet/presenter/UiMode;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method clearQuery()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->startEditingQuery()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->clearQuery()V

    return-void
.end method

.method public createMenuItems(Landroid/view/Menu;Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldUsePredictiveInMode(Lcom/google/android/velvet/presenter/UiMode;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->addTheGoogleMenuItems(Landroid/view/Menu;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/VelvetApplication;

    invoke-static {v0}, Lcom/google/android/goggles/GogglesUtils;->deviceSupportsGoggles(Lcom/google/android/velvet/VelvetApplication;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldShowGogglesMenuItem(Lcom/google/android/velvet/presenter/UiMode;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->addGogglesMenuItem(Landroid/view/Menu;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/VelvetApplication;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_CAPTURE:Lcom/google/android/velvet/presenter/UiMode;

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/GogglesSettings;->shouldShowDebugView()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/google/android/goggles/QueryFrameProcessor;->insertFakeTriggers(Landroid/view/Menu;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->insertFakeTriggers(Landroid/app/Activity;Landroid/view/Menu;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0, p1, p2}, Lcom/google/android/searchcommon/SearchSettings;->addMenuItems(Landroid/view/Menu;Z)V

    new-instance v0, Lcom/google/android/velvet/Help;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/velvet/Help;-><init>(Landroid/content/Context;)V

    const-string v1, "main"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/velvet/Help;->addHelpMenuItem(Landroid/view/Menu;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->addFeedbackMenuItem(Landroid/view/Menu;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "[DEBUG] Sysdump"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/VelvetPresenter$7;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$7;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_3
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "VelvetPresenter state:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Mode: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, " Pending: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getCommittedQuery()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public getConfig()Lcom/google/android/searchcommon/SearchConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-object v0
.end method

.method getDimensionPixelSize(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public getFactory()Lcom/google/android/velvet/VelvetFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    return-object v0
.end method

.method public getFragments()Lcom/google/android/velvet/presenter/VelvetFragments;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    return-object v0
.end method

.method getGogglesController()Lcom/google/android/goggles/GogglesController;
    .locals 3

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchController;->getGogglesCallback()Lcom/google/android/goggles/GogglesController$Callback;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/SearchController;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/VelvetFactory;->createGogglesController(Lcom/google/android/goggles/GogglesController$Callback;Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/goggles/GogglesController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mGogglesController:Lcom/google/android/goggles/GogglesController;

    return-object v0
.end method

.method public getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;

    move-result-object v0

    return-object v0
.end method

.method public getLayoutInflater()Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method getModeToSwitchTo()Lcom/google/android/velvet/presenter/UiMode;
    .locals 5

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/Corpus;->isSummons()Z

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->resultsLoadedAndNothingToShow()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_FALLBACK:Lcom/google/android/velvet/presenter/UiMode;

    :goto_0
    return-object v3

    :cond_0
    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->haveCommit()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v2, :cond_1

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    :goto_1
    goto :goto_0

    :cond_1
    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->RESULTS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    :goto_2
    goto :goto_0

    :cond_3
    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    goto :goto_2

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getSentinelMode()Lcom/google/android/velvet/presenter/UiMode;

    move-result-object v3

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/Corpus;->isSummons()Z

    move-result v4

    if-eqz v4, :cond_6

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->shouldShowError()Z

    move-result v4

    if-eqz v4, :cond_7

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->CONNECTION_ERROR:Lcom/google/android/velvet/presenter/UiMode;

    goto :goto_0

    :cond_7
    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->resultsLoadedAndNothingToShow()Z

    move-result v4

    if-eqz v4, :cond_8

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_FALLBACK:Lcom/google/android/velvet/presenter/UiMode;

    goto :goto_0

    :cond_8
    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    if-nez v0, :cond_9

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_CAPTURE:Lcom/google/android/velvet/presenter/UiMode;

    goto :goto_0

    :cond_9
    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->needDisambiguation()Z

    move-result v4

    if-eqz v4, :cond_a

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_DISAMBIGUATION:Lcom/google/android/velvet/presenter/UiMode;

    goto :goto_0

    :cond_a
    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    goto :goto_0

    :cond_b
    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_c

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->VOICESEARCH:Lcom/google/android/velvet/presenter/UiMode;

    goto :goto_0

    :cond_c
    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->isSoundSearch()Z

    move-result v4

    if-eqz v4, :cond_d

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v4

    if-nez v4, :cond_d

    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->SOUND_SEARCH:Lcom/google/android/velvet/presenter/UiMode;

    goto/16 :goto_0

    :cond_d
    sget-object v3, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    goto/16 :goto_0
.end method

.method public getResultsAreaSizeDp()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getResultsAreaSizeDp()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getScrollingContainer()Lcom/google/android/velvet/ui/util/CoScrollContainer;

    move-result-object v0

    return-object v0
.end method

.method public getScrollingContainer()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getScrollingContainer()Lcom/google/android/velvet/ui/util/CoScrollContainer;

    move-result-object v0

    return-object v0
.end method

.method getSearchController()Lcom/google/android/velvet/presenter/SearchController;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    return-object v0
.end method

.method public getSearchPlateHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getSearchPlateHeight()I

    move-result v0

    return v0
.end method

.method public getSearchPlateStickiness()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiModeManager;->getSearchPlateStickiness(Lcom/google/android/velvet/presenter/UiMode;Z)I

    move-result v0

    return v0
.end method

.method getString(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method varargs getString(I[Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I
    .param p2    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestionClickListener()Lcom/google/android/searchcommon/ui/SuggestionClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionLauncher:Lcom/google/android/searchcommon/suggest/SuggestionLauncher;

    return-object v0
.end method

.method public getSuggestionViewRecycler()Lcom/google/android/velvet/ui/util/ViewRecycler;
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionViewRecycler:Lcom/google/android/velvet/ui/util/ViewRecycler;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetFactory;->createSuggestionViewRecycler()Lcom/google/android/velvet/ui/util/ViewRecycler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionViewRecycler:Lcom/google/android/velvet/ui/util/ViewRecycler;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionViewRecycler:Lcom/google/android/velvet/ui/util/ViewRecycler;

    return-object v0
.end method

.method public getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsController:Lcom/google/android/searchcommon/suggest/SuggestionsController;

    return-object v0
.end method

.method goBack()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchController;->onBackPressed()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->goBack()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChangingConfigurations()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->isChangingConfigurations()Z

    move-result v0

    return v0
.end method

.method public isMarinerEnabled()Z
    .locals 6

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->isTheGoogleDeployed()Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v5, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->canAccountRunTheGoogle(Landroid/accounts/Account;)I

    move-result v5

    if-ne v5, v3, :cond_0

    move v1, v3

    :goto_0
    iget-object v5, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v5, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isAccountOptedIn(Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    :goto_1
    return v3

    :cond_0
    move v1, v4

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_1
.end method

.method isPredictiveOnlyMode()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v0

    return v0
.end method

.method public onCorpusSelected(Lcom/google/android/velvet/Corpus;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpus;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/QueryState;->selectCorpus(Lcom/google/android/velvet/Corpus;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Lcom/google/android/velvet/presenter/SearchController;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
    .param p2    # Lcom/google/android/velvet/presenter/SearchController;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-virtual {v0}, Lcom/google/android/velvet/Cookies;->sync()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mDelayedInitializeTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeOnIdle(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNewIntent:Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNewIntent:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setSourceParams(Landroid/content/Intent;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mRestoredInstance:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mVelvetFactory:Lcom/google/android/velvet/VelvetFactory;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/SearchController;->getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/velvet/VelvetFactory;->createSuggestionsLauncher(Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;)Lcom/google/android/searchcommon/suggest/SuggestionLauncher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionLauncher:Lcom/google/android/searchcommon/suggest/SuggestionLauncher;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mRestoredInstance:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSearchController()Lcom/google/android/velvet/presenter/SearchController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchController;->getCardController()Lcom/google/android/voicesearch/VelvetCardController;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/velvet/presenter/QueryState;->restoreInstanceState(Landroid/os/Bundle;Lcom/google/android/voicesearch/VelvetCardController;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->clearIntent()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->createMenuItems(Landroid/view/Menu;Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    iput-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->setBrowserDimensionsSupplier(Lcom/google/common/base/Supplier;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorporaObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Corpora;->unregisterObserver(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->cancelIdleTasks()V

    return-void
.end method

.method public onFragmentLayoutTransitionFinished()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mWaitingForLayoutTransitionToSwitchMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->areFragmentsRunningLayoutTransitions()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->cancelModeChangeTasks()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mChangeModeTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onFragmentPresenterAttached(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)V
    .locals 5
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "searchplate"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    check-cast p1, Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/SearchController;->setSearchPlatePresenter(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "contextheader"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast p1, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContextHeaderPresenter:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    goto :goto_0

    :cond_2
    const-string v1, "suggest"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    check-cast p1, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->updateMode(Lcom/google/android/velvet/presenter/UiModeManager;Lcom/google/android/velvet/presenter/UiMode;Z)V

    goto :goto_0

    :cond_3
    const-string v1, "footer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/velvet/presenter/FooterPresenter;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    goto :goto_0
.end method

.method public onFragmentPresenterDetached(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getTag()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    if-ne p1, v1, :cond_1

    iput-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    if-ne p1, v1, :cond_2

    iput-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContextHeaderPresenter:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    if-ne p1, v1, :cond_3

    iput-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContextHeaderPresenter:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    if-ne p1, v1, :cond_0

    iput-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x4

    if-ne p1, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->onBackPressed()Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    return v2

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->goBack()Z

    move-result v2

    goto :goto_1

    :cond_2
    const/16 v4, 0x54

    if-ne p1, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->enterQueryEditMode()V

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v4

    int-to-char v0, v4

    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->startQueryEdit()V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/Query;->withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_1
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldGoBackOnPreImeBackPress(Lcom/google/android/velvet/presenter/UiMode;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->goBack()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMainViewClicked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldStopQueryEditOnMainViewClick(Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/QueryState;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->stopQueryEdit()V

    :cond_0
    return-void
.end method

.method public onMainViewTouched()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->isViewAndEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->hideKeyboard()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->resetSearchPlate()V

    :cond_0
    return-void
.end method

.method public onMenuButtonClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/VelvetUi;->showOptionsMenu(Landroid/view/View;)V

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/googlequicksearchbox/DebugIntent;->shouldSkipSetup(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/googlequicksearchbox/DebugIntent;->handleIntent(Landroid/app/Activity;Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mRestoredInstance:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0, v1, v1}, Lcom/google/android/velvet/presenter/VelvetUi;->setFragments(Lcom/google/android/velvet/ui/VelvetFragment;Lcom/google/android/velvet/ui/VelvetFragment;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->clearIntent()V

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setSourceParams(Landroid/content/Intent;)V

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setupFromIntent(Landroid/content/Intent;Z)V

    iput-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNewIntent:Landroid/content/Intent;

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/QueryState;->removeObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mResumed:Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->cancelModeChangeTasks()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->closeOptionsMenu()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->hasGogglesInQueryStack()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/TraceTracker;->closeAllSessions()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->scheduleSendEvents(Landroid/content/Context;)V

    :cond_1
    return-void
.end method

.method public onPostCreate()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    new-instance v3, Lcom/google/android/velvet/presenter/VelvetPresenter$BrowserDimensionsSupplier;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/velvet/presenter/VelvetPresenter$BrowserDimensionsSupplier;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/VelvetPresenter$1;)V

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->setBrowserDimensionsSupplier(Lcom/google/common/base/Supplier;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorporaObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v3}, Lcom/google/android/velvet/Corpora;->registerObserver(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    if-eqz v0, :cond_3

    :goto_3
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->maybeBindWebView()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateCorpora()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateMode(Lcom/google/android/velvet/presenter/UiMode;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method public onQueryStateChanged()V
    .locals 5

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->shouldSuppressCorpora()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateFooterStickiness(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->takeNewlyCommittedWebQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->maybeInsertQueryIntoLocalHistory(Lcom/google/android/velvet/Query;)V

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0d039d

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getString(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->doABarrelRoll()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->maybeRefreshSearchHistory()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateHintText()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateSpinnerState()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSearchPlateStickiness()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/velvet/presenter/VelvetUi;->setSearchPlateStickiness(IZZ)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->setQuery(Lcom/google/android/velvet/Query;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->usesSuggestions()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->updateSuggestionsBuffered(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isReady(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/FooterPresenter;->setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isMarinerEnabled()Z

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldShowTgFooterButton(Lcom/google/android/velvet/presenter/UiMode;ZZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/FooterPresenter;->setShowTgFooterButton(Z)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getModeToSwitchTo()Lcom/google/android/velvet/presenter/UiMode;

    move-result-object v0

    if-eqz v0, :cond_5

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    if-eq v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    if-eq v0, v1, :cond_5

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->enterMode(Lcom/google/android/velvet/presenter/UiMode;)Z

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFocused:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateKeyboard()V

    :cond_4
    return-void

    :cond_5
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateSuggestFragment()V

    goto :goto_0
.end method

.method public onQueryTextChanged(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/Query;->withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    return-void
.end method

.method public onRestart()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->updateSuggestionsNow()V

    return-void
.end method

.method public onResume()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mResumed:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->updateSuggestionsNow()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mPendingMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNotifyFragmentsAndChangeModeTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->logCurrentMode()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiModeManager;->isStartupComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/velvet/VelvetBackgroundTasks;->notifyUiLaunched()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/QueryState;->addObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLogIdleTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeOnIdle(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/QueryState;->saveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onSearchBoxTouched()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->startEditingQuery()V

    return-void
.end method

.method public onSearchPhoneClickedInSuggest()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v2}, Lcom/google/android/velvet/Corpora;->getSummonsCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/Query;->withCorpus(Lcom/google/android/velvet/Corpus;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    return-void
.end method

.method public onStart()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationOracleLock:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    const-string v2, "search ui"

    invoke-interface {v0, v2}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->newRunningLock(Ljava/lang/String;)Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationOracleLock:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationOracleLock:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getLocationExpiryTimeSeconds()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->requestRecentLocation(J)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->usesSuggestions()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setSuggestionsActive(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsController:Lcom/google/android/searchcommon/suggest/SuggestionsController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->start()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNewIntent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNewIntent:Landroid/content/Intent;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mNewIntent:Landroid/content/Intent;

    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mRestoredInstance:Z

    if-nez v2, :cond_1

    invoke-direct {p0, v0, v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setupFromIntent(Landroid/content/Intent;Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;

    invoke-interface {v0}, Lcom/google/android/velvet/ActivityLifecycleObserver;->onActivityStart()V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setSuggestionsActive(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsController:Lcom/google/android/searchcommon/suggest/SuggestionsController;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->stop()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-virtual {v0}, Lcom/google/android/velvet/Cookies;->stopSync()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationOracleLock:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationOracleLock:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLocationOracleLock:Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/HttpHelper;->scheduleCacheFlush()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mLifecycleObserver:Lcom/google/android/velvet/ActivityLifecycleObserver;

    invoke-interface {v0}, Lcom/google/android/velvet/ActivityLifecycleObserver;->onActivityStop()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->forceReportEditingQueryChanged()V

    :cond_2
    return-void
.end method

.method onTgFooterButtonPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isReady(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->onTgFooterButtonClicked()V

    :cond_0
    return-void
.end method

.method public onWebSuggestionsDismissed()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldSwitchToSummonsOnWebSuggestDismiss(Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/QueryState;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->startQueryEdit()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v1}, Lcom/google/android/velvet/Corpora;->getSummonsCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->selectCorpus(Lcom/google/android/velvet/Corpus;)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->goBack()Z

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFocused:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/QueryState;->setHotwordAllowed(Z)V

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mResumed:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->updateKeyboard()V

    :cond_0
    return-void
.end method

.method public openUrlInBrowser(Lcom/google/android/searchcommon/util/UriRequest;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/presenter/VelvetUi;->openUrlInBrowser(Lcom/google/android/searchcommon/util/UriRequest;)V

    return-void
.end method

.method setTgFooterButtonEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/FooterPresenter;->setTgFooterButtonEnabled(Z)V

    return-void
.end method

.method setTgFooterButtonText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mFooterPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/FooterPresenter;->setTgFooterButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showCardInDialog(Lcom/google/geo/sidekick/Sidekick$Entry;Z)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0, p1, p2}, Lcom/google/android/velvet/presenter/VelvetUi;->showCardInDialog(Lcom/google/geo/sidekick/Sidekick$Entry;Z)V

    return-void
.end method

.method public showTheGoogleCardList()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    sget-object v2, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE_CARD_LIST:Lcom/google/android/velvet/presenter/UiMode;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/Query;->sentinel(Lcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    return-void
.end method

.method startEditingQuery()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->cancelCardCountDownByUser()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->startQueryEdit()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mModeManager:Lcom/google/android/velvet/presenter/UiModeManager;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mCurrentMode:Lcom/google/android/velvet/presenter/UiMode;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldScrollToTopOnSearchBoxTouch(Lcom/google/android/velvet/presenter/UiMode;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->smoothScrollToY(I)V

    :cond_1
    return-void
.end method
