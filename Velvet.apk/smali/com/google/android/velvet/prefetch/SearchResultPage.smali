.class public interface abstract Lcom/google/android/velvet/prefetch/SearchResultPage;
.super Ljava/lang/Object;
.source "SearchResultPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;
    }
.end annotation


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
.end method

.method public abstract fetch()V
.end method

.method public abstract getFetchTimeMillis()J
.end method

.method public abstract getQuery()Lcom/google/android/velvet/Query;
.end method

.method public abstract getSpeechRequestId()Ljava/lang/String;
.end method

.method public abstract isComplete()Z
.end method

.method public abstract isFailed()Z
.end method

.method public abstract sendCardsToQueryStateAsync(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;)V
.end method

.method public abstract toWebResourceResponse()Landroid/webkit/WebResourceResponse;
.end method

.method public abstract updateQuery(Lcom/google/android/velvet/Query;)V
.end method

.method public abstract waitUntilHeadersAvailable(J)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method
