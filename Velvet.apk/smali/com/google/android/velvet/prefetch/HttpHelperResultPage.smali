.class public Lcom/google/android/velvet/prefetch/HttpHelperResultPage;
.super Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;
.source "HttpHelperResultPage.java"


# instance fields
.field private mCancelled:Z

.field private final mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

.field private final mHttpResponseSet:Ljava/util/concurrent/CountDownLatch;

.field private final mLock:Ljava/lang/Object;

.field private final mQuery:Lcom/google/android/velvet/Query;

.field private final mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mSpeechRequestId:Ljava/lang/String;

.field private final mUiThread:Ljava/util/concurrent/Executor;

.field private mUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/Query;JLcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # J
    .param p4    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p5    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p6    # Ljava/util/concurrent/Executor;
    .param p7    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, p2, p3}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;-><init>(J)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mLock:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/velvet/prefetch/CardAggregator;

    invoke-direct {v0}, Lcom/google/android/velvet/prefetch/CardAggregator;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponseSet:Ljava/util/concurrent/CountDownLatch;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/Query;

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mQuery:Lcom/google/android/velvet/Query;

    iput-object p5, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p4, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mUrl:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mCancelled:Z

    iput-object p6, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mUiThread:Ljava/util/concurrent/Executor;

    iput-object p7, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mSpeechRequestId:Ljava/lang/String;

    return-void
.end method

.method private createRequest()Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;
    .locals 5

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mQuery:Lcom/google/android/velvet/Query;

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mSpeechRequestId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchRequest(Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/util/UriRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object v2
.end method


# virtual methods
.method public cancel()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mCancelled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mCancelled:Z

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;->cancel()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "HttpHelperResultPage:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v7, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string v1, "mQuery: "

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mQuery:Lcom/google/android/velvet/Query;

    aput-object v1, v0, v6

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    new-array v0, v7, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string v1, "mFetchTimeMillis: "

    aput-object v1, v0, v4

    invoke-virtual {p0}, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->getFetchTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    new-array v0, v7, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string v1, "mSpeechRequestId: "

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mSpeechRequestId:Ljava/lang/String;

    aput-object v1, v0, v6

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    new-array v0, v7, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string v1, "mCards: "

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

    aput-object v1, v0, v6

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/Object;

    aput-object p1, v1, v3

    const-string v0, "mHttpResponse is "

    aput-object v0, v1, v4

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponseSet:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    const-string v0, "not "

    :goto_0
    aput-object v0, v1, v6

    const-string v0, "set"

    aput-object v0, v1, v7

    invoke-static {p2, v1}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x3

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    const/4 v2, 0x1

    const-string v3, "mUrl: "

    aput-object v3, v0, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mUrl:Ljava/lang/String;

    aput-object v3, v0, v2

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    const/4 v2, 0x1

    const-string v3, "mHttpResponse: "

    aput-object v3, v0, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    aput-object v3, v0, v2

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    const/4 v2, 0x1

    const-string v3, "mCancelled: "

    aput-object v3, v0, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mCancelled:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public fetch()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->cancel()V

    :goto_0
    return-void

    :cond_0
    const-string v3, "Velvet.HttpHelperResultPage"

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->isPrefetch()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Prefetching search result page"

    :goto_1
    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->createRequest()Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mCancelled:Z

    if-nez v2, :cond_4

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->getUrl()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->isPrefetch()Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v1, 0xa

    :goto_2
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v2, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->shouldRequestPelletResponse(Lcom/google/android/velvet/Query;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

    iget-object v5, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mSrpMetadataConsumer:Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;

    invoke-interface {v2, v0, v1, v4, v5}, Lcom/google/android/searchcommon/util/HttpHelper;->getAsync(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;ILcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    :goto_3
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponseSet:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_1
    const-string v2, "Loading search result page"

    goto :goto_1

    :cond_2
    const/16 v1, 0xb

    goto :goto_2

    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    invoke-interface {v2, v0, v1}, Lcom/google/android/searchcommon/util/HttpHelper;->getAsync(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method public getQuery()Lcom/google/android/velvet/Query;
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mQuery:Lcom/google/android/velvet/Query;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSpeechRequestId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mSpeechRequestId:Ljava/lang/String;

    return-object v0
.end method

.method public isComplete()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isFailed()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;->isFailed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendCardsToQueryStateAsync(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mUiThread:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/velvet/prefetch/CardAggregator;->sendToQueryStateAsync(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    if-nez v1, :cond_0

    const-string v0, "not started"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PrefetchedResultPage:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;->isComplete()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "complete"

    goto :goto_0

    :cond_1
    const-string v0, "not complete"

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public toWebResourceResponse()Landroid/webkit/WebResourceResponse;
    .locals 8

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget-object v5, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    if-nez v5, :cond_0

    monitor-exit v4

    :goto_0
    return-object v3

    :cond_0
    iget-object v5, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    invoke-interface {v5}, Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;->reset()V

    iget-object v5, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    invoke-interface {v5}, Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;->getContentTypeIfAvailable()[Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_1
    const/4 v5, 0x0

    :try_start_1
    aget-object v2, v1, v5

    const/4 v5, 0x1

    aget-object v0, v1, v5

    if-eqz v2, :cond_2

    if-nez v1, :cond_3

    :cond_2
    const-string v5, "Velvet.HttpHelperResultPage"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bad content-type information: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v4

    goto :goto_0

    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->maybeLogEventId()V

    new-instance v3, Landroid/webkit/WebResourceResponse;

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v2, v0, v4}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method public updateQuery(Lcom/google/android/velvet/Query;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "#updateQuery unsupported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public waitUntilHeadersAvailable(J)Ljava/util/Map;
    .locals 3
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponseSet:Ljava/util/concurrent/CountDownLatch;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p1, p2, v2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;->getHeaders()Lcom/google/android/searchcommon/util/NowOrLater;

    move-result-object v1

    new-instance v2, Lcom/google/android/velvet/prefetch/HttpHelperResultPage$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/velvet/prefetch/HttpHelperResultPage$1;-><init>(Lcom/google/android/velvet/prefetch/HttpHelperResultPage;Ljava/util/concurrent/CountDownLatch;)V

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/NowOrLater;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;->mHttpResponse:Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;->getHeaders()Lcom/google/android/searchcommon/util/NowOrLater;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/NowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
