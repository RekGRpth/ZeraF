.class public Lcom/google/android/velvet/prefetch/S3ResultPage;
.super Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;
.source "S3ResultPage.java"


# static fields
.field private static final CANCELLED:Lcom/google/android/speech/exception/RecognizeException;


# instance fields
.field private final mBufferingExecutor:Ljava/util/concurrent/ExecutorService;

.field private mCancelled:Z

.field private final mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

.field private mComplete:Z

.field private mFailed:Z

.field private final mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

.field private mQuery:Lcom/google/android/velvet/Query;

.field private mResponseInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

.field private mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

.field private final mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mUiThread:Ljava/util/concurrent/Executor;

.field private mWebResourceContentStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/speech/exception/ResponseRecognizeException;

    const-string v1, "cancelled"

    invoke-direct {v0, v1}, Lcom/google/android/speech/exception/ResponseRecognizeException;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/velvet/prefetch/S3ResultPage;->CANCELLED:Lcom/google/android/speech/exception/RecognizeException;

    return-void
.end method

.method public constructor <init>(JLjava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 7
    .param p1    # J
    .param p3    # Ljava/util/concurrent/ExecutorService;
    .param p4    # Ljava/util/concurrent/Executor;
    .param p5    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    new-instance v5, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-direct {v5}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;-><init>()V

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/prefetch/S3ResultPage;-><init>(JLjava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;Lcom/google/android/velvet/prefetch/S3HeaderProcessor;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    return-void
.end method

.method constructor <init>(JLjava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;Lcom/google/android/velvet/prefetch/S3HeaderProcessor;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 1
    .param p1    # J
    .param p3    # Ljava/util/concurrent/ExecutorService;
    .param p4    # Ljava/util/concurrent/Executor;
    .param p5    # Lcom/google/android/velvet/prefetch/S3HeaderProcessor;
    .param p6    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;-><init>(J)V

    iput-object p3, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mBufferingExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mUiThread:Ljava/util/concurrent/Executor;

    iput-object p5, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    iput-object p6, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    new-instance v0, Lcom/google/android/velvet/prefetch/CardAggregator;

    invoke-direct {v0}, Lcom/google/android/velvet/prefetch/CardAggregator;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

    return-void
.end method

.method private maybeInitializeResponseStreamLocked()V
    .locals 9

    const/4 v8, 0x1

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v1}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->getMimeTypeAndCharset()[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    const/4 v1, 0x1

    :try_start_0
    aget-object v1, v6, v1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/prefetch/S3ResultPage;->newResponseProducer(Ljava/lang/String;)Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    invoke-static {v1}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->newStream(Lcom/google/android/searchcommon/util/ChunkProducer;)Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    new-instance v0, Lcom/google/android/searchcommon/google/PelletChunkProducer;

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mBufferingExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mSrpMetadataConsumer:Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;

    iget-object v5, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSingleRequestBaseUrl()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/google/PelletChunkProducer;-><init>(Ljava/io/InputStream;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->newStream(Lcom/google/android/searchcommon/util/ChunkProducer;)Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mWebResourceContentStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;
    :try_end_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v7

    const-string v1, "Velvet.S3ResultPage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported charset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v6, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    iput-boolean v8, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mFailed:Z

    goto :goto_0

    :cond_0
    const-string v1, "Velvet.S3ResultPage"

    const-string v2, "Missing / unparsable content type HTTP header"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v8, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mFailed:Z

    goto :goto_0
.end method

.method private declared-synchronized shouldRejectMutations()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mCancelled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mComplete:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mFailed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/velvet/prefetch/S3ResultPage;->shouldRejectMutations()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mCancelled:Z

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->isMarkedReady()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    sget-object v1, Lcom/google/android/velvet/prefetch/S3ResultPage;->CANCELLED:Lcom/google/android/speech/exception/RecognizeException;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->reportError(Lcom/google/android/speech/exception/RecognizeException;)V

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->markHeadersReady()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->markComplete()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "S3ResultPage:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v6, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string v1, "mFetchTimeMs : "

    aput-object v1, v0, v4

    invoke-virtual {p0}, Lcom/google/android/velvet/prefetch/S3ResultPage;->getFetchTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    new-array v0, v6, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string v1, "mQuery: "

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mQuery:Lcom/google/android/velvet/Query;

    aput-object v1, v0, v5

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    new-array v0, v6, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string v1, "mCards: "

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

    aput-object v1, v0, v5

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    const-string v2, "mFailed: "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mFailed:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    const-string v2, "mCancelled: "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mCancelled:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    const-string v2, "mComplete: "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mComplete:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    const-string v2, "mResponseInputStream: "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    const-string v2, "mWebResourceContentStream: "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mWebResourceContentStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    const-string v2, "mHeaderProcessor: "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lcom/google/android/searchcommon/debug/DumpUtils;->println(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public fetch()V
    .locals 0

    return-void
.end method

.method public declared-synchronized getQuery()Lcom/google/android/velvet/Query;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mQuery:Lcom/google/android/velvet/Query;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mQuery:Lcom/google/android/velvet/Query;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSpeechRequestId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method declared-synchronized isCancelled()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mCancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isComplete()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isFailed()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mFailed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected newResponseProducer(Ljava/lang/String;)Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public declared-synchronized offerPinholeResult(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V
    .locals 2
    .param p1    # Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/velvet/prefetch/S3ResultPage;->shouldRejectMutations()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsHeaderFragment()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsHeaderFragment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {p1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsHeaderFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->appendHeaderFragment(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsHeaderComplete()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->processHeaders()V

    invoke-direct {p0}, Lcom/google/android/velvet/prefetch/S3ResultPage;->maybeInitializeResponseStreamLocked()V

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->markHeadersReady()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/velvet/prefetch/S3ResultPage;->isFailed()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsBodyFragment()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsBodyFragment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    if-nez v0, :cond_5

    const-string v0, "Velvet.S3ResultPage"

    const-string v1, "Missing response producer. (Out of order message ?)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mFailed:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    invoke-virtual {p1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsBodyFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->offerChunk(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsResponseComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    if-nez v0, :cond_7

    const-string v0, "Velvet.S3ResultPage"

    const-string v1, "Missing response producer. (Out of order message ?)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mFailed:Z

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->markComplete()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mComplete:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized reportError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    monitor-enter p0

    :try_start_0
    const-string v0, "Velvet.S3ResultPage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed S3ResultPage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/speech/exception/RecognizeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/velvet/prefetch/S3ResultPage;->shouldRejectMutations()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mFailed:Z

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->isMarkedReady()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseProducer:Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->reportError(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->reportError(Lcom/google/android/speech/exception/RecognizeException;)V

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->markHeadersReady()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public sendCardsToQueryStateAsync(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mCards:Lcom/google/android/velvet/prefetch/CardAggregator;

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mUiThread:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/velvet/prefetch/CardAggregator;->sendToQueryStateAsync(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public declared-synchronized toWebResourceResponse()Landroid/webkit/WebResourceResponse;
    .locals 5

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mQuery:Lcom/google/android/velvet/Query;

    if-nez v2, :cond_1

    const-string v2, "Velvet.S3ResultPage"

    const-string v3, "Invalid query"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v2}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->isMarkedReady()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "Velvet.S3ResultPage"

    const-string v3, "No response headers received."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v2}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->getResponseCode()I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_3

    const-string v2, "Velvet.S3ResultPage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bad HTTP response, not OK: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v4}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->getResponseCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mResponseInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mWebResourceContentStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    if-nez v2, :cond_5

    :cond_4
    const-string v2, "Velvet.S3ResultPage"

    const-string v3, "No response body received."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v2}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->getMimeTypeAndCharset()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "application/json"

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    const-string v2, "text/html"

    aput-object v2, v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mWebResourceContentStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->reset()V

    invoke-virtual {p0}, Lcom/google/android/velvet/prefetch/S3ResultPage;->maybeLogEventId()V

    new-instance v1, Landroid/webkit/WebResourceResponse;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mWebResourceContentStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    invoke-direct {v1, v2, v3, v4}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized updateQuery(Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mQuery:Lcom/google/android/velvet/Query;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/Query;

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mQuery:Lcom/google/android/velvet/Query;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public waitUntilHeadersAvailable(J)Ljava/util/Map;
    .locals 1
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/S3ResultPage;->mHeaderProcessor:Lcom/google/android/velvet/prefetch/S3HeaderProcessor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/prefetch/S3HeaderProcessor;->waitForHeaders(J)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
