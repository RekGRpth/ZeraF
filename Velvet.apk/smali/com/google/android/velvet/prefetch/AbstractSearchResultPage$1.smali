.class Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$1;
.super Ljava/lang/Object;
.source "AbstractSearchResultPage.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$1;->this$0:Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;)Z
    .locals 3
    .param p1    # Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;

    const-string v1, "Velvet.AbstractSearchResultPage"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Showing SRP with event id: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_0

    const-string v0, "unknown"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;->mEventId:Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$1;->consume(Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;)Z

    move-result v0

    return v0
.end method
