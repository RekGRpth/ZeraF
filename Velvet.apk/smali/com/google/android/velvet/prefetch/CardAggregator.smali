.class public Lcom/google/android/velvet/prefetch/CardAggregator;
.super Lcom/google/android/searchcommon/util/CachedLater;
.source "CardAggregator.java"

# interfaces
.implements Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/CachedLater",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/velvet/ActionServerResult;",
        ">;>;",
        "Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;"
    }
.end annotation


# instance fields
.field private mCards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ActionServerResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/CachedLater;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/CardAggregator;->mCards:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/prefetch/CardAggregator;Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/prefetch/CardAggregator;
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;
    .param p3    # Ljava/util/concurrent/Executor;
    .param p4    # Ljava/util/List;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/prefetch/CardAggregator;->callQueryState(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;Ljava/util/List;)V

    return-void
.end method

.method private callQueryState(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;
    .param p3    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/velvet/presenter/QueryState;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ActionServerResult;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/velvet/prefetch/CardAggregator$2;

    invoke-direct {v0, p0, p2, p1, p4}, Lcom/google/android/velvet/prefetch/CardAggregator$2;-><init>(Lcom/google/android/velvet/prefetch/CardAggregator;Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;Ljava/util/List;)V

    invoke-interface {p3, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public consume(Lcom/google/android/velvet/ActionServerResult;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/ActionServerResult;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/CardAggregator;->mCards:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/prefetch/CardAggregator;->store(Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {v0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getUrlResponseCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/CardAggregator;->mCards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/CardAggregator;->mCards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/velvet/ActionServerResult;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/prefetch/CardAggregator;->consume(Lcom/google/android/velvet/ActionServerResult;)Z

    move-result v0

    return v0
.end method

.method public create()V
    .locals 0

    return-void
.end method

.method public noMoreCards()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/prefetch/CardAggregator;->consume(Lcom/google/android/velvet/ActionServerResult;)Z

    return-void
.end method

.method public sendToQueryStateAsync(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;
    .param p3    # Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/velvet/prefetch/CardAggregator$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/velvet/prefetch/CardAggregator$1;-><init>(Lcom/google/android/velvet/prefetch/CardAggregator;Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/prefetch/CardAggregator;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/16 v1, 0x2c

    invoke-static {v1}, Lcom/google/common/base/Joiner;->on(C)Lcom/google/common/base/Joiner;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CardAggregator["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/CardAggregator;->mCards:Ljava/util/List;

    invoke-virtual {v0, v2}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
