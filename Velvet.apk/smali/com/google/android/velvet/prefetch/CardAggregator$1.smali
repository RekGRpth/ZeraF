.class Lcom/google/android/velvet/prefetch/CardAggregator$1;
.super Ljava/lang/Object;
.source "CardAggregator.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/prefetch/CardAggregator;->sendToQueryStateAsync(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/velvet/ActionServerResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/prefetch/CardAggregator;

.field final synthetic val$query:Lcom/google/android/velvet/Query;

.field final synthetic val$queryState:Lcom/google/android/velvet/presenter/QueryState;

.field final synthetic val$uiThread:Ljava/util/concurrent/Executor;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/prefetch/CardAggregator;Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/prefetch/CardAggregator$1;->this$0:Lcom/google/android/velvet/prefetch/CardAggregator;

    iput-object p2, p0, Lcom/google/android/velvet/prefetch/CardAggregator$1;->val$query:Lcom/google/android/velvet/Query;

    iput-object p3, p0, Lcom/google/android/velvet/prefetch/CardAggregator$1;->val$queryState:Lcom/google/android/velvet/presenter/QueryState;

    iput-object p4, p0, Lcom/google/android/velvet/prefetch/CardAggregator$1;->val$uiThread:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/prefetch/CardAggregator$1;->consume(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public consume(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ActionServerResult;",
            ">;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/CardAggregator$1;->this$0:Lcom/google/android/velvet/prefetch/CardAggregator;

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/CardAggregator$1;->val$query:Lcom/google/android/velvet/Query;

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/CardAggregator$1;->val$queryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/CardAggregator$1;->val$uiThread:Ljava/util/concurrent/Executor;

    # invokes: Lcom/google/android/velvet/prefetch/CardAggregator;->callQueryState(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;Ljava/util/List;)V
    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/velvet/prefetch/CardAggregator;->access$000(Lcom/google/android/velvet/prefetch/CardAggregator;Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;Ljava/util/List;)V

    const/4 v0, 0x1

    return v0
.end method
