.class Lcom/google/android/velvet/prefetch/CardAggregator$2;
.super Ljava/lang/Object;
.source "CardAggregator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/prefetch/CardAggregator;->callQueryState(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/prefetch/CardAggregator;

.field final synthetic val$query:Lcom/google/android/velvet/Query;

.field final synthetic val$queryState:Lcom/google/android/velvet/presenter/QueryState;

.field final synthetic val$results:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/prefetch/CardAggregator;Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/prefetch/CardAggregator$2;->this$0:Lcom/google/android/velvet/prefetch/CardAggregator;

    iput-object p2, p0, Lcom/google/android/velvet/prefetch/CardAggregator$2;->val$queryState:Lcom/google/android/velvet/presenter/QueryState;

    iput-object p3, p0, Lcom/google/android/velvet/prefetch/CardAggregator$2;->val$query:Lcom/google/android/velvet/Query;

    iput-object p4, p0, Lcom/google/android/velvet/prefetch/CardAggregator$2;->val$results:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/CardAggregator$2;->val$queryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/CardAggregator$2;->val$query:Lcom/google/android/velvet/Query;

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/CardAggregator$2;->val$results:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/velvet/presenter/Action;->fromActionServerResults(Ljava/util/List;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    return-void
.end method
