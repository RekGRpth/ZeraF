.class public Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;
.super Ljava/lang/Object;
.source "WeatherCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/WeatherCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ForecastBuilder"
.end annotation


# instance fields
.field private mForecasts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/cards/WeatherCard$Forecast;",
            ">;"
        }
    .end annotation
.end field

.field private mLaterConditionChanges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/cards/WeatherCard$Forecast;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->mLocationName:Ljava/lang/String;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->mForecasts:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->mLaterConditionChanges:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addForecast(Ljava/lang/CharSequence;Landroid/net/Uri;II)Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/net/Uri;
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->mForecasts:Ljava/util/List;

    new-instance v1, Lcom/google/android/velvet/cards/WeatherCard$Forecast;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/velvet/cards/WeatherCard$Forecast;-><init>(Ljava/lang/CharSequence;Landroid/net/Uri;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addLaterConditionChange(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->mLaterConditionChanges:Ljava/util/List;

    new-instance v1, Lcom/google/android/velvet/cards/WeatherCard$Forecast;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/velvet/cards/WeatherCard$Forecast;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public create(Landroid/content/Context;)Lcom/google/android/velvet/cards/WeatherCard;
    .locals 1
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/google/android/velvet/cards/WeatherCard;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/cards/WeatherCard;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->update(Lcom/google/android/velvet/cards/WeatherCard;)V

    return-object v0
.end method

.method public update(Lcom/google/android/velvet/cards/WeatherCard;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/cards/WeatherCard;

    iget-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->mLocationName:Ljava/lang/String;

    # invokes: Lcom/google/android/velvet/cards/WeatherCard;->setLocation(Ljava/lang/CharSequence;)V
    invoke-static {p1, v0}, Lcom/google/android/velvet/cards/WeatherCard;->access$100(Lcom/google/android/velvet/cards/WeatherCard;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->mForecasts:Ljava/util/List;

    # invokes: Lcom/google/android/velvet/cards/WeatherCard;->setWeekdayForecasts(Ljava/util/List;)V
    invoke-static {p1, v0}, Lcom/google/android/velvet/cards/WeatherCard;->access$200(Lcom/google/android/velvet/cards/WeatherCard;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;->mLaterConditionChanges:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/cards/WeatherCard;->setLaterConditionChanges(Ljava/util/List;)V

    return-void
.end method
