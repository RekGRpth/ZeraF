.class public Lcom/google/android/velvet/cards/SportsMatchCard;
.super Landroid/widget/LinearLayout;
.source "SportsMatchCard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;,
        Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;,
        Lcom/google/android/velvet/cards/SportsMatchCard$Builder;
    }
.end annotation


# instance fields
.field private mActionViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mDetailsSeparator:Landroid/view/View;

.field private mDetailsView:Landroid/view/View;

.field private mLeftLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

.field private mLeftName:Landroid/widget/TextView;

.field private mLeftScore:Landroid/widget/TextView;

.field private mRightLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

.field private mRightName:Landroid/widget/TextView;

.field private mRightScore:Landroid/widget/TextView;

.field private mStatusText:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/SportsMatchCard;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/SportsMatchCard;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/SportsMatchCard;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/cards/SportsMatchCard;Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/SportsMatchCard;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/cards/SportsMatchCard;->setMatchInfo(Ljava/lang/String;Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/cards/SportsMatchCard;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/SportsMatchCard;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/cards/SportsMatchCard;->setDetailsView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/velvet/cards/SportsMatchCard;Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/SportsMatchCard;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # Landroid/graphics/Rect;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/cards/SportsMatchCard;->setLeftContestant(Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/velvet/cards/SportsMatchCard;Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/SportsMatchCard;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # Landroid/graphics/Rect;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/cards/SportsMatchCard;->setRightContestant(Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V

    return-void
.end method

.method private static goneIfEmpty(Ljava/lang/CharSequence;)I
    .locals 1
    .param p0    # Ljava/lang/CharSequence;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init()V
    .locals 4

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->setOrientation(I)V

    const v1, 0x7f02000b

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/SportsMatchCard;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400bd

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v1, 0x7f10023f

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f1000d8

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f100241

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/ClippedWebImageView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mLeftLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

    const v1, 0x7f100243

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mLeftScore:Landroid/widget/TextView;

    const v1, 0x7f100242

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mLeftName:Landroid/widget/TextView;

    const v1, 0x7f100244

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/ClippedWebImageView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mRightLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

    const v1, 0x7f100246

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mRightScore:Landroid/widget/TextView;

    const v1, 0x7f100245

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mRightName:Landroid/widget/TextView;

    const v1, 0x7f100247

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsSeparator:Landroid/view/View;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mActionViews:Ljava/util/List;

    return-void
.end method

.method private setDetailsView(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const v4, 0x7f0c0066

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsView:Landroid/view/View;

    if-ne p1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsView:Landroid/view/View;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/SportsMatchCard;->removeView(Landroid/view/View;)V

    :cond_1
    iput-object p1, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsView:Landroid/view/View;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsSeparator:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsView:Landroid/view/View;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/velvet/cards/SportsMatchCard;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x1

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/SportsMatchCard;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    const v2, 0x7f0c006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mDetailsSeparator:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setLeftContestant(Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # Landroid/graphics/Rect;
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mLeftName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mLeftLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

    invoke-virtual {v0, p3}, Lcom/google/android/velvet/ui/ClippedWebImageView;->setClipRect(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mLeftLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/ui/ClippedWebImageView;->setImageUri(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mLeftLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/ClippedWebImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mLeftScore:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mLeftScore:Landroid/widget/TextView;

    invoke-static {p4}, Lcom/google/android/velvet/cards/SportsMatchCard;->goneIfEmpty(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private setMatchInfo(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mTitle:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/velvet/cards/SportsMatchCard;->goneIfEmpty(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mStatusText:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/google/android/velvet/cards/SportsMatchCard;->goneIfEmpty(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private setRightContestant(Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # Landroid/graphics/Rect;
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mRightName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mRightLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

    invoke-virtual {v0, p3}, Lcom/google/android/velvet/ui/ClippedWebImageView;->setClipRect(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mRightLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/ui/ClippedWebImageView;->setImageUri(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mRightLogo:Lcom/google/android/velvet/ui/ClippedWebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/ClippedWebImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mRightScore:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mRightScore:Landroid/widget/TextView;

    invoke-static {p4}, Lcom/google/android/velvet/cards/SportsMatchCard;->goneIfEmpty(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public addAction(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/view/View$OnClickListener;

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/SportsMatchCard;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040010

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v3, 0x7f10004c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/cards/SportsMatchCard;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mActionViews:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearActions()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mActionViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/SportsMatchCard;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard;->mActionViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    return-void
.end method
