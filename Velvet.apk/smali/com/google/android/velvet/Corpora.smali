.class public Lcom/google/android/velvet/Corpora;
.super Landroid/database/DataSetObservable;
.source "Corpora.java"


# instance fields
.field private final mAppVersion:I

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;

.field private mInitialized:Z

.field private final mLoader:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<[B>;"
        }
    .end annotation
.end field

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mSubCorpora:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/velvet/Corpus;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/velvet/Corpus;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mTopLevelCorpora:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/Corpus;",
            ">;"
        }
    .end annotation
.end field

.field private final mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private mWebCorporaConfig:Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;
    .param p5    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p6    # Ljava/util/concurrent/Executor;
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/searchcommon/SearchConfig;",
            "Lcom/google/android/searchcommon/SearchSettings;",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<[B>;",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            "Ljava/util/concurrent/Executor;",
            "I)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object p2, p0, Lcom/google/android/velvet/Corpora;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/velvet/Corpora;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p4, p0, Lcom/google/android/velvet/Corpora;->mLoader:Lcom/google/android/searchcommon/util/UriLoader;

    iput-object p5, p0, Lcom/google/android/velvet/Corpora;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p6, p0, Lcom/google/android/velvet/Corpora;->mBgExecutor:Ljava/util/concurrent/Executor;

    iput p7, p0, Lcom/google/android/velvet/Corpora;->mAppVersion:I

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Corpora;->mTopLevelCorpora:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Corpora;->mSubCorpora:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/android/velvet/Corpora;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/Corpora;)Lcom/google/android/searchcommon/SearchSettings;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Corpora;

    iget-object v0, p0, Lcom/google/android/velvet/Corpora;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/Corpora;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/Corpora;

    invoke-direct {p0}, Lcom/google/android/velvet/Corpora;->loadDefaultCorpora()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/Corpora;Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/Corpora;
    .param p1    # Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/Corpora;->buildWebCorpora(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/velvet/Corpora;)Lcom/google/android/searchcommon/util/UriLoader;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Corpora;

    iget-object v0, p0, Lcom/google/android/velvet/Corpora;->mLoader:Lcom/google/android/searchcommon/util/UriLoader;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/Corpora;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Corpora;

    iget-object v0, p0, Lcom/google/android/velvet/Corpora;->mTopLevelCorpora:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/Corpora;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/velvet/Corpora;

    iget-object v0, p0, Lcom/google/android/velvet/Corpora;->mSubCorpora:Ljava/util/Map;

    return-object v0
.end method

.method private addCorpora(Lcom/google/android/velvet/Corpus;Ljava/util/LinkedHashMap;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Corpus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Corpus;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/velvet/Corpus;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/Corpora;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v1, Lcom/google/android/velvet/Corpora$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/velvet/Corpora$3;-><init>(Lcom/google/android/velvet/Corpora;Lcom/google/android/velvet/Corpus;Ljava/util/LinkedHashMap;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private buildWebCorpora(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;)V
    .locals 7
    .param p1    # Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;

    iget-object v5, p0, Lcom/google/android/velvet/Corpora;->mWebCorporaConfig:Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/velvet/Corpora;->mWebCorporaConfig:Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;

    invoke-virtual {v5, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    :cond_0
    iput-object p1, p0, Lcom/google/android/velvet/Corpora;->mWebCorporaConfig:Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;

    const/4 v3, 0x0

    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;->getCorpusList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/Corpora;->shouldUseCorpus(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v0, v3, p0}, Lcom/google/android/velvet/WebCorpus;->createWebCorpus(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;Lcom/google/android/velvet/WebCorpus;Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/WebCorpus;

    move-result-object v4

    if-nez v3, :cond_2

    move-object v3, v4

    invoke-virtual {v3}, Lcom/google/android/velvet/WebCorpus;->getIdentifier()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/velvet/WebCorpus;->getIdentifier()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-direct {p0, v3, v2}, Lcom/google/android/velvet/Corpora;->addCorpora(Lcom/google/android/velvet/Corpus;Ljava/util/LinkedHashMap;)V

    :cond_4
    return-void
.end method

.method private loadDefaultCorpora()V
    .locals 5

    :try_start_0
    iget-object v3, p0, Lcom/google/android/velvet/Corpora;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/SearchConfig;->getDefaultCorpora()[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;->parseFrom([B)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/Corpora;->buildWebCorpora(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v3, "Velvet.Corpora"

    const-string v4, "Unable to parse default corpora."

    invoke-static {v3, v4}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "Velvet.Corpora"

    const-string v4, "Default corpora not found."

    invoke-static {v3, v4}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private loadWebCorpora()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/Corpora;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/velvet/Corpora$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/Corpora$1;-><init>(Lcom/google/android/velvet/Corpora;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private maybeFetchNewCorpora()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/Corpora;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getCorporaConfigUri()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/Corpora;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getWebCorporaConfigUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/Corpora;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/velvet/Corpora$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/velvet/Corpora$2;-><init>(Lcom/google/android/velvet/Corpora;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private shouldUseCorpus(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;)Z
    .locals 4
    .param p1    # Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMinimumAppVersion()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getMinimumAppVersion()I

    move-result v1

    iget v3, p0, Lcom/google/android/velvet/Corpora;->mAppVersion:I

    if-ge v3, v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMaximumAppVersion()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getMaximumAppVersion()I

    move-result v0

    iget v3, p0, Lcom/google/android/velvet/Corpora;->mAppVersion:I

    if-gt v3, v0, :cond_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public areWebCorporaLoaded()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/Corpora;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCorpus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/Corpus;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/velvet/Corpora;->mTopLevelCorpora:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/Corpus;

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/velvet/PendingCorpus;

    invoke-direct {v1, p1, p2}, Lcom/google/android/velvet/PendingCorpus;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/velvet/Corpus;->getIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/Corpora;->mSubCorpora:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/Corpus;

    move-object v1, v2

    goto :goto_0
.end method

.method public getSubCorpora(Lcom/google/android/velvet/Corpus;)Ljava/lang/Iterable;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Corpus;",
            ")",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/android/velvet/Corpus;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/Corpora;->mSubCorpora:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getSummonsCorpus()Lcom/google/android/velvet/Corpus;
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/Corpora;->mTopLevelCorpora:Ljava/util/Map;

    const-string v1, "summons"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/Corpus;

    return-object v0
.end method

.method public getWebCorpus()Lcom/google/android/velvet/WebCorpus;
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/Corpora;->mTopLevelCorpora:Ljava/util/Map;

    const-string v1, "web"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/WebCorpus;

    return-object v0
.end method

.method public declared-synchronized init()V
    .locals 9

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/velvet/Corpora;->mInitialized:Z

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/velvet/Corpus;

    const-string v1, "summons"

    const-string v2, ""

    iget-object v3, p0, Lcom/google/android/velvet/Corpora;->mContext:Landroid/content/Context;

    const v4, 0x7f02010f

    invoke-static {v3, v4}, Lcom/google/android/searchcommon/util/Util;->getResourceUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/Corpora;->mContext:Landroid/content/Context;

    const v5, 0x7f0d032d

    invoke-static {v4, v5}, Lcom/google/android/searchcommon/util/Util;->getResourceUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v4

    const v5, 0x7f040023

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/velvet/Corpus;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;ILcom/google/android/velvet/Corpus;Lcom/google/android/velvet/Corpora;Ljava/util/Map;)V

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/android/velvet/Corpora;->addCorpora(Lcom/google/android/velvet/Corpus;Ljava/util/LinkedHashMap;)V

    invoke-direct {p0}, Lcom/google/android/velvet/Corpora;->loadWebCorpora()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/Corpora;->mInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public initializeDelayed()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/Corpora;->maybeFetchNewCorpora()V

    return-void
.end method

.method public waitForWebCorpus()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/velvet/Corpora;->mInitialized:Z

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/velvet/Corpora;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    const/4 v1, 0x0

    monitor-exit p0

    :goto_1
    return v1

    :cond_0
    const/4 v1, 0x1

    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
