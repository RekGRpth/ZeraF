.class Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;
.super Ljava/lang/Object;
.source "CrossfadingWebImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/CrossfadingWebImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrawableUpdater"
.end annotation


# instance fields
.field private final mPendingDrawable:Landroid/graphics/drawable/Drawable;

.field final synthetic this$0:Lcom/google/android/velvet/ui/CrossfadingWebImageView;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/ui/CrossfadingWebImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p2    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;->this$0:Lcom/google/android/velvet/ui/CrossfadingWebImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;->mPendingDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/ui/CrossfadingWebImageView;Landroid/graphics/drawable/Drawable;Lcom/google/android/velvet/ui/CrossfadingWebImageView$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/ui/CrossfadingWebImageView;
    .param p2    # Landroid/graphics/drawable/Drawable;
    .param p3    # Lcom/google/android/velvet/ui/CrossfadingWebImageView$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;-><init>(Lcom/google/android/velvet/ui/CrossfadingWebImageView;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;->this$0:Lcom/google/android/velvet/ui/CrossfadingWebImageView;

    iget-object v1, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;->mPendingDrawable:Landroid/graphics/drawable/Drawable;

    # invokes: Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->access$101(Lcom/google/android/velvet/ui/CrossfadingWebImageView;Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;->this$0:Lcom/google/android/velvet/ui/CrossfadingWebImageView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mDrawableUpdater:Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->access$202(Lcom/google/android/velvet/ui/CrossfadingWebImageView;Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;)Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

    return-void
.end method
