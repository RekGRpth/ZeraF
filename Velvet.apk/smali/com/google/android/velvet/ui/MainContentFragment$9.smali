.class Lcom/google/android/velvet/ui/MainContentFragment$9;
.super Ljava/lang/Object;
.source "MainContentFragment.java"

# interfaces
.implements Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/ui/MainContentFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/MainContentFragment;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/MainContentFragment$9;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewsDismissed(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$9;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$9;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->getPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/MainContentPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter;->onViewsDismissed(Ljava/lang/Iterable;)V

    :cond_0
    return-void
.end method
