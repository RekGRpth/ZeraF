.class Lcom/google/android/velvet/ui/ContextHeaderFragment$OnDoodleTouch;
.super Ljava/lang/Object;
.source "ContextHeaderFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/ContextHeaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OnDoodleTouch"
.end annotation


# instance fields
.field private final mDoodleAnchorLink:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment$OnDoodleTouch;->mDoodleAnchorLink:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v12, v1, Lcom/google/android/velvet/presenter/VelvetUi;

    if-eqz v12, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getBackFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v5

    instance-of v12, v5, Lcom/google/android/velvet/presenter/TgPresenter;

    if-eqz v12, :cond_0

    move-object v9, v5

    check-cast v9, Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v9}, Lcom/google/android/velvet/presenter/TgPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v12, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment$OnDoodleTouch;->mDoodleAnchorLink:Ljava/lang/String;

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v10

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v12

    invoke-virtual {v10, v12, v7}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;)Lcom/google/android/velvet/Query;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v4, v12, v13}, Lcom/google/android/velvet/Query;->fromPredictive(Ljava/lang/String;Landroid/location/Location;)Lcom/google/android/velvet/Query;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    :cond_0
    return-void
.end method
