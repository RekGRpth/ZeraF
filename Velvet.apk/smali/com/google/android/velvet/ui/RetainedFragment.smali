.class public Lcom/google/android/velvet/ui/RetainedFragment;
.super Landroid/app/Fragment;
.source "RetainedFragment.java"


# instance fields
.field private mInitialized:Z

.field private mSearchController:Lcom/google/android/velvet/presenter/SearchController;

.field private mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/RetainedFragment;->setRetainInstance(Z)V

    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/presenter/SearchController;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    return-void
.end method

.method public getSearchController()Lcom/google/android/velvet/presenter/SearchController;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mInitialized:Z

    if-nez v1, :cond_0

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetFactory;->createSearchController()Lcom/google/android/velvet/presenter/SearchController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mInitialized:Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    check-cast p1, Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-virtual {p1}, Lcom/google/android/velvet/ui/VelvetActivity;->getPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/SearchController;->onAttach(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->onSessionStart()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mInitialized:Z

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->onSessionStop()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->onDestroy()V

    const-string v0, "RetainedFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->onDetach()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->onResume()V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->onStop()V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Landroid/app/Fragment;->onTrimMemory(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragment;->mSearchController:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/SearchController;->onTrimMemory(I)V

    return-void
.end method
