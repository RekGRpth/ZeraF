.class Lcom/google/android/velvet/ui/MainContentFragment$8;
.super Ljava/lang/Object;
.source "MainContentFragment.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/MainContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/MainContentFragment;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/MainContentFragment$8;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$8;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/velvet/ui/MainContentFragment;->mAttachedToWindow:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/MainContentFragment;->access$1102(Lcom/google/android/velvet/ui/MainContentFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$8;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/velvet/ui/MainContentFragment;->maybePostCommitTransactions(Z)V
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/MainContentFragment;->access$300(Lcom/google/android/velvet/ui/MainContentFragment;Z)V

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$8;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/velvet/ui/MainContentFragment;->mAttachedToWindow:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/MainContentFragment;->access$1102(Lcom/google/android/velvet/ui/MainContentFragment;Z)Z

    return-void
.end method
