.class public Lcom/google/android/velvet/ui/CrossfadingWebImageView;
.super Lcom/google/android/velvet/ui/RoundedCornerWebImageView;
.source "CrossfadingWebImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/CrossfadingWebImageView$1;,
        Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;
    }
.end annotation


# instance fields
.field private mDrawableUpdater:Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

.field private final mFadeIfLoadedFromCache:Z

.field private final mTransitionTime:I

.field private final pressedHighlightColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/google/android/googlequicksearchbox/R$styleable;->CrossfadingWebImageView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v1, 0x12c

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mTransitionTime:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mFadeIfLoadedFromCache:Z

    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->pressedHighlightColor:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic access$101(Lcom/google/android/velvet/ui/CrossfadingWebImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/CrossfadingWebImageView;
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/velvet/ui/CrossfadingWebImageView;Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;)Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/CrossfadingWebImageView;
    .param p1    # Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

    iput-object p1, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mDrawableUpdater:Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

    return-object p1
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 8

    iget v6, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->pressedHighlightColor:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->getDrawableState()[I

    move-result-object v5

    const/4 v3, 0x0

    move-object v0, v5

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    aget v4, v0, v1

    const v6, 0x10100a7

    if-eq v4, v6, :cond_1

    const v6, 0x101009c

    if-ne v4, v6, :cond_3

    :cond_1
    const/4 v3, 0x1

    :cond_2
    if-eqz v3, :cond_4

    iget v6, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->pressedHighlightColor:I

    invoke-virtual {p0, v6}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setColorFilter(I)V

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 12
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mDrawableUpdater:Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mDrawableUpdater:Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

    invoke-interface {v6, v8}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iput-object v11, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mDrawableUpdater:Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->isLoadedFromCache()Z

    move-result v8

    if-eqz v8, :cond_1

    iget-boolean v8, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mFadeIfLoadedFromCache:Z

    if-nez v8, :cond_1

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    const/4 v3, 0x0

    :goto_1
    if-eqz v2, :cond_2

    instance-of v8, v2, Landroid/view/ViewGroup;

    if-eqz v8, :cond_3

    move-object v8, v2

    check-cast v8, Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v3, 0x1

    :cond_2
    if-eqz v3, :cond_4

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    goto :goto_1

    :cond_4
    if-nez p1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    if-nez v8, :cond_5

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_7

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    :cond_6
    :goto_2
    new-instance v5, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v8, 0x2

    new-array v8, v8, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v8, v9

    aput-object p1, v8, v10

    invoke-direct {v5, v8}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v5, v10}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    invoke-super {p0, v5}, Lcom/google/android/velvet/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget v8, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mTransitionTime:I

    invoke-virtual {v5, v8}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    new-instance v8, Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

    invoke-direct {v8, p0, p1, v11}, Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;-><init>(Lcom/google/android/velvet/ui/CrossfadingWebImageView;Landroid/graphics/drawable/Drawable;Lcom/google/android/velvet/ui/CrossfadingWebImageView$1;)V

    iput-object v8, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mDrawableUpdater:Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

    iget-object v8, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mDrawableUpdater:Lcom/google/android/velvet/ui/CrossfadingWebImageView$DrawableUpdater;

    iget v9, p0, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->mTransitionTime:I

    int-to-long v9, v9

    invoke-interface {v6, v8, v9, v10}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0

    :cond_7
    if-nez p1, :cond_6

    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p1, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_2
.end method
