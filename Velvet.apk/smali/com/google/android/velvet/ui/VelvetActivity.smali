.class public Lcom/google/android/velvet/ui/VelvetActivity;
.super Lcom/google/android/velvet/ui/SavedStateTrackingActivity;
.source "VelvetActivity.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/VelvetUi;


# static fields
.field private static sActivityCount:I


# instance fields
.field private mContextHeader:Landroid/view/View;

.field private mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

.field private mFactory:Lcom/google/android/velvet/VelvetFactory;

.field private mFadeOutSearchHeader:Z

.field private mFooter:Landroid/view/View;

.field private mFooterHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

.field private mIntentStarter:Lcom/google/android/searchcommon/util/ActivityIntentStarter;

.field private mIsAttachedToWindow:Z

.field private final mMainViewClickListener:Landroid/view/View$OnClickListener;

.field private mMenu:Landroid/widget/PopupMenu;

.field private final mOnFocusGainedTask:Ljava/lang/Runnable;

.field private mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

.field private final mResultsAreaSizeLock:Ljava/lang/Object;

.field private mResultsAreaSizePx:Landroid/graphics/Point;

.field private mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

.field private mSearchPlate:Landroid/view/View;

.field private mSearchPlateBgFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

.field private mSearchPlateHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

.field private mSearchPlateShield:Landroid/view/View;

.field private mSearchPlateShieldFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

.field private mSearchPlateStrongShield:Landroid/view/View;

.field private mSearchPlateStrongShieldFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

.field private mShowContextHeader:Z

.field private mTheGoogleLogo:Landroid/view/View;

.field private mTopLevelContainer:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

.field private mUiThread:Lcom/google/android/searchcommon/util/CancellableSingleThreadedExecutor;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;-><init>()V

    new-instance v0, Lcom/google/android/velvet/ui/VelvetActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/VelvetActivity$1;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mOnFocusGainedTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/ui/VelvetActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/VelvetActivity$2;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMainViewClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizeLock:Ljava/lang/Object;

    sget v0, Lcom/google/android/velvet/ui/VelvetActivity;->sActivityCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/velvet/ui/VelvetActivity;->sActivityCount:I

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->onStartupPoint(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/ui/VelvetActivity;)Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/VelvetActivity;

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/ui/VelvetActivity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/VelvetActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/VelvetActivity;->updateSearchPlateOffset(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/ui/VelvetActivity;ZZ)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/VelvetActivity;
    .param p1    # Z
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/ui/VelvetActivity;->updateStrongShieldFade(ZZ)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/velvet/ui/VelvetActivity;Landroid/widget/PopupMenu;)Landroid/widget/PopupMenu;
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/VelvetActivity;
    .param p1    # Landroid/widget/PopupMenu;

    iput-object p1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    return-object p1
.end method

.method private closeMenu()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    :cond_0
    return-void
.end method

.method private commitAndExecutePendingTransactions(Landroid/app/FragmentTransaction;)V
    .locals 3
    .param p1    # Landroid/app/FragmentTransaction;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->haveSavedState()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Velvet.VelvetActivity"

    const-string v2, "have saved state, may see UI issues later"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Velvet.VelvetActivity"

    const-string v2, "executePendingTransactions returned false?"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method private getMainContentFragment(I)Lcom/google/android/velvet/ui/MainContentFragment;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/velvet/ui/MainContentFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/velvet/ui/MainContentFragment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPresenterForFragment(I)Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/VelvetActivity;->getVelvetFragment(I)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/VelvetFragment;->getPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getRetainedFragment()Lcom/google/android/velvet/ui/RetainedFragment;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "RetainedFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/RetainedFragment;

    return-object v0
.end method

.method private getVelvetFragment(I)Lcom/google/android/velvet/ui/VelvetFragment;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/velvet/ui/VelvetFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/velvet/ui/VelvetFragment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeAddRetainedFragment()Lcom/google/android/velvet/ui/RetainedFragment;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "RetainedFragment"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/RetainedFragment;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/ui/RetainedFragment;

    invoke-direct {v1}, Lcom/google/android/velvet/ui/RetainedFragment;-><init>()V

    const-string v3, "RetainedFragment"

    invoke-virtual {v0, v1, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->commitAndExecutePendingTransactions(Landroid/app/FragmentTransaction;)V

    move-object v2, v1

    :goto_0
    return-object v2

    :cond_0
    move-object v2, v1

    goto :goto_0
.end method

.method private pxToDp(I)I
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    int-to-float v1, p1

    div-float/2addr v1, v0

    float-to-int v1, v1

    return v1
.end method

.method private setFragment(Landroid/app/FragmentTransaction;Lcom/google/android/velvet/ui/VelvetFragment;I)Z
    .locals 4
    .param p1    # Landroid/app/FragmentTransaction;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/FragmentTransaction;",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;I)Z"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    if-eq v0, p2, :cond_3

    if-nez p2, :cond_0

    invoke-virtual {p1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/velvet/ui/VelvetFragment;->isAdded()Z

    move-result v3

    if-nez v3, :cond_1

    move v1, v2

    :cond_1
    const-string v3, "Cannot move a fragment"

    invoke-static {v1, v3}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/velvet/ui/VelvetFragment;->getVelvetTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p3, p2, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/velvet/ui/VelvetFragment;->getVelvetTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p3, p2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_0
.end method

.method private updateSearchPlateFading()V
    .locals 5

    const/high16 v4, 0x3f800000

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mShowContextHeader:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFadeOutSearchHeader:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateBgFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeader:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeader:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    add-int/lit8 v2, v2, 0x32

    const v3, 0x3f70a3d7

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->setFadePoints(IIFF)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateBgFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    invoke-virtual {v0, v4}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->setFixedAlpha(F)V

    goto :goto_0
.end method

.method private updateSearchPlateOffset(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mShowContextHeader:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mTheGoogleLogo:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setOffsetFromEdge(IZ)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateShieldFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateShield:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->setFadePoints(II)V

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->updateSearchPlateFading()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateStrongShieldFade(ZZ)V
    .locals 4
    .param p1    # Z
    .param p2    # Z

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mShowContextHeader:Z

    if-eqz v1, :cond_2

    if-nez p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShield:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShieldFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->setFixedAlpha(F)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShield:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/google/android/velvet/ui/VelvetActivity$7;

    invoke-direct {v2, p0}, Lcom/google/android/velvet/ui/VelvetActivity$7;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShield:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v0

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShield:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShieldFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->setFadePoints(II)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShield:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/google/android/velvet/ui/VelvetActivity$8;

    invoke-direct {v2, p0}, Lcom/google/android/velvet/ui/VelvetActivity$8;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method


# virtual methods
.method public areFragmentsRunningLayoutTransitions()Z
    .locals 4

    const/4 v2, 0x1

    const v3, 0x7f100273

    invoke-direct {p0, v3}, Lcom/google/android/velvet/ui/VelvetActivity;->getVelvetFragment(I)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/velvet/ui/HasLayoutTransitions;

    if-eqz v3, :cond_1

    check-cast v0, Lcom/google/android/velvet/ui/HasLayoutTransitions;

    invoke-interface {v0}, Lcom/google/android/velvet/ui/HasLayoutTransitions;->isRunningLayoutTransition()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const v3, 0x7f100274

    invoke-direct {p0, v3}, Lcom/google/android/velvet/ui/VelvetActivity;->getVelvetFragment(I)Lcom/google/android/velvet/ui/VelvetFragment;

    move-result-object v1

    instance-of v3, v1, Lcom/google/android/velvet/ui/HasLayoutTransitions;

    if-eqz v3, :cond_2

    check-cast v1, Lcom/google/android/velvet/ui/HasLayoutTransitions;

    invoke-interface {v1}, Lcom/google/android/velvet/ui/HasLayoutTransitions;->isRunningLayoutTransition()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearIntent()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public doABarrelRoll()V
    .locals 3

    const v0, 0x7f100270

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x43b40000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotationBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/FileDescriptor;
    .param p3    # Ljava/io/PrintWriter;
    .param p4    # [Ljava/lang/String;

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {p0, p1, p3}, Lcom/google/android/velvet/ui/VelvetActivity;->dumpActivityState(Ljava/lang/String;Ljava/io/PrintWriter;)V

    return-void
.end method

.method public dumpActivityState(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getRetainedFragment()Lcom/google/android/velvet/ui/RetainedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/ui/RetainedFragment;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    :cond_0
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "VelvetActivity state:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "Current front fragment: "

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f100274

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Current back fragment: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f100273

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    return-void
.end method

.method public finalize()V
    .locals 1

    sget v0, Lcom/google/android/velvet/ui/VelvetActivity;->sActivityCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/velvet/ui/VelvetActivity;->sActivityCount:I

    return-void
.end method

.method public findFragmentByTag(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/VelvetFragment;

    return-object v0
.end method

.method public finish()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->finish()V

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 0

    return-object p0
.end method

.method public getBackFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .locals 1

    const v0, 0x7f100273

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->getPresenterForFragment(I)Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v0

    return-object v0
.end method

.method public getFrontFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .locals 1

    const v0, 0x7f100274

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->getPresenterForFragment(I)Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v0

    return-object v0
.end method

.method public getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mIntentStarter:Lcom/google/android/searchcommon/util/ActivityIntentStarter;

    return-object v0
.end method

.method getPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    return-object v0
.end method

.method public getResultsAreaSizeDp()Landroid/graphics/Point;
    .locals 4

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizeLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizePx:Landroid/graphics/Point;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/Point;

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizePx:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    invoke-direct {p0, v2}, Lcom/google/android/velvet/ui/VelvetActivity;->pxToDp(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizePx:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-direct {p0, v3}, Lcom/google/android/velvet/ui/VelvetActivity;->pxToDp(I)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getScrollingContainer()Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    return-object v0
.end method

.method public getSearchPlateHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlate:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public indicateRemoveFromHistoryFailed()V
    .locals 2

    const v0, 0x7f0d0364

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onActivityResult(IILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mIntentStarter:Lcom/google/android/searchcommon/util/ActivityIntentStarter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/searchcommon/util/ActivityIntentStarter;->onActivityResultDelegate(IILandroid/content/Intent;)V

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mIsAttachedToWindow:Z

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onAttachedToWindow()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v5, 0x4

    invoke-static {v5}, Lcom/google/android/voicesearch/logger/EventLogger;->recordLatencyStart(I)V

    const/16 v5, 0x12

    invoke-static {v5}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v5, p0}, Lcom/google/android/velvet/VelvetFactory;->createPresenter(Lcom/google/android/velvet/presenter/VelvetUi;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    new-instance v5, Lcom/google/android/searchcommon/util/ActivityIntentStarter;

    const/16 v6, 0x64

    invoke-direct {v5, p0, v6}, Lcom/google/android/searchcommon/util/ActivityIntentStarter;-><init>(Landroid/app/Activity;I)V

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mIntentStarter:Lcom/google/android/searchcommon/util/ActivityIntentStarter;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mUiThread:Lcom/google/android/searchcommon/util/CancellableSingleThreadedExecutor;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->maybeAddRetainedFragment()Lcom/google/android/velvet/ui/RetainedFragment;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/RetainedFragment;->getSearchController()Lcom/google/android/velvet/presenter/SearchController;

    move-result-object v6

    invoke-virtual {v5, p1, v6}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onCreate(Landroid/os/Bundle;Lcom/google/android/velvet/presenter/SearchController;)V

    const v5, 0x7f0400d0

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->setContentView(I)V

    const v5, 0x7f100270

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mTopLevelContainer:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mTopLevelContainer:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    new-instance v6, Lcom/google/android/velvet/ui/VelvetActivity$3;

    invoke-direct {v6, p0}, Lcom/google/android/velvet/ui/VelvetActivity$3;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mTopLevelContainer:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->setFragmentManager(Landroid/app/FragmentManager;)V

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mTopLevelContainer:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    new-instance v6, Lcom/google/android/velvet/ui/VelvetActivity$4;

    invoke-direct {v6, p0}, Lcom/google/android/velvet/ui/VelvetActivity$4;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->setPreImeKeyListener(Landroid/view/View$OnKeyListener;)V

    const v5, 0x7f100271

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    const v5, 0x7f100275

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeader:Landroid/view/View;

    new-instance v5, Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget-object v6, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeader:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;-><init>(Landroid/view/View;Lcom/google/android/velvet/ui/util/ScrollViewControl;Z)V

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v5, v10, v9, v8}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setStickiness(IZZ)V

    const v5, 0x7f100076

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mTheGoogleLogo:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeader:Landroid/view/View;

    new-instance v6, Lcom/google/android/velvet/ui/VelvetActivity$5;

    invoke-direct {v6, p0}, Lcom/google/android/velvet/ui/VelvetActivity$5;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    const v5, 0x7f100274

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMainViewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v5, 0x7f100273

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMainViewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v5, 0x7f100276

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlate:Landroid/view/View;

    new-instance v5, Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget-object v6, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlate:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;-><init>(Landroid/view/View;Lcom/google/android/velvet/ui/util/ScrollViewControl;Z)V

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    const v5, 0x7f100232

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    new-instance v5, Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    iget-object v6, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-direct {v5, v4, v6}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;-><init>(Landroid/view/View;Lcom/google/android/velvet/ui/util/ScrollViewControl;)V

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateBgFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateBgFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    invoke-virtual {v5, v8}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->setFadeBackgroundOnly(Z)V

    const v5, 0x7f100277

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFooter:Landroid/view/View;

    new-instance v5, Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget-object v6, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFooter:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-direct {v5, v6, v7, v9}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;-><init>(Landroid/view/View;Lcom/google/android/velvet/ui/util/ScrollViewControl;Z)V

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFooterHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFooterHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget-object v6, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setForceShowOrHideOnScrollFinishedDelegate(Lcom/google/android/velvet/ui/util/OnScrollViewHider;)V

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFooterHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget-object v6, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setRevealAtScrollEndDelegate(Lcom/google/android/velvet/ui/util/OnScrollViewHider;)V

    const v5, 0x7f100231

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateShield:Landroid/view/View;

    new-instance v5, Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    iget-object v6, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateShield:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-direct {v5, v6, v7}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;-><init>(Landroid/view/View;Lcom/google/android/velvet/ui/util/ScrollViewControl;)V

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateShieldFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    const v5, 0x7f100230

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShield:Landroid/view/View;

    new-instance v5, Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    iget-object v6, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShield:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-direct {v5, v6, v7}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;-><init>(Landroid/view/View;Lcom/google/android/velvet/ui/util/ScrollViewControl;)V

    iput-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateStrongShieldFader:Lcom/google/android/velvet/ui/util/OnScrollViewFader;

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    new-instance v6, Lcom/google/android/velvet/ui/VelvetActivity$6;

    invoke-direct {v6, p0}, Lcom/google/android/velvet/ui/VelvetActivity$6;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setInterceptedTouchEventListener(Landroid/view/View$OnTouchListener;)V

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iget-object v6, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMainViewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onPostCreate()V

    const/16 v5, 0x13

    invoke-static {v5}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    invoke-static {v10}, Lcom/google/android/velvet/VelvetStrictMode;->onStartupPoint(I)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onCreateOptionsMenu(Landroid/view/Menu;)V

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mUiThread:Lcom/google/android/searchcommon/util/CancellableSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mOnFocusGainedTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/CancellableSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onDestroy()V

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onDestroy()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mIsAttachedToWindow:Z

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onDetachedFromWindow()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onNewIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->closeMenu()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onPause()V

    invoke-static {}, Lcom/google/android/voicesearch/logger/EventLogger;->resetOneOff()V

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onPause()V

    return-void
.end method

.method public onRestart()V
    .locals 1

    const/4 v0, 0x7

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordLatencyStart(I)V

    const/16 v0, 0x1b

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onRestart()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onRestart()V

    return-void
.end method

.method public onResume()V
    .locals 1

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordLatencyStart(I)V

    const/16 v0, 0x17

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onResume()V

    const/16 v0, 0x18

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->onStartupPoint(I)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onStart()V

    const/16 v0, 0x16

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordBreakdownEvent(I)V

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->onStartupPoint(I)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onStop()V

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onStop()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->onWindowFocusChanged(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mUiThread:Lcom/google/android/searchcommon/util/CancellableSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mOnFocusGainedTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/CancellableSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mUiThread:Lcom/google/android/searchcommon/util/CancellableSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mOnFocusGainedTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/CancellableSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onWindowFocusChanged(Z)V

    goto :goto_0
.end method

.method public openUrlInBrowser(Lcom/google/android/searchcommon/util/UriRequest;)V
    .locals 9
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;

    new-instance v4, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lcom/google/android/searchcommon/util/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v6, 0x10000000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/util/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v6

    if-lez v6, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v6, "com.android.browser.headers"

    invoke-virtual {v4, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_1
    :try_start_0
    invoke-virtual {p0, v4}, Lcom/google/android/velvet/ui/VelvetActivity;->startActivity(Landroid/content/Intent;)V

    const v6, 0x7f050001

    const v7, 0x7f050002

    invoke-virtual {p0, v6, v7}, Lcom/google/android/velvet/ui/VelvetActivity;->overridePendingTransition(II)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v1

    const-string v6, "Velvet.VelvetActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No activity found to open: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/searchcommon/util/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v6, 0x7f0d0370

    const/4 v7, 0x0

    invoke-static {p0, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public setFadeSearchPlateOverHeader(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFadeOutSearchHeader:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFadeOutSearchHeader:Z

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->updateSearchPlateFading()V

    :cond_0
    return-void
.end method

.method public setFooterPositionLocked(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFooterHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setLocked(Z)V

    return-void
.end method

.method public setFooterStickiness(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mTopLevelContainer:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {v2, v1, v0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->setIncludeFooterPadding(ZZ)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFooterHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setStickiness(IZZ)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFragments(Lcom/google/android/velvet/ui/VelvetFragment;Lcom/google/android/velvet/ui/VelvetFragment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f100274

    invoke-direct {p0, v1, p1, v2}, Lcom/google/android/velvet/ui/VelvetActivity;->setFragment(Landroid/app/FragmentTransaction;Lcom/google/android/velvet/ui/VelvetFragment;I)Z

    move-result v0

    const v2, 0x7f100273

    invoke-direct {p0, v1, p2, v2}, Lcom/google/android/velvet/ui/VelvetActivity;->setFragment(Landroid/app/FragmentTransaction;Lcom/google/android/velvet/ui/VelvetFragment;I)Z

    move-result v2

    or-int/2addr v0, v2

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/velvet/ui/VelvetActivity;->commitAndExecutePendingTransactions(Landroid/app/FragmentTransaction;)V

    :cond_0
    return-void
.end method

.method setResultsAreaSizePx(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizeLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizePx:Landroid/graphics/Point;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizePx:Landroid/graphics/Point;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizePx:Landroid/graphics/Point;

    iput p1, v0, Landroid/graphics/Point;->x:I

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mResultsAreaSizePx:Landroid/graphics/Point;

    iput p2, v0, Landroid/graphics/Point;->y:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSearchPlateStickiness(IZZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setStickiness(IZZ)V

    return-void
.end method

.method public setShowContextHeader(ZZ)V
    .locals 7
    .param p1    # Z
    .param p2    # Z

    const/16 v6, 0x12c

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mShowContextHeader:Z

    if-eq p1, v2, :cond_1

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mShowContextHeader:Z

    const/4 v1, 0x0

    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget-object v3, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mTheGoogleLogo:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setPartialHide(I)V

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v2, v5}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setFadeWithTranslation(Z)V

    const v2, 0x7f100273

    invoke-direct {p0, v2}, Lcom/google/android/velvet/ui/VelvetActivity;->getMainContentFragment(I)Lcom/google/android/velvet/ui/MainContentFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->isRunningDisappearTransitions()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setAnimationStartDelay(I)V

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setAnimationStartDelay(I)V

    const/4 v1, 0x1

    :cond_0
    invoke-direct {p0, p2, v1}, Lcom/google/android/velvet/ui/VelvetActivity;->updateStrongShieldFade(ZZ)V

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mTopLevelContainer:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    iget-boolean v3, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mShowContextHeader:Z

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->setContextHeaderShown(Z)V

    iget-object v3, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    if-eqz p1, :cond_2

    const/4 v2, 0x2

    :goto_0
    invoke-virtual {v3, v2, p2, v5}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setStickiness(IZZ)V

    invoke-direct {p0, p2}, Lcom/google/android/velvet/ui/VelvetActivity;->updateSearchPlateOffset(Z)V

    if-nez p2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v2, v4}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setAnimationStartDelay(I)V

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mSearchPlateHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v2, v4}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setAnimationStartDelay(I)V

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v2, v4}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setPartialHide(I)V

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mContextHeaderHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v2, v4}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->setFadeWithTranslation(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v2, 0x3

    goto :goto_0
.end method

.method public showCardInDialog(Lcom/google/geo/sidekick/Sidekick$Entry;Z)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Z

    invoke-static {p1, p2}, Lcom/google/android/velvet/ui/CardDialogFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Z)Lcom/google/android/velvet/ui/CardDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/ui/CardDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public showDebugDialog(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/google/android/searchcommon/debug/DebugDialogFragment;

    invoke-direct {v0}, Lcom/google/android/searchcommon/debug/DebugDialogFragment;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/debug/DebugDialogFragment;->setText(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "debug_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/debug/DebugDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public showFooter()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFooterHider:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->show()V

    return-void
.end method

.method public showOptionsMenu(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    new-instance v0, Landroid/widget/PopupMenu;

    invoke-direct {v0, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/velvet/ui/VelvetActivity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    new-instance v1, Lcom/google/android/velvet/ui/VelvetActivity$9;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/VelvetActivity$9;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    new-instance v1, Lcom/google/android/velvet/ui/VelvetActivity$10;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/VelvetActivity$10;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method
