.class Lcom/google/android/velvet/ui/MainContentFragment$1;
.super Ljava/lang/Object;
.source "MainContentFragment.java"

# interfaces
.implements Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/MainContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/MainContentFragment;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/MainContentFragment$1;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollAnimationFinished()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$1;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/velvet/ui/MainContentFragment;->maybePostCommitTransactions(Z)V
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/MainContentFragment;->access$300(Lcom/google/android/velvet/ui/MainContentFragment;Z)V

    return-void
.end method

.method public onScrollChanged(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$1;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    # getter for: Lcom/google/android/velvet/ui/MainContentFragment;->mNotifyScrollPosted:Z
    invoke-static {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->access$000(Lcom/google/android/velvet/ui/MainContentFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$1;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/velvet/ui/MainContentFragment;->mNotifyScrollPosted:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/MainContentFragment;->access$002(Lcom/google/android/velvet/ui/MainContentFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$1;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    # getter for: Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->access$200(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment$1;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    # getter for: Lcom/google/android/velvet/ui/MainContentFragment;->mNotifyScrolledRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/velvet/ui/MainContentFragment;->access$100(Lcom/google/android/velvet/ui/MainContentFragment;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    :cond_0
    return-void
.end method

.method public onScrollFinished()V
    .locals 0

    return-void
.end method

.method public onScrollMarginConsumed(Landroid/view/View;II)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    return-void
.end method
