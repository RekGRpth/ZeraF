.class public Lcom/google/android/velvet/ui/ContextHeaderFragment;
.super Lcom/google/android/velvet/ui/VelvetFragment;
.source "ContextHeaderFragment.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/ContextHeaderUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/ContextHeaderFragment$OnDoodleTouch;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/velvet/ui/VelvetFragment",
        "<",
        "Lcom/google/android/velvet/presenter/ContextHeaderPresenter;",
        ">;",
        "Lcom/google/android/velvet/presenter/ContextHeaderUi;"
    }
.end annotation


# instance fields
.field private mContextImage:Lcom/google/android/velvet/ui/WebImageView;

.field private mGoogleLogo:Landroid/widget/ImageView;

.field private mGoogleLogoVisibility:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v1, 0x7f040021

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100075

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v1, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mContextImage:Lcom/google/android/velvet/ui/WebImageView;

    const v1, 0x7f100076

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mGoogleLogo:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mGoogleLogo:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mGoogleLogoVisibility:I

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mContextImage:Lcom/google/android/velvet/ui/WebImageView;

    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->onDestroyView()V

    return-void
.end method

.method public setContextHeaderHeight(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mContextImage:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/WebImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v1, p1, :cond_0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mContextImage:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/WebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public setContextImageDrawable(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/sidekick/inject/BackgroundImage;)V
    .locals 6
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Lcom/google/android/apps/sidekick/inject/BackgroundImage;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mContextImage:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v4, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->isDoodle()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->getPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mContextImage:Lcom/google/android/velvet/ui/WebImageView;

    new-instance v4, Lcom/google/android/velvet/ui/ContextHeaderFragment$OnDoodleTouch;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;->getInfoUrl()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/velvet/ui/ContextHeaderFragment$OnDoodleTouch;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mGoogleLogo:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    iget-object v3, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mGoogleLogo:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    iput v3, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mGoogleLogoVisibility:I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/ContextHeaderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-virtual {v2, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->setFadeSearchPlateOverHeader(Z)V

    return-void

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mContextImage:Lcom/google/android/velvet/ui/WebImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/velvet/ui/ContextHeaderFragment;->mGoogleLogo:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
