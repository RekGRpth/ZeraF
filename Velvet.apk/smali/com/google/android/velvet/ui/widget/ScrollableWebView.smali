.class public Lcom/google/android/velvet/ui/widget/ScrollableWebView;
.super Lcom/google/android/velvet/ui/widget/TextScalingWebview;
.source "ScrollableWebView.java"

# interfaces
.implements Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;


# instance fields
.field private mCoScrollLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->setOverScrollMode(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/ui/widget/ScrollableWebView;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/widget/ScrollableWebView;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->notifyScrollChangeToFixWebKitNotInSync()V

    return-void
.end method

.method private notifyScrollChangeToFixWebKitNotInSync()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollY()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollX()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollY()I

    move-result v3

    invoke-super {p0, v0, v1, v2, v3}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onScrollChanged(IIII)V

    return-void
.end method


# virtual methods
.method public computeScroll()V
    .locals 0

    return-void
.end method

.method public getDrawingRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1    # Landroid/graphics/Rect;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->getDrawingRect(Landroid/graphics/Rect;)V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getTranslationY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->mCoScrollLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->cropDrawingRectByPadding(Landroid/graphics/Rect;)V

    return-void
.end method

.method public getScrollingContentHeight()I
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getContentHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScale()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected overScrollBy(IIIIIIIIZ)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->mCoScrollLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->consumeVerticalScroll(I)I

    move-result p2

    invoke-super/range {p0 .. p9}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    return v0
.end method

.method public scrollTo(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->mCoScrollLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    invoke-virtual {v1, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->adjustScrollToY(I)I

    move-result v0

    invoke-super {p0, p1, v0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->scrollTo(II)V

    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->mCoScrollLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->mCoScrollLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    new-instance v1, Lcom/google/android/velvet/ui/widget/ScrollableWebView$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView$1;-><init>(Lcom/google/android/velvet/ui/widget/ScrollableWebView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setAnimationEndAction(Ljava/lang/Runnable;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setScrollYFromContainer(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollX()I

    move-result v0

    invoke-super {p0, v0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->scrollTo(II)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
