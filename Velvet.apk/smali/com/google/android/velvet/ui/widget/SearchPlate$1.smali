.class Lcom/google/android/velvet/ui/widget/SearchPlate$1;
.super Ljava/lang/Object;
.source "SearchPlate.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/TextChangeWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/ui/widget/SearchPlate;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/widget/SearchPlate;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate$1;->this$0:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate$1;->this$0:Lcom/google/android/velvet/ui/widget/SearchPlate;

    # getter for: Lcom/google/android/velvet/ui/widget/SearchPlate;->mPresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->access$100(Lcom/google/android/velvet/ui/widget/SearchPlate;)Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate$1;->this$0:Lcom/google/android/velvet/ui/widget/SearchPlate;

    # getter for: Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;
    invoke-static {v1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->access$000(Lcom/google/android/velvet/ui/widget/SearchPlate;)Lcom/google/android/searchcommon/ui/SimpleSearchText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->hasFocusFromKeyboard()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->onQueryTextChanged(Ljava/lang/String;Z)V

    return-void
.end method

.method public onTextEditStarted()V
    .locals 0

    return-void
.end method
