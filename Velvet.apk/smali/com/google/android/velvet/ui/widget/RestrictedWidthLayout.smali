.class public Lcom/google/android/velvet/ui/widget/RestrictedWidthLayout;
.super Landroid/widget/FrameLayout;
.source "RestrictedWidthLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/RestrictedWidthLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/velvet/ui/util/RestrictedWidthUtils;->getRestrictedWidthMeasureSpec(Landroid/content/res/Resources;IZ)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method
