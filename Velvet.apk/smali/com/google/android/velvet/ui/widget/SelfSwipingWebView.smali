.class public Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;
.super Lcom/google/android/velvet/ui/widget/TextScalingWebview;
.source "SelfSwipingWebView.java"


# instance fields
.field private mActivePointerId:I

.field private mDirectChildToSwipe:Landroid/view/View;

.field private mDownPositionX:F

.field private mLastTranslationX:F

.field private mSwipeAmount:F

.field private mSwipeControl:Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;

.field private mSwiping:Z

.field private final mTouchSlop:F

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mActivePointerId:I

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->setOverScrollMode(I)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mTouchSlop:F

    return-void
.end method

.method private removeSpuriousScrollAmountFromDelta(F)F
    .locals 2
    .param p1    # F

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDirectChildToSwipe:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    iget v1, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mLastTranslationX:F

    sub-float v1, v0, v1

    sub-float/2addr p1, v1

    iput v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mLastTranslationX:F

    return p1
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onAttachedToWindow()V

    move-object v0, p0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iput-object v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeControl:Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;

    iput-object v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDirectChildToSwipe:Landroid/view/View;

    :goto_0
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    instance-of v2, v1, Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport;

    if-eqz v2, :cond_1

    check-cast v1, Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport;

    invoke-interface {v1}, Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport;->getSwipeControlForSelfSwipingChild()Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeControl:Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDirectChildToSwipe:Landroid/view/View;

    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    check-cast v0, Landroid/view/View;

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onDetachedFromWindow()V

    iput-object v1, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeControl:Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;

    iput-object v1, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDirectChildToSwipe:Landroid/view/View;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwiping:Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v1, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, -0x1

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return v1

    :pswitch_0
    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDirectChildToSwipe:Landroid/view/View;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mActivePointerId:I

    if-ne v3, v6, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDirectChildToSwipe:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTranslationX()F

    move-result v3

    iput v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeAmount:F

    iget v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeAmount:F

    iput v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mLastTranslationX:F

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mActivePointerId:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDownPositionX:F

    goto :goto_0

    :pswitch_1
    iget v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mActivePointerId:I

    if-eq v2, v6, :cond_0

    iget v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mActivePointerId:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-ltz v0, :cond_0

    iget v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDownPositionX:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mTouchSlop:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_2
    iget-boolean v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwiping:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeControl:Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeControl:Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;

    iget-object v4, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDirectChildToSwipe:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-interface {v3, v4, v5}, Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;->onSwipeRelease(Landroid/view/View;Landroid/view/VelocityTracker;)V

    :cond_2
    :pswitch_3
    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mVelocityTracker:Landroid/view/VelocityTracker;

    iput-boolean v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwiping:Z

    iput v6, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mActivePointerId:I

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected overScrollBy(IIIIIIIIZ)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeControl:Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwiping:Z

    iget v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeAmount:F

    int-to-float v1, p1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->removeSpuriousScrollAmountFromDelta(F)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeAmount:F

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeControl:Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mDirectChildToSwipe:Landroid/view/View;

    iget v2, p0, Lcom/google/android/velvet/ui/widget/SelfSwipingWebView;->mSwipeAmount:F

    invoke-interface {v0, v1, v2}, Lcom/google/android/velvet/ui/util/SelfSwipingChildSupport$SwipeControl;->onSwipe(Landroid/view/View;F)V

    :cond_0
    invoke-super/range {p0 .. p9}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    return v0
.end method
