.class public Lcom/google/android/velvet/ui/util/ScrollHelper;
.super Ljava/lang/Object;
.source "ScrollHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;
    }
.end annotation


# instance fields
.field private mActivePointerId:I

.field private final mContext:Landroid/content/Context;

.field private mCurrentScrollAnimationTarget:I

.field private mDragging:Z

.field private mEdgeGlow:Landroid/widget/EdgeEffect;

.field private final mEdgeGlowSize:I

.field private final mInterpolator:Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;

.field private mLastPosition:F

.field private mMovement:I

.field private mOverscroll:I

.field private mOverscrolling:Z

.field private mPositiveOverscroll:Z

.field private final mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

.field private final mScroller:Landroid/widget/OverScroller;

.field private mShowingOverscrollEffect:Z

.field private mTotalMovement:I

.field private mTrackingVelocityForInterceptedFling:Z

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private final mView:Landroid/view/View;

.field private final mViewConfiguration:Landroid/view/ViewConfiguration;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/velvet/ui/util/ScrollViewControl;Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/velvet/ui/util/ScrollViewControl;
    .param p3    # Landroid/view/View;
    .param p4    # I

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    iput v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mActivePointerId:I

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    iput-object p3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    iput p4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlowSize:I

    new-instance v0, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;

    invoke-direct {v0}, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mInterpolator:Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->createOverscroller()Landroid/widget/OverScroller;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mViewConfiguration:Landroid/view/ViewConfiguration;

    return-void
.end method

.method private canScroll(Z)Z
    .locals 4
    .param p1    # Z

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v3}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getMaxScrollY()I

    move-result v3

    if-ge v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private endDrag(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mActivePointerId:I

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->startFlingIfFastEnough(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->isAnimatingScroll()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->notifyScrollFinished()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mActivePointerId:I

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTrackingVelocityForInterceptedFling:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mShowingOverscrollEffect:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_3
    return-void
.end method

.method private hasMovedEnough()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mViewConfiguration:Landroid/view/ViewConfiguration;

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setDownPosition(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->isAnimatingScroll()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->abortAnimation()V

    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mActivePointerId:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mLastPosition:F

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    iput v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    return-void
.end method

.method private startFlingIfFastEnough(I)V
    .locals 12
    .param p1    # I

    const/4 v3, 0x0

    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v0

    int-to-float v9, v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v0

    int-to-float v10, v0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1, v9}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v0

    float-to-int v11, v0

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v10

    if-lez v0, :cond_1

    :goto_0
    if-eqz v11, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mInterpolator:Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;->setInterpolatorOverride(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getScrollY()I

    move-result v2

    neg-int v4, v11

    iget-object v5, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v5}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getMaxScrollY()I

    move-result v8

    move v5, v3

    move v6, v3

    move v7, v3

    invoke-virtual/range {v0 .. v8}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidateOnAnimation()V

    :cond_0
    return-void

    :cond_1
    move v11, v3

    goto :goto_0
.end method

.method private trackVelocity(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private updateMovement(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    iget v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mActivePointerId:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mActivePointerId:I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iget v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mLastPosition:F

    cmpl-float v3, v2, v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mLastPosition:F

    sub-float v0, v3, v2

    float-to-int v3, v0

    iput v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    iget v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    int-to-float v3, v3

    sub-float/2addr v3, v0

    sub-float v3, v2, v3

    iput v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mLastPosition:F

    iget v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    iget v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    iget-boolean v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscroll:I

    iget v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscroll:I

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    goto :goto_0
.end method

.method private updateOverscrollEffect()V
    .locals 3

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    if-nez v1, :cond_0

    new-instance v1, Landroid/widget/EdgeEffect;

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->willNotDraw()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Velvet.ScrollHelper"

    const-string v2, "Can\'t draw overscroll effects if the view doesn\'t draw"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mShowingOverscrollEffect:Z

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mShowingOverscrollEffect:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    int-to-float v0, v1

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mPositiveOverscroll:Z

    if-eqz v1, :cond_2

    neg-float v0, v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    invoke-virtual {v1, v0}, Landroid/widget/EdgeEffect;->onPull(F)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->finish()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mShowingOverscrollEffect:Z

    goto :goto_0
.end method

.method private updateOverscrollStateAndGetScrollAmount()I
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    if-lez v1, :cond_3

    move v1, v2

    :goto_0
    iget-boolean v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mPositiveOverscroll:Z

    if-ne v1, v4, :cond_4

    iput v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    :cond_0
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    if-lez v1, :cond_8

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v4}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getMaxScrollY()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v5}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_1
    :goto_2
    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->hasMovedEnough()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-boolean v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    iput v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    iput v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscroll:I

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscroll:I

    if-lez v1, :cond_9

    :goto_3
    iput-boolean v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mPositiveOverscroll:Z

    :cond_2
    return v0

    :cond_3
    move v1, v3

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->hasMovedEnough()Z

    move-result v1

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    if-lez v1, :cond_5

    move v1, v2

    :goto_4
    invoke-direct {p0, v1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->canScroll(Z)Z

    move-result v1

    if-eqz v1, :cond_6

    iput-boolean v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    iput v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscroll:I

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    iput v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_4

    :cond_6
    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscroll:I

    if-lez v1, :cond_7

    move v1, v2

    :goto_5
    iget-boolean v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mPositiveOverscroll:Z

    if-eq v1, v4, :cond_0

    iput-boolean v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscroll:I

    iput v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    iput v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTotalMovement:I

    goto :goto_1

    :cond_7
    move v1, v3

    goto :goto_5

    :cond_8
    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    if-gez v1, :cond_1

    iget v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mMovement:I

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v4}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v4

    neg-int v4, v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    :cond_9
    move v2, v3

    goto :goto_3
.end method


# virtual methods
.method public computeScroll()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->isAnimatingScroll()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setScrollY(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidateOnAnimation()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->notifyScrollAnimationFinished()V

    goto :goto_0
.end method

.method createOverscroller()Landroid/widget/OverScroller;
    .locals 3

    new-instance v0, Landroid/widget/OverScroller;

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mInterpolator:Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;

    invoke-direct {v0, v1, v2}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    return-object v0
.end method

.method public drawOverscrollEffect(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mShowingOverscrollEffect:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    invoke-virtual {v4}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getScrollY()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-boolean v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mPositiveOverscroll:Z

    if-eqz v4, :cond_1

    const/high16 v4, 0x43340000

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v4, v3

    int-to-float v4, v4

    neg-int v5, v2

    sub-int/2addr v5, v0

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    iget v5, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlowSize:I

    invoke-virtual {v4, v3, v5}, Landroid/widget/EdgeEffect;->setSize(II)V

    :goto_0
    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->postInvalidateOnAnimation()V

    :goto_1
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x0

    int-to-float v5, v2

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlow:Landroid/widget/EdgeEffect;

    iget v5, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mEdgeGlowSize:I

    invoke-virtual {v4, v3, v5}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mShowingOverscrollEffect:Z

    goto :goto_1
.end method

.method getOverscrollAmount()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscroll:I

    return v0
.end method

.method public isAnimatingScroll()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFlinging()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isOverscrolling()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mOverscrolling:Z

    return v0
.end method

.method isShowingOverscrollEffect()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mShowingOverscrollEffect:Z

    return v0
.end method

.method public maybeStartInterceptedFling(I)Z
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTrackingVelocityForInterceptedFling:Z

    if-eqz v1, :cond_1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTrackingVelocityForInterceptedFling:Z

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->startFlingIfFastEnough(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->isAnimatingScroll()Z

    move-result v0

    :cond_1
    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->endDrag(Z)V

    :goto_0
    return v1

    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    goto :goto_0

    :cond_2
    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_1
    :pswitch_0
    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->setDownPosition(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->trackVelocity(Landroid/view/MotionEvent;)V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->updateMovement(Landroid/view/MotionEvent;)V

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    if-nez v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->hasMovedEnough()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->requestDisallowInterceptTouchEvent()V

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->trackVelocity(Landroid/view/MotionEvent;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const v5, 0x3e4ccccd

    const/16 v2, 0x14

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v3}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getMaxScrollY()I

    move-result v3

    if-ge v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v3}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int v0, v2, v3

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getMaxScrollY()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v2, v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->setScrollY(I)V

    :goto_0
    return v1

    :cond_0
    const/16 v2, 0x13

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v4}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int v0, v2, v3

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v2, v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->setScrollY(I)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onMaxScrollChanged()V
    .locals 6

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v5}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v2

    iget-object v5, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v5}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getMaxScrollY()I

    move-result v1

    const/4 v0, 0x0

    iget v5, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    packed-switch v5, :pswitch_data_0

    iget v5, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    if-le v5, v1, :cond_4

    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->isAnimatingScroll()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->abortAnimation()V

    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->smoothScrollTo(I)Z

    :cond_1
    return-void

    :pswitch_0
    iget-object v5, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v5}, Landroid/widget/OverScroller;->getFinalY()I

    move-result v5

    if-le v5, v1, :cond_2

    move v0, v3

    :goto_1
    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    :pswitch_1
    if-le v2, v1, :cond_3

    move v0, v3

    :goto_2
    goto :goto_0

    :cond_3
    move v0, v4

    goto :goto_2

    :cond_4
    move v0, v4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->trackVelocity(Landroid/view/MotionEvent;)V

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return v4

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->setDownPosition(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->updateMovement(Landroid/view/MotionEvent;)V

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->hasMovedEnough()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->requestDisallowInterceptTouchEvent()V

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mDragging:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->updateOverscrollStateAndGetScrollAmount()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v3}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v3

    add-int/2addr v3, v1

    invoke-interface {v2, v3}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->setScrollY(I)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->updateOverscrollEffect()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v4}, Lcom/google/android/velvet/ui/util/ScrollHelper;->endDrag(Z)V

    goto :goto_0

    :pswitch_3
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/velvet/ui/util/ScrollHelper;->endDrag(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method requestDisallowInterceptTouchEvent()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    return-void
.end method

.method public smoothScrollTo(I)Z
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->smoothScrollTo(ILandroid/animation/TimeInterpolator;I)Z

    move-result v0

    return v0
.end method

.method public smoothScrollTo(ILandroid/animation/TimeInterpolator;I)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/animation/TimeInterpolator;
    .param p3    # I

    const/4 v6, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->isAnimatingScroll()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0, v6}, Landroid/widget/OverScroller;->forceFinished(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScrollViewControl:Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-interface {v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getMaxScrollY()I

    move-result v0

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result p1

    if-eq p1, v2, :cond_1

    iput p1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mCurrentScrollAnimationTarget:I

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mInterpolator:Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;->setInterpolatorOverride(Landroid/animation/TimeInterpolator;)V

    const/4 v0, -0x1

    if-ne p3, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    sub-int v3, p1, v2

    invoke-virtual {v0, v1, v2, v1, v3}, Landroid/widget/OverScroller;->startScroll(IIII)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidateOnAnimation()V

    move v1, v6

    :cond_1
    return v1

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mScroller:Landroid/widget/OverScroller;

    sub-int v4, p1, v2

    move v3, v1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    goto :goto_0
.end method

.method public trackVelocityForFlingIntercept(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper;->mTrackingVelocityForInterceptedFling:Z

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->trackVelocity(Landroid/view/MotionEvent;)V

    return-void
.end method
