.class public interface abstract Lcom/google/android/velvet/ui/util/ScrollViewControl;
.super Ljava/lang/Object;
.source "ScrollViewControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;
    }
.end annotation


# virtual methods
.method public abstract addScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V
.end method

.method public abstract getDescendantTop(Landroid/view/View;)I
.end method

.method public abstract getMaxScrollY()I
.end method

.method public abstract getScrollY()I
.end method

.method public abstract getViewportHeight()I
.end method

.method public abstract notifyScrollAnimationFinished()V
.end method

.method public abstract notifyScrollFinished()V
.end method

.method public abstract removeScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V
.end method

.method public abstract scrollToView(Landroid/view/View;I)V
.end method

.method public abstract setScrollY(I)V
.end method

.method public abstract smoothScrollToY(I)V
.end method

.method public abstract smoothScrollToYSyncWithTransition(ILandroid/view/ViewGroup;I)V
.end method
