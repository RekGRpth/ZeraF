.class public Lcom/google/android/velvet/ui/util/Animations;
.super Ljava/lang/Object;
.source "Animations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;
    }
.end annotation


# static fields
.field public static final INTERPOLATOR:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/velvet/ui/util/Animations;->INTERPOLATOR:Landroid/view/animation/Interpolator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 1
    .param p0    # Landroid/view/View;

    invoke-static {p0}, Lcom/google/android/velvet/ui/util/Animations;->getAnimator(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static crossFadeViews(Landroid/view/View;Landroid/view/View;Z)V
    .locals 4
    .param p0    # Landroid/view/View;
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v2}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1
.end method

.method public static fadeOutAndHide(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 1
    .param p0    # Landroid/view/View;

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static fadeOutAndHide(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;
    .locals 2
    .param p0    # Landroid/view/View;
    .param p1    # I

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p0}, Lcom/google/android/velvet/ui/util/Animations;->getAnimator(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/google/android/velvet/ui/util/Animations;->hideOnAnimationEnd(Landroid/view/View;I)Landroid/animation/Animator$AnimatorListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fadeScaleUpdateText(Landroid/widget/TextView;Ljava/lang/String;F)Landroid/view/ViewPropertyAnimator;
    .locals 2
    .param p0    # Landroid/widget/TextView;
    .param p1    # Ljava/lang/String;
    .param p2    # F

    invoke-static {p0}, Lcom/google/android/velvet/ui/util/Animations;->getAnimator(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/ui/util/Animations$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/velvet/ui/util/Animations$1;-><init>(Landroid/widget/TextView;Ljava/lang/String;F)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static fadeUpdateText(Landroid/widget/TextView;Ljava/lang/String;)Landroid/view/ViewPropertyAnimator;
    .locals 1
    .param p0    # Landroid/widget/TextView;
    .param p1    # Ljava/lang/String;

    const/high16 v0, 0x3f800000

    invoke-static {p0, p1, v0}, Lcom/google/android/velvet/ui/util/Animations;->fadeScaleUpdateText(Landroid/widget/TextView;Ljava/lang/String;F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method private static getAnimator(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 3
    .param p0    # Landroid/view/View;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/velvet/ui/util/Animations;->INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static hideOnAnimationEnd(Landroid/view/View;I)Landroid/animation/Animator$AnimatorListener;
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I

    new-instance v0, Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;

    invoke-direct {v0, p0, p1}, Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;-><init>(Landroid/view/View;I)V

    return-object v0
.end method

.method public static showAndFadeIn(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 2
    .param p0    # Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    invoke-static {p0}, Lcom/google/android/velvet/ui/util/Animations;->getAnimator(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static varargs stagger(JF[Landroid/view/ViewPropertyAnimator;)V
    .locals 8
    .param p0    # J
    .param p2    # F
    .param p3    # [Landroid/view/ViewPropertyAnimator;

    const/4 v2, 0x0

    move-object v1, p3

    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v1, v3

    int-to-long v5, v2

    mul-long/2addr v5, p0

    invoke-virtual {v0, v5, v6}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->getDuration()J

    move-result-wide v6

    long-to-float v6, v6

    mul-float/2addr v6, p2

    float-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
