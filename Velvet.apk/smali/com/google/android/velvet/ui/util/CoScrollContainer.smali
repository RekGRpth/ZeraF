.class public Lcom/google/android/velvet/ui/util/CoScrollContainer;
.super Landroid/widget/FrameLayout;
.source "CoScrollContainer.java"

# interfaces
.implements Lcom/google/android/velvet/ui/util/ScrollViewControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    }
.end annotation


# instance fields
.field private mDisallowIntercept:Z

.field private mFooterPadding:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field private mHaveScrollToConsume:Z

.field private mHeaderPadding:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field private mInterceptedTouchEventListener:Landroid/view/View$OnTouchListener;

.field private final mInterpolator:Landroid/view/animation/DecelerateInterpolator;

.field private mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

.field private mPointerIdForFlingInterception:I

.field private mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

.field private final mScrollListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollToAfterLayoutDescendent:Landroid/view/View;

.field private mScrollToAfterLayoutDuration:I

.field private mScrollToAfterLayoutInterpolator:Landroid/animation/TimeInterpolator;

.field private mScrollToAfterLayoutOffset:I

.field private mScrollableChildHandlingFocusChangeKeyEvent:Z

.field private final mTmpRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40200000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollListeners:Ljava/util/List;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mTmpRect:Landroid/graphics/Rect;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mPointerIdForFlingInterception:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40200000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollListeners:Ljava/util/List;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mTmpRect:Landroid/graphics/Rect;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mPointerIdForFlingInterception:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40200000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollListeners:Ljava/util/List;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mTmpRect:Landroid/graphics/Rect;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mPointerIdForFlingInterception:I

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/velvet/ui/util/CoScrollContainer;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->consumeChildVerticalScroll(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/velvet/ui/util/CoScrollContainer;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->adjustChildScrollToY(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1802(Lcom/google/android/velvet/ui/util/CoScrollContainer;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHaveScrollToConsume:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/android/velvet/ui/util/CoScrollContainer;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->resolveChildTop(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/velvet/ui/util/CoScrollContainer;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->syncChild(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/velvet/ui/util/CoScrollContainer;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHeaderPadding:I

    return v0
.end method

.method static synthetic access$2200(Lcom/google/android/velvet/ui/util/CoScrollContainer;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mFooterPadding:I

    return v0
.end method

.method private adjustChildScrollToY(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    .locals 4
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollY()I

    move-result v1

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$500(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v2

    if-ge v1, v2, :cond_1

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollableChildHandlingFocusChangeKeyEvent:Z

    if-nez v2, :cond_1

    const/4 p2, 0x0

    :goto_0
    return p2

    :cond_1
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$500(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v2

    add-int v0, v2, p2

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollableChildHandlingFocusChangeKeyEvent:Z

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollX()I

    move-result v2

    invoke-super {p0, v2, v0}, Landroid/widget/FrameLayout;->scrollTo(II)V

    goto :goto_0
.end method

.method private consumeChildVerticalScroll(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    .locals 12
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p2    # I

    const/4 v11, 0x0

    iget-object v9, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    if-eq v9, p1, :cond_0

    const-string v9, "Velvet.CoScrollContainer"

    const-string v10, "Multiple children causing a scroll?"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # setter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mSpuriousScrollAmount:I
    invoke-static {v9, v11}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$702(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    :cond_0
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getScrollY()I

    move-result v1

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mSpuriousScrollAmount:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$700(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v9

    sub-int/2addr p2, v9

    move v3, p2

    iget-boolean v9, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHaveScrollToConsume:Z

    if-eqz v9, :cond_1

    if-lez p2, :cond_1

    invoke-direct {p0, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->consumeScrollDelta(I)I

    move-result p2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollY()I

    move-result v9

    add-int/2addr v9, p2

    invoke-static {v11, v9}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getMaxScrollY()I

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollX()I

    move-result v9

    invoke-super {p0, v9, v7}, Landroid/widget/FrameLayout;->scrollTo(II)V

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$500(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v2

    sub-int v8, v7, v2

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollableChild:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$600(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;->getScrollingContentHeight()I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getHeight()I

    move-result v10

    sub-int v0, v9, v10

    invoke-virtual {p0, v2, v8, v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getTranslationForChild(III)I

    move-result v5

    invoke-direct {p0, p1, v5}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    invoke-virtual {p0, v8, v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollForChild(II)I

    move-result v4

    sub-int v6, v4, v1

    sub-int v9, v6, v3

    # setter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mSpuriousScrollAmount:I
    invoke-static {p1, v9}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$702(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    return v6
.end method

.method private consumeScrollDelta(I)I
    .locals 9
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getChildCount()I

    move-result v6

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v6, :cond_3

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I
    invoke-static {v4}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$800(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v7

    if-lez v7, :cond_2

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I
    invoke-static {v4}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$800(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v7

    invoke-static {v7, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    # -= operator for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I
    invoke-static {v4, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$520(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    # -= operator for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I
    invoke-static {v4, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$820(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    iget-object v7, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    if-eq v4, v7, :cond_0

    invoke-direct {p0, v4, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->syncChild(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    :cond_0
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I
    invoke-static {v4}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$800(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v7

    if-lez v7, :cond_1

    const/4 v2, 0x1

    :cond_1
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I
    invoke-static {v4}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$800(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v7

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mOriginalConsumableMargin:I
    invoke-static {v4}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$900(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v8

    invoke-direct {p0, v0, v7, v8}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->notifyScrollMarginConsumed(Landroid/view/View;II)V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iput-boolean v2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHaveScrollToConsume:Z

    sub-int v7, p1, v5

    return v7
.end method

.method private isFocusChangingKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x3d

    if-eq v0, v1, :cond_0

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private notifyScroll()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollY()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getMaxScrollY()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v4}, Lcom/google/android/velvet/ui/util/ScrollHelper;->isFlinging()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_0
    iget-object v4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollListeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    invoke-interface {v1, v3, v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;->onScrollChanged(II)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private notifyScrollMarginConsumed(Landroid/view/View;II)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;->onScrollMarginConsumed(Landroid/view/View;II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private resolveChildTop(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    const/4 v1, 0x0

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$800(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v2

    add-int/2addr v1, v2

    # setter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I
    invoke-static {p1, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$502(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    return-void

    :pswitch_1
    iget v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHeaderPadding:I

    goto :goto_0

    :pswitch_2
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchor:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1000(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getDescendantTop(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchor:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1000(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v0

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchorMargin:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1100(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v3

    add-int v1, v2, v3

    goto :goto_0

    :cond_0
    const-string v2, "Velvet.CoScrollContainer"

    const-string v3, "Scroll anchor is not a descendant"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_3
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private scrollToAfterLayout(Landroid/view/View;ILandroid/animation/TimeInterpolator;I)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/animation/TimeInterpolator;
    .param p4    # I

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutDescendent:Landroid/view/View;

    iput p2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutOffset:I

    iput-object p3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutInterpolator:Landroid/animation/TimeInterpolator;

    iput p4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutDuration:I

    return-void
.end method

.method private scrollableChildHasFocus()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z
    invoke-static {v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$000(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setOrAnimateTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;II)V
    .locals 9
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p2    # I
    .param p3    # I

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTranslationY()F

    move-result v5

    float-to-int v2, v5

    if-eq p2, v2, :cond_4

    if-gez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollY()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getHeight()I

    move-result v6

    add-int v2, v5, v6

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v5

    int-to-float v6, v2

    invoke-virtual {v5, v6}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    const-wide/16 v0, 0x0

    sub-int v5, v2, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-le v5, v6, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnimationEndTime:J
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-lez v5, :cond_2

    if-eqz p3, :cond_2

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnimationEndTime:J
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)J

    move-result-wide v5

    sub-long v0, v5, v3

    :cond_1
    :goto_0
    const-wide/16 v5, 0x32

    cmp-long v5, v0, v5

    if-lez v5, :cond_3

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    int-to-float v6, p2

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mEndAction:Ljava/lang/Runnable;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1400(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :goto_1
    return-void

    :cond_2
    const-wide/16 v0, 0x190

    add-long v5, v3, v0

    # setter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnimationEndTime:J
    invoke-static {p1, v5, v6}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1302(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;J)J

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    goto :goto_1

    :cond_4
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewPropertyAnimator;->cancel()V

    goto :goto_1
.end method

.method private setTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p2    # I

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$1200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;

    move-result-object v0

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method private syncChild(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V
    .locals 8
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p2    # I

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getHeight()I

    move-result v6

    neg-int v6, v6

    invoke-direct {p0, p1, v6}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$000(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Z

    move-result v6

    if-eqz v6, :cond_0

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollableChild:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$600(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;->setScrollYFromContainer(I)V

    goto :goto_0

    :pswitch_1
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$500(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollY()I

    move-result v6

    sub-int v4, v6, v1

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$000(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Z

    move-result v6

    if-eqz v6, :cond_1

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollableChild:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$600(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;->getScrollingContentHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getHeight()I

    move-result v7

    sub-int v0, v6, v7

    invoke-virtual {p0, v4, v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollForChild(II)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;->setScrollYFromContainer(I)V

    invoke-virtual {p0, v1, v4, v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getTranslationForChild(III)I

    move-result v5

    invoke-direct {p0, p1, v5, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setOrAnimateTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;II)V

    goto :goto_0

    :cond_1
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$500(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v6

    invoke-direct {p0, p1, v6, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setOrAnimateTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;II)V

    goto :goto_0

    :pswitch_2
    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$500(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v6

    invoke-direct {p0, p1, v6}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->computeVerticalScrollRange()I

    move-result v6

    iget v7, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mFooterPadding:I

    sub-int/2addr v6, v7

    invoke-direct {p0, p1, v6}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollY()I

    move-result v6

    iget v7, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHeaderPadding:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-direct {p0, p1, v6, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setOrAnimateTranslation(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private syncChildren(ZI)V
    .locals 5
    .param p1    # Z
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getChildCount()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I
    invoke-static {v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v4

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->resolveChildTop(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)V

    :cond_0
    invoke-direct {p0, v2, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->syncChild(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public addScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/ViewGroup$LayoutParams;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # invokes: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setView(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$100(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;Landroid/view/View;)V

    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;
    invoke-static {p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->computeScroll()V

    return-void
.end method

.method protected computeVerticalScrollRange()I
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getMeasuredHeight()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z
    invoke-static {v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$000(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Z

    move-result v4

    if-eqz v4, :cond_1

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I
    invoke-static {v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v4

    const/4 v5, 0x5

    if-eq v4, v5, :cond_0

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I
    invoke-static {v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$500(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v4

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollableChild:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;
    invoke-static {v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$600(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;->getScrollingContentHeight()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mFooterPadding:I

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_1

    :cond_2
    return v3
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->scrollableChildHasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->isFocusChangingKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollableChildHandlingFocusChangeKeyEvent:Z

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mPointerIdForFlingInterception:I

    iput-object v4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mDisallowIntercept:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->trackVelocityForFlingIntercept(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mDisallowIntercept:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mPointerIdForFlingInterception:I

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    iget v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mPointerIdForFlingInterception:I

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->maybeStartInterceptedFling(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->notifyScrollFinished()V

    :cond_1
    :pswitch_3
    iput-boolean v2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mDisallowIntercept:Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # setter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mSpuriousScrollAmount:I
    invoke-static {v0, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$702(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    iput-object v4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mParamsOfChildCurrentlyScrolling:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    :cond_2
    iput v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mPointerIdForFlingInterception:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->generateDefaultLayoutParams()Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->generateDefaultLayoutParams()Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .locals 1

    new-instance v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;-><init>(Lcom/google/android/velvet/ui/util/CoScrollContainer;)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/util/AttributeSet;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 1
    .param p1    # Landroid/util/AttributeSet;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;-><init>(Landroid/content/Context;Lcom/google/android/velvet/ui/util/CoScrollContainer;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    invoke-direct {v0, p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;-><init>(Lcom/google/android/velvet/ui/util/CoScrollContainer;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getDescendantBounds(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v7, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {p2, v8, v8, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    move-object v1, p1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    :goto_0
    if-eq v4, p0, :cond_1

    instance-of v5, v4, Landroid/view/View;

    if-eqz v5, :cond_0

    move-object v0, v4

    check-cast v0, Landroid/view/View;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    goto :goto_0

    :cond_0
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Descendant isn\'t our descendant?"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v2

    iput v7, p2, Landroid/graphics/Rect;->right:I

    iput v7, p2, Landroid/graphics/Rect;->bottom:I

    iput v7, p2, Landroid/graphics/Rect;->left:I

    iput v7, p2, Landroid/graphics/Rect;->top:I

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I
    invoke-static {v3}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    const/4 v5, -0x1

    iput v5, p2, Landroid/graphics/Rect;->right:I

    iput v5, p2, Landroid/graphics/Rect;->bottom:I

    iput v5, p2, Landroid/graphics/Rect;->left:I

    iput v5, p2, Landroid/graphics/Rect;->top:I

    goto :goto_1

    :cond_2
    iget v5, p2, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v6

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p2, Landroid/graphics/Rect;->top:I

    iget v5, p2, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    move-result v6

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p2, Landroid/graphics/Rect;->left:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public getDescendantTop(Landroid/view/View;)I
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getDescendantBounds(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mTmpRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public getMaxScrollY()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->computeVerticalScrollRange()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method getScrollForChild(II)I
    .locals 1
    .param p1    # I
    .param p2    # I

    if-gtz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method getTranslationForChild(III)I
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    if-gtz p2, :cond_0

    :goto_0
    return p1

    :cond_0
    if-ge p2, p3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollY()I

    move-result p1

    goto :goto_0

    :cond_1
    add-int/2addr p1, p3

    goto :goto_0
.end method

.method public getViewportHeight()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getHeight()I

    move-result v0

    return v0
.end method

.method public isAnimatingScroll()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutDescendent:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/util/ScrollHelper;->isAnimatingScroll()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyScrollAnimationFinished()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    invoke-interface {v1}, Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;->onScrollAnimationFinished()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public notifyScrollFinished()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    invoke-interface {v1}, Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;->onScrollFinished()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->drawOverscrollEffect(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    new-instance v0, Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-direct {v0, v1, p0, p0, v2}, Lcom/google/android/velvet/ui/util/ScrollHelper;-><init>(Landroid/content/Context;Lcom/google/android/velvet/ui/util/ScrollViewControl;Landroid/view/View;I)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setWillNotDraw(Z)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mInterceptedTouchEventListener:Landroid/view/View$OnTouchListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mInterceptedTouchEventListener:Landroid/view/View$OnTouchListener;

    invoke-interface {v1, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    :cond_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/ui/util/ScrollHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->syncChildren(ZI)V

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutDescendent:Landroid/view/View;

    if-eqz v3, :cond_1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutDescendent:Landroid/view/View;

    if-ne v3, p0, :cond_3

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutInterpolator:Landroid/animation/TimeInterpolator;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    iget v4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutOffset:I

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/ui/util/ScrollHelper;->smoothScrollTo(I)Z

    move-result v1

    :cond_0
    :goto_0
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutDescendent:Landroid/view/View;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->notifyScrollAnimationFinished()V

    :cond_1
    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v3}, Lcom/google/android/velvet/ui/util/ScrollHelper;->onMaxScrollChanged()V

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->notifyScroll()V

    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    iget v4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutOffset:I

    iget-object v5, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutInterpolator:Landroid/animation/TimeInterpolator;

    iget v6, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutDuration:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/velvet/ui/util/ScrollHelper;->smoothScrollTo(ILandroid/animation/TimeInterpolator;I)Z

    move-result v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutDescendent:Landroid/view/View;

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getDescendantTop(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    iget v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutOffset:I

    sub-int v2, v0, v3

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutInterpolator:Landroid/animation/TimeInterpolator;

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v3, v2}, Lcom/google/android/velvet/ui/util/ScrollHelper;->smoothScrollTo(I)Z

    move-result v1

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutInterpolator:Landroid/animation/TimeInterpolator;

    iget v5, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollToAfterLayoutDuration:I

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/velvet/ui/util/ScrollHelper;->smoothScrollTo(ILandroid/animation/TimeInterpolator;I)Z

    move-result v1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/high16 v9, 0x40000000

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    if-ne v6, v9, :cond_1

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    if-ne v6, v9, :cond_2

    :goto_1
    invoke-static {v7}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getChildCount()I

    move-result v6

    if-ge v2, v6, :cond_4

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z
    invoke-static {v3}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$000(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    :cond_0
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v6, v8

    goto :goto_0

    :cond_2
    move v7, v8

    goto :goto_1

    :cond_3
    invoke-virtual {v0, p1, v4}, Landroid/view/View;->measure(II)V

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mFillViewport:Z
    invoke-static {v3}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$400(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    if-ge v6, v1, :cond_0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    goto :goto_3

    :cond_4
    invoke-virtual {p0, v5, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onScrollChanged(IIII)V

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->notifyScroll()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public removeScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;
    invoke-static {v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer;

    move-result-object v1

    if-ne v1, p0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    const/4 v1, 0x0

    # setter for: Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->access$202(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;Lcom/google/android/velvet/ui/util/CoScrollContainer;)Lcom/google/android/velvet/ui/util/CoScrollContainer;

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mDisallowIntercept:Z

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    return-void
.end method

.method public scrollTo(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getScrollY()I

    move-result v0

    sub-int v1, p2, v0

    if-lez v1, :cond_0

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHaveScrollToConsume:Z

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->consumeScrollDelta(I)I

    move-result v2

    add-int p2, v0, v2

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    const/4 v2, 0x0

    invoke-direct {p0, v2, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->syncChildren(ZI)V

    return-void
.end method

.method public scrollToView(Landroid/view/View;I)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->isLayoutRequested()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->scrollToAfterLayout(Landroid/view/View;ILandroid/animation/TimeInterpolator;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->getDescendantTop(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    sub-int v2, v0, p2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/util/ScrollHelper;->smoothScrollTo(I)Z

    goto :goto_0
.end method

.method public setHeaderAndFooterPadding(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHeaderPadding:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mFooterPadding:I

    if-eq p2, v0, :cond_1

    :cond_0
    iput p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHeaderPadding:I

    iput p2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mFooterPadding:I

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->syncChildren(ZI)V

    :cond_1
    return-void
.end method

.method public setInterceptedTouchEventListener(Landroid/view/View$OnTouchListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnTouchListener;

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mInterceptedTouchEventListener:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method public smoothScrollToY(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p0, p1, v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->scrollToAfterLayout(Landroid/view/View;ILandroid/animation/TimeInterpolator;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->smoothScrollTo(I)Z

    goto :goto_0
.end method

.method public smoothScrollToYSyncWithTransition(ILandroid/view/ViewGroup;I)V
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # I

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->smoothScrollToY(I)V

    :cond_0
    invoke-virtual {v3, p3}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->smoothScrollToY(I)V

    :cond_1
    invoke-virtual {v3, p3}, Landroid/animation/LayoutTransition;->getInterpolator(I)Landroid/animation/TimeInterpolator;

    move-result-object v2

    invoke-virtual {v0}, Landroid/animation/Animator;->getDuration()J

    move-result-wide v4

    long-to-int v1, v4

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->isLayoutRequested()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, p0, p1, v2, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->scrollToAfterLayout(Landroid/view/View;ILandroid/animation/TimeInterpolator;I)V

    :goto_0
    return-void

    :cond_2
    iget-object v4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer;->mScrollHelper:Lcom/google/android/velvet/ui/util/ScrollHelper;

    invoke-virtual {v4, p1, v2, v1}, Lcom/google/android/velvet/ui/util/ScrollHelper;->smoothScrollTo(ILandroid/animation/TimeInterpolator;I)Z

    goto :goto_0
.end method
