.class public Lcom/google/android/velvet/ui/util/ViewRecycler;
.super Ljava/lang/Object;
.source "ViewRecycler.java"


# instance fields
.field private final mMinRecycleBinSize:I

.field private final mScrapViews:[Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mViewTypeCount:I


# direct methods
.method public constructor <init>(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/velvet/ui/util/ViewRecycler;->mMinRecycleBinSize:I

    iput p1, p0, Lcom/google/android/velvet/ui/util/ViewRecycler;->mViewTypeCount:I

    iget v1, p0, Lcom/google/android/velvet/ui/util/ViewRecycler;->mViewTypeCount:I

    new-array v1, v1, [Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/velvet/ui/util/ViewRecycler;->mScrapViews:[Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/velvet/ui/util/ViewRecycler;->mViewTypeCount:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ViewRecycler;->mScrapViews:[Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getView(I)Landroid/view/View;
    .locals 4
    .param p1    # I

    if-ltz p1, :cond_1

    iget-object v3, p0, Lcom/google/android/velvet/ui/util/ViewRecycler;->mScrapViews:[Ljava/util/List;

    aget-object v0, v3, p1

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    :cond_0
    return-object v2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseView(Landroid/view/View;II)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    const/4 v3, -0x1

    if-ltz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/ViewRecycler;->mScrapViews:[Ljava/util/List;

    aget-object v0, v1, p2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lcom/google/android/velvet/ui/util/ViewRecycler;->mMinRecycleBinSize:I

    invoke-static {p3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusUpId(I)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusDownId(I)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
