.class public Lcom/android/ex/photo/views/ProgressBarWrapper;
.super Ljava/lang/Object;
.source "ProgressBarWrapper.java"


# instance fields
.field private final mDeterminate:Landroid/widget/ProgressBar;

.field private final mIndeterminate:Landroid/widget/ProgressBar;

.field private mIsIndeterminate:Z


# direct methods
.method public constructor <init>(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;Z)V
    .locals 0
    .param p1    # Landroid/widget/ProgressBar;
    .param p2    # Landroid/widget/ProgressBar;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/ex/photo/views/ProgressBarWrapper;->mDeterminate:Landroid/widget/ProgressBar;

    iput-object p2, p0, Lcom/android/ex/photo/views/ProgressBarWrapper;->mIndeterminate:Landroid/widget/ProgressBar;

    invoke-virtual {p0, p3}, Lcom/android/ex/photo/views/ProgressBarWrapper;->setIndeterminate(Z)V

    return-void
.end method

.method private setVisibility(Z)V
    .locals 4
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/ex/photo/views/ProgressBarWrapper;->mIndeterminate:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/ex/photo/views/ProgressBarWrapper;->mDeterminate:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method


# virtual methods
.method public setIndeterminate(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/ex/photo/views/ProgressBarWrapper;->mIsIndeterminate:Z

    iget-boolean v0, p0, Lcom/android/ex/photo/views/ProgressBarWrapper;->mIsIndeterminate:Z

    invoke-direct {p0, v0}, Lcom/android/ex/photo/views/ProgressBarWrapper;->setVisibility(Z)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/ex/photo/views/ProgressBarWrapper;->mIndeterminate:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/ex/photo/views/ProgressBarWrapper;->mDeterminate:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/ex/photo/views/ProgressBarWrapper;->mIsIndeterminate:Z

    invoke-direct {p0, v0}, Lcom/android/ex/photo/views/ProgressBarWrapper;->setVisibility(Z)V

    goto :goto_0
.end method
