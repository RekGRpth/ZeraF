.class public Lcom/android/ex/photo/loaders/PhotoBitmapLoader;
.super Lvedroid/support/v4/content/AsyncTaskLoader;
.source "PhotoBitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;",
        ">;"
    }
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mPhotoUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lvedroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mPhotoUri:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public deliverResult(Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;)V
    .locals 3
    .param p1    # Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;

    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;->bitmap:Landroid/graphics/Bitmap;

    :goto_0
    invoke-virtual {p0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->isReset()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->onReleaseResources(Landroid/graphics/Bitmap;)V

    :cond_0
    iget-object v1, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-super {p0, p1}, Lvedroid/support/v4/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_1
    if-eqz v1, :cond_2

    if-eq v1, v0, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->onReleaseResources(Landroid/graphics/Bitmap;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;

    invoke-virtual {p0, p1}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->deliverResult(Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;)V

    return-void
.end method

.method public loadInBackground()Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;
    .locals 6

    new-instance v3, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;

    invoke-direct {v3}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;-><init>()V

    invoke-virtual {p0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mPhotoUri:Ljava/lang/String;

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    :try_start_0
    iget-object v4, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mPhotoUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/android/ex/photo/fragments/PhotoViewFragment;->sPhotoSize:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v2, v4, v5}, Lcom/android/ex/photo/util/ImageUtils;->createLocalBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;I)Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;

    move-result-object v3

    iget-object v4, v3, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;->bitmap:Landroid/graphics/Bitmap;

    const/16 v5, 0xa0

    invoke-virtual {v4, v5}, Landroid/graphics/Bitmap;->setDensity(I)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v3

    :catch_0
    move-exception v1

    const/4 v4, 0x1

    iput v4, v3, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;->status:I

    goto :goto_0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->loadInBackground()Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;

    move-result-object v0

    return-object v0
.end method

.method public onCanceled(Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;)V
    .locals 1
    .param p1    # Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;

    invoke-super {p0, p1}, Lvedroid/support/v4/content/AsyncTaskLoader;->onCanceled(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->onReleaseResources(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onCanceled(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;

    invoke-virtual {p0, p1}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->onCanceled(Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;)V

    return-void
.end method

.method protected onReleaseResources(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void
.end method

.method protected onReset()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/content/AsyncTaskLoader;->onReset()V

    invoke-virtual {p0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->onStopLoading()V

    iget-object v0, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->onReleaseResources(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method protected onStartLoading()V
    .locals 2

    iget-object v1, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    new-instance v0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;

    invoke-direct {v0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;-><init>()V

    const/4 v1, 0x0

    iput v1, v0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;->status:I

    iget-object v1, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->deliverResult(Lcom/android/ex/photo/loaders/PhotoBitmapLoader$BitmapResult;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->takeContentChanged()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->forceLoad()V

    :cond_2
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->cancelLoad()Z

    return-void
.end method

.method public setPhotoUri(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->mPhotoUri:Ljava/lang/String;

    return-void
.end method
