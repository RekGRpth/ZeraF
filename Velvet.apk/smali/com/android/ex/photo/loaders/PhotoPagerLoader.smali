.class public Lcom/android/ex/photo/loaders/PhotoPagerLoader;
.super Lvedroid/support/v4/content/CursorLoader;
.source "PhotoPagerLoader.java"


# instance fields
.field private final mPhotosUri:Landroid/net/Uri;

.field private final mProjection:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lvedroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/ex/photo/loaders/PhotoPagerLoader;->mPhotosUri:Landroid/net/Uri;

    if-eqz p3, :cond_0

    :goto_0
    iput-object p3, p0, Lcom/android/ex/photo/loaders/PhotoPagerLoader;->mProjection:[Ljava/lang/String;

    return-void

    :cond_0
    sget-object p3, Lcom/android/ex/photo/provider/PhotoContract$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public loadInBackground()Landroid/database/Cursor;
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/ex/photo/loaders/PhotoPagerLoader;->mPhotosUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "contentType"

    const-string v4, "image/"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/ex/photo/loaders/PhotoPagerLoader;->setUri(Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/android/ex/photo/loaders/PhotoPagerLoader;->mProjection:[Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/android/ex/photo/loaders/PhotoPagerLoader;->setProjection([Ljava/lang/String;)V

    invoke-super {p0}, Lvedroid/support/v4/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/ex/photo/loaders/PhotoPagerLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
