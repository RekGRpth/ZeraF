.class public final Lcom/android/okhttp/internal/spdy/SpdyConnection;
.super Ljava/lang/Object;
.source "SpdyConnection.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;,
        Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final executor:Ljava/util/concurrent/ExecutorService;


# instance fields
.field final client:Z

.field private final handler:Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;

.field private final hostName:Ljava/lang/String;

.field private idleStartTimeNs:J

.field private lastGoodStreamId:I

.field private nextPingId:I

.field private nextStreamId:I

.field private pings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/okhttp/internal/spdy/Ping;",
            ">;"
        }
    .end annotation
.end field

.field settings:Lcom/android/okhttp/internal/spdy/Settings;

.field private shutdown:Z

.field private final spdyReader:Lcom/android/okhttp/internal/spdy/SpdyReader;

.field private final spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

.field private final streams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/okhttp/internal/spdy/SpdyStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v1, 0x0

    const-class v0, Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->$assertionsDisabled:Z

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const v2, 0x7fffffff

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v6}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->executor:Ljava/util/concurrent/ExecutorService;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private constructor <init>(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)V
    .locals 5
    .param p1    # Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->idleStartTimeNs:J

    iget-boolean v0, p1, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->client:Z

    iput-boolean v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->client:Z

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->handler:Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;
    invoke-static {p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->access$000(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->handler:Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;

    new-instance v0, Lcom/android/okhttp/internal/spdy/SpdyReader;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->in:Ljava/io/InputStream;
    invoke-static {p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->access$100(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/okhttp/internal/spdy/SpdyReader;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyReader:Lcom/android/okhttp/internal/spdy/SpdyReader;

    new-instance v0, Lcom/android/okhttp/internal/spdy/SpdyWriter;

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->out:Ljava/io/OutputStream;
    invoke-static {p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->access$200(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)Ljava/io/OutputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/okhttp/internal/spdy/SpdyWriter;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    iget-boolean v0, p1, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->client:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->nextStreamId:I

    iget-boolean v0, p1, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->client:Z

    if-eqz v0, :cond_1

    :goto_1
    iput v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->nextPingId:I

    # getter for: Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->hostName:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->access$300(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->hostName:Ljava/lang/String;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Reader;-><init>(Lcom/android/okhttp/internal/spdy/SpdyConnection;Lcom/android/okhttp/internal/spdy/SpdyConnection$1;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Spdy Reader "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->hostName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;Lcom/android/okhttp/internal/spdy/SpdyConnection$1;)V
    .locals 0
    .param p1    # Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;
    .param p2    # Lcom/android/okhttp/internal/spdy/SpdyConnection$1;

    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;-><init>(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Z
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-boolean v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->shutdown:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/android/okhttp/internal/spdy/SpdyConnection;Z)Z
    .locals 0
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->shutdown:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/android/okhttp/internal/spdy/SpdyConnection;I)I
    .locals 0
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .param p1    # I

    iput p1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->lastGoodStreamId:I

    return p1
.end method

.method static synthetic access$1200(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->hostName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->handler:Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;

    return-object v0
.end method

.method static synthetic access$1500()Ljava/util/concurrent/ExecutorService;
    .locals 1

    sget-object v0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->executor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/okhttp/internal/spdy/SpdyConnection;ILcom/android/okhttp/internal/spdy/Ping;)V
    .locals 0
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .param p1    # I
    .param p2    # Lcom/android/okhttp/internal/spdy/Ping;

    invoke-direct {p0, p1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->writePingLater(ILcom/android/okhttp/internal/spdy/Ping;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/okhttp/internal/spdy/SpdyConnection;I)Lcom/android/okhttp/internal/spdy/Ping;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->removePing(I)Lcom/android/okhttp/internal/spdy/Ping;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/okhttp/internal/spdy/SpdyConnection;ILcom/android/okhttp/internal/spdy/Ping;)V
    .locals 0
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .param p1    # I
    .param p2    # Lcom/android/okhttp/internal/spdy/Ping;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->writePing(ILcom/android/okhttp/internal/spdy/Ping;)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/okhttp/internal/spdy/SpdyConnection;)Lcom/android/okhttp/internal/spdy/SpdyReader;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyReader:Lcom/android/okhttp/internal/spdy/SpdyReader;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/okhttp/internal/spdy/SpdyConnection;II)V
    .locals 0
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->close(II)V

    return-void
.end method

.method static synthetic access$900(Lcom/android/okhttp/internal/spdy/SpdyConnection;I)Lcom/android/okhttp/internal/spdy/SpdyStream;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->getStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;

    move-result-object v0

    return-object v0
.end method

.method private close(II)V
    .locals 12
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-boolean v10, Lcom/android/okhttp/internal/spdy/SpdyConnection;->$assertionsDisabled:Z

    if-nez v10, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    :cond_0
    const/4 v9, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->shutdown(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v8, 0x0

    const/4 v6, 0x0

    monitor-enter p0

    :try_start_1
    iget-object v10, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v10

    iget-object v11, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v11

    new-array v11, v11, [Lcom/android/okhttp/internal/spdy/SpdyStream;

    invoke-interface {v10, v11}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, [Lcom/android/okhttp/internal/spdy/SpdyStream;

    move-object v8, v0

    iget-object v10, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->clear()V

    const/4 v10, 0x0

    invoke-direct {p0, v10}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->setIdle(Z)V

    :cond_1
    iget-object v10, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->pings:Ljava/util/Map;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->pings:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v10

    iget-object v11, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->pings:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v11

    new-array v11, v11, [Lcom/android/okhttp/internal/spdy/Ping;

    invoke-interface {v10, v11}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, [Lcom/android/okhttp/internal/spdy/Ping;

    move-object v6, v0

    const/4 v10, 0x0

    iput-object v10, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->pings:Ljava/util/Map;

    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v8, :cond_4

    move-object v1, v8

    array-length v4, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v7, v1, v3

    :try_start_2
    invoke-virtual {v7, p2}, Lcom/android/okhttp/internal/spdy/SpdyStream;->close(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    move-object v9, v2

    goto :goto_0

    :catchall_0
    move-exception v10

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v10

    :catch_1
    move-exception v2

    if-eqz v9, :cond_3

    move-object v9, v2

    goto :goto_2

    :cond_4
    if-eqz v6, :cond_5

    move-object v1, v6

    array-length v4, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v4, :cond_5

    aget-object v5, v1, v3

    invoke-virtual {v5}, Lcom/android/okhttp/internal/spdy/Ping;->cancel()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_5
    :try_start_4
    iget-object v10, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyReader:Lcom/android/okhttp/internal/spdy/SpdyReader;

    invoke-virtual {v10}, Lcom/android/okhttp/internal/spdy/SpdyReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    iget-object v10, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    invoke-virtual {v10}, Lcom/android/okhttp/internal/spdy/SpdyWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_6
    :goto_5
    if-eqz v9, :cond_7

    throw v9

    :catch_2
    move-exception v2

    move-object v9, v2

    goto :goto_4

    :catch_3
    move-exception v2

    if-nez v9, :cond_6

    move-object v9, v2

    goto :goto_5

    :cond_7
    return-void
.end method

.method private declared-synchronized getStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;
    .locals 2
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/okhttp/internal/spdy/SpdyStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized removePing(I)Lcom/android/okhttp/internal/spdy/Ping;
    .locals 2
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->pings:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->pings:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/okhttp/internal/spdy/Ping;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setIdle(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->idleStartTimeNs:J

    return-void

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private writePing(ILcom/android/okhttp/internal/spdy/Ping;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/okhttp/internal/spdy/Ping;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    monitor-enter v1

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p2}, Lcom/android/okhttp/internal/spdy/Ping;->send()V

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, p1}, Lcom/android/okhttp/internal/spdy/SpdyWriter;->ping(II)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private writePingLater(ILcom/android/okhttp/internal/spdy/Ping;)V
    .locals 6

    sget-object v0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/android/okhttp/internal/spdy/SpdyConnection$3;

    const-string v2, "Spdy Writer %s ping %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->hostName:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection$3;-><init>(Lcom/android/okhttp/internal/spdy/SpdyConnection;Ljava/lang/String;ILcom/android/okhttp/internal/spdy/Ping;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->close(II)V

    return-void
.end method

.method public flush()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    iget-object v0, v0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getIdleStartTimeNs()J
    .locals 2

    iget-wide v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->idleStartTimeNs:J

    return-wide v0
.end method

.method public isIdle()Z
    .locals 4

    iget-wide v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->idleStartTimeNs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newStream(Ljava/util/List;ZZ)Lcom/android/okhttp/internal/spdy/SpdyStream;
    .locals 14
    .param p2    # Z
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)",
            "Lcom/android/okhttp/internal/spdy/SpdyStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p2, :cond_0

    const/4 v2, 0x0

    move v6, v2

    :goto_0
    if-eqz p3, :cond_1

    const/4 v2, 0x0

    :goto_1
    or-int v3, v6, v2

    const/4 v9, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v13, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    monitor-enter v13

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v2, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->shutdown:Z

    if-eqz v2, :cond_2

    new-instance v2, Ljava/io/IOException;

    const-string v6, "shutdown"

    invoke-direct {v2, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2

    :catchall_1
    move-exception v2

    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    :cond_0
    const/4 v2, 0x1

    move v6, v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    goto :goto_1

    :cond_2
    :try_start_3
    iget v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->nextStreamId:I

    iget v2, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->nextStreamId:I

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->nextStreamId:I

    new-instance v0, Lcom/android/okhttp/internal/spdy/SpdyStream;

    iget-object v7, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->settings:Lcom/android/okhttp/internal/spdy/Settings;

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/okhttp/internal/spdy/SpdyStream;-><init>(ILcom/android/okhttp/internal/spdy/SpdyConnection;IIILjava/util/List;Lcom/android/okhttp/internal/spdy/Settings;)V

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->setIdle(Z)V

    :cond_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v6, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    move v7, v3

    move v8, v1

    move v10, v4

    move v11, v5

    move-object v12, p1

    invoke-virtual/range {v6 .. v12}, Lcom/android/okhttp/internal/spdy/SpdyWriter;->synStream(IIIIILjava/util/List;)V

    monitor-exit v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-object v0
.end method

.method declared-synchronized removeStream(I)Lcom/android/okhttp/internal/spdy/SpdyStream;
    .locals 3
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/okhttp/internal/spdy/SpdyStream;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->streams:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->setIdle(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public shutdown(I)V
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    monitor-enter v2

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->shutdown:Z

    if-eqz v1, :cond_0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->shutdown:Z

    iget v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->lastGoodStreamId:I

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v0, p1}, Lcom/android/okhttp/internal/spdy/SpdyWriter;->goAway(III)V

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method writeFrame([BII)V
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    iget-object v0, v0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/DataOutputStream;->write([BII)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method writeSynReset(II)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    invoke-virtual {v0, p1, p2}, Lcom/android/okhttp/internal/spdy/SpdyWriter;->rstStream(II)V

    return-void
.end method

.method writeSynResetLater(II)V
    .locals 6

    sget-object v0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/android/okhttp/internal/spdy/SpdyConnection$1;

    const-string v2, "Spdy Writer %s stream %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->hostName:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection$1;-><init>(Lcom/android/okhttp/internal/spdy/SpdyConnection;Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method writeWindowUpdate(II)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->spdyWriter:Lcom/android/okhttp/internal/spdy/SpdyWriter;

    invoke-virtual {v0, p1, p2}, Lcom/android/okhttp/internal/spdy/SpdyWriter;->windowUpdate(II)V

    return-void
.end method

.method writeWindowUpdateLater(II)V
    .locals 6

    sget-object v0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/android/okhttp/internal/spdy/SpdyConnection$2;

    const-string v2, "Spdy Writer %s stream %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection;->hostName:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/android/okhttp/internal/spdy/SpdyConnection$2;-><init>(Lcom/android/okhttp/internal/spdy/SpdyConnection;Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method
