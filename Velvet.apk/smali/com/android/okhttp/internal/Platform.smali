.class public final Lcom/android/okhttp/internal/Platform;
.super Ljava/lang/Object;
.source "Platform.java"


# static fields
.field private static final PLATFORM:Lcom/android/okhttp/internal/Platform;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/okhttp/internal/Platform;

    invoke-direct {v0}, Lcom/android/okhttp/internal/Platform;-><init>()V

    sput-object v0, Lcom/android/okhttp/internal/Platform;->PLATFORM:Lcom/android/okhttp/internal/Platform;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lcom/android/okhttp/internal/Platform;
    .locals 1

    sget-object v0, Lcom/android/okhttp/internal/Platform;->PLATFORM:Lcom/android/okhttp/internal/Platform;

    return-object v0
.end method


# virtual methods
.method public enableTlsExtensions(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljavax/net/ssl/SSLSocket;
    .param p2    # Ljava/lang/String;

    instance-of v1, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setUseSessionTickets(Z)V

    invoke-virtual {v0, p2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setHostname(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getNpnSelectedProtocol(Ljavax/net/ssl/SSLSocket;)[B
    .locals 1
    .param p1    # Ljavax/net/ssl/SSLSocket;

    instance-of v0, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    invoke-virtual {p1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->getNpnSelectedProtocol()[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public logW(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/System;->logW(Ljava/lang/String;)V

    return-void
.end method

.method public newDeflaterOutputStream(Ljava/io/OutputStream;Ljava/util/zip/Deflater;Z)Ljava/io/OutputStream;
    .locals 1
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Ljava/util/zip/Deflater;
    .param p3    # Z

    new-instance v0, Ljava/util/zip/DeflaterOutputStream;

    invoke-direct {v0, p1, p2, p3}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;Z)V

    return-object v0
.end method

.method public setNpnProtocols(Ljavax/net/ssl/SSLSocket;[B)V
    .locals 1
    .param p1    # Ljavax/net/ssl/SSLSocket;
    .param p2    # [B

    instance-of v0, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    invoke-virtual {p1, p2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setNpnProtocols([B)V

    :cond_0
    return-void
.end method

.method public supportTlsIntolerantServer(Ljavax/net/ssl/SSLSocket;)V
    .locals 3
    .param p1    # Ljavax/net/ssl/SSLSocket;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SSLv3"

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    return-void
.end method

.method public tagSocket(Ljava/net/Socket;)V
    .locals 1
    .param p1    # Ljava/net/Socket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    invoke-static {}, Ldalvik/system/SocketTagger;->get()Ldalvik/system/SocketTagger;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldalvik/system/SocketTagger;->tag(Ljava/net/Socket;)V

    return-void
.end method

.method public toUriLenient(Ljava/net/URL;)Ljava/net/URI;
    .locals 1
    .param p1    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/net/URL;->toURILenient()Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method

.method public untagSocket(Ljava/net/Socket;)V
    .locals 1
    .param p1    # Ljava/net/Socket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    invoke-static {}, Ldalvik/system/SocketTagger;->get()Ldalvik/system/SocketTagger;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldalvik/system/SocketTagger;->untag(Ljava/net/Socket;)V

    return-void
.end method
