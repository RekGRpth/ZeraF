.class public final Lspeech/InterpretationProto$Slot;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "InterpretationProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/InterpretationProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Slot"
.end annotation


# instance fields
.field private alternates_:Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

.field private cachedSize:I

.field private confidence_:F

.field private decodedWords_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/DecodedWordProto$DecodedWord;",
            ">;"
        }
    .end annotation
.end field

.field private hasAlternates:Z

.field private hasConfidence:Z

.field private hasLiteral:Z

.field private hasName:Z

.field private hasPretextnormValue:Z

.field private hasValue:Z

.field private literal_:Ljava/lang/String;

.field private name_:Ljava/lang/String;

.field private pretextnormDecodedWords_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/DecodedWordProto$DecodedWord;",
            ">;"
        }
    .end annotation
.end field

.field private pretextnormValue_:Ljava/lang/String;

.field private subslot_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/InterpretationProto$Slot;",
            ">;"
        }
    .end annotation
.end field

.field private value_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->value_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->literal_:Ljava/lang/String;

    const/high16 v0, -0x40800000

    iput v0, p0, Lspeech/InterpretationProto$Slot;->confidence_:F

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->subslot_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->decodedWords_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->pretextnormValue_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->pretextnormDecodedWords_:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->alternates_:Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    const/4 v0, -0x1

    iput v0, p0, Lspeech/InterpretationProto$Slot;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addDecodedWords(Lspeech/DecodedWordProto$DecodedWord;)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # Lspeech/DecodedWordProto$DecodedWord;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->decodedWords_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->decodedWords_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->decodedWords_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPretextnormDecodedWords(Lspeech/DecodedWordProto$DecodedWord;)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # Lspeech/DecodedWordProto$DecodedWord;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->pretextnormDecodedWords_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->pretextnormDecodedWords_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->pretextnormDecodedWords_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSubslot(Lspeech/InterpretationProto$Slot;)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # Lspeech/InterpretationProto$Slot;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->subslot_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lspeech/InterpretationProto$Slot;->subslot_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->subslot_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAlternates()Lcom/google/speech/common/Alternates$RecognitionClientAlternates;
    .locals 1

    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->alternates_:Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lspeech/InterpretationProto$Slot;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lspeech/InterpretationProto$Slot;->cachedSize:I

    return v0
.end method

.method public getConfidence()F
    .locals 1

    iget v0, p0, Lspeech/InterpretationProto$Slot;->confidence_:F

    return v0
.end method

.method public getDecodedWordsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/DecodedWordProto$DecodedWord;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->decodedWords_:Ljava/util/List;

    return-object v0
.end method

.method public getLiteral()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->literal_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPretextnormDecodedWordsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/DecodedWordProto$DecodedWord;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->pretextnormDecodedWords_:Ljava/util/List;

    return-object v0
.end method

.method public getPretextnormValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->pretextnormValue_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasName()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasValue()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasConfidence()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getConfidence()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasLiteral()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getLiteral()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getSubslotList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/InterpretationProto$Slot;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getDecodedWordsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/DecodedWordProto$DecodedWord;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasPretextnormValue()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0x8

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getPretextnormValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getPretextnormDecodedWordsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/DecodedWordProto$DecodedWord;

    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasAlternates()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x12

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getAlternates()Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    iput v2, p0, Lspeech/InterpretationProto$Slot;->cachedSize:I

    return v2
.end method

.method public getSubslotList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/InterpretationProto$Slot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->subslot_:Ljava/util/List;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lspeech/InterpretationProto$Slot;->value_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAlternates()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasAlternates:Z

    return v0
.end method

.method public hasConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasConfidence:Z

    return v0
.end method

.method public hasLiteral()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasLiteral:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasName:Z

    return v0
.end method

.method public hasPretextnormValue()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasPretextnormValue:Z

    return v0
.end method

.method public hasValue()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasValue:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lspeech/InterpretationProto$Slot;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lspeech/InterpretationProto$Slot;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lspeech/InterpretationProto$Slot;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lspeech/InterpretationProto$Slot;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lspeech/InterpretationProto$Slot;->setName(Ljava/lang/String;)Lspeech/InterpretationProto$Slot;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lspeech/InterpretationProto$Slot;->setValue(Ljava/lang/String;)Lspeech/InterpretationProto$Slot;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lspeech/InterpretationProto$Slot;->setConfidence(F)Lspeech/InterpretationProto$Slot;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lspeech/InterpretationProto$Slot;->setLiteral(Ljava/lang/String;)Lspeech/InterpretationProto$Slot;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lspeech/InterpretationProto$Slot;

    invoke-direct {v1}, Lspeech/InterpretationProto$Slot;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lspeech/InterpretationProto$Slot;->addSubslot(Lspeech/InterpretationProto$Slot;)Lspeech/InterpretationProto$Slot;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lspeech/DecodedWordProto$DecodedWord;

    invoke-direct {v1}, Lspeech/DecodedWordProto$DecodedWord;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lspeech/InterpretationProto$Slot;->addDecodedWords(Lspeech/DecodedWordProto$DecodedWord;)Lspeech/InterpretationProto$Slot;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lspeech/InterpretationProto$Slot;->setPretextnormValue(Ljava/lang/String;)Lspeech/InterpretationProto$Slot;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lspeech/DecodedWordProto$DecodedWord;

    invoke-direct {v1}, Lspeech/DecodedWordProto$DecodedWord;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lspeech/InterpretationProto$Slot;->addPretextnormDecodedWords(Lspeech/DecodedWordProto$DecodedWord;)Lspeech/InterpretationProto$Slot;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    invoke-direct {v1}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lspeech/InterpretationProto$Slot;->setAlternates(Lcom/google/speech/common/Alternates$RecognitionClientAlternates;)Lspeech/InterpretationProto$Slot;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x92 -> :sswitch_9
    .end sparse-switch
.end method

.method public setAlternates(Lcom/google/speech/common/Alternates$RecognitionClientAlternates;)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasAlternates:Z

    iput-object p1, p0, Lspeech/InterpretationProto$Slot;->alternates_:Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    return-object p0
.end method

.method public setConfidence(F)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasConfidence:Z

    iput p1, p0, Lspeech/InterpretationProto$Slot;->confidence_:F

    return-object p0
.end method

.method public setLiteral(Ljava/lang/String;)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasLiteral:Z

    iput-object p1, p0, Lspeech/InterpretationProto$Slot;->literal_:Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasName:Z

    iput-object p1, p0, Lspeech/InterpretationProto$Slot;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setPretextnormValue(Ljava/lang/String;)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasPretextnormValue:Z

    iput-object p1, p0, Lspeech/InterpretationProto$Slot;->pretextnormValue_:Ljava/lang/String;

    return-object p0
.end method

.method public setValue(Ljava/lang/String;)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/InterpretationProto$Slot;->hasValue:Z

    iput-object p1, p0, Lspeech/InterpretationProto$Slot;->value_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasValue()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasConfidence()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getConfidence()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasLiteral()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getLiteral()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getSubslotList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/InterpretationProto$Slot;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getDecodedWordsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/DecodedWordProto$DecodedWord;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasPretextnormValue()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getPretextnormValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getPretextnormDecodedWordsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/DecodedWordProto$DecodedWord;

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->hasAlternates()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x12

    invoke-virtual {p0}, Lspeech/InterpretationProto$Slot;->getAlternates()Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    return-void
.end method
