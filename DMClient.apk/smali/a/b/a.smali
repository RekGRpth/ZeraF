.class public La/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/b/a$a;,
        La/b/a$b;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:La/a/b/e;

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;La/a/b/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, La/b/a;->a:Ljava/lang/String;

    iput-object p2, p0, La/b/a;->b:La/a/b/e;

    return-void
.end method

.method public static a(I)La/b/a$a;
    .locals 2

    new-instance v0, La/b/a$a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, La/b/a$a;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public a(La/a/b/a/f;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, La/b/a;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "CordovaPlugin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempted to send a second callback for ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, La/b/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nResult was: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, La/a/b/a/f;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, La/a/b/a/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, La/b/a;->c:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, La/b/a;->b:La/a/b/e;

    iget-object v1, p0, La/b/a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, La/a/b/e;->a(La/a/b/a/f;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lorg/json/JSONArray;)V
    .locals 2

    new-instance v0, La/a/b/a/f;

    sget-object v1, La/a/b/a/f$a;->b:La/a/b/a/f$a;

    invoke-direct {v0, v1, p1}, La/a/b/a/f;-><init>(La/a/b/a/f$a;Lorg/json/JSONArray;)V

    invoke-virtual {p0, v0}, La/b/a;->a(La/a/b/a/f;)V

    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 2

    new-instance v0, La/a/b/a/f;

    sget-object v1, La/a/b/a/f$a;->b:La/a/b/a/f$a;

    invoke-direct {v0, v1, p1}, La/a/b/a/f;-><init>(La/a/b/a/f$a;Lorg/json/JSONObject;)V

    invoke-virtual {p0, v0}, La/b/a;->a(La/a/b/a/f;)V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, La/b/a;->c:Z

    return v0
.end method

.method public b()V
    .locals 2

    new-instance v0, La/a/b/a/f;

    sget-object v1, La/a/b/a/f$a;->b:La/a/b/a/f$a;

    invoke-direct {v0, v1}, La/a/b/a/f;-><init>(La/a/b/a/f$a;)V

    invoke-virtual {p0, v0}, La/b/a;->a(La/a/b/a/f;)V

    return-void
.end method

.method public b(I)V
    .locals 3

    new-instance v0, La/a/b/a/f;

    sget-object v1, La/a/b/a/f$a;->f:La/a/b/a/f$a;

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, La/a/b/a/f;-><init>(La/a/b/a/f$a;I)V

    invoke-virtual {p0, v0}, La/b/a;->a(La/a/b/a/f;)V

    return-void
.end method
