.class public final La/b/a$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private final a:Lb/e;

.field private final b:Lb/Q;


# direct methods
.method protected constructor <init>(La/b/a$a;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, La/b/a$a;->a(La/b/a$a;)Lb/e;

    move-result-object v0

    iput-object v0, p0, La/b/a$b;->a:Lb/e;

    iget-object v0, p0, La/b/a$b;->a:Lb/e;

    invoke-virtual {v0, p2}, Lb/e;->a(I)Lb/Q;

    move-result-object v0

    iput-object v0, p0, La/b/a$b;->b:Lb/Q;

    invoke-static {}, La/b/a$b;->c()V

    return-void
.end method

.method private static final c()V
    .locals 2

    const/4 v0, 0x0

    invoke-static {v0}, Lb/ae;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x23

    invoke-static {v0}, Lb/ae;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, La/b/b;

    invoke-static {}, Lb/ae;->a()I

    move-result v1

    invoke-direct {v0, v1}, La/b/b;-><init>(I)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, La/b/a$b;->b:Lb/Q;

    invoke-virtual {v1, p1}, Lb/Q;->e(I)Lb/y;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lb/y;->f()[B

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    :cond_0
    return-object v0

    :cond_1
    invoke-static {}, La/b/a$b;->c()V

    move-object v1, v0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, La/b/a$b;->b:Lb/Q;

    invoke-virtual {v0}, Lb/Q;->r()V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, La/b/a$b;->b:Lb/Q;

    invoke-virtual {v0, p1}, Lb/Q;->a(Ljava/lang/String;)Z

    invoke-static {}, La/b/a$b;->c()V

    return-void
.end method

.method public final a([B)V
    .locals 2

    iget-object v0, p0, La/b/a$b;->b:Lb/Q;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lb/Q;->b(ILjava/lang/Object;)Z

    const v0, 0x9523dfd

    invoke-static {v0}, Lb/ae;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, La/b/a$b;->c()V

    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, La/b/a$b;->b:Lb/Q;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Lb/Q;->d(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
