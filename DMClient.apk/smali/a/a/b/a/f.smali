.class public final La/a/b/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/a/b/a/f$a;
    }
.end annotation


# static fields
.field private static f:[Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:I

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "No result"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "OK"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Class not found"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Illegal access"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Instantiation error"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Malformed url"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "IO error"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Invalid action"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "JSON error"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Error"

    aput-object v2, v0, v1

    sput-object v0, La/a/b/a/f;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(La/a/b/a/f$a;)V
    .locals 2

    sget-object v0, La/a/b/a/f;->f:[Ljava/lang/String;

    invoke-virtual {p1}, La/a/b/a/f$a;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    invoke-direct {p0, p1, v0}, La/a/b/a/f;-><init>(La/a/b/a/f$a;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(La/a/b/a/f$a;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/b/a/f;->c:Z

    invoke-virtual {p1}, La/a/b/a/f$a;->ordinal()I

    move-result v0

    iput v0, p0, La/a/b/a/f;->a:I

    const/4 v0, 0x3

    iput v0, p0, La/a/b/a/f;->b:I

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, La/a/b/a/f;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(La/a/b/a/f$a;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/b/a/f;->c:Z

    invoke-virtual {p1}, La/a/b/a/f$a;->ordinal()I

    move-result v0

    iput v0, p0, La/a/b/a/f;->a:I

    if-nez p2, :cond_0

    const/4 v0, 0x5

    :goto_0
    iput v0, p0, La/a/b/a/f;->b:I

    iput-object p2, p0, La/a/b/a/f;->d:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(La/a/b/a/f$a;Lorg/json/JSONArray;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/b/a/f;->c:Z

    invoke-virtual {p1}, La/a/b/a/f$a;->ordinal()I

    move-result v0

    iput v0, p0, La/a/b/a/f;->a:I

    const/4 v0, 0x2

    iput v0, p0, La/a/b/a/f;->b:I

    invoke-virtual {p2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, La/a/b/a/f;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(La/a/b/a/f$a;Lorg/json/JSONObject;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/b/a/f;->c:Z

    invoke-virtual {p1}, La/a/b/a/f$a;->ordinal()I

    move-result v0

    iput v0, p0, La/a/b/a/f;->a:I

    const/4 v0, 0x2

    iput v0, p0, La/a/b/a/f;->b:I

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, La/a/b/a/f;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, La/a/b/a/f;->a:I

    return v0
.end method

.method public final a(Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, La/a/b/a/f;->c:Z

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, La/a/b/a/f;->b:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, La/a/b/a/f;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/b/a/f;->d:Ljava/lang/String;

    invoke-static {v0}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, La/a/b/a/f;->e:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, La/a/b/a/f;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, La/a/b/a/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, La/a/b/a/f;->c:Z

    return v0
.end method
