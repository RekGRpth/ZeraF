.class public La/a/b/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:La/a/b/a/b;

.field public c:Z

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, La/a/b/a/d;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, La/a/b/a/d;->d:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, La/a/b/a/d;->b:La/a/b/a/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/b/a/d;->c:Z

    iput-object p1, p0, La/a/b/a/d;->a:Ljava/lang/String;

    iput-object p2, p0, La/a/b/a/d;->d:Ljava/lang/String;

    iput-boolean p3, p0, La/a/b/a/d;->c:Z

    return-void
.end method


# virtual methods
.method public a(La/a/b/e;La/a/b/a/a;)La/a/b/a/b;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, La/a/b/a/d;->b:La/a/b/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/a/d;->b:La/a/b/a/b;

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, La/a/b/a/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    move-object v2, v0

    :goto_1
    if-eqz v2, :cond_1

    const-class v0, La/a/b/a/b;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/a/b;

    iput-object v0, p0, La/a/b/a/d;->b:La/a/b/a/b;

    iget-object v0, p0, La/a/b/a/d;->b:La/a/b/a/b;

    invoke-virtual {v0, p2, p1}, La/a/b/a/b;->a(La/a/b/a/a;La/a/b/e;)V

    iget-object v0, p0, La/a/b/a/d;->b:La/a/b/a/b;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error adding plugin "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, La/a/b/a/d;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v2, v1

    goto :goto_1
.end method
