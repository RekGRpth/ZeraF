.class public La/a/b/m;
.super La/a/b/a/b;
.source "SourceFile"


# instance fields
.field a:Landroid/net/ConnectivityManager;

.field private b:La/b/a;

.field private e:Z

.field private f:Landroid/content/BroadcastReceiver;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, La/a/b/a/b;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/b/m;->e:Z

    const-string v0, ""

    iput-object v0, p0, La/a/b/m;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, La/a/b/m;->f:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(Landroid/net/NetworkInfo;)Ljava/lang/String;
    .locals 4

    const-string v0, "none"

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "none"

    :cond_0
    :goto_0
    const-string v1, "CordovaNetworkManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Connection Type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_1
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "wifi"

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mobile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "gsm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "gprs"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "edge"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const-string v0, "2g"

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cdma"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "umts"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1xrtt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ehrpd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "hsupa"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "hsdpa"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "hspa"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    const-string v0, "3g"

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "lte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "umb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hspa+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_7
    const-string v0, "4g"

    goto/16 :goto_0

    :cond_8
    const-string v0, "none"

    goto/16 :goto_0

    :cond_9
    const-string v0, "unknown"

    goto/16 :goto_0
.end method

.method static synthetic a(La/a/b/m;Landroid/net/NetworkInfo;)V
    .locals 3

    invoke-direct {p0, p1}, La/a/b/m;->a(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, La/a/b/m;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, La/a/b/m;->b:La/b/a;

    if-eqz v1, :cond_0

    new-instance v1, La/a/b/a/f;

    sget-object v2, La/a/b/a/f$a;->b:La/a/b/a/f$a;

    invoke-direct {v1, v2, v0}, La/a/b/a/f;-><init>(La/a/b/a/f$a;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, La/a/b/a/f;->a(Z)V

    iget-object v2, p0, La/a/b/m;->b:La/b/a;

    invoke-virtual {v2, v1}, La/b/a;->a(La/a/b/a/f;)V

    :cond_0
    iget-object v1, p0, La/a/b/m;->c:La/a/b/e;

    const-string v2, "networkconnection"

    invoke-virtual {v1, v2, v0}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, La/a/b/m;->g:Ljava/lang/String;

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, La/a/b/m;->f:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, La/a/b/m;->e:Z

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, La/a/b/m;->d:La/a/b/a/a;

    invoke-interface {v0}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, La/a/b/m;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/b/m;->e:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "NetworkManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error unregistering network receiver: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(La/a/b/a/a;La/a/b/e;)V
    .locals 3

    invoke-super {p0, p1, p2}, La/a/b/a/b;->a(La/a/b/a/a;La/a/b/e;)V

    invoke-interface {p1}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, La/a/b/m;->a:Landroid/net/ConnectivityManager;

    const/4 v0, 0x0

    iput-object v0, p0, La/a/b/m;->b:La/b/a;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, La/a/b/m;->f:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    new-instance v1, La/a/b/m$1;

    invoke-direct {v1, p0}, La/a/b/m$1;-><init>(La/a/b/m;)V

    iput-object v1, p0, La/a/b/m;->f:Landroid/content/BroadcastReceiver;

    invoke-interface {p1}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, La/a/b/m;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, La/a/b/m;->e:Z

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONArray;La/b/a;)Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, "getConnectionInfo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object p3, p0, La/a/b/m;->b:La/b/a;

    iget-object v1, p0, La/a/b/m;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    new-instance v2, La/a/b/a/f;

    sget-object v3, La/a/b/a/f$a;->b:La/a/b/a/f$a;

    invoke-direct {p0, v1}, La/a/b/m;->a(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, La/a/b/a/f;-><init>(La/a/b/a/f$a;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, La/a/b/a/f;->a(Z)V

    invoke-virtual {p3, v2}, La/b/a;->a(La/a/b/a/f;)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
