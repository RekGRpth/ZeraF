.class public final La/a/b/l;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/a/b/l$a;,
        La/a/b/l$b;,
        La/a/b/l$c;,
        La/a/b/l$d;,
        La/a/b/l$e;
    }
.end annotation


# static fields
.field private static a:I


# instance fields
.field private b:I

.field private c:Z

.field private final d:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "La/a/b/l$b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:[La/a/b/l$a;

.field private final f:La/a/b/a/a;

.field private final g:La/a/b/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, La/a/b/l;->a:I

    return-void
.end method

.method public constructor <init>(La/a/b/e;La/a/b/a/a;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    iput-object p2, p0, La/a/b/l;->f:La/a/b/a/a;

    iput-object p1, p0, La/a/b/l;->g:La/a/b/e;

    const/4 v0, 0x4

    new-array v0, v0, [La/a/b/l$a;

    iput-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    const/4 v1, 0x0

    aput-object v1, v0, v3

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    const/4 v1, 0x1

    new-instance v2, La/a/b/l$c;

    invoke-direct {v2, p0, v3}, La/a/b/l$c;-><init>(La/a/b/l;B)V

    aput-object v2, v0, v1

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    const/4 v1, 0x2

    new-instance v2, La/a/b/l$d;

    invoke-direct {v2, p0}, La/a/b/l$d;-><init>(La/a/b/l;)V

    aput-object v2, v0, v1

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    const/4 v1, 0x3

    new-instance v2, La/a/b/l$e;

    invoke-direct {v2, p0, v3}, La/a/b/l$e;-><init>(La/a/b/l;B)V

    aput-object v2, v0, v1

    invoke-virtual {p0}, La/a/b/l;->a()V

    return-void
.end method

.method static synthetic a(La/a/b/l;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, La/a/b/l;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(La/a/b/l$b;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, La/a/b/l;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    iget v1, p0, La/a/b/l;->b:I

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    iget v1, p0, La/a/b/l;->b:I

    aget-object v0, v0, v1

    invoke-interface {v0}, La/a/b/l$a;->a()V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(La/a/b/l;)La/a/b/e;
    .locals 1

    iget-object v0, p0, La/a/b/l;->g:La/a/b/e;

    return-object v0
.end method

.method static synthetic c(La/a/b/l;)La/a/b/a/a;
    .locals 1

    iget-object v0, p0, La/a/b/l;->f:La/a/b/a/a;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    monitor-exit p0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v5, v2

    move v3, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ne v5, v0, :cond_5

    move v4, v1

    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    if-eqz v4, :cond_6

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    move v3, v2

    :goto_4
    if-lt v3, v5, :cond_7

    if-nez v4, :cond_2

    const-string v0, "window.setTimeout(function(){cordova.require(\'cordova/plugin/android/polling\').pollOnce();},0);"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    if-eqz v4, :cond_9

    move v0, v1

    :goto_5
    if-lt v0, v5, :cond_a

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/l$b;

    invoke-virtual {v0}, La/a/b/l$b;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x32

    if-lez v5, :cond_4

    add-int v6, v3, v0

    sget v7, La/a/b/l;->a:I

    if-le v6, v7, :cond_4

    sget v6, La/a/b/l;->a:I

    if-gtz v6, :cond_1

    :cond_4
    add-int/2addr v3, v0

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_5
    move v4, v2

    goto :goto_2

    :cond_6
    const/16 v0, 0x64

    goto :goto_3

    :cond_7
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/l$b;

    if-eqz v4, :cond_8

    add-int/lit8 v7, v3, 0x1

    if-ne v7, v5, :cond_8

    invoke-virtual {v0, v6}, La/a/b/l$b;->a(Ljava/lang/StringBuilder;)V

    :goto_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_8
    const-string v7, "try{"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, La/a/b/l$b;->a(Ljava/lang/StringBuilder;)V

    const-string v0, "}finally{"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_9
    move v0, v2

    goto :goto_5

    :cond_a
    const/16 v1, 0x7d

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method static synthetic d(La/a/b/l;)Ljava/util/LinkedList;
    .locals 1

    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, La/a/b/l;->a(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 3

    if-ltz p1, :cond_0

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    array-length v0, v0

    if-lt p1, v0, :cond_2

    :cond_0
    const-string v0, "JsMessageQueue"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid NativeToJsBridgeMode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, La/a/b/l;->b:I

    if-eq p1, v0, :cond_1

    const-string v0, "JsMessageQueue"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Set native->JS mode to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    iput p1, p0, La/a/b/l;->b:I

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    aget-object v0, v0, p1

    iget-boolean v1, p0, La/a/b/l;->c:Z

    if-nez v1, :cond_3

    iget-object v1, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    invoke-interface {v0}, La/a/b/l$a;->a()V

    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(La/a/b/a/f;Ljava/lang/String;)V
    .locals 3

    if-nez p2, :cond_1

    const-string v0, "JsMessageQueue"

    const-string v1, "Got plugin result with no callbackId"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, La/a/b/a/f;->a()I

    move-result v0

    sget-object v1, La/a/b/a/f$a;->a:La/a/b/a/f$a;

    invoke-virtual {v1}, La/a/b/a/f$a;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1}, La/a/b/a/f;->e()Z

    move-result v1

    if-eqz v0, :cond_2

    if-nez v1, :cond_0

    :cond_2
    new-instance v0, La/a/b/l$b;

    invoke-direct {v0, p1, p2}, La/a/b/l$b;-><init>(La/a/b/a/f;Ljava/lang/String;)V

    invoke-direct {p0, v0}, La/a/b/l;->a(La/a/b/l$b;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    new-instance v0, La/a/b/l$b;

    invoke-direct {v0, p1}, La/a/b/l$b;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, La/a/b/l;->a(La/a/b/l$b;)V

    return-void
.end method

.method public final a(Z)V
    .locals 3

    iget-boolean v0, p0, La/a/b/l;->c:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "JsMessageQueue"

    const-string v1, "nested call to setPaused detected."

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    iput-boolean p1, p0, La/a/b/l;->c:Z

    if-nez p1, :cond_2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    iget v1, p0, La/a/b/l;->b:I

    aget-object v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, La/a/b/l;->e:[La/a/b/l$a;

    iget v1, p0, La/a/b/l;->b:I

    aget-object v0, v0, v1

    invoke-interface {v0}, La/a/b/l$a;->a()V

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 11

    const/16 v4, 0x53

    const/4 v5, 0x1

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit p0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v7, v2

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    move v1, v2

    :goto_2
    if-lt v1, v7, :cond_5

    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x2a

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/l$b;

    invoke-virtual {v0}, La/a/b/l$b;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v0, v6

    add-int/lit8 v0, v0, 0x1

    if-lez v7, :cond_4

    add-int v6, v1, v0

    sget v8, La/a/b/l;->a:I

    if-le v6, v8, :cond_4

    sget v6, La/a/b/l;->a:I

    if-gtz v6, :cond_1

    :cond_4
    add-int/2addr v1, v0

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, La/a/b/l;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b/l$b;

    invoke-virtual {v0}, La/a/b/l$b;->a()I

    move-result v3

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, v0, La/a/b/l$b;->b:La/a/b/a/f;

    if-nez v3, :cond_6

    const/16 v3, 0x4a

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, La/a/b/l$b;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_6
    iget-object v3, v0, La/a/b/l$b;->b:La/a/b/a/f;

    invoke-virtual {v3}, La/a/b/a/f;->a()I

    move-result v9

    sget-object v3, La/a/b/a/f$a;->a:La/a/b/a/f$a;

    invoke-virtual {v3}, La/a/b/a/f$a;->ordinal()I

    move-result v3

    if-ne v9, v3, :cond_8

    move v6, v5

    :goto_4
    sget-object v3, La/a/b/a/f$a;->b:La/a/b/a/f$a;

    invoke-virtual {v3}, La/a/b/a/f$a;->ordinal()I

    move-result v3

    if-ne v9, v3, :cond_9

    move v3, v5

    :goto_5
    iget-object v10, v0, La/a/b/l$b;->b:La/a/b/a/f;

    invoke-virtual {v10}, La/a/b/a/f;->e()Z

    move-result v10

    if-nez v6, :cond_7

    if-eqz v3, :cond_a

    :cond_7
    move v3, v4

    :goto_6
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v10, :cond_b

    const/16 v3, 0x31

    :goto_7
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v0, La/a/b/l$b;->a:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, v0, La/a/b/l$b;->b:La/a/b/a/f;

    invoke-virtual {v3}, La/a/b/a/f;->b()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    iget-object v0, v0, La/a/b/l$b;->b:La/a/b/a/f;

    invoke-virtual {v0}, La/a/b/a/f;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_8
    move v6, v2

    goto :goto_4

    :cond_9
    move v3, v2

    goto :goto_5

    :cond_a
    const/16 v3, 0x46

    goto :goto_6

    :cond_b
    const/16 v3, 0x30

    goto :goto_7

    :pswitch_1
    iget-object v0, v0, La/a/b/l$b;->b:La/a/b/a/f;

    invoke-virtual {v0}, La/a/b/a/f;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    :pswitch_2
    const/16 v0, 0x4e

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    :pswitch_3
    const/16 v3, 0x6e

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, La/a/b/l$b;->b:La/a/b/a/f;

    invoke-virtual {v0}, La/a/b/a/f;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :pswitch_4
    const/16 v3, 0x73

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, v0, La/a/b/l$b;->b:La/a/b/a/f;

    invoke-virtual {v0}, La/a/b/a/f;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :pswitch_5
    const/16 v3, 0x53

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, v0, La/a/b/l$b;->b:La/a/b/a/f;

    invoke-virtual {v0}, La/a/b/a/f;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :pswitch_6
    const/16 v3, 0x41

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, v0, La/a/b/l$b;->b:La/a/b/a/f;

    invoke-virtual {v0}, La/a/b/a/f;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method
