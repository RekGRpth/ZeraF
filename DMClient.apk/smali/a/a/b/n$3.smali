.class final La/a/b/n$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = La/a/b/n;->b(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;La/b/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:La/a/b/a/a;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Landroid/widget/EditText;

.field private final synthetic e:Lorg/json/JSONArray;

.field private final synthetic f:La/b/a;


# direct methods
.method constructor <init>(La/a/b/n;La/a/b/a/a;Ljava/lang/String;Ljava/lang/String;Landroid/widget/EditText;Lorg/json/JSONArray;La/b/a;)V
    .locals 0

    iput-object p2, p0, La/a/b/n$3;->a:La/a/b/a/a;

    iput-object p3, p0, La/a/b/n$3;->b:Ljava/lang/String;

    iput-object p4, p0, La/a/b/n$3;->c:Ljava/lang/String;

    iput-object p5, p0, La/a/b/n$3;->d:Landroid/widget/EditText;

    iput-object p6, p0, La/a/b/n$3;->e:Lorg/json/JSONArray;

    iput-object p7, p0, La/a/b/n$3;->f:La/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, La/a/b/n$3;->a:La/a/b/a/a;

    invoke-interface {v1}, La/a/b/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, La/a/b/n$3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, La/a/b/n$3;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, La/a/b/n$3;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    iget-object v2, p0, La/a/b/n$3;->e:Lorg/json/JSONArray;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_0

    :try_start_0
    iget-object v2, p0, La/a/b/n$3;->e:Lorg/json/JSONArray;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, La/a/b/n$3$1;

    iget-object v4, p0, La/a/b/n$3;->d:Landroid/widget/EditText;

    iget-object v5, p0, La/a/b/n$3;->f:La/b/a;

    invoke-direct {v3, p0, v1, v4, v5}, La/a/b/n$3$1;-><init>(La/a/b/n$3;Lorg/json/JSONObject;Landroid/widget/EditText;La/b/a;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    :goto_0
    iget-object v2, p0, La/a/b/n$3;->e:Lorg/json/JSONArray;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-le v2, v6, :cond_1

    :try_start_1
    iget-object v2, p0, La/a/b/n$3;->e:Lorg/json/JSONArray;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, La/a/b/n$3$2;

    iget-object v4, p0, La/a/b/n$3;->d:Landroid/widget/EditText;

    iget-object v5, p0, La/a/b/n$3;->f:La/b/a;

    invoke-direct {v3, p0, v1, v4, v5}, La/a/b/n$3$2;-><init>(La/a/b/n$3;Lorg/json/JSONObject;Landroid/widget/EditText;La/b/a;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    iget-object v2, p0, La/a/b/n$3;->e:Lorg/json/JSONArray;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-le v2, v7, :cond_2

    :try_start_2
    iget-object v2, p0, La/a/b/n$3;->e:Lorg/json/JSONArray;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, La/a/b/n$3$3;

    iget-object v4, p0, La/a/b/n$3;->d:Landroid/widget/EditText;

    iget-object v5, p0, La/a/b/n$3;->f:La/b/a;

    invoke-direct {v3, p0, v1, v4, v5}, La/a/b/n$3$3;-><init>(La/a/b/n$3;Lorg/json/JSONObject;Landroid/widget/EditText;La/b/a;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :goto_2
    new-instance v2, La/a/b/n$3$4;

    iget-object v3, p0, La/a/b/n$3;->d:Landroid/widget/EditText;

    iget-object v4, p0, La/a/b/n$3;->f:La/b/a;

    invoke-direct {v2, p0, v1, v3, v4}, La/a/b/n$3$4;-><init>(La/a/b/n$3;Lorg/json/JSONObject;Landroid/widget/EditText;La/b/a;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :catch_0
    move-exception v2

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_1

    :catch_2
    move-exception v2

    goto :goto_0
.end method
