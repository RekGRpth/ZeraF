.class final La/a/b/g$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/a/b/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:La/a/b/g;


# direct methods
.method constructor <init>(La/a/b/g;)V
    .locals 0

    iput-object p1, p0, La/a/b/g$1;->a:La/a/b/g;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "state"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "state"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "Device"

    const-string v1, "Telephone RINGING"

    invoke-static {v0, v1}, La/a/b/a/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, La/a/b/g$1;->a:La/a/b/g;

    iget-object v0, v0, La/a/b/g;->c:La/a/b/e;

    const-string v1, "telephone"

    const-string v2, "ringing"

    invoke-virtual {v0, v1, v2}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "Device"

    const-string v1, "Telephone OFFHOOK"

    invoke-static {v0, v1}, La/a/b/a/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, La/a/b/g$1;->a:La/a/b/g;

    iget-object v0, v0, La/a/b/g;->c:La/a/b/e;

    const-string v1, "telephone"

    const-string v2, "offhook"

    invoke-virtual {v0, v1, v2}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Device"

    const-string v1, "Telephone IDLE"

    invoke-static {v0, v1}, La/a/b/a/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, La/a/b/g$1;->a:La/a/b/g;

    iget-object v0, v0, La/a/b/g;->c:La/a/b/e;

    const-string v1, "telephone"

    const-string v2, "idle"

    invoke-virtual {v0, v1, v2}, La/a/b/e;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
