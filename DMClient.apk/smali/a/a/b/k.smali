.class public final La/a/b/k;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:La/a/b/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput v0, p0, La/a/b/k;->a:I

    iput v0, p0, La/a/b/k;->b:I

    iput v0, p0, La/a/b/k;->c:I

    iput v0, p0, La/a/b/k;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, La/a/b/k;->e:La/a/b/h;

    iput p2, p0, La/a/b/k;->c:I

    iput p3, p0, La/a/b/k;->d:I

    check-cast p1, La/a/b/h;

    iput-object p1, p0, La/a/b/k;->e:La/a/b/h;

    return-void
.end method


# virtual methods
.method protected final onMeasure(II)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    const-string v0, "SoftKeyboardDetect"

    const-string v1, "We are in our onMeasure method"

    invoke-static {v0, v1}, La/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const-string v2, "SoftKeyboardDetect"

    const-string v3, "Old Height = %d"

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, La/a/b/k;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, La/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v2, "SoftKeyboardDetect"

    const-string v3, "Height = %d"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, La/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v2, "SoftKeyboardDetect"

    const-string v3, "Old Width = %d"

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, La/a/b/k;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, La/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v2, "SoftKeyboardDetect"

    const-string v3, "Width = %d"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, La/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget v2, p0, La/a/b/k;->a:I

    if-eqz v2, :cond_0

    iget v2, p0, La/a/b/k;->a:I

    if-ne v2, v0, :cond_2

    :cond_0
    const-string v2, "SoftKeyboardDetect"

    const-string v3, "Ignore this event"

    invoke-static {v2, v3}, La/a/b/a/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    iput v0, p0, La/a/b/k;->a:I

    iput v1, p0, La/a/b/k;->b:I

    return-void

    :cond_2
    iget v2, p0, La/a/b/k;->d:I

    if-ne v2, v1, :cond_3

    iget v2, p0, La/a/b/k;->d:I

    iget v3, p0, La/a/b/k;->c:I

    iput v3, p0, La/a/b/k;->d:I

    iput v2, p0, La/a/b/k;->c:I

    const-string v2, "SoftKeyboardDetect"

    const-string v3, "Orientation Change"

    invoke-static {v2, v3}, La/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget v2, p0, La/a/b/k;->a:I

    if-le v0, v2, :cond_4

    iget-object v2, p0, La/a/b/k;->e:La/a/b/h;

    if-eqz v2, :cond_1

    iget-object v2, p0, La/a/b/k;->e:La/a/b/h;

    iget-object v2, v2, La/a/b/h;->appView:La/a/b/e;

    const-string v3, "cordova.fireDocumentEvent(\'hidekeyboard\');"

    invoke-virtual {v2, v3}, La/a/b/e;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget v2, p0, La/a/b/k;->a:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, La/a/b/k;->e:La/a/b/h;

    if-eqz v2, :cond_1

    iget-object v2, p0, La/a/b/k;->e:La/a/b/h;

    iget-object v2, v2, La/a/b/h;->appView:La/a/b/e;

    const-string v3, "cordova.fireDocumentEvent(\'showkeyboard\');"

    invoke-virtual {v2, v3}, La/a/b/e;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
