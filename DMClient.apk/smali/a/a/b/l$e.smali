.class final La/a/b/l$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements La/a/b/l$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/a/b/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "e"
.end annotation


# instance fields
.field private a:Ljava/lang/reflect/Method;

.field private b:Ljava/lang/Object;

.field private c:Z

.field private synthetic d:La/a/b/l;


# direct methods
.method private constructor <init>(La/a/b/l;)V
    .locals 0

    iput-object p1, p0, La/a/b/l$e;->d:La/a/b/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(La/a/b/l;B)V
    .locals 0

    invoke-direct {p0, p1}, La/a/b/l$e;-><init>(La/a/b/l;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    const/4 v5, 0x1

    iget-object v0, p0, La/a/b/l$e;->a:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    iget-boolean v0, p0, La/a/b/l$e;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/b/l$e;->d:La/a/b/l;

    invoke-static {v0}, La/a/b/l;->b(La/a/b/l;)La/a/b/e;

    move-result-object v0

    const-class v1, Landroid/webkit/WebView;

    :try_start_0
    const-string v2, "mProvider"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    iget-object v3, p0, La/a/b/l$e;->d:La/a/b/l;

    invoke-static {v3}, La/a/b/l;->b(La/a/b/l;)La/a/b/e;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_0
    :try_start_1
    const-string v2, "mWebViewCore"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, La/a/b/l$e;->b:Ljava/lang/Object;

    iget-object v0, p0, La/a/b/l$e;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b/l$e;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "sendMessage"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/os/Message;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, La/a/b/l$e;->a:Ljava/lang/reflect/Method;

    iget-object v0, p0, La/a/b/l$e;->a:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_1
    iget-object v0, p0, La/a/b/l$e;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    iget-object v0, p0, La/a/b/l$e;->d:La/a/b/l;

    invoke-static {v0}, La/a/b/l;->a(La/a/b/l;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0xc2

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    :try_start_2
    iget-object v1, p0, La/a/b/l$e;->a:Ljava/lang/reflect/Method;

    iget-object v2, p0, La/a/b/l$e;->b:Ljava/lang/Object;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    :goto_2
    return-void

    :catch_0
    move-exception v2

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_0

    :catch_1
    move-exception v0

    iput-boolean v5, p0, La/a/b/l$e;->c:Z

    const-string v1, "JsMessageQueue"

    const-string v2, "PrivateApiBridgeMode failed to find the expected APIs."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "JsMessageQueue"

    const-string v2, "Reflection message bridge failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method
