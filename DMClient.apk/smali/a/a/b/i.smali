.class final La/a/b/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:La/a/b/a/e;

.field private b:La/a/b/l;


# direct methods
.method public constructor <init>(La/a/b/a/e;La/a/b/l;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, La/a/b/i;->a:La/a/b/a/e;

    iput-object p2, p0, La/a/b/i;->b:La/a/b/l;

    return-void
.end method


# virtual methods
.method public final exec(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, La/a/b/i;->b:La/a/b/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, La/a/b/l;->a(Z)V

    :try_start_0
    iget-object v0, p0, La/a/b/i;->a:La/a/b/a/e;

    invoke-virtual {v0, p1, p2, p3, p4}, La/a/b/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    iget-object v0, p0, La/a/b/i;->b:La/a/b/l;

    invoke-virtual {v0}, La/a/b/l;->b()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, La/a/b/i;->b:La/a/b/l;

    invoke-virtual {v1, v2}, La/a/b/l;->a(Z)V

    return-object v0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    new-instance v1, Lorg/json/JSONException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, La/a/b/i;->b:La/a/b/l;

    invoke-virtual {v1, v2}, La/a/b/l;->a(Z)V

    throw v0
.end method

.method public final retrieveJsMessages()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, La/a/b/i;->b:La/a/b/l;

    invoke-virtual {v0}, La/a/b/l;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setNativeToJsBridgeMode(I)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, La/a/b/i;->b:La/a/b/l;

    invoke-virtual {v0, p1}, La/a/b/l;->a(I)V

    return-void
.end method
