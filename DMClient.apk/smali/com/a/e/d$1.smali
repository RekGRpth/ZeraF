.class final Lcom/a/e/d$1;
.super Lcom/a/e/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/a/e/d;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/a/e/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/a/e/d$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/database/sqlite/SQLiteDatabase;

.field private final synthetic c:Lcom/a/e/d$c;


# direct methods
.method constructor <init>(Lcom/a/e/d$c;)V
    .locals 1

    iput-object p1, p0, Lcom/a/e/d$1;->c:Lcom/a/e/d$c;

    invoke-direct {p0}, Lcom/a/e/d;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/e/d$1;->a:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/e/d$1;->b:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method


# virtual methods
.method final a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    iget-object v0, p0, Lcom/a/e/d$1;->b:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/a/e/d$1;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/a/e/d$1;->c:Lcom/a/e/d$c;

    invoke-virtual {v0}, Lcom/a/e/d$c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/a/e/d$1;->b:Landroid/database/sqlite/SQLiteDatabase;

    :cond_1
    iget-object v0, p0, Lcom/a/e/d$1;->b:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/a/e/d$a;
    .locals 3

    iget-object v0, p0, Lcom/a/e/d$1;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/e/d$1;->a:Ljava/util/Map;

    new-instance v1, Lcom/a/e/d$a;

    iget-object v2, p0, Lcom/a/e/d$1;->c:Lcom/a/e/d$c;

    invoke-direct {v1, p0, v2, p1}, Lcom/a/e/d$a;-><init>(Lcom/a/e/d;Lcom/a/e/d$c;Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/a/e/d$1;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/e/d$a;

    return-object v0
.end method
