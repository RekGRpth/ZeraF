.class final Lcom/a/c/a$2$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/a/c/a$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/a/c/a$2;


# direct methods
.method constructor <init>(Lcom/a/c/a$2;)V
    .locals 0

    iput-object p1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 12

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    iput v2, v1, Lcom/a/c/a;->h:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Response headers of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v2}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v2

    iget-object v2, v2, Lcom/a/c/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v2}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v2

    iget v2, v2, Lcom/a/c/a;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_6

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    iput-object v2, v1, Lcom/a/c/a;->m:Ljava/util/Map;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-lt v2, v4, :cond_7

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-boolean v1, v1, Lcom/a/c/a;->j:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/c/a;->a()V

    :cond_0
    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/c/a;->e()V

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(I)V

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    if-eqz v1, :cond_5

    :try_start_0
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    const-string v1, "Content-Encoding"

    invoke-interface {p1, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v3, "gzip"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v1, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v1, v2}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v3, v1

    :goto_2
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-direct {v4, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-boolean v1, v1, Lcom/a/c/a;->j:Z

    if-eqz v1, :cond_9

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-object v1, v1, Lcom/a/c/a;->f:Ljava/io/File;

    iget-object v5, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v5}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v5

    iget-object v5, v5, Lcom/a/c/a;->f:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    invoke-direct {v2, v1, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    :goto_3
    const/16 v1, 0x2800

    new-array v5, v1, [B

    const/4 v1, -0x1

    :goto_4
    invoke-virtual {v4, v5}, Ljava/io/InputStream;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_a

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-boolean v1, v1, Lcom/a/c/a;->j:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v4

    move-object v0, v2

    check-cast v0, Ljava/io/ByteArrayOutputStream;

    move-object v1, v0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iput-object v1, v4, Lcom/a/c/a;->k:[B

    :cond_1
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-boolean v1, v1, Lcom/a/c/a;->j:Z

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/c/a;->d()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    const-string v2, "Transfer-Encoding"

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "chunked"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-object v1, v1, Lcom/a/c/a;->e:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-object v1, v1, Lcom/a/c/a;->e:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_3
    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-object v1, v1, Lcom/a/c/a;->f:Ljava/io/File;

    iget-object v2, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v2}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v2

    iget-object v2, v2, Lcom/a/c/a;->e:Ljava/io/File;

    invoke-static {v1, v2}, Lcom/a/e/a;->a(Ljava/io/File;Ljava/io/File;)V

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-object v1, v1, Lcom/a/c/a;->f:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-object v1, v1, Lcom/a/c/a;->g:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    :goto_5
    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(I)V

    :cond_5
    :goto_6
    const/4 v1, 0x0

    return-object v1

    :cond_6
    aget-object v4, v2, v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-interface {v4}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_7
    aget-object v5, v3, v2

    invoke-interface {v5}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-object v1, v1, Lcom/a/c/a;->m:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-object v1, v1, Lcom/a/c/a;->m:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    :goto_7
    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_1

    :cond_8
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v7}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v7

    iget-object v7, v7, Lcom/a/c/a;->m:Ljava/util/Map;

    invoke-interface {v7, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_9
    :try_start_1
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    :catch_0
    move-exception v1

    :try_start_2
    iget-object v2, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v2}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(I)V

    goto/16 :goto_6

    :cond_a
    :try_start_3
    iget-object v7, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v7}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v7

    iget-wide v8, v7, Lcom/a/c/a;->c:J

    int-to-long v10, v6

    add-long/2addr v8, v10

    iput-wide v8, v7, Lcom/a/c/a;->c:J

    iget-object v7, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v7}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v7

    iget-wide v7, v7, Lcom/a/c/a;->d:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_b

    iget-object v7, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v7}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v7

    iget-wide v7, v7, Lcom/a/c/a;->c:J

    const-wide/16 v9, 0x64

    mul-long/2addr v7, v9

    iget-object v9, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v9}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v9

    iget-wide v9, v9, Lcom/a/c/a;->d:J

    div-long/2addr v7, v9

    int-to-long v9, v1

    cmp-long v7, v7, v9

    if-eqz v7, :cond_b

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-wide v7, v1, Lcom/a/c/a;->c:J

    const-wide/16 v9, 0x64

    mul-long/2addr v7, v9

    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    iget-wide v9, v1, Lcom/a/c/a;->d:J

    div-long/2addr v7, v9

    long-to-int v1, v7

    iget-object v7, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v7}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v7

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Lcom/a/c/a;->a(I)V

    :cond_b
    const/4 v7, 0x0

    invoke-virtual {v2, v5, v7, v6}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v2}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/a/c/a;->a(I)V

    throw v1

    :cond_c
    :try_start_4
    iget-object v1, p0, Lcom/a/c/a$2$1;->a:Lcom/a/c/a$2;

    invoke-static {v1}, Lcom/a/c/a$2;->a(Lcom/a/c/a$2;)Lcom/a/c/a;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_5

    :cond_d
    move-object v3, v2

    goto/16 :goto_2
.end method
