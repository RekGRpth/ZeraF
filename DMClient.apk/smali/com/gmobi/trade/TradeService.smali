.class public abstract Lcom/gmobi/trade/TradeService;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final BUILD:Ljava/lang/String; = "2013.11.26.1"

.field public static DEBUG:Z = false

.field public static final PARAM_API_URL_BASE:Ljava/lang/String; = "ApiUrlBase"

.field public static final PARAM_BILLING_URL_BASE:Ljava/lang/String; = "BillingUrlBase"

.field public static final PARAM_CHANNEL:Ljava/lang/String; = "Channel"

.field public static final PARAM_FORCE_REMOTE:Ljava/lang/String; = "ForceRemote"

.field public static final PARAM_IMEI:Ljava/lang/String; = "IMEI"

.field public static final PARAM_IMSI:Ljava/lang/String; = "IMSI"

.field public static final PARAM_LOADING_MESSAGE:Ljava/lang/String; = "LoadingMessage"

.field public static final PARAM_LOADING_TITLE:Ljava/lang/String; = "LoadingTitle"

.field public static final PARAM_MAIN_ACTIVITY:Ljava/lang/String; = "MainActivity"

.field public static final PARAM_PAGE_URL_BASE:Ljava/lang/String; = "PageUrlBase"

.field public static final PARAM_PUSH_CHANNEL:Ljava/lang/String; = "PushChannel"

.field public static final PARAM_SDK_URL_BASE:Ljava/lang/String; = "SdkUrlBase"

.field public static final VERSION:Ljava/lang/String; = "1.0"

.field private static a:Lcom/gmobi/trade/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/gmobi/trade/TradeService;->DEBUG:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/gmobi/trade/TradeService;->a:Lcom/gmobi/trade/a/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()V
    .locals 2

    invoke-static {}, Lcom/gmobi/trade/TradeService;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "TradeService has not been started!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public static execute(Ljava/lang/String;Lcom/gmobi/trade/ICallback;Landroid/os/Bundle;)V
    .locals 1

    invoke-static {}, Lcom/gmobi/trade/TradeService;->a()V

    sget-object v0, Lcom/gmobi/trade/TradeService;->a:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0, p0, p1, p2}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;Lcom/gmobi/trade/ICallback;Landroid/os/Bundle;)V

    return-void
.end method

.method public static getBalance(Z)I
    .locals 1

    invoke-static {}, Lcom/gmobi/trade/TradeService;->a()V

    sget-object v0, Lcom/gmobi/trade/TradeService;->a:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0, p0}, Lcom/gmobi/trade/a/e;->a(Z)I

    move-result v0

    return v0
.end method

.method public static getInstance()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/gmobi/trade/TradeService;->a:Lcom/gmobi/trade/a/e;

    return-object v0
.end method

.method public static isStarted()Z
    .locals 1

    sget-object v0, Lcom/gmobi/trade/TradeService;->a:Lcom/gmobi/trade/a/e;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/gmobi/trade/TradeService;->a:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/e;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setOnBalanceChangedListener(Lcom/gmobi/trade/ICallback;)V
    .locals 1

    invoke-static {}, Lcom/gmobi/trade/TradeService;->a()V

    sget-object v0, Lcom/gmobi/trade/TradeService;->a:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0, p0}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;)V

    return-void
.end method

.method public static setOnPushReceivedListener(Lcom/gmobi/trade/IPushCallback;)V
    .locals 1

    invoke-static {}, Lcom/gmobi/trade/TradeService;->a()V

    sget-object v0, Lcom/gmobi/trade/TradeService;->a:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0, p0}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/IPushCallback;)V

    return-void
.end method

.method public static start(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    invoke-static {}, Lcom/gmobi/trade/TradeService;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "TradeService is already running."

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/gmobi/trade/TradeService;->DEBUG:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/a/e/e;->a(Z)V

    :cond_1
    new-instance v0, Lcom/gmobi/trade/a/e;

    invoke-direct {v0}, Lcom/gmobi/trade/a/e;-><init>()V

    sput-object v0, Lcom/gmobi/trade/TradeService;->a:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/gmobi/trade/a/e;->a(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_0
.end method
