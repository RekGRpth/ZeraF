.class public interface abstract Lcom/gmobi/trade/Actions;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final CHARGE:Ljava/lang/String; = "charge/"

.field public static final DATA:Ljava/lang/String; = "data/"

.field public static final EARN:Ljava/lang/String; = "earn/"

.field public static final PARAM_DATA:Ljava/lang/String; = "data"

.field public static final PARAM_FORCE_GOOGLE_WALLET:Ljava/lang/String; = "force_google_wallet"

.field public static final PARAM_PASSWORD:Ljava/lang/String; = "password"

.field public static final PARAM_TRACK_ID:Ljava/lang/String; = "track_id"

.field public static final PARAM_USERNAME:Ljava/lang/String; = "username"

.field public static final RESULT_EXISTS:Ljava/lang/String; = "exists"

.field public static final RESULT_PAYMENT:Ljava/lang/String; = "payment"

.field public static final RESULT_TRACK_ID:Ljava/lang/String; = "track_id"

.field public static final RESULT_USER_ID:Ljava/lang/String; = "user_id"

.field public static final SSO_CHECK:Ljava/lang/String; = "sso/check"

.field public static final SSO_CONNECT:Ljava/lang/String; = "sso/connect"

.field public static final SSO_LOGIN:Ljava/lang/String; = "sso/login"

.field public static final SSO_REGISTER:Ljava/lang/String; = "sso/register"
