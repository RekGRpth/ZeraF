.class public Lcom/gmobi/trade/ActionActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const-string v0, "onConfigurationChanged"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 24

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/gmobi/trade/a/e;->e()Lcom/gmobi/trade/a/b;

    move-result-object v4

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v13

    if-nez v13, :cond_2

    :try_start_0
    invoke-virtual {v6}, Lcom/gmobi/trade/a/e;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/gmobi/trade/ActionActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->finish()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_1
    new-instance v3, Landroid/content/Intent;

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/gmobi/trade/ActionActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-static {v2}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    :try_start_2
    const-string v2, "NOTIF_ID"

    const/4 v3, 0x0

    invoke-virtual {v13, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    const-string v2, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/gmobi/trade/ActionActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/NotificationManager;

    const-string v2, "SHORTCUT"

    const/4 v3, 0x0

    invoke-virtual {v13, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    const-string v2, "ID"

    invoke-virtual {v13, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "TYPE"

    invoke-virtual {v13, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "JSON"

    invoke-virtual {v13, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x2

    invoke-virtual {v4, v5, v7}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    invoke-static {v3}, Landroid/a/a/a/a$a;->b(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v10, "ActionActivity ("

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ") : "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x2

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    if-eqz v2, :cond_3

    if-nez v3, :cond_4

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->finish()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v2

    invoke-static {v2}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    :try_start_3
    const-string v7, "sms"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    const-string v2, "title"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, ""

    move-object v13, v2

    :goto_2
    const-string v2, "description"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, ""

    move-object v12, v2

    :goto_3
    const-string v2, "yes"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "Yes"

    move-object v11, v2

    :goto_4
    const-string v2, "no"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "No"

    move-object v10, v2

    :goto_5
    const-string v2, "number"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v2, "message"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v6, ""

    :goto_6
    new-instance v14, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v14, v13}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v14, v12}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/gmobi/trade/ActionActivity$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/gmobi/trade/ActionActivity$1;-><init>(Lcom/gmobi/trade/ActionActivity;Lcom/gmobi/trade/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/NotificationManager;I)V

    invoke-virtual {v14, v11, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/gmobi/trade/ActionActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4, v5}, Lcom/gmobi/trade/ActionActivity$2;-><init>(Lcom/gmobi/trade/ActionActivity;Lcom/gmobi/trade/a/b;Ljava/lang/String;)V

    invoke-virtual {v14, v10, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v14}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_1

    :cond_5
    const-string v2, "title"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v13, v2

    goto :goto_2

    :cond_6
    const-string v2, "description"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v12, v2

    goto :goto_3

    :cond_7
    const-string v2, "yes"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v11, v2

    goto :goto_4

    :cond_8
    const-string v2, "no"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    goto :goto_5

    :cond_9
    const-string v2, "message"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    :cond_a
    const-string v7, "install"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_17

    const-string v2, "title"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v7, ""

    :goto_7
    const-string v2, "description"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v18, ""

    :goto_8
    const-string v2, "yes"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "Download"

    move-object/from16 v23, v2

    :goto_9
    const-string v2, "no"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "No"

    move-object/from16 v22, v2

    :goto_a
    const-string v2, "wo"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    const/4 v10, 0x0

    :goto_b
    const-string v2, "uri"

    invoke-static {v3, v2}, Lcom/a/e/f;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v2, "package"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const/4 v2, 0x0

    move-object v12, v2

    :goto_c
    const-string v2, "gp"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    const/4 v2, 0x0

    move v11, v2

    :goto_d
    const-string v2, "package"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v2, 0x0

    :goto_e
    const-string v3, "GOOGLE_PLAY_INSTALLED"

    const/4 v15, 0x0

    invoke-virtual {v13, v3, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v14, :cond_15

    if-eqz v12, :cond_13

    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/a/e/g;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_13

    const/16 v2, 0xa

    invoke-virtual {v4, v5, v2}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v12}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/gmobi/trade/ActionActivity;->startActivity(Landroid/content/Intent;)V

    :goto_f
    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->finish()V

    goto/16 :goto_1

    :cond_b
    const-string v2, "title"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_7

    :cond_c
    const-string v2, "description"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    goto :goto_8

    :cond_d
    const-string v2, "yes"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v23, v2

    goto :goto_9

    :cond_e
    const-string v2, "no"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v22, v2

    goto :goto_a

    :cond_f
    const-string v2, "wo"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    goto :goto_b

    :cond_10
    const-string v2, "package"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v12, v2

    goto :goto_c

    :cond_11
    const-string v2, "gp"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move v11, v2

    goto :goto_d

    :cond_12
    const-string v2, "package"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_e

    :cond_13
    if-eqz v11, :cond_14

    if-eqz v3, :cond_14

    if-eqz v2, :cond_14

    const/16 v3, 0xb

    invoke-virtual {v4, v5, v3}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    invoke-static {v2}, Lcom/a/e/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/a/e/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_f

    :cond_14
    const/4 v2, 0x6

    invoke-virtual {v4, v5, v2}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    const/4 v9, 0x1

    move-object/from16 v8, v17

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    goto :goto_f

    :cond_15
    if-eqz v11, :cond_16

    if-eqz v3, :cond_16

    if-eqz v2, :cond_16

    const/16 v3, 0xb

    invoke-virtual {v4, v5, v3}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    invoke-static {v2}, Lcom/a/e/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/a/e/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->finish()V

    goto/16 :goto_1

    :cond_16
    new-instance v2, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v11, Lcom/gmobi/trade/ActionActivity$3;

    move-object/from16 v12, p0

    move-object v13, v4

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, v7

    move/from16 v19, v10

    move-object/from16 v20, v8

    move/from16 v21, v9

    invoke-direct/range {v11 .. v21}, Lcom/gmobi/trade/ActionActivity$3;-><init>(Lcom/gmobi/trade/ActionActivity;Lcom/gmobi/trade/a/b;Ljava/lang/String;Lcom/gmobi/trade/a/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/app/NotificationManager;I)V

    move-object/from16 v0, v23

    invoke-virtual {v2, v0, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/gmobi/trade/ActionActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5}, Lcom/gmobi/trade/ActionActivity$4;-><init>(Lcom/gmobi/trade/ActionActivity;Lcom/gmobi/trade/a/b;Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_1

    :cond_17
    const-string v7, "link"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_19

    const-string v2, "uri"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/e/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_18

    invoke-virtual {v6}, Lcom/gmobi/trade/a/e;->e()Lcom/gmobi/trade/a/b;

    move-result-object v3

    const/16 v4, 0xb

    invoke-virtual {v3, v5, v4}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    :cond_18
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/a/e/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->finish()V

    goto/16 :goto_1

    :cond_19
    const-string v7, "popup"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "DOC_ID"

    invoke-virtual {v13, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    if-nez v17, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lcom/gmobi/trade/ActionActivity;->finish()V

    goto/16 :goto_1

    :cond_1a
    invoke-virtual {v6}, Lcom/gmobi/trade/a/e;->b()Lcom/a/e/d$a;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/a/e/d$a;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_1d

    :goto_10
    const-string v3, "uri"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v3, "notifId"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1b

    const/16 v19, 0x0

    :goto_11
    const-string v3, "imagePath"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v7, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v21, Landroid/widget/ImageView;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const-class v2, Lcom/gmobi/trade/ActionActivity;

    const-string v10, "/com/gmobi/trade/internal/close.png"

    invoke-virtual {v2, v10}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_1c

    const/4 v2, 0x0

    :goto_12
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x2

    const/4 v11, -0x2

    invoke-direct {v2, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v10, 0x6

    const/16 v11, 0x64

    invoke-virtual {v2, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v10, 0x7

    const/16 v11, 0x64

    invoke-virtual {v2, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    new-instance v10, Lcom/gmobi/trade/ActionActivity$5;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v4, v5}, Lcom/gmobi/trade/ActionActivity$5;-><init>(Lcom/gmobi/trade/ActionActivity;Lcom/gmobi/trade/a/b;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v22, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x2

    const/4 v11, -0x2

    move-object/from16 v0, v22

    invoke-direct {v0, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v10, 0xe

    const/4 v11, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/16 v10, 0xf

    const/4 v11, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    new-instance v23, Landroid/widget/ImageView;

    invoke-direct/range {v23 .. v24}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/16 v10, 0x64

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setId(I)V

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, -0x1

    const/4 v11, -0x1

    invoke-static {v10, v3, v11}, Landroid/a/a/a/a$a;->a(Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-instance v10, Lcom/gmobi/trade/ActionActivity$6;

    move-object/from16 v11, p0

    move-object v12, v4

    move-object v13, v5

    move-object v15, v6

    move-object/from16 v16, p0

    move-object/from16 v18, v8

    move/from16 v20, v9

    invoke-direct/range {v10 .. v20}, Lcom/gmobi/trade/ActionActivity$6;-><init>(Lcom/gmobi/trade/ActionActivity;Lcom/gmobi/trade/a/b;Ljava/lang/String;Ljava/lang/String;Lcom/gmobi/trade/a/e;Landroid/content/Context;Ljava/lang/String;Landroid/app/NotificationManager;II)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, v21

    invoke-virtual {v7, v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/gmobi/trade/ActionActivity;->setContentView(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_1b
    const-string v3, "notifId"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_11

    :cond_1c
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v2

    goto/16 :goto_12

    :cond_1d
    move-object v2, v3

    goto/16 :goto_10
.end method
