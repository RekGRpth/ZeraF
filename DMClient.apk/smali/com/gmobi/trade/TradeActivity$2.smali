.class final Lcom/gmobi/trade/TradeActivity$2;
.super Lcom/gmobi/trade/TradeActivity$CustomCordovaWebViewClient;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/TradeActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic c:Lcom/gmobi/trade/TradeActivity;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/TradeActivity;La/a/b/h;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/TradeActivity$2;->c:Lcom/gmobi/trade/TradeActivity;

    invoke-direct {p0, p1, p2}, Lcom/gmobi/trade/TradeActivity$CustomCordovaWebViewClient;-><init>(Lcom/gmobi/trade/TradeActivity;La/a/b/h;)V

    return-void
.end method


# virtual methods
.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 12

    const/4 v11, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    const-string v0, "result://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v5

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const/16 v0, 0x3f

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    array-length v8, v7

    move v2, v3

    :goto_0
    if-lt v2, v8, :cond_1

    :cond_0
    const-string v0, "error"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v5, v3, v6, v11}, Lcom/gmobi/trade/a/e;->a(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :goto_1
    iget-object v0, p0, Lcom/gmobi/trade/TradeActivity$2;->c:Lcom/gmobi/trade/TradeActivity;

    iput-boolean v1, v0, Lcom/gmobi/trade/TradeActivity;->a:Z

    iget-object v0, p0, Lcom/gmobi/trade/TradeActivity$2;->c:Lcom/gmobi/trade/TradeActivity;

    invoke-virtual {v0}, Lcom/gmobi/trade/TradeActivity;->endActivity()V

    move v0, v1

    :goto_2
    return v0

    :cond_1
    aget-object v0, v7, v2

    const-string v9, "="

    invoke-virtual {v0, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    aget-object v10, v9, v3

    array-length v0, v9

    if-le v0, v1, :cond_3

    aget-object v0, v9, v1

    :goto_3
    invoke-virtual {v6, v10, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    aget-object v10, v9, v3

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " = "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v9, v9, v3

    invoke-virtual {v6, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->b(Ljava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_3

    :cond_4
    const-string v0, "success"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v5, v1, v6, v11}, Lcom/gmobi/trade/a/e;->a(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unknown result : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    invoke-super {p0, p1, p2}, Lcom/gmobi/trade/TradeActivity$CustomCordovaWebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_2
.end method
