.class final Lcom/gmobi/trade/a/e$8;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/gmobi/trade/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gmobi/trade/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/e;

.field private final synthetic b:Lcom/gmobi/trade/ICallback;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/e;Lcom/gmobi/trade/ICallback;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/e$8;->a:Lcom/gmobi/trade/a/e;

    iput-object p2, p0, Lcom/gmobi/trade/a/e$8;->b:Lcom/gmobi/trade/ICallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    if-nez p1, :cond_0

    iget-object v2, p0, Lcom/gmobi/trade/a/e$8;->a:Lcom/gmobi/trade/a/e;

    iget-object v3, p0, Lcom/gmobi/trade/a/e$8;->b:Lcom/gmobi/trade/ICallback;

    invoke-virtual {v2, v3, v1, v6, v6}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :cond_0
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SsoCheck Result : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "exists"

    const-string v4, "result"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e$8;->a:Lcom/gmobi/trade/a/e;

    iget-object v3, p0, Lcom/gmobi/trade/a/e$8;->b:Lcom/gmobi/trade/ICallback;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e$8;->a:Lcom/gmobi/trade/a/e;

    iget-object v2, p0, Lcom/gmobi/trade/a/e$8;->b:Lcom/gmobi/trade/ICallback;

    invoke-virtual {v0, v2, v1, v6, v6}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
