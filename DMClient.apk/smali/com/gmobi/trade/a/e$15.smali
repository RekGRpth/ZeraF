.class final Lcom/gmobi/trade/a/e$15;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/gmobi/trade/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/e;->p()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/e;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/e;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/e$15;->a:Lcom/gmobi/trade/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)V
    .locals 3

    iget-object v0, p0, Lcom/gmobi/trade/a/e$15;->a:Lcom/gmobi/trade/a/e;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/gmobi/trade/a/e;->t:Z

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/gmobi/trade/a/e$15;->a:Lcom/gmobi/trade/a/e;

    const-string v0, "sid"

    invoke-static {p1, v0}, Landroid/a/a/a/a$a;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_0
    iput-object v0, v1, Lcom/gmobi/trade/a/e;->s:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Session:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e$15;->a:Lcom/gmobi/trade/a/e;

    iget-object v1, v1, Lcom/gmobi/trade/a/e;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e$15;->a:Lcom/gmobi/trade/a/e;

    iget-object v0, v0, Lcom/gmobi/trade/a/e;->j:Lcom/gmobi/trade/a/c;

    iget-object v1, p0, Lcom/gmobi/trade/a/e$15;->a:Lcom/gmobi/trade/a/e;

    iget-object v1, v1, Lcom/gmobi/trade/a/e;->s:Ljava/lang/String;

    iput-object v1, v0, Lcom/gmobi/trade/a/c;->a:Ljava/lang/String;

    const-string v0, "memberSubscriberInfo/gollar"

    invoke-static {p1, v0}, Landroid/a/a/a/a$a;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/gmobi/trade/a/e$15;->a:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/gmobi/trade/a/e;->a(I)V

    :cond_0
    return-void

    :cond_1
    if-eqz v0, :cond_2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
