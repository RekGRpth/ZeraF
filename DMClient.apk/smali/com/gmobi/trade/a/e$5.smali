.class final Lcom/gmobi/trade/a/e$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/gmobi/trade/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gmobi/trade/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/e;

.field private final synthetic b:Lcom/gmobi/trade/ICallback;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/e;Lcom/gmobi/trade/ICallback;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/e$5;->a:Lcom/gmobi/trade/a/e;

    iput-object p2, p0, Lcom/gmobi/trade/a/e$5;->b:Lcom/gmobi/trade/ICallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/e$5;->a:Lcom/gmobi/trade/a/e;

    iget-object v1, p0, Lcom/gmobi/trade/a/e$5;->b:Lcom/gmobi/trade/ICallback;

    invoke-virtual {v0, v1, v6, v5, v5}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SsoConnect Result : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "user_id"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/gmobi/trade/a/e$5;->a:Lcom/gmobi/trade/a/e;

    invoke-virtual {v2, v0}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e$5;->a:Lcom/gmobi/trade/a/e;

    iget-object v2, p0, Lcom/gmobi/trade/a/e$5;->b:Lcom/gmobi/trade/ICallback;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/gmobi/trade/a/e$5;->a:Lcom/gmobi/trade/a/e;

    iget-object v1, p0, Lcom/gmobi/trade/a/e$5;->b:Lcom/gmobi/trade/ICallback;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/e$5;->a:Lcom/gmobi/trade/a/e;

    iget-object v1, p0, Lcom/gmobi/trade/a/e$5;->b:Lcom/gmobi/trade/ICallback;

    invoke-virtual {v0, v1, v6, v5, v5}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/ICallback;ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
