.class public final Lcom/gmobi/trade/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gmobi/trade/a/c$a;,
        Lcom/gmobi/trade/a/c$b;
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field private j:Lcom/a/d/d;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/a/d/d;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->a:Ljava/lang/String;

    const-string v0, "location"

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->c:Ljava/lang/String;

    const-string v0, "member/connect"

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->d:Ljava/lang/String;

    const-string v0, "trade/earn"

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->k:Ljava/lang/String;

    const-string v0, "android/member"

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->e:Ljava/lang/String;

    const-string v0, "user/nologin"

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->f:Ljava/lang/String;

    const-string v0, "user/register"

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->g:Ljava/lang/String;

    const-string v0, "user/login"

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->h:Ljava/lang/String;

    const-string v0, "user/verifyusername"

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->i:Ljava/lang/String;

    iput-object p1, p0, Lcom/gmobi/trade/a/c;->j:Lcom/a/d/d;

    iput-object p2, p0, Lcom/gmobi/trade/a/c;->b:Ljava/lang/String;

    const-string v0, "PathConnect"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PathConnect"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->d:Ljava/lang/String;

    :cond_0
    const-string v0, "PathEarn"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PathEarn"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->k:Ljava/lang/String;

    :cond_1
    const-string v0, "PathGetBalance"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PathGetBalance"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/c;->e:Ljava/lang/String;

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/gmobi/trade/a/c$b;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GET "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/gmobi/trade/a/c$b;

    invoke-direct {v0}, Lcom/gmobi/trade/a/c$b;-><init>()V

    iget-object v1, p0, Lcom/gmobi/trade/a/c;->j:Lcom/a/d/d;

    invoke-interface {v1, p1}, Lcom/a/d/d;->a(Ljava/lang/String;)Lcom/a/c/a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(Z)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/a/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/gmobi/trade/a/c$b;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/a/c/a;->g()I

    move-result v1

    iput v1, v0, Lcom/gmobi/trade/a/c$b;->a:I

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/gmobi/trade/a/c$b;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "POST "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/gmobi/trade/a/c$b;

    invoke-direct {v0}, Lcom/gmobi/trade/a/c$b;-><init>()V

    iget-object v1, p0, Lcom/gmobi/trade/a/c;->j:Lcom/a/d/d;

    invoke-interface {v1, p1, p2}, Lcom/a/d/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/c/a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/a/c/a;->a(Z)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/a/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/gmobi/trade/a/c$b;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/a/c/a;->g()I

    move-result v1

    iput v1, v0, Lcom/gmobi/trade/a/c$b;->a:I

    return-object v0
.end method

.method final a()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/gmobi/trade/a/c;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/c;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";jsessionid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/gmobi/trade/a/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/Runnable;)V
    .locals 1

    new-instance v0, Lcom/gmobi/trade/a/c$7;

    invoke-direct {v0, p0, p1}, Lcom/gmobi/trade/a/c$7;-><init>(Lcom/gmobi/trade/a/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/gmobi/trade/a/c$7;->start()V

    return-void
.end method

.method public final a(Ljava/lang/String;ILcom/gmobi/trade/a/c$a;)V
    .locals 1

    new-instance v0, Lcom/gmobi/trade/a/c$9;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/gmobi/trade/a/c$9;-><init>(Lcom/gmobi/trade/a/c;Ljava/lang/String;ILcom/gmobi/trade/a/c$a;)V

    invoke-virtual {p0, v0}, Lcom/gmobi/trade/a/c;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method final a(Lorg/json/JSONObject;Lcom/gmobi/trade/a/c$a;)V
    .locals 1

    new-instance v0, Lcom/gmobi/trade/a/c$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/gmobi/trade/a/c$1;-><init>(Lcom/gmobi/trade/a/c;Lorg/json/JSONObject;Lcom/gmobi/trade/a/c$a;)V

    invoke-virtual {v0}, Lcom/gmobi/trade/a/c$1;->start()V

    return-void
.end method
