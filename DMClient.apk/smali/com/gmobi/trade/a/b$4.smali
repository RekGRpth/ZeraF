.class final Lcom/gmobi/trade/a/b$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/a/b/d",
        "<",
        "Lcom/a/c/a$b;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/b;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/io/File;

.field private final synthetic e:Z

.field private final synthetic f:Ljava/lang/String;

.field private final synthetic g:Landroid/graphics/Bitmap;

.field private final synthetic h:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;ZLjava/lang/String;Landroid/graphics/Bitmap;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/b$4;->a:Lcom/gmobi/trade/a/b;

    iput-object p2, p0, Lcom/gmobi/trade/a/b$4;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/gmobi/trade/a/b$4;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/gmobi/trade/a/b$4;->d:Ljava/io/File;

    iput-boolean p5, p0, Lcom/gmobi/trade/a/b$4;->e:Z

    iput-object p6, p0, Lcom/gmobi/trade/a/b$4;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/gmobi/trade/a/b$4;->g:Landroid/graphics/Bitmap;

    iput-object p8, p0, Lcom/gmobi/trade/a/b$4;->h:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/a/b/b;)V
    .locals 5

    check-cast p2, Lcom/a/c/a$b;

    invoke-virtual {p2}, Lcom/a/c/a$b;->a()Lcom/a/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/c/a;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/gmobi/trade/a/b$4;->a:Lcom/gmobi/trade/a/b;

    iget-object v1, p0, Lcom/gmobi/trade/a/b$4;->b:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Url "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/b$4;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has been downloaded!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$4;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/gmobi/trade/a/b$4;->d:Ljava/io/File;

    invoke-static {v0, v1}, Lcom/a/e/b;->a(Landroid/content/Context;Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/b$4;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/gmobi/trade/a/b$4;->d:Ljava/io/File;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, "application/vnd.android.package-archive"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    iget-boolean v0, p0, Lcom/gmobi/trade/a/b$4;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/gmobi/trade/a/b$4;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/gmobi/trade/a/b$4;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/gmobi/trade/a/b$4;->g:Landroid/graphics/Bitmap;

    const-class v3, Lcom/gmobi/trade/ActionActivity;

    iget-object v4, p0, Lcom/gmobi/trade/a/b$4;->h:Landroid/os/Bundle;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method
