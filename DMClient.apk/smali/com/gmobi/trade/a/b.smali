.class public final Lcom/gmobi/trade/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/gmobi/trade/a/b$a;
    }
.end annotation


# static fields
.field static a:J

.field static b:J

.field static c:J

.field private static v:J


# instance fields
.field private A:Landroid/content/BroadcastReceiver;

.field d:Lcom/gmobi/trade/a/b$a;

.field e:Landroid/content/Context;

.field f:Landroid/net/ConnectivityManager;

.field g:Z

.field h:Z

.field i:Z

.field j:Z

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:Lorg/json/JSONArray;

.field n:Lcom/a/d/d;

.field o:Ljava/util/Timer;

.field p:Z

.field q:Z

.field r:Lcom/a/e/d$a;

.field s:Lcom/a/e/d$a;

.field t:J

.field u:Lcom/gmobi/trade/a/e;

.field private w:Lcom/a/e/d$a;

.field private x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/gmobi/trade/IPushCallback;",
            ">;>;"
        }
    .end annotation
.end field

.field private y:Lcom/a/a/b;

.field private z:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/32 v0, 0xea60

    sput-wide v0, Lcom/gmobi/trade/a/b;->a:J

    sput-wide v0, Lcom/gmobi/trade/a/b;->v:J

    const-wide/16 v0, 0x2710

    sput-wide v0, Lcom/gmobi/trade/a/b;->b:J

    const-wide/32 v0, 0x2bf20

    sput-wide v0, Lcom/gmobi/trade/a/b;->c:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->d:Lcom/gmobi/trade/a/b$a;

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->f:Landroid/net/ConnectivityManager;

    iput-boolean v1, p0, Lcom/gmobi/trade/a/b;->g:Z

    iput-boolean v2, p0, Lcom/gmobi/trade/a/b;->h:Z

    iput-boolean v2, p0, Lcom/gmobi/trade/a/b;->i:Z

    iput-boolean v1, p0, Lcom/gmobi/trade/a/b;->j:Z

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->m:Lorg/json/JSONArray;

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->o:Ljava/util/Timer;

    iput-boolean v1, p0, Lcom/gmobi/trade/a/b;->p:Z

    iput-boolean v1, p0, Lcom/gmobi/trade/a/b;->q:Z

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/gmobi/trade/a/b;->t:J

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->x:Ljava/util/Map;

    new-instance v0, Lcom/gmobi/trade/a/b$1;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/b$1;-><init>(Lcom/gmobi/trade/a/b;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->z:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/gmobi/trade/a/b$2;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/b$2;-><init>(Lcom/gmobi/trade/a/b;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->A:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/e;->f()Lcom/a/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/e;->c()Lcom/a/e/d$a;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->s:Lcom/a/e/d$a;

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/e;->k()Lcom/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->y:Lcom/a/a/b;

    iput-object p1, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/e;->b()Lcom/a/e/d$a;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->r:Lcom/a/e/d$a;

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/e;->d()Lcom/a/e/d$a;

    move-result-object v0

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->w:Lcom/a/e/d$a;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/gmobi/trade/a/b;->A:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->z:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->f:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->f:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    iput-boolean v0, p0, Lcom/gmobi/trade/a/b;->p:Z

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->f:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    iput-boolean v0, p0, Lcom/gmobi/trade/a/b;->q:Z

    iget-boolean v0, p0, Lcom/gmobi/trade/a/b;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/gmobi/trade/a/b;->q:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/gmobi/trade/a/b;->g:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Connectivity init: connected="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/gmobi/trade/a/b;->g:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/gmobi/trade/ActionMonitor;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1, v1, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Alarm every "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v2, Lcom/gmobi/trade/a/b;->v:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sget-wide v4, Lcom/gmobi/trade/a/b;->v:J

    add-long/2addr v2, v4

    sget-wide v4, Lcom/gmobi/trade/a/b;->v:J

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/gmobi/trade/a/b;->t:J

    new-instance v0, Lcom/gmobi/trade/a/b$3;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/b$3;-><init>(Lcom/gmobi/trade/a/b;)V

    invoke-virtual {v0}, Lcom/gmobi/trade/a/b$3;->start()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/gmobi/trade/a/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/gmobi/trade/a/b;->h()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "fullid"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "uri"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "status"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/gmobi/trade/a/b;->w:Lcom/a/e/d$a;

    invoke-virtual {v1, p1, v0}, Lcom/a/e/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 22

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    const-string v3, "icon"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/a/e/f;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-string v5, "icon"

    invoke-static {v4, v5, v3}, Lcom/a/e/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    invoke-interface {v5, v3, v4}, Lcom/a/d/d;->a(Ljava/lang/String;Ljava/io/File;)Lcom/a/c/a;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/a/c/a;->a(Z)V

    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0x40

    const/16 v5, 0x40

    invoke-static {v4, v3, v5}, Landroid/a/a/a/a$a;->a(Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v7

    :cond_1
    :try_start_0
    const-string v3, "shortcut"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v15, 0x0

    :goto_0
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    const-string v3, "ID"

    move-object/from16 v0, p1

    invoke-virtual {v9, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "TYPE"

    move-object/from16 v0, p2

    invoke-virtual {v9, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "JSON"

    invoke-virtual/range {p3 .. p3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "title"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v4, ""

    :goto_1
    const-string v3, "description"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v5, ""

    :goto_2
    const-string v3, "sms"

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    if-eqz p3, :cond_2

    const-string v3, "number"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-nez v3, :cond_2

    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const v6, 0x1080057

    const-class v8, Lcom/gmobi/trade/ActionActivity;

    invoke-static/range {v3 .. v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)I

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-class v5, Lcom/gmobi/trade/ActionActivity;

    invoke-static {v3, v4, v7, v5, v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_3
    return-void

    :cond_3
    :try_start_2
    const-string v3, "shortcut"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    goto :goto_0

    :cond_4
    const-string v3, "title"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_5
    const-string v3, "description"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :catch_0
    move-exception v3

    invoke-static {v3}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    move-exception v3

    invoke-static {v3}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    :try_start_3
    const-string v3, "popup"

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    if-eqz p3, :cond_2

    const-string v3, "uri"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "uri"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/a/e/f;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/a/e/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v6, v1, v3}, Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    const-string v3, "image1"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/a/e/f;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v3, "notif"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    const/4 v3, 0x0

    move v8, v3

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v3}, Lcom/a/e/g;->l(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "This is a Tablet"

    invoke-static {v3}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v3, "image2"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/a/e/f;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    move-object v6, v3

    :cond_8
    const/4 v3, 0x0

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-string v10, "image"

    invoke-static {v3, v10, v6}, Lcom/a/e/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    invoke-interface {v11, v6, v10}, Lcom/a/d/d;->a(Ljava/lang/String;Ljava/io/File;)Lcom/a/c/a;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/a/c/a;->a(Z)V

    :cond_9
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_a

    const/16 v7, 0x40

    const/16 v11, 0x40

    invoke-static {v10, v7, v11}, Landroid/a/a/a/a$a;->a(Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v7

    :cond_a
    const-string v10, "imageUri"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v6, "imagePath"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "JSON"

    invoke-virtual/range {p3 .. p3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DOC_ID"

    move-object/from16 v0, p1

    invoke-virtual {v9, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v8, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const v6, 0x1080057

    const-class v8, Lcom/gmobi/trade/ActionActivity;

    invoke-static/range {v3 .. v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)I

    move-result v3

    const-string v5, "notifId"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->r:Lcom/a/e/d$a;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v3, v0, v1}, Lcom/a/e/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-class v5, Lcom/gmobi/trade/ActionActivity;

    invoke-static {v3, v4, v7, v5, v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_c
    const-string v3, "notif"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    move v8, v3

    goto/16 :goto_4

    :cond_d
    const-string v3, "link"

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    if-eqz p3, :cond_2

    const-string v3, "uri"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "uri"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/a/e/f;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/a/e/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v6, v1, v3}, Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const v6, 0x1080057

    const-class v8, Lcom/gmobi/trade/ActionActivity;

    invoke-static/range {v3 .. v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)I

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-class v5, Lcom/gmobi/trade/ActionActivity;

    invoke-static {v3, v4, v7, v5, v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_f
    const-string v3, "install"

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    if-eqz p3, :cond_2

    const-string v3, "uri"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "uri"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/a/e/f;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v3, "wo"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v20, 0x0

    :goto_5
    const-string v3, "gp"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    const/4 v3, 0x0

    move v8, v3

    :goto_6
    const-string v3, "package"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    const/4 v3, 0x0

    move-object v6, v3

    :goto_7
    const-string v3, "mode"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    const-string v3, "download"

    :goto_8
    if-eqz v6, :cond_10

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v6, v1, v13}, Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    if-eqz v8, :cond_16

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-boolean v8, v8, Lcom/gmobi/trade/a/e;->r:Z

    if-eqz v8, :cond_16

    if-eqz v6, :cond_16

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "Google Play First : "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " ("

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ")"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    const-string v8, "GOOGLE_PLAY_INSTALLED"

    const/4 v10, 0x1

    invoke-virtual {v9, v8, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v8, "download"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const v6, 0x108004e

    const-class v8, Lcom/gmobi/trade/ActionActivity;

    invoke-static/range {v3 .. v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)I

    :goto_9
    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-class v5, Lcom/gmobi/trade/ActionActivity;

    invoke-static {v3, v4, v7, v5, v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_11
    const-string v3, "wo"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    goto/16 :goto_5

    :cond_12
    const-string v3, "gp"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    move v8, v3

    goto/16 :goto_6

    :cond_13
    const-string v3, "package"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v6, v3

    goto/16 :goto_7

    :cond_14
    const-string v3, "mode"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_8

    :cond_15
    const/16 v3, 0xb

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v6}, Lcom/a/e/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/a/e/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_9

    :cond_16
    const-string v6, "auto"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-string v5, "push"

    invoke-static {v3, v5, v13}, Lcom/a/e/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    invoke-interface {v3, v13, v14}, Lcom/a/d/d;->a(Ljava/lang/String;Ljava/io/File;)Lcom/a/c/a;

    move-result-object v3

    const/4 v5, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    invoke-virtual {v3}, Lcom/a/c/a;->c()Lcom/a/b/a;

    move-result-object v5

    new-instance v10, Lcom/gmobi/trade/a/b$4;

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v16, v4

    move-object/from16 v17, v7

    move-object/from16 v18, v9

    invoke-direct/range {v10 .. v18}, Lcom/gmobi/trade/a/b$4;-><init>(Lcom/gmobi/trade/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;ZLjava/lang/String;Landroid/graphics/Bitmap;Landroid/os/Bundle;)V

    invoke-virtual {v5, v10}, Lcom/a/b/a;->a(Lcom/a/b/d;)V

    invoke-virtual {v3}, Lcom/a/c/a;->f()V

    goto/16 :goto_3

    :cond_17
    const-string v6, "install"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v16

    const/16 v19, 0x1

    move-object/from16 v17, v4

    move-object/from16 v18, v13

    move-object/from16 v21, p1

    invoke-virtual/range {v16 .. v21}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-class v5, Lcom/gmobi/trade/ActionActivity;

    invoke-static {v3, v4, v7, v5, v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_18
    const-string v6, "download"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const v6, 0x108004e

    const-class v8, Lcom/gmobi/trade/ActionActivity;

    invoke-static/range {v3 .. v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)I

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-class v5, Lcom/gmobi/trade/ActionActivity;

    invoke-static {v3, v4, v7, v5, v9}, Lcom/a/e/f;->a(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/gmobi/trade/a/b;->x:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1c

    const-string v4, "*"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1c

    const/4 v4, 0x0

    :goto_a
    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/gmobi/trade/a/b;->x:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/List;

    move-object v4, v0

    const/4 v3, 0x0

    move v5, v3

    :goto_b
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    if-ge v5, v3, :cond_1a

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/gmobi/trade/IPushCallback;

    if-eqz v3, :cond_1b

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-interface {v3, v0, v1, v2}, Lcom/gmobi/trade/IPushCallback;->onReceived(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_1b
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_b

    :cond_1c
    const/4 v4, 0x1

    goto :goto_a
.end method

.method private static a(Lorg/json/JSONArray;Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lt v0, v2, :cond_0

    :goto_1
    return v1

    :cond_0
    :try_start_0
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private a(DD)[Ljava/lang/String;
    .locals 10

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://maps.googleapis.com/maps/api/geocode/json?latlng="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&sensor=true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/a/d/d;->a(Ljava/lang/String;)Lcom/a/c/a;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/a/c/a;->a(Z)V

    invoke-virtual {v5}, Lcom/a/c/a;->g()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_4

    const-string v3, ""

    const-string v2, ""

    const-string v1, ""

    :try_start_0
    invoke-virtual {v5}, Lcom/a/c/a;->b()Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "status"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "OK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "results"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "address_components"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    move v5, v0

    move-object v0, v1

    move-object v1, v2

    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lt v5, v2, :cond_0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v2, v5

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    move-object v0, v2

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    const-string v2, "long_name"

    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v8, "short_name"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "types"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-gtz v9, :cond_1

    const-string v9, ""

    if-eq v2, v9, :cond_5

    :cond_1
    const-string v9, "locality"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v3, v2

    goto :goto_0

    :cond_2
    const-string v9, "administrative_area_level_1"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    :cond_3
    const-string v2, "country"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v3

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    :cond_4
    move-object v0, v4

    goto :goto_1

    :cond_5
    move-object v2, v3

    goto :goto_2
.end method

.method static synthetic b(Lcom/gmobi/trade/a/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/gmobi/trade/a/b;->i()V

    return-void
.end method

.method private static e(Ljava/lang/String;)Z
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/push/locks/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/a/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/a;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isProcessedGlobally "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Ljava/lang/String;)Z
    .locals 3

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-string v1, "received-push"

    invoke-static {v0, v1, p1}, Lcom/a/e/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isProcessed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private g()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->y:Lcom/a/a/b;

    const-string v1, "KEY_BRAND"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static g(Ljava/lang/String;)V
    .locals 2

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/push/locks/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/a/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/a;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/a;->b(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private declared-synchronized h()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/gmobi/trade/a/b;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->d:Lcom/gmobi/trade/a/b$a;

    if-nez v0, :cond_1

    new-instance v0, Lcom/gmobi/trade/a/b$a;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/b$a;-><init>(Lcom/gmobi/trade/a/b;)V

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->d:Lcom/gmobi/trade/a/b$a;

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->d:Lcom/gmobi/trade/a/b$a;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/b$a;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/gmobi/trade/a/b;->d:Lcom/gmobi/trade/a/b$a;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/b$a;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    const-string v1, "received-push"

    invoke-static {v0, v1, p1}, Lcom/a/e/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private declared-synchronized i()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/gmobi/trade/a/b;->d:Lcom/gmobi/trade/a/b$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->d:Lcom/gmobi/trade/a/b$a;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/b$a;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gmobi/trade/a/b;->d:Lcom/gmobi/trade/a/b$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->y:Lcom/a/a/b;

    const-string v1, "KEY_LAST"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->y:Lcom/a/a/b;

    const-string v1, "KEY_LAST"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 4

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->w:Lcom/a/e/d$a;

    invoke-virtual {v0, p1}, Lcom/a/e/d$a;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v1, "fullid"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "status"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/16 v2, 0x8

    invoke-virtual {p0, v1, v2}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    const-string v1, "status"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/gmobi/trade/a/b;->w:Lcom/a/e/d$a;

    invoke-virtual {v1, p1, v0}, Lcom/a/e/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    if-ne v2, v0, :cond_0

    const/16 v0, 0x9

    :try_start_1
    invoke-virtual {p0, v1, v0}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->w:Lcom/a/e/d$a;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/a/e/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Lcom/gmobi/trade/IPushCallback;)V
    .locals 2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Parameter topic cannot be null!"

    invoke-static {v0, v1}, Lcom/a/e/e;->a(ZLjava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "addHandler "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->x:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->x:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    :goto_1
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/gmobi/trade/a/b;->x:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONArray;)V
    .locals 4

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "type"

    const-string v2, "apps"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "ac"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "apps"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "at"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/gmobi/trade/a/b;->s:Lcom/a/e/d$a;

    const-string v2, "apps"

    invoke-virtual {v1, v2, v0}, Lcom/a/e/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Record "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method final declared-synchronized a(Lorg/json/JSONArray;)V
    .locals 10

    const/4 v2, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    if-nez p1, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    move v3, v1

    :goto_0
    :try_start_1
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-ge v3, v0, :cond_0

    :try_start_2
    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v0, "filters"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "filters"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v0, "imeis"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    move v0, v2

    :goto_1
    const-string v7, "apps"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v7}, Lcom/a/e/g;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "apps"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lez v9, :cond_3

    invoke-static {v8, v7}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v9, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, v4, Lcom/gmobi/trade/a/e;->l:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    const-string v0, "App or channel does not match!"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    :cond_2
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    const-string v7, "brands"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-direct {p0}, Lcom/gmobi/trade/a/b;->g()Ljava/lang/String;

    move-result-object v7

    const-string v8, "brands"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lez v9, :cond_4

    if-eqz v7, :cond_4

    invoke-static {v8, v7}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v9, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v9, 0x0

    invoke-static {v9}, Lcom/a/e/g;->a(Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v0, "Brand or model does not match!"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_4
    const-string v7, "regions"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v7}, Lcom/a/e/g;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "regions"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lez v9, :cond_5

    invoke-static {v8, v7}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v0, "Country does not match!"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    const-string v7, "operators"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    const-string v7, "operators"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-lez v7, :cond_8

    iget-object v7, v4, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    if-eqz v7, :cond_6

    iget-object v7, v4, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    if-eqz v7, :cond_8

    iget-object v7, v4, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x3

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, v4, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x5

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, v4, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x6

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/gmobi/trade/a/b;->a(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_8

    :cond_6
    const-string v0, "Operators does not match!"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    move v0, v1

    :cond_8
    const-string v6, "content"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    const-string v6, "_id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    const-string v6, "_at"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Bad Push Message :"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->d(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_a
    const-string v6, "test"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_b

    const-string v6, "test"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    iget-boolean v6, p0, Lcom/gmobi/trade/a/b;->j:Z

    if-nez v6, :cond_b

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/gmobi/trade/a/b;->j:Z

    const-string v6, "Test Now!"

    invoke-static {v6}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    :cond_b
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "_id"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_at"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "content"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    const-string v7, "_id"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_c

    const-string v7, "_at"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_c

    const-string v7, "type"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_c

    const-string v7, "data"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_d

    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Bad Push Message :"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->d(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_d
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Push Message : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "_id"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_at"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/gmobi/trade/a/b;->f(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_e

    invoke-static {v7}, Lcom/gmobi/trade/a/b;->e(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    :cond_e
    invoke-direct {p0, v6}, Lcom/gmobi/trade/a/b;->f(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_f

    invoke-static {v6}, Lcom/gmobi/trade/a/b;->e(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_10

    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "_"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " has been processed"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_10
    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " has been processed"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_11
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "Processing "

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-direct {p0, v7}, Lcom/gmobi/trade/a/b;->h(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/gmobi/trade/a/b;->g(Ljava/lang/String;)V

    invoke-direct {p0, v6}, Lcom/gmobi/trade/a/b;->h(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/gmobi/trade/a/b;->g(Ljava/lang/String;)V

    const-string v0, "type"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_12

    const-string v0, ""

    :cond_12
    const-string v8, "data"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    if-eqz v5, :cond_2

    const-string v8, "fileUrl"

    iget-object v9, p0, Lcom/gmobi/trade/a/b;->l:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_"

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, v0, v5}, Lcom/gmobi/trade/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    :cond_13
    move v0, v1

    goto/16 :goto_1
.end method

.method final b()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->y:Lcom/a/a/b;

    const-string v1, "KEY_DEVICE_ID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->y:Lcom/a/a/b;

    const-string v1, "KEY_DEVICE_ID"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 4

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "type"

    const-string v2, "activities"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "fullid"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "action"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "at"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/gmobi/trade/a/b;->s:Lcom/a/e/d$a;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/a/e/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Record "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method final c()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->y:Lcom/a/a/b;

    const-string v1, "KEY_SESSION_ID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->y:Lcom/a/a/b;

    const-string v1, "KEY_SESSION_ID"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-direct {p0}, Lcom/gmobi/trade/a/b;->i()V

    invoke-direct {p0}, Lcom/gmobi/trade/a/b;->h()V

    return-void
.end method

.method final d(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->y:Lcom/a/a/b;

    const-string v1, "KEY_BRAND"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final e()Lorg/json/JSONObject;
    .locals 8

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "sdk_v"

    const-string v2, "1.0"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sdk_b"

    const-string v2, "2013.11.26.1"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "app"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ch"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-object v2, v2, Lcom/gmobi/trade/a/e;->l:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "app_v"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "imsi"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-object v2, v2, Lcom/gmobi/trade/a/e;->m:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "imei"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-object v2, v2, Lcom/gmobi/trade/a/e;->n:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "wifi"

    iget-boolean v2, p0, Lcom/gmobi/trade/a/b;->p:Z

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "gprs"

    iget-boolean v2, p0, Lcom/gmobi/trade/a/b;->q:Z

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "brand"

    invoke-direct {p0}, Lcom/gmobi/trade/a/b;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sd"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {}, Lcom/a/e/g;->c()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "id"

    invoke-virtual {p0}, Lcom/gmobi/trade/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "cid"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->c(Landroid/content/Context;)La/a/a/a/b/a;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ua:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/a/e/g;->a(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|imei:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, La/a/a/a/b/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|imsi:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, La/a/a/a/b/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|wmac:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v3}, Lcom/a/e/g;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|bmac:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {}, Lcom/a/e/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|sn:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v3}, Lcom/a/e/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/e/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ua"

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/a/e/g;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "os"

    const-string v2, "android"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "os_v"

    invoke-static {}, Lcom/a/e/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "lang"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "country"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "gp"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-boolean v2, v2, Lcom/gmobi/trade/a/e;->r:Z

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "wmac"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "bmac"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {}, Lcom/a/e/g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sn"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sa"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->g(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "sw"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->j(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "sh"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->k(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/a/e/g;->f(Landroid/content/Context;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "lng"

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v3, "lat"

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v3, "loc"

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/gmobi/trade/a/b;->a(DD)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "country"

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "country"

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "region"

    const/4 v4, 0x1

    aget-object v4, v0, v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "city"

    const/4 v4, 0x2

    aget-object v0, v0, v4

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    :goto_0
    const-string v0, "roaming"

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/a/e/g;->o(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Push Device Info: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    :goto_1
    return-object v1

    :cond_1
    iget-object v0, p0, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-object v3, v3, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "api/push/ipinfo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/a/d/d;->a(Ljava/lang/String;)Lcom/a/c/a;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/a/c/a;->a(Z)V

    invoke-virtual {v0}, Lcom/a/c/a;->b()Lorg/json/JSONObject;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/a/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    if-eqz v2, :cond_0

    const-string v0, "ll"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "ll"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v6

    const-string v3, "range"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    const-string v3, "ll"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    const-string v3, "lng"

    invoke-virtual {v0, v3, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v3, "lat"

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v3, "loc"

    invoke-virtual {v1, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "iploc"

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/gmobi/trade/a/b;->e:Landroid/content/Context;

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/gmobi/trade/a/b;->a(DD)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "country"

    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "country"

    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "region"

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "city"

    const/4 v4, 0x2

    aget-object v2, v2, v4

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method public final f()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/gmobi/trade/a/b;->w:Lcom/a/e/d$a;

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v0, v0}, Lcom/a/e/d$a;->a(Lcom/gmobi/a/d$b;ZI)Ljava/util/List;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    return-object v2

    :cond_0
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/e/d$b;

    invoke-virtual {v0}, Lcom/a/e/d$b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
