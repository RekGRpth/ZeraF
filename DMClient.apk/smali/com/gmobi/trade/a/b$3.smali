.class final Lcom/gmobi/trade/a/b$3;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/b;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/b;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    const/4 v2, 0x0

    :cond_0
    :goto_0
    :try_start_0
    sget-wide v0, Lcom/gmobi/trade/a/b;->b:J

    invoke-static {v0, v1}, Lcom/gmobi/trade/a/b$3;->sleep(J)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    iget-boolean v0, v0, Lcom/gmobi/trade/a/b;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    iget-boolean v0, v0, Lcom/gmobi/trade/a/b;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->d:Lcom/gmobi/trade/a/b$a;

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v3, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    iget-wide v3, v3, Lcom/gmobi/trade/a/b;->t:J

    sub-long/2addr v0, v3

    sget-wide v3, Lcom/gmobi/trade/a/b;->c:J

    cmp-long v0, v0, v3

    if-lez v0, :cond_1

    const-string v0, "The push service is blocked!"

    invoke-static {v0}, Lcom/a/e/e;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    invoke-static {v0}, Lcom/gmobi/trade/a/b;->b(Lcom/gmobi/trade/a/b;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    invoke-static {v0}, Lcom/gmobi/trade/a/b;->a(Lcom/gmobi/trade/a/b;)V

    :cond_1
    iget-object v0, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->s:Lcom/a/e/d$a;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Lcom/a/e/d$a;->a(Lcom/gmobi/a/d$b;ZI)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "$"

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-string v1, "at"

    invoke-virtual {v0, v1, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "sid"

    iget-object v3, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    invoke-virtual {v3}, Lcom/gmobi/trade/a/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "did"

    iget-object v3, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    invoke-virtual {v3}, Lcom/gmobi/trade/a/b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move v3, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lt v3, v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Send collected data : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    iget-object v0, v0, Lcom/gmobi/trade/a/b;->n:Lcom/a/d/d;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    iget-object v3, v3, Lcom/gmobi/trade/a/b;->u:Lcom/gmobi/trade/a/e;

    iget-object v3, v3, Lcom/gmobi/trade/a/e;->f:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "api/data/d"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/a/d/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/c/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/a/c/a;->a(Z)V

    invoke-virtual {v0}, Lcom/a/c/a;->g()I

    move-result v1

    const/16 v3, 0xc8

    if-ne v1, v3, :cond_0

    invoke-virtual {v0}, Lcom/a/c/a;->b()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "ok"

    const-string v3, "status"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Clear collected data"

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/e/d$b;

    iget-object v3, p0, Lcom/gmobi/trade/a/b$3;->a:Lcom/gmobi/trade/a/b;

    iget-object v3, v3, Lcom/gmobi/trade/a/b;->s:Lcom/a/e/d$a;

    invoke-virtual {v0}, Lcom/a/e/d$b;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5}, Lcom/a/e/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/e/d$b;

    invoke-virtual {v0}, Lcom/a/e/d$b;->b()Lorg/json/JSONObject;

    move-result-object v8

    const-string v0, "type"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "activities"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-object v1, v0

    :goto_3
    const-string v0, "fullid"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v1, v9, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_4
    const-string v1, "action"

    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v9, "at"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v10, v1}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    sub-long v8, v6, v8

    long-to-int v1, v8

    invoke-virtual {v10, v1}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    invoke-virtual {v0, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    :goto_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    :cond_3
    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    :cond_4
    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    goto :goto_4

    :cond_5
    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_6
    invoke-virtual {v0, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_6
    :try_start_1
    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_6
.end method
