.class final Lcom/gmobi/trade/a/a$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/gmobi/trade/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/a;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/a$1;->a:Lcom/gmobi/trade/a/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/gmobi/trade/a/a$1;->a:Lcom/gmobi/trade/a/a;

    iget-object v1, v0, Lcom/gmobi/trade/a/a;->c:Ljava/util/Timer;

    if-eqz v1, :cond_0

    const-string v1, "AppUsageMonitor stops"

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/gmobi/trade/a/a;->c:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/gmobi/trade/a/a;->c:Ljava/util/Timer;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/a$1;->a:Lcom/gmobi/trade/a/a;

    invoke-virtual {v0}, Lcom/gmobi/trade/a/a;->a()V

    goto :goto_0
.end method
