.class final Lcom/gmobi/trade/a/e$11;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/e;->a(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/e;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/e;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/e$11;->a:Lcom/gmobi/trade/a/e;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "extra_download_id"

    const-wide/16 v1, -0x1

    invoke-virtual {p2, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iget-object v0, p0, Lcom/gmobi/trade/a/e$11;->a:Lcom/gmobi/trade/a/e;

    iget-object v0, v0, Lcom/gmobi/trade/a/e;->u:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/gmobi/trade/a/e$11;->a:Lcom/gmobi/trade/a/e;

    iget-object v0, v0, Lcom/gmobi/trade/a/e;->u:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/gmobi/trade/a/e$11;->a:Lcom/gmobi/trade/a/e;

    iget-object v3, v3, Lcom/gmobi/trade/a/e;->k:Lcom/gmobi/trade/a/b;

    const/4 v4, 0x7

    invoke-virtual {v3, v0, v4}, Lcom/gmobi/trade/a/b;->b(Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lcom/gmobi/trade/a/e$11;->a:Lcom/gmobi/trade/a/e;

    invoke-virtual {v0, v1, v2}, Lcom/gmobi/trade/a/e;->a(J)V

    goto :goto_0
.end method
