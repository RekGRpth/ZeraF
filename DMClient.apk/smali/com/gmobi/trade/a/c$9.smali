.class final Lcom/gmobi/trade/a/c$9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/c;->a(Ljava/lang/String;ILcom/gmobi/trade/a/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/c;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:I

.field private final synthetic d:Lcom/gmobi/trade/a/c$a;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/c;Ljava/lang/String;ILcom/gmobi/trade/a/c$a;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/c$9;->a:Lcom/gmobi/trade/a/c;

    iput-object p2, p0, Lcom/gmobi/trade/a/c$9;->b:Ljava/lang/String;

    iput p3, p0, Lcom/gmobi/trade/a/c$9;->c:I

    iput-object p4, p0, Lcom/gmobi/trade/a/c$9;->d:Lcom/gmobi/trade/a/c$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "googleId"

    iget-object v2, p0, Lcom/gmobi/trade/a/c$9;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "action"

    iget v2, p0, Lcom/gmobi/trade/a/c$9;->c:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Earn: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Earn URL : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/gmobi/trade/a/c$9;->a:Lcom/gmobi/trade/a/c;

    invoke-virtual {v2}, Lcom/gmobi/trade/a/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/trade/a/c$9;->a:Lcom/gmobi/trade/a/c;

    iget-object v2, p0, Lcom/gmobi/trade/a/c$9;->a:Lcom/gmobi/trade/a/c;

    invoke-virtual {v2}, Lcom/gmobi/trade/a/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/gmobi/trade/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/gmobi/trade/a/c$b;

    move-result-object v0

    iget v1, v0, Lcom/gmobi/trade/a/c$b;->a:I

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Earn Resp : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/gmobi/trade/a/c$b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/gmobi/trade/a/c$b;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/a/a/a/a$a;->b(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lcom/gmobi/trade/a/c$9;->d:Lcom/gmobi/trade/a/c$a;

    invoke-interface {v1, v0}, Lcom/gmobi/trade/a/c$a;->a(Lorg/json/JSONObject;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
