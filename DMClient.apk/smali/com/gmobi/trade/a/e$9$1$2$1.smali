.class final Lcom/gmobi/trade/a/e$9$1$2$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/gmobi/a/d$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/trade/a/e$9$1$2;->a(Lcom/gmobi/a/g;Lcom/gmobi/a/e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/trade/a/e$9$1$2;

.field private final synthetic b:Z

.field private final synthetic c:Lcom/gmobi/trade/ICallback;


# direct methods
.method constructor <init>(Lcom/gmobi/trade/a/e$9$1$2;ZLcom/gmobi/trade/ICallback;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/trade/a/e$9$1$2$1;->a:Lcom/gmobi/trade/a/e$9$1$2;

    iput-boolean p2, p0, Lcom/gmobi/trade/a/e$9$1$2$1;->b:Z

    iput-object p3, p0, Lcom/gmobi/trade/a/e$9$1$2$1;->c:Lcom/gmobi/trade/ICallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/gmobi/a/e;Lcom/gmobi/a/g;)V
    .locals 4

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "2. Purchase finished: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", purchase: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/gmobi/a/e;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/gmobi/trade/a/e$9$1$2$1;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/gmobi/a/e;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/gmobi/a/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/trade/a/e$9$1$2$1;->c:Lcom/gmobi/trade/ICallback;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v2, v2}, Lcom/gmobi/trade/ICallback;->onResult(ZLandroid/os/Bundle;Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "result"

    invoke-virtual {p1}, Lcom/gmobi/a/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/trade/a/e$9$1$2$1;->a:Lcom/gmobi/trade/a/e$9$1$2;

    iget-object v1, v1, Lcom/gmobi/trade/a/e$9$1$2;->a:Lcom/gmobi/trade/a/e$9$1;

    iget-object v1, v1, Lcom/gmobi/trade/a/e$9$1;->a:Lcom/gmobi/trade/a/e$9;

    invoke-static {v1}, Lcom/gmobi/trade/a/e$9;->a(Lcom/gmobi/trade/a/e$9;)Lcom/gmobi/trade/a/e;

    move-result-object v1

    invoke-static {v1}, Lcom/gmobi/trade/a/e;->a(Lcom/gmobi/trade/a/e;)Lcom/gmobi/a/d;

    move-result-object v1

    new-instance v2, Lcom/gmobi/trade/a/e$9$1$2$1$1;

    iget-object v3, p0, Lcom/gmobi/trade/a/e$9$1$2$1;->c:Lcom/gmobi/trade/ICallback;

    invoke-direct {v2, p0, v3, v0}, Lcom/gmobi/trade/a/e$9$1$2$1$1;-><init>(Lcom/gmobi/trade/a/e$9$1$2$1;Lcom/gmobi/trade/ICallback;Landroid/os/Bundle;)V

    invoke-virtual {v1, p2, v2}, Lcom/gmobi/a/d;->a(Lcom/gmobi/a/g;Lcom/gmobi/a/d$a;)V

    goto :goto_0
.end method
