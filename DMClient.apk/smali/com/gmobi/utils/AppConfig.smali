.class public Lcom/gmobi/utils/AppConfig;
.super Ljava/lang/Object;
.source "AppConfig.java"


# instance fields
.field ctx:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/gmobi/utils/AppConfig;->ctx:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public batteryCheck()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public menuIsOpen()Z
    .locals 2

    iget-object v0, p0, Lcom/gmobi/utils/AppConfig;->ctx:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/utils/AppConfig;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public titleBarIsCustomize()Z
    .locals 2

    iget-object v0, p0, Lcom/gmobi/utils/AppConfig;->ctx:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/utils/AppConfig;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
