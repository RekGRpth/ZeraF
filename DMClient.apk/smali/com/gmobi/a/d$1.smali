.class final Lcom/gmobi/a/d$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/gmobi/a/d;->a(Lcom/gmobi/a/d$d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/gmobi/a/d;

.field private final synthetic b:Lcom/gmobi/a/d$d;


# direct methods
.method constructor <init>(Lcom/gmobi/a/d;Lcom/gmobi/a/d$d;)V
    .locals 0

    iput-object p1, p0, Lcom/gmobi/a/d$1;->a:Lcom/gmobi/a/d;

    iput-object p2, p0, Lcom/gmobi/a/d$1;->b:Lcom/gmobi/a/d$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    const-string v0, "Billing service connected."

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/a/d$1;->a:Lcom/gmobi/a/d;

    invoke-static {p2}, Lcom/android/vending/billing/IInAppBillingService$a;->a(Landroid/os/IBinder;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v1

    iput-object v1, v0, Lcom/gmobi/a/d;->c:Lcom/android/vending/billing/IInAppBillingService;

    iget-object v0, p0, Lcom/gmobi/a/d$1;->a:Lcom/gmobi/a/d;

    iget-object v0, v0, Lcom/gmobi/a/d;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v1, "Checking for in-app billing 3 support."

    invoke-static {v1}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/gmobi/a/d$1;->a:Lcom/gmobi/a/d;

    iget-object v1, v1, Lcom/gmobi/a/d;->c:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v2, 0x3

    const-string v3, "inapp"

    invoke-interface {v1, v2, v0, v3}, Lcom/android/vending/billing/IInAppBillingService;->isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/gmobi/a/d$1;->b:Lcom/gmobi/a/d$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/a/d$1;->b:Lcom/gmobi/a/d$d;

    new-instance v2, Lcom/gmobi/a/e;

    const-string v3, "Error checking for billing v3 support."

    invoke-direct {v2, v1, v3}, Lcom/gmobi/a/e;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/gmobi/a/d$d;->a(Lcom/gmobi/a/e;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "In-app billing version 3 supported for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/a/d$1;->a:Lcom/gmobi/a/d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/gmobi/a/d;->a:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/gmobi/a/d$1;->b:Lcom/gmobi/a/d$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gmobi/a/d$1;->b:Lcom/gmobi/a/d$d;

    new-instance v1, Lcom/gmobi/a/e;

    const/4 v2, 0x0

    const-string v3, "Setup successful."

    invoke-direct {v1, v2, v3}, Lcom/gmobi/a/e;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/gmobi/a/d$d;->a(Lcom/gmobi/a/e;)V

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/gmobi/a/d$1;->b:Lcom/gmobi/a/d$d;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/gmobi/a/d$1;->b:Lcom/gmobi/a/d$d;

    new-instance v2, Lcom/gmobi/a/e;

    const/16 v3, -0x3e9

    const-string v4, "RemoteException while setting up in-app billing."

    invoke-direct {v2, v3, v4}, Lcom/gmobi/a/e;-><init>(ILjava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/gmobi/a/d$d;->a(Lcom/gmobi/a/e;)V

    :cond_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    const-string v0, "Billing service disconnected."

    invoke-static {v0}, Lcom/a/e/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/gmobi/a/d$1;->a:Lcom/gmobi/a/d;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/gmobi/a/d;->c:Lcom/android/vending/billing/IInAppBillingService;

    return-void
.end method
