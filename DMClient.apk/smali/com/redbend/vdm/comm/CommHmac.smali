.class public Lcom/redbend/vdm/comm/CommHmac;
.super Ljava/lang/Object;
.source "CommHmac.java"


# instance fields
.field private _algorithm:Ljava/lang/String;

.field private _mac:Ljava/lang/String;

.field private _username:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "MD5"

    invoke-direct {p0, v1, v1, v0}, Lcom/redbend/vdm/comm/CommHmac;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v0, "MD5"

    invoke-direct {p0, p1, p2, v0}, Lcom/redbend/vdm/comm/CommHmac;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/redbend/vdm/comm/CommHmac;->_algorithm:Ljava/lang/String;

    iput-object p2, p0, Lcom/redbend/vdm/comm/CommHmac;->_username:Ljava/lang/String;

    iput-object p3, p0, Lcom/redbend/vdm/comm/CommHmac;->_mac:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public fromHttpHeaderField(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v7, ","

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v0, v3

    const-string v7, "="

    const/4 v8, 0x2

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v8, v6, v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const-string v7, "algorithm"

    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iput-object v7, p0, Lcom/redbend/vdm/comm/CommHmac;->_algorithm:Ljava/lang/String;

    const-string v7, "username"

    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iput-object v7, p0, Lcom/redbend/vdm/comm/CommHmac;->_username:Ljava/lang/String;

    const-string v7, "mac"

    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iput-object v7, p0, Lcom/redbend/vdm/comm/CommHmac;->_mac:Ljava/lang/String;

    return-void
.end method

.method public get_algorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/vdm/comm/CommHmac;->_algorithm:Ljava/lang/String;

    return-object v0
.end method

.method public get_mac()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/vdm/comm/CommHmac;->_mac:Ljava/lang/String;

    return-object v0
.end method

.method public get_username()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/redbend/vdm/comm/CommHmac;->_username:Ljava/lang/String;

    return-object v0
.end method

.method public set_algorithm(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/redbend/vdm/comm/CommHmac;->_algorithm:Ljava/lang/String;

    return-void
.end method

.method public set_mac(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/redbend/vdm/comm/CommHmac;->_mac:Ljava/lang/String;

    return-void
.end method

.method public set_username(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/redbend/vdm/comm/CommHmac;->_username:Ljava/lang/String;

    return-void
.end method

.method public toHttpHeaderField()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "algorithm="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/redbend/vdm/comm/CommHmac;->_algorithm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "username=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/redbend/vdm/comm/CommHmac;->_username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mac="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/redbend/vdm/comm/CommHmac;->_mac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
