.class public Lcom/redbend/dmcFramework/DmcInfoMessage;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "DmcInfoMessage.java"


# instance fields
.field private infoMessageText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "DmcInfoMessage"

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcInfoMessage;->LOG_TAG:Ljava/lang/String;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcInfoMessage;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcInfoMessage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uiAlertTextExtra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcInfoMessage;->infoMessageText:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcInfoMessage;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const v0, 0x7f030023

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcInfoMessage;->setContentView(I)V

    return-void
.end method

.method public setContentView(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcInfoMessage;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setContentView()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->setContentView(I)V

    const v1, 0x7f06003c

    invoke-virtual {p0, v1}, Lcom/redbend/dmcFramework/DmcInfoMessage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcInfoMessage;->infoMessageText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
