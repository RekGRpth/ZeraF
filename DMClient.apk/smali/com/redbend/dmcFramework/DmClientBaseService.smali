.class public abstract Lcom/redbend/dmcFramework/DmClientBaseService;
.super Ljava/lang/Object;
.source "DmClientBaseService.java"


# instance fields
.field protected dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

.field protected dmcService:Lcom/redbend/dmcFramework/DmcService;


# direct methods
.method public constructor <init>(Lcom/redbend/dmcFramework/DmcService;)V
    .locals 1
    .param p1    # Lcom/redbend/dmcFramework/DmcService;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmClientBaseService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmClientBaseService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    iput-object p1, p0, Lcom/redbend/dmcFramework/DmClientBaseService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmClientBaseService;->dmcService:Lcom/redbend/dmcFramework/DmcService;

    invoke-virtual {v0}, Lcom/redbend/dmcFramework/DmcService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/redbend/dmcFramework/DmcApplication;

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmClientBaseService;->dmcApplication:Lcom/redbend/dmcFramework/DmcApplication;

    return-void
.end method


# virtual methods
.method public abstract dispatch(Landroid/os/Message;)V
.end method
