.class public Lcom/redbend/dmcFramework/DmcActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "DmcActivity.java"


# instance fields
.field protected LOG_TAG:Ljava/lang/String;

.field protected a:Lcom/redbend/dmcFramework/DmcApplication;

.field private actionMenuEnable:Z

.field private btnFuncEnable:Z

.field private buttons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/redbend/dmcFramework/DMCButton;",
            ">;"
        }
    .end annotation
.end field

.field protected finishOnStop:Z

.field private flagOnPause:Z

.field private flagOnRotation:Z

.field private layoutId:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    const-string v0, "DmcActivity"

    iput-object v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->finishOnStop:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->btnFuncEnable:Z

    iput-boolean v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->actionMenuEnable:Z

    return-void
.end method

.method static synthetic access$000(Lcom/redbend/dmcFramework/DmcActivity;)V
    .locals 0
    .param p0    # Lcom/redbend/dmcFramework/DmcActivity;

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;->disableAllButtons()V

    return-void
.end method

.method private disableAllButtons()V
    .locals 3

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/redbend/dmcFramework/DMCButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/redbend/dmcFramework/DMCButton;->setClickable(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private enableAllButtons()V
    .locals 3

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/redbend/dmcFramework/DMCButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/redbend/dmcFramework/DMCButton;->setClickable(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public native isVdmCancelRequested()Z
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onActivityResult()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/ActionBarActivity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_1

    :cond_0
    iput-boolean v2, p0, Lcom/redbend/dmcFramework/DmcActivity;->flagOnRotation:Z

    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->layoutId:I

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcActivity;->setContentView(I)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3, v3}, Lcom/redbend/dmcFramework/DmcActivity;->overridePendingTransition(II)V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/redbend/dmcFramework/DmcApplication;

    iput-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->a:Lcom/redbend/dmcFramework/DmcApplication;

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->a:Lcom/redbend/dmcFramework/DmcApplication;

    iput-object p0, v1, Lcom/redbend/dmcFramework/DmcApplication;->currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->buttons:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/redbend/dmcFramework/DmcActivity;->flagOnPause:Z

    iput-boolean v3, p0, Lcom/redbend/dmcFramework/DmcActivity;->flagOnRotation:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->btnFuncEnable:Z

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->a:Lcom/redbend/dmcFramework/DmcApplication;

    invoke-virtual {v1}, Lcom/redbend/dmcFramework/DmcApplication;->getConfig()Lcom/gmobi/utils/AppConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/gmobi/utils/AppConfig;->titleBarIsCustomize()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f020064

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setIcon(I)V

    :cond_0
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(I)V

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iget-boolean v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->actionMenuEnable:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0d0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v1, "back button pressed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    :sswitch_0
    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->showAboutDialog()V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/redbend/dmcFramework/DmcActivity;->a:Lcom/redbend/dmcFramework/DmcApplication;

    iget-object v2, v2, Lcom/redbend/dmcFramework/DmcApplication;->currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

    const-class v3, Lcom/redbend/dmClient/DmcConfig;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f060054 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 6

    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onPause()V

    iget-object v3, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v4, "onPause()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/redbend/dmcFramework/DmcActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.redbend"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v4, "DMC Activity paused by another DMC Actitivity. Do nothing."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/redbend/dmcFramework/DmcActivity;->flagOnPause:Z

    monitor-exit p0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v4, "DMC Activity paused by non-DMC activity, so it should cancel the DM session and exit."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "nextStateExtra"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v3, p0, Lcom/redbend/dmcFramework/DmcActivity;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v4, 0xc

    new-instance v5, Ljava/lang/Integer;

    invoke-direct {v5, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v4, v5}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public onResume()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onResume()V

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onResume()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v4, p0, Lcom/redbend/dmcFramework/DmcActivity;->flagOnRotation:Z

    iget-boolean v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->flagOnPause:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v2, 0x10

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->finish()V

    :cond_0
    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;->enableAllButtons()V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->isVdmCancelRequested()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "VDM is in process of cancelling the session."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v4}, Lcom/redbend/dmcFramework/DmcActivity;->setVisible(Z)V

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->finish()V

    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStop()V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->flagOnPause:Z

    iget-boolean v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->finishOnStop:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->finish()V

    :cond_0
    return-void
.end method

.method public onSupportContentChanged()V
    .locals 4

    iget-boolean v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->flagOnRotation:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "nextStateExtra"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onContentChanged() nextState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->a:Lcom/redbend/dmcFramework/DmcApplication;

    const/16 v2, 0xd

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setActionMenuEnable()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->actionMenuEnable:Z

    return-void
.end method

.method public setButtonFuncDesable()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/redbend/dmcFramework/DmcActivity;->btnFuncEnable:Z

    return-void
.end method

.method public setContentView(I)V
    .locals 9
    .param p1    # I

    iput p1, p0, Lcom/redbend/dmcFramework/DmcActivity;->layoutId:I

    iget v7, p0, Lcom/redbend/dmcFramework/DmcActivity;->layoutId:I

    invoke-super {p0, v7}, Landroid/support/v7/app/ActionBarActivity;->setContentView(I)V

    iget-boolean v7, p0, Lcom/redbend/dmcFramework/DmcActivity;->btnFuncEnable:Z

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-class v0, Lcom/redbend/dmClient/R$id;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v5

    const/4 v6, 0x0

    :goto_1
    array-length v7, v5

    if-ge v6, v7, :cond_0

    aget-object v7, v5, v6

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    invoke-super {p0, v2}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    instance-of v7, v3, Landroid/widget/Button;

    if-eqz v7, :cond_2

    new-instance v1, Lcom/redbend/dmcFramework/DMCButton;

    check-cast v3, Landroid/widget/Button;

    invoke-direct {v1, v3}, Lcom/redbend/dmcFramework/DMCButton;-><init>(Landroid/widget/Button;)V

    iget-object v7, p0, Lcom/redbend/dmcFramework/DmcActivity;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    aget-object v7, v5, v6

    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/redbend/dmcFramework/DMCButton;->setTag(Ljava/lang/Object;)V

    new-instance v7, Lcom/redbend/dmcFramework/DmcActivity$1;

    invoke-direct {v7, p0}, Lcom/redbend/dmcFramework/DmcActivity$1;-><init>(Lcom/redbend/dmcFramework/DmcActivity;)V

    invoke-virtual {v1, v7}, Lcom/redbend/dmcFramework/DMCButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :catch_0
    move-exception v4

    iget-object v7, p0, Lcom/redbend/dmcFramework/DmcActivity;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Error occured due to Android bug"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public showAboutDialog()V
    .locals 11

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v8, "About"

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f030020

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v8, 0x7f06004e

    :try_start_0
    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v8, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const v8, 0x7f06004f

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b000d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b000e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "*"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v8, 0x7f0b0015

    new-instance v9, Lcom/redbend/dmcFramework/DmcActivity$2;

    invoke-direct {v9, p0}, Lcom/redbend/dmcFramework/DmcActivity$2;-><init>(Lcom/redbend/dmcFramework/DmcActivity;)V

    invoke-virtual {v1, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :catch_0
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
