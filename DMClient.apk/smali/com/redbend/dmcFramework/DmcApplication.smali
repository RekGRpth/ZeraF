.class public abstract Lcom/redbend/dmcFramework/DmcApplication;
.super Landroid/app/Application;
.source "DmcApplication.java"


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "DmcApplication"


# instance fields
.field private appConf:Lcom/gmobi/utils/AppConfig;

.field private bootFlag:Z

.field public currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

.field private lockReceiver:Landroid/content/BroadcastReceiver;

.field private logMonitor:Lcom/gmobi/utils/AppLogMonitor;

.field public serviceHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/redbend/dmcFramework/DmcApplication;->bootFlag:Z

    return-void
.end method


# virtual methods
.method public abstract getClientServiceClassName()Ljava/lang/String;
.end method

.method public getConfig()Lcom/gmobi/utils/AppConfig;
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcApplication;->appConf:Lcom/gmobi/utils/AppConfig;

    return-object v0
.end method

.method public isBootCompleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/redbend/dmcFramework/DmcApplication;->bootFlag:Z

    return v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    const-string v1, "DmcApplication"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/redbend/dmcFramework/ScreenLockReceiver;

    invoke-direct {v1}, Lcom/redbend/dmcFramework/ScreenLockReceiver;-><init>()V

    iput-object v1, p0, Lcom/redbend/dmcFramework/DmcApplication;->lockReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcApplication;->lockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/redbend/dmcFramework/DmcApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Lcom/gmobi/utils/AppLogMonitor;

    invoke-direct {v1}, Lcom/gmobi/utils/AppLogMonitor;-><init>()V

    iput-object v1, p0, Lcom/redbend/dmcFramework/DmcApplication;->logMonitor:Lcom/gmobi/utils/AppLogMonitor;

    new-instance v1, Lcom/gmobi/utils/AppConfig;

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/gmobi/utils/AppConfig;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/redbend/dmcFramework/DmcApplication;->appConf:Lcom/gmobi/utils/AppConfig;

    return-void
.end method

.method public onTerminate()V
    .locals 2

    const-string v0, "DmcApplication"

    const-string v1, "onTerminate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/redbend/dmcFramework/DmcApplication;->unregisterLockReceiver()V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcApplication;->logMonitor:Lcom/gmobi/utils/AppLogMonitor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcApplication;->logMonitor:Lcom/gmobi/utils/AppLogMonitor;

    invoke-virtual {v0}, Lcom/gmobi/utils/AppLogMonitor;->stop()V

    const-wide/16 v0, 0xa

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public sendMessage(ILjava/lang/Object;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcApplication;->serviceHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcApplication;->serviceHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput p1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/redbend/dmcFramework/DmcApplication;->serviceHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public setBootFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/redbend/dmcFramework/DmcApplication;->bootFlag:Z

    return-void
.end method

.method public unregisterLockReceiver()V
    .locals 1

    iget-object v0, p0, Lcom/redbend/dmcFramework/DmcApplication;->lockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/redbend/dmcFramework/DmcApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
