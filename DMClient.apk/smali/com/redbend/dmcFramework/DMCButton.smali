.class public Lcom/redbend/dmcFramework/DMCButton;
.super Landroid/widget/Button;
.source "DMCButton.java"


# instance fields
.field private button:Landroid/widget/Button;

.field private canFireClick:Z

.field private listener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/widget/Button;)V
    .locals 1
    .param p1    # Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/widget/Button;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/redbend/dmcFramework/DMCButton;->canFireClick:Z

    iput-object p1, p0, Lcom/redbend/dmcFramework/DMCButton;->button:Landroid/widget/Button;

    return-void
.end method

.method static synthetic access$000(Lcom/redbend/dmcFramework/DMCButton;)Z
    .locals 1
    .param p0    # Lcom/redbend/dmcFramework/DMCButton;

    iget-boolean v0, p0, Lcom/redbend/dmcFramework/DMCButton;->canFireClick:Z

    return v0
.end method

.method static synthetic access$002(Lcom/redbend/dmcFramework/DMCButton;Z)Z
    .locals 0
    .param p0    # Lcom/redbend/dmcFramework/DMCButton;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/redbend/dmcFramework/DMCButton;->canFireClick:Z

    return p1
.end method

.method static synthetic access$100(Lcom/redbend/dmcFramework/DMCButton;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0    # Lcom/redbend/dmcFramework/DMCButton;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DMCButton;->listener:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public setClickable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/redbend/dmcFramework/DMCButton;->button:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/redbend/dmcFramework/DMCButton;->button:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/redbend/dmcFramework/DMCButton;->canFireClick:Z

    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/redbend/dmcFramework/DMCButton;->listener:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DMCButton;->button:Landroid/widget/Button;

    new-instance v1, Lcom/redbend/dmcFramework/DMCButton$1;

    invoke-direct {v1, p0}, Lcom/redbend/dmcFramework/DMCButton$1;-><init>(Lcom/redbend/dmcFramework/DMCButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DMCButton;->button:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    return-void
.end method
