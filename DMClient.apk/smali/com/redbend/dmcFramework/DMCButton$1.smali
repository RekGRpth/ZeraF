.class Lcom/redbend/dmcFramework/DMCButton$1;
.super Ljava/lang/Object;
.source "DMCButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/redbend/dmcFramework/DMCButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/redbend/dmcFramework/DMCButton;


# direct methods
.method constructor <init>(Lcom/redbend/dmcFramework/DMCButton;)V
    .locals 0

    iput-object p1, p0, Lcom/redbend/dmcFramework/DMCButton$1;->this$0:Lcom/redbend/dmcFramework/DMCButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/redbend/dmcFramework/DMCButton$1;->this$0:Lcom/redbend/dmcFramework/DMCButton;

    # getter for: Lcom/redbend/dmcFramework/DMCButton;->canFireClick:Z
    invoke-static {v0}, Lcom/redbend/dmcFramework/DMCButton;->access$000(Lcom/redbend/dmcFramework/DMCButton;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/redbend/dmcFramework/DMCButton$1;->this$0:Lcom/redbend/dmcFramework/DMCButton;

    # getter for: Lcom/redbend/dmcFramework/DMCButton;->listener:Landroid/view/View$OnClickListener;
    invoke-static {v0}, Lcom/redbend/dmcFramework/DMCButton;->access$100(Lcom/redbend/dmcFramework/DMCButton;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    iget-object v0, p0, Lcom/redbend/dmcFramework/DMCButton$1;->this$0:Lcom/redbend/dmcFramework/DMCButton;

    const/4 v1, 0x0

    # setter for: Lcom/redbend/dmcFramework/DMCButton;->canFireClick:Z
    invoke-static {v0, v1}, Lcom/redbend/dmcFramework/DMCButton;->access$002(Lcom/redbend/dmcFramework/DMCButton;Z)Z

    :cond_0
    return-void
.end method
