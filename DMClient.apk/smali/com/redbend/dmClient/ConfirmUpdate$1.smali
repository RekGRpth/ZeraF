.class Lcom/redbend/dmClient/ConfirmUpdate$1;
.super Ljava/lang/Object;
.source "ConfirmUpdate.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/redbend/dmClient/ConfirmUpdate;->setContentView(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/redbend/dmClient/ConfirmUpdate;


# direct methods
.method constructor <init>(Lcom/redbend/dmClient/ConfirmUpdate;)V
    .locals 0

    iput-object p1, p0, Lcom/redbend/dmClient/ConfirmUpdate$1;->this$0:Lcom/redbend/dmClient/ConfirmUpdate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v2, p0, Lcom/redbend/dmClient/ConfirmUpdate$1;->this$0:Lcom/redbend/dmClient/ConfirmUpdate;

    # getter for: Lcom/redbend/dmClient/ConfirmUpdate;->a:Lcom/redbend/dmcFramework/DmcApplication;
    invoke-static {v2}, Lcom/redbend/dmClient/ConfirmUpdate;->access$000(Lcom/redbend/dmClient/ConfirmUpdate;)Lcom/redbend/dmcFramework/DmcApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/redbend/dmcFramework/DmcApplication;->getConfig()Lcom/gmobi/utils/AppConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/gmobi/utils/AppConfig;->menuIsOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Lcom/redbend/dmcFramework/DmcUtils;

    iget-object v2, p0, Lcom/redbend/dmClient/ConfirmUpdate$1;->this$0:Lcom/redbend/dmClient/ConfirmUpdate;

    # getter for: Lcom/redbend/dmClient/ConfirmUpdate;->a:Lcom/redbend/dmcFramework/DmcApplication;
    invoke-static {v2}, Lcom/redbend/dmClient/ConfirmUpdate;->access$100(Lcom/redbend/dmClient/ConfirmUpdate;)Lcom/redbend/dmcFramework/DmcApplication;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/redbend/dmcFramework/DmcUtils;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/redbend/dmcFramework/DmcUtils;->checkNetworkWithRoamingConfig()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/redbend/dmClient/ConfirmUpdate$1;->this$0:Lcom/redbend/dmClient/ConfirmUpdate;

    # getter for: Lcom/redbend/dmClient/ConfirmUpdate;->a:Lcom/redbend/dmcFramework/DmcApplication;
    invoke-static {v3}, Lcom/redbend/dmClient/ConfirmUpdate;->access$200(Lcom/redbend/dmClient/ConfirmUpdate;)Lcom/redbend/dmcFramework/DmcApplication;

    move-result-object v3

    iget-object v3, v3, Lcom/redbend/dmcFramework/DmcApplication;->currentActivity:Lcom/redbend/dmcFramework/DmcActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b0013

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b0015

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/redbend/dmClient/ConfirmUpdate$1;->this$0:Lcom/redbend/dmClient/ConfirmUpdate;

    # getter for: Lcom/redbend/dmClient/ConfirmUpdate;->a:Lcom/redbend/dmcFramework/DmcApplication;
    invoke-static {v2}, Lcom/redbend/dmClient/ConfirmUpdate;->access$300(Lcom/redbend/dmClient/ConfirmUpdate;)Lcom/redbend/dmcFramework/DmcApplication;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/redbend/dmcFramework/DmcApplication;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0
.end method
