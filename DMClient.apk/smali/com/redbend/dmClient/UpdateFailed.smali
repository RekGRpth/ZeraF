.class public Lcom/redbend/dmClient/UpdateFailed;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "UpdateFailed.java"


# instance fields
.field wi:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "UpdateFailed"

    iput-object v1, p0, Lcom/redbend/dmClient/UpdateFailed;->LOG_TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/redbend/dmClient/UpdateFailed;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/redbend/dmClient/UpdateFailed;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v1, 0x10000006

    iget-object v2, p0, Lcom/redbend/dmClient/UpdateFailed;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/redbend/dmClient/UpdateFailed;->wi:Landroid/os/PowerManager$WakeLock;

    const v1, 0x7f030031

    invoke-virtual {p0, v1}, Lcom/redbend/dmClient/UpdateFailed;->setContentView(I)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/redbend/dmcFramework/DmcActivity;->finishOnStop:Z

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/redbend/dmClient/UpdateFailed;->wi:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v1, 0x3a98

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    invoke-super {p0}, Lcom/redbend/dmcFramework/DmcActivity;->onResume()V

    return-void
.end method
