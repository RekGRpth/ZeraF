.class public final Lcom/redbend/dmClient/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/redbend/dmClient/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBarWindow:[I

.field public static final ActionBarWindow_windowActionBar:I = 0x0

.field public static final ActionBarWindow_windowActionBarOverlay:I = 0x1

.field public static final ActionBarWindow_windowSplitActionBar:I = 0x2

.field public static final ActionBar_background:I = 0xa

.field public static final ActionBar_backgroundSplit:I = 0xc

.field public static final ActionBar_backgroundStacked:I = 0xb

.field public static final ActionBar_customNavigationLayout:I = 0xd

.field public static final ActionBar_displayOptions:I = 0x3

.field public static final ActionBar_divider:I = 0x9

.field public static final ActionBar_height:I = 0x1

.field public static final ActionBar_homeLayout:I = 0xe

.field public static final ActionBar_icon:I = 0x7

.field public static final ActionBar_indeterminateProgressStyle:I = 0x10

.field public static final ActionBar_itemPadding:I = 0x12

.field public static final ActionBar_logo:I = 0x8

.field public static final ActionBar_navigationMode:I = 0x2

.field public static final ActionBar_progressBarPadding:I = 0x11

.field public static final ActionBar_progressBarStyle:I = 0xf

.field public static final ActionBar_subtitle:I = 0x4

.field public static final ActionBar_subtitleTextStyle:I = 0x6

.field public static final ActionBar_title:I = 0x0

.field public static final ActionBar_titleTextStyle:I = 0x5

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x3

.field public static final ActionMode_backgroundSplit:I = 0x4

.field public static final ActionMode_height:I = 0x0

.field public static final ActionMode_subtitleTextStyle:I = 0x2

.field public static final ActionMode_titleTextStyle:I = 0x1

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x1

.field public static final ActivityChooserView_initialActivityCount:I = 0x0

.field public static final CompatTextView:[I

.field public static final CompatTextView_textAllCaps:I = 0x0

.field public static final LinearLayoutICS:[I

.field public static final LinearLayoutICS_divider:I = 0x0

.field public static final LinearLayoutICS_dividerPadding:I = 0x2

.field public static final LinearLayoutICS_showDividers:I = 0x1

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0xe

.field public static final MenuItem_actionProviderClass:I = 0x10

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_showAsAction:I = 0xd

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_preserveIconSpacing:I = 0x7

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final SearchView:[I

.field public static final SearchView_android_imeOptions:I = 0x2

.field public static final SearchView_android_inputType:I = 0x1

.field public static final SearchView_android_maxWidth:I = 0x0

.field public static final SearchView_iconifiedByDefault:I = 0x3

.field public static final SearchView_queryHint:I = 0x4

.field public static final Spinner:[I

.field public static final Spinner_android_dropDownHorizontalOffset:I = 0x4

.field public static final Spinner_android_dropDownSelector:I = 0x1

.field public static final Spinner_android_dropDownVerticalOffset:I = 0x5

.field public static final Spinner_android_dropDownWidth:I = 0x3

.field public static final Spinner_android_gravity:I = 0x0

.field public static final Spinner_android_popupBackground:I = 0x2

.field public static final Spinner_disableChildrenWhenDisabled:I = 0x9

.field public static final Spinner_popupPromptView:I = 0x8

.field public static final Spinner_prompt:I = 0x6

.field public static final Spinner_spinnerMode:I = 0x7

.field public static final Theme:[I

.field public static final Theme_actionDropDownStyle:I = 0x0

.field public static final Theme_dropdownListPreferredItemHeight:I = 0x1

.field public static final Theme_listChoiceBackgroundIndicator:I = 0x5

.field public static final Theme_panelMenuListTheme:I = 0x4

.field public static final Theme_panelMenuListWidth:I = 0x3

.field public static final Theme_popupMenuStyle:I = 0x2

.field public static final View:[I

.field public static final View_android_focusable:I = 0x0

.field public static final View_paddingEnd:I = 0x2

.field public static final View_paddingStart:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->ActionBar:[I

    new-array v0, v3, [I

    const v1, 0x10100b3

    aput v1, v0, v2

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->ActionBarLayout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->ActionBarWindow:[I

    new-array v0, v3, [I

    const v1, 0x101013f

    aput v1, v0, v2

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->ActionMenuItemView:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->ActionMenuView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->ActionMode:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->ActivityChooserView:[I

    new-array v0, v3, [I

    const v1, 0x7f010069

    aput v1, v0, v2

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->CompatTextView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->LinearLayoutICS:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->MenuGroup:[I

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->MenuItem:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->MenuView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->SearchView:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->Spinner:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->Theme:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/redbend/dmClient/R$styleable;->View:[I

    return-void

    :array_0
    .array-data 4
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
    .end array-data

    :array_1
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
    .end array-data

    :array_2
    .array-data 4
        0x7f010022
        0x7f010026
        0x7f010027
        0x7f01002b
        0x7f01002d
    .end array-data

    :array_3
    .array-data 4
        0x7f010066
        0x7f010067
    .end array-data

    :array_4
    .array-data 4
        0x7f01002a
        0x7f010051
        0x7f010052
    .end array-data

    :array_5
    .array-data 4
        0x101000e
        0x10100d0
        0x1010194
        0x10101de
        0x10101df
        0x10101e0
    .end array-data

    :array_6
    .array-data 4
        0x1010002
        0x101000e
        0x10100d0
        0x1010106
        0x1010194
        0x10101de
        0x10101df
        0x10101e1
        0x10101e2
        0x10101e3
        0x10101e4
        0x10101e5
        0x101026f
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
    .end array-data

    :array_7
    .array-data 4
        0x10100ae
        0x101012c
        0x101012d
        0x101012e
        0x101012f
        0x1010130
        0x1010131
        0x101041a
    .end array-data

    :array_8
    .array-data 4
        0x101011f
        0x1010220
        0x1010264
        0x7f010056
        0x7f010057
    .end array-data

    :array_9
    .array-data 4
        0x10100af
        0x1010175
        0x1010176
        0x1010262
        0x10102ac
        0x10102ad
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
    .end array-data

    :array_a
    .array-data 4
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
        0x7f010048
    .end array-data

    :array_b
    .array-data 4
        0x10100da
        0x7f010034
        0x7f010035
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
