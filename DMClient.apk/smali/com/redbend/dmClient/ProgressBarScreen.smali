.class public Lcom/redbend/dmClient/ProgressBarScreen;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "ProgressBarScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "ProgressBarScreen"

    iput-object v0, p0, Lcom/redbend/dmClient/ProgressBarScreen;->LOG_TAG:Ljava/lang/String;

    iget-object v0, p0, Lcom/redbend/dmClient/ProgressBarScreen;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/redbend/dmClient/ProgressBarScreen;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const v0, 0x7f030027

    invoke-virtual {p0, v0}, Lcom/redbend/dmClient/ProgressBarScreen;->setContentView(I)V

    return-void
.end method

.method public updateProgress(I)V
    .locals 5
    .param p1    # I

    const v3, 0x7f060051

    :try_start_0
    invoke-virtual {p0, v3}, Lcom/redbend/dmClient/ProgressBarScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v3

    if-eq v3, p1, :cond_0

    invoke-virtual {v1, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_0
    const v3, 0x7f060052

    invoke-virtual {p0, v3}, Lcom/redbend/dmClient/ProgressBarScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v3, p0, Lcom/redbend/dmClient/ProgressBarScreen;->LOG_TAG:Ljava/lang/String;

    const-string v4, "The screen is not initialized, but the updates from SM layer already coming"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
