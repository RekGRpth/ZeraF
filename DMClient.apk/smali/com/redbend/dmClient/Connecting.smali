.class public Lcom/redbend/dmClient/Connecting;
.super Lcom/redbend/dmcFramework/DmcActivity;
.source "Connecting.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/redbend/dmcFramework/DmcActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/redbend/dmcFramework/DmcActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "Connecting"

    iput-object v0, p0, Lcom/redbend/dmClient/Connecting;->LOG_TAG:Ljava/lang/String;

    iget-object v0, p0, Lcom/redbend/dmClient/Connecting;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/redbend/dmClient/Connecting;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const v0, 0x7f03001e

    invoke-virtual {p0, v0}, Lcom/redbend/dmClient/Connecting;->setContentView(I)V

    return-void
.end method
