.class public final Landroid/a/a/a/a$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/a/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field a:Landroid/content/Context;

.field b:Ljava/lang/CharSequence;

.field c:Ljava/lang/CharSequence;

.field d:Landroid/app/PendingIntent;

.field e:Landroid/graphics/Bitmap;

.field f:I

.field g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/a/a/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field h:Landroid/app/Notification;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/a/a/a/a$b;->g:Ljava/util/ArrayList;

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Landroid/a/a/a/a$b;->h:Landroid/app/Notification;

    iput-object p1, p0, Landroid/a/a/a/a$b;->a:Landroid/content/Context;

    iget-object v0, p0, Landroid/a/a/a/a$b;->h:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Landroid/app/Notification;->when:J

    iget-object v0, p0, Landroid/a/a/a/a$b;->h:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    const/4 v0, 0x0

    iput v0, p0, Landroid/a/a/a/a$b;->f:I

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/a/a/a/a$b;
    .locals 1

    iget-object v0, p0, Landroid/a/a/a/a$b;->h:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Landroid/a/a/a/a$b;
    .locals 0

    iput-object p1, p0, Landroid/a/a/a/a$b;->d:Landroid/app/PendingIntent;

    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)Landroid/a/a/a/a$b;
    .locals 0

    iput-object p1, p0, Landroid/a/a/a/a$b;->e:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/a/a/a/a$b;
    .locals 0

    iput-object p1, p0, Landroid/a/a/a/a$b;->b:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final a()Landroid/app/Notification;
    .locals 1

    invoke-static {}, Landroid/a/a/a/a;->a()Landroid/a/a/a/a$c;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/a/a/a/a$c;->a(Landroid/a/a/a/a$b;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/a/a/a/a$b;
    .locals 0

    iput-object p1, p0, Landroid/a/a/a/a$b;->c:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)Landroid/a/a/a/a$b;
    .locals 1

    iget-object v0, p0, Landroid/a/a/a/a$b;->h:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    return-object p0
.end method
