.class public final Landroid/a/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/a/a/a/a$a;,
        Landroid/a/a/a/a$b;,
        Landroid/a/a/a/a$c;,
        Landroid/a/a/a/a$d;,
        Landroid/a/a/a/a$e;,
        Landroid/a/a/a/a$f;,
        Landroid/a/a/a/a$g;
    }
.end annotation


# static fields
.field private static final a:Landroid/a/a/a/a$c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/a/a/a/a$g;

    invoke-direct {v0}, Landroid/a/a/a/a$g;-><init>()V

    sput-object v0, Landroid/a/a/a/a;->a:Landroid/a/a/a/a$c;

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    new-instance v0, Landroid/a/a/a/a$f;

    invoke-direct {v0}, Landroid/a/a/a/a$f;-><init>()V

    sput-object v0, Landroid/a/a/a/a;->a:Landroid/a/a/a/a$c;

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    new-instance v0, Landroid/a/a/a/a$e;

    invoke-direct {v0}, Landroid/a/a/a/a$e;-><init>()V

    sput-object v0, Landroid/a/a/a/a;->a:Landroid/a/a/a/a$c;

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/a/a/a/a$d;

    invoke-direct {v0}, Landroid/a/a/a/a$d;-><init>()V

    sput-object v0, Landroid/a/a/a/a;->a:Landroid/a/a/a/a$c;

    goto :goto_0
.end method

.method static synthetic a()Landroid/a/a/a/a$c;
    .locals 1

    sget-object v0, Landroid/a/a/a/a;->a:Landroid/a/a/a/a$c;

    return-object v0
.end method
