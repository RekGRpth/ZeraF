.class public final Lb/K;
.super Lb/ag;
.source "SourceFile"

# interfaces
.implements Lb/t;


# instance fields
.field private final a:Lb/x;

.field private b:Ljava/nio/channels/SelectableChannel;

.field private final c:Lb/F;

.field private d:I

.field private volatile e:Z

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lb/e;I)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lb/ag;-><init>(Lb/e;I)V

    iput v1, p0, Lb/K;->d:I

    iput-boolean v1, p0, Lb/K;->e:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "reaper-1"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/K;->f:Ljava/lang/String;

    new-instance v0, Lb/F;

    iget-object v1, p0, Lb/K;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Lb/F;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lb/K;->c:Lb/F;

    new-instance v0, Lb/x;

    iget-object v1, p0, Lb/K;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Lb/x;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lb/K;->a:Lb/x;

    iget-object v0, p0, Lb/K;->a:Lb/x;

    invoke-virtual {v0}, Lb/x;->a()Ljava/nio/channels/SelectableChannel;

    move-result-object v0

    iput-object v0, p0, Lb/K;->b:Ljava/nio/channels/SelectableChannel;

    iget-object v0, p0, Lb/K;->c:Lb/F;

    iget-object v1, p0, Lb/K;->b:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1, p0}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;Lb/t;)V

    iget-object v0, p0, Lb/K;->c:Lb/F;

    iget-object v1, p0, Lb/K;->b:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1}, Lb/F;->b(Ljava/nio/channels/SelectableChannel;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lb/K;->c:Lb/F;

    invoke-virtual {v0}, Lb/F;->a()V

    iget-object v0, p0, Lb/K;->a:Lb/x;

    invoke-virtual {v0}, Lb/x;->b()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final a(Lb/Q;)V
    .locals 1

    iget-object v0, p0, Lb/K;->c:Lb/F;

    invoke-virtual {p1, v0}, Lb/Q;->a(Lb/F;)V

    iget v0, p0, Lb/K;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/K;->d:I

    return-void
.end method

.method public final d()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final e()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final f()Lb/x;
    .locals 1

    iget-object v0, p0, Lb/K;->a:Lb/x;

    return-object v0
.end method

.method public final g_()V
    .locals 3

    :goto_0
    iget-object v0, p0, Lb/K;->a:Lb/x;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lb/x;->a(J)Lb/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb/c;->a()Lb/ag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lb/ag;->a(Lb/c;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final h_()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final k()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/K;->e:Z

    iget v0, p0, Lb/K;->d:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lb/K;->x()V

    iget-object v0, p0, Lb/K;->c:Lb/F;

    iget-object v1, p0, Lb/K;->b:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/K;->c:Lb/F;

    invoke-virtual {v0}, Lb/F;->c()V

    :cond_0
    return-void
.end method

.method public final l()V
    .locals 1

    iget-object v0, p0, Lb/K;->c:Lb/F;

    invoke-virtual {v0}, Lb/F;->b()V

    return-void
.end method

.method public final m()V
    .locals 1

    iget-boolean v0, p0, Lb/K;->e:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lb/K;->v()V

    :cond_0
    return-void
.end method

.method protected final o()V
    .locals 2

    iget v0, p0, Lb/K;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/K;->d:I

    iget v0, p0, Lb/K;->d:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/K;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/K;->x()V

    iget-object v0, p0, Lb/K;->c:Lb/F;

    iget-object v1, p0, Lb/K;->b:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/K;->c:Lb/F;

    invoke-virtual {v0}, Lb/F;->c()V

    :cond_0
    return-void
.end method
