.class public final Lb/M$a;
.super Lb/f$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb/M;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/M$a$a;
    }
.end annotation


# static fields
.field private static synthetic c:[I


# instance fields
.field private b:Lb/M$a$a;


# direct methods
.method public constructor <init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lb/f$a;-><init>(Lb/s;ZLb/Q;Lb/B;Lb/a;)V

    sget-object v0, Lb/M$a$a;->a:Lb/M$a$a;

    iput-object v0, p0, Lb/M$a;->b:Lb/M$a$a;

    return-void
.end method

.method private static synthetic y()[I
    .locals 3

    sget-object v0, Lb/M$a;->c:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lb/M$a$a;->values()[Lb/M$a$a;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lb/M$a$a;->c:Lb/M$a$a;

    invoke-virtual {v1}, Lb/M$a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lb/M$a$a;->b:Lb/M$a$a;

    invoke-virtual {v1}, Lb/M$a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lb/M$a$a;->a:Lb/M$a$a;

    invoke-virtual {v1}, Lb/M$a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lb/M$a;->c:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lb/y;)Z
    .locals 3

    const/4 v2, 0x1

    invoke-static {}, Lb/M$a;->y()[I

    move-result-object v0

    iget-object v1, p0, Lb/M$a;->b:Lb/M$a$a;

    invoke-virtual {v1}, Lb/M$a$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lb/M$a;->b:Lb/M$a$a;

    invoke-virtual {v1}, Lb/M$a$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-virtual {p1}, Lb/y;->c()I

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Lb/y;->g()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lb/M$a$a;->c:Lb/M$a$a;

    iput-object v0, p0, Lb/M$a;->b:Lb/M$a$a;

    invoke-super {p0, p1}, Lb/f$a;->a(Lb/y;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p1}, Lb/y;->c()I

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-super {p0, p1}, Lb/f$a;->a(Lb/y;)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lb/y;->c()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lb/M$a$a;->b:Lb/M$a$a;

    iput-object v0, p0, Lb/M$a;->b:Lb/M$a$a;

    invoke-super {p0, p1}, Lb/f$a;->a(Lb/y;)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lb/y;->c()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lb/M$a$a;->b:Lb/M$a$a;

    iput-object v0, p0, Lb/M$a;->b:Lb/M$a$a;

    invoke-super {p0, p1}, Lb/f$a;->a(Lb/y;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final p()V
    .locals 1

    invoke-super {p0}, Lb/f$a;->p()V

    sget-object v0, Lb/M$a$a;->a:Lb/M$a$a;

    iput-object v0, p0, Lb/M$a;->b:Lb/M$a$a;

    return-void
.end method
