.class public Lb/P;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static synthetic g:Z


# instance fields
.field private a:Ljava/nio/channels/Pipe$SinkChannel;

.field private b:Ljava/nio/channels/Pipe$SourceChannel;

.field private c:Ljava/nio/channels/Selector;

.field private d:Ljava/nio/ByteBuffer;

.field private final e:Ljava/util/concurrent/atomic/AtomicInteger;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/P;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/P;->g:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lb/P;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iput v1, p0, Lb/P;->f:I

    :try_start_0
    invoke-static {}, Ljava/nio/channels/Pipe;->open()Ljava/nio/channels/Pipe;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/Pipe;->source()Ljava/nio/channels/Pipe$SourceChannel;

    move-result-object v1

    iput-object v1, p0, Lb/P;->b:Ljava/nio/channels/Pipe$SourceChannel;

    invoke-virtual {v0}, Ljava/nio/channels/Pipe;->sink()Ljava/nio/channels/Pipe$SinkChannel;

    move-result-object v0

    iput-object v0, p0, Lb/P;->a:Ljava/nio/channels/Pipe$SinkChannel;

    :try_start_1
    iget-object v0, p0, Lb/P;->a:Ljava/nio/channels/Pipe$SinkChannel;

    invoke-static {v0}, Lb/X;->a(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/P;->b:Ljava/nio/channels/Pipe$SourceChannel;

    invoke-static {v0}, Lb/X;->a(Ljava/nio/channels/SelectableChannel;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v0

    iput-object v0, p0, Lb/P;->c:Ljava/nio/channels/Selector;

    iget-object v0, p0, Lb/P;->b:Ljava/nio/channels/Pipe$SourceChannel;

    iget-object v1, p0, Lb/P;->c:Ljava/nio/channels/Selector;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/nio/channels/Pipe$SourceChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lb/P;->d:Ljava/nio/ByteBuffer;

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lb/ae$a;

    invoke-direct {v1, v0}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lb/ae$a;

    invoke-direct {v1, v0}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lb/ae$a;

    invoke-direct {v1, v0}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lb/P;->b:Ljava/nio/channels/Pipe$SourceChannel;

    invoke-virtual {v0}, Ljava/nio/channels/Pipe$SourceChannel;->close()V

    iget-object v0, p0, Lb/P;->a:Ljava/nio/channels/Pipe$SinkChannel;

    invoke-virtual {v0}, Ljava/nio/channels/Pipe$SinkChannel;->close()V

    iget-object v0, p0, Lb/P;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public final a(J)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v3, 0x0

    cmp-long v2, p1, v3

    if-nez v2, :cond_1

    :try_start_0
    iget v2, p0, Lb/P;->f:I

    iget-object v3, p0, Lb/P;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    if-ge v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    cmp-long v2, p1, v3

    if-gez v2, :cond_2

    iget-object v2, p0, Lb/P;->c:Ljava/nio/channels/Selector;

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/Selector;->select(J)I

    move-result v2

    :goto_1
    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lb/P;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v2, p1, p2}, Ljava/nio/channels/Selector;->select(J)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lb/ae$a;

    invoke-direct {v1, v0}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_3
    iget-object v1, p0, Lb/P;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method

.method public final b()Ljava/nio/channels/SelectableChannel;
    .locals 1

    iget-object v0, p0, Lb/P;->b:Ljava/nio/channels/Pipe$SourceChannel;

    return-object v0
.end method

.method public final c()V
    .locals 3

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    :cond_0
    :try_start_0
    iget-object v1, p0, Lb/P;->a:Ljava/nio/channels/Pipe$SinkChannel;

    invoke-virtual {v1, v0}, Ljava/nio/channels/Pipe$SinkChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lb/P;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    sget-boolean v0, Lb/P;->g:Z

    if-nez v0, :cond_1

    if-eq v1, v2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    new-instance v1, Lb/ae$a;

    invoke-direct {v1, v0}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_1
    return-void
.end method

.method public final d()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    iget v0, p0, Lb/P;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/P;->f:I

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lb/P;->b:Ljava/nio/channels/Pipe$SourceChannel;

    iget-object v1, p0, Lb/P;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/channels/Pipe$SourceChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    iget-object v1, p0, Lb/P;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lb/ae$a;

    invoke-direct {v1, v0}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v1
.end method
