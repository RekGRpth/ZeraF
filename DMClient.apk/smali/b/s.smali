.class public Lb/s;
.super Lb/ag;
.source "SourceFile"

# interfaces
.implements Lb/t;


# static fields
.field private static synthetic e:Z


# instance fields
.field private final a:Lb/x;

.field private final b:Ljava/nio/channels/SelectableChannel;

.field private final c:Lb/F;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/s;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/s;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;I)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lb/ag;-><init>(Lb/e;I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "iothread-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/s;->d:Ljava/lang/String;

    new-instance v0, Lb/F;

    iget-object v1, p0, Lb/s;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Lb/F;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lb/s;->c:Lb/F;

    new-instance v0, Lb/x;

    iget-object v1, p0, Lb/s;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Lb/x;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lb/s;->a:Lb/x;

    iget-object v0, p0, Lb/s;->a:Lb/x;

    invoke-virtual {v0}, Lb/x;->a()Ljava/nio/channels/SelectableChannel;

    move-result-object v0

    iput-object v0, p0, Lb/s;->b:Ljava/nio/channels/SelectableChannel;

    iget-object v0, p0, Lb/s;->c:Lb/F;

    iget-object v1, p0, Lb/s;->b:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1, p0}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;Lb/t;)V

    iget-object v0, p0, Lb/s;->c:Lb/F;

    iget-object v1, p0, Lb/s;->b:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1}, Lb/F;->b(Ljava/nio/channels/SelectableChannel;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lb/s;->c:Lb/F;

    invoke-virtual {v0}, Lb/F;->b()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d_()V
    .locals 0

    invoke-virtual {p0}, Lb/s;->v()V

    return-void
.end method

.method public final e()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lb/s;->c:Lb/F;

    invoke-virtual {v0}, Lb/F;->a()V

    iget-object v0, p0, Lb/s;->a:Lb/x;

    invoke-virtual {v0}, Lb/x;->b()V

    return-void
.end method

.method public final g_()V
    .locals 3

    :goto_0
    iget-object v0, p0, Lb/s;->a:Lb/x;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lb/x;->a(J)Lb/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb/c;->a()Lb/ag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lb/ag;->a(Lb/c;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final h()Lb/x;
    .locals 1

    iget-object v0, p0, Lb/s;->a:Lb/x;

    return-object v0
.end method

.method public final h_()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final i()I
    .locals 1

    iget-object v0, p0, Lb/s;->c:Lb/F;

    invoke-virtual {v0}, Lb/F;->d()I

    move-result v0

    return v0
.end method

.method public final j()Lb/F;
    .locals 1

    sget-boolean v0, Lb/s;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/s;->c:Lb/F;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/s;->c:Lb/F;

    return-object v0
.end method

.method protected final k()V
    .locals 2

    iget-object v0, p0, Lb/s;->c:Lb/F;

    iget-object v1, p0, Lb/s;->b:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/s;->c:Lb/F;

    invoke-virtual {v0}, Lb/F;->c()V

    return-void
.end method
