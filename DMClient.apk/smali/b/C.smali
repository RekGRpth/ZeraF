.class public abstract Lb/C;
.super Lb/ag;
.source "SourceFile"


# static fields
.field private static synthetic h:Z


# instance fields
.field protected final a:Lb/B;

.field private b:Z

.field private final c:Ljava/util/concurrent/atomic/AtomicLong;

.field private d:J

.field private e:Lb/C;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lb/C;",
            ">;"
        }
    .end annotation
.end field

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/C;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/C;->h:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;I)V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lb/ag;-><init>(Lb/e;I)V

    iput-boolean v1, p0, Lb/C;->b:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lb/C;->c:Ljava/util/concurrent/atomic/AtomicLong;

    iput-wide v2, p0, Lb/C;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Lb/C;->e:Lb/C;

    iput v1, p0, Lb/C;->g:I

    new-instance v0, Lb/B;

    invoke-direct {v0}, Lb/B;-><init>()V

    iput-object v0, p0, Lb/C;->a:Lb/B;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lb/C;->f:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lb/s;Lb/B;)V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lb/ag;-><init>(Lb/ag;)V

    iput-object p2, p0, Lb/C;->a:Lb/B;

    iput-boolean v1, p0, Lb/C;->b:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lb/C;->c:Ljava/util/concurrent/atomic/AtomicLong;

    iput-wide v2, p0, Lb/C;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Lb/C;->e:Lb/C;

    iput v1, p0, Lb/C;->g:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lb/C;->f:Ljava/util/Set;

    return-void
.end method

.method private a()V
    .locals 4

    iget-boolean v0, p0, Lb/C;->b:Z

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lb/C;->d:J

    iget-object v2, p0, Lb/C;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget v0, p0, Lb/C;->g:I

    if-nez v0, :cond_2

    sget-boolean v0, Lb/C;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/C;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/C;->e:Lb/C;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/C;->e:Lb/C;

    invoke-virtual {p0, v0}, Lb/C;->e(Lb/C;)V

    :cond_1
    invoke-virtual {p0}, Lb/C;->a_()V

    :cond_2
    return-void
.end method


# virtual methods
.method protected final a(Lb/C;)V
    .locals 1

    sget-boolean v0, Lb/C;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lb/C;->e:Lb/C;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p0, p1, Lb/C;->e:Lb/C;

    invoke-virtual {p0, p1}, Lb/C;->d(Lb/C;)V

    invoke-virtual {p0, p0, p1}, Lb/C;->a(Lb/C;Lb/C;)V

    return-void
.end method

.method protected a_()V
    .locals 0

    invoke-virtual {p0}, Lb/C;->f()V

    return-void
.end method

.method protected b(I)V
    .locals 2

    sget-boolean v0, Lb/C;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/C;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/C;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/C;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lb/C;->c(I)V

    iget-object v0, p0, Lb/C;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/C;->b:Z

    invoke-direct {p0}, Lb/C;->a()V

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/C;

    invoke-virtual {p0, v0, p1}, Lb/C;->a(Lb/C;I)V

    goto :goto_0
.end method

.method protected final b(Lb/C;)V
    .locals 1

    iget-boolean v0, p0, Lb/C;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lb/C;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/C;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lb/C;->c(I)V

    iget-object v0, p0, Lb/C;->a:Lb/B;

    iget v0, v0, Lb/B;->m:I

    invoke-virtual {p0, p1, v0}, Lb/C;->a(Lb/C;I)V

    goto :goto_0
.end method

.method protected final b_()V
    .locals 1

    iget-boolean v0, p0, Lb/C;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lb/C;->e:Lb/C;

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/C;->a:Lb/B;

    iget v0, v0, Lb/B;->m:I

    invoke-virtual {p0, v0}, Lb/C;->b(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lb/C;->e:Lb/C;

    invoke-virtual {p0, v0, p0}, Lb/C;->b(Lb/C;Lb/C;)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    iget v0, p0, Lb/C;->g:I

    add-int/2addr v0, p1

    iput v0, p0, Lb/C;->g:I

    return-void
.end method

.method protected final c(Lb/C;)V
    .locals 1

    iget-boolean v0, p0, Lb/C;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lb/C;->c(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lb/C;->a(Lb/C;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lb/C;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public abstract f()V
.end method

.method protected final f_()V
    .locals 4

    iget-wide v0, p0, Lb/C;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lb/C;->d:J

    invoke-direct {p0}, Lb/C;->a()V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lb/C;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    return-void
.end method

.method protected final l()Z
    .locals 1

    iget-boolean v0, p0, Lb/C;->b:Z

    return v0
.end method

.method public final m()V
    .locals 1

    sget-boolean v0, Lb/C;->h:Z

    if-nez v0, :cond_0

    iget v0, p0, Lb/C;->g:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lb/C;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/C;->g:I

    invoke-direct {p0}, Lb/C;->a()V

    return-void
.end method

.method protected final n()V
    .locals 0

    invoke-virtual {p0}, Lb/C;->m()V

    return-void
.end method
