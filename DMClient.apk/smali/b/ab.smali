.class public Lb/ab;
.super Lb/Q;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/ab$a;
    }
.end annotation


# static fields
.field private static h:Lb/W$a;

.field private static synthetic i:Z


# instance fields
.field private final b:Lb/l;

.field private final c:Lb/i;

.field private final d:Lb/W;

.field private e:Z

.field private f:Lb/y;

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/ab;->i:Z

    new-instance v0, Lb/W$a;

    invoke-direct {v0}, Lb/W$a;-><init>()V

    sput-object v0, Lb/ab;->h:Lb/W$a;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;II)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Lb/Q;-><init>(Lb/e;II)V

    iget-object v0, p0, Lb/ab;->a:Lb/B;

    const/16 v1, 0xa

    iput v1, v0, Lb/B;->l:I

    iput-boolean v2, p0, Lb/ab;->e:Z

    iput-boolean v2, p0, Lb/ab;->g:Z

    iget-object v0, p0, Lb/ab;->a:Lb/B;

    iput v2, v0, Lb/B;->m:I

    new-instance v0, Lb/l;

    invoke-direct {v0}, Lb/l;-><init>()V

    iput-object v0, p0, Lb/ab;->b:Lb/l;

    new-instance v0, Lb/i;

    invoke-direct {v0}, Lb/i;-><init>()V

    iput-object v0, p0, Lb/ab;->c:Lb/i;

    new-instance v0, Lb/W;

    invoke-direct {v0}, Lb/W;-><init>()V

    iput-object v0, p0, Lb/ab;->d:Lb/W;

    return-void
.end method

.method private a(Lb/y;)Z
    .locals 2

    iget-object v0, p0, Lb/ab;->d:Lb/W;

    invoke-virtual {p1}, Lb/y;->f()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lb/W;->a([B)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected final a(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/ab;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->c(Lb/E;)V

    return-void
.end method

.method protected final a(Lb/E;Z)V
    .locals 2

    sget-boolean v0, Lb/ab;->i:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/ab;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->a(Lb/E;)V

    iget-object v0, p0, Lb/ab;->c:Lb/i;

    invoke-virtual {v0, p1}, Lb/i;->a(Lb/E;)V

    iget-object v0, p0, Lb/ab;->d:Lb/W;

    sget-object v1, Lb/ab;->h:Lb/W$a;

    invoke-virtual {v0, v1, p1}, Lb/W;->a(Lb/W$a;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lb/E;->f()V

    return-void
.end method

.method protected a(Lb/y;I)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lb/y;->f()[B

    move-result-object v2

    array-length v3, v2

    if-lez v3, :cond_0

    aget-byte v3, v2, v1

    if-eqz v3, :cond_2

    aget-byte v3, v2, v1

    if-eq v3, v0, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    aget-byte v1, v2, v1

    if-ne v1, v0, :cond_3

    iget-object v1, p0, Lb/ab;->d:Lb/W;

    invoke-virtual {v1, v2, v0}, Lb/W;->a([BI)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lb/ab;->c:Lb/i;

    invoke-virtual {v0, p1, p2}, Lb/i;->a(Lb/y;I)Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lb/ab;->d:Lb/W;

    invoke-virtual {v1, v2, v0}, Lb/W;->b([BI)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lb/ab;->c:Lb/i;

    invoke-virtual {v0, p1, p2}, Lb/i;->a(Lb/y;I)Z

    move-result v0

    goto :goto_0
.end method

.method protected final a_(I)Lb/y;
    .locals 2

    iget-boolean v0, p0, Lb/ab;->e:Z

    if-eqz v0, :cond_1

    new-instance v0, Lb/y;

    iget-object v1, p0, Lb/ab;->f:Lb/y;

    invoke-direct {v0, v1}, Lb/y;-><init>(Lb/y;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lb/ab;->e:Z

    invoke-virtual {v0}, Lb/y;->d()Z

    move-result v1

    iput-boolean v1, p0, Lb/ab;->g:Z

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lb/y;->d()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    iget-object v0, p0, Lb/ab;->b:Lb/l;

    invoke-virtual {v0}, Lb/l;->a()Lb/y;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lb/ab;->g:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lb/ab;->a:Lb/B;

    iget-boolean v1, v1, Lb/B;->x:Z

    if-eqz v1, :cond_3

    invoke-direct {p0, v0}, Lb/ab;->a(Lb/y;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    invoke-virtual {v0}, Lb/y;->d()Z

    move-result v1

    iput-boolean v1, p0, Lb/ab;->g:Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lb/ab;->b:Lb/l;

    invoke-virtual {v0}, Lb/l;->a()Lb/y;

    move-result-object v0

    sget-boolean v1, Lb/ab;->i:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected final b(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/ab;->c:Lb/i;

    invoke-virtual {v0, p1}, Lb/i;->d(Lb/E;)V

    return-void
.end method

.method protected final c(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/ab;->b:Lb/l;

    invoke-virtual {v0, p1}, Lb/l;->b(Lb/E;)V

    iget-object v0, p0, Lb/ab;->c:Lb/i;

    invoke-virtual {v0, p1}, Lb/i;->c(Lb/E;)V

    return-void
.end method

.method protected final i(Lb/E;)V
    .locals 2

    iget-object v0, p0, Lb/ab;->d:Lb/W;

    sget-object v1, Lb/ab;->h:Lb/W$a;

    invoke-virtual {v0, v1, p1}, Lb/W;->a(Lb/W$a;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lb/E;->f()V

    return-void
.end method
