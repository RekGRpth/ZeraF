.class public Lb/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static synthetic f:Z


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lb/E;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/w;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/w;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lb/w;->b:I

    iput v0, p0, Lb/w;->c:I

    iput-boolean v0, p0, Lb/w;->d:Z

    iput-boolean v0, p0, Lb/w;->e:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/w;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/w;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lb/w;->c(Lb/E;)V

    return-void
.end method

.method public final a(Lb/y;)Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lb/w;->e:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v0

    iput-boolean v0, p0, Lb/w;->d:Z

    iget-boolean v0, p0, Lb/w;->d:Z

    iput-boolean v0, p0, Lb/w;->e:Z

    invoke-virtual {p1}, Lb/y;->h()V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lb/w;->a:Ljava/util/List;

    iget v3, p0, Lb/w;->c:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/E;

    invoke-virtual {v0, p1}, Lb/E;->a(Lb/y;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lb/w;->f:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lb/w;->d:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget v0, p0, Lb/w;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/w;->b:I

    iget v0, p0, Lb/w;->c:I

    iget v3, p0, Lb/w;->b:I

    if-ge v0, v3, :cond_4

    iget-object v0, p0, Lb/w;->a:Ljava/util/List;

    iget v3, p0, Lb/w;->c:I

    iget v4, p0, Lb/w;->b:I

    invoke-static {v0, v3, v4}, Lb/X;->a(Ljava/util/List;II)V

    :cond_2
    :goto_1
    iget v0, p0, Lb/w;->b:I

    if-gtz v0, :cond_0

    :cond_3
    iget v0, p0, Lb/w;->b:I

    if-nez v0, :cond_5

    const/16 v0, 0x23

    invoke-static {v0}, Lb/ae;->a(I)V

    move v0, v2

    goto :goto_0

    :cond_4
    iput v2, p0, Lb/w;->c:I

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v0

    iput-boolean v0, p0, Lb/w;->d:Z

    iget-boolean v0, p0, Lb/w;->d:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lb/w;->a:Ljava/util/List;

    iget v2, p0, Lb/w;->c:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/E;

    invoke-virtual {v0}, Lb/E;->f()V

    iget v0, p0, Lb/w;->b:I

    if-le v0, v1, :cond_6

    iget v0, p0, Lb/w;->c:I

    add-int/lit8 v0, v0, 0x1

    iget v2, p0, Lb/w;->b:I

    rem-int/2addr v0, v2

    iput v0, p0, Lb/w;->c:I

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final b(Lb/E;)V
    .locals 3

    iget-object v0, p0, Lb/w;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lb/w;->c:I

    if-ne v0, v1, :cond_0

    iget-boolean v1, p0, Lb/w;->d:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lb/w;->e:Z

    :cond_0
    iget v1, p0, Lb/w;->b:I

    if-ge v0, v1, :cond_1

    iget v1, p0, Lb/w;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lb/w;->b:I

    iget-object v1, p0, Lb/w;->a:Ljava/util/List;

    iget v2, p0, Lb/w;->b:I

    invoke-static {v1, v0, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/w;->c:I

    iget v1, p0, Lb/w;->b:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lb/w;->c:I

    :cond_1
    iget-object v0, p0, Lb/w;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final c(Lb/E;)V
    .locals 3

    iget-object v0, p0, Lb/w;->a:Ljava/util/List;

    iget-object v1, p0, Lb/w;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    iget v2, p0, Lb/w;->b:I

    invoke-static {v0, v1, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/w;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/w;->b:I

    return-void
.end method
