.class public abstract Lb/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lb/n;


# instance fields
.field private a:[B

.field private b:Ljava/nio/channels/FileChannel;

.field private c:I

.field private d:I

.field private e:I

.field private f:Ljava/nio/ByteBuffer;

.field private g:I

.field private h:Z


# direct methods
.method protected constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lb/k;->g:I

    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lb/k;->f:Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/k;->h:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)Lb/V;
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_0

    iget-object p1, p0, Lb/k;->f:Ljava/nio/ByteBuffer;

    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    new-instance v0, Lb/V$a;

    invoke-direct {v0, p1}, Lb/V$a;-><init>(Ljava/nio/ByteBuffer;)V

    :goto_1
    return-object v0

    :cond_3
    iget v0, p0, Lb/k;->e:I

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lb/k;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_4
    iget-object v0, p0, Lb/k;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-nez v0, :cond_5

    iget v0, p0, Lb/k;->e:I

    iget v1, p0, Lb/k;->g:I

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lb/k;->a:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v0, p0, Lb/k;->c:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    new-instance v0, Lb/V$a;

    invoke-direct {v0, v1}, Lb/V$a;-><init>(Ljava/nio/ByteBuffer;)V

    iput v3, p0, Lb/k;->c:I

    iput v3, p0, Lb/k;->e:I

    goto :goto_1

    :cond_5
    iget v0, p0, Lb/k;->e:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-lez v0, :cond_1

    iget-object v1, p0, Lb/k;->a:[B

    iget v2, p0, Lb/k;->c:I

    invoke-virtual {p1, v1, v2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    iget v1, p0, Lb/k;->c:I

    add-int/2addr v1, v0

    iput v1, p0, Lb/k;->c:I

    iget v1, p0, Lb/k;->e:I

    sub-int v0, v1, v0

    iput v0, p0, Lb/k;->e:I

    goto :goto_0
.end method

.method protected final a([BIIZ)V
    .locals 1

    iput-object p1, p0, Lb/k;->a:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lb/k;->b:Ljava/nio/channels/FileChannel;

    const/4 v0, 0x0

    iput v0, p0, Lb/k;->c:I

    iput p2, p0, Lb/k;->e:I

    iput p3, p0, Lb/k;->d:I

    return-void
.end method

.method protected abstract a()Z
.end method

.method public final b()Z
    .locals 1

    iget v0, p0, Lb/k;->e:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final c()I
    .locals 1

    iget v0, p0, Lb/k;->d:I

    return v0
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
