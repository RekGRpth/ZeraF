.class public final Lb/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lb/E;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lb/i;->b:I

    iput v0, p0, Lb/i;->c:I

    iput v0, p0, Lb/i;->d:I

    iput-boolean v0, p0, Lb/i;->e:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/i;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lb/i;->b:I

    return-void
.end method

.method public final a(Lb/E;)V
    .locals 3

    iget-boolean v0, p0, Lb/i;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    iget v1, p0, Lb/i;->d:I

    iget-object v2, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/i;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/i;->d:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    iget v1, p0, Lb/i;->c:I

    iget-object v2, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/i;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/i;->c:I

    iget v0, p0, Lb/i;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/i;->d:I

    goto :goto_0
.end method

.method public final a(Lb/y;I)Z
    .locals 1

    iget v0, p0, Lb/i;->c:I

    iput v0, p0, Lb/i;->b:I

    invoke-virtual {p0, p1, p2}, Lb/i;->b(Lb/y;I)Z

    move-result v0

    return v0
.end method

.method public final b(Lb/E;)V
    .locals 3

    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lb/i;->b:I

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lb/i;->d:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lb/i;->a:Ljava/util/List;

    iget v2, p0, Lb/i;->b:I

    invoke-static {v1, v0, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/i;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/i;->b:I

    goto :goto_0
.end method

.method public final b(Lb/y;I)Z
    .locals 8

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v4

    iget v0, p0, Lb/i;->b:I

    if-eqz v0, :cond_0

    move v1, v2

    :goto_0
    iget v0, p0, Lb/i;->b:I

    if-lt v1, v0, :cond_2

    :cond_0
    if-nez v4, :cond_1

    iget v0, p0, Lb/i;->d:I

    iput v0, p0, Lb/i;->c:I

    :cond_1
    iput-boolean v4, p0, Lb/i;->e:Z

    return v3

    :cond_2
    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/E;

    invoke-virtual {v0, p1}, Lb/E;->a(Lb/y;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lb/i;->a:Ljava/util/List;

    iget-object v6, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    iget v7, p0, Lb/i;->b:I

    add-int/lit8 v7, v7, -0x1

    invoke-static {v5, v6, v7}, Lb/X;->a(Ljava/util/List;II)V

    iget v5, p0, Lb/i;->b:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lb/i;->b:I

    iget-object v5, p0, Lb/i;->a:Ljava/util/List;

    iget-object v6, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget v6, p0, Lb/i;->c:I

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v0, v6}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/i;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/i;->c:I

    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    iget v5, p0, Lb/i;->c:I

    iget v6, p0, Lb/i;->d:I

    add-int/lit8 v6, v6, -0x1

    invoke-static {v0, v5, v6}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/i;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/i;->d:I

    move v0, v2

    :goto_1
    if-nez v0, :cond_3

    add-int/lit8 v1, v1, -0x1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v0}, Lb/E;->f()V

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public final c(Lb/E;)V
    .locals 2

    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lb/i;->b:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lb/i;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/i;->b:I

    :cond_0
    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lb/i;->c:I

    if-ge v0, v1, :cond_1

    iget v0, p0, Lb/i;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/i;->c:I

    :cond_1
    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lb/i;->d:I

    if-ge v0, v1, :cond_2

    iget v0, p0, Lb/i;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/i;->d:I

    :cond_2
    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final d(Lb/E;)V
    .locals 3

    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    iget-object v1, p0, Lb/i;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    iget v2, p0, Lb/i;->d:I

    invoke-static {v0, v1, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/i;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/i;->d:I

    iget-boolean v0, p0, Lb/i;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/i;->a:Ljava/util/List;

    iget v1, p0, Lb/i;->d:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lb/i;->c:I

    invoke-static {v0, v1, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/i;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/i;->c:I

    :cond_0
    return-void
.end method
