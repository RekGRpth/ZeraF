.class public Lb/E;
.super Lb/ag;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/E$a;,
        Lb/E$b;
    }
.end annotation


# static fields
.field private static synthetic p:Z


# instance fields
.field private a:Lb/ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/ac",
            "<",
            "Lb/y;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lb/ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/ac",
            "<",
            "Lb/y;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field private e:I

.field private f:I

.field private g:J

.field private h:J

.field private i:J

.field private j:Lb/E;

.field private k:Lb/E$a;

.field private l:Lb/E$b;

.field private m:Z

.field private n:Lb/b;

.field private o:Lb/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/E;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/E;->p:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lb/ag;Lb/ac;Lb/ac;IIZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/ag;",
            "Lb/ac",
            "<",
            "Lb/y;",
            ">;",
            "Lb/ac",
            "<",
            "Lb/y;",
            ">;IIZ)V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v0, 0x1

    const-wide/16 v1, 0x0

    invoke-direct {p0, p1}, Lb/ag;-><init>(Lb/ag;)V

    iput-object p2, p0, Lb/E;->a:Lb/ac;

    iput-object p3, p0, Lb/E;->b:Lb/ac;

    iput-boolean v0, p0, Lb/E;->c:Z

    iput-boolean v0, p0, Lb/E;->d:Z

    iput p5, p0, Lb/E;->e:I

    sget-object v0, Lb/d;->f:Lb/d;

    invoke-virtual {v0}, Lb/d;->a()I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    if-le p4, v0, :cond_0

    sget-object v0, Lb/d;->f:Lb/d;

    invoke-virtual {v0}, Lb/d;->a()I

    move-result v0

    sub-int v0, p4, v0

    :goto_0
    iput v0, p0, Lb/E;->f:I

    iput-wide v1, p0, Lb/E;->g:J

    iput-wide v1, p0, Lb/E;->h:J

    iput-wide v1, p0, Lb/E;->i:J

    iput-object v3, p0, Lb/E;->j:Lb/E;

    iput-object v3, p0, Lb/E;->k:Lb/E$a;

    sget-object v0, Lb/E$b;->a:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    iput-boolean p6, p0, Lb/E;->m:Z

    iput-object p1, p0, Lb/E;->o:Lb/ag;

    return-void

    :cond_0
    add-int/lit8 v0, p4, 0x1

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method private a(Lb/E;)V
    .locals 1

    sget-boolean v0, Lb/E;->p:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lb/E;->j:Lb/E;

    return-void
.end method

.method public static a([Lb/ag;[Lb/E;[I[Z)V
    .locals 11

    new-instance v2, Lb/ac;

    const-class v0, Lb/y;

    sget-object v1, Lb/d;->a:Lb/d;

    invoke-virtual {v1}, Lb/d;->a()I

    move-result v1

    invoke-direct {v2, v0, v1}, Lb/ac;-><init>(Ljava/lang/Class;I)V

    new-instance v3, Lb/ac;

    const-class v0, Lb/y;

    sget-object v1, Lb/d;->a:Lb/d;

    invoke-virtual {v1}, Lb/d;->a()I

    move-result v1

    invoke-direct {v3, v0, v1}, Lb/ac;-><init>(Ljava/lang/Class;I)V

    const/4 v7, 0x0

    new-instance v0, Lb/E;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    const/4 v4, 0x1

    aget v4, p2, v4

    const/4 v5, 0x0

    aget v5, p2, v5

    const/4 v6, 0x0

    aget-boolean v6, p3, v6

    invoke-direct/range {v0 .. v6}, Lb/E;-><init>(Lb/ag;Lb/ac;Lb/ac;IIZ)V

    aput-object v0, p1, v7

    const/4 v0, 0x1

    new-instance v4, Lb/E;

    const/4 v1, 0x1

    aget-object v5, p0, v1

    const/4 v1, 0x0

    aget v8, p2, v1

    const/4 v1, 0x1

    aget v9, p2, v1

    const/4 v1, 0x1

    aget-boolean v10, p3, v1

    move-object v6, v3

    move-object v7, v2

    invoke-direct/range {v4 .. v10}, Lb/E;-><init>(Lb/ag;Lb/ac;Lb/ac;IIZ)V

    aput-object v4, p1, v0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    const/4 v1, 0x1

    aget-object v1, p1, v1

    invoke-direct {v0, v1}, Lb/E;->a(Lb/E;)V

    const/4 v0, 0x1

    aget-object v0, p1, v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-direct {v0, v1}, Lb/E;->a(Lb/E;)V

    return-void
.end method

.method private m()V
    .locals 2

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->a:Lb/E$b;

    if-ne v0, v1, :cond_1

    sget-object v0, Lb/E$b;->b:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->c:Lb/E$b;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    iput-object v0, p0, Lb/E;->b:Lb/ac;

    iget-object v0, p0, Lb/E;->j:Lb/E;

    invoke-virtual {p0, v0}, Lb/E;->l(Lb/E;)V

    sget-object v0, Lb/E$b;->d:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    goto :goto_0

    :cond_2
    sget-boolean v0, Lb/E;->p:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method


# virtual methods
.method public final a()Lb/b;
    .locals 1

    iget-object v0, p0, Lb/E;->n:Lb/b;

    return-object v0
.end method

.method protected final a(J)V
    .locals 2

    iput-wide p1, p0, Lb/E;->i:J

    iget-boolean v0, p0, Lb/E;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->a:Lb/E$b;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/E;->d:Z

    iget-object v0, p0, Lb/E;->k:Lb/E$a;

    invoke-interface {v0, p0}, Lb/E$a;->e(Lb/E;)V

    :cond_0
    return-void
.end method

.method public final a(Lb/E$a;)V
    .locals 1

    sget-boolean v0, Lb/E;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/E;->k:Lb/E$a;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lb/E;->k:Lb/E$a;

    return-void
.end method

.method public final a(Lb/b;)V
    .locals 0

    iput-object p1, p0, Lb/E;->n:Lb/b;

    return-void
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 2

    sget-boolean v0, Lb/E;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/E;->b:Lb/ac;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/E;->b:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->b()Z

    :cond_1
    iget-object v0, p0, Lb/E;->b:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->d()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    sget-boolean v0, Lb/E;->p:Z

    if-nez v0, :cond_2

    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    check-cast p1, Lb/ac;

    iput-object p1, p0, Lb/E;->b:Lb/ac;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/E;->d:Z

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->a:Lb/E$b;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lb/E;->k:Lb/E$a;

    invoke-interface {v0, p0}, Lb/E$a;->f(Lb/E;)V

    :cond_3
    return-void
.end method

.method public final a(Z)V
    .locals 3

    const/4 v2, 0x0

    iput-boolean p1, p0, Lb/E;->m:Z

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->e:Lb/E$b;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->f:Lb/E$b;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->d:Lb/E$b;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->a:Lb/E$b;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lb/E;->j:Lb/E;

    invoke-virtual {p0, v0}, Lb/E;->k(Lb/E;)V

    sget-object v0, Lb/E$b;->e:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    :cond_2
    :goto_1
    iput-boolean v2, p0, Lb/E;->d:Z

    iget-object v0, p0, Lb/E;->b:Lb/ac;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/E;->e()V

    new-instance v0, Lb/y;

    invoke-direct {v0}, Lb/y;-><init>()V

    invoke-virtual {v0}, Lb/y;->e()V

    iget-object v1, p0, Lb/E;->b:Lb/ac;

    invoke-virtual {v1, v0, v2}, Lb/ac;->a(Ljava/lang/Object;Z)V

    invoke-virtual {p0}, Lb/E;->f()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->c:Lb/E$b;

    if-ne v0, v1, :cond_4

    iget-boolean v0, p0, Lb/E;->m:Z

    if-nez v0, :cond_4

    const/4 v0, 0x0

    iput-object v0, p0, Lb/E;->b:Lb/ac;

    iget-object v0, p0, Lb/E;->j:Lb/E;

    invoke-virtual {p0, v0}, Lb/E;->l(Lb/E;)V

    sget-object v0, Lb/E$b;->d:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->c:Lb/E$b;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->b:Lb/E$b;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lb/E;->j:Lb/E;

    invoke-virtual {p0, v0}, Lb/E;->k(Lb/E;)V

    sget-object v0, Lb/E$b;->e:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    goto :goto_1

    :cond_5
    sget-boolean v0, Lb/E;->p:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final a(Lb/y;)Z
    .locals 4

    invoke-virtual {p0}, Lb/E;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v0

    iget-object v1, p0, Lb/E;->b:Lb/ac;

    invoke-virtual {v1, p1, v0}, Lb/ac;->a(Ljava/lang/Object;Z)V

    if-nez v0, :cond_1

    iget-wide v0, p0, Lb/E;->h:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lb/E;->h:J

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lb/E;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v2, Lb/E$b;->a:Lb/E$b;

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v2, Lb/E$b;->c:Lb/E$b;

    if-eq v0, v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lb/E;->a:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->c()Z

    move-result v0

    if-nez v0, :cond_2

    iput-boolean v1, p0, Lb/E;->c:Z

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lb/E;->a:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/y;

    invoke-virtual {v0}, Lb/y;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lb/E;->a:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/y;

    sget-boolean v2, Lb/E;->p:Z

    if-nez v2, :cond_3

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    invoke-direct {p0}, Lb/E;->m()V

    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Lb/y;
    .locals 5

    const/4 v1, 0x0

    iget-boolean v0, p0, Lb/E;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v2, Lb/E$b;->a:Lb/E$b;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v2, Lb/E$b;->c:Lb/E$b;

    if-eq v0, v2, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lb/E;->a:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/y;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/E;->c:Z

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lb/y;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lb/E;->m()V

    move-object v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lb/y;->d()Z

    move-result v1

    if-nez v1, :cond_5

    iget-wide v1, p0, Lb/E;->g:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lb/E;->g:J

    :cond_5
    iget v1, p0, Lb/E;->f:I

    if-lez v1, :cond_1

    iget-wide v1, p0, Lb/E;->g:J

    iget v3, p0, Lb/E;->f:I

    int-to-long v3, v3

    rem-long/2addr v1, v3

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    iget-object v1, p0, Lb/E;->j:Lb/E;

    iget-wide v2, p0, Lb/E;->g:J

    invoke-virtual {p0, v1, v2, v3}, Lb/E;->a(Lb/E;J)V

    goto :goto_0
.end method

.method protected final c_()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->a:Lb/E$b;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lb/E;->m:Z

    if-nez v0, :cond_1

    sget-object v0, Lb/E$b;->d:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    iput-object v2, p0, Lb/E;->b:Lb/ac;

    iget-object v0, p0, Lb/E;->j:Lb/E;

    invoke-virtual {p0, v0}, Lb/E;->l(Lb/E;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lb/E$b;->c:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->b:Lb/E$b;

    if-ne v0, v1, :cond_3

    sget-object v0, Lb/E$b;->d:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    iput-object v2, p0, Lb/E;->b:Lb/ac;

    iget-object v0, p0, Lb/E;->j:Lb/E;

    invoke-virtual {p0, v0}, Lb/E;->l(Lb/E;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->e:Lb/E$b;

    if-ne v0, v1, :cond_4

    sget-object v0, Lb/E$b;->f:Lb/E$b;

    iput-object v0, p0, Lb/E;->l:Lb/E$b;

    iput-object v2, p0, Lb/E;->b:Lb/ac;

    iget-object v0, p0, Lb/E;->j:Lb/E;

    invoke-virtual {p0, v0}, Lb/E;->l(Lb/E;)V

    goto :goto_0

    :cond_4
    sget-boolean v0, Lb/E;->p:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final d()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lb/E;->d:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lb/E;->l:Lb/E$b;

    sget-object v3, Lb/E$b;->a:Lb/E$b;

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lb/E;->e:I

    if-lez v2, :cond_2

    iget-wide v2, p0, Lb/E;->h:J

    iget-wide v4, p0, Lb/E;->i:J

    sub-long/2addr v2, v4

    iget v4, p0, Lb/E;->e:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v1

    :goto_1
    if-eqz v2, :cond_3

    iput-boolean v0, p0, Lb/E;->d:Z

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lb/E;->b:Lb/ac;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lb/E;->b:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/y;

    if-nez v0, :cond_2

    :cond_1
    return-void

    :cond_2
    sget-boolean v1, Lb/E;->p:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lb/y;->c()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected final e_()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lb/E;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/E;->k:Lb/E$a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/E;->k:Lb/E$a;

    invoke-interface {v0, p0}, Lb/E$a;->g(Lb/E;)V

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->e:Lb/E$b;

    if-ne v0, v1, :cond_2

    iput-object v2, p0, Lb/E;->b:Lb/ac;

    iget-object v0, p0, Lb/E;->j:Lb/E;

    invoke-virtual {p0, v0}, Lb/E;->l(Lb/E;)V

    :cond_1
    iget-object v0, p0, Lb/E;->a:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->d()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    iput-object v2, p0, Lb/E;->a:Lb/ac;

    return-void

    :cond_2
    sget-boolean v0, Lb/E;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->d:Lb/E$b;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->f:Lb/E$b;

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->d:Lb/E$b;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lb/E;->b:Lb/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/E;->b:Lb/ac;

    invoke-virtual {v0}, Lb/ac;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/E;->j:Lb/E;

    invoke-virtual {p0, v0}, Lb/E;->j(Lb/E;)V

    goto :goto_0
.end method

.method protected final g()V
    .locals 2

    iget-boolean v0, p0, Lb/E;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->a:Lb/E$b;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->c:Lb/E$b;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/E;->c:Z

    iget-object v0, p0, Lb/E;->k:Lb/E$a;

    invoke-interface {v0, p0}, Lb/E$a;->d(Lb/E;)V

    :cond_1
    return-void
.end method

.method public final l()V
    .locals 3

    iget-object v0, p0, Lb/E;->l:Lb/E$b;

    sget-object v1, Lb/E$b;->a:Lb/E$b;

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lb/E;->a:Lb/ac;

    new-instance v0, Lb/ac;

    const-class v1, Lb/y;

    sget-object v2, Lb/d;->a:Lb/d;

    invoke-virtual {v2}, Lb/d;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lb/ac;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lb/E;->a:Lb/ac;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/E;->c:Z

    iget-object v0, p0, Lb/E;->j:Lb/E;

    iget-object v1, p0, Lb/E;->a:Lb/ac;

    invoke-virtual {p0, v0, v1}, Lb/E;->a(Lb/E;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lb/E;->o:Lb/ag;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
