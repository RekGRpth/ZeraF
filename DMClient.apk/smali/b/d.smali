.class public final enum Lb/d;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lb/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lb/d;

.field public static final enum b:Lb/d;

.field public static final enum c:Lb/d;

.field public static final enum d:Lb/d;

.field public static final enum e:Lb/d;

.field public static final enum f:Lb/d;

.field public static final enum g:Lb/d;

.field private static enum h:Lb/d;

.field private static enum i:Lb/d;

.field private static enum j:Lb/d;

.field private static enum k:Lb/d;

.field private static final synthetic m:[Lb/d;


# instance fields
.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lb/d;

    const-string v1, "message_pipe_granularity"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v4, v2}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->a:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "command_pipe_granularity"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v5, v2}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->b:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "inbound_poll_rate"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v6, v2}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->c:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "in_batch_size"

    const/16 v2, 0x2000

    invoke-direct {v0, v1, v7, v2}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->d:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "out_batch_size"

    const/16 v2, 0x2000

    invoke-direct {v0, v1, v8, v2}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->e:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "max_wm_delta"

    const/4 v2, 0x5

    const/16 v3, 0x400

    invoke-direct {v0, v1, v2, v3}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->f:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "max_io_events"

    const/4 v2, 0x6

    const/16 v3, 0x100

    invoke-direct {v0, v1, v2, v3}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->h:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "max_command_delay"

    const/4 v2, 0x7

    const v3, 0x2dc6c0

    invoke-direct {v0, v1, v2, v3}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->g:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "clock_precision"

    const/16 v2, 0x8

    const v3, 0xf4240

    invoke-direct {v0, v1, v2, v3}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->i:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "pgm_max_tpdu"

    const/16 v2, 0x9

    const/16 v3, 0x5dc

    invoke-direct {v0, v1, v2, v3}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->j:Lb/d;

    new-instance v0, Lb/d;

    const-string v1, "signaler_port"

    const/16 v2, 0xa

    const/16 v3, 0x1711

    invoke-direct {v0, v1, v2, v3}, Lb/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lb/d;->k:Lb/d;

    const/16 v0, 0xb

    new-array v0, v0, [Lb/d;

    sget-object v1, Lb/d;->a:Lb/d;

    aput-object v1, v0, v4

    sget-object v1, Lb/d;->b:Lb/d;

    aput-object v1, v0, v5

    sget-object v1, Lb/d;->c:Lb/d;

    aput-object v1, v0, v6

    sget-object v1, Lb/d;->d:Lb/d;

    aput-object v1, v0, v7

    sget-object v1, Lb/d;->e:Lb/d;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lb/d;->f:Lb/d;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lb/d;->h:Lb/d;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lb/d;->g:Lb/d;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lb/d;->i:Lb/d;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lb/d;->j:Lb/d;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lb/d;->k:Lb/d;

    aput-object v2, v0, v1

    sput-object v0, Lb/d;->m:[Lb/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lb/d;->l:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lb/d;
    .locals 1

    const-class v0, Lb/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lb/d;

    return-object v0
.end method

.method public static values()[Lb/d;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lb/d;->m:[Lb/d;

    array-length v1, v0

    new-array v2, v1, [Lb/d;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lb/d;->l:I

    return v0
.end method
