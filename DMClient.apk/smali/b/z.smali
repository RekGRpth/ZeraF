.class public Lb/z;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/z$a;
    }
.end annotation


# static fields
.field private static synthetic f:Z


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lb/E;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I

.field private e:[Lb/z;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/z;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/z;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lb/z;->b:I

    iput v0, p0, Lb/z;->c:I

    iput v0, p0, Lb/z;->d:I

    iput-object v1, p0, Lb/z;->a:Ljava/util/Set;

    iput-object v1, p0, Lb/z;->e:[Lb/z;

    return-void
.end method

.method private a()Z
    .locals 1

    iget-object v0, p0, Lb/z;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    iget v0, p0, Lb/z;->d:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lb/E;[BIILb/z$a;Ljava/lang/Object;)Z
    .locals 12

    iget-object v2, p0, Lb/z;->a:Ljava/util/Set;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lb/z;->a:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lb/z;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-interface {v0, v2, p2, p3, v1}, Lb/z$a;->a(Lb/E;[BILjava/lang/Object;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lb/z;->a:Ljava/util/Set;

    :cond_0
    move/from16 v0, p4

    if-lt p3, v0, :cond_17

    add-int/lit16 v6, p3, 0x100

    invoke-static {p2, v6}, Lb/X;->a([BI)[B

    move-result-object v4

    :goto_0
    iget v2, p0, Lb/z;->c:I

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_1
    iget v2, p0, Lb/z;->c:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    iget v2, p0, Lb/z;->b:I

    int-to-byte v2, v2

    aput-byte v2, v4, p3

    add-int/lit8 v5, p3, 0x1

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    move-object v3, p1

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v2 .. v8}, Lb/z;->a(Lb/E;[BIILb/z$a;Ljava/lang/Object;)Z

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-direct {v2}, Lb/z;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    iput-object v2, p0, Lb/z;->e:[Lb/z;

    const/4 v2, 0x0

    iput v2, p0, Lb/z;->c:I

    iget v2, p0, Lb/z;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lb/z;->d:I

    sget-boolean v2, Lb/z;->f:Z

    if-nez v2, :cond_2

    iget v2, p0, Lb/z;->d:I

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_2
    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    iget v2, p0, Lb/z;->b:I

    iget v3, p0, Lb/z;->c:I

    add-int/2addr v2, v3

    add-int/lit8 v5, v2, -0x1

    iget v3, p0, Lb/z;->b:I

    const/4 v2, 0x0

    move v9, v2

    move v10, v3

    move v11, v5

    :goto_2
    iget v2, p0, Lb/z;->c:I

    if-ne v9, v2, :cond_4

    sget-boolean v2, Lb/z;->f:Z

    if-nez v2, :cond_8

    iget v2, p0, Lb/z;->c:I

    const/4 v3, 0x1

    if-gt v2, v3, :cond_8

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_4
    iget v2, p0, Lb/z;->b:I

    add-int/2addr v2, v9

    int-to-byte v2, v2

    aput-byte v2, v4, p3

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    aget-object v2, v2, v9

    if-eqz v2, :cond_16

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    aget-object v2, v2, v9

    add-int/lit8 v5, p3, 0x1

    move-object v3, p1

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v2 .. v8}, Lb/z;->a(Lb/E;[BIILb/z$a;Ljava/lang/Object;)Z

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    aget-object v2, v2, v9

    invoke-direct {v2}, Lb/z;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    const/4 v3, 0x0

    aput-object v3, v2, v9

    sget-boolean v2, Lb/z;->f:Z

    if-nez v2, :cond_5

    iget v2, p0, Lb/z;->d:I

    if-gtz v2, :cond_5

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_5
    iget v2, p0, Lb/z;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lb/z;->d:I

    move v3, v10

    move v5, v11

    :goto_3
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v10, v3

    move v11, v5

    goto :goto_2

    :cond_6
    iget v2, p0, Lb/z;->b:I

    add-int/2addr v2, v9

    if-ge v2, v11, :cond_7

    iget v2, p0, Lb/z;->b:I

    add-int v11, v9, v2

    :cond_7
    iget v2, p0, Lb/z;->b:I

    add-int/2addr v2, v9

    if-le v2, v10, :cond_16

    iget v2, p0, Lb/z;->b:I

    add-int v10, v9, v2

    move v3, v10

    move v5, v11

    goto :goto_3

    :cond_8
    iget v2, p0, Lb/z;->d:I

    if-nez v2, :cond_a

    const/4 v2, 0x0

    iput-object v2, p0, Lb/z;->e:[Lb/z;

    const/4 v2, 0x0

    iput v2, p0, Lb/z;->c:I

    :cond_9
    :goto_4
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_a
    iget v2, p0, Lb/z;->d:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_f

    sget-boolean v2, Lb/z;->f:Z

    if-nez v2, :cond_b

    if-eq v11, v10, :cond_b

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_b
    sget-boolean v2, Lb/z;->f:Z

    if-nez v2, :cond_d

    iget v2, p0, Lb/z;->b:I

    if-lt v11, v2, :cond_c

    iget v2, p0, Lb/z;->b:I

    iget v3, p0, Lb/z;->c:I

    add-int/2addr v2, v3

    if-lt v11, v2, :cond_d

    :cond_c
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_d
    iget-object v2, p0, Lb/z;->e:[Lb/z;

    iget v3, p0, Lb/z;->b:I

    sub-int v3, v11, v3

    aget-object v2, v2, v3

    sget-boolean v3, Lb/z;->f:Z

    if-nez v3, :cond_e

    if-nez v2, :cond_e

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_e
    const/4 v3, 0x0

    iput-object v3, p0, Lb/z;->e:[Lb/z;

    const/4 v3, 0x1

    new-array v3, v3, [Lb/z;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    iput-object v3, p0, Lb/z;->e:[Lb/z;

    const/4 v2, 0x1

    iput v2, p0, Lb/z;->c:I

    iput v11, p0, Lb/z;->b:I

    goto :goto_4

    :cond_f
    iget v2, p0, Lb/z;->d:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_9

    iget v2, p0, Lb/z;->b:I

    if-gt v11, v2, :cond_10

    iget v2, p0, Lb/z;->b:I

    iget v3, p0, Lb/z;->c:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    if-ge v10, v2, :cond_9

    :cond_10
    sget-boolean v2, Lb/z;->f:Z

    if-nez v2, :cond_11

    sub-int v2, v10, v11

    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-gt v2, v3, :cond_11

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_11
    iget-object v2, p0, Lb/z;->e:[Lb/z;

    sget-boolean v3, Lb/z;->f:Z

    if-nez v3, :cond_12

    iget v3, p0, Lb/z;->b:I

    if-gt v11, v3, :cond_12

    iget v3, p0, Lb/z;->b:I

    iget v4, p0, Lb/z;->c:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    if-lt v10, v3, :cond_12

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_12
    sget-boolean v3, Lb/z;->f:Z

    if-nez v3, :cond_13

    iget v3, p0, Lb/z;->b:I

    if-ge v11, v3, :cond_13

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_13
    sget-boolean v3, Lb/z;->f:Z

    if-nez v3, :cond_14

    iget v3, p0, Lb/z;->b:I

    iget v4, p0, Lb/z;->c:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    if-le v10, v3, :cond_14

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_14
    sget-boolean v3, Lb/z;->f:Z

    if-nez v3, :cond_15

    sub-int v3, v10, v11

    add-int/lit8 v3, v3, 0x1

    iget v4, p0, Lb/z;->c:I

    if-lt v3, v4, :cond_15

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_15
    sub-int v3, v10, v11

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lb/z;->c:I

    iget v3, p0, Lb/z;->c:I

    new-array v3, v3, [Lb/z;

    iput-object v3, p0, Lb/z;->e:[Lb/z;

    iget v3, p0, Lb/z;->b:I

    sub-int v3, v11, v3

    iget-object v4, p0, Lb/z;->e:[Lb/z;

    const/4 v5, 0x0

    iget v6, p0, Lb/z;->c:I

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v11, p0, Lb/z;->b:I

    goto/16 :goto_4

    :cond_16
    move v3, v10

    move v5, v11

    goto/16 :goto_3

    :cond_17
    move/from16 v6, p4

    move-object v4, p2

    goto/16 :goto_0
.end method

.method private static a([Lb/z;IZ)[Lb/z;
    .locals 1

    const-class v0, Lb/z;

    invoke-static {v0, p0, p1, p2}, Lb/X;->a(Ljava/lang/Class;[Ljava/lang/Object;IZ)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lb/z;

    return-object v0
.end method

.method private c([BILb/E;)Z
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_0
    if-eqz p1, :cond_0

    array-length v2, p1

    if-ne v2, p2, :cond_3

    :cond_0
    iget-object v2, p0, Lb/z;->a:Ljava/util/Set;

    if-nez v2, :cond_2

    :goto_1
    iget-object v1, p0, Lb/z;->a:Ljava/util/Set;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lb/z;->a:Ljava/util/Set;

    :cond_1
    iget-object v1, p0, Lb/z;->a:Ljava/util/Set;

    invoke-interface {v1, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return v0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    aget-byte v3, p1, p2

    iget v2, p0, Lb/z;->b:I

    if-lt v3, v2, :cond_4

    iget v2, p0, Lb/z;->b:I

    iget v4, p0, Lb/z;->c:I

    add-int/2addr v2, v4

    if-lt v3, v2, :cond_5

    :cond_4
    iget v2, p0, Lb/z;->c:I

    if-nez v2, :cond_7

    iput v3, p0, Lb/z;->b:I

    iput v0, p0, Lb/z;->c:I

    const/4 v2, 0x0

    iput-object v2, p0, Lb/z;->e:[Lb/z;

    :cond_5
    :goto_2
    iget v2, p0, Lb/z;->c:I

    if-ne v2, v0, :cond_b

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    if-nez v2, :cond_6

    new-array v2, v0, [Lb/z;

    iput-object v2, p0, Lb/z;->e:[Lb/z;

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    new-instance v3, Lb/z;

    invoke-direct {v3}, Lb/z;-><init>()V

    aput-object v3, v2, v1

    iget v2, p0, Lb/z;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lb/z;->d:I

    :cond_6
    iget-object v2, p0, Lb/z;->e:[Lb/z;

    aget-object p0, v2, v1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_7
    iget v2, p0, Lb/z;->c:I

    if-ne v2, v0, :cond_9

    iget v4, p0, Lb/z;->b:I

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    aget-object v5, v2, v1

    iget v2, p0, Lb/z;->b:I

    if-ge v2, v3, :cond_8

    iget v2, p0, Lb/z;->b:I

    sub-int v2, v3, v2

    :goto_3
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lb/z;->c:I

    iget v2, p0, Lb/z;->c:I

    new-array v2, v2, [Lb/z;

    iput-object v2, p0, Lb/z;->e:[Lb/z;

    iget v2, p0, Lb/z;->b:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lb/z;->b:I

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    iget v6, p0, Lb/z;->b:I

    sub-int/2addr v4, v6

    aput-object v5, v2, v4

    goto :goto_2

    :cond_8
    iget v2, p0, Lb/z;->b:I

    sub-int/2addr v2, v3

    goto :goto_3

    :cond_9
    iget v2, p0, Lb/z;->b:I

    if-ge v2, v3, :cond_a

    iget v2, p0, Lb/z;->b:I

    sub-int v2, v3, v2

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lb/z;->c:I

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    iget v4, p0, Lb/z;->c:I

    invoke-static {v2, v4, v0}, Lb/z;->a([Lb/z;IZ)[Lb/z;

    move-result-object v2

    iput-object v2, p0, Lb/z;->e:[Lb/z;

    goto :goto_2

    :cond_a
    iget v2, p0, Lb/z;->b:I

    iget v4, p0, Lb/z;->c:I

    add-int/2addr v2, v4

    sub-int/2addr v2, v3

    iput v2, p0, Lb/z;->c:I

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    iget v4, p0, Lb/z;->c:I

    invoke-static {v2, v4, v1}, Lb/z;->a([Lb/z;IZ)[Lb/z;

    move-result-object v2

    iput-object v2, p0, Lb/z;->e:[Lb/z;

    iput v3, p0, Lb/z;->b:I

    goto :goto_2

    :cond_b
    iget-object v2, p0, Lb/z;->e:[Lb/z;

    iget v4, p0, Lb/z;->b:I

    sub-int v4, v3, v4

    aget-object v2, v2, v4

    if-nez v2, :cond_c

    iget-object v2, p0, Lb/z;->e:[Lb/z;

    iget v4, p0, Lb/z;->b:I

    sub-int v4, v3, v4

    new-instance v5, Lb/z;

    invoke-direct {v5}, Lb/z;-><init>()V

    aput-object v5, v2, v4

    iget v2, p0, Lb/z;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lb/z;->d:I

    :cond_c
    iget-object v2, p0, Lb/z;->e:[Lb/z;

    iget v4, p0, Lb/z;->b:I

    sub-int/2addr v3, v4

    aget-object p0, v2, v3

    add-int/lit8 p2, p2, 0x1

    goto/16 :goto_0
.end method

.method private d([BILb/E;)Z
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_0

    array-length v0, p1

    if-ne v0, p2, :cond_4

    :cond_0
    iget-object v0, p0, Lb/z;->a:Ljava/util/Set;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/z;->a:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    sget-boolean v3, Lb/z;->f:Z

    if-nez v3, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lb/z;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v6, p0, Lb/z;->a:Ljava/util/Set;

    :cond_2
    iget-object v0, p0, Lb/z;->a:Ljava/util/Set;

    if-nez v0, :cond_3

    move v1, v2

    :cond_3
    :goto_0
    return v1

    :cond_4
    aget-byte v4, p1, p2

    iget v0, p0, Lb/z;->c:I

    if-eqz v0, :cond_3

    iget v0, p0, Lb/z;->b:I

    if-lt v4, v0, :cond_3

    iget v0, p0, Lb/z;->b:I

    iget v3, p0, Lb/z;->c:I

    add-int/2addr v0, v3

    if-ge v4, v0, :cond_3

    iget v0, p0, Lb/z;->c:I

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lb/z;->e:[Lb/z;

    aget-object v0, v0, v1

    :goto_1
    if-eqz v0, :cond_3

    add-int/lit8 v3, p2, 0x1

    invoke-direct {v0, p1, v3, p3}, Lb/z;->d([BILb/E;)Z

    move-result v3

    invoke-direct {v0}, Lb/z;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    sget-boolean v0, Lb/z;->f:Z

    if-nez v0, :cond_6

    iget v0, p0, Lb/z;->c:I

    if-gtz v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    iget-object v0, p0, Lb/z;->e:[Lb/z;

    iget v3, p0, Lb/z;->b:I

    sub-int v3, v4, v3

    aget-object v0, v0, v3

    goto :goto_1

    :cond_6
    iget v0, p0, Lb/z;->c:I

    if-ne v0, v2, :cond_7

    iput-object v6, p0, Lb/z;->e:[Lb/z;

    iput v1, p0, Lb/z;->c:I

    iget v0, p0, Lb/z;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/z;->d:I

    sget-boolean v0, Lb/z;->f:Z

    if-nez v0, :cond_c

    iget v0, p0, Lb/z;->d:I

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_7
    iget-object v0, p0, Lb/z;->e:[Lb/z;

    iget v5, p0, Lb/z;->b:I

    sub-int v5, v4, v5

    aput-object v6, v0, v5

    sget-boolean v0, Lb/z;->f:Z

    if-nez v0, :cond_8

    iget v0, p0, Lb/z;->d:I

    if-gt v0, v2, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_8
    iget v0, p0, Lb/z;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/z;->d:I

    iget v0, p0, Lb/z;->d:I

    if-ne v0, v2, :cond_d

    move v0, v1

    :goto_2
    iget v4, p0, Lb/z;->c:I

    if-lt v0, v4, :cond_a

    :cond_9
    sget-boolean v4, Lb/z;->f:Z

    if-nez v4, :cond_b

    iget v4, p0, Lb/z;->c:I

    if-lt v0, v4, :cond_b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_a
    iget-object v4, p0, Lb/z;->e:[Lb/z;

    aget-object v4, v4, v0

    if-nez v4, :cond_9

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_b
    iget v4, p0, Lb/z;->b:I

    add-int/2addr v4, v0

    iput v4, p0, Lb/z;->b:I

    iput v2, p0, Lb/z;->c:I

    iget-object v4, p0, Lb/z;->e:[Lb/z;

    aget-object v0, v4, v0

    new-array v2, v2, [Lb/z;

    aput-object v0, v2, v1

    iput-object v2, p0, Lb/z;->e:[Lb/z;

    :cond_c
    :goto_3
    move v1, v3

    goto/16 :goto_0

    :cond_d
    iget v0, p0, Lb/z;->b:I

    if-ne v4, v0, :cond_11

    move v0, v2

    :goto_4
    iget v1, p0, Lb/z;->c:I

    if-lt v0, v1, :cond_f

    :cond_e
    sget-boolean v1, Lb/z;->f:Z

    if-nez v1, :cond_10

    iget v1, p0, Lb/z;->c:I

    if-lt v0, v1, :cond_10

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_f
    iget-object v1, p0, Lb/z;->e:[Lb/z;

    aget-object v1, v1, v0

    if-nez v1, :cond_e

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_10
    iget v1, p0, Lb/z;->b:I

    add-int/2addr v1, v0

    iput v1, p0, Lb/z;->b:I

    iget v1, p0, Lb/z;->c:I

    sub-int v0, v1, v0

    iput v0, p0, Lb/z;->c:I

    iget-object v0, p0, Lb/z;->e:[Lb/z;

    iget v1, p0, Lb/z;->c:I

    invoke-static {v0, v1, v2}, Lb/z;->a([Lb/z;IZ)[Lb/z;

    move-result-object v0

    iput-object v0, p0, Lb/z;->e:[Lb/z;

    goto :goto_3

    :cond_11
    iget v0, p0, Lb/z;->b:I

    iget v5, p0, Lb/z;->c:I

    add-int/2addr v0, v5

    add-int/lit8 v0, v0, -0x1

    if-ne v4, v0, :cond_c

    :goto_5
    iget v0, p0, Lb/z;->c:I

    if-lt v2, v0, :cond_13

    :cond_12
    sget-boolean v0, Lb/z;->f:Z

    if-nez v0, :cond_14

    iget v0, p0, Lb/z;->c:I

    if-lt v2, v0, :cond_14

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_13
    iget-object v0, p0, Lb/z;->e:[Lb/z;

    iget v4, p0, Lb/z;->c:I

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v2

    aget-object v0, v0, v4

    if-nez v0, :cond_12

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_14
    iget v0, p0, Lb/z;->c:I

    sub-int/2addr v0, v2

    iput v0, p0, Lb/z;->c:I

    iget-object v0, p0, Lb/z;->e:[Lb/z;

    iget v2, p0, Lb/z;->c:I

    invoke-static {v0, v2, v1}, Lb/z;->a([Lb/z;IZ)[Lb/z;

    move-result-object v0

    iput-object v0, p0, Lb/z;->e:[Lb/z;

    goto :goto_3
.end method


# virtual methods
.method public final a([BILb/z$a;Ljava/lang/Object;)V
    .locals 5

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lb/z;->a:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/z;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz p2, :cond_3

    iget v0, p0, Lb/z;->c:I

    if-eqz v0, :cond_3

    aget-byte v0, p1, v1

    iget v3, p0, Lb/z;->c:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    iget v3, p0, Lb/z;->b:I

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lb/z;->e:[Lb/z;

    aget-object p0, v0, v2

    add-int/lit8 v0, v1, 0x1

    add-int/lit8 p2, p2, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/E;

    const/4 v4, 0x0

    invoke-interface {p3, v0, v4, v2, p4}, Lb/z$a;->a(Lb/E;[BILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget v3, p0, Lb/z;->b:I

    if-lt v0, v3, :cond_3

    iget v3, p0, Lb/z;->b:I

    iget v4, p0, Lb/z;->c:I

    add-int/2addr v3, v4

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lb/z;->e:[Lb/z;

    iget v4, p0, Lb/z;->b:I

    sub-int v4, v0, v4

    aget-object v3, v3, v4

    if-eqz v3, :cond_3

    iget-object v3, p0, Lb/z;->e:[Lb/z;

    iget v4, p0, Lb/z;->b:I

    sub-int/2addr v0, v4

    aget-object p0, v3, v0

    add-int/lit8 v0, v1, 0x1

    add-int/lit8 p2, p2, -0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final a(Lb/E;Lb/z$a;Ljava/lang/Object;)Z
    .locals 7

    const/4 v3, 0x0

    new-array v2, v3, [B

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lb/z;->a(Lb/E;[BIILb/z$a;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a([BILb/E;)Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p3}, Lb/z;->c([BILb/E;)Z

    move-result v0

    return v0
.end method

.method public final a([BLb/E;)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p2}, Lb/z;->c([BILb/E;)Z

    move-result v0

    return v0
.end method

.method public final b([BILb/E;)Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p3}, Lb/z;->d([BILb/E;)Z

    move-result v0

    return v0
.end method
