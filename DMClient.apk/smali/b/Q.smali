.class public abstract Lb/Q;
.super Lb/C;
.source "SourceFile"

# interfaces
.implements Lb/E$a;
.implements Lb/t;


# static fields
.field private static synthetic n:Z


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lb/C;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field private final e:Lb/x;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lb/E;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lb/F;

.field private h:Ljava/nio/channels/SelectableChannel;

.field private i:J

.field private j:I

.field private k:Z

.field private l:Lb/Q;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/Q;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/Q;->n:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lb/e;II)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lb/C;-><init>(Lb/e;I)V

    iput-boolean v2, p0, Lb/Q;->c:Z

    iput-boolean v2, p0, Lb/Q;->d:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lb/Q;->i:J

    iput v2, p0, Lb/Q;->j:I

    iput-boolean v2, p0, Lb/Q;->k:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lb/Q;->l:Lb/Q;

    iput v2, p0, Lb/Q;->m:I

    iget-object v0, p0, Lb/Q;->a:Lb/B;

    iput p3, v0, Lb/B;->D:I

    new-instance v0, Lb/A;

    invoke-direct {v0}, Lb/A;-><init>()V

    iput-object v0, p0, Lb/Q;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/Q;->f:Ljava/util/List;

    new-instance v0, Lb/x;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "socket-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lb/x;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lb/Q;->e:Lb/x;

    return-void
.end method

.method public static a(ILb/e;II)Lb/Q;
    .locals 3

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lb/D;

    invoke-direct {v0, p1, p2, p3}, Lb/D;-><init>(Lb/e;II)V

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lb/H;

    invoke-direct {v0, p1, p2, p3}, Lb/H;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lb/S;

    invoke-direct {v0, p1, p2, p3}, Lb/S;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lb/M;

    invoke-direct {v0, p1, p2, p3}, Lb/M;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lb/L;

    invoke-direct {v0, p1, p2, p3}, Lb/L;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Lb/f;

    invoke-direct {v0, p1, p2, p3}, Lb/f;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_6
    new-instance v0, Lb/N;

    invoke-direct {v0, p1, p2, p3}, Lb/N;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_7
    new-instance v0, Lb/I;

    invoke-direct {v0, p1, p2, p3}, Lb/I;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_8
    new-instance v0, Lb/J;

    invoke-direct {v0, p1, p2, p3}, Lb/J;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_9
    new-instance v0, Lb/aa;

    invoke-direct {v0, p1, p2, p3}, Lb/aa;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_a
    new-instance v0, Lb/ab;

    invoke-direct {v0, p1, p2, p3}, Lb/ab;-><init>(Lb/e;II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private a()V
    .locals 2

    iget-boolean v0, p0, Lb/Q;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/Q;->g:Lb/F;

    iget-object v1, p0, Lb/Q;->h:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;)V

    invoke-virtual {p0, p0}, Lb/Q;->c(Lb/Q;)V

    invoke-virtual {p0}, Lb/Q;->w()V

    invoke-super {p0}, Lb/C;->a_()V

    :cond_0
    return-void
.end method

.method private a(Lb/y;)V
    .locals 1

    invoke-virtual {p1}, Lb/y;->c()I

    move-result v0

    and-int/lit8 v0, v0, 0x40

    if-lez v0, :cond_0

    sget-boolean v0, Lb/Q;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/Q;->a:Lb/B;

    iget-boolean v0, v0, Lb/B;->y:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v0

    iput-boolean v0, p0, Lb/Q;->k:Z

    return-void
.end method

.method private a(IZ)Z
    .locals 6

    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    if-eqz p1, :cond_1

    iget-object v1, p0, Lb/Q;->e:Lb/x;

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Lb/x;->a(J)Lb/c;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lb/c;->a()Lb/ag;

    move-result-object v2

    invoke-virtual {v2, v1}, Lb/ag;->a(Lb/c;)V

    :cond_0
    :goto_1
    iget-object v1, p0, Lb/Q;->e:Lb/x;

    invoke-virtual {v1, v4, v5}, Lb/x;->a(J)Lb/c;

    move-result-object v1

    goto :goto_0

    :cond_1
    cmp-long v1, v4, v4

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    cmp-long v1, v4, v4

    if-ltz v1, :cond_3

    sget-object v1, Lb/d;->g:Lb/d;

    invoke-virtual {v1}, Lb/d;->a()I

    move-result v1

    int-to-long v1, v1

    cmp-long v1, v4, v1

    if-gtz v1, :cond_3

    :cond_2
    :goto_2
    return v0

    :cond_3
    iput-wide v4, p0, Lb/Q;->i:J

    goto :goto_1

    :cond_4
    iget-boolean v1, p0, Lb/Q;->c:Z

    if-eqz v1, :cond_2

    const v0, 0x9523dfd

    invoke-static {v0}, Lb/ae;->a(I)V

    const/4 v0, 0x0

    goto :goto_2
.end method

.method private b(Lb/E;Z)V
    .locals 1

    invoke-virtual {p1, p0}, Lb/E;->a(Lb/E$a;)V

    iget-object v0, p0, Lb/Q;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1, p2}, Lb/Q;->a(Lb/E;Z)V

    invoke-virtual {p0}, Lb/Q;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lb/Q;->c(I)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lb/E;->a(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected a(Lb/E;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Must Override"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract a(Lb/E;Z)V
.end method

.method public final a(Lb/F;)V
    .locals 2

    iput-object p1, p0, Lb/Q;->g:Lb/F;

    iget-object v0, p0, Lb/Q;->e:Lb/x;

    invoke-virtual {v0}, Lb/x;->a()Ljava/nio/channels/SelectableChannel;

    move-result-object v0

    iput-object v0, p0, Lb/Q;->h:Ljava/nio/channels/SelectableChannel;

    iget-object v0, p0, Lb/Q;->g:Lb/F;

    iget-object v1, p0, Lb/Q;->h:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1, p0}, Lb/F;->a(Ljava/nio/channels/SelectableChannel;Lb/t;)V

    iget-object v0, p0, Lb/Q;->g:Lb/F;

    iget-object v1, p0, Lb/Q;->h:Ljava/nio/channels/SelectableChannel;

    invoke-virtual {v0, v1}, Lb/F;->b(Ljava/nio/channels/SelectableChannel;)V

    invoke-virtual {p0}, Lb/Q;->b_()V

    invoke-direct {p0}, Lb/Q;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/nio/channels/SelectableChannel;)V
    .locals 0

    return-void
.end method

.method protected a(ILjava/lang/Object;)Z
    .locals 1

    const/16 v0, 0x16

    invoke-static {v0}, Lb/ae;->a(I)V

    const/4 v0, 0x0

    return v0
.end method

.method protected a(Lb/y;I)Z
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Must Override"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 10

    const/16 v8, 0x40

    const/16 v6, 0x2b

    const/4 v9, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lb/Q;->c:Z

    if-eqz v0, :cond_1

    const v0, 0x9523dfd

    invoke-static {v0}, Lb/ae;->a(I)V

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-direct {p0, v2, v2}, Lb/Q;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/net/URI;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    if-nez v1, :cond_15

    :goto_1
    const-string v1, "inproc"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ipc"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "tcp"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v6}, Lb/ae;->a(I)V

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    const-string v1, "pgm"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "epgm"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget v1, v1, Lb/B;->l:I

    if-eq v1, v3, :cond_4

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget v1, v1, Lb/B;->l:I

    if-eq v1, v9, :cond_4

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget v1, v1, Lb/B;->l:I

    const/16 v5, 0x9

    if-eq v1, v5, :cond_4

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget v1, v1, Lb/B;->l:I

    const/16 v5, 0xa

    if-eq v1, v5, :cond_4

    invoke-static {v6}, Lb/ae;->a(I)V

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ",type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/Q;->a:Lb/B;

    iget v2, v2, Lb/B;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const-string v1, "inproc"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {p0, p1}, Lb/Q;->b(Ljava/lang/String;)Lb/e$a;

    move-result-object v4

    iget-object v0, v4, Lb/e$a;->a:Lb/Q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/Q;->a:Lb/B;

    iget v0, v0, Lb/B;->a:I

    if-eqz v0, :cond_14

    iget-object v0, v4, Lb/e$a;->b:Lb/B;

    iget v0, v0, Lb/B;->b:I

    if-eqz v0, :cond_14

    iget-object v0, p0, Lb/Q;->a:Lb/B;

    iget v0, v0, Lb/B;->a:I

    iget-object v1, v4, Lb/e$a;->b:Lb/B;

    iget v1, v1, Lb/B;->b:I

    add-int/2addr v0, v1

    :goto_2
    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget v1, v1, Lb/B;->b:I

    if-eqz v1, :cond_13

    iget-object v1, v4, Lb/e$a;->b:Lb/B;

    iget v1, v1, Lb/B;->a:I

    if-eqz v1, :cond_13

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget v1, v1, Lb/B;->b:I

    iget-object v5, v4, Lb/e$a;->b:Lb/B;

    iget v5, v5, Lb/B;->a:I

    add-int/2addr v1, v5

    :goto_3
    new-array v5, v9, [Lb/ag;

    aput-object p0, v5, v2

    iget-object v6, v4, Lb/e$a;->a:Lb/Q;

    aput-object v6, v5, v3

    new-array v6, v9, [Lb/E;

    new-array v7, v9, [I

    aput v0, v7, v2

    aput v1, v7, v3

    new-array v0, v9, [Z

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget-boolean v1, v1, Lb/B;->w:Z

    aput-boolean v1, v0, v2

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget-boolean v1, v1, Lb/B;->v:Z

    aput-boolean v1, v0, v3

    invoke-static {v5, v6, v7, v0}, Lb/E;->a([Lb/ag;[Lb/E;[I[Z)V

    aget-object v0, v6, v2

    invoke-direct {p0, v0, v2}, Lb/Q;->b(Lb/E;Z)V

    iget-object v0, v4, Lb/e$a;->b:Lb/B;

    iget-boolean v0, v0, Lb/B;->y:Z

    if-eqz v0, :cond_6

    new-instance v0, Lb/y;

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget-byte v1, v1, Lb/B;->d:B

    invoke-direct {v0, v1}, Lb/y;-><init>(I)V

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget-object v1, v1, Lb/B;->e:[B

    iget-object v5, p0, Lb/Q;->a:Lb/B;

    iget-byte v5, v5, Lb/B;->d:B

    invoke-virtual {v0, v1, v2, v5}, Lb/y;->a([BII)V

    invoke-virtual {v0, v8}, Lb/y;->a(I)V

    aget-object v1, v6, v2

    invoke-virtual {v1, v0}, Lb/E;->a(Lb/y;)Z

    move-result v0

    sget-boolean v1, Lb/Q;->n:Z

    if-nez v1, :cond_5

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    aget-object v0, v6, v2

    invoke-virtual {v0}, Lb/E;->f()V

    :cond_6
    iget-object v0, p0, Lb/Q;->a:Lb/B;

    iget-boolean v0, v0, Lb/B;->y:Z

    if-eqz v0, :cond_8

    new-instance v0, Lb/y;

    iget-object v1, v4, Lb/e$a;->b:Lb/B;

    iget-byte v1, v1, Lb/B;->d:B

    invoke-direct {v0, v1}, Lb/y;-><init>(I)V

    iget-object v1, v4, Lb/e$a;->b:Lb/B;

    iget-object v1, v1, Lb/B;->e:[B

    iget-object v5, v4, Lb/e$a;->b:Lb/B;

    iget-byte v5, v5, Lb/B;->d:B

    invoke-virtual {v0, v1, v2, v5}, Lb/y;->a([BII)V

    invoke-virtual {v0, v8}, Lb/y;->a(I)V

    aget-object v1, v6, v3

    invoke-virtual {v1, v0}, Lb/E;->a(Lb/y;)Z

    move-result v0

    sget-boolean v1, Lb/Q;->n:Z

    if-nez v1, :cond_7

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_7
    aget-object v0, v6, v3

    invoke-virtual {v0}, Lb/E;->f()V

    :cond_8
    iget-object v0, v4, Lb/e$a;->a:Lb/Q;

    aget-object v1, v6, v3

    invoke-virtual {p0, v0, v1, v2}, Lb/Q;->a(Lb/C;Lb/E;Z)V

    iget-object v0, p0, Lb/Q;->a:Lb/B;

    iput-object p1, v0, Lb/B;->f:Ljava/lang/String;

    move v2, v3

    goto/16 :goto_0

    :cond_9
    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget-wide v5, v1, Lb/B;->c:J

    invoke-virtual {p0, v5, v6}, Lb/Q;->b(J)Lb/s;

    move-result-object v5

    if-nez v5, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Empty IO Thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    new-instance v6, Lb/a;

    invoke-direct {v6, v4, v0}, Lb/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "tcp"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v1, Lb/T;

    invoke-direct {v1}, Lb/T;-><init>()V

    invoke-virtual {v6, v1}, Lb/a;->a(Lb/a$a;)Lb/a$a;

    invoke-virtual {v6}, Lb/a;->b()Lb/a$a;

    move-result-object v7

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget v1, v1, Lb/B;->t:I

    if-eqz v1, :cond_c

    move v1, v3

    :goto_4
    invoke-interface {v7, v0, v1}, Lb/a$a;->a(Ljava/lang/String;Z)V

    :cond_b
    :goto_5
    iget-object v0, p0, Lb/Q;->a:Lb/B;

    invoke-static {v5, v3, p0, v0, v6}, Lb/O;->a(Lb/s;ZLb/Q;Lb/B;Lb/a;)Lb/O;

    move-result-object v1

    sget-boolean v0, Lb/Q;->n:Z

    if-nez v0, :cond_e

    if-nez v1, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_c
    move v1, v2

    goto :goto_4

    :cond_d
    const-string v1, "ipc"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Lb/u;

    invoke-direct {v1}, Lb/u;-><init>()V

    invoke-virtual {v6, v1}, Lb/a;->a(Lb/a$a;)Lb/a$a;

    invoke-virtual {v6}, Lb/a;->b()Lb/a$a;

    move-result-object v1

    invoke-interface {v1, v0, v3}, Lb/a$a;->a(Ljava/lang/String;Z)V

    goto :goto_5

    :cond_e
    const-string v0, "pgm"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "epgm"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_f
    move v0, v3

    :goto_6
    iget-object v4, p0, Lb/Q;->a:Lb/B;

    iget v4, v4, Lb/B;->u:I

    if-ne v4, v3, :cond_10

    if-eqz v0, :cond_11

    :cond_10
    new-array v4, v9, [Lb/ag;

    aput-object p0, v4, v2

    aput-object v1, v4, v3

    new-array v5, v9, [Lb/E;

    new-array v7, v9, [I

    iget-object v8, p0, Lb/Q;->a:Lb/B;

    iget v8, v8, Lb/B;->a:I

    aput v8, v7, v2

    iget-object v8, p0, Lb/Q;->a:Lb/B;

    iget v8, v8, Lb/B;->b:I

    aput v8, v7, v3

    new-array v8, v9, [Z

    iget-object v9, p0, Lb/Q;->a:Lb/B;

    iget-boolean v9, v9, Lb/B;->w:Z

    aput-boolean v9, v8, v2

    iget-object v9, p0, Lb/Q;->a:Lb/B;

    iget-boolean v9, v9, Lb/B;->v:Z

    aput-boolean v9, v8, v3

    invoke-static {v4, v5, v7, v8}, Lb/E;->a([Lb/ag;[Lb/E;[I[Z)V

    aget-object v2, v5, v2

    invoke-direct {p0, v2, v0}, Lb/Q;->b(Lb/E;Z)V

    aget-object v0, v5, v3

    invoke-virtual {v1, v0}, Lb/O;->a(Lb/E;)V

    :cond_11
    iget-object v0, p0, Lb/Q;->a:Lb/B;

    invoke-virtual {v6}, Lb/a;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lb/B;->f:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lb/Q;->a(Lb/C;)V

    iget-object v0, p0, Lb/Q;->b:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    goto/16 :goto_0

    :cond_12
    move v0, v2

    goto :goto_6

    :cond_13
    move v1, v2

    goto/16 :goto_3

    :cond_14
    move v0, v2

    goto/16 :goto_2

    :cond_15
    move-object v0, v1

    goto/16 :goto_1
.end method

.method protected a_(I)Lb/y;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Must Override"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a_()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/Q;->d:Z

    return-void
.end method

.method protected final b(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, p0}, Lb/Q;->b(Lb/Q;)V

    move v1, v2

    :goto_0
    iget-object v0, p0, Lb/Q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lb/Q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lb/Q;->c(I)V

    invoke-super {p0, p1}, Lb/C;->b(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lb/Q;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/E;

    invoke-virtual {v0, v2}, Lb/E;->a(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected b(Lb/E;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Must Override"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/nio/channels/SelectableChannel;)V
    .locals 0

    return-void
.end method

.method public final b(ILjava/lang/Object;)Z
    .locals 3

    const/4 v2, 0x6

    const/4 v0, 0x0

    iget-boolean v1, p0, Lb/Q;->c:Z

    if-eqz v1, :cond_1

    const v1, 0x9523dfd

    invoke-static {v1}, Lb/ae;->a(I)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v2, p2}, Lb/Q;->a(ILjava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x16

    invoke-static {v1}, Lb/ae;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lb/ae;->b()V

    iget-object v0, p0, Lb/Q;->a:Lb/B;

    invoke-virtual {v0, v2, p2}, Lb/B;->a(ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method protected abstract c(Lb/E;)V
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/nio/channels/SelectableChannel;)V
    .locals 0

    return-void
.end method

.method public final d(I)I
    .locals 1

    iget-boolean v0, p0, Lb/Q;->c:Z

    if-eqz v0, :cond_0

    const v0, 0x9523dfd

    invoke-static {v0}, Lb/ae;->a(I)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lb/Q;->k:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d(Lb/E;)V
    .locals 0

    invoke-virtual {p0, p1}, Lb/Q;->a(Lb/E;)V

    return-void
.end method

.method public final e(I)Lb/y;
    .locals 10

    const/4 v3, 0x1

    const/16 v9, 0x23

    const/4 v6, 0x0

    const/4 v4, 0x0

    iget-boolean v0, p0, Lb/Q;->c:Z

    if-eqz v0, :cond_0

    const v0, 0x9523dfd

    invoke-static {v0}, Lb/ae;->a(I)V

    move-object v0, v6

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lb/Q;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/Q;->j:I

    sget-object v1, Lb/d;->c:Lb/d;

    invoke-virtual {v1}, Lb/d;->a()I

    move-result v1

    if-ne v0, v1, :cond_2

    invoke-direct {p0, v4, v4}, Lb/Q;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v6

    goto :goto_0

    :cond_1
    iput v4, p0, Lb/Q;->j:I

    :cond_2
    invoke-virtual {p0, p1}, Lb/Q;->a_(I)Lb/y;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {v9}, Lb/ae;->b(I)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v0, v6

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    invoke-direct {p0, v0}, Lb/Q;->a(Lb/y;)V

    goto :goto_0

    :cond_4
    and-int/lit8 v0, p1, 0x1

    if-gtz v0, :cond_5

    iget-object v0, p0, Lb/Q;->a:Lb/B;

    iget v0, v0, Lb/B;->r:I

    if-nez v0, :cond_8

    :cond_5
    invoke-direct {p0, v4, v4}, Lb/Q;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v6

    goto :goto_0

    :cond_6
    iput v4, p0, Lb/Q;->j:I

    invoke-virtual {p0, p1}, Lb/Q;->a_(I)Lb/y;

    move-result-object v0

    if-nez v0, :cond_7

    move-object v0, v6

    goto :goto_0

    :cond_7
    invoke-direct {p0, v0}, Lb/Q;->a(Lb/y;)V

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lb/Q;->a:Lb/B;

    iget v5, v0, Lb/B;->r:I

    if-gez v5, :cond_9

    const-wide/16 v0, 0x0

    :goto_1
    iget v2, p0, Lb/Q;->j:I

    if-eqz v2, :cond_a

    move v2, v3

    :goto_2
    if-eqz v2, :cond_b

    move v2, v5

    :goto_3
    invoke-direct {p0, v2, v4}, Lb/Q;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_c

    move-object v0, v6

    goto :goto_0

    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    int-to-long v7, v5

    add-long/2addr v0, v7

    goto :goto_1

    :cond_a
    move v2, v4

    goto :goto_2

    :cond_b
    move v2, v4

    goto :goto_3

    :cond_c
    invoke-virtual {p0, p1}, Lb/Q;->a_(I)Lb/y;

    move-result-object v2

    if-eqz v2, :cond_d

    iput v4, p0, Lb/Q;->j:I

    invoke-direct {p0, v2}, Lb/Q;->a(Lb/y;)V

    move-object v0, v2

    goto :goto_0

    :cond_d
    invoke-static {v9}, Lb/ae;->b(I)Z

    move-result v2

    if-nez v2, :cond_e

    move-object v0, v6

    goto/16 :goto_0

    :cond_e
    if-lez v5, :cond_f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long v7, v0, v7

    long-to-int v5, v7

    if-gtz v5, :cond_f

    invoke-static {v9}, Lb/ae;->a(I)V

    move-object v0, v6

    goto/16 :goto_0

    :cond_f
    move v2, v3

    goto :goto_2
.end method

.method public final e()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final e(Lb/E;)V
    .locals 0

    invoke-virtual {p0, p1}, Lb/Q;->b(Lb/E;)V

    return-void
.end method

.method public final f()V
    .locals 1

    sget-boolean v0, Lb/Q;->n:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/Q;->d:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method public final f(Lb/E;)V
    .locals 2

    iget-object v0, p0, Lb/Q;->a:Lb/B;

    iget v0, v0, Lb/B;->u:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lb/E;->a(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lb/Q;->i(Lb/E;)V

    goto :goto_0
.end method

.method public final g(Lb/E;)V
    .locals 1

    invoke-virtual {p0, p1}, Lb/Q;->c(Lb/E;)V

    iget-object v0, p0, Lb/Q;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lb/Q;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/Q;->m()V

    :cond_0
    return-void
.end method

.method public final g_()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lb/Q;->a(IZ)Z

    invoke-direct {p0}, Lb/Q;->a()V

    return-void
.end method

.method protected final h(Lb/E;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lb/Q;->b(Lb/E;Z)V

    return-void
.end method

.method public final h_()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected i(Lb/E;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Must override"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final k()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/Q;->c:Z

    return-void
.end method

.method public final p()Lb/x;
    .locals 1

    iget-object v0, p0, Lb/Q;->e:Lb/x;

    return-object v0
.end method

.method public final q()V
    .locals 0

    invoke-virtual {p0}, Lb/Q;->v()V

    return-void
.end method

.method public final r()V
    .locals 0

    invoke-virtual {p0, p0}, Lb/Q;->d(Lb/Q;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lb/Q;->a:Lb/B;

    iget v1, v1, Lb/B;->D:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
