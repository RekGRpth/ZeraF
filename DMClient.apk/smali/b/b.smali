.class public final Lb/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:[B

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lb/b;->b:I

    new-array v0, p1, [B

    iput-object v0, p0, Lb/b;->a:[B

    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lb/b;->b:I

    invoke-static {p1}, Lb/X;->a(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    iput-object v0, p0, Lb/b;->a:[B

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lb/b;->b:I

    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lb/b;->a:[B

    return-void
.end method


# virtual methods
.method public final a(IB)Lb/b;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lb/b;->a:[B

    aput-byte v1, v0, v1

    iput v1, p0, Lb/b;->b:I

    return-object p0
.end method

.method public final a(I[BII)Lb/b;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lb/b;->a:[B

    const/4 v1, 0x1

    invoke-static {p2, v2, v0, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v2, p0, Lb/b;->b:I

    return-object p0
.end method

.method public final a()[B
    .locals 1

    iget-object v0, p0, Lb/b;->a:[B

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lb/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/b;->a:[B

    check-cast p1, Lb/b;

    iget-object v1, p1, Lb/b;->a:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    iget v0, p0, Lb/b;->b:I

    if-nez v0, :cond_0

    iget-object v1, p0, Lb/b;->a:[B

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_1

    :cond_0
    iget v0, p0, Lb/b;->b:I

    return v0

    :cond_1
    aget-byte v3, v1, v0

    iget v4, p0, Lb/b;->b:I

    mul-int/lit8 v4, v4, 0x1f

    add-int/2addr v3, v4

    iput v3, p0, Lb/b;->b:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
