.class public Lb/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/e$a;
    }
.end annotation


# static fields
.field private static n:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static synthetic r:Z


# instance fields
.field private a:I

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lb/Q;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private volatile d:Z

.field private e:Z

.field private final f:Ljava/util/concurrent/locks/Lock;

.field private g:Lb/K;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lb/s;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:[Lb/x;

.field private final k:Lb/x;

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lb/e$a;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/concurrent/locks/Lock;

.field private o:I

.field private p:I

.field private final q:Ljava/util/concurrent/locks/Lock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lb/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/e;->r:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lb/e;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, -0x54523502

    iput v0, p0, Lb/e;->a:I

    iput-boolean v2, p0, Lb/e;->d:Z

    iput-boolean v1, p0, Lb/e;->e:Z

    iput-object v3, p0, Lb/e;->g:Lb/K;

    iput v1, p0, Lb/e;->i:I

    iput-object v3, p0, Lb/e;->j:[Lb/x;

    const/16 v0, 0x400

    iput v0, p0, Lb/e;->o:I

    iput v2, p0, Lb/e;->p:I

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lb/e;->m:Ljava/util/concurrent/locks/Lock;

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lb/e;->q:Ljava/util/concurrent/locks/Lock;

    new-instance v0, Lb/x;

    const-string v1, "terminater"

    invoke-direct {v0, v1}, Lb/x;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lb/e;->k:Lb/x;

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lb/e;->c:Ljava/util/Deque;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/e;->h:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/e;->b:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lb/e;->l:Ljava/util/Map;

    return-void
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lb/e;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/e;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lb/e;->g:Lb/K;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/e;->g:Lb/K;

    invoke-virtual {v0}, Lb/K;->a()V

    :cond_0
    iget-object v0, p0, Lb/e;->k:Lb/x;

    invoke-virtual {v0}, Lb/x;->b()V

    const v0, -0x21524111

    iput v0, p0, Lb/e;->a:I

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/s;

    invoke-virtual {v0}, Lb/s;->d_()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/s;

    invoke-virtual {v0}, Lb/s;->f()V

    goto :goto_1
.end method


# virtual methods
.method public final a(I)Lb/Q;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Lb/e;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/e;->d:Z

    iget-object v0, p0, Lb/e;->q:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    iget v0, p0, Lb/e;->o:I

    iget v2, p0, Lb/e;->p:I

    iget-object v3, p0, Lb/e;->q:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lb/e;->i:I

    iget v0, p0, Lb/e;->i:I

    new-array v0, v0, [Lb/x;

    iput-object v0, p0, Lb/e;->j:[Lb/x;

    iget-object v0, p0, Lb/e;->j:[Lb/x;

    const/4 v3, 0x0

    iget-object v4, p0, Lb/e;->k:Lb/x;

    aput-object v4, v0, v3

    new-instance v0, Lb/K;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v3}, Lb/K;-><init>(Lb/e;I)V

    iput-object v0, p0, Lb/e;->g:Lb/K;

    iget-object v0, p0, Lb/e;->j:[Lb/x;

    const/4 v3, 0x1

    iget-object v4, p0, Lb/e;->g:Lb/K;

    invoke-virtual {v4}, Lb/K;->f()Lb/x;

    move-result-object v4

    aput-object v4, v0, v3

    iget-object v0, p0, Lb/e;->g:Lb/K;

    invoke-virtual {v0}, Lb/K;->l()V

    const/4 v0, 0x2

    :goto_0
    add-int/lit8 v3, v2, 0x2

    if-ne v0, v3, :cond_1

    iget v0, p0, Lb/e;->i:I

    add-int/lit8 v0, v0, -0x1

    :goto_1
    add-int/lit8 v3, v2, 0x2

    if-ge v0, v3, :cond_2

    :cond_0
    iget-boolean v0, p0, Lb/e;->e:Z

    if-eqz v0, :cond_3

    const v0, 0x9523dfd

    invoke-static {v0}, Lb/ae;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move-object v0, v1

    :goto_3
    return-object v0

    :cond_1
    :try_start_1
    new-instance v3, Lb/s;

    invoke-direct {v3, p0, v0}, Lb/s;-><init>(Lb/e;I)V

    iget-object v4, p0, Lb/e;->h:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lb/e;->j:[Lb/x;

    invoke-virtual {v3}, Lb/s;->h()Lb/x;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v3}, Lb/s;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lb/e;->c:Ljava/util/Deque;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lb/e;->j:[Lb/x;

    const/4 v4, 0x0

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lb/e;->c:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x9523e33

    invoke-static {v0}, Lb/ae;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_4
    :try_start_2
    iget-object v0, p0, Lb/e;->c:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pollLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v0, Lb/e;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-static {p1, p0, v2, v0}, Lb/Q;->a(ILb/e;II)Lb/Q;

    move-result-object v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lb/e;->c:Ljava/util/Deque;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move-object v0, v1

    goto :goto_3

    :cond_5
    :try_start_3
    iget-object v1, p0, Lb/e;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lb/e;->j:[Lb/x;

    invoke-virtual {v0}, Lb/Q;->p()Lb/x;

    move-result-object v3

    aput-object v3, v1, v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v1, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;)Lb/e$a;
    .locals 3

    iget-object v0, p0, Lb/e;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lb/e;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/e$a;

    if-nez v0, :cond_0

    const/16 v0, 0x3d

    invoke-static {v0}, Lb/ae;->a(I)V

    new-instance v0, Lb/e$a;

    const/4 v1, 0x0

    new-instance v2, Lb/B;

    invoke-direct {v2}, Lb/B;-><init>()V

    invoke-direct {v0, v1, v2}, Lb/e$a;-><init>(Lb/Q;Lb/B;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lb/e;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    iget-object v1, v0, Lb/e$a;->a:Lb/Q;

    invoke-virtual {v1}, Lb/Q;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lb/e;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lb/e;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(J)Lb/s;
    .locals 9

    const-wide/16 v7, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lb/e;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    const/4 v2, -0x1

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    iget-object v0, p0, Lb/e;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v4, v0, :cond_0

    cmp-long v0, p1, v7

    if-eqz v0, :cond_2

    const-wide/16 v5, 0x1

    shl-long/2addr v5, v4

    and-long/2addr v5, p1

    cmp-long v0, v5, v7

    if-lez v0, :cond_4

    :cond_2
    iget-object v0, p0, Lb/e;->h:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/s;

    invoke-virtual {v0}, Lb/s;->i()I

    move-result v3

    if-eqz v1, :cond_3

    if-ge v3, v2, :cond_4

    :cond_3
    iget-object v0, p0, Lb/e;->h:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/s;

    move v1, v3

    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public final a(II)V
    .locals 3

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    if-lez p2, :cond_0

    iget-object v0, p0, Lb/e;->q:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    iput p2, p0, Lb/e;->o:I

    iget-object v0, p0, Lb/e;->q:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    if-ltz p2, :cond_1

    iget-object v0, p0, Lb/e;->q:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    iput p2, p0, Lb/e;->p:I

    iget-object v0, p0, Lb/e;->q:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "option = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(ILb/c;)V
    .locals 1

    iget-object v0, p0, Lb/e;->j:[Lb/x;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lb/x;->a(Lb/c;)V

    return-void
.end method

.method public final a(Lb/Q;)V
    .locals 3

    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p1}, Lb/Q;->u()I

    move-result v0

    iget-object v1, p0, Lb/e;->c:Ljava/util/Deque;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lb/e;->j:[Lb/x;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lb/x;->b()V

    iget-object v1, p0, Lb/e;->j:[Lb/x;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    iget-object v0, p0, Lb/e;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lb/e;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/e;->g:Lb/K;

    invoke-virtual {v0}, Lb/K;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a()Z
    .locals 2

    iget v0, p0, Lb/e;->a:I

    const v1, -0x54523502

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    const v0, -0x21524111

    iput v0, p0, Lb/e;->a:I

    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    iget-boolean v0, p0, Lb/e;->d:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lb/e;->e:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lb/e;->e:Z

    iget-object v1, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lb/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_2

    iget-object v0, p0, Lb/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/e;->g:Lb/K;

    invoke-virtual {v0}, Lb/K;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :cond_1
    iget-object v0, p0, Lb/e;->k:Lb/x;

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lb/x;->a(J)Lb/c;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lb/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/Q;

    invoke-virtual {v0}, Lb/Q;->q()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_3
    sget-boolean v1, Lb/e;->r:Z

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lb/c;->b()Lb/c$a;

    move-result-object v0

    sget-object v1, Lb/c$a;->p:Lb/c$a;

    if-eq v0, v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    sget-boolean v0, Lb/e;->r:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lb/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    iget-object v0, p0, Lb/e;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    invoke-direct {p0}, Lb/e;->d()V

    return-void
.end method

.method public final b(Lb/Q;)V
    .locals 2

    iget-object v0, p0, Lb/e;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lb/e;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/e;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/e$a;

    iget-object v0, v0, Lb/e$a;->a:Lb/Q;

    if-ne v0, p1, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lb/e;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final c()Lb/ag;
    .locals 1

    iget-object v0, p0, Lb/e;->g:Lb/K;

    return-object v0
.end method
