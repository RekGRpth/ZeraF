.class public Lb/R;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lb/o;
.implements Lb/p;
.implements Lb/t;


# static fields
.field private static synthetic s:Z


# instance fields
.field private a:Z

.field private b:Ljava/nio/channels/SocketChannel;

.field private c:Ljava/nio/ByteBuffer;

.field private d:I

.field private e:Lb/h;

.field private f:Lb/V;

.field private g:I

.field private h:Lb/k;

.field private i:Z

.field private final j:Ljava/nio/ByteBuffer;

.field private final k:Ljava/nio/ByteBuffer;

.field private l:Lb/O;

.field private m:Lb/B;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Z

.field private q:Lb/Q;

.field private r:Lb/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/R;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/R;->s:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/nio/channels/SocketChannel;Lb/B;Ljava/lang/String;)V
    .locals 4

    const/16 v3, 0xc

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    iput-object v1, p0, Lb/R;->c:Ljava/nio/ByteBuffer;

    iput v2, p0, Lb/R;->d:I

    iput-boolean v2, p0, Lb/R;->a:Z

    iput-object v1, p0, Lb/R;->f:Lb/V;

    iput v2, p0, Lb/R;->g:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/R;->i:Z

    iput-object v1, p0, Lb/R;->l:Lb/O;

    iput-object p2, p0, Lb/R;->m:Lb/B;

    iput-boolean v2, p0, Lb/R;->o:Z

    iput-boolean v2, p0, Lb/R;->p:Z

    iput-object p3, p0, Lb/R;->n:Ljava/lang/String;

    iput-object v1, p0, Lb/R;->q:Lb/Q;

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    iput-object v1, p0, Lb/R;->h:Lb/k;

    iput-object v1, p0, Lb/R;->e:Lb/h;

    :try_start_0
    iget-object v0, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-static {v0}, Lb/X;->a(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget v0, v0, Lb/B;->j:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    iget-object v1, p0, Lb/R;->m:Lb/B;

    iget v1, v1, Lb/B;->j:I

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSendBufferSize(I)V

    :cond_0
    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget v0, v0, Lb/B;->k:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    iget-object v1, p0, Lb/R;->m:Lb/B;

    iget v1, v1, Lb/B;->k:I

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setReceiveBufferSize(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lb/ae$a;

    invoke-direct {v1, v0}, Lb/ae$a;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method private a(Lb/V;)I
    .locals 1

    :try_start_0
    iget-object v0, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-interface {p1, v0}, Lb/V;->a(Ljava/nio/channels/WritableByteChannel;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(Ljava/nio/ByteBuffer;)I
    .locals 1

    :try_start_0
    iget-object v0, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, p1}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(IJLb/O;I)Lb/h;
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-object v0, v0, Lb/B;->E:Ljava/lang/Class;

    if-nez v0, :cond_1

    if-ne p5, v1, :cond_0

    new-instance v0, Lb/Y;

    invoke-direct {v0, p1, p2, p3, p4}, Lb/Y;-><init>(IJLb/p;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lb/g;

    invoke-direct {v0, p1, p2, p3}, Lb/g;-><init>(IJ)V

    goto :goto_0

    :cond_1
    if-nez p5, :cond_2

    :try_start_0
    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-object v0, v0, Lb/B;->E:Ljava/lang/Class;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/h;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-object v0, v0, Lb/B;->E:Ljava/lang/Class;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lb/p;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    const/4 v2, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/h;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_4
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(ILb/O;I)Lb/k;
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-object v0, v0, Lb/B;->F:Ljava/lang/Class;

    if-nez v0, :cond_1

    if-ne p3, v1, :cond_0

    new-instance v0, Lb/Z;

    invoke-direct {v0, p1, p2}, Lb/Z;-><init>(ILb/q;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lb/j;

    invoke-direct {v0, p1}, Lb/j;-><init>(I)V

    goto :goto_0

    :cond_1
    if-nez p3, :cond_2

    :try_start_0
    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-object v0, v0, Lb/B;->F:Ljava/lang/Class;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/k;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-object v0, v0, Lb/B;->F:Ljava/lang/Class;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lb/q;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/k;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_4
    move-exception v0

    new-instance v1, Lb/ae$b;

    invoke-direct {v1, v0}, Lb/ae$b;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private f()V
    .locals 1

    sget-boolean v0, Lb/R;->s:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/R;->o:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private g()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    sget-boolean v0, Lb/R;->s:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/R;->o:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-boolean v3, p0, Lb/R;->o:Z

    iget-boolean v0, p0, Lb/R;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->b(Ljava/nio/channels/SelectableChannel;)V

    iput-boolean v3, p0, Lb/R;->a:Z

    :cond_1
    iget-object v0, p0, Lb/R;->r:Lb/r;

    invoke-virtual {v0}, Lb/r;->a()V

    iget-object v0, p0, Lb/R;->h:Lb/k;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/R;->h:Lb/k;

    invoke-virtual {v0, v2}, Lb/k;->a(Lb/q;)V

    :cond_2
    iget-object v0, p0, Lb/R;->e:Lb/h;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lb/R;->e:Lb/h;

    invoke-virtual {v0, v2}, Lb/h;->a(Lb/p;)V

    :cond_3
    iput-object v2, p0, Lb/R;->l:Lb/O;

    return-void
.end method

.method private h()V
    .locals 3

    sget-boolean v0, Lb/R;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/R;->l:Lb/O;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/R;->q:Lb/Q;

    iget-object v1, p0, Lb/R;->n:Ljava/lang/String;

    iget-object v2, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1, v2}, Lb/Q;->c(Ljava/lang/String;Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v0}, Lb/O;->t()V

    invoke-direct {p0}, Lb/R;->g()V

    invoke-direct {p0}, Lb/R;->f()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-boolean v0, p0, Lb/R;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/R;->h:Lb/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/R;->h:Lb/k;

    invoke-virtual {v0}, Lb/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/R;->p:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lb/R;->g()V

    invoke-direct {p0}, Lb/R;->f()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lb/s;Lb/O;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    sget-boolean v2, Lb/R;->s:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lb/R;->o:Z

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-boolean v0, p0, Lb/R;->o:Z

    sget-boolean v2, Lb/R;->s:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lb/R;->l:Lb/O;

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    sget-boolean v2, Lb/R;->s:Z

    if-nez v2, :cond_2

    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iput-object p2, p0, Lb/R;->l:Lb/O;

    iget-object v2, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v2}, Lb/O;->r()Lb/Q;

    move-result-object v2

    iput-object v2, p0, Lb/R;->q:Lb/Q;

    new-instance v2, Lb/r;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lb/r;-><init>(Lb/s;)V

    iput-object v2, p0, Lb/R;->r:Lb/r;

    iget-object v2, p0, Lb/R;->r:Lb/r;

    invoke-virtual {v2, p0}, Lb/r;->a(Lb/t;)V

    iget-object v2, p0, Lb/R;->r:Lb/r;

    invoke-virtual {v2, p1}, Lb/r;->a(Lb/s;)V

    iget-object v2, p0, Lb/R;->r:Lb/r;

    iget-object v3, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2, v3}, Lb/r;->a(Ljava/nio/channels/SelectableChannel;)V

    iput-boolean v0, p0, Lb/R;->a:Z

    iget-object v2, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lb/R;->m:Lb/B;

    iget-byte v3, v3, Lb/B;->d:B

    add-int/lit8 v3, v3, 0x1

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    const/16 v3, 0x7f

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lb/R;->r:Lb/r;

    iget-object v3, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2, v3}, Lb/r;->c(Ljava/nio/channels/SelectableChannel;)V

    :try_start_0
    iget-object v2, p0, Lb/R;->m:Lb/B;

    iget-object v2, v2, Lb/B;->F:Ljava/lang/Class;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lb/R;->m:Lb/B;

    iget-object v2, v2, Lb/B;->F:Ljava/lang/Class;

    const-string v3, "RAW_ENCODER"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_4

    :goto_0
    move v1, v0

    :goto_1
    if-nez v1, :cond_3

    iget-object v0, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iput v0, p0, Lb/R;->g:I

    new-instance v1, Lb/V$a;

    iget-object v0, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-direct {v1, v0}, Lb/V$a;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v1, p0, Lb/R;->f:Lb/V;

    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->d(Ljava/nio/channels/SelectableChannel;)V

    :cond_3
    invoke-virtual {p0}, Lb/R;->g_()V

    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lb/y;)Z
    .locals 3

    const/4 v2, 0x1

    sget-boolean v0, Lb/R;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget v0, v0, Lb/B;->l:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget v0, v0, Lb/B;->l:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v0, p1}, Lb/O;->a(Lb/y;)Z

    move-result v0

    sget-boolean v1, Lb/R;->s:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Lb/y;

    invoke-direct {v0, v2}, Lb/y;-><init>(I)V

    invoke-virtual {v0, v2}, Lb/y;->a(B)V

    iget-object v1, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v1, v0}, Lb/O;->a(Lb/y;)Z

    move-result v0

    iget-object v1, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v1}, Lb/O;->q()V

    sget-boolean v1, Lb/R;->s:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lb/R;->e:Lb/h;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iget-object v1, p0, Lb/R;->e:Lb/h;

    iget-object v2, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v1, v2}, Lb/h;->a(Lb/p;)V

    return v0
.end method

.method public final b()V
    .locals 3

    iget-boolean v0, p0, Lb/R;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/R;->e:Lb/h;

    iget-object v1, p0, Lb/R;->c:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lb/h;->a(Ljava/nio/ByteBuffer;I)I

    sget-boolean v0, Lb/R;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/R;->e:Lb/h;

    invoke-virtual {v0}, Lb/h;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v0}, Lb/O;->q()V

    invoke-direct {p0}, Lb/R;->h()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->c(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/R;->r:Lb/r;

    invoke-virtual {v0}, Lb/r;->g_()V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->d(Ljava/nio/channels/SelectableChannel;)V

    invoke-virtual {p0}, Lb/R;->h_()V

    return-void
.end method

.method public final d()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final e()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final g_()V
    .locals 13

    const/4 v4, 0x0

    const/16 v6, 0xa

    const/4 v12, -0x1

    const/4 v11, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p0, Lb/R;->i:Z

    if-eqz v0, :cond_f

    sget-boolean v0, Lb/R;->s:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lb/R;->i:Z

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lb/R;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    if-ne v0, v12, :cond_2

    invoke-direct {p0}, Lb/R;->h()V

    move v0, v5

    :goto_0
    if-nez v0, :cond_f

    :cond_1
    :goto_1
    return-void

    :cond_2
    if-nez v0, :cond_3

    move v0, v5

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    aget-byte v0, v0, v5

    if-ne v0, v12, :cond_6

    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lt v0, v6, :cond_5

    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    iget-object v0, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    const/16 v1, 0xc

    if-ge v0, v1, :cond_5

    iget v0, p0, Lb/R;->g:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->d(Ljava/nio/channels/SelectableChannel;)V

    :cond_4
    iget-object v0, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget-object v1, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v11}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lb/R;->m:Lb/B;

    iget v2, v2, Lb/B;->l:I

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lb/R;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget v0, p0, Lb/R;->g:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lb/R;->g:I

    :cond_5
    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    :cond_6
    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    aget-byte v0, v0, v5

    if-ne v0, v12, :cond_7

    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_d

    :cond_7
    sget-object v0, Lb/d;->e:Lb/d;

    invoke-virtual {v0}, Lb/d;->a()I

    move-result v0

    invoke-direct {p0, v0, v4, v5}, Lb/R;->a(ILb/O;I)Lb/k;

    move-result-object v0

    iput-object v0, p0, Lb/R;->h:Lb/k;

    iget-object v0, p0, Lb/R;->h:Lb/k;

    iget-object v1, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v0, v1}, Lb/k;->a(Lb/q;)V

    sget-object v0, Lb/d;->d:Lb/d;

    invoke-virtual {v0}, Lb/d;->a()I

    move-result v1

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-wide v2, v0, Lb/B;->q:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lb/R;->a(IJLb/O;I)Lb/h;

    move-result-object v0

    iput-object v0, p0, Lb/R;->e:Lb/h;

    iget-object v0, p0, Lb/R;->e:Lb/h;

    iget-object v1, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v0, v1}, Lb/h;->a(Lb/p;)V

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-byte v0, v0, Lb/B;->d:B

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0xff

    if-lt v0, v1, :cond_8

    move v0, v6

    :goto_2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v2, p0, Lb/R;->h:Lb/k;

    invoke-virtual {v2, v1}, Lb/k;->a(Ljava/nio/ByteBuffer;)Lb/V;

    sget-boolean v2, Lb/R;->s:Z

    if-nez v2, :cond_9

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-eq v1, v0, :cond_9

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_8
    const/4 v0, 0x2

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lb/R;->c:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iput v0, p0, Lb/R;->d:I

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget v0, v0, Lb/B;->l:I

    if-eq v0, v11, :cond_a

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget v0, v0, Lb/B;->l:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_b

    :cond_a
    iget-object v0, p0, Lb/R;->e:Lb/h;

    invoke-virtual {v0, p0}, Lb/h;->a(Lb/p;)V

    :cond_b
    :goto_3
    iget v0, p0, Lb/R;->g:I

    if-nez v0, :cond_c

    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->d(Ljava/nio/channels/SelectableChannel;)V

    :cond_c
    iput-boolean v5, p0, Lb/R;->i:Z

    move v0, v11

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lb/R;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    aget-byte v0, v0, v6

    if-nez v0, :cond_e

    sget-object v0, Lb/d;->e:Lb/d;

    invoke-virtual {v0}, Lb/d;->a()I

    move-result v0

    invoke-direct {p0, v0, v4, v5}, Lb/R;->a(ILb/O;I)Lb/k;

    move-result-object v0

    iput-object v0, p0, Lb/R;->h:Lb/k;

    iget-object v0, p0, Lb/R;->h:Lb/k;

    iget-object v1, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v0, v1}, Lb/k;->a(Lb/q;)V

    sget-object v0, Lb/d;->d:Lb/d;

    invoke-virtual {v0}, Lb/d;->a()I

    move-result v1

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-wide v2, v0, Lb/B;->q:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lb/R;->a(IJLb/O;I)Lb/h;

    move-result-object v0

    iput-object v0, p0, Lb/R;->e:Lb/h;

    iget-object v0, p0, Lb/R;->e:Lb/h;

    iget-object v1, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v0, v1}, Lb/h;->a(Lb/p;)V

    goto :goto_3

    :cond_e
    sget-object v0, Lb/d;->e:Lb/d;

    invoke-virtual {v0}, Lb/d;->a()I

    move-result v0

    iget-object v1, p0, Lb/R;->l:Lb/O;

    invoke-direct {p0, v0, v1, v11}, Lb/R;->a(ILb/O;I)Lb/k;

    move-result-object v0

    iput-object v0, p0, Lb/R;->h:Lb/k;

    sget-object v0, Lb/d;->d:Lb/d;

    invoke-virtual {v0}, Lb/d;->a()I

    move-result v7

    iget-object v0, p0, Lb/R;->m:Lb/B;

    iget-wide v8, v0, Lb/B;->q:J

    iget-object v10, p0, Lb/R;->l:Lb/O;

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lb/R;->a(IJLb/O;I)Lb/h;

    move-result-object v0

    iput-object v0, p0, Lb/R;->e:Lb/h;

    goto :goto_3

    :cond_f
    sget-boolean v0, Lb/R;->s:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lb/R;->e:Lb/h;

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_10
    iget v0, p0, Lb/R;->d:I

    if-nez v0, :cond_14

    iget-object v0, p0, Lb/R;->e:Lb/h;

    invoke-virtual {v0}, Lb/h;->b()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lb/R;->c:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lb/R;->c:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lb/R;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lb/R;->d:I

    iget-object v0, p0, Lb/R;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget v0, p0, Lb/R;->d:I

    if-ne v0, v12, :cond_14

    iput v5, p0, Lb/R;->d:I

    move v0, v11

    :goto_4
    iget-object v1, p0, Lb/R;->e:Lb/h;

    iget-object v2, p0, Lb/R;->c:Ljava/nio/ByteBuffer;

    iget v3, p0, Lb/R;->d:I

    invoke-virtual {v1, v2, v3}, Lb/h;->a(Ljava/nio/ByteBuffer;I)I

    move-result v1

    if-ne v1, v12, :cond_11

    :goto_5
    iget-object v0, p0, Lb/R;->l:Lb/O;

    invoke-virtual {v0}, Lb/O;->q()V

    if-eqz v11, :cond_1

    iget-object v0, p0, Lb/R;->e:Lb/h;

    invoke-virtual {v0}, Lb/h;->e()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->b(Ljava/nio/channels/SelectableChannel;)V

    iput-boolean v5, p0, Lb/R;->a:Z

    goto/16 :goto_1

    :cond_11
    iget v2, p0, Lb/R;->d:I

    if-ge v1, v2, :cond_12

    iget-object v2, p0, Lb/R;->r:Lb/r;

    iget-object v3, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2, v3}, Lb/r;->f(Ljava/nio/channels/SelectableChannel;)V

    :cond_12
    iget v2, p0, Lb/R;->d:I

    sub-int v1, v2, v1

    iput v1, p0, Lb/R;->d:I

    move v11, v0

    goto :goto_5

    :cond_13
    invoke-direct {p0}, Lb/R;->h()V

    goto/16 :goto_1

    :cond_14
    move v0, v5

    goto :goto_4
.end method

.method public final h_()V
    .locals 2

    iget v0, p0, Lb/R;->g:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lb/R;->h:Lb/k;

    if-nez v0, :cond_0

    sget-boolean v0, Lb/R;->s:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lb/R;->i:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/R;->h:Lb/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lb/k;->a(Ljava/nio/ByteBuffer;)Lb/V;

    move-result-object v0

    iput-object v0, p0, Lb/R;->f:Lb/V;

    iget-object v0, p0, Lb/R;->f:Lb/V;

    invoke-interface {v0}, Lb/V;->c()I

    move-result v0

    iput v0, p0, Lb/R;->g:I

    iget-object v0, p0, Lb/R;->f:Lb/V;

    invoke-interface {v0}, Lb/V;->c()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->g(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lb/R;->h:Lb/k;

    invoke-virtual {v0}, Lb/k;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lb/R;->h()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lb/R;->f:Lb/V;

    invoke-direct {p0, v0}, Lb/R;->a(Lb/V;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->g(Ljava/nio/channels/SelectableChannel;)V

    iget-boolean v0, p0, Lb/R;->p:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lb/R;->a()V

    goto :goto_0

    :cond_3
    iget v1, p0, Lb/R;->g:I

    sub-int v0, v1, v0

    iput v0, p0, Lb/R;->g:I

    iget-boolean v0, p0, Lb/R;->i:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lb/R;->g:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lb/R;->r:Lb/r;

    iget-object v1, p0, Lb/R;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v1}, Lb/r;->g(Ljava/nio/channels/SelectableChannel;)V

    :cond_4
    iget v0, p0, Lb/R;->g:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/R;->h:Lb/k;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lb/R;->h:Lb/k;

    invoke-virtual {v0}, Lb/k;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lb/R;->h()V

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lb/R;->p:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lb/R;->a()V

    goto :goto_0
.end method
