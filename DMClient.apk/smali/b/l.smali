.class public Lb/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static synthetic e:Z


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lb/E;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/l;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/l;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lb/l;->b:I

    iput v0, p0, Lb/l;->c:I

    iput-boolean v0, p0, Lb/l;->d:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/l;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Lb/y;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb/l;->a([Lb/E;)Lb/y;

    move-result-object v0

    return-object v0
.end method

.method public final a([Lb/E;)Lb/y;
    .locals 4

    const/4 v2, 0x0

    :cond_0
    :goto_0
    iget v0, p0, Lb/l;->b:I

    if-gtz v0, :cond_1

    const/16 v0, 0x23

    invoke-static {v0}, Lb/ae;->a(I)V

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lb/l;->a:Ljava/util/List;

    iget v1, p0, Lb/l;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/E;

    invoke-virtual {v0}, Lb/E;->c()Lb/y;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_5

    if-eqz p1, :cond_2

    iget-object v0, p0, Lb/l;->a:Ljava/util/List;

    iget v3, p0, Lb/l;->c:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/E;

    aput-object v0, p1, v2

    :cond_2
    invoke-virtual {v1}, Lb/y;->d()Z

    move-result v0

    iput-boolean v0, p0, Lb/l;->d:Z

    iget-boolean v0, p0, Lb/l;->d:Z

    if-nez v0, :cond_3

    iget v0, p0, Lb/l;->c:I

    add-int/lit8 v0, v0, 0x1

    iget v2, p0, Lb/l;->b:I

    rem-int/2addr v0, v2

    iput v0, p0, Lb/l;->c:I

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    sget-boolean v0, Lb/l;->e:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lb/l;->d:Z

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_6
    iget v0, p0, Lb/l;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb/l;->b:I

    iget-object v0, p0, Lb/l;->a:Ljava/util/List;

    iget v1, p0, Lb/l;->c:I

    iget v3, p0, Lb/l;->b:I

    invoke-static {v0, v1, v3}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/l;->c:I

    iget v1, p0, Lb/l;->b:I

    if-ne v0, v1, :cond_0

    iput v2, p0, Lb/l;->c:I

    goto :goto_0
.end method

.method public final a(Lb/E;)V
    .locals 3

    iget-object v0, p0, Lb/l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lb/l;->a:Ljava/util/List;

    iget v1, p0, Lb/l;->b:I

    iget-object v2, p0, Lb/l;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/l;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/l;->b:I

    return-void
.end method

.method public final b(Lb/E;)V
    .locals 3

    iget-object v0, p0, Lb/l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lb/l;->b:I

    if-ge v0, v1, :cond_0

    iget v1, p0, Lb/l;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lb/l;->b:I

    iget-object v1, p0, Lb/l;->a:Ljava/util/List;

    iget v2, p0, Lb/l;->b:I

    invoke-static {v1, v0, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/l;->c:I

    iget v1, p0, Lb/l;->b:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lb/l;->c:I

    :cond_0
    iget-object v0, p0, Lb/l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final c(Lb/E;)V
    .locals 3

    iget-object v0, p0, Lb/l;->a:Ljava/util/List;

    iget-object v1, p0, Lb/l;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    iget v2, p0, Lb/l;->b:I

    invoke-static {v0, v1, v2}, Lb/X;->a(Ljava/util/List;II)V

    iget v0, p0, Lb/l;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lb/l;->b:I

    return-void
.end method
