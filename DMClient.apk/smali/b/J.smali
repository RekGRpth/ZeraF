.class public Lb/J;
.super Lb/Q;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/J$a;
    }
.end annotation


# static fields
.field private static synthetic c:Z


# instance fields
.field private final b:Lb/w;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/J;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/J;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lb/Q;-><init>(Lb/e;II)V

    iget-object v0, p0, Lb/J;->a:Lb/B;

    const/16 v1, 0x8

    iput v1, v0, Lb/B;->l:I

    new-instance v0, Lb/w;

    invoke-direct {v0}, Lb/w;-><init>()V

    iput-object v0, p0, Lb/J;->b:Lb/w;

    return-void
.end method


# virtual methods
.method protected final a(Lb/E;Z)V
    .locals 1

    sget-boolean v0, Lb/J;->c:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lb/J;->b:Lb/w;

    invoke-virtual {v0, p1}, Lb/w;->a(Lb/E;)V

    return-void
.end method

.method public final a(Lb/y;I)Z
    .locals 1

    iget-object v0, p0, Lb/J;->b:Lb/w;

    invoke-virtual {v0, p1}, Lb/w;->a(Lb/y;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/J;->b:Lb/w;

    invoke-virtual {v0, p1}, Lb/w;->c(Lb/E;)V

    return-void
.end method

.method protected final c(Lb/E;)V
    .locals 1

    iget-object v0, p0, Lb/J;->b:Lb/w;

    invoke-virtual {v0, p1}, Lb/w;->b(Lb/E;)V

    return-void
.end method
