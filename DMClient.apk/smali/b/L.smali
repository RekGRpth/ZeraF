.class public Lb/L;
.super Lb/N;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/L$a;
    }
.end annotation


# static fields
.field private static synthetic d:Z


# instance fields
.field private b:Z

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/L;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/L;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lb/N;-><init>(Lb/e;II)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/L;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/L;->c:Z

    iget-object v0, p0, Lb/L;->a:Lb/B;

    const/4 v1, 0x4

    iput v1, v0, Lb/B;->l:I

    return-void
.end method


# virtual methods
.method protected final a(Lb/y;I)Z
    .locals 2

    iget-boolean v0, p0, Lb/L;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot send another reply"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v1

    invoke-super {p0, p1, p2}, Lb/N;->a(Lb/y;I)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    return v0

    :cond_1
    if-nez v1, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/L;->b:Z

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final a_(I)Lb/y;
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lb/L;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot receive another request"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lb/L;->c:Z

    if-eqz v0, :cond_6

    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lb/N;->a_(I)Lb/y;

    move-result-object v4

    if-nez v4, :cond_3

    move-object v0, v3

    :cond_2
    :goto_1
    return-object v0

    :cond_3
    invoke-virtual {v4}, Lb/y;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v4}, Lb/y;->g()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    invoke-super {p0, v4, p1}, Lb/N;->a(Lb/y;I)Z

    move-result v4

    sget-boolean v5, Lb/L;->d:Z

    if-nez v5, :cond_5

    if-nez v4, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lb/L;->c:Z

    :cond_6
    invoke-super {p0, p1}, Lb/N;->a_(I)Lb/y;

    move-result-object v0

    if-nez v0, :cond_8

    move-object v0, v3

    goto :goto_1

    :cond_7
    invoke-super {p0}, Lb/N;->a()V

    goto :goto_0

    :cond_8
    invoke-virtual {v0}, Lb/y;->d()Z

    move-result v2

    if-nez v2, :cond_2

    iput-boolean v1, p0, Lb/L;->b:Z

    iput-boolean v1, p0, Lb/L;->c:Z

    goto :goto_1
.end method
