.class public Lb/M;
.super Lb/f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/M$a;
    }
.end annotation


# static fields
.field private static synthetic d:Z


# instance fields
.field private b:Z

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/M;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lb/M;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/e;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lb/f;-><init>(Lb/e;II)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/M;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/M;->c:Z

    iget-object v0, p0, Lb/M;->a:Lb/B;

    const/4 v1, 0x3

    iput v1, v0, Lb/B;->l:I

    return-void
.end method


# virtual methods
.method public final a(Lb/y;I)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lb/M;->b:Z

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot send another request"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v2, p0, Lb/M;->c:Z

    if-eqz v2, :cond_3

    new-instance v2, Lb/y;

    invoke-direct {v2}, Lb/y;-><init>()V

    invoke-virtual {v2, v1}, Lb/y;->a(I)V

    invoke-super {p0, v2, v0}, Lb/f;->a(Lb/y;I)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    iput-boolean v0, p0, Lb/M;->c:Z

    :cond_3
    invoke-virtual {p1}, Lb/y;->d()Z

    move-result v2

    invoke-super {p0, p1, p2}, Lb/f;->a(Lb/y;I)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v2, :cond_4

    iput-boolean v1, p0, Lb/M;->b:Z

    iput-boolean v1, p0, Lb/M;->c:Z

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method protected final a_(I)Lb/y;
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-boolean v1, p0, Lb/M;->b:Z

    if-nez v1, :cond_0

    const v0, 0x9523dfb

    invoke-static {v0}, Lb/ae;->a(I)V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot wait before send"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v1, p0, Lb/M;->c:Z

    if-eqz v1, :cond_6

    invoke-super {p0, p1}, Lb/f;->a_(I)Lb/y;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v1}, Lb/y;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lb/y;->g()I

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    invoke-super {p0, p1}, Lb/f;->a_(I)Lb/y;

    move-result-object v1

    sget-boolean v2, Lb/M;->d:Z

    if-nez v2, :cond_4

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {v1}, Lb/y;->d()Z

    move-result v1

    if-nez v1, :cond_3

    const/16 v1, 0x23

    invoke-static {v1}, Lb/ae;->a(I)V

    goto :goto_0

    :cond_5
    iput-boolean v3, p0, Lb/M;->c:Z

    :cond_6
    invoke-super {p0, p1}, Lb/f;->a_(I)Lb/y;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lb/y;->d()Z

    move-result v0

    if-nez v0, :cond_7

    iput-boolean v3, p0, Lb/M;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/M;->c:Z

    :cond_7
    move-object v0, v1

    goto :goto_0
.end method
