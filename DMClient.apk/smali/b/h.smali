.class public abstract Lb/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lb/m;


# instance fields
.field private a:[B

.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/nio/ByteBuffer;

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lb/h;->f:I

    iput v1, p0, Lb/h;->c:I

    iput p1, p0, Lb/h;->d:I

    if-lez p1, :cond_0

    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lb/h;->e:Ljava/nio/ByteBuffer;

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lb/h;->a:[B

    iput-boolean v1, p0, Lb/h;->g:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;I)I
    .locals 5

    const/4 v1, -0x1

    iget v0, p0, Lb/h;->f:I

    if-gez v0, :cond_1

    move p2, v1

    :cond_0
    :goto_0
    return p2

    :cond_1
    iget-boolean v0, p0, Lb/h;->g:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lb/h;->b:I

    add-int/2addr v0, p2

    iput v0, p0, Lb/h;->b:I

    iget v0, p0, Lb/h;->c:I

    sub-int/2addr v0, p2

    iput v0, p0, Lb/h;->c:I

    :cond_2
    iget v0, p0, Lb/h;->c:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lb/h;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lb/h;->f:I

    if-gez v0, :cond_0

    move p2, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :cond_4
    :goto_1
    iget v2, p0, Lb/h;->c:I

    if-eqz v2, :cond_5

    if-ne v0, p2, :cond_7

    move p2, v0

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lb/h;->a()Z

    move-result v2

    if-nez v2, :cond_4

    iget v2, p0, Lb/h;->f:I

    if-gez v2, :cond_6

    move p2, v1

    goto :goto_0

    :cond_6
    move p2, v0

    goto :goto_0

    :cond_7
    iget v2, p0, Lb/h;->c:I

    sub-int v3, p2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, p0, Lb/h;->a:[B

    iget v4, p0, Lb/h;->b:I

    invoke-virtual {p1, v3, v4, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    iget v3, p0, Lb/h;->b:I

    add-int/2addr v3, v2

    iput v3, p0, Lb/h;->b:I

    add-int/2addr v0, v2

    iget v3, p0, Lb/h;->c:I

    sub-int v2, v3, v2

    iput v2, p0, Lb/h;->c:I

    goto :goto_1
.end method

.method protected final a(Lb/y;I)V
    .locals 3

    invoke-virtual {p1}, Lb/y;->f()[B

    move-result-object v0

    invoke-virtual {p1}, Lb/y;->g()I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, Lb/h;->a([BII)V

    return-void
.end method

.method protected final a([BII)V
    .locals 1

    iput-object p1, p0, Lb/h;->a:[B

    const/4 v0, 0x0

    iput v0, p0, Lb/h;->b:I

    iput p2, p0, Lb/h;->c:I

    iput p3, p0, Lb/h;->f:I

    return-void
.end method

.method protected abstract a()Z
.end method

.method public final b()Ljava/nio/ByteBuffer;
    .locals 2

    iget v0, p0, Lb/h;->c:I

    iget v1, p0, Lb/h;->d:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/h;->g:Z

    iget-object v0, p0, Lb/h;->a:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iget v1, p0, Lb/h;->b:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/h;->g:Z

    iget-object v0, p0, Lb/h;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method protected final c()I
    .locals 1

    iget v0, p0, Lb/h;->f:I

    return v0
.end method

.method protected final d()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lb/h;->f:I

    return-void
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lb/h;->a()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lb/h;->a()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lb/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget v1, p0, Lb/h;->c:I

    if-eqz v1, :cond_1

    goto :goto_0
.end method
