Wireless Update

Tips

This Firmware Over-The-Air (FOTA) update may help your device run faster and more efficiently. Please take note of the following:

1. It is recommended that you download the update via a Wi-Fi connection.
2. Please ensure that you plug in the device to its charger while conducting the update.
3. Do not unplug or turn off the device or remove the battery until the update has been completed.
4. Your device may reboot several times during the update, this is part of the update process. 
5. If your device freezes or does not respond for a prolonged period, press and hold the power on/off button until the device shuts down.
