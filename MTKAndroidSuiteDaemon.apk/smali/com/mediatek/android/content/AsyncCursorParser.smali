.class public abstract Lcom/mediatek/android/content/AsyncCursorParser;
.super Ljava/lang/Object;
.source "AsyncCursorParser.java"


# instance fields
.field private mCount:I

.field private mCursor:Landroid/database/Cursor;

.field private mPosition:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    iput v0, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mPosition:I

    if-nez p1, :cond_0

    :goto_0
    iput v0, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCount:I

    return-void

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCount:I

    return v0
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getCursorPosition()I
    .locals 2

    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mPosition:I

    return v0
.end method

.method public abstract isBlockReady()Z
.end method

.method public moveToNext()Z
    .locals 2

    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 2

    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    goto :goto_0
.end method

.method protected abstract onBlockReady()V
.end method

.method protected abstract onBlockReadyForEx()V
.end method

.method protected abstract onNewRow(Landroid/database/Cursor;)V
.end method

.method protected abstract onParseOver()V
.end method

.method protected abstract onParseStart()V
.end method

.method public parse()V
    .locals 3

    const-string v1, "Parse begin..."

    invoke-static {v1}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_0

    const-string v1, "Curosr is null."

    invoke-static {v1}, Lcom/mediatek/apst/target/util/Debugger;->logW(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/android/content/AsyncCursorParser;->onParseStart()V

    :cond_1
    :goto_1
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mPosition:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mPosition:I

    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v1}, Lcom/mediatek/android/content/AsyncCursorParser;->onNewRow(Landroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/mediatek/android/content/AsyncCursorParser;->isBlockReady()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/android/content/AsyncCursorParser;->onBlockReady()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, ">>>>>>>>>>Catched IllegalStateException!"

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/android/content/AsyncCursorParser;->onBlockReadyForEx()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lcom/mediatek/android/content/AsyncCursorParser;->onParseOver()V

    const-string v1, "Parse finished."

    invoke-static {v1}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/android/content/AsyncCursorParser;->onParseOver()V

    const-string v1, "Parse finished."

    invoke-static {v1}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {p0}, Lcom/mediatek/android/content/AsyncCursorParser;->onParseOver()V

    const-string v2, "Parse finished."

    invoke-static {v2}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    throw v1
.end method

.method public resetCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    const-string v2, "Cursor Closed!"

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logD([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    iput-object p1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    iput v0, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mPosition:I

    if-nez p1, :cond_1

    :goto_0
    iput v0, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCount:I

    return-void

    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method protected setCursor(Landroid/database/Cursor;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/mediatek/android/content/AsyncCursorParser;->mCursor:Landroid/database/Cursor;

    return-void
.end method
