.class public interface abstract Lcom/mediatek/apst/target/data/proxy/contacts/USIMUtils$SimType;
.super Ljava/lang/Object;
.source "USIMUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/apst/target/data/proxy/contacts/USIMUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SimType"
.end annotation


# static fields
.field public static final SIM_TYPE_SIM:I = 0x0

.field public static final SIM_TYPE_USIM:I = 0x1

.field public static final SIM_TYPE_USIM_TAG:Ljava/lang/String; = "USIM"
