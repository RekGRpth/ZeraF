.class public final Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;
.super Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;
.source "CalendarProxy.java"


# static fields
.field private static sInstance:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->sInstance:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;-><init>(Landroid/content/Context;)V

    const-string v0, "CalendarProxy"

    invoke-virtual {p0, v0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->setProxyName(Ljava/lang/String;)V

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->sInstance:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    invoke-direct {v0, p0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->sInstance:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    :goto_0
    sget-object v0, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->sInstance:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->sInstance:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    invoke-virtual {v0, p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->setContext(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getMaxEventIdFromInsert()J
    .locals 9

    const-wide/16 v7, 0x1

    const/4 v6, 0x1

    const-wide/16 v0, 0x0

    new-instance v3, Landroid/content/ContentValues;

    const/4 v4, 0x7

    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    const-string v4, "calendar_id"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "title"

    const-string v5, "Test"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "dtstart"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "allDay"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "dtend"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eventTimezone"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "organizer"

    const-string v5, "Test"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v4

    sget-object v5, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v4

    sget-object v5, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMaxEventIdFromInsert: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    :cond_0
    return-wide v0
.end method


# virtual methods
.method public deleteAll()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, " 1 = 1 "

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, " 1 = 1 "

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, " 1 = 1 "

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, ">>>>Delete all end"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public deleteAttendee(J)I
    .locals 7
    .param p1    # J

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v1

    sget-object v2, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to delete Attendee, result is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-le v0, v5, :cond_0

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "Deleted several Attendees in one time, please check if it is normal."

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deleteAttendeesByEventId(JLjava/lang/String;)I
    .locals 6
    .param p1    # J
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v0, 0x0

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v1

    sget-object v2, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "attendeeEmail"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " <> \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v5}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    :goto_0
    if-gez v0, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to delete Attendee, result is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v1

    sget-object v2, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v5}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public deleteEvent(J)I
    .locals 10
    .param p1    # J

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v3

    sget-object v4, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v9}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to delete Event, result is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->deleteRemindersByEventId(J)I

    move-result v1

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete Reminders Count is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logV([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, v9}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->deleteAttendeesByEventId(JLjava/lang/String;)I

    move-result v0

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete Attendees Count is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logV([Ljava/lang/Object;Ljava/lang/String;)V

    return v2

    :cond_1
    if-le v2, v7, :cond_0

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v8

    const-string v4, "Deleted several Events in one time, please check if it is normal."

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deleteEvents([J)[Z
    .locals 9
    .param p1    # [J

    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x1

    if-nez p1, :cond_1

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v8

    const-string v6, "EventIds is null."

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v4

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    array-length v2, p1

    new-array v3, v2, [Z

    if-ne v2, v7, :cond_3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    aget-wide v4, p1, v1

    invoke-virtual {p0, v4, v5}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->deleteEvent(J)I

    move-result v0

    if-lt v0, v7, :cond_2

    aput-boolean v7, v3, v1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    if-le v2, v7, :cond_0

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v8

    const-string v6, ">>>>Delete all event start"

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v5

    sget-object v6, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4, v4}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v5

    sget-object v6, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4, v4}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v5

    sget-object v6, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4, v4}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v8

    const-string v5, ">>>>Delete all event end"

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    goto :goto_0
.end method

.method public deleteReminder(J)I
    .locals 7
    .param p1    # J

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v1

    sget-object v2, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to delete Reminder, result is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-le v0, v5, :cond_0

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "Deleted several Reminders in one time, please check if it is normal."

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deleteRemindersByEventId(J)I
    .locals 5
    .param p1    # J

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v1

    sget-object v2, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to delete Reminders, result is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return v0
.end method

.method public fastDeleteEvents([J)I
    .locals 8
    .param p1    # [J

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v0

    const-string v6, "List is null."

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "_id IN("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    :goto_1
    array-length v5, p1

    if-ge v2, v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v6, p1, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v3, v6}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public fastSyncAddEvents([B)[B
    .locals 1
    .param p1    # [B

    invoke-virtual {p0, p1}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->slowSyncAddEvents([B)[B

    move-result-object v0

    return-object v0
.end method

.method public fastSyncGetAllSyncFlags(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p2    # Ljava/nio/ByteBuffer;

    const/4 v0, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    aput-object p2, v0, v2

    const-string v1, "Block consumer should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "modifyTime"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "calendar_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "eventTimezone"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "dtstart"

    aput-object v4, v2, v3

    const-string v3, "deleted<>1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mediatek/apst/target/data/proxy/calendar/FastCalendarSyncFlagsCursorParser;

    invoke-direct {v7, v6, p1, p2}, Lcom/mediatek/apst/target/data/proxy/calendar/FastCalendarSyncFlagsCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v7}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_2
    throw v0
.end method

.method public fastSyncGetAttendees([JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 12
    .param p1    # [J
    .param p2    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p3    # Ljava/nio/ByteBuffer;

    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    if-nez p2, :cond_1

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    const-string v1, "Block consumer should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    const-string v1, "Requested events id list should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v11, v2, v2}, Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;->consume([BII)V

    :cond_2
    array-length v0, p1

    if-gtz v0, :cond_3

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    const-string v1, "Requested events id list is empty."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v11, v2, v2}, Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;->consume([BII)V

    :cond_3
    const/4 v6, 0x0

    const/4 v3, 0x0

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v0, "event_id IN("

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v7, 0x0

    :goto_1
    array-length v0, p1

    if-ge v7, v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v1, p1, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    const-string v0, ")"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "event_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "attendeeName"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "attendeeEmail"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "attendeeStatus"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "attendeeRelationship"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "attendeeType"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v8, Lcom/mediatek/apst/target/data/proxy/calendar/FastAttendeeCursorParser;

    invoke-direct {v8, v6, p2, p3}, Lcom/mediatek/apst/target/data/proxy/calendar/FastAttendeeCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v8}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_5
    throw v0
.end method

.method public fastSyncGetEvents([JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 12
    .param p1    # [J
    .param p2    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p3    # Ljava/nio/ByteBuffer;

    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    if-nez p2, :cond_1

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    const-string v1, "Block consumer should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    const-string v1, "Requested events id list should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v11, v2, v2}, Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;->consume([BII)V

    :cond_2
    array-length v0, p1

    if-gtz v0, :cond_3

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    const-string v1, "Requested events id list is empty."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v11, v2, v2}, Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;->consume([BII)V

    :cond_3
    const/4 v6, 0x0

    const/4 v3, 0x0

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v0, "(_id IN("

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v7, 0x0

    :goto_1
    array-length v0, p1

    if-ge v7, v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v1, p1, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    const-string v0, ")) AND deleted<>1"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/16 v2, 0x10

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "calendar_id"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "title"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "dtstart"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "dtend"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "allDay"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "eventLocation"

    aput-object v5, v2, v4

    const/4 v4, 0x7

    const-string v5, "description"

    aput-object v5, v2, v4

    const/16 v4, 0x8

    const-string v5, "organizer"

    aput-object v5, v2, v4

    const/16 v4, 0x9

    const-string v5, "rrule"

    aput-object v5, v2, v4

    const/16 v4, 0xa

    const-string v5, "createTime"

    aput-object v5, v2, v4

    const/16 v4, 0xb

    const-string v5, "modifyTime"

    aput-object v5, v2, v4

    const/16 v4, 0xc

    const-string v5, "duration"

    aput-object v5, v2, v4

    const/16 v4, 0xd

    const-string v5, "eventTimezone"

    aput-object v5, v2, v4

    const/16 v4, 0xe

    const-string v5, "availability"

    aput-object v5, v2, v4

    const/16 v4, 0xf

    const-string v5, "accessLevel"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v8, Lcom/mediatek/apst/target/data/proxy/calendar/FastEventCursorParser;

    invoke-direct {v8, v6, p2, p3}, Lcom/mediatek/apst/target/data/proxy/calendar/FastEventCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v8}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_5
    throw v0
.end method

.method public fastSyncGetReminders([JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 12
    .param p1    # [J
    .param p2    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p3    # Ljava/nio/ByteBuffer;

    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    if-nez p2, :cond_1

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    const-string v1, "Block consumer should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    const-string v1, "Requested events id list should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v11, v2, v2}, Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;->consume([BII)V

    :cond_2
    array-length v0, p1

    if-gtz v0, :cond_3

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    const-string v1, "Requested events id list is empty."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v11, v2, v2}, Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;->consume([BII)V

    :cond_3
    const/4 v6, 0x0

    const/4 v3, 0x0

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v0, "event_id IN("

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v7, 0x0

    :goto_1
    array-length v0, p1

    if-ge v7, v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v1, p1, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    const-string v0, ")"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "event_id"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "method"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "minutes"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v8, Lcom/mediatek/apst/target/data/proxy/calendar/FastReminderCursorParser;

    invoke-direct {v8, v6, p2, p3}, Lcom/mediatek/apst/target/data/proxy/calendar/FastReminderCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v8}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_5
    throw v0
.end method

.method public fastSyncUpdateEvents([B)[B
    .locals 15
    .param p1    # [B

    if-nez p1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Raw data is null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static/range {p1 .. p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    :try_start_0
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-gez v8, :cond_1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid events count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v9

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Can not get the events count in raw data "

    invoke-static {v0, v1, v9}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-array v14, v8, [J

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v8, :cond_5

    new-instance v11, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    invoke-direct {v11}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;-><init>()V

    const/16 v0, 0x51a

    invoke-virtual {v11, v6, v0}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-virtual {v11}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v12

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "deleted"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cursor is null. Failed to find the event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to update."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_3

    invoke-virtual {p0, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->insertEvent(Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;)J

    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    aput-wide v12, v14, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v0, v1, :cond_4

    invoke-virtual {p0, v12, v13}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->deleteEvent(J)I

    invoke-virtual {p0, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->insertEvent(Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;)J

    goto :goto_2

    :cond_4
    invoke-virtual {p0, v12, v13, v11}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->updateEvent(JLcom/mediatek/apst/util/entity/calendar/CalendarEvent;)I

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v14}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getSyncFlags([J)[B

    move-result-object v0

    goto/16 :goto_0
.end method

.method public getAttendees(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p2    # Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "event_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "attendeeName"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "attendeeEmail"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "attendeeStatus"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "attendeeRelationship"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "attendeeType"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mediatek/apst/target/data/proxy/calendar/FastAttendeeCursorParser;

    invoke-direct {v7, v6, p1, p2}, Lcom/mediatek/apst/target/data/proxy/calendar/FastAttendeeCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v7}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_1
    throw v0
.end method

.method public getCalendars(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p2    # Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "calendar_displayName"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "ownerAccount"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mediatek/apst/target/data/proxy/calendar/FastCalendarCursorParser;

    invoke-direct {v7, v6, p1, p2}, Lcom/mediatek/apst/target/data/proxy/calendar/FastCalendarCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v7}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_1
    throw v0
.end method

.method public getEvent(JZZ)Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;
    .locals 12
    .param p1    # J
    .param p3    # Z
    .param p4    # Z

    new-instance v10, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    invoke-direct {v10}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;-><init>()V

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/16 v2, 0x10

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "calendar_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "dtstart"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "dtend"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "allDay"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "eventLocation"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "description"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "organizer"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "rrule"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "modifyTime"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "createTime"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "duration"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "eventTimezone"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "availability"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "accessLevel"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v8}, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->cursorToEvent(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    move-result-object v10

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    if-nez p3, :cond_1

    move-object v0, v10

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Do not find the event. "

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "event_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "method"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "minutes"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_4

    :cond_2
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v11, Lcom/mediatek/apst/util/entity/calendar/Reminder;

    invoke-direct {v11}, Lcom/mediatek/apst/util/entity/calendar/Reminder;-><init>()V

    invoke-static {v9}, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->cursorToReminder(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/calendar/Reminder;

    move-result-object v11

    if-eqz v10, :cond_2

    invoke-virtual {v10, v11}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->addReminder(Lcom/mediatek/apst/util/entity/calendar/Reminder;)V

    goto :goto_1

    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_2
    if-nez p4, :cond_5

    move-object v0, v10

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Do not find the reminder. "

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "event_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "attendeeName"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "attendeeEmail"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "attendeeStatus"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "attendeeRelationship"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "attendeeType"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_8

    :cond_6
    :goto_3
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v6, Lcom/mediatek/apst/util/entity/calendar/Attendee;

    invoke-direct {v6}, Lcom/mediatek/apst/util/entity/calendar/Attendee;-><init>()V

    invoke-static {v7}, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->cursorToAttendee(Landroid/database/Cursor;)Lcom/mediatek/apst/util/entity/calendar/Attendee;

    move-result-object v6

    if-eqz v10, :cond_6

    invoke-virtual {v10, v6}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->addAttendee(Lcom/mediatek/apst/util/entity/calendar/Attendee;)V

    goto :goto_3

    :cond_7
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :goto_4
    move-object v0, v10

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Do not find the attendee. "

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public getEvents(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p2    # Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/16 v2, 0x10

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "calendar_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "dtstart"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "dtend"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "allDay"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "eventLocation"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "description"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "organizer"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "rrule"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "modifyTime"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "createTime"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "duration"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "eventTimezone"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "availability"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "accessLevel"

    aput-object v4, v2, v3

    const-string v3, "deleted<>1"

    const/4 v4, 0x0

    const-string v5, "_id ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mediatek/apst/target/data/proxy/calendar/FastEventCursorParser;

    invoke-direct {v7, v6, p1, p2}, Lcom/mediatek/apst/target/data/proxy/calendar/FastEventCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v7}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_1
    throw v0
.end method

.method public getLastSyncDate()J
    .locals 7

    const-wide/16 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/apst/target/util/SharedPrefs;->open(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "calendar_last_sync_date"

    const-wide/16 v5, 0x0

    invoke-interface {v3, v4, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :goto_0
    return-wide v1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getLocalAccountId()J
    .locals 10

    const/4 v4, 0x0

    const/4 v9, 0x0

    const-wide/16 v7, 0x0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v9

    const-string v3, "account_type = \'local\' or account_type = \'LOCAL\'"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    return-wide v7
.end method

.method public getMaxEventId()J
    .locals 10

    const/4 v3, 0x0

    const/4 v9, 0x0

    const-wide/16 v7, 0x0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v9

    const-string v5, "_id DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getMaxEventId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    return-wide v7
.end method

.method public getPcSyncEventsCount()I
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v3

    const-string v3, "deleted<>1"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_0
    return v7

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public getReminders(Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p2    # Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "event_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "method"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "minutes"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "minutes ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mediatek/apst/target/data/proxy/calendar/FastReminderCursorParser;

    invoke-direct {v7, v6, p1, p2}, Lcom/mediatek/apst/target/data/proxy/calendar/FastReminderCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v7}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_1
    throw v0
.end method

.method public getSyncFlags(JJ)[B
    .locals 9
    .param p1    # J
    .param p3    # J

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v6

    const/4 v0, 0x4

    new-array v8, v0, [B

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "modifyTime"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "calendar_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "eventTimezone"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "dtstart"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id>="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "<="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "deleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "<>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "_id ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    invoke-virtual {v6}, Ljava/nio/Buffer;->clear()Ljava/nio/Buffer;

    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSyncFlags count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const/4 v0, 0x5

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :goto_1
    invoke-virtual {v6}, Ljava/nio/Buffer;->flip()Ljava/nio/Buffer;

    invoke-virtual {v6}, Ljava/nio/Buffer;->limit()I

    move-result v0

    new-array v8, v0, [B

    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    return-object v8

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "cursor is null"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getSyncFlags([J)[B
    .locals 14
    .param p1    # [J

    if-nez p1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Target ID list is null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    new-array v13, v0, [B

    :goto_0
    return-object v13

    :cond_0
    array-length v0, p1

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Target ID list is empty."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    new-array v13, v0, [B

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v0, "("

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "_id IN("

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v8, 0x0

    :goto_1
    array-length v0, p1

    if-ge v8, v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v1, p1, v8

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    const-string v0, ")) AND deleted<>1"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v6

    const/4 v0, 0x4

    new-array v13, v0, [B

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "modifyTime"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "calendar_id"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "title"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "eventTimezone"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "dtstart"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const-string v5, "_id ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    invoke-virtual {v6}, Ljava/nio/Buffer;->clear()Ljava/nio/Buffer;

    array-length v0, p1

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v11, 0x0

    if-eqz v7, :cond_5

    :cond_3
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    :goto_3
    array-length v0, p1

    if-gt v11, v0, :cond_3

    array-length v0, p1

    if-ne v11, v0, :cond_6

    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    :goto_4
    array-length v0, p1

    if-ge v11, v0, :cond_b

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    :cond_6
    const-wide/16 v0, 0x0

    aget-wide v4, p1, v11

    cmp-long v0, v0, v4

    if-ltz v0, :cond_8

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    :cond_7
    :goto_5
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :cond_8
    aget-wide v0, p1, v11

    cmp-long v0, v9, v0

    if-nez v0, :cond_9

    invoke-virtual {v6, v9, v10}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const/4 v0, 0x5

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto :goto_5

    :cond_9
    aget-wide v0, p1, v11

    cmp-long v0, v9, v0

    if-lez v0, :cond_a

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v6, v0}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const-wide/16 v0, -0x1

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto :goto_5

    :cond_a
    aget-wide v0, p1, v11

    cmp-long v0, v9, v0

    if-gez v0, :cond_7

    goto/16 :goto_2

    :cond_b
    invoke-virtual {v6}, Ljava/nio/Buffer;->flip()Ljava/nio/Buffer;

    invoke-virtual {v6}, Ljava/nio/Buffer;->limit()I

    move-result v0

    new-array v13, v0, [B

    invoke-virtual {v6, v13}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    goto/16 :goto_0
.end method

.method public insertAllEvents(Ljava/util/List;)J
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;",
            ">;)J"
        }
    .end annotation

    if-nez p1, :cond_0

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object p1, v20, v21

    const-string v21, "eventList is null."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v20, -0x1

    :goto_0
    return-wide v20

    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getMaxEventId()J

    move-result-wide v20

    const-wide/16 v22, 0x1

    add-long v7, v20, v22

    new-instance v10, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$4;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$4;-><init>(Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;)V

    new-instance v4, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$5;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$5;-><init>(Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;)V

    new-instance v15, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$6;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$6;-><init>(Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;)V

    const/16 v18, 0x0

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v9, :cond_e

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "----->count is "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    new-instance v19, Lcom/mediatek/android/content/MeasuredContentValues;

    const/16 v20, 0x14

    invoke-direct/range {v19 .. v20}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v20, "_id"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "newEvent.getId() "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v20, "calendar_id"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarId()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v20, "title"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTitle()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "dtstart"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeFrom()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v21, "allDay"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->isAllDay()Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v20, 0x1

    :goto_2
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v20, "eventLocation"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getEventLocation()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "description"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDescription()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "organizer"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarOwner()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "rrule"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getRepetition()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "modifyTime"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getModifyTime()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v20, "createTime"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCreateTime()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v20, "duration"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDuration()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDuration()Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_3

    const-string v21, "dtend"

    const/16 v20, 0x0

    check-cast v20, Ljava/lang/Long;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_3
    const-string v20, "guestsCanModify"

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getReminders()Ljava/util/List;

    move-result-object v16

    if-eqz v16, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v20

    if-lez v20, :cond_6

    const-string v20, "hasAlarm"

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/mediatek/apst/util/entity/calendar/Reminder;

    new-instance v17, Lcom/mediatek/android/content/MeasuredContentValues;

    const/16 v20, 0x3

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v20, "event_id"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "----->Reminder.COLUMN_EVENT_ID is "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v20, "method"

    invoke-virtual {v14}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->getMethod()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v20, "minutes"

    invoke-virtual {v14}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->getMinutes()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->append(Lcom/mediatek/android/content/MeasuredContentValues;)Z

    move-result v20

    if-nez v20, :cond_5

    const/16 v18, 0x1

    :goto_4
    if-eqz v18, :cond_1

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Error in bulk inserting reminders, statusCode: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v15}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v20, 0x0

    goto/16 :goto_0

    :cond_2
    const/16 v20, 0x0

    goto/16 :goto_2

    :cond_3
    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeTo()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    if-nez v20, :cond_4

    const-string v20, "dtend"

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    :cond_4
    const-string v20, "dtend"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeTo()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_3

    :cond_5
    const/16 v18, 0x0

    goto :goto_4

    :cond_6
    const-string v20, "hasAlarm"

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_7
    const-string v20, "hasExtendedProperties"

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAttendees()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_a

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v20

    if-lez v20, :cond_a

    const-string v20, "hasAttendeeData"

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_8
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_b

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/apst/util/entity/calendar/Attendee;

    new-instance v6, Lcom/mediatek/android/content/MeasuredContentValues;

    const/16 v20, 0x6

    move/from16 v0, v20

    invoke-direct {v6, v0}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v20, "event_id"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "event_id = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->getId()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v20, "attendeeName"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeName()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "attendeeEmail"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeEmail()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "attendeeStatus"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeStatus()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v20, "attendeeRelationship"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeRelationShip()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v20, "attendeeType"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeType()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v4, v6}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->append(Lcom/mediatek/android/content/MeasuredContentValues;)Z

    move-result v20

    if-nez v20, :cond_9

    const/16 v18, 0x1

    :goto_5
    if-eqz v18, :cond_8

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Error in bulk inserting atendees, statusCode: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v4}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v20, 0x0

    goto/16 :goto_0

    :cond_9
    const/16 v18, 0x0

    goto :goto_5

    :cond_a
    const-string v20, "hasAttendeeData"

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_b
    const-string v20, "eventTimezone"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeZone()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "availability"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAvailability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v20, "accessLevel"

    invoke-virtual {v13}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAccessLevel()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->append(Lcom/mediatek/android/content/MeasuredContentValues;)Z

    move-result v20

    if-nez v20, :cond_c

    const/16 v18, 0x1

    :goto_6
    if-eqz v18, :cond_d

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Error in bulk inserting events, statusCode: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v10}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v20, 0x0

    goto/16 :goto_0

    :cond_c
    const/16 v18, 0x0

    goto :goto_6

    :cond_d
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    :cond_e
    invoke-virtual {v10}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->execute()Z

    move-result v20

    if-nez v20, :cond_f

    const/16 v18, 0x1

    :goto_7
    if-eqz v18, :cond_10

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Error in bulk inserting events, statusCode: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v10}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v20, 0x0

    goto/16 :goto_0

    :cond_f
    const/16 v18, 0x0

    goto :goto_7

    :cond_10
    invoke-virtual {v15}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->execute()Z

    move-result v20

    if-nez v20, :cond_11

    const/16 v18, 0x1

    :goto_8
    if-eqz v18, :cond_12

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Error in bulk inserting reminders, statusCode: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v15}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v20, 0x0

    goto/16 :goto_0

    :cond_11
    const/16 v18, 0x0

    goto :goto_8

    :cond_12
    invoke-virtual {v4}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->execute()Z

    move-result v20

    if-nez v20, :cond_13

    const/16 v18, 0x1

    :goto_9
    if-eqz v18, :cond_14

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Error in bulk inserting attendees, statusCode: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v4}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v20, 0x0

    goto/16 :goto_0

    :cond_13
    const/16 v18, 0x0

    goto :goto_9

    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getMaxEventId()J

    move-result-wide v20

    sub-long v20, v20, v7

    const-wide/16 v22, 0x1

    add-long v20, v20, v22

    goto/16 :goto_0
.end method

.method public insertAttendee(Lcom/mediatek/apst/util/entity/calendar/Attendee;J)J
    .locals 10
    .param p1    # Lcom/mediatek/apst/util/entity/calendar/Attendee;
    .param p2    # J

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    if-nez p1, :cond_0

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p1, v5, v7

    const-string v6, "Attendee passed in is null."

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v1, -0x1

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x6

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v5, "event_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "attendeeName"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "attendeeEmail"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeEmail()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "attendeeStatus"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeStatus()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "attendeeRelationship"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeRelationShip()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "attendeeType"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeType()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v5

    sget-object v6, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v1

    :cond_1
    :goto_1
    invoke-virtual {p1, v1, v2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->setId(J)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p1, v5, v7

    invoke-static {v5, v9, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_1
    move-exception v0

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p1, v5, v7

    invoke-static {v5, v9, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public insertEvent(Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;)J
    .locals 14
    .param p1    # Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x1

    if-nez p1, :cond_1

    new-array v8, v10, [Ljava/lang/Object;

    aput-object p1, v8, v11

    const-string v9, "Event passed in is null."

    invoke-static {v8, v9}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v3, -0x1

    :cond_0
    return-wide v3

    :cond_1
    const-wide/16 v3, -0x1

    new-instance v7, Landroid/content/ContentValues;

    const/16 v8, 0x13

    invoke-direct {v7, v8}, Landroid/content/ContentValues;-><init>(I)V

    const-string v8, "calendar_id"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarId()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "title"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "dtstart"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeFrom()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v12, "allDay"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->isAllDay()Z

    move-result v8

    if-eqz v8, :cond_3

    move v8, v10

    :goto_0
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v12, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "eventLocation"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getEventLocation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "description"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDescription()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "organizer"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarOwner()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "rrule"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getRepetition()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "modifyTime"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getModifyTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "createTime"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCreateTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "duration"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDuration()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDuration()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    const-string v12, "dtend"

    move-object v8, v9

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v7, v12, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_1
    const-string v8, "guestsCanModify"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getReminders()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getReminders()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_6

    const-string v8, "hasAlarm"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_2
    const-string v8, "hasExtendedProperties"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAttendees()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAttendees()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_7

    const-string v8, "hasAttendeeData"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_3
    const-string v8, "eventTimezone"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeZone()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "availability"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAvailability()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "accessLevel"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAccessLevel()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v8

    sget-object v12, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v12, v7}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v3

    :cond_2
    :goto_4
    invoke-virtual {p1, v3, v4}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->setId(J)V

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getReminders()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/apst/util/entity/calendar/Reminder;

    invoke-virtual {p0, v5, v3, v4}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->insertReminder(Lcom/mediatek/apst/util/entity/calendar/Reminder;J)J

    goto :goto_5

    :cond_3
    move v8, v11

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeTo()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    if-nez v8, :cond_5

    const-string v8, "dtend"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    :cond_5
    const-string v8, "dtend"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeTo()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1

    :cond_6
    const-string v8, "hasAlarm"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_2

    :cond_7
    const-string v8, "hasAttendeeData"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    :catch_0
    move-exception v1

    new-array v8, v10, [Ljava/lang/Object;

    aput-object p1, v8, v11

    invoke-static {v8, v9, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :catch_1
    move-exception v1

    new-array v8, v10, [Ljava/lang/Object;

    aput-object p1, v8, v11

    invoke-static {v8, v9, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAttendees()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/calendar/Attendee;

    invoke-virtual {p0, v0, v3, v4}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->insertAttendee(Lcom/mediatek/apst/util/entity/calendar/Attendee;J)J

    goto :goto_6
.end method

.method public insertReminder(Lcom/mediatek/apst/util/entity/calendar/Reminder;J)J
    .locals 11
    .param p1    # Lcom/mediatek/apst/util/entity/calendar/Reminder;
    .param p2    # J

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    if-nez p1, :cond_0

    new-array v5, v9, [Ljava/lang/Object;

    aput-object p1, v5, v8

    const-string v6, "Reminder passed in is null."

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v1, -0x1

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v5, "event_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "method"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->getMethod()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "minutes"

    invoke-virtual {p1}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->getMinutes()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v5

    sget-object v6, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v1

    :cond_1
    :goto_1
    invoke-virtual {p1, v1, v2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->setId(J)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-array v5, v9, [Ljava/lang/Object;

    aput-object p1, v5, v8

    invoke-static {v5, v10, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_1
    move-exception v0

    new-array v5, v9, [Ljava/lang/Object;

    aput-object p1, v5, v8

    invoke-static {v5, v10, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public isSyncAble()Z
    .locals 10

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v7, -0x1

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarContent;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "sync_events"

    aput-object v3, v2, v9

    const-string v3, "account_type = \'local\' or account_type = \'LOCAL\'"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    if-ne v7, v8, :cond_2

    move v0, v8

    :goto_0
    return v0

    :cond_2
    move v0, v9

    goto :goto_0
.end method

.method public isSyncNeedReinit()Z
    .locals 5

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/apst/target/util/SharedPrefs;->open(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "calendar_sync_need_reinit"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public slowSyncAddEvents([B)[B
    .locals 26
    .param p1    # [B

    if-nez p1, :cond_0

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p1, v22, v23

    const-string v23, "Raw data is null."

    invoke-static/range {v22 .. v23}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v22, 0x0

    :goto_0
    return-object v22

    :cond_0
    invoke-static/range {p1 .. p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    :try_start_0
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    if-gez v10, :cond_1

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p1, v22, v23

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Invalid events count "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v22, 0x0

    goto :goto_0

    :catch_0
    move-exception v11

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p1, v22, v23

    const-string v23, "Can not get the events count in raw data "

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v11}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v22, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getMaxEventId()J

    move-result-wide v22

    const-wide/16 v24, 0x1

    add-long v7, v22, v24

    const-wide/16 v22, 0x1

    cmp-long v22, v7, v22

    if-nez v22, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getMaxEventIdFromInsert()J

    move-result-wide v22

    const-wide/16 v24, 0x1

    add-long v7, v22, v24

    :cond_2
    new-instance v12, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v12, v0, v1}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$1;-><init>(Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;[B)V

    new-instance v4, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v1}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$2;-><init>(Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;[B)V

    new-instance v17, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$3;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy$3;-><init>(Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;[B)V

    const/16 v20, 0x0

    const/4 v13, 0x0

    :goto_1
    if-ge v13, v10, :cond_10

    new-instance v15, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    invoke-direct {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;-><init>()V

    const/16 v22, 0x51a

    move/from16 v0, v22

    invoke-virtual {v15, v9, v0}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    new-instance v21, Lcom/mediatek/android/content/MeasuredContentValues;

    const/16 v22, 0x13

    invoke-direct/range {v21 .. v22}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v22, "calendar_id"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarId()J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v22, "title"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTitle()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v22, "dtstart"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeFrom()J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v23, "allDay"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->isAllDay()Z

    move-result v22

    if-eqz v22, :cond_4

    const/16 v22, 0x1

    :goto_2
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v22, "eventLocation"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getEventLocation()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v22, "description"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDescription()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v22, "organizer"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarOwner()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v22, "rrule"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getRepetition()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v22, "modifyTime"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getModifyTime()J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v22, "createTime"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCreateTime()J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v22, "duration"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDuration()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDuration()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_5

    const-string v23, "dtend"

    const/16 v22, 0x0

    check-cast v22, Ljava/lang/Long;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_3
    const-string v22, "guestsCanModify"

    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getReminders()Ljava/util/List;

    move-result-object v18

    if-eqz v18, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v22

    if-lez v22, :cond_8

    const-string v22, "hasAlarm"

    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/mediatek/apst/util/entity/calendar/Reminder;

    new-instance v19, Lcom/mediatek/android/content/MeasuredContentValues;

    const/16 v22, 0x3

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v22, "event_id"

    int-to-long v0, v13

    move-wide/from16 v23, v0

    add-long v23, v23, v7

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v22, "method"

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->getMethod()J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v22, "minutes"

    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->getMinutes()J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->append(Lcom/mediatek/android/content/MeasuredContentValues;)Z

    move-result v22

    if-nez v22, :cond_7

    const/16 v20, 0x1

    :goto_4
    if-eqz v20, :cond_3

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p1, v22, v23

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error in bulk inserting reminders, statusCode: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v22, 0x0

    goto/16 :goto_0

    :cond_4
    const/16 v22, 0x0

    goto/16 :goto_2

    :cond_5
    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeTo()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    if-nez v22, :cond_6

    const-string v22, "dtend"

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    :cond_6
    const-string v22, "dtend"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeTo()J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_3

    :cond_7
    const/16 v20, 0x0

    goto :goto_4

    :cond_8
    const-string v22, "hasAlarm"

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_9
    const-string v22, "hasExtendedProperties"

    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAttendees()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_c

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v22

    if-lez v22, :cond_c

    const-string v22, "hasAttendeeData"

    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_a
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_d

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/apst/util/entity/calendar/Attendee;

    new-instance v6, Lcom/mediatek/android/content/MeasuredContentValues;

    const/16 v22, 0x6

    move/from16 v0, v22

    invoke-direct {v6, v0}, Lcom/mediatek/android/content/MeasuredContentValues;-><init>(I)V

    const-string v22, "event_id"

    int-to-long v0, v13

    move-wide/from16 v23, v0

    add-long v23, v23, v7

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v22, "attendeeName"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeName()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v22, "attendeeEmail"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeEmail()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v22, "attendeeStatus"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeStatus()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v22, "attendeeRelationship"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeRelationShip()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v22, "attendeeType"

    invoke-virtual {v3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeType()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v4, v6}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->append(Lcom/mediatek/android/content/MeasuredContentValues;)Z

    move-result v22

    if-nez v22, :cond_b

    const/16 v20, 0x1

    :goto_5
    if-eqz v20, :cond_a

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p1, v22, v23

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error in bulk inserting atendees, statusCode: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v4}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v22, 0x0

    goto/16 :goto_0

    :cond_b
    const/16 v20, 0x0

    goto :goto_5

    :cond_c
    const-string v22, "hasAttendeeData"

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_d
    const-string v22, "eventTimezone"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeZone()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v22, "availability"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAvailability()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v22, "accessLevel"

    invoke-virtual {v15}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAccessLevel()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lcom/mediatek/android/content/MeasuredContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->append(Lcom/mediatek/android/content/MeasuredContentValues;)Z

    move-result v22

    if-nez v22, :cond_e

    const/16 v20, 0x1

    :goto_6
    if-eqz v20, :cond_f

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p1, v22, v23

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error in bulk inserting events, statusCode: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v12}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v22, 0x0

    goto/16 :goto_0

    :cond_e
    const/16 v20, 0x0

    goto :goto_6

    :cond_f
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    :cond_10
    invoke-virtual {v12}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->execute()Z

    move-result v22

    if-nez v22, :cond_11

    const/16 v20, 0x1

    :goto_7
    if-eqz v20, :cond_12

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p1, v22, v23

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error in bulk inserting events, statusCode: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v12}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v22, 0x0

    goto/16 :goto_0

    :cond_11
    const/16 v20, 0x0

    goto :goto_7

    :cond_12
    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->execute()Z

    move-result v22

    if-nez v22, :cond_13

    const/16 v20, 0x1

    :goto_8
    if-eqz v20, :cond_14

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p1, v22, v23

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error in bulk inserting reminders, statusCode: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v22, 0x0

    goto/16 :goto_0

    :cond_13
    const/16 v20, 0x0

    goto :goto_8

    :cond_14
    invoke-virtual {v4}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->execute()Z

    move-result v22

    if-nez v22, :cond_15

    const/16 v20, 0x1

    :goto_9
    if-eqz v20, :cond_16

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p1, v22, v23

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error in bulk inserting attendees, statusCode: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v4}, Lcom/mediatek/android/content/DefaultBulkInsertHelper;->getStatusCode()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v22, 0x0

    goto/16 :goto_0

    :cond_15
    const/16 v20, 0x0

    goto :goto_9

    :cond_16
    int-to-long v0, v10

    move-wide/from16 v22, v0

    add-long v22, v22, v7

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-virtual {v0, v7, v8, v1, v2}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->getSyncFlags(JJ)[B

    move-result-object v22

    goto/16 :goto_0
.end method

.method public slowSyncGetAllAttendees(JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1    # J
    .param p3    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p4    # Ljava/nio/ByteBuffer;

    const/4 v0, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p3, :cond_1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v2

    aput-object p3, v0, v3

    aput-object p4, v0, v4

    const-string v1, "Block consumer should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "event_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "attendeeName"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "attendeeEmail"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "attendeeStatus"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "attendeeRelationship"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "attendeeType"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id<="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mediatek/apst/target/data/proxy/calendar/FastAttendeeCursorParser;

    invoke-direct {v7, v6, p3, p4}, Lcom/mediatek/apst/target/data/proxy/calendar/FastAttendeeCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v7}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_2
    throw v0
.end method

.method public slowSyncGetAllEvents(JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1    # J
    .param p3    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p4    # Ljava/nio/ByteBuffer;

    const/4 v0, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-nez p3, :cond_1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v3

    aput-object p3, v0, v2

    aput-object p4, v0, v4

    const-string v1, "Block consumer should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    const/16 v2, 0x10

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "calendar_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "dtstart"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "dtend"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "allDay"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "eventLocation"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "description"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "organizer"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "rrule"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "createTime"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "modifyTime"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "duration"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "eventTimezone"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "availability"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "accessLevel"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id<="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "deleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "<>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mediatek/apst/target/data/proxy/calendar/FastEventCursorParser;

    invoke-direct {v7, v6, p3, p4}, Lcom/mediatek/apst/target/data/proxy/calendar/FastEventCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v7}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_2
    throw v0
.end method

.method public slowSyncGetAllReminders(JLcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1    # J
    .param p3    # Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;
    .param p4    # Ljava/nio/ByteBuffer;

    const/4 v0, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p3, :cond_1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v2

    aput-object p3, v0, v3

    aput-object p4, v0, v4

    const-string v1, "Block consumer should not be null."

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "event_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "method"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "minutes"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id<="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mediatek/apst/target/data/proxy/calendar/FastReminderCursorParser;

    invoke-direct {v7, v6, p3, p4}, Lcom/mediatek/apst/target/data/proxy/calendar/FastReminderCursorParser;-><init>(Landroid/database/Cursor;Lcom/mediatek/apst/target/data/proxy/IRawBlockConsumer;Ljava/nio/ByteBuffer;)V

    invoke-virtual {v7}, Lcom/mediatek/android/content/AsyncCursorParser;->parse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_2
    throw v0
.end method

.method public updateAttendee(JLcom/mediatek/apst/util/entity/calendar/Attendee;)I
    .locals 6
    .param p1    # J
    .param p3    # Lcom/mediatek/apst/util/entity/calendar/Attendee;

    const/4 v0, 0x0

    if-nez p3, :cond_0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const-string v3, "New attendee passed in is null."

    invoke-static {v2, v3}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x6

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "event_id"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getEventId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "attendeeName"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "attendeeEmail"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeEmail()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "attendeeStatus"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeStatus()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "attendeeRelationship"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeRelationShip()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "attendeeType"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v2

    sget-object v3, Lcom/mediatek/apst/target/data/provider/calendar/AttendeeContent;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public updateEvent(JLcom/mediatek/apst/util/entity/calendar/CalendarEvent;)I
    .locals 11
    .param p1    # J
    .param p3    # Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    if-nez p3, :cond_1

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p3, v8, v9

    const-string v9, "New event passed in is null."

    invoke-static {v8, v9}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    :cond_0
    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x0

    new-instance v7, Landroid/content/ContentValues;

    const/16 v8, 0x10

    invoke-direct {v7, v8}, Landroid/content/ContentValues;-><init>(I)V

    const-string v8, "calendar_id"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarId()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "title"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "dtstart"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeFrom()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "allDay"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->isAllDay()Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x1

    :goto_1
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "eventLocation"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getEventLocation()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "description"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "organizer"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarOwner()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "rrule"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getRepetition()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "modifyTime"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getModifyTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "createTime"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCreateTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "duration"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDuration()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getDuration()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    const-string v9, "dtend"

    const/4 v8, 0x0

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v7, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_2
    const-string v8, "eventTimezone"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeZone()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "availability"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAvailability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "accessLevel"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAccessLevel()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "guestsCanModify"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarOwner()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, p1, p2, v8}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->deleteAttendeesByEventId(JLjava/lang/String;)I

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getAttendees()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_6

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/calendar/Attendee;

    invoke-virtual {v0}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->getAttendeeEmail()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getCalendarOwner()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    invoke-virtual {p0, v0, p1, p2}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->insertAttendee(Lcom/mediatek/apst/util/entity/calendar/Attendee;J)J

    goto :goto_3

    :cond_2
    const/4 v8, 0x0

    goto/16 :goto_1

    :cond_3
    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeTo()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    if-nez v8, :cond_4

    const-string v8, "dtend"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    :cond_4
    const-string v8, "dtend"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getTimeTo()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_2

    :cond_5
    const-string v8, "The calender owner will not be inserted, because it is not deleted."

    invoke-static {v8}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    const-string v8, "attendeeList size is 0."

    invoke-static {v8}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    :cond_7
    :goto_4
    invoke-virtual {p0, p1, p2}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->deleteRemindersByEventId(J)I

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->getReminders()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_b

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_9

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/apst/util/entity/calendar/Reminder;

    invoke-virtual {p0, v3, p1, p2}, Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;->insertReminder(Lcom/mediatek/apst/util/entity/calendar/Reminder;J)J

    goto :goto_5

    :cond_8
    const-string v8, "attendeeList is null."

    invoke-static {v8}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    const-string v8, "attendeeList size is not 1."

    invoke-static {v8}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    :cond_a
    :goto_6
    sget-object v8, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v6, v7, v9, v10}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_c

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to update event, eventId is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", updateCount is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v8, "reminderList is null"

    invoke-static {v8}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    goto :goto_6

    :cond_c
    const/4 v8, 0x1

    if-le v5, v8, :cond_0

    const-string v8, "Updated several event in one time, please check if it is normal."

    invoke-static {v8}, Lcom/mediatek/apst/target/util/Debugger;->logW(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public updateReminder(JLcom/mediatek/apst/util/entity/calendar/Reminder;)I
    .locals 6
    .param p1    # J
    .param p3    # Lcom/mediatek/apst/util/entity/calendar/Reminder;

    const/4 v0, 0x0

    if-nez p3, :cond_0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const-string v3, "New reminder passed in is null."

    invoke-static {v2, v3}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "event_id"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->getEventId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "method"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->getMethod()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "minutes"

    invoke-virtual {p3}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->getMinutes()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getObservedContentResolver()Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;

    move-result-object v2

    sget-object v3, Lcom/mediatek/apst/target/data/provider/calendar/ReminderContent;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Lcom/mediatek/apst/target/data/proxy/ObservedContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public updateSyncDate(J)Z
    .locals 3
    .param p1    # J

    invoke-virtual {p0}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/apst/target/util/SharedPrefs;->open(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "calendar_last_sync_date"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "calendar_sync_need_reinit"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, 0x1

    return v0
.end method
