.class public Lcom/mediatek/apst/target/ftp/FtpConnection;
.super Ljava/lang/Thread;
.source "FtpConnection.java"


# static fields
.field private static final BUFFER_SIZE:I = 0x19000

.field private static final DATA_PORT:I = 0x4da6

.field private static final FTP_CMD_CDUP:Ljava/lang/String; = "CDUP"

.field private static final FTP_CMD_CWD:Ljava/lang/String; = "CWD"

.field private static final FTP_CMD_DELE:Ljava/lang/String; = "DELE"

.field private static final FTP_CMD_FEAT:Ljava/lang/String; = "FEAT"

.field private static final FTP_CMD_LIST:Ljava/lang/String; = "LIST"

.field private static final FTP_CMD_MKD:Ljava/lang/String; = "MKD"

.field private static final FTP_CMD_NOOP:Ljava/lang/String; = "NOOP"

.field private static final FTP_CMD_OPTS:Ljava/lang/String; = "OPTS"

.field private static final FTP_CMD_PASS:Ljava/lang/String; = "PASS"

.field private static final FTP_CMD_PASV:Ljava/lang/String; = "PASV"

.field private static final FTP_CMD_PORT:Ljava/lang/String; = "PORT"

.field private static final FTP_CMD_PWD:Ljava/lang/String; = "PWD"

.field private static final FTP_CMD_QUIT:Ljava/lang/String; = "QUIT"

.field private static final FTP_CMD_RETR:Ljava/lang/String; = "RETR"

.field private static final FTP_CMD_RMD:Ljava/lang/String; = "RMD"

.field private static final FTP_CMD_RNFR:Ljava/lang/String; = "RNFR"

.field private static final FTP_CMD_RNTO:Ljava/lang/String; = "RNTO"

.field private static final FTP_CMD_SITE:Ljava/lang/String; = "SITE"

.field private static final FTP_CMD_SIZE:Ljava/lang/String; = "SIZE"

.field private static final FTP_CMD_STOR:Ljava/lang/String; = "STOR"

.field private static final FTP_CMD_SYST:Ljava/lang/String; = "SYST"

.field private static final FTP_CMD_TYPE:Ljava/lang/String; = "TYPE"

.field private static final FTP_CMD_USER:Ljava/lang/String; = "USER"

.field private static final LOGGABLE:Z = false

.field private static final MS_IN_SIX_MONTHS:J = -0x61075000L

.field private static final PASSWORD:Ljava/lang/String; = "APST"

.field private static final TAG:[Ljava/lang/Object;

.field private static final TIME_OUT:I = 0x1770

.field private static final USERNAME:Ljava/lang/String; = "APST"


# instance fields
.field private mBinaryMode:Z

.field private mCurrentDir:Ljava/lang/String;

.field private mFtpCharset:Ljava/lang/String;

.field private final mHost:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mPasvSocket:Ljava/net/ServerSocket;

.field private mPort:I

.field private mReader:Ljava/io/BufferedReader;

.field private mRenameFrom:Ljava/io/File;

.field private mSocket:Ljava/net/Socket;

.field private mUserName:Ljava/lang/String;

.field private mWriter:Ljava/io/BufferedOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "APST/FTP"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/net/Socket;)V
    .locals 3
    .param p1    # Ljava/net/Socket;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const-string v0, "/"

    iput-object v0, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    iput-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    iput-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    iput-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    const-string v0, "127.0.0.1"

    iput-object v0, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mHost:Ljava/lang/String;

    const/16 v0, 0x4da6

    iput v0, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPort:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mBinaryMode:Z

    iput-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mUserName:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPassword:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mRenameFrom:Ljava/io/File;

    iput-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mFtpCharset:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    sget-object v0, Lcom/mediatek/apst/target/ftp/FtpService;->sFtpEncoding:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mFtpCharset:Ljava/lang/String;

    sget-object v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FtpCharset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mFtpCharset:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method private ftpLog([Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/Object;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method private inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2f

    if-ne v2, v3, :cond_0

    new-instance v0, Ljava/io/File;

    const-string v2, "/"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private parseCommand(Ljava/lang/String;)V
    .locals 53
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    const-string v50, ""

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v48

    const-string v50, "USER"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_2

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mUserName:Ljava/lang/String;

    const-string v50, "331 Send password\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v50, "PASS"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_4

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPassword:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mUserName:Ljava/lang/String;

    move-object/from16 v50, v0

    const-string v51, "APST"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPassword:Ljava/lang/String;

    move-object/from16 v50, v0

    const-string v51, "APST"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_3

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Username Password Correct!"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v50, "230 Access granted\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v50, "530 Login incorrect.\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const-string v50, "QUIT"

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_5

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "QUIT"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v50, "221 Goodbye\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v50, "TYPE"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_a

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "TYPE executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const-string v50, "I"

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_6

    const-string v50, "L 8"

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_7

    :cond_6
    const/16 v50, 0x1

    move/from16 v0, v50

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mBinaryMode:Z

    const-string v33, "200 Binary type set\r\n"

    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "TYPE complete"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v50, "A"

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_8

    const-string v50, "A N"

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_9

    :cond_8
    const/16 v50, 0x0

    move/from16 v0, v50

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mBinaryMode:Z

    const-string v33, "200 ASCII type set\r\n"

    goto :goto_1

    :cond_9
    const-string v33, "503 Malformed TYPE command\r\n"

    goto :goto_1

    :cond_a
    const-string v50, "OPTS"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_10

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const/16 v16, 0x0

    if-nez v34, :cond_b

    const-string v16, "550 Need argument to OPTS\r\n"

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Couldn\'t understand empty OPTS command"

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logD([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    if-eqz v16, :cond_f

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Template log message"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v50, " "

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, v40

    array-length v0, v0

    move/from16 v50, v0

    const/16 v51, 0x2

    move/from16 v0, v50

    move/from16 v1, v51

    if-eq v0, v1, :cond_c

    const-string v16, "550 Malformed OPTS command\r\n"

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Couldn\'t parse OPTS command"

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logD([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_c
    const/16 v50, 0x0

    aget-object v50, v40, v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v29

    const/16 v50, 0x1

    aget-object v50, v40, v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v30

    const-string v50, "UTF8"

    move-object/from16 v0, v29

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_e

    const-string v50, "ON"

    move-object/from16 v0, v30

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_d

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Got OPTS UTF8 ON"

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_d
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Ignoring OPTS UTF8 for something besides ON"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_e
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Unrecognized OPTS option: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, v51

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v16, "502 Unrecognized option\r\n"

    goto/16 :goto_2

    :cond_f
    const-string v50, "504 OPTS accepted\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Handled OPTS ok"

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    const-string v50, "SYST"

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_11

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "SYST executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v50, "215 UNIX Type: L8\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "SYST finished"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    const-string v50, "NOOP"

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_12

    const-string v50, "200 NOOP OK\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_12
    const-string v50, "CWD"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_15

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "CWD executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const/16 v16, 0x0

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "WorkingDir ---------------->"

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v52, v0

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Param ---------------->"

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, v51

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v50, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_13

    const-string v16, "550 Invalid name or chroot violation\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_3
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "CWD complete"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    :try_start_0
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->canExecute()Z

    move-result v50

    if-eqz v50, :cond_14

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    const-string v50, "250 CWD successful\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v14

    const-string v50, "550 Invalid path\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_3

    :cond_14
    :try_start_1
    const-string v50, "550 That path is inaccessible\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_15
    const-string v50, "CDUP"

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_1a

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "CDUP executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v16, 0x0

    new-instance v49, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v50, v0

    invoke-direct/range {v49 .. v50}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v27

    if-nez v27, :cond_16

    const-string v16, "550 Current dir cannot find parent\r\n"

    :goto_4
    if-eqz v16, :cond_19

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "CDUP error: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, v51

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_16
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_17

    const-string v16, "550 Invalid name or chroot violation\r\n"

    goto :goto_4

    :cond_17
    :try_start_2
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->canRead()Z

    move-result v50

    if-eqz v50, :cond_18

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    :catch_1
    move-exception v14

    const-string v16, "550 Invalid path\r\n"

    goto :goto_4

    :cond_18
    :try_start_3
    const-string v16, "550 That path is inaccessible\r\n"
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    :cond_19
    const-string v50, "200 CDUP successful\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "CDUP success"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1a
    const-string v50, "SIZE"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_21

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "SIZE executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v16, 0x0

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const-wide/16 v38, 0x0

    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v50, v0

    move-object/from16 v0, v50

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v44, 0x0

    sget-object v50, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_1b

    new-instance v44, Ljava/io/File;

    move-object/from16 v0, v44

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->isDirectory()Z

    move-result v50

    if-eqz v50, :cond_1c

    const-string v16, "550 No directory traversal allowed in SIZE param\r\n"

    :goto_5
    if-eqz v16, :cond_20

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    :goto_6
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "SIZE complete"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1b
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v12, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v44

    :cond_1c
    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_1d

    const-string v16, "550 SIZE target violates chroot\r\n"

    goto :goto_5

    :cond_1d
    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->exists()Z

    move-result v50

    if-nez v50, :cond_1e

    const-string v16, "550 Cannot get the SIZE of nonexistent object\r\n"

    goto :goto_5

    :cond_1e
    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->isFile()Z

    move-result v50

    if-nez v50, :cond_1f

    const-string v16, "550 Cannot get the size of a non-file\r\n"

    goto :goto_5

    :cond_1f
    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->length()J

    move-result-wide v38

    goto :goto_5

    :cond_20
    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "213 "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, "\r\n"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_6

    :cond_21
    const-string v50, "PWD"

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_23

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "PWD executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_4
    new-instance v50, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v50 .. v50}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v12

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "currentDir: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, v51

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v50

    if-nez v50, :cond_22

    const-string v12, "/"

    :cond_22
    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "257 \""

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, "\"\r\n"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_7
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "PWD complete"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_2
    move-exception v14

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "PWD canonicalize"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_7

    :cond_23
    const-string v50, "PORT"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V

    const/16 v50, 0x0

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    :cond_24
    const/16 v16, 0x0

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const-string v50, "|"

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_25

    const-string v50, "::"

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v50

    if-eqz v50, :cond_25

    const-string v16, "550 No IPv6 support, reconfigure your client\r\n"

    :goto_8
    if-nez v16, :cond_2c

    const-string v50, "200 PORT OK\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "PORT completed"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_25
    const-string v50, ","

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v43

    array-length v0, v0

    move/from16 v50, v0

    const/16 v51, 0x6

    move/from16 v0, v50

    move/from16 v1, v51

    if-eq v0, v1, :cond_26

    const-string v16, "550 Malformed PORT argument\r\n"

    goto :goto_8

    :cond_26
    const/16 v20, 0x0

    :goto_9
    move-object/from16 v0, v43

    array-length v0, v0

    move/from16 v50, v0

    move/from16 v0, v20

    move/from16 v1, v50

    if-ge v0, v1, :cond_29

    aget-object v50, v43, v20

    const-string v51, "[0-9]+"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_27

    aget-object v50, v43, v20

    invoke-virtual/range {v50 .. v50}, Ljava/lang/String;->length()I

    move-result v50

    const/16 v51, 0x3

    move/from16 v0, v50

    move/from16 v1, v51

    if-le v0, v1, :cond_28

    :cond_27
    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "550 Invalid PORT argument: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    aget-object v51, v43, v20

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, "\r\n"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto :goto_8

    :cond_28
    add-int/lit8 v20, v20, 0x1

    goto :goto_9

    :cond_29
    const/16 v50, 0x4

    move/from16 v0, v50

    new-array v0, v0, [B

    move-object/from16 v25, v0

    const/16 v20, 0x0

    :goto_a
    const/16 v50, 0x4

    move/from16 v0, v20

    move/from16 v1, v50

    if-ge v0, v1, :cond_2b

    :try_start_5
    aget-object v50, v43, v20

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    const/16 v50, 0x80

    move/from16 v0, v24

    move/from16 v1, v50

    if-lt v0, v1, :cond_2a

    move/from16 v0, v24

    add-int/lit16 v0, v0, -0x100

    move/from16 v24, v0

    :cond_2a
    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v50, v0

    aput-byte v50, v25, v20
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_3

    add-int/lit8 v20, v20, 0x1

    goto :goto_a

    :catch_3
    move-exception v14

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "550 Invalid PORT format: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    aget-object v51, v43, v20

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, "\r\n"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_8

    :cond_2b
    const/16 v50, 0x4

    aget-object v50, v43, v50

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v50

    move/from16 v0, v50

    mul-int/lit16 v0, v0, 0x100

    move/from16 v50, v0

    const/16 v51, 0x5

    aget-object v51, v43, v51

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v51

    add-int v50, v50, v51

    move/from16 v0, v50

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPort:I

    goto/16 :goto_8

    :cond_2c
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "PORT error: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, v51

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2d
    const-string v50, "PASV"

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_2e

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "PasvSocket is not null: pasvSocket.close()"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V

    const/16 v50, 0x0

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    :cond_2e
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "PASV mode begin"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x1

    const/4 v9, 0x0

    move v10, v9

    :goto_b
    const-wide/16 v50, 0x64

    :try_start_6
    invoke-static/range {v50 .. v51}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/net/BindException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_17

    add-int/lit8 v9, v10, 0x1

    const/16 v50, 0x32

    move/from16 v0, v50

    if-le v10, v0, :cond_30

    :cond_2f
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v4

    if-nez v4, :cond_31

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "PASV IP string invalid"

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v50, "502 Couldn\'t open a port\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v14

    :goto_c
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "PASV Exception: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V

    const/16 v50, 0x0

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    goto/16 :goto_0

    :cond_30
    :try_start_8
    new-instance v50, Ljava/net/ServerSocket;

    const/16 v51, 0x4da6

    const/16 v52, 0x32

    invoke-direct/range {v50 .. v52}, Ljava/net/ServerSocket;-><init>(II)V

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    const/4 v6, 0x0

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "PasvSocket: New Pasv ServerSocket! Data Port: 19878"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/net/BindException; {:try_start_8 .. :try_end_8} :catch_19
    .catch Ljava/net/SocketException; {:try_start_8 .. :try_end_8} :catch_18
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_4

    :goto_d
    if-eqz v6, :cond_2f

    move v10, v9

    goto/16 :goto_b

    :catch_5
    move-exception v14

    move v9, v10

    :goto_e
    :try_start_9
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "BindException: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x1

    goto :goto_d

    :catch_6
    move-exception v14

    move v9, v10

    :goto_f
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "SocketException"

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x1

    goto :goto_d

    :cond_31
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "PASV sending IP: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v35, Ljava/lang/StringBuilder;

    const-string v50, "227 Entering Passive Mode ("

    move-object/from16 v0, v35

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v50

    const/16 v51, 0x2e

    const/16 v52, 0x2c

    invoke-virtual/range {v50 .. v52}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v35

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v50, ","

    move-object/from16 v0, v35

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v50, 0x4d

    move-object/from16 v0, v35

    move/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v50, ","

    move-object/from16 v0, v35

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v50, 0xa6

    move-object/from16 v0, v35

    move/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v50, ").\r\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_0

    :cond_32
    const-string v50, "RETR"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_45

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "RETR executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v13

    :goto_10
    const/16 v50, 0x1770

    move/from16 v0, v50

    invoke-virtual {v13, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    new-instance v50, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v18

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Download file dir = "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_36

    const-string v16, "550 Invalid name or chroot violation\r\n"

    :cond_33
    :goto_11
    if-eqz v16, :cond_43

    :try_start_a
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :goto_12
    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V

    :goto_13
    const/16 v50, 0x0

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    :cond_34
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "RETR done"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_35
    new-instance v13, Ljava/net/Socket;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v50, "127.0.0.1"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPort:I

    move/from16 v51, v0

    move-object/from16 v0, v50

    move/from16 v1, v51

    invoke-direct {v13, v0, v1}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "dataSocket: New Data Socket!"

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_10

    :cond_36
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->isDirectory()Z

    move-result v50

    if-eqz v50, :cond_37

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Ignoring RETR for directory"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v16, "550 Can\'t RETR a directory\r\n"

    goto :goto_11

    :cond_37
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v50

    if-nez v50, :cond_38

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Can\'t RETR nonexistent file: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v16, "550 File does not exist\r\n"

    goto/16 :goto_11

    :cond_38
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->canRead()Z

    move-result v50

    if-nez v50, :cond_39

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Failed RETR permission (canRead() is false)"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v16, "550 No read permissions\r\n"

    goto/16 :goto_11

    :cond_39
    const/16 v21, 0x0

    :try_start_b
    new-instance v22, Ljava/io/FileInputStream;

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_16
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_15
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    const v50, 0x19000

    :try_start_c
    move/from16 v0, v50

    new-array v7, v0, [B

    const-string v50, "150 Sending file\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mBinaryMode:Z

    move/from16 v50, v0

    if-eqz v50, :cond_3b

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Transferring in binary mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_3a
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/16 v50, -0x1

    move/from16 v0, v50

    if-eq v8, v0, :cond_7d

    const/16 v50, 0x0

    invoke-virtual {v13}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v51

    move-object/from16 v0, p0

    move/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v7, v1, v8, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->sendViaDataSocket([BIILjava/io/OutputStream;)Z

    move-result v50

    if-nez v50, :cond_3a

    const-string v16, "426 Data socket error\r\n"

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Data socket error"

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_7
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    if-eqz v22, :cond_7e

    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    :goto_14
    const/16 v21, 0x0

    goto/16 :goto_11

    :cond_3b
    :try_start_d
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Transferring in ASCII mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v26, 0x0

    :goto_15
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/16 v50, -0x1

    move/from16 v0, v50

    if-eq v8, v0, :cond_7d

    const/16 v41, 0x0

    const/4 v15, 0x0

    const/16 v50, 0x2

    move/from16 v0, v50

    new-array v11, v0, [B

    fill-array-data v11, :array_0

    new-instance v50, Ljava/lang/String;

    move-object/from16 v0, v50

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>([B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mFtpCharset:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    array-length v8, v7

    const/4 v15, 0x0

    :goto_16
    if-ge v15, v8, :cond_40

    aget-byte v50, v7, v15

    const/16 v51, 0xa

    move/from16 v0, v50

    move/from16 v1, v51

    if-ne v0, v1, :cond_3d

    sub-int v50, v15, v41

    invoke-virtual {v13}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v51

    move-object/from16 v0, p0

    move/from16 v1, v41

    move/from16 v2, v50

    move-object/from16 v3, v51

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->sendViaDataSocket([BIILjava/io/OutputStream;)Z

    if-nez v15, :cond_3e

    if-nez v26, :cond_3c

    const/16 v50, 0x0

    const/16 v51, 0x1

    invoke-virtual {v13}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v52

    move-object/from16 v0, p0

    move/from16 v1, v50

    move/from16 v2, v51

    move-object/from16 v3, v52

    invoke-direct {v0, v11, v1, v2, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->sendViaDataSocket([BIILjava/io/OutputStream;)Z

    :cond_3c
    :goto_17
    move/from16 v41, v15

    :cond_3d
    add-int/lit8 v15, v15, 0x1

    goto :goto_16

    :cond_3e
    add-int/lit8 v50, v15, -0x1

    aget-byte v50, v7, v50

    const/16 v51, 0xd

    move/from16 v0, v50

    move/from16 v1, v51

    if-eq v0, v1, :cond_3f

    const/16 v50, 0x0

    const/16 v51, 0x1

    invoke-virtual {v13}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v52

    move-object/from16 v0, p0

    move/from16 v1, v50

    move/from16 v2, v51

    move-object/from16 v3, v52

    invoke-direct {v0, v11, v1, v2, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->sendViaDataSocket([BIILjava/io/OutputStream;)Z
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_7
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    goto :goto_17

    :catch_7
    move-exception v14

    move-object/from16 v21, v22

    :goto_18
    :try_start_e
    const-string v16, "550 File not found\r\n"
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    if-eqz v21, :cond_33

    invoke-virtual/range {v21 .. v21}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_14

    :cond_3f
    :try_start_f
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "parseCommand"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_7
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    goto :goto_17

    :catch_8
    move-exception v14

    move-object/from16 v21, v22

    :goto_19
    :try_start_10
    const-string v16, "425 Network error\r\n"
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    if-eqz v21, :cond_33

    invoke-virtual/range {v21 .. v21}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_14

    :cond_40
    sub-int v50, v15, v41

    :try_start_11
    invoke-virtual {v13}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v51

    move-object/from16 v0, p0

    move/from16 v1, v41

    move/from16 v2, v50

    move-object/from16 v3, v51

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->sendViaDataSocket([BIILjava/io/OutputStream;)Z

    add-int/lit8 v50, v8, -0x1

    aget-byte v50, v7, v50
    :try_end_11
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_7
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_8
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    const/16 v51, 0xd

    move/from16 v0, v50

    move/from16 v1, v51

    if-ne v0, v1, :cond_41

    const/16 v26, 0x1

    goto/16 :goto_15

    :cond_41
    const/16 v26, 0x0

    goto/16 :goto_15

    :catchall_0
    move-exception v50

    :goto_1a
    if-eqz v21, :cond_42

    invoke-virtual/range {v21 .. v21}, Ljava/io/FileInputStream;->close()V

    const/16 v21, 0x0

    :cond_42
    throw v50

    :cond_43
    :try_start_12
    const-string v50, "226 Transmission finished\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_9
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    goto/16 :goto_12

    :catch_9
    move-exception v14

    :try_start_13
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Exception: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V

    goto/16 :goto_13

    :catchall_1
    move-exception v50

    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v51, v0

    if-eqz v51, :cond_44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v51, v0

    invoke-virtual/range {v51 .. v51}, Ljava/net/ServerSocket;->close()V

    const/16 v51, 0x0

    move-object/from16 v0, v51

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    :cond_44
    throw v50

    :cond_45
    const-string v50, "STOR"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_58

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_49

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v13

    :goto_1b
    const/16 v50, 0x1770

    move/from16 v0, v50

    invoke-virtual {v13, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "STOR/APPE executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    new-instance v50, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v42

    const/16 v16, 0x0

    const/16 v31, 0x0

    :try_start_14
    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_4a

    const-string v16, "550 Invalid name or chroot violation\r\n"

    :goto_1c
    if-eqz v16, :cond_57

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "STOR error: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_e
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :goto_1d
    if-eqz v13, :cond_46

    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    :cond_46
    if-eqz v31, :cond_47

    invoke-virtual/range {v31 .. v31}, Ljava/io/FileOutputStream;->close()V

    const/16 v31, 0x0

    :cond_47
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_48

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V

    :goto_1e
    const/16 v50, 0x0

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    :cond_48
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "STOR finished"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_49
    new-instance v13, Ljava/net/Socket;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v50, "127.0.0.1"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPort:I

    move/from16 v51, v0

    move-object/from16 v0, v50

    move/from16 v1, v51

    invoke-direct {v13, v0, v1}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "dataSocket: New Data Socket!"

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1b

    :cond_4a
    :try_start_15
    invoke-virtual/range {v42 .. v42}, Ljava/io/File;->isDirectory()Z

    move-result v50

    if-eqz v50, :cond_4b

    const-string v16, "451 Can\'t overwrite a directory\r\n"
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_e
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    goto/16 :goto_1c

    :cond_4b
    :try_start_16
    invoke-virtual/range {v42 .. v42}, Ljava/io/File;->exists()Z

    move-result v50

    if-eqz v50, :cond_4c

    if-nez v5, :cond_4c

    invoke-virtual/range {v42 .. v42}, Ljava/io/File;->delete()Z

    move-result v50

    if-nez v50, :cond_4c

    const-string v16, "451 Couldn\'t truncate file\r\n"

    goto/16 :goto_1c

    :cond_4c
    new-instance v32, Ljava/io/FileOutputStream;

    move-object/from16 v0, v32

    move-object/from16 v1, v42

    invoke-direct {v0, v1, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_16
    .catch Ljava/io/FileNotFoundException; {:try_start_16 .. :try_end_16} :catch_b
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_e
    .catchall {:try_start_16 .. :try_end_16} :catchall_4

    :try_start_17
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Data socket ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v50, "150 Data socket ready\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    const v50, 0x19000

    move/from16 v0, v50

    new-array v7, v0, [B

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mBinaryMode:Z

    move/from16 v50, v0

    if-eqz v50, :cond_4e

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Mode is binary"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1f
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v13}, Lcom/mediatek/apst/target/ftp/FtpConnection;->receiveFromDataSocket([BLjava/net/Socket;)I
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    move-result v28

    packed-switch v28, :pswitch_data_0

    :try_start_18
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mBinaryMode:Z

    move/from16 v50, v0

    if-eqz v50, :cond_51

    const/16 v50, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v50

    move/from16 v2, v28

    invoke-virtual {v0, v7, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    :cond_4d
    :goto_20
    invoke-virtual/range {v32 .. v32}, Ljava/io/OutputStream;->flush()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_a
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    goto :goto_1f

    :catch_a
    move-exception v14

    :try_start_19
    const-string v16, "451 File IO problem. Device might be full.\r\n"

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Exception while storing: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_d
    .catchall {:try_start_19 .. :try_end_19} :catchall_2

    move-object/from16 v31, v32

    goto/16 :goto_1c

    :catch_b
    move-exception v14

    :try_start_1a
    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "451 Couldn\'t open file \""

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, "\" aka \""

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v42 .. v42}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v51

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, "\" for writing\r\n"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_c
    .catchall {:try_start_1a .. :try_end_1a} :catchall_4

    move-result-object v16

    goto/16 :goto_1c

    :catch_c
    move-exception v23

    :try_start_1b
    const-string v16, "451 Couldn\'t open file, nested exception\r\n"
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_e
    .catchall {:try_start_1b .. :try_end_1b} :catchall_4

    goto/16 :goto_1c

    :cond_4e
    :try_start_1c
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Mode is ascii"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_d
    .catchall {:try_start_1c .. :try_end_1c} :catchall_2

    goto/16 :goto_1f

    :catch_d
    move-exception v14

    move-object/from16 v31, v32

    :goto_21
    :try_start_1d
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "IOException: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_4

    if-eqz v13, :cond_4f

    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    :cond_4f
    if-eqz v31, :cond_50

    invoke-virtual/range {v31 .. v31}, Ljava/io/FileOutputStream;->close()V

    const/16 v31, 0x0

    :cond_50
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_48

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V

    goto/16 :goto_1e

    :pswitch_0
    :try_start_1e
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Returned from final read"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v31, v32

    goto/16 :goto_1c

    :pswitch_1
    const-string v16, "426 Couldn\'t receive data\r\n"

    move-object/from16 v31, v32

    goto/16 :goto_1c

    :pswitch_2
    const-string v16, "425 Could not connect data socket\r\n"
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_d
    .catchall {:try_start_1e .. :try_end_1e} :catchall_2

    move-object/from16 v31, v32

    goto/16 :goto_1c

    :cond_51
    const/16 v41, 0x0

    :try_start_1f
    new-instance v50, Ljava/lang/String;

    move-object/from16 v0, v50

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>([B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mFtpCharset:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    array-length v0, v7

    move/from16 v28, v0

    const/4 v15, 0x0

    :goto_22
    move/from16 v0, v28

    if-ge v15, v0, :cond_53

    aget-byte v50, v7, v15

    const/16 v51, 0xd

    move/from16 v0, v50

    move/from16 v1, v51

    if-ne v0, v1, :cond_52

    sub-int v50, v15, v41

    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v50

    invoke-virtual {v0, v7, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    add-int/lit8 v41, v15, 0x1

    :cond_52
    add-int/lit8 v15, v15, 0x1

    goto :goto_22

    :cond_53
    move/from16 v0, v41

    move/from16 v1, v28

    if-ge v0, v1, :cond_4d

    sub-int v50, v15, v41

    move-object/from16 v0, v32

    move/from16 v1, v41

    move/from16 v2, v50

    invoke-virtual {v0, v7, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_a
    .catchall {:try_start_1f .. :try_end_1f} :catchall_2

    goto/16 :goto_20

    :catchall_2
    move-exception v50

    move-object/from16 v31, v32

    :goto_23
    if-eqz v13, :cond_54

    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    :cond_54
    if-eqz v31, :cond_55

    invoke-virtual/range {v31 .. v31}, Ljava/io/FileOutputStream;->close()V

    const/16 v31, 0x0

    :cond_55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v51, v0

    if-eqz v51, :cond_56

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v51, v0

    invoke-virtual/range {v51 .. v51}, Ljava/net/ServerSocket;->close()V

    const/16 v51, 0x0

    move-object/from16 v0, v51

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    :cond_56
    throw v50

    :cond_57
    :try_start_20
    const-string v50, "226 Transmission complete\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_e
    .catchall {:try_start_20 .. :try_end_20} :catchall_4

    goto/16 :goto_1d

    :catch_e
    move-exception v14

    goto/16 :goto_21

    :cond_58
    const-string v50, "LIST"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_5f

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_5a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v13

    :goto_24
    const/16 v50, 0x1770

    move/from16 v0, v50

    invoke-virtual {v13, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    const-string v50, "150 Opening ASCII mode data connection\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    :try_start_21
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/mediatek/apst/target/ftp/FtpConnection;->responseList(Ljava/lang/String;Ljava/net/Socket;)V

    const-string v50, "226 transfer complete\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V
    :try_end_21
    .catch Ljava/net/SocketTimeoutException; {:try_start_21 .. :try_end_21} :catch_f
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_11
    .catchall {:try_start_21 .. :try_end_21} :catchall_3

    if-eqz v13, :cond_59

    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    :cond_59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_0

    :try_start_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_14

    :goto_25
    const/16 v50, 0x0

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    goto/16 :goto_0

    :cond_5a
    new-instance v13, Ljava/net/Socket;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v50, "127.0.0.1"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPort:I

    move/from16 v51, v0

    move-object/from16 v0, v50

    move/from16 v1, v51

    invoke-direct {v13, v0, v1}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "dataSocket: New Data Socket!"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_24

    :catch_f
    move-exception v37

    :try_start_23
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "LIST SocketTimeoutException: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v37 .. v37}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_3

    if-eqz v13, :cond_5b

    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    :cond_5b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_0

    :try_start_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_10

    goto :goto_25

    :catch_10
    move-exception v14

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_25

    :catch_11
    move-exception v14

    :try_start_25
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "LIST IOException: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_3

    if-eqz v13, :cond_5c

    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    :cond_5c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    if-eqz v50, :cond_0

    :try_start_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Ljava/net/ServerSocket;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_12

    goto/16 :goto_25

    :catch_12
    move-exception v14

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_25

    :catchall_3
    move-exception v50

    if-eqz v13, :cond_5d

    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    const/4 v13, 0x0

    :cond_5d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v51, v0

    if-eqz v51, :cond_5e

    :try_start_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    move-object/from16 v51, v0

    invoke-virtual/range {v51 .. v51}, Ljava/net/ServerSocket;->close()V
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_27} :catch_13

    :goto_26
    const/16 v51, 0x0

    move-object/from16 v0, v51

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    :cond_5e
    throw v50

    :cond_5f
    const-string v50, "DELE"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_64

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "DELE executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    new-instance v50, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v42

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_61

    const-string v16, "550 Invalid name or chroot violation\r\n"

    :cond_60
    :goto_27
    if-eqz v16, :cond_63

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "DELE failed: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_28
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "DELE finished"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_61
    invoke-virtual/range {v42 .. v42}, Ljava/io/File;->isDirectory()Z

    move-result v50

    if-eqz v50, :cond_62

    const-string v16, "550 Can\'t DELE a directory\r\n"

    goto :goto_27

    :cond_62
    invoke-virtual/range {v42 .. v42}, Ljava/io/File;->delete()Z

    move-result v50

    if-nez v50, :cond_60

    const-string v16, "450 Error deleting file\r\n"

    goto :goto_27

    :cond_63
    const-string v50, "250 File successfully deleted\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_28

    :cond_64
    const-string v50, "MKD"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_6a

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "MKD executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const/16 v16, 0x0

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->length()I

    move-result v50

    const/16 v51, 0x1

    move/from16 v0, v50

    move/from16 v1, v51

    if-ge v0, v1, :cond_66

    const-string v16, "550 Invalid name\r\n"

    :cond_65
    :goto_29
    if-eqz v16, :cond_69

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "MKD error: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2a
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "MKD complete"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_66
    new-instance v50, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v45

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_67

    const-string v16, "550 Invalid name or chroot violation\r\n"

    goto :goto_29

    :cond_67
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->exists()Z

    move-result v50

    if-eqz v50, :cond_68

    const-string v16, "550 Already exists\r\n"

    goto :goto_29

    :cond_68
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->mkdir()Z

    move-result v50

    if-nez v50, :cond_65

    const-string v16, "550 Error making directory (permissions?)\r\n"

    goto :goto_29

    :cond_69
    const-string v50, "250 Directory created\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_2a

    :cond_6a
    const-string v50, "RNFR"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_6e

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const/16 v16, 0x0

    const/16 v17, 0x0

    new-instance v50, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_6c

    const-string v16, "550 Invalid name or chroot violation\r\n"

    :cond_6b
    :goto_2b
    if-eqz v16, :cond_6d

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "RNFR failed: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->setRenameFrom(Ljava/io/File;)V

    goto/16 :goto_0

    :cond_6c
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v50

    if-nez v50, :cond_6b

    const-string v16, "450 Cannot rename nonexistent file\r\n"

    goto :goto_2b

    :cond_6d
    const-string v50, "350 Filename noted, now send RNTO\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->setRenameFrom(Ljava/io/File;)V

    goto/16 :goto_0

    :cond_6e
    const-string v50, "RNTO"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_73

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const/16 v16, 0x0

    const/16 v46, 0x0

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "RNTO executing\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "param: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, v51

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v50, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v46

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "RNTO parsed: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v46 .. v46}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_70

    const-string v16, "550 Invalid name or chroot violation\r\n"

    :cond_6f
    :goto_2c
    if-eqz v16, :cond_72

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "RNFR failed: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2d
    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->setRenameFrom(Ljava/io/File;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "RNTO finished"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_70
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getRenameFrom()Ljava/io/File;

    move-result-object v19

    if-nez v19, :cond_71

    const-string v16, "550 Rename error, maybe RNFR not sent\r\n"

    goto :goto_2c

    :cond_71
    move-object/from16 v0, v19

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v50

    if-nez v50, :cond_6f

    const-string v16, "550 Error during rename operation\r\n"

    goto :goto_2c

    :cond_72
    const-string v50, "250 rename successful\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_2d

    :cond_73
    const-string v50, "RMD"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_7a

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "RMD executing"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v50, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v34

    const/16 v16, 0x0

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->length()I

    move-result v50

    const/16 v51, 0x1

    move/from16 v0, v50

    move/from16 v1, v51

    if-ge v0, v1, :cond_75

    const-string v16, "550 Invalid argument\r\n"

    :cond_74
    :goto_2e
    if-eqz v16, :cond_79

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "RMD failed: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2f
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "RMD finished"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_75
    new-instance v50, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->inputPathToChrootedFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v47

    move-object/from16 v0, p0

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v50

    if-eqz v50, :cond_76

    const-string v16, "550 Invalid name or chroot violation\r\n"

    goto :goto_2e

    :cond_76
    invoke-virtual/range {v47 .. v47}, Ljava/io/File;->isDirectory()Z

    move-result v50

    if-nez v50, :cond_77

    const-string v16, "550 Can\'t RMD a non-directory\r\n"

    goto :goto_2e

    :cond_77
    new-instance v50, Ljava/io/File;

    const-string v51, "/"

    invoke-direct/range {v50 .. v51}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v47

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_78

    const-string v16, "550 Won\'t RMD the root directory\r\n"

    goto :goto_2e

    :cond_78
    move-object/from16 v0, p0

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->recursiveDelete(Ljava/io/File;)Z

    move-result v50

    if-nez v50, :cond_74

    const-string v16, "550 Deletion error, possibly incomplete\r\n"

    goto/16 :goto_2e

    :cond_79
    const-string v50, "250 Removed directory\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_2f

    :cond_7a
    const-string v50, "FEAT"

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_7b

    const-string v50, "211-Features supported\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    const-string v50, " UTF8\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    const-string v50, "211 End\r\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7b
    const-string v50, "SITE"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v50

    if-eqz v50, :cond_7c

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v51, "Command: site"

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "200 invalid command:"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, "\r\n"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7c
    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "---------------->Invalid Command: "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, v51

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "200 invalid command:"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    const-string v51, "\r\n"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7d
    if-eqz v22, :cond_7e

    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_14

    :catch_13
    move-exception v14

    sget-object v51, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v52

    invoke-static/range {v51 .. v52}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_26

    :catch_14
    move-exception v14

    sget-object v50, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v50 .. v51}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_25

    :catchall_4
    move-exception v50

    goto/16 :goto_23

    :catchall_5
    move-exception v50

    move-object/from16 v21, v22

    goto/16 :goto_1a

    :catch_15
    move-exception v14

    goto/16 :goto_19

    :catch_16
    move-exception v14

    goto/16 :goto_18

    :catch_17
    move-exception v14

    move v9, v10

    goto/16 :goto_c

    :catch_18
    move-exception v14

    goto/16 :goto_f

    :catch_19
    move-exception v14

    goto/16 :goto_e

    :cond_7e
    move-object/from16 v21, v22

    goto/16 :goto_11

    nop

    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private receiveFromDataSocket([BLjava/net/Socket;)I
    .locals 7
    .param p1    # [B
    .param p2    # Ljava/net/Socket;

    const/4 v3, -0x1

    const/4 v0, -0x2

    const/4 v4, 0x0

    if-nez p2, :cond_1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v4, "Can\'t receive from null dataSocket"

    invoke-direct {p0, v3, v4}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2}, Ljava/net/Socket;->isConnected()Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v4, "Can\'t receive from unconnected socket"

    invoke-direct {p0, v3, v4}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {p2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    :goto_1
    const/4 v5, 0x0

    array-length v6, p1

    invoke-virtual {v2, p1, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    if-nez v0, :cond_3

    sget-object v5, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v6, "receiveFromDataSocket"

    invoke-direct {p0, v5, v6}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SocketTimeoutException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v5}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move v0, v4

    goto :goto_0

    :cond_3
    if-ne v0, v3, :cond_0

    move v0, v3

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v5, "Error reading data socket"

    invoke-direct {p0, v3, v5}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move v0, v4

    goto :goto_0
.end method

.method private response(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mFtpCharset:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    invoke-virtual {v2, v1}, Ljava/io/OutputStream;->write([B)V

    iget-object v2, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V

    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported encoding: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mFtpCharset:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    goto :goto_0
.end method

.method private responseList(Ljava/lang/String;Ljava/net/Socket;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/net/Socket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v9, 0x0

    invoke-virtual {p0, p1, v9}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    sget-object v6, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "List parameter: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    const-string v6, "-"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    sget-object v6, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LIST is skipping dashed arg "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v4, v9}, Lcom/mediatek/apst/target/ftp/FtpConnection;->getParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mCurrentDir:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->listDirectory(Ljava/lang/StringBuilder;Ljava/io/File;)Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v6, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mFtpCharset:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v6, v0

    invoke-virtual {p2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-direct {p0, v0, v9, v6, v7}, Lcom/mediatek/apst/target/ftp/FtpConnection;->sendViaDataSocket([BIILjava/io/OutputStream;)Z

    :goto_2
    return-void

    :cond_2
    const-string v6, "*"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "550 LIST does not support wildcards\r\n"

    invoke-direct {p0, v6}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->violatesChroot(Ljava/io/File;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "450 Listing target violates chroot\r\n"

    invoke-direct {p0, v6}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_2

    :cond_4
    invoke-virtual {p0, v1}, Lcom/mediatek/apst/target/ftp/FtpConnection;->makeLsString(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    const-string v6, "450 Couldn\'t list that file\r\n"

    invoke-direct {p0, v6}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method

.method private sendViaDataSocket([BIILjava/io/OutputStream;)Z
    .locals 5
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/io/OutputStream;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez p4, :cond_0

    sget-object v2, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v3, "Can\'t send via null dataOutputStream"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    if-nez p3, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {p4, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    invoke-virtual {p4}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move v1, v2

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SocketTimeoutException when sendViaDataSocket: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v2, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v3, "Couldn\'t write output stream for data socket"

    invoke-direct {p0, v2, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public closeSocket()V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException: socket "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getParameter(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    if-nez p1, :cond_1

    const-string v1, ""

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/16 v2, 0x20

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    const-string v1, ""

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\s+$"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez p2, :cond_0

    sget-object v2, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parsed argument: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getRenameFrom()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mRenameFrom:Ljava/io/File;

    return-object v0
.end method

.method public listDirectory(Ljava/lang/StringBuilder;Ljava/io/File;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # Ljava/io/File;

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "500 Internal error, listDirectory on non-directory\r\n"

    :goto_0
    return-object v6

    :cond_0
    sget-object v6, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Listing directory: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v6, "500 Couldn\'t list directory. Check config and mount status.\r\n"

    goto :goto_0

    :cond_1
    sget-object v6, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Dir len "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v2

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_3

    aget-object v3, v0, v4

    invoke-virtual {p0, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->makeLsString(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method

.method protected makeLsString(Ljava/io/File;)Ljava/lang/String;
    .locals 14
    .param p1    # Ljava/io/File;

    const/4 v10, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    sget-object v11, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v12, "makeLsString had nonexistent file"

    invoke-direct {p0, v11, v12}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v10

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v11, "*"

    invoke-virtual {v3, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    const-string v11, "/"

    invoke-virtual {v3, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    :cond_1
    sget-object v11, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v12, "Filename omitted due to disallowed character"

    invoke-direct {p0, v11, v12}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "drwxr-xr-x 1 owner group"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    rsub-int/lit8 v6, v10, 0xd

    move v7, v6

    :goto_2
    add-int/lit8 v6, v7, -0x1

    if-lez v7, :cond_4

    const/16 v10, 0x20

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v7, v6

    goto :goto_2

    :cond_3
    const-string v10, "-rw-r--r-- 1 owner group"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v4

    const-wide/32 v12, -0x61075000

    cmp-long v10, v10, v12

    if-lez v10, :cond_5

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v10, " MMM dd HH:mm "

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v10, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    :goto_3
    new-instance v10, Ljava/sql/Date;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v11

    invoke-direct {v10, v11, v12}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v2, v10}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\r\n"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    :cond_5
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v10, " MMM dd  yyyy "

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v10, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_3
.end method

.method protected recursiveDelete(Ljava/io/File;)Z
    .locals 10
    .param p1    # Ljava/io/File;

    const/4 v6, 0x0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v5, 0x1

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v2, v0, v3

    invoke-virtual {p0, v2}, Lcom/mediatek/apst/target/ftp/FtpConnection;->recursiveDelete(Ljava/io/File;)Z

    move-result v7

    and-int/2addr v5, v7

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    sget-object v7, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Recursively deleted: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_3
    sget-object v6, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "RMD deleting file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v6

    goto :goto_0
.end method

.method public run()V
    .locals 7

    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    iget-object v5, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mFtpCharset:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    const/16 v5, 0x2000

    invoke-direct {v3, v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/BufferedOutputStream;

    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    const/16 v5, 0x2000

    invoke-direct {v3, v4, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    const-string v3, "220-Welcome message......\r\n"

    invoke-direct {p0, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    const-string v3, "220 Notice:last msg\r\n"

    invoke-direct {p0, v3}, Lcom/mediatek/apst/target/ftp/FtpConnection;->response(Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_5

    :goto_0
    :try_start_1
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e

    :cond_1
    :goto_1
    :try_start_2
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f

    :cond_2
    :goto_2
    :try_start_3
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_10

    :cond_3
    :goto_3
    :try_start_4
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_11

    :cond_4
    :goto_4
    return-void

    :cond_5
    :try_start_5
    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/ftp/FtpConnection;->parseCommand(Ljava/lang/String;)V

    const-string v3, "QUIT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_6
    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SocketException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_8

    :cond_6
    :goto_5
    :try_start_8
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9

    :cond_7
    :goto_6
    :try_start_9
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_a

    :cond_8
    :goto_7
    :try_start_a
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    goto :goto_4

    :catch_1
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "socket exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_8
    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4

    :catch_2
    move-exception v1

    :try_start_b
    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException: Start Ftp connection error!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :try_start_c
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_b

    :cond_9
    :goto_9
    :try_start_d
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_c

    :cond_a
    :goto_a
    :try_start_e
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_d

    :cond_b
    :goto_b
    :try_start_f
    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3

    goto/16 :goto_4

    :catch_3
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "socket exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    :catchall_0
    move-exception v3

    :try_start_10
    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;

    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mReader:Ljava/io/BufferedReader;
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_4

    :cond_c
    :goto_c
    :try_start_11
    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    if-eqz v4, :cond_d

    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;

    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mWriter:Ljava/io/BufferedOutputStream;
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_5

    :cond_d
    :goto_d
    :try_start_12
    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->close()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mPasvSocket:Ljava/net/ServerSocket;
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_6

    :cond_e
    :goto_e
    :try_start_13
    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    if-eqz v4, :cond_f

    iget-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->close()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mSocket:Ljava/net/Socket;
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_7

    :cond_f
    :goto_f
    throw v3

    :catch_4
    move-exception v1

    sget-object v4, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reader exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_c

    :catch_5
    move-exception v1

    sget-object v4, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "writer exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_d

    :catch_6
    move-exception v1

    sget-object v4, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pasvSocket exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_e

    :catch_7
    move-exception v1

    sget-object v4, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "socket exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_f

    :catch_8
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reader exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_5

    :catch_9
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "writer exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_6

    :catch_a
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pasvSocket exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_7

    :catch_b
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reader exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_9

    :catch_c
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "writer exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_a

    :catch_d
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pasvSocket exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_b

    :catch_e
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reader exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_f
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "writer exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_10
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pasvSocket exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_3

    :catch_11
    move-exception v1

    sget-object v3, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "socket exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8
.end method

.method public setRenameFrom(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lcom/mediatek/apst/target/ftp/FtpConnection;->mRenameFrom:Ljava/io/File;

    return-void
.end method

.method public violatesChroot(Ljava/io/File;)Z
    .locals 7
    .param p1    # Ljava/io/File;

    const/4 v3, 0x1

    new-instance v1, Ljava/io/File;

    const-string v4, "/"

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    const-string v5, "Path violated folder restriction, denying"

    invoke-direct {p0, v4, v5}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "path: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chroot: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :catch_0
    move-exception v2

    sget-object v4, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Path canonicalization problem: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/mediatek/apst/target/ftp/FtpConnection;->TAG:[Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "When checking file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/mediatek/apst/target/ftp/FtpConnection;->ftpLog([Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
