.class public abstract Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;
.super Ljava/lang/Thread;
.source "BlockingCommandHandlingThread.java"


# static fields
.field private static final DEFAULT_CMD_QUEUE_CAPACITY:I = 0x3e8


# instance fields
.field private mCommandQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/mediatek/apst/util/command/BaseCommand;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxPriority:I

.field private mShouldTerminate:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mShouldTerminate:Z

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    const/16 v0, 0xa

    iput v0, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mMaxPriority:I

    return-void
.end method

.method private dequeue(Z)Lcom/mediatek/apst/util/command/BaseCommand;
    .locals 7
    .param p1    # Z

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/command/BaseCommand;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "dequeue"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v3, v4, v2, v0}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/command/BaseCommand;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized enqueue(Lcom/mediatek/apst/util/command/BaseCommand;Z)Z
    .locals 8
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;
    .param p2    # Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    monitor-enter p0

    if-eqz p2, :cond_1

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4, p1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "enqueue"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x0

    invoke-static {v2, v4, v5, v6, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mCommandQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4, p1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "enqueue"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    const-string v6, "Failed, command queue is full."

    invoke-static {v2, v4, v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getClassName()Ljava/lang/String;
    .locals 1

    const-string v0, "BlockingCommandHandlingThread"

    return-object v0
.end method

.method protected abstract handle(Lcom/mediatek/apst/util/command/BaseCommand;)V
.end method

.method public isShouldTerminate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mShouldTerminate:Z

    return v0
.end method

.method public run()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "run"

    const-string v3, "Thread start running."

    invoke-static {v1, v2, v4, v3}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->isShouldTerminate()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->dequeue(Z)Lcom/mediatek/apst/util/command/BaseCommand;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mMaxPriority:I

    invoke-virtual {p0, v1}, Ljava/lang/Thread;->setPriority(I)V

    :cond_0
    :goto_1
    invoke-virtual {p0, v0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->handle(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Thread;->getPriority()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Thread;->getPriority()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/Thread;->setPriority(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "run"

    const-string v3, "Thread terminated."

    invoke-static {v1, v2, v4, v3}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setMaxPriority(I)V
    .locals 2
    .param p1    # I

    move v0, p1

    const/16 v1, 0xa

    if-le v0, v1, :cond_1

    const/16 v0, 0xa

    :cond_0
    :goto_0
    iput v0, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mMaxPriority:I

    return-void

    :cond_1
    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public terminate()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->mShouldTerminate:Z

    return-void
.end method
