.class public Lcom/mediatek/apst/target/service/MainService;
.super Landroid/app/Service;
.source "MainService.java"

# interfaces
.implements Lcom/mediatek/apst/target/event/IBatteryListener;
.implements Lcom/mediatek/apst/target/event/ICalendarEventListener;
.implements Lcom/mediatek/apst/target/event/IContactsListener;
.implements Lcom/mediatek/apst/target/event/IMmsListener;
.implements Lcom/mediatek/apst/target/event/IPackageListener;
.implements Lcom/mediatek/apst/target/event/ISdStateListener;
.implements Lcom/mediatek/apst/target/event/ISimStateListener;
.implements Lcom/mediatek/apst/target/event/ISmsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/apst/target/service/MainService$MainHandler;,
        Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;,
        Lcom/mediatek/apst/target/service/MainService$CommandHandler;,
        Lcom/mediatek/apst/target/service/MainService$CommandSender;,
        Lcom/mediatek/apst/target/service/MainService$Connector;,
        Lcom/mediatek/apst/target/service/MainService$MainCallback;
    }
.end annotation


# static fields
.field private static final MSG_CHECK_TIMEOUT:I = 0x2

.field private static final MSG_CONNECTED:I = 0x1

.field private static final MSG_FORCE_STOP:I = 0x4

.field private static final MSG_SAFE_STOP:I = 0x3

.field private static final RSP_ST_APPEND_BATCH:I = 0x2

.field private static final RSP_ST_SEND_BATCH:I = 0x3

.field private static final RSP_ST_SEND_SINGLE:I = 0x1

.field private static sHasInstance:Ljava/lang/Boolean;


# instance fields
.field private mApplicationProxy:Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;

.field private mBR:Lcom/mediatek/apst/target/receiver/InternalReceiver;

.field private mBookmarkProxy:Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

.field private mCalendarEventOb:Lcom/mediatek/apst/target/service/CalendarEventObserver;

.field private mCalendarProxy:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

.field private mCmdHandler:Lcom/mediatek/apst/target/service/MainService$CommandHandler;

.field private mCmdSender:Lcom/mediatek/apst/target/service/MainService$CommandSender;

.field private mComm:Lcom/mediatek/apst/util/communication/common/CommHandler;

.field private mCommandBatch:Lcom/mediatek/apst/util/command/ICommandBatch;

.field private mConnected:Z

.field private mConnector:Lcom/mediatek/apst/target/service/MainService$Connector;

.field private mContactsOb:Lcom/mediatek/apst/target/service/ContactsObserver;

.field private mContactsProxy:Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

.field private mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

.field private mHasNotifiedStart:Z

.field private mIncomingSmsFinder:Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;

.field private mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

.field private mMediaProxy:Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

.field private mMessageOb:Lcom/mediatek/apst/target/service/MessageObserver;

.field private mMessageProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

.field private mMsgHandler:Lcom/mediatek/apst/target/service/MainService$MainHandler;

.field private mMulMessageOb:Lcom/mediatek/apst/target/service/MulMessageObserver;

.field private mPrepareToStop:Z

.field private mResponseState:I

.field private mSmsSender:Lcom/mediatek/apst/target/service/SmsSender;

.field private mSysInfoProxy:Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/service/MainService;->sHasInstance:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x1f4

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-boolean v1, p0, Lcom/mediatek/apst/target/service/MainService;->mHasNotifiedStart:Z

    iput-boolean v1, p0, Lcom/mediatek/apst/target/service/MainService;->mConnected:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/apst/target/service/MainService;->mResponseState:I

    iput-boolean v1, p0, Lcom/mediatek/apst/target/service/MainService;->mPrepareToStop:Z

    new-instance v0, Lcom/mediatek/apst/target/service/MainService$Connector;

    invoke-direct {v0, p0}, Lcom/mediatek/apst/target/service/MainService$Connector;-><init>(Lcom/mediatek/apst/target/service/MainService;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mConnector:Lcom/mediatek/apst/target/service/MainService$Connector;

    new-instance v0, Lcom/mediatek/apst/target/service/MainService$CommandHandler;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/apst/target/service/MainService$CommandHandler;-><init>(Lcom/mediatek/apst/target/service/MainService;I)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdHandler:Lcom/mediatek/apst/target/service/MainService$CommandHandler;

    new-instance v0, Lcom/mediatek/apst/target/service/MainService$CommandSender;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/apst/target/service/MainService$CommandSender;-><init>(Lcom/mediatek/apst/target/service/MainService;I)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdSender:Lcom/mediatek/apst/target/service/MainService$CommandSender;

    new-instance v0, Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;

    invoke-direct {v0, p0}, Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;-><init>(Lcom/mediatek/apst/target/service/MainService;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mIncomingSmsFinder:Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;

    invoke-static {}, Lcom/mediatek/apst/target/service/SmsSender;->getInstance()Lcom/mediatek/apst/target/service/SmsSender;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mSmsSender:Lcom/mediatek/apst/target/service/SmsSender;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/util/communication/common/Dispatcher;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/mediatek/apst/target/service/MainService;Lcom/mediatek/apst/util/command/ICommandBatch;)Lcom/mediatek/apst/util/command/ICommandBatch;
    .locals 0
    .param p0    # Lcom/mediatek/apst/target/service/MainService;
    .param p1    # Lcom/mediatek/apst/util/command/ICommandBatch;

    iput-object p1, p0, Lcom/mediatek/apst/target/service/MainService;->mCommandBatch:Lcom/mediatek/apst/util/command/ICommandBatch;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/mediatek/apst/target/service/MainService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/apst/target/service/MainService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/target/service/MainService;->mResponseState:I

    return p1
.end method

.method static synthetic access$1200(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mSysInfoProxy:Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mContactsProxy:Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/receiver/InternalReceiver;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mBR:Lcom/mediatek/apst/target/receiver/InternalReceiver;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MulMessageObserver;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMulMessageOb:Lcom/mediatek/apst/target/service/MulMessageObserver;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/SmsSender;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mSmsSender:Lcom/mediatek/apst/target/service/SmsSender;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarProxy:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMediaProxy:Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mBookmarkProxy:Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/apst/target/service/MainService;Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    invoke-direct {p0, p1}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/mediatek/apst/target/service/MainService;)V
    .locals 0
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->init()V

    return-void
.end method

.method static synthetic access$2100(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MainService$Connector;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mConnector:Lcom/mediatek/apst/target/service/MainService$Connector;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mApplicationProxy:Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/apst/target/service/MainService;Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    invoke-direct {p0, p1}, Lcom/mediatek/apst/target/service/MainService;->enqueueHandleCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/target/service/MainService$MainHandler;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMsgHandler:Lcom/mediatek/apst/target/service/MainService$MainHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/apst/target/service/MainService;)Lcom/mediatek/apst/util/communication/common/CommHandler;
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mComm:Lcom/mediatek/apst/util/communication/common/CommHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/apst/target/service/MainService;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/apst/target/service/MainService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/apst/target/service/MainService;->setConnected(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/apst/target/service/MainService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->isConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/mediatek/apst/target/service/MainService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/apst/target/service/MainService;

    iget-boolean v0, p0, Lcom/mediatek/apst/target/service/MainService;->mPrepareToStop:Z

    return v0
.end method

.method private declared-synchronized appendBatch(Lcom/mediatek/apst/util/command/BaseCommand;)V
    .locals 3
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    monitor-enter p0

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Command is null!"

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCommandBatch:Lcom/mediatek/apst/util/command/ICommandBatch;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FeatureID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getFeatureID()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Token="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->getToken()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCommandBatch:Lcom/mediatek/apst/util/command/ICommandBatch;

    invoke-interface {v0}, Lcom/mediatek/apst/util/command/ICommandBatch;->getCommands()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private deInit()V
    .locals 2

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->unregisterEventListeners()V

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->terminateThreads()V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mComm:Lcom/mediatek/apst/util/communication/common/CommHandler;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mComm:Lcom/mediatek/apst/util/communication/common/CommHandler;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/communication/common/CommHandler;->closeConnection()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-direct {p0, v1}, Lcom/mediatek/apst/target/service/MainService;->setConnected(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mComm:Lcom/mediatek/apst/util/communication/common/CommHandler;

    :cond_0
    const-string v1, "Socket connection closed..."

    invoke-static {v1}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->uninstallCommunicationCallbacks()V

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->notifyStop()V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logE(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private declared-synchronized enqueueHandleCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 3
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdHandler:Lcom/mediatek/apst/target/service/MainService$CommandHandler;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdHandler:Lcom/mediatek/apst/target/service/MainService$CommandHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->enqueue(Lcom/mediatek/apst/util/command/BaseCommand;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v2, "Command handler thread is null."

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    .locals 3
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdSender:Lcom/mediatek/apst/target/service/MainService$CommandSender;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdSender:Lcom/mediatek/apst/target/service/MainService$CommandSender;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->enqueue(Lcom/mediatek/apst/util/command/BaseCommand;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v2, "Command sender thread is null."

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private init()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->installCommunicationCallbacks()V

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->startThreads()V

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->registerEventListeners()V

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->notifyStart()V

    return-void
.end method

.method private installCommunicationCallbacks()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/16 v1, 0x100

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/16 v1, 0x1000

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x10000

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x100000

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x1000000

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x1100000

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const v1, 0x11000

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x1110000

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->installCallback(ILcom/mediatek/apst/util/communication/common/ICallback;)Z

    const-string v0, "Communication callbacks installed..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    return-void
.end method

.method private isConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/target/service/MainService;->mConnected:Z

    return v0
.end method

.method private notifyStart()V
    .locals 8

    const v7, 0x7f030001

    const/4 v6, 0x0

    const v3, 0x7f030002

    invoke-virtual {p0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v1, Landroid/app/Notification;

    const/high16 v3, 0x7f020000

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v3, v2, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const/4 v3, 0x2

    iput v3, v1, Landroid/app/Notification;->flags:I

    new-instance v3, Landroid/content/Intent;

    const-string v4, ""

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v6, v3, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const/high16 v3, 0x7f030000

    invoke-virtual {p0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, p0, v3, v2, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v7, v1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    invoke-static {p0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/apst/target/service/MainService;->mHasNotifiedStart:Z

    return-void
.end method

.method private notifyStop()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/mediatek/apst/target/service/MainService;->mHasNotifiedStart:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/mediatek/apst/target/service/MainService;->mHasNotifiedStart:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Service;->stopForeground(Z)V

    const v0, 0x7f030003

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method private registerEventListeners()V
    .locals 4

    const/4 v3, 0x1

    invoke-static {p0}, Lcom/mediatek/apst/target/event/EventDispatcher;->registerBatteryListener(Lcom/mediatek/apst/target/event/IBatteryListener;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/event/EventDispatcher;->registerPackageListener(Lcom/mediatek/apst/target/event/IPackageListener;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/event/EventDispatcher;->registerSdStateListener(Lcom/mediatek/apst/target/event/ISdStateListener;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/event/EventDispatcher;->registerSimStateListener(Lcom/mediatek/apst/target/event/ISimStateListener;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/event/EventDispatcher;->registerSmsListener(Lcom/mediatek/apst/target/event/ISmsListener;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/event/EventDispatcher;->registerContactsListener(Lcom/mediatek/apst/target/event/IContactsListener;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/event/EventDispatcher;->registerCalendarEventListener(Lcom/mediatek/apst/target/event/ICalendarEventListener;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/event/EventDispatcher;->registerMmsListener(Lcom/mediatek/apst/target/event/IMmsListener;)V

    const-string v0, "MainService registered for listening events..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mBR:Lcom/mediatek/apst/target/receiver/InternalReceiver;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/receiver/InternalReceiver;->registerAll()V

    const-string v0, "Broadcast receivers registered..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/message/SmsContent;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageOb:Lcom/mediatek/apst/target/service/MessageObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/message/MmsContent;->CONTENT_URI_OB:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMulMessageOb:Lcom/mediatek/apst/target/service/MulMessageObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageOb:Lcom/mediatek/apst/target/service/MessageObserver;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/MessageObserver;->start()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/apst/target/data/provider/calendar/CalendarEventContent;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarEventOb:Lcom/mediatek/apst/target/service/CalendarEventObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMulMessageOb:Lcom/mediatek/apst/target/service/MulMessageObserver;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/MulMessageObserver;->start()V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarEventOb:Lcom/mediatek/apst/target/service/CalendarEventObserver;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/CalendarEventObserver;->start()V

    const-string v0, "Content observers registered and started..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    return-void
.end method

.method private setConnected(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/target/service/MainService;->mConnected:Z

    return-void
.end method

.method private startThreads()V
    .locals 2

    const/16 v1, 0xa

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdHandler:Lcom/mediatek/apst/target/service/MainService$CommandHandler;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->setMaxPriority(I)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdHandler:Lcom/mediatek/apst/target/service/MainService$CommandHandler;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdSender:Lcom/mediatek/apst/target/service/MainService$CommandSender;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->setMaxPriority(I)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdSender:Lcom/mediatek/apst/target/service/MainService$CommandSender;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mIncomingSmsFinder:Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mSmsSender:Lcom/mediatek/apst/target/service/SmsSender;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/SmsSender;->start()V

    return-void
.end method

.method private terminateThreads()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mIncomingSmsFinder:Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mIncomingSmsFinder:Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/NewSmsFinder;->terminate()V

    iput-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mIncomingSmsFinder:Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mSmsSender:Lcom/mediatek/apst/target/service/SmsSender;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mSmsSender:Lcom/mediatek/apst/target/service/SmsSender;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/SmsSender;->terminate()V

    iput-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mSmsSender:Lcom/mediatek/apst/target/service/SmsSender;

    :cond_1
    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdSender:Lcom/mediatek/apst/target/service/MainService$CommandSender;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdSender:Lcom/mediatek/apst/target/service/MainService$CommandSender;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->terminate()V

    iput-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdSender:Lcom/mediatek/apst/target/service/MainService$CommandSender;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdHandler:Lcom/mediatek/apst/target/service/MainService$CommandHandler;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdHandler:Lcom/mediatek/apst/target/service/MainService$CommandHandler;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/BlockingCommandHandlingThread;->terminate()V

    iput-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mCmdHandler:Lcom/mediatek/apst/target/service/MainService$CommandHandler;

    :cond_3
    return-void
.end method

.method private uninstallCommunicationCallbacks()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x100000

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x1100000

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const v1, 0x11000

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    const/high16 v1, 0x1110000

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->uninstallCallback(I)Z

    :cond_0
    const-string v0, "Communication callbacks uninstalled..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    return-void
.end method

.method private unregisterEventListeners()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageOb:Lcom/mediatek/apst/target/service/MessageObserver;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/MessageObserver;->stop()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageOb:Lcom/mediatek/apst/target/service/MessageObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMulMessageOb:Lcom/mediatek/apst/target/service/MulMessageObserver;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/MulMessageObserver;->stop()V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarEventOb:Lcom/mediatek/apst/target/service/CalendarEventObserver;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/CalendarEventObserver;->stop()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mMulMessageOb:Lcom/mediatek/apst/target/service/MulMessageObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarEventOb:Lcom/mediatek/apst/target/service/CalendarEventObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const-string v0, "Content observers stopped and unregistered..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mBR:Lcom/mediatek/apst/target/receiver/InternalReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mBR:Lcom/mediatek/apst/target/receiver/InternalReceiver;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/receiver/InternalReceiver;->unregisterAll()V

    :cond_0
    const-string v0, "Broadcast receivers unregistered..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/event/EventDispatcher;->unregisterListener(Lcom/mediatek/apst/target/event/IEventListener;)V

    const-string v0, "MainService unregistered for listening events..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onBatteryStateChanged(Lcom/mediatek/apst/target/event/Event;)V
    .locals 2
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    new-instance v0, Lcom/mediatek/apst/util/command/sysinfo/NotifyBatteryReq;

    invoke-direct {v0}, Lcom/mediatek/apst/util/command/sysinfo/NotifyBatteryReq;-><init>()V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    const-string v1, "level"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifyBatteryReq;->setBatteryLevel(I)V

    const-string v1, "scale"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifyBatteryReq;->setBatteryScale(I)V

    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCalendarEventContentChanged(Lcom/mediatek/apst/target/event/Event;)V
    .locals 2
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    const-string v1, "by_self"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/mediatek/apst/util/command/calendar/NotifyCalendarEventChangeReq;

    invoke-direct {v0}, Lcom/mediatek/apst/util/command/calendar/NotifyCalendarEventChangeReq;-><init>()V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    const-string v1, "calendarEvent"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/calendar/NotifyCalendarEventChangeReq;->setEvent(Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;)V

    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    :cond_0
    return-void
.end method

.method public onContactsContentChanged(Lcom/mediatek/apst/target/event/Event;)V
    .locals 2
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    const-string v1, "by_self"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/mediatek/apst/util/command/contacts/NotifyContactsContentChangeReq;

    invoke-direct {v0}, Lcom/mediatek/apst/util/command/contacts/NotifyContactsContentChangeReq;-><init>()V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 3

    sput-object p0, Lcom/mediatek/apst/target/util/Global;->sContext:Landroid/content/Context;

    new-instance v0, Lcom/mediatek/apst/target/receiver/InternalReceiver;

    invoke-direct {v0, p0}, Lcom/mediatek/apst/target/receiver/InternalReceiver;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mBR:Lcom/mediatek/apst/target/receiver/InternalReceiver;

    invoke-static {p0}, Lcom/mediatek/apst/target/data/proxy/ProxyManager;->getSystemInfoProxy(Landroid/content/Context;)Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mSysInfoProxy:Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;

    invoke-static {p0}, Lcom/mediatek/apst/target/data/proxy/ProxyManager;->getContactsProxy(Landroid/content/Context;)Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mContactsProxy:Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mContactsProxy:Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mContactsOb:Lcom/mediatek/apst/target/service/ContactsObserver;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->registerSelfChangeObserver(Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/data/proxy/ProxyManager;->getMessageProxy(Landroid/content/Context;)Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageOb:Lcom/mediatek/apst/target/service/MessageObserver;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->registerSelfChangeObserver(Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mMulMessageOb:Lcom/mediatek/apst/target/service/MulMessageObserver;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->registerSelfChangeObserver(Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/data/proxy/ProxyManager;->getCalendarProxy(Landroid/content/Context;)Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarProxy:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    new-instance v0, Lcom/mediatek/apst/target/service/CalendarEventObserver;

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mMsgHandler:Lcom/mediatek/apst/target/service/MainService$MainHandler;

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarProxy:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/CalendarEventObserver;-><init>(Landroid/os/Handler;Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarEventOb:Lcom/mediatek/apst/target/service/CalendarEventObserver;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarProxy:Lcom/mediatek/apst/target/data/proxy/calendar/CalendarProxy;

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mCalendarEventOb:Lcom/mediatek/apst/target/service/CalendarEventObserver;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->registerSelfChangeObserver(Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;)V

    invoke-static {p0}, Lcom/mediatek/apst/target/data/proxy/ProxyManager;->getApplicationProxy(Landroid/content/Context;)Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mApplicationProxy:Lcom/mediatek/apst/target/data/proxy/app/ApplicationProxy;

    invoke-static {p0}, Lcom/mediatek/apst/target/data/proxy/ProxyManager;->getMediaProxy(Landroid/content/Context;)Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMediaProxy:Lcom/mediatek/apst/target/data/proxy/media/MediaProxy;

    invoke-static {p0}, Lcom/mediatek/apst/target/data/proxy/ProxyManager;->getBookmarkProxy(Landroid/content/Context;)Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mBookmarkProxy:Lcom/mediatek/apst/target/data/proxy/bookmark/BookmarkProxy;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/mediatek/apst/util/communication/comm/CommFactory;->createCommHandler(I)Lcom/mediatek/apst/util/communication/common/CommHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mComm:Lcom/mediatek/apst/util/communication/common/CommHandler;

    invoke-static {}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getInstance()Lcom/mediatek/apst/util/communication/common/Dispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    new-instance v0, Lcom/mediatek/apst/target/service/MainService$MainCallback;

    invoke-direct {v0, p0}, Lcom/mediatek/apst/target/service/MainService$MainCallback;-><init>(Lcom/mediatek/apst/target/service/MainService;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMainCallback:Lcom/mediatek/apst/util/communication/common/ICallback;

    new-instance v0, Lcom/mediatek/apst/target/service/MainService$MainHandler;

    invoke-direct {v0, p0}, Lcom/mediatek/apst/target/service/MainService$MainHandler;-><init>(Lcom/mediatek/apst/target/service/MainService;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMsgHandler:Lcom/mediatek/apst/target/service/MainService$MainHandler;

    new-instance v0, Lcom/mediatek/apst/target/service/ContactsObserver;

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mMsgHandler:Lcom/mediatek/apst/target/service/MainService$MainHandler;

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mContactsProxy:Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/ContactsObserver;-><init>(Landroid/os/Handler;Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mContactsOb:Lcom/mediatek/apst/target/service/ContactsObserver;

    new-instance v0, Lcom/mediatek/apst/target/service/MessageObserver;

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mMsgHandler:Lcom/mediatek/apst/target/service/MainService$MainHandler;

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MessageObserver;-><init>(Landroid/os/Handler;Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageOb:Lcom/mediatek/apst/target/service/MessageObserver;

    new-instance v0, Lcom/mediatek/apst/target/service/MulMessageObserver;

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mMsgHandler:Lcom/mediatek/apst/target/service/MainService$MainHandler;

    iget-object v2, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    invoke-direct {v0, v1, v2}, Lcom/mediatek/apst/target/service/MulMessageObserver;-><init>(Landroid/os/Handler;Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;)V

    iput-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMulMessageOb:Lcom/mediatek/apst/target/service/MulMessageObserver;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMessageProxy:Lcom/mediatek/apst/target/data/proxy/message/MessageProxy;

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mMulMessageOb:Lcom/mediatek/apst/target/service/MulMessageObserver;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/target/data/proxy/ContextBasedProxy;->registerSelfChangeObserver(Lcom/mediatek/apst/target/data/proxy/ISelfChangeObserver;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    sget-object v1, Lcom/mediatek/apst/target/service/MainService;->sHasInstance:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Try to destroy service..."

    invoke-static {v1}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->deInit()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/mediatek/apst/target/service/MainService;->sHasInstance:Ljava/lang/Boolean;

    const-string v1, "Service Destroyed."

    invoke-static {v1}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/apst/target/ftp/FtpService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method public onMmsInserted(Lcom/mediatek/apst/target/event/Event;)V
    .locals 3
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v2, "mms inserted"

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "by_self"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/mediatek/apst/util/command/message/NotifyNewMessageReq;

    invoke-direct {v0}, Lcom/mediatek/apst/util/command/message/NotifyNewMessageReq;-><init>()V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    const-string v1, "mms"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/message/Mms;

    check-cast v1, Lcom/mediatek/apst/util/entity/message/Mms;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/message/NotifyNewMessageReq;->setNewMessage(Lcom/mediatek/apst/util/entity/message/Message;)V

    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    :cond_0
    return-void
.end method

.method public onMmsReceived(Lcom/mediatek/apst/target/event/Event;)V
    .locals 3
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v2, "mms received"

    invoke-static {v1, v2}, Lcom/mediatek/apst/target/util/Debugger;->logI([Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/apst/util/command/message/NotifyNewMessageReq;

    invoke-direct {v0}, Lcom/mediatek/apst/util/command/message/NotifyNewMessageReq;-><init>()V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    const-string v1, "mms"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/message/Mms;

    check-cast v1, Lcom/mediatek/apst/util/entity/message/Mms;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/message/NotifyNewMessageReq;->setNewMessage(Lcom/mediatek/apst/util/entity/message/Message;)V

    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    return-void
.end method

.method public onMmsSent(Lcom/mediatek/apst/target/event/Event;)V
    .locals 0
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    return-void
.end method

.method public onPackageAdded(Lcom/mediatek/apst/target/event/Event;)V
    .locals 2
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    const-string v1, "uid"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->getInt(Ljava/lang/String;)I

    move-result v0

    new-instance v1, Lcom/mediatek/apst/target/service/MainService$2;

    invoke-direct {v1, p0, v0}, Lcom/mediatek/apst/target/service/MainService$2;-><init>(Lcom/mediatek/apst/target/service/MainService;I)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onPackageDataCleared(Lcom/mediatek/apst/target/event/Event;)V
    .locals 10
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-string v5, "uid"

    invoke-virtual {p1, v5}, Lcom/mediatek/apst/target/event/Event;->getInt(Ljava/lang/String;)I

    move-result v4

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.android.contacts"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v0, v4, :cond_1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const-string v6, "Contacts package data cleared!"

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/mediatek/apst/util/command/contacts/NotifyContactsDataClearedReq;

    invoke-direct {v3}, Lcom/mediatek/apst/util/command/contacts/NotifyContactsDataClearedReq;-><init>()V

    iget-object v5, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v5}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    invoke-direct {p0, v3}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.android.mms"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v2, v4, :cond_0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const-string v6, "Messaging package data cleared! "

    invoke-static {v5, v6}, Lcom/mediatek/apst/target/util/Debugger;->logW([Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/mediatek/apst/util/command/message/NotifyMessageDataClearedReq;

    invoke-direct {v3}, Lcom/mediatek/apst/util/command/message/NotifyMessageDataClearedReq;-><init>()V

    iget-object v5, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v5}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    invoke-direct {p0, v3}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x0

    invoke-static {v5, v6, v1}, Lcom/mediatek/apst/target/util/Debugger;->logE([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onRespond(Lcom/mediatek/apst/util/command/BaseCommand;)V
    .locals 1
    .param p1    # Lcom/mediatek/apst/util/command/BaseCommand;

    iget v0, p0, Lcom/mediatek/apst/target/service/MainService;->mResponseState:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/mediatek/apst/target/service/MainService;->appendBatch(Lcom/mediatek/apst/util/command/BaseCommand;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/mediatek/apst/target/service/MainService;->appendBatch(Lcom/mediatek/apst/util/command/BaseCommand;)V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mCommandBatch:Lcom/mediatek/apst/util/command/ICommandBatch;

    check-cast v0, Lcom/mediatek/apst/util/command/BaseCommand;

    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/apst/target/service/MainService;->mResponseState:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onSdStateChanged(Lcom/mediatek/apst/target/event/Event;)V
    .locals 7
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    const-string v3, "mounted"

    invoke-virtual {p1, v3}, Lcom/mediatek/apst/target/event/Event;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const-string v3, "writeable"

    invoke-virtual {p1, v3}, Lcom/mediatek/apst/target/event/Event;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    new-instance v1, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;

    invoke-direct {v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;-><init>()V

    iget-object v3, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v3}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdMounted(Z)V

    invoke-virtual {v1, v2}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdWriteable(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSdPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdCardPath(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSdTotalSpace()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdCardTotalSpace(J)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->getSdAvailableSpace()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdCardAvailableSpace(J)V

    :goto_0
    invoke-direct {p0, v1}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    return-void

    :cond_0
    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdMounted(Z)V

    invoke-virtual {v1, v4}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdWriteable(Z)V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdCardPath(Ljava/lang/String;)V

    invoke-virtual {v1, v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdCardTotalSpace(J)V

    invoke-virtual {v1, v5, v6}, Lcom/mediatek/apst/util/command/sysinfo/NotifySDStateReq;->setSdCardAvailableSpace(J)V

    goto :goto_0
.end method

.method public onSimStateChanged(Lcom/mediatek/apst/target/event/Event;)V
    .locals 4
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;

    invoke-direct {v0}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;-><init>()V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mSysInfoProxy:Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;

    invoke-virtual {v1}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSimAccessible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSimAccessible(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSim1Accessible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSim1Accessible(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSim2Accessible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSim2Accessible(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSim3Accessible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSim3Accessible(Z)V

    invoke-static {}, Lcom/mediatek/apst/target/data/proxy/sysinfo/SystemInfoProxy;->isSim4Accessible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSim4Accessible(Z)V

    invoke-static {v2}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSimDetailInfo(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V

    invoke-static {v2}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSim1DetailInfo(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V

    invoke-static {v3}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSim2DetailInfo(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSim3DetailInfo(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V

    const/4 v1, 0x3

    invoke-static {v1}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setSim4DetailInfo(Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;)V

    invoke-virtual {v0}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->getSlotInfoList()Ljava/util/List;

    move-result-object v1

    invoke-static {v2}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->getSlotInfoList()Ljava/util/List;

    move-result-object v1

    invoke-static {v3}, Lcom/mediatek/apst/target/util/Global;->getSimInfoBySlot(I)Lcom/mediatek/apst/util/command/sysinfo/SimDetailInfo;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "sim_info_flag"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setInfoChanged(Z)V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mContactsProxy:Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;

    invoke-virtual {v1}, Lcom/mediatek/apst/target/data/proxy/contacts/ContactsProxy;->getAvailableContactsCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/sysinfo/NotifySimStateReq;->setContactsCount(I)V

    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    return-void
.end method

.method public onSmsInserted(Lcom/mediatek/apst/target/event/Event;)V
    .locals 2
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    const-string v1, "by_self"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/mediatek/apst/util/command/message/NotifyNewMessageReq;

    invoke-direct {v0}, Lcom/mediatek/apst/util/command/message/NotifyNewMessageReq;-><init>()V

    iget-object v1, p0, Lcom/mediatek/apst/target/service/MainService;->mDispatcher:Lcom/mediatek/apst/util/communication/common/Dispatcher;

    invoke-virtual {v1}, Lcom/mediatek/apst/util/communication/common/Dispatcher;->getToken()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    const-string v1, "sms"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/message/Sms;

    check-cast v1, Lcom/mediatek/apst/util/entity/message/Sms;

    invoke-virtual {v0, v1}, Lcom/mediatek/apst/util/command/message/NotifyNewMessageReq;->setNewMessage(Lcom/mediatek/apst/util/entity/message/Message;)V

    invoke-direct {p0, v0}, Lcom/mediatek/apst/target/service/MainService;->enqueueSendCommand(Lcom/mediatek/apst/util/command/BaseCommand;)Z

    :cond_0
    return-void
.end method

.method public onSmsReceived(Lcom/mediatek/apst/target/event/Event;)V
    .locals 7
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    iget-object v6, p0, Lcom/mediatek/apst/target/service/MainService;->mIncomingSmsFinder:Lcom/mediatek/apst/target/service/MainService$IncomingSmsFinder;

    new-instance v0, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;

    const-string v1, "after_time_of"

    invoke-virtual {p1, v1}, Lcom/mediatek/apst/target/event/Event;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    const-string v3, "address"

    invoke-virtual {p1, v3}, Lcom/mediatek/apst/target/event/Event;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "body"

    invoke-virtual {p1, v4}, Lcom/mediatek/apst/target/event/Event;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v6, v0}, Lcom/mediatek/apst/target/service/NewSmsFinder;->appendTask(Lcom/mediatek/apst/target/service/NewSmsFinder$Clue;)V

    return-void
.end method

.method public onSmsSent(Lcom/mediatek/apst/target/event/Event;)V
    .locals 7
    .param p1    # Lcom/mediatek/apst/target/event/Event;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mSmsSender:Lcom/mediatek/apst/target/service/SmsSender;

    invoke-virtual {v0}, Lcom/mediatek/apst/target/service/SmsSender;->allowSendNext()V

    const-string v0, "sms_id"

    invoke-virtual {p1, v0}, Lcom/mediatek/apst/target/event/Event;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v0, "date"

    invoke-virtual {p1, v0}, Lcom/mediatek/apst/target/event/Event;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v0, "sent"

    invoke-virtual {p1, v0}, Lcom/mediatek/apst/target/event/Event;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    new-instance v0, Lcom/mediatek/apst/target/service/MainService$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/apst/target/service/MainService$1;-><init>(Lcom/mediatek/apst/target/service/MainService;JJZ)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v5, 0x1

    sget-object v1, Lcom/mediatek/apst/target/service/MainService;->sHasInstance:Ljava/lang/Boolean;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/apst/target/service/MainService;->sHasInstance:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Try to start service..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/mediatek/apst/target/service/MainService;->sHasInstance:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mConnector:Lcom/mediatek/apst/target/service/MainService$Connector;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/mediatek/apst/target/service/MainService;->mMsgHandler:Lcom/mediatek/apst/target/service/MainService$MainHandler;

    const/4 v2, 0x2

    const-wide/16 v3, 0x1194

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/mediatek/apst/target/util/Global;->initFeatureOptionList()V

    return v5

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public safeStop()V
    .locals 1

    const-string v0, "Wait for safe stopping..."

    invoke-static {v0}, Lcom/mediatek/apst/target/util/Debugger;->logI(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/apst/target/service/MainService;->mPrepareToStop:Z

    invoke-direct {p0}, Lcom/mediatek/apst/target/service/MainService;->notifyStop()V

    return-void
.end method
