.class public Lcom/mediatek/apst/util/entity/contacts/ContactData;
.super Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
.source "ContactData.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mimeType:I

.field private primary:Z

.field private rawContactId:J

.field private superPrimary:Z


# direct methods
.method public constructor <init>(I)V
    .locals 9
    .param p1    # I

    const-wide/16 v1, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move v3, p1

    move-wide v4, v1

    move v7, v6

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lcom/mediatek/apst/util/entity/contacts/ContactData;-><init>(JIJZZI)V

    return-void
.end method

.method public constructor <init>(JI)V
    .locals 9
    .param p1    # J
    .param p3    # I

    const/4 v6, 0x0

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move v7, v6

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lcom/mediatek/apst/util/entity/contacts/ContactData;-><init>(JIJZZI)V

    return-void
.end method

.method public constructor <init>(JIJ)V
    .locals 9
    .param p1    # J
    .param p3    # I
    .param p4    # J

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    move v7, v6

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lcom/mediatek/apst/util/entity/contacts/ContactData;-><init>(JIJZZI)V

    return-void
.end method

.method public constructor <init>(JIJZZI)V
    .locals 0
    .param p1    # J
    .param p3    # I
    .param p4    # J
    .param p6    # Z
    .param p7    # Z
    .param p8    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;-><init>(J)V

    iput p3, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->mimeType:I

    iput-wide p4, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->rawContactId:J

    iput-boolean p6, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->primary:Z

    iput-boolean p7, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->superPrimary:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/apst/util/entity/contacts/ContactData;->clone()Lcom/mediatek/apst/util/entity/contacts/ContactData;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/mediatek/apst/util/entity/contacts/ContactData;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->clone()Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/contacts/ContactData;

    return-object v0
.end method

.method public getMimeType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->mimeType:I

    return v0
.end method

.method public getMimeTypeString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getRawContactId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->rawContactId:J

    return-wide v0
.end method

.method public isPrimary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->primary:Z

    return v0
.end method

.method public isSuperPrimary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->superPrimary:Z

    return v0
.end method

.method public readRaw(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->readRaw(Ljava/nio/ByteBuffer;)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->rawContactId:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->mimeType:I

    return-void
.end method

.method public setMimeType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->mimeType:I

    return-void
.end method

.method public setPrimary(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->primary:Z

    return-void
.end method

.method public setRawContactId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->rawContactId:J

    return-void
.end method

.method public setSuperPrimary(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->superPrimary:Z

    return-void
.end method

.method public writeRaw(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->writeRaw(Ljava/nio/ByteBuffer;)V

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->rawContactId:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/mediatek/apst/util/entity/contacts/ContactData;->mimeType:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    return-void
.end method
