.class public Lcom/mediatek/apst/util/entity/calendar/Reminder;
.super Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
.source "Reminder.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private eventId:J

.field private method:J

.field private minutes:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/entity/calendar/Reminder;-><init>(J)V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 2
    .param p1    # J

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;-><init>()V

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->eventId:J

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->minutes:J

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->method:J

    return-void
.end method


# virtual methods
.method public getEventId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->eventId:J

    return-wide v0
.end method

.method public getMethod()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->method:J

    return-wide v0
.end method

.method public getMinutes()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->minutes:J

    return-wide v0
.end method

.method public readRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->eventId:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->minutes:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->method:J

    return-void
.end method

.method public setEventId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->eventId:J

    return-void
.end method

.method public setMethod(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->method:J

    return-void
.end method

.method public setMinutes(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->minutes:J

    return-void
.end method

.method public writeRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->eventId:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->minutes:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/Reminder;->method:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    return-void
.end method
