.class public Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;
.super Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;
.source "CalendarEvent.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final GMT_BEIJING_8:Ljava/lang/String; = "Asia/Shanghai"

.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private accessLevel:I

.field private attendees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/calendar/Attendee;",
            ">;"
        }
    .end annotation
.end field

.field private availability:I

.field private calendarId:J

.field private calendarOwner:Ljava/lang/String;

.field private createTime:J

.field private description:Ljava/lang/String;

.field private duration:Ljava/lang/String;

.field private eventLocation:Ljava/lang/String;

.field private isAllDay:Z

.field private modifyTime:J

.field private privacy:I

.field private reminders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/calendar/Reminder;",
            ">;"
        }
    .end annotation
.end field

.field private repetition:Ljava/lang/String;

.field private timeFrom:J

.field private timeTo:J

.field private timeZone:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private transparency:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;-><init>(J)V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 2
    .param p1    # J

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;-><init>(J)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    const-string v0, "Asia/Shanghai"

    iput-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeZone:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->transparency:I

    iput v1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->privacy:I

    return-void
.end method


# virtual methods
.method public addAttendee(Lcom/mediatek/apst/util/entity/calendar/Attendee;)V
    .locals 1
    .param p1    # Lcom/mediatek/apst/util/entity/calendar/Attendee;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addReminder(Lcom/mediatek/apst/util/entity/calendar/Reminder;)V
    .locals 1
    .param p1    # Lcom/mediatek/apst/util/entity/calendar/Reminder;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getAccessLevel()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->accessLevel:I

    return v0
.end method

.method public getAttendees()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/calendar/Attendee;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    return-object v0
.end method

.method public getAvailability()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->availability:I

    return v0
.end method

.method public getCalendarId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->calendarId:J

    return-wide v0
.end method

.method public getCalendarOwner()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->calendarOwner:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->createTime:J

    return-wide v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->duration:Ljava/lang/String;

    return-object v0
.end method

.method public getEventLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->eventLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getModifyTime()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->modifyTime:J

    return-wide v0
.end method

.method public getPrivacy()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->privacy:I

    return v0
.end method

.method public getReminders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/calendar/Reminder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    return-object v0
.end method

.method public getRepetition()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->repetition:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeFrom()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeFrom:J

    return-wide v0
.end method

.method public getTimeTo()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeTo:J

    return-wide v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTransparency()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->transparency:I

    return v0
.end method

.method public isAllDay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->isAllDay:Z

    return v0
.end method

.method public readRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 6
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferUnderflowException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->calendarId:J

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->title:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeFrom:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeTo:J

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getBoolean(Ljava/nio/ByteBuffer;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->isAllDay:Z

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->eventLocation:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->description:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->calendarOwner:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    if-lez v3, :cond_0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_2

    :cond_0
    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->repetition:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    if-lez v3, :cond_1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v3, :cond_3

    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->modifyTime:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->createTime:J

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->duration:Ljava/lang/String;

    invoke-static {p1}, Lcom/mediatek/apst/util/entity/RawTransUtil;->getString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeZone:Ljava/lang/String;

    const/16 v4, 0x47e

    if-ge p2, v4, :cond_4

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    iput v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->transparency:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    iput v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->privacy:I

    :goto_2
    return-void

    :cond_2
    new-instance v0, Lcom/mediatek/apst/util/entity/calendar/Attendee;

    invoke-direct {v0}, Lcom/mediatek/apst/util/entity/calendar/Attendee;-><init>()V

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    new-instance v2, Lcom/mediatek/apst/util/entity/calendar/Reminder;

    invoke-direct {v2}, Lcom/mediatek/apst/util/entity/calendar/Reminder;-><init>()V

    invoke-virtual {v2, p1, p2}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-object v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    iput v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->availability:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    iput v4, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->accessLevel:I

    goto :goto_2
.end method

.method public setAccessLevel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->accessLevel:I

    return-void
.end method

.method public setAllDay(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->isAllDay:Z

    return-void
.end method

.method public setAttendees(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/calendar/Attendee;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    return-void
.end method

.method public setAvailability(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->availability:I

    return-void
.end method

.method public setCalendarId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->calendarId:J

    return-void
.end method

.method public setCalendarOwner(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->calendarOwner:Ljava/lang/String;

    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->createTime:J

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->description:Ljava/lang/String;

    return-void
.end method

.method public setDuration(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->duration:Ljava/lang/String;

    return-void
.end method

.method public setEventLocation(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->eventLocation:Ljava/lang/String;

    return-void
.end method

.method public setModifyTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->modifyTime:J

    return-void
.end method

.method public setPrivacy(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->privacy:I

    return-void
.end method

.method public setReminders(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/apst/util/entity/calendar/Reminder;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    return-void
.end method

.method public setRepetition(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->repetition:Ljava/lang/String;

    return-void
.end method

.method public setTimeFrom(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeFrom:J

    return-void
.end method

.method public setTimeTo(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeTo:J

    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeZone:Ljava/lang/String;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->title:Ljava/lang/String;

    return-void
.end method

.method public setTransparency(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->transparency:I

    return-void
.end method

.method public writeRawWithVersion(Ljava/nio/ByteBuffer;I)V
    .locals 5
    .param p1    # Ljava/nio/ByteBuffer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    const/4 v4, -0x1

    invoke-super {p0, p1, p2}, Lcom/mediatek/apst/util/entity/DatabaseRecordEntity;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    iget-wide v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->calendarId:J

    invoke-virtual {p1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->title:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeFrom:J

    invoke-virtual {p1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-wide v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeTo:J

    invoke-virtual {p1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-boolean v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->isAllDay:Z

    invoke-static {p1, v2}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putBoolean(Ljava/nio/ByteBuffer;Z)V

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->eventLocation:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->description:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->calendarOwner:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->attendees:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->repetition:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->reminders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_3
    iget-wide v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->modifyTime:J

    invoke-virtual {p1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-wide v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->createTime:J

    invoke-virtual {p1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->duration:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->timeZone:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/mediatek/apst/util/entity/RawTransUtil;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    const/16 v2, 0x47e

    if-ge p2, v2, :cond_4

    iget v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->transparency:I

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->privacy:I

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_4
    return-void

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/apst/util/entity/calendar/Attendee;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/apst/util/entity/calendar/Attendee;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apst/util/entity/calendar/Reminder;

    invoke-virtual {v1, p1, p2}, Lcom/mediatek/apst/util/entity/calendar/Reminder;->writeRawWithVersion(Ljava/nio/ByteBuffer;I)V

    goto :goto_2

    :cond_3
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_3

    :cond_4
    iget v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->availability:I

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/mediatek/apst/util/entity/calendar/CalendarEvent;->accessLevel:I

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_4
.end method
