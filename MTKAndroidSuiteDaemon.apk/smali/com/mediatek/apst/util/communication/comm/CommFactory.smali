.class public Lcom/mediatek/apst/util/communication/comm/CommFactory;
.super Ljava/lang/Object;
.source "CommFactory.java"


# static fields
.field public static final HOST_SIDE:I = 0x0

.field public static final SLAVE_SIDE:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createCommHandler(I)Lcom/mediatek/apst/util/communication/common/CommHandler;
    .locals 1
    .param p0    # I

    if-nez p0, :cond_0

    new-instance v0, Lcom/mediatek/apst/util/communication/server/CommHostHandler;

    invoke-direct {v0}, Lcom/mediatek/apst/util/communication/server/CommHostHandler;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    new-instance v0, Lcom/mediatek/apst/util/communication/client/CommSlaveHandler;

    invoke-direct {v0}, Lcom/mediatek/apst/util/communication/client/CommSlaveHandler;-><init>()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
