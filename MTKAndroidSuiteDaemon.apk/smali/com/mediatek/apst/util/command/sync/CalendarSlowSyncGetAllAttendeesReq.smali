.class public Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllAttendeesReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "CalendarSlowSyncGetAllAttendeesReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private eventIdLimit:J


# direct methods
.method public constructor <init>()V
    .locals 1

    const v0, 0x11000

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getEventIdLimit()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllAttendeesReq;->eventIdLimit:J

    return-wide v0
.end method

.method public setEventIdLimit(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/sync/CalendarSlowSyncGetAllAttendeesReq;->eventIdLimit:J

    return-void
.end method
