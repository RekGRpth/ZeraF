.class public Lcom/mediatek/apst/util/command/message/GetMmsResourceRsp;
.super Lcom/mediatek/apst/util/command/RawBlockResponse;
.source "GetMmsResourceRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private mmsId:J


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x100

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/RawBlockResponse;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getMmsId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/message/GetMmsResourceRsp;->mmsId:J

    return-wide v0
.end method

.method public getResource(Lcom/mediatek/apst/util/entity/message/Mms;I)V
    .locals 7
    .param p1    # Lcom/mediatek/apst/util/entity/message/Mms;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mediatek/apst/util/command/RawBlockResponse;->getRaw()[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    move-object v3, p1

    const/4 v6, -0x1

    if-eq v6, v1, :cond_1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    :goto_1
    invoke-virtual {v3, v5}, Lcom/mediatek/apst/util/entity/message/Mms;->setParts(Ljava/util/List;)V

    return-void

    :cond_0
    new-instance v4, Lcom/mediatek/apst/util/entity/message/MmsPart;

    invoke-direct {v4}, Lcom/mediatek/apst/util/entity/message/MmsPart;-><init>()V

    invoke-virtual {v4, v0, p2}, Lcom/mediatek/apst/util/entity/message/MmsPart;->readRawWithVersion(Ljava/nio/ByteBuffer;I)V

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public setMmsId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/message/GetMmsResourceRsp;->mmsId:J

    return-void
.end method
