.class public Lcom/mediatek/apst/util/command/message/ResendSmsReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "ResendSmsReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private body:Ljava/lang/String;

.field private date:J

.field private id:J

.field private recipient:Ljava/lang/String;

.field private simId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x100

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->simId:I

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->date:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->id:J

    return-wide v0
.end method

.method public getRecipient()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->recipient:Ljava/lang/String;

    return-object v0
.end method

.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->simId:I

    return v0
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->body:Ljava/lang/String;

    return-void
.end method

.method public setDate(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->date:J

    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->id:J

    return-void
.end method

.method public setRecipient(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->recipient:Ljava/lang/String;

    return-void
.end method

.method public setSimId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/message/ResendSmsReq;->simId:I

    return-void
.end method
