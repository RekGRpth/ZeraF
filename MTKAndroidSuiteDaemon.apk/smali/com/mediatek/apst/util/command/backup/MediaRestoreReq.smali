.class public Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "MediaRestoreReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private contentType:I

.field private fileSize:J

.field private restorePath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/high16 v0, 0x1110000

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getContentType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;->contentType:I

    return v0
.end method

.method public getFileSize()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;->fileSize:J

    return-wide v0
.end method

.method public getRestorePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;->restorePath:Ljava/lang/String;

    return-object v0
.end method

.method public setContentType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;->contentType:I

    return-void
.end method

.method public setFileSize(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;->fileSize:J

    return-void
.end method

.method public setRestorePath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/backup/MediaRestoreReq;->restorePath:Ljava/lang/String;

    return-void
.end method
