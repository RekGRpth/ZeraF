.class public Lcom/mediatek/apst/util/command/backup/RestoreCalendarRsp;
.super Lcom/mediatek/apst/util/command/ResponseCommand;
.source "RestoreCalendarRsp.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private insertedCount:J


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/high16 v0, 0x1110000

    invoke-direct {p0, v0, p1}, Lcom/mediatek/apst/util/command/ResponseCommand;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getInsertedCount()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/apst/util/command/backup/RestoreCalendarRsp;->insertedCount:J

    return-wide v0
.end method

.method public setInsertedCount(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/apst/util/command/backup/RestoreCalendarRsp;->insertedCount:J

    return-void
.end method
