.class public abstract Lcom/mediatek/apst/util/command/ResponseCommand;
.super Lcom/mediatek/apst/util/command/BaseCommand;
.source "ResponseCommand.java"


# static fields
.field public static final SC_FAILED:I = 0x2

.field public static final SC_INVALID_ARGUMENTS:I = 0x3

.field public static final SC_OK:I = 0x1

.field public static final SC_UNSUPPORTED_REQUEST:I = 0x4

.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private errorMessage:Ljava/lang/String;

.field private statusCode:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/mediatek/apst/util/command/BaseCommand;-><init>(I)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/apst/util/command/ResponseCommand;->statusCode:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/apst/util/command/ResponseCommand;->errorMessage:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/mediatek/apst/util/communication/common/TransportEntity;->setToken(I)V

    return-void
.end method


# virtual methods
.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/ResponseCommand;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/ResponseCommand;->statusCode:I

    return v0
.end method

.method public setErrorMessage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/ResponseCommand;->errorMessage:Ljava/lang/String;

    return-void
.end method

.method public setStatusCode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/ResponseCommand;->statusCode:I

    return-void
.end method
