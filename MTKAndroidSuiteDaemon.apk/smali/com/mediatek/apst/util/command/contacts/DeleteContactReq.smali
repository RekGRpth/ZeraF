.class public Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "DeleteContactReq.java"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private deleteIds:[J

.field private simEmails:[Ljava/lang/String;

.field private simNames:[Ljava/lang/String;

.field private simNumbers:[Ljava/lang/String;

.field private sourceLocation:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getDeleteIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->deleteIds:[J

    return-object v0
.end method

.method public getSimEmails()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->simEmails:[Ljava/lang/String;

    return-object v0
.end method

.method public getSimNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->simNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getSimNumbers()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->simNumbers:[Ljava/lang/String;

    return-object v0
.end method

.method public getSourceLocation()I
    .locals 1

    iget v0, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->sourceLocation:I

    return v0
.end method

.method public setDeleteIds([J)V
    .locals 0
    .param p1    # [J

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->deleteIds:[J

    return-void
.end method

.method public setSimEmails([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->simEmails:[Ljava/lang/String;

    return-void
.end method

.method public setSimNames([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->simNames:[Ljava/lang/String;

    return-void
.end method

.method public setSimNumbers([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->simNumbers:[Ljava/lang/String;

    return-void
.end method

.method public setSourceLocation(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/apst/util/command/contacts/DeleteContactReq;->sourceLocation:I

    return-void
.end method
