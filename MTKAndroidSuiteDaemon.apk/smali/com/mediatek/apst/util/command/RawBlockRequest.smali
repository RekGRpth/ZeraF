.class public Lcom/mediatek/apst/util/command/RawBlockRequest;
.super Lcom/mediatek/apst/util/command/RequestCommand;
.source "RawBlockRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/apst/util/command/RawBlockRequest$Builder;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private raw:[B


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/apst/util/command/RequestCommand;-><init>(I)V

    return-void
.end method

.method public static builder(I)Lcom/mediatek/apst/util/command/RawBlockRequest$Builder;
    .locals 1
    .param p0    # I

    new-instance v0, Lcom/mediatek/apst/util/command/RawBlockRequest$Builder;

    invoke-direct {v0, p0}, Lcom/mediatek/apst/util/command/RawBlockRequest$Builder;-><init>(I)V

    return-object v0
.end method

.method public static builder(II)Lcom/mediatek/apst/util/command/RawBlockRequest$Builder;
    .locals 1
    .param p0    # I
    .param p1    # I

    new-instance v0, Lcom/mediatek/apst/util/command/RawBlockRequest$Builder;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/apst/util/command/RawBlockRequest$Builder;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public getRaw()[B
    .locals 1

    iget-object v0, p0, Lcom/mediatek/apst/util/command/RawBlockRequest;->raw:[B

    return-object v0
.end method

.method public setRaw([B)V
    .locals 1
    .param p1    # [B

    if-nez p1, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/mediatek/apst/util/command/RawBlockRequest;->raw:[B

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/mediatek/apst/util/command/RawBlockRequest;->raw:[B

    goto :goto_0
.end method
