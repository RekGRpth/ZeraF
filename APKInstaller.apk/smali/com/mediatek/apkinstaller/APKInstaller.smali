.class public Lcom/mediatek/apkinstaller/APKInstaller;
.super Landroid/preference/PreferenceActivity;
.source "APKInstaller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/apkinstaller/APKInstaller$PkgAddBroadcastReceiver;,
        Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;,
        Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;
    }
.end annotation


# static fields
.field private static final MENUID_FRESH:I = 0x0

.field private static final MSG_FILE_OBSERVER:I = 0x3

.field private static final MSG_LOAD_ENTRIES:I = 0x5

.field private static final MSG_LOAD_ICONS:I = 0x6

.field private static final MSG_REFRESH_UI:I = 0x1

.field private static final MSG_RELOAD_APK_FILE:I = 0x4

.field private static final MSG_SHOW_LOADING:I = 0x2

.field private static final TAG:Ljava/lang/String; = "APKInstaller"


# instance fields
.field private mAppDetail:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mAppDetailMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mAppEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mBackgroundHandler:Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

.field private mEntriesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mExitAddPrf:Z

.field private mFileObserverMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/FileObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mFileRoot:Ljava/io/File;

.field private mFiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mListContainer:Landroid/view/View;

.field private mLoadingContainer:Landroid/view/View;

.field private mParentPreference:Landroid/preference/PreferenceScreen;

.field private mPkgReceiver:Lcom/mediatek/apkinstaller/APKInstaller$PkgAddBroadcastReceiver;

.field private mPm:Landroid/content/pm/PackageManager;

.field mStorageListener:Landroid/os/storage/StorageEventListener;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mThread:Landroid/os/HandlerThread;

.field final mUiHandler:Landroid/os/Handler;

.field mVolumePathList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mExitAddPrf:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFileObserverMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFiles:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mEntriesMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppEntries:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppDetailMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppDetail:Ljava/util/ArrayList;

    new-instance v0, Lcom/mediatek/apkinstaller/APKInstaller$2;

    invoke-direct {v0, p0}, Lcom/mediatek/apkinstaller/APKInstaller$2;-><init>(Lcom/mediatek/apkinstaller/APKInstaller;)V

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mUiHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/apkinstaller/APKInstaller$3;

    invoke-direct {v0, p0}, Lcom/mediatek/apkinstaller/APKInstaller$3;-><init>(Lcom/mediatek/apkinstaller/APKInstaller;)V

    iput-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageListener:Landroid/os/storage/StorageEventListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/apkinstaller/APKInstaller;)Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;
    .locals 1
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mBackgroundHandler:Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppDetailMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/apkinstaller/APKInstaller;)Landroid/os/storage/StorageManager;
    .locals 1
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/apkinstaller/APKInstaller;Ljava/io/File;)V
    .locals 0
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/mediatek/apkinstaller/APKInstaller;->getApkFile(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/apkinstaller/APKInstaller;)V
    .locals 0
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-direct {p0}, Lcom/mediatek/apkinstaller/APKInstaller;->loadAppEntry()V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/apkinstaller/APKInstaller;)V
    .locals 0
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-direct {p0}, Lcom/mediatek/apkinstaller/APKInstaller;->loadIcon()V

    return-void
.end method

.method static synthetic access$1502(Lcom/mediatek/apkinstaller/APKInstaller;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mExitAddPrf:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFileObserverMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/apkinstaller/APKInstaller;)V
    .locals 0
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-direct {p0}, Lcom/mediatek/apkinstaller/APKInstaller;->showLoadingContainer()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/apkinstaller/APKInstaller;)Landroid/preference/PreferenceScreen;
    .locals 1
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mParentPreference:Landroid/preference/PreferenceScreen;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppDetail:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/apkinstaller/APKInstaller;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/Intent;

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/apkinstaller/APKInstaller;->addPreference(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/apkinstaller/APKInstaller;)V
    .locals 0
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-direct {p0}, Lcom/mediatek/apkinstaller/APKInstaller;->showListContainer()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFiles:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mEntriesMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppEntries:Ljava/util/ArrayList;

    return-object v0
.end method

.method private addPreference(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mParentPreference:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, p2}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Lcom/mediatek/apkinstaller/IconPreferenceScreen;

    invoke-direct {v0, p0}, Lcom/mediatek/apkinstaller/IconPreferenceScreen;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/mediatek/apkinstaller/IconPreferenceScreen;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-virtual {v0, p3}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mParentPreference:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void
.end method

.method private getApkFile(Ljava/io/File;)V
    .locals 10
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object v0, v4

    array-length v7, v0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v3, v0, v6

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-direct {p0, v3}, Lcom/mediatek/apkinstaller/APKInstaller;->getApkFile(Ljava/io/File;)V

    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    iget-boolean v9, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mExitAddPrf:Z

    if-nez v9, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v9, "."

    invoke-virtual {v5, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v8, v9, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v5, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v9, "apk"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFiles:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private getApkIcon(Landroid/content/pm/PackageInfo;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1    # Landroid/content/pm/PackageInfo;
    .param p2    # Ljava/lang/String;

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    iput-object p2, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object p2, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isPkgInstalled(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mPm:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    const-string v2, "APKInstaller"

    const-string v3, "the package name cannot be null!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private loadAppEntry()V
    .locals 10

    const/4 v9, 0x6

    const/4 v5, 0x0

    const/4 v3, 0x0

    :goto_0
    iget-object v7, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFiles:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    if-ge v5, v9, :cond_1

    iget-object v7, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFiles:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mPm:Landroid/content/pm/PackageManager;

    const/4 v8, 0x0

    invoke-virtual {v7, v2, v8}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v6, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/mediatek/apkinstaller/APKInstaller;->isPkgInstalled(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mEntriesMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_0

    new-instance v0, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;

    invoke-direct {v0, p0, v4, v1}, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;-><init>(Lcom/mediatek/apkinstaller/APKInstaller;Landroid/content/pm/PackageInfo;Ljava/io/File;)V

    iget-object v7, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mEntriesMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppEntries:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-lt v5, v9, :cond_2

    iget-object v7, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mBackgroundHandler:Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    return-void

    :cond_2
    iget-object v7, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mBackgroundHandler:Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    invoke-virtual {v7, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method private loadIcon()V
    .locals 13

    const/4 v12, 0x2

    const/4 v8, 0x0

    const-string v9, "APKInstaller"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "------loadIcon()----mAppEntries.size() "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppEntries:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_0
    iget-object v9, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppEntries:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v3, v9, :cond_1

    if-ge v8, v12, :cond_1

    iget-object v9, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppEntries:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;

    iget-object v5, v1, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;->info:Landroid/content/pm/PackageInfo;

    iget-object v9, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppDetailMap:Ljava/util/HashMap;

    iget-object v10, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_0

    iget-object v2, v1, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;->file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v5, v9}, Lcom/mediatek/apkinstaller/APKInstaller;->getApkIcon(Landroid/content/pm/PackageInfo;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    new-instance v6, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v6, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    const-string v10, "application/vnd.android.package-archive"

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;

    invoke-direct {v0, p0, v4, v7, v6}, Lcom/mediatek/apkinstaller/APKInstaller$AppEntry;-><init>(Lcom/mediatek/apkinstaller/APKInstaller;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/content/Intent;)V

    iget-object v9, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppDetailMap:Ljava/util/HashMap;

    iget-object v10, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v9, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mAppDetail:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-lt v8, v12, :cond_2

    iget-object v9, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mBackgroundHandler:Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    const/4 v10, 0x6

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    iget-object v9, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mUiHandler:Landroid/os/Handler;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method private showListContainer()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mLoadingContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mLoadingContainer:Landroid/view/View;

    const v1, 0x10a0001

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mListContainer:Landroid/view/View;

    const/high16 v1, 0x10a0000

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mListContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mLoadingContainer:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private showLoadingContainer()V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mLoadingContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v2, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mLoadingContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x4

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mPm:Landroid/content/pm/PackageManager;

    const/high16 v4, 0x7f030000

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setContentView(I)V

    const v4, 0x7f060001

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mLoadingContainer:Landroid/view/View;

    const/high16 v4, 0x7f060000

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mListContainer:Landroid/view/View;

    const/high16 v4, 0x7f040000

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mParentPreference:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mParentPreference:Landroid/preference/PreferenceScreen;

    invoke-virtual {v4, v8}, Landroid/preference/PreferenceGroup;->setOrderingAsAdded(Z)V

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v4, :cond_0

    const-string v4, "storage"

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/storage/StorageManager;

    iput-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v5, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageListener:Landroid/os/storage/StorageEventListener;

    invoke-virtual {v4, v5}, Landroid/os/storage/StorageManager;->registerListener(Landroid/os/storage/StorageEventListener;)V

    :cond_0
    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v4}, Landroid/os/storage/StorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v3

    array-length v2, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "not_present"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    aget-object v5, v3, v1

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    iget-object v5, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFileObserverMap:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    new-instance v7, Lcom/mediatek/apkinstaller/APKInstaller$1;

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v7, p0, v4}, Lcom/mediatek/apkinstaller/APKInstaller$1;-><init>(Lcom/mediatek/apkinstaller/APKInstaller;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const-string v4, "APKInstaller"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mVolumePathList size"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mLoadingContainer:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mListContainer:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    new-instance v4, Landroid/os/HandlerThread;

    const-string v5, "APKInstaller.Loader"

    const/16 v6, 0xa

    invoke-direct {v4, v5, v6}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mThread:Landroid/os/HandlerThread;

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    new-instance v4, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    iget-object v5, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;-><init>(Lcom/mediatek/apkinstaller/APKInstaller;Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mBackgroundHandler:Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mBackgroundHandler:Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    invoke-virtual {v4, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    new-instance v4, Lcom/mediatek/apkinstaller/APKInstaller$PkgAddBroadcastReceiver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/mediatek/apkinstaller/APKInstaller$PkgAddBroadcastReceiver;-><init>(Lcom/mediatek/apkinstaller/APKInstaller;Lcom/mediatek/apkinstaller/APKInstaller$1;)V

    iput-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mPkgReceiver:Lcom/mediatek/apkinstaller/APKInstaller$PkgAddBroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v4, "package"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mPkgReceiver:Lcom/mediatek/apkinstaller/APKInstaller$PkgAddBroadcastReceiver;

    invoke-virtual {p0, v4, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    const/4 v1, 0x0

    const v0, 0x7f050003

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mExitAddPrf:Z

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageListener:Landroid/os/storage/StorageEventListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageListener:Landroid/os/storage/StorageEventListener;

    invoke-virtual {v0, v1}, Landroid/os/storage/StorageManager;->unregisterListener(Landroid/os/storage/StorageEventListener;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mPkgReceiver:Lcom/mediatek/apkinstaller/APKInstaller$PkgAddBroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mBackgroundHandler:Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFileObserverMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFileObserverMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/FileObserver;

    invoke-virtual {v1}, Landroid/os/FileObserver;->stopWatching()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 2

    const-string v0, "APKInstaller"

    const-string v1, " start onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method startObserver()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFileObserverMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mFileObserverMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/FileObserver;

    invoke-virtual {v1}, Landroid/os/FileObserver;->startWatching()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
