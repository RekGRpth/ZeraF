.class Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;
.super Landroid/os/Handler;
.source "APKInstaller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/apkinstaller/APKInstaller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BackgroundHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/apkinstaller/APKInstaller;


# direct methods
.method public constructor <init>(Lcom/mediatek/apkinstaller/APKInstaller;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v1}, Lcom/mediatek/apkinstaller/APKInstaller;->access$700(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v1}, Lcom/mediatek/apkinstaller/APKInstaller;->access$800(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v1}, Lcom/mediatek/apkinstaller/APKInstaller;->access$900(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v1}, Lcom/mediatek/apkinstaller/APKInstaller;->access$1000(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v1}, Lcom/mediatek/apkinstaller/APKInstaller;->access$400(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v1, v1, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v1}, Lcom/mediatek/apkinstaller/APKInstaller;->access$1100(Lcom/mediatek/apkinstaller/APKInstaller;)Landroid/os/storage/StorageManager;

    move-result-object v2

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v1, v1, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    new-instance v3, Ljava/io/File;

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    iget-object v1, v1, Lcom/mediatek/apkinstaller/APKInstaller;->mVolumePathList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/mediatek/apkinstaller/APKInstaller;->access$1200(Lcom/mediatek/apkinstaller/APKInstaller;Ljava/io/File;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const-string v1, "APKInstaller"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Files size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v3}, Lcom/mediatek/apkinstaller/APKInstaller;->access$700(Lcom/mediatek/apkinstaller/APKInstaller;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v1}, Lcom/mediatek/apkinstaller/APKInstaller;->access$000(Lcom/mediatek/apkinstaller/APKInstaller;)Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v1}, Lcom/mediatek/apkinstaller/APKInstaller;->access$1300(Lcom/mediatek/apkinstaller/APKInstaller;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/apkinstaller/APKInstaller$BackgroundHandler;->this$0:Lcom/mediatek/apkinstaller/APKInstaller;

    invoke-static {v1}, Lcom/mediatek/apkinstaller/APKInstaller;->access$1400(Lcom/mediatek/apkinstaller/APKInstaller;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
