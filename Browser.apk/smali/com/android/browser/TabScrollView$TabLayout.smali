.class Lcom/android/browser/TabScrollView$TabLayout;
.super Landroid/widget/LinearLayout;
.source "TabScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/TabScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TabLayout"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/TabScrollView;


# direct methods
.method public constructor <init>(Lcom/android/browser/TabScrollView;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/browser/TabScrollView$TabLayout;->this$0:Lcom/android/browser/TabScrollView;

    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setChildrenDrawingOrderEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected getChildDrawingOrder(II)I
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v0, -0x1

    add-int/lit8 v1, p1, -0x1

    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/android/browser/TabScrollView$TabLayout;->this$0:Lcom/android/browser/TabScrollView;

    invoke-static {v1}, Lcom/android/browser/TabScrollView;->access$200(Lcom/android/browser/TabScrollView;)I

    move-result v1

    if-ltz v1, :cond_1

    iget-object v1, p0, Lcom/android/browser/TabScrollView$TabLayout;->this$0:Lcom/android/browser/TabScrollView;

    invoke-static {v1}, Lcom/android/browser/TabScrollView;->access$200(Lcom/android/browser/TabScrollView;)I

    move-result v1

    if-ge v1, p1, :cond_1

    iget-object v1, p0, Lcom/android/browser/TabScrollView$TabLayout;->this$0:Lcom/android/browser/TabScrollView;

    invoke-static {v1}, Lcom/android/browser/TabScrollView;->access$200(Lcom/android/browser/TabScrollView;)I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    sub-int v1, p1, p2

    add-int/lit8 v0, v1, -0x1

    iget-object v1, p0, Lcom/android/browser/TabScrollView$TabLayout;->this$0:Lcom/android/browser/TabScrollView;

    invoke-static {v1}, Lcom/android/browser/TabScrollView;->access$200(Lcom/android/browser/TabScrollView;)I

    move-result v1

    if-gt v0, v1, :cond_0

    if-lez v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/android/browser/TabScrollView$TabLayout;->this$0:Lcom/android/browser/TabScrollView;

    invoke-static {v5}, Lcom/android/browser/TabScrollView;->access$100(Lcom/android/browser/TabScrollView;)I

    move-result v5

    sub-int v1, v4, v5

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int v3, v4, v5

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    add-int v5, v1, v3

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-virtual {v2, v1, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    iget-object v4, p0, Lcom/android/browser/TabScrollView$TabLayout;->this$0:Lcom/android/browser/TabScrollView;

    invoke-static {v4}, Lcom/android/browser/TabScrollView;->access$100(Lcom/android/browser/TabScrollView;)I

    move-result v4

    sub-int v4, v3, v4

    add-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/browser/TabScrollView$TabLayout;->this$0:Lcom/android/browser/TabScrollView;

    invoke-static {v2}, Lcom/android/browser/TabScrollView;->access$000(Lcom/android/browser/TabScrollView;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/android/browser/TabScrollView$TabLayout;->this$0:Lcom/android/browser/TabScrollView;

    invoke-static {v2}, Lcom/android/browser/TabScrollView;->access$100(Lcom/android/browser/TabScrollView;)I

    move-result v2

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method
