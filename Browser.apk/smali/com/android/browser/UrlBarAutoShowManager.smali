.class public Lcom/android/browser/UrlBarAutoShowManager;
.super Ljava/lang/Object;
.source "UrlBarAutoShowManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/android/browser/BrowserWebView$OnScrollChangedListener;


# static fields
.field private static IGNORE_INTERVAL:J

.field private static SCROLL_TIMEOUT_DURATION:J

.field private static V_TRIGGER_ANGLE:F


# instance fields
.field private mHasTriggered:Z

.field private mIsScrolling:Z

.field private mIsTracking:Z

.field private mLastScrollTime:J

.field private mSlop:I

.field private mStartTouchX:F

.field private mStartTouchY:F

.field private mTarget:Lcom/android/browser/BrowserWebView;

.field private mTriggeredTime:J

.field private mUi:Lcom/android/browser/BaseUi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const v0, 0x3f666666

    sput v0, Lcom/android/browser/UrlBarAutoShowManager;->V_TRIGGER_ANGLE:F

    const-wide/16 v0, 0x96

    sput-wide v0, Lcom/android/browser/UrlBarAutoShowManager;->SCROLL_TIMEOUT_DURATION:J

    const-wide/16 v0, 0xfa

    sput-wide v0, Lcom/android/browser/UrlBarAutoShowManager;->IGNORE_INTERVAL:J

    return-void
.end method

.method public constructor <init>(Lcom/android/browser/BaseUi;)V
    .locals 2
    .param p1    # Lcom/android/browser/BaseUi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    iget-object v1, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v1}, Lcom/android/browser/BaseUi;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/android/browser/UrlBarAutoShowManager;->mSlop:I

    return-void
.end method


# virtual methods
.method public onScrollChanged(IIII)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/browser/UrlBarAutoShowManager;->mLastScrollTime:J

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/browser/UrlBarAutoShowManager;->mIsScrolling:Z

    if-eqz p2, :cond_1

    iget-object v2, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->isTitleBarShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/android/browser/UrlBarAutoShowManager;->mLastScrollTime:J

    iget-wide v4, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTriggeredTime:J

    sub-long v0, v2, v4

    const-wide/16 v2, 0x7d0

    sub-long/2addr v2, v0

    sget-wide v4, Lcom/android/browser/UrlBarAutoShowManager;->SCROLL_TIMEOUT_DURATION:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2, v0, v1}, Lcom/android/browser/BaseUi;->showTitleBarForDuration(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->suggestHideTitleBar()V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v12, 0x0

    const/4 v11, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-le v7, v11, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/UrlBarAutoShowManager;->stopTracking()V

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :cond_1
    :goto_0
    return v12

    :pswitch_0
    iget-boolean v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mIsTracking:Z

    if-nez v7, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-ne v7, v11, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    iget-wide v9, p0, Lcom/android/browser/UrlBarAutoShowManager;->mLastScrollTime:J

    sub-long v4, v7, v9

    sget-wide v7, Lcom/android/browser/UrlBarAutoShowManager;->IGNORE_INTERVAL:J

    cmp-long v7, v4, v7

    if-ltz v7, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mStartTouchY:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iput v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mStartTouchX:F

    iput-boolean v11, p0, Lcom/android/browser/UrlBarAutoShowManager;->mIsTracking:Z

    iput-boolean v12, p0, Lcom/android/browser/UrlBarAutoShowManager;->mHasTriggered:Z

    goto :goto_0

    :pswitch_1
    iget-boolean v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mIsTracking:Z

    if-eqz v7, :cond_1

    iget-boolean v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mHasTriggered:Z

    if-nez v7, :cond_1

    move-object v6, p1

    check-cast v6, Landroid/webkit/WebView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iget v8, p0, Lcom/android/browser/UrlBarAutoShowManager;->mStartTouchY:F

    sub-float v3, v7, v8

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iget v8, p0, Lcom/android/browser/UrlBarAutoShowManager;->mStartTouchX:F

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mSlop:I

    int-to-float v7, v7

    cmpl-float v7, v1, v7

    if-lez v7, :cond_1

    iput-boolean v11, p0, Lcom/android/browser/UrlBarAutoShowManager;->mHasTriggered:Z

    float-to-double v7, v1

    float-to-double v9, v0

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v7

    double-to-float v2, v7

    iget v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mSlop:I

    int-to-float v7, v7

    cmpl-float v7, v3, v7

    if-lez v7, :cond_1

    sget v7, Lcom/android/browser/UrlBarAutoShowManager;->V_TRIGGER_ANGLE:F

    cmpl-float v7, v2, v7

    if-lez v7, :cond_1

    iget-object v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v7}, Lcom/android/browser/BaseUi;->isTitleBarShowing()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v6}, Landroid/webkit/WebView;->getVisibleTitleHeight()I

    move-result v7

    if-eqz v7, :cond_2

    iget-boolean v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mIsScrolling:Z

    if-nez v7, :cond_1

    invoke-virtual {v6}, Landroid/view/View;->getScrollY()I

    move-result v7

    if-lez v7, :cond_1

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTriggeredTime:J

    iget-object v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v7}, Lcom/android/browser/BaseUi;->showTitleBar()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    iget-object v7, v7, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    iget-object v7, v7, Lcom/android/browser/BaseUi;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v7}, Lcom/android/browser/TitleBar;->isShowing()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    const-wide/16 v8, 0x7d0

    invoke-virtual {v7, v8, v9}, Lcom/android/browser/BaseUi;->showBottomBarForDuration(J)V

    :cond_3
    invoke-virtual {p0}, Lcom/android/browser/UrlBarAutoShowManager;->stopTracking()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setTarget(Lcom/android/browser/BrowserWebView;)V
    .locals 2
    .param p1    # Lcom/android/browser/BrowserWebView;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTarget:Lcom/android/browser/BrowserWebView;

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTarget:Lcom/android/browser/BrowserWebView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTarget:Lcom/android/browser/BrowserWebView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTarget:Lcom/android/browser/BrowserWebView;

    invoke-virtual {v0, v1}, Lcom/android/browser/BrowserWebView;->setOnScrollChangedListener(Lcom/android/browser/BrowserWebView$OnScrollChangedListener;)V

    :cond_2
    iput-object p1, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTarget:Lcom/android/browser/BrowserWebView;

    iget-object v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTarget:Lcom/android/browser/BrowserWebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTarget:Lcom/android/browser/BrowserWebView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mTarget:Lcom/android/browser/BrowserWebView;

    invoke-virtual {v0, p0}, Lcom/android/browser/BrowserWebView;->setOnScrollChangedListener(Lcom/android/browser/BrowserWebView$OnScrollChangedListener;)V

    goto :goto_0
.end method

.method stopTracking()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mIsTracking:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/android/browser/UrlBarAutoShowManager;->mIsTracking:Z

    iput-boolean v1, p0, Lcom/android/browser/UrlBarAutoShowManager;->mIsScrolling:Z

    iget-object v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v0}, Lcom/android/browser/BaseUi;->isTitleBarShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/UrlBarAutoShowManager;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v0}, Lcom/android/browser/BaseUi;->showTitleBarForDuration()V

    :cond_0
    return-void
.end method
