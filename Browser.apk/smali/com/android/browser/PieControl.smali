.class public Lcom/android/browser/PieControl;
.super Ljava/lang/Object;
.source "PieControl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/browser/view/PieMenu$PieController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/PieControl$TabAdapter;
    }
.end annotation


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field private mAddBookmark:Lcom/android/browser/view/PieItem;

.field private mBack:Lcom/android/browser/view/PieItem;

.field private mBookmarks:Lcom/android/browser/view/PieItem;

.field private mClose:Lcom/android/browser/view/PieItem;

.field private mFind:Lcom/android/browser/view/PieItem;

.field private mForward:Lcom/android/browser/view/PieItem;

.field private mHistory:Lcom/android/browser/view/PieItem;

.field private mIncognito:Lcom/android/browser/view/PieItem;

.field private mInfo:Lcom/android/browser/view/PieItem;

.field protected mItemSize:I

.field private mNewTab:Lcom/android/browser/view/PieItem;

.field private mOptions:Lcom/android/browser/view/PieItem;

.field protected mPie:Lcom/android/browser/view/PieMenu;

.field private mRDS:Lcom/android/browser/view/PieItem;

.field private mRefresh:Lcom/android/browser/view/PieItem;

.field private mShare:Lcom/android/browser/view/PieItem;

.field private mShowTabs:Lcom/android/browser/view/PieItem;

.field private mTabAdapter:Lcom/android/browser/PieControl$TabAdapter;

.field protected mTabsCount:Landroid/widget/TextView;

.field private mUi:Lcom/android/browser/BaseUi;

.field protected mUiController:Lcom/android/browser/UiController;

.field private mUrl:Lcom/android/browser/view/PieItem;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/android/browser/UiController;Lcom/android/browser/BaseUi;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/android/browser/UiController;
    .param p3    # Lcom/android/browser/BaseUi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/browser/PieControl;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-virtual {p1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/browser/PieControl;->mItemSize:I

    iput-object p3, p0, Lcom/android/browser/PieControl;->mUi:Lcom/android/browser/BaseUi;

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/PieControl;)V
    .locals 0
    .param p0    # Lcom/android/browser/PieControl;

    invoke-direct {p0}, Lcom/android/browser/PieControl;->buildTabs()V

    return-void
.end method

.method private buildTabs()V
    .locals 3

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->getTabs()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->getActiveTab()Lcom/android/browser/Tab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/browser/Tab;->capture()V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mTabAdapter:Lcom/android/browser/PieControl$TabAdapter;

    invoke-virtual {v2, v1}, Lcom/android/browser/PieControl$TabAdapter;->setTabs(Ljava/util/List;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getPieView()Lcom/android/browser/view/PieMenu$PieView;

    move-result-object v0

    check-cast v0, Lcom/android/browser/view/PieStackView;

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->getTabControl()Lcom/android/browser/TabControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/browser/TabControl;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/browser/view/PieStackView;->setCurrent(I)V

    return-void
.end method


# virtual methods
.method protected attachToContainer(Landroid/widget/FrameLayout;)V
    .locals 4
    .param p1    # Landroid/widget/FrameLayout;

    const/4 v3, -0x1

    iget-object v1, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/browser/view/PieMenu;

    iget-object v2, p0, Lcom/android/browser/PieControl;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/android/browser/view/PieMenu;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->populateMenu()V

    iget-object v1, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    invoke-virtual {v1, p0}, Lcom/android/browser/view/PieMenu;->setController(Lcom/android/browser/view/PieMenu$PieController;)V

    :cond_0
    iget-object v1, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected forceToTop(Landroid/widget/FrameLayout;)V
    .locals 1
    .param p1    # Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method protected makeFiller()Lcom/android/browser/view/PieItem;
    .locals 3

    new-instance v0, Lcom/android/browser/view/PieItem;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/android/browser/view/PieItem;-><init>(Landroid/view/View;I)V

    return-object v0
.end method

.method protected makeItem(II)Lcom/android/browser/view/PieItem;
    .locals 4
    .param p1    # I
    .param p2    # I

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/browser/PieControl;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget v2, p0, Lcom/android/browser/PieControl;->mItemSize:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumWidth(I)V

    iget v2, p0, Lcom/android/browser/PieControl;->mItemSize:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    iget v2, p0, Lcom/android/browser/PieControl;->mItemSize:I

    iget v3, p0, Lcom/android/browser/PieControl;->mItemSize:I

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Lcom/android/browser/view/PieItem;

    invoke-direct {v2, v1, p2}, Lcom/android/browser/view/PieItem;-><init>(Landroid/view/View;I)V

    return-object v2
.end method

.method protected makeTabsView()Landroid/view/View;
    .locals 6

    iget-object v3, p0, Lcom/android/browser/PieControl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f04002a

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0d0018

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/browser/PieControl;->mTabsCount:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mTabsCount:Landroid/widget/TextView;

    const-string v4, "1"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0d006e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v3, 0x7f020059

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget v3, p0, Lcom/android/browser/PieControl;->mItemSize:I

    iget v4, p0, Lcom/android/browser/PieControl;->mItemSize:I

    invoke-direct {v1, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->getTabControl()Lcom/android/browser/TabControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/browser/TabControl;->getCurrentTab()Lcom/android/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/browser/Tab;->getWebView()Landroid/webkit/WebView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/PieControl;->mBack:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_1

    invoke-virtual {v0}, Lcom/android/browser/Tab;->goBack()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/browser/PieControl;->mForward:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_2

    invoke-virtual {v0}, Lcom/android/browser/Tab;->goForward()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/browser/PieControl;->mRefresh:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_4

    invoke-virtual {v0}, Lcom/android/browser/Tab;->inPageLoad()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/webkit/WebView;->stopLoading()V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/webkit/WebView;->reload()V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/browser/PieControl;->mUrl:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_5

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2, v3, v4}, Lcom/android/browser/BaseUi;->editUrl(ZZ)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/android/browser/PieControl;->mBookmarks:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_6

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    sget-object v3, Lcom/android/browser/UI$ComboViews;->Bookmarks:Lcom/android/browser/UI$ComboViews;

    invoke-interface {v2, v3}, Lcom/android/browser/UiController;->bookmarksOrHistoryPicker(Lcom/android/browser/UI$ComboViews;)V

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/android/browser/PieControl;->mHistory:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_7

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    sget-object v3, Lcom/android/browser/UI$ComboViews;->History:Lcom/android/browser/UI$ComboViews;

    invoke-interface {v2, v3}, Lcom/android/browser/UiController;->bookmarksOrHistoryPicker(Lcom/android/browser/UI$ComboViews;)V

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/android/browser/PieControl;->mAddBookmark:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_8

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->bookmarkCurrentPage()V

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/android/browser/PieControl;->mNewTab:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_9

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->openTabToHomePage()Lcom/android/browser/Tab;

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2, v3, v4}, Lcom/android/browser/BaseUi;->editUrl(ZZ)V

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lcom/android/browser/PieControl;->mIncognito:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_a

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->openIncognitoTab()Lcom/android/browser/Tab;

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2, v3, v4}, Lcom/android/browser/BaseUi;->editUrl(ZZ)V

    goto/16 :goto_0

    :cond_a
    iget-object v2, p0, Lcom/android/browser/PieControl;->mClose:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_b

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->closeCurrentTab()V

    goto/16 :goto_0

    :cond_b
    iget-object v2, p0, Lcom/android/browser/PieControl;->mOptions:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_c

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->openPreferences()V

    goto/16 :goto_0

    :cond_c
    iget-object v2, p0, Lcom/android/browser/PieControl;->mShare:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_d

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->shareCurrentPage()V

    goto/16 :goto_0

    :cond_d
    iget-object v2, p0, Lcom/android/browser/PieControl;->mInfo:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_e

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->showPageInfo()V

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Lcom/android/browser/PieControl;->mFind:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_f

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->findOnPage()V

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Lcom/android/browser/PieControl;->mRDS:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_10

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->toggleUserAgent()V

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUi:Lcom/android/browser/BaseUi;

    check-cast v2, Lcom/android/browser/PhoneUi;

    invoke-virtual {v2}, Lcom/android/browser/PhoneUi;->showNavScreen()V

    goto/16 :goto_0
.end method

.method public onOpen()Z
    .locals 6

    iget-object v4, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v4}, Lcom/android/browser/UiController;->getTabControl()Lcom/android/browser/TabControl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/browser/TabControl;->getTabCount()I

    move-result v1

    iget-object v4, p0, Lcom/android/browser/PieControl;->mTabsCount:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v4}, Lcom/android/browser/UiController;->getCurrentTab()Lcom/android/browser/Tab;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/android/browser/PieControl;->mForward:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/Tab;->canGoForward()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/browser/view/PieItem;->setEnabled(Z)V

    :cond_0
    iget-object v4, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v4}, Lcom/android/browser/UiController;->getCurrentWebView()Landroid/webkit/WebView;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/android/browser/PieControl;->mRDS:Lcom/android/browser/view/PieItem;

    invoke-virtual {v4}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v4}, Lcom/android/browser/UiController;->getSettings()Lcom/android/browser/BrowserSettings;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/browser/BrowserSettings;->hasDesktopUseragent(Landroid/webkit/WebView;)Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f020041

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    :goto_0
    const/4 v4, 0x1

    return v4

    :cond_2
    const v4, 0x7f020029

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method protected populateMenu()V
    .locals 6

    const/4 v5, 0x1

    const v2, 0x7f02001d

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mBack:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020058

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mUrl:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020026

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mBookmarks:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020037

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mHistory:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020021

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mAddBookmark:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020048

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mRefresh:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020032

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mForward:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020043

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mNewTab:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020042

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mIncognito:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020028

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mClose:Lcom/android/browser/view/PieItem;

    const v2, 0x1080041

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mInfo:Lcom/android/browser/view/PieItem;

    const v2, 0x7f02004d

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mFind:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020051

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mShare:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeTabsView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/android/browser/view/PieItem;

    invoke-direct {v2, v1, v5}, Lcom/android/browser/view/PieItem;-><init>(Landroid/view/View;I)V

    iput-object v2, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020050

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mOptions:Lcom/android/browser/view/PieItem;

    const v2, 0x7f020029

    invoke-virtual {p0, v2, v5}, Lcom/android/browser/PieControl;->makeItem(II)Lcom/android/browser/view/PieItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/PieControl;->mRDS:Lcom/android/browser/view/PieItem;

    new-instance v2, Lcom/android/browser/PieControl$TabAdapter;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/android/browser/PieControl;->mUiController:Lcom/android/browser/UiController;

    invoke-direct {v2, v3, v4}, Lcom/android/browser/PieControl$TabAdapter;-><init>(Landroid/content/Context;Lcom/android/browser/UiController;)V

    iput-object v2, p0, Lcom/android/browser/PieControl;->mTabAdapter:Lcom/android/browser/PieControl$TabAdapter;

    new-instance v0, Lcom/android/browser/view/PieStackView;

    iget-object v2, p0, Lcom/android/browser/PieControl;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v2}, Lcom/android/browser/view/PieStackView;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/android/browser/PieControl$1;

    invoke-direct {v2, p0}, Lcom/android/browser/PieControl$1;-><init>(Lcom/android/browser/PieControl;)V

    invoke-virtual {v0, v2}, Lcom/android/browser/view/BasePieView;->setLayoutListener(Lcom/android/browser/view/PieMenu$PieView$OnLayoutListener;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mTabAdapter:Lcom/android/browser/PieControl$TabAdapter;

    invoke-virtual {v0, v2}, Lcom/android/browser/view/PieStackView;->setOnCurrentListener(Lcom/android/browser/view/PieStackView$OnCurrentListener;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mTabAdapter:Lcom/android/browser/PieControl$TabAdapter;

    invoke-virtual {v0, v2}, Lcom/android/browser/view/BasePieView;->setAdapter(Landroid/widget/Adapter;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v0}, Lcom/android/browser/view/PieItem;->setPieView(Lcom/android/browser/view/PieMenu$PieView;)V

    const/16 v2, 0xf

    new-array v2, v2, [Lcom/android/browser/view/PieItem;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/browser/PieControl;->mBack:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/android/browser/PieControl;->mRefresh:Lcom/android/browser/view/PieItem;

    aput-object v3, v2, v5

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/android/browser/PieControl;->mForward:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/android/browser/PieControl;->mUrl:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/android/browser/PieControl;->mFind:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/android/browser/PieControl;->mInfo:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/android/browser/PieControl;->mShare:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-object v4, p0, Lcom/android/browser/PieControl;->mBookmarks:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    iget-object v4, p0, Lcom/android/browser/PieControl;->mNewTab:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/android/browser/PieControl;->mIncognito:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    iget-object v4, p0, Lcom/android/browser/PieControl;->mClose:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, p0, Lcom/android/browser/PieControl;->mHistory:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    iget-object v4, p0, Lcom/android/browser/PieControl;->mAddBookmark:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    iget-object v4, p0, Lcom/android/browser/PieControl;->mOptions:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    iget-object v4, p0, Lcom/android/browser/PieControl;->mRDS:Lcom/android/browser/view/PieItem;

    aput-object v4, v2, v3

    invoke-virtual {p0, p0, v2}, Lcom/android/browser/PieControl;->setClickListener(Landroid/view/View$OnClickListener;[Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/android/browser/BrowserActivity;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v2, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mOptions:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieMenu;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mOptions:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mRDS:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mOptions:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mOptions:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mOptions:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mBack:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieMenu;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mBack:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mRefresh:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mBack:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mForward:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mBack:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mBack:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mUrl:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieMenu;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUrl:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mFind:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUrl:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mShare:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUrl:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mUrl:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieMenu;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mClose:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mIncognito:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mNewTab:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mShowTabs:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mBookmarks:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieMenu;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mBookmarks:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mBookmarks:Lcom/android/browser/view/PieItem;

    invoke-virtual {p0}, Lcom/android/browser/PieControl;->makeFiller()Lcom/android/browser/view/PieItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mBookmarks:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mAddBookmark:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    iget-object v2, p0, Lcom/android/browser/PieControl;->mBookmarks:Lcom/android/browser/view/PieItem;

    iget-object v3, p0, Lcom/android/browser/PieControl;->mHistory:Lcom/android/browser/view/PieItem;

    invoke-virtual {v2, v3}, Lcom/android/browser/view/PieItem;->addItem(Lcom/android/browser/view/PieItem;)V

    return-void
.end method

.method protected removeFromContainer(Landroid/widget/FrameLayout;)V
    .locals 1
    .param p1    # Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/android/browser/PieControl;->mPie:Lcom/android/browser/view/PieMenu;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method protected varargs setClickListener(Landroid/view/View$OnClickListener;[Lcom/android/browser/view/PieItem;)V
    .locals 5
    .param p1    # Landroid/view/View$OnClickListener;
    .param p2    # [Lcom/android/browser/view/PieItem;

    move-object v0, p2

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    invoke-virtual {v2}, Lcom/android/browser/view/PieItem;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public stopEditingUrl()V
    .locals 1

    iget-object v0, p0, Lcom/android/browser/PieControl;->mUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v0}, Lcom/android/browser/BaseUi;->stopEditingUrl()V

    return-void
.end method
