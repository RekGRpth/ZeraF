.class public Lcom/android/browser/BrowserActivity;
.super Landroid/app/Activity;
.source "BrowserActivity.java"


# static fields
.field public static final ACTION_NOTIFICATIONS:Ljava/lang/String; = "notifications"

.field public static final ACTION_RESTART:Ljava/lang/String; = "--restart--"

.field public static final ACTION_SHOW_BOOKMARKS:Ljava/lang/String; = "show_bookmarks"

.field public static final ACTION_SHOW_BROWSER:Ljava/lang/String; = "show_browser"

.field public static final EXTRA_BODY:Ljava/lang/String; = "BODY"

.field public static final EXTRA_COUNTER:Ljava/lang/String; = "COUNTER"

.field public static final EXTRA_DISABLE_URL_OVERRIDE:Ljava/lang/String; = "disable_url_override"

.field public static final EXTRA_POSITION:Ljava/lang/String; = "Position"

.field public static final EXTRA_SHOWN:Ljava/lang/String; = "Shown"

.field private static final EXTRA_STATE:Ljava/lang/String; = "state"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "TITLE"

.field private static final LOGTAG:Ljava/lang/String; = "browser"

.field private static final LOGV_ENABLED:Z = false

.field private static final XLOGTAG:Ljava/lang/String; = "browser/BrowserActivity"


# instance fields
.field private mController:Lcom/android/browser/ActivityController;

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mUi:Lcom/android/browser/UI;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    sget-object v0, Lcom/android/browser/stub/NullController;->INSTANCE:Lcom/android/browser/stub/NullController;

    iput-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    return-void
.end method

.method private createController()Lcom/android/browser/Controller;
    .locals 3

    new-instance v0, Lcom/android/browser/Controller;

    invoke-direct {v0, p0}, Lcom/android/browser/Controller;-><init>(Landroid/app/Activity;)V

    invoke-static {p0}, Lcom/android/browser/BrowserActivity;->isTablet(Landroid/content/Context;)Z

    move-result v2

    const/4 v1, 0x0

    if-eqz v2, :cond_0

    new-instance v1, Lcom/android/browser/XLargeUi;

    invoke-direct {v1, p0, v0}, Lcom/android/browser/XLargeUi;-><init>(Landroid/app/Activity;Lcom/android/browser/UiController;)V

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/browser/Controller;->setUi(Lcom/android/browser/UI;)V

    return-object v0

    :cond_0
    new-instance v1, Lcom/android/browser/PhoneUi;

    invoke-direct {v1, p0, v0}, Lcom/android/browser/PhoneUi;-><init>(Landroid/app/Activity;Lcom/android/browser/UiController;)V

    goto :goto_0
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method private shouldIgnoreIntents()Z
    .locals 2

    iget-object v1, p0, Lcom/android/browser/BrowserActivity;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-nez v1, :cond_0

    const-string v1, "keyguard"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    iput-object v1, p0, Lcom/android/browser/BrowserActivity;->mKeyguardManager:Landroid/app/KeyguardManager;

    :cond_0
    iget-object v1, p0, Lcom/android/browser/BrowserActivity;->mPowerManager:Landroid/os/PowerManager;

    if-nez v1, :cond_1

    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/android/browser/BrowserActivity;->mPowerManager:Landroid/os/PowerManager;

    :cond_1
    iget-object v1, p0, Lcom/android/browser/BrowserActivity;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/browser/BrowserActivity;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    or-int/2addr v0, v1

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getController()Lcom/android/browser/Controller;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    check-cast v0, Lcom/android/browser/Controller;

    return-object v0
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1
    .param p1    # Landroid/view/ActionMode;

    invoke-super {p0, p1}, Landroid/app/Activity;->onActionModeFinished(Landroid/view/ActionMode;)V

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onActionModeFinished(Landroid/view/ActionMode;)V

    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1
    .param p1    # Landroid/view/ActionMode;

    invoke-super {p0, p1}, Landroid/app/Activity;->onActionModeStarted(Landroid/view/ActionMode;)V

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onActionModeStarted(Landroid/view/ActionMode;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/browser/ActivityController;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onConfgurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onContextMenuClosed(Landroid/view/Menu;)V
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onContextMenuClosed(Landroid/view/Menu;)V

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onContextMenuClosed(Landroid/view/Menu;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/android/browser/BrowserActivity;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/browser/BrowserActivity;->shouldIgnoreIntents()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/android/browser/IntentHandler;->handleWebSearchIntent(Landroid/app/Activity;Lcom/android/browser/Controller;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/browser/BrowserActivity;->createController()Lcom/android/browser/Controller;

    move-result-object v1

    iput-object v1, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    if-nez p1, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    :cond_3
    iget-object v1, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v1, v0}, Lcom/android/browser/ActivityController;->start(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/browser/ActivityController;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0}, Lcom/android/browser/ActivityController;->onDestroy()V

    sget-object v0, Lcom/android/browser/stub/NullController;->INSTANCE:Lcom/android/browser/stub/NullController;

    iput-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1, p2}, Lcom/android/browser/ActivityController;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1, p2}, Lcom/android/browser/ActivityController;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1, p2}, Lcom/android/browser/ActivityController;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onLowMemory()V

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0}, Lcom/android/browser/ActivityController;->onLowMemory()V

    return-void
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/Menu;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1, p2}, Lcom/android/browser/ActivityController;->onMenuOpened(ILandroid/view/Menu;)Z

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Intent;

    const/4 v10, -0x1

    invoke-direct {p0}, Lcom/android/browser/BrowserActivity;->shouldIgnoreIntents()Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v8, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    sget-object v9, Lcom/android/browser/stub/NullController;->INSTANCE:Lcom/android/browser/stub/NullController;

    if-ne v8, v9, :cond_2

    const-string v8, "browser/BrowserActivity"

    const-string v9, "onNewIntent for Action_Search Intent reached before finish(), so enter onNewIntent instead of on create"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const-string v8, "notifications"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "browser/BrowserActivity"

    const-string v9, "ACTION_NOTIFICATIONS.equals(intent.getAction())"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v8, "Shown"

    const/4 v9, 0x0

    invoke-interface {v2, v8, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string v8, "TITLE"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "BODY"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v8, "Position"

    invoke-virtual {p1, v8, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string v8, "COUNTER"

    invoke-virtual {p1, v8, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v8, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    if-eqz v8, :cond_3

    invoke-virtual {p0}, Lcom/android/browser/BrowserActivity;->getController()Lcom/android/browser/Controller;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/browser/Controller;->getTabControl()Lcom/android/browser/TabControl;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v4, v6}, Lcom/android/browser/TabControl;->getTab(I)Lcom/android/browser/Tab;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/BrowserActivity;->getController()Lcom/android/browser/Controller;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/browser/Controller;->switchToTab(Lcom/android/browser/Tab;)Z

    invoke-virtual {v3, v7, v0, v1}, Lcom/android/browser/Tab;->onTest(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_3
    const-string v8, "--restart--"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    iget-object v8, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v8, v5}, Lcom/android/browser/ActivityController;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    new-instance v9, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const-class v11, Lcom/android/browser/BrowserActivity;

    invoke-direct {v9, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v10, 0x10000000

    invoke-virtual {v9, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v9

    const-string v10, "state"

    invoke-virtual {v9, v10, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    iget-object v8, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v8, p1}, Lcom/android/browser/ActivityController;->handleNewIntent(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 1
    .param p1    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onOptionsMenuClosed(Landroid/view/Menu;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0}, Lcom/android/browser/ActivityController;->onPause()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0}, Lcom/android/browser/ActivityController;->onResume()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0, p1}, Lcom/android/browser/ActivityController;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/android/browser/BrowserActivity;->mController:Lcom/android/browser/ActivityController;

    invoke-interface {v0}, Lcom/android/browser/ActivityController;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
