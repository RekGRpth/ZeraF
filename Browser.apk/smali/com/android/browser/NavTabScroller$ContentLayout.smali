.class Lcom/android/browser/NavTabScroller$ContentLayout;
.super Landroid/widget/LinearLayout;
.source "NavTabScroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/NavTabScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ContentLayout"
.end annotation


# instance fields
.field mScroller:Lcom/android/browser/NavTabScroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/browser/NavTabScroller;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/browser/NavTabScroller;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/browser/NavTabScroller$ContentLayout;->mScroller:Lcom/android/browser/NavTabScroller;

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    iget-object v2, p0, Lcom/android/browser/NavTabScroller$ContentLayout;->mScroller:Lcom/android/browser/NavTabScroller;

    invoke-virtual {v2}, Lcom/android/browser/NavTabScroller;->getGap()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/browser/NavTabScroller$ContentLayout;->mScroller:Lcom/android/browser/NavTabScroller;

    invoke-virtual {v2}, Lcom/android/browser/NavTabScroller;->isHorizontal()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int v0, v2, v3

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Landroid/view/View;->setMeasuredDimension(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int v0, v2, v3

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0, v2, v0}, Landroid/view/View;->setMeasuredDimension(II)V

    goto :goto_0
.end method
