.class public Lcom/android/browser/AddBookmarkFolderForOP01Menu;
.super Landroid/app/Activity;
.source "AddBookmarkFolderForOP01Menu.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/android/browser/BreadCrumbView$Controller;
.implements Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/AddBookmarkFolderForOP01Menu$EditBookmarkInfoLoader;,
        Lcom/android/browser/AddBookmarkFolderForOP01Menu$EditBookmarkInfo;,
        Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;,
        Lcom/android/browser/AddBookmarkFolderForOP01Menu$AccountsLoader;,
        Lcom/android/browser/AddBookmarkFolderForOP01Menu$FolderAdapter;,
        Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Activity;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/android/browser/BreadCrumbView$Controller;",
        "Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# static fields
.field public static final BOOKMARK_CURRENT_ID:Ljava/lang/String; = "bookmark_current_id"

.field public static final CHECK_FOR_DUPE:Ljava/lang/String; = "check_for_dupe"

.field public static final DEFAULT_FOLDER_ID:J = -0x1L

.field static final EXTRA_EDIT_BOOKMARK:Ljava/lang/String; = "bookmark"

.field static final EXTRA_IS_FOLDER:Ljava/lang/String; = "is_folder"

.field private static final LOADER_ID_ACCOUNTS:I = 0x0

.field private static final LOADER_ID_EDIT_INFO:I = 0x2

.field private static final LOADER_ID_FOLDER_CONTENTS:I = 0x1

.field private static final MAX_CRUMBS_SHOWN:I = 0x2

.field private static final XLOGTAG:Ljava/lang/String; = "browser/AddBookmarkFolderForOP01Menu"


# instance fields
.field private mAccountAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;",
            ">;"
        }
    .end annotation
.end field

.field private mAccountSpinner:Landroid/widget/Spinner;

.field private mAdapter:Lcom/android/browser/AddBookmarkFolderForOP01Menu$FolderAdapter;

.field private mAddNewFolder:Landroid/view/View;

.field private mAddSeparator:Landroid/view/View;

.field private mAddress:Landroid/widget/EditText;

.field private mButton:Landroid/widget/TextView;

.field private mCancelButton:Landroid/view/View;

.field private mCrumbHolder:Landroid/view/View;

.field private mCrumbs:Lcom/android/browser/BreadCrumbView;

.field private mCurrentFolder:J

.field private mDefaultView:Landroid/view/View;

.field private mEditInfoLoaderCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/android/browser/AddBookmarkFolderForOP01Menu$EditBookmarkInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mFakeTitle:Landroid/widget/TextView;

.field private mFakeTitleHolder:Landroid/view/View;

.field private mFolder:Lcom/android/browser/addbookmark/FolderSpinner;

.field private mFolderAdapter:Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

.field private mFolderCancel:Landroid/view/View;

.field private mFolderNamer:Landroid/widget/EditText;

.field private mFolderNamerHolder:Landroid/view/View;

.field private mFolderSelector:Landroid/view/View;

.field private mHeaderIcon:Landroid/graphics/drawable/Drawable;

.field private mIsFolderChanged:Z

.field private mIsFolderNamerShowing:Z

.field private mIsOtherFolderSelected:Z

.field private mIsRecentFolder:Z

.field private mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

.field private mMap:Landroid/os/Bundle;

.field private mOriginalFolder:J

.field private mRemoveLink:Landroid/view/View;

.field private mRootFolder:J

.field private mTitle:Landroid/widget/EditText;

.field private mTopLevelLabel:Landroid/widget/TextView;

.field private mWarningDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mOriginalFolder:J

    iput-boolean v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsFolderChanged:Z

    iput-boolean v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsOtherFolderSelected:Z

    iput-boolean v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsRecentFolder:Z

    new-instance v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$1;

    invoke-direct {v0, p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu$1;-><init>(Lcom/android/browser/AddBookmarkFolderForOP01Menu;)V

    iput-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mEditInfoLoaderCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/AddBookmarkFolderForOP01Menu;)J
    .locals 2
    .param p0    # Lcom/android/browser/AddBookmarkFolderForOP01Menu;

    iget-wide v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/android/browser/AddBookmarkFolderForOP01Menu;)Lcom/android/browser/addbookmark/FolderSpinnerAdapter;
    .locals 1
    .param p0    # Lcom/android/browser/AddBookmarkFolderForOP01Menu;

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderAdapter:Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/browser/AddBookmarkFolderForOP01Menu;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/android/browser/AddBookmarkFolderForOP01Menu;

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/browser/AddBookmarkFolderForOP01Menu;)Landroid/os/Bundle;
    .locals 1
    .param p0    # Lcom/android/browser/AddBookmarkFolderForOP01Menu;

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mMap:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/browser/AddBookmarkFolderForOP01Menu;)Z
    .locals 1
    .param p0    # Lcom/android/browser/AddBookmarkFolderForOP01Menu;

    iget-boolean v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsFolderNamerShowing:Z

    return v0
.end method

.method static synthetic access$500(Lcom/android/browser/AddBookmarkFolderForOP01Menu;)V
    .locals 0
    .param p0    # Lcom/android/browser/AddBookmarkFolderForOP01Menu;

    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->showWarningDialog()V

    return-void
.end method

.method private addFolderToCurrent(Ljava/lang/String;)J
    .locals 10
    .param p1    # Ljava/lang/String;

    const-wide/16 v5, -0x1

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "title"

    invoke-virtual {v4, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "folder"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v2, 0x0

    iget-object v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    invoke-virtual {v7}, Lcom/android/browser/BreadCrumbView;->getTopData()Ljava/lang/Object;

    move-result-object v2

    :cond_0
    if-eqz v2, :cond_2

    check-cast v2, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;

    iget-wide v0, v2, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;->mId:J

    :goto_0
    iget-wide v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iget-boolean v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsRecentFolder:Z

    if-eqz v7, :cond_3

    const-string v7, "browser/AddBookmarkFolderForOP01Menu"

    const-string v8, "recentFolder"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "parent"

    iget-wide v8, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_1
    const-string v7, "browser/AddBookmarkFolderForOP01Menu"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "values:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "parent"

    invoke-virtual {v4, v9}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Landroid/provider/BrowserContract$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v5

    :cond_1
    return-wide v5

    :cond_2
    iget-wide v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    goto :goto_0

    :cond_3
    iget-boolean v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsFolderChanged:Z

    if-eqz v7, :cond_4

    iget-boolean v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsOtherFolderSelected:Z

    if-nez v7, :cond_5

    :cond_4
    iget-wide v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mOriginalFolder:J

    cmp-long v7, v7, v5

    if-eqz v7, :cond_5

    const-string v7, "browser/AddBookmarkFolderForOP01Menu"

    const-string v8, "not changed"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "parent"

    iget-wide v8, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mOriginalFolder:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    :cond_5
    const-string v7, "browser/AddBookmarkFolderForOP01Menu"

    const-string v8, "defaultFolder"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "parent"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1
.end method

.method private completeOrCancelFolderNaming(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x0

    if-nez p1, :cond_0

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->addFolderToCurrent(Ljava/lang/String;)J

    move-result-wide v0

    invoke-direct {p0, v2, v0, v1}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->descendInto(Ljava/lang/String;J)V

    :cond_0
    invoke-direct {p0, v5}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->setShowFolderNamer(Z)V

    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method private descendInto(Ljava/lang/String;J)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    new-instance v1, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;

    invoke-direct {v1, p1, p2, p3}, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, p1, v1}, Lcom/android/browser/BreadCrumbView;->pushView(Ljava/lang/String;Ljava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    invoke-virtual {v0}, Lcom/android/browser/BreadCrumbView;->notifyController()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c001e

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private displayToastForExistingFolder()V
    .locals 3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c001e

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public static getIdFromData(Ljava/lang/Object;)J
    .locals 3
    .param p0    # Ljava/lang/Object;

    if-nez p0, :cond_0

    const-wide/16 v1, 0x1

    :goto_0
    return-wide v1

    :cond_0
    move-object v0, p0

    check-cast v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;

    iget-wide v1, v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;->mId:J

    goto :goto_0
.end method

.method private getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method private getNameFromId(J)Ljava/lang/String;
    .locals 9
    .param p1    # J

    const-string v7, ""

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/BrowserContract$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "title"

    aput-object v4, v2, v3

    const-string v3, "_id = ? AND deleted = ? AND folder = ? "

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    const-string v8, "0"

    aput-object v8, v4, v5

    const/4 v5, 0x2

    const-string v8, "1"

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    goto :goto_0

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    const-string v0, "browser/AddBookmarkFolderForOP01Menu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "title :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v7

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getUriForFolder(J)Landroid/net/Uri;
    .locals 4
    .param p1    # J

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;

    iget-wide v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    cmp-long v1, p1, v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    sget-object v1, Landroid/provider/BrowserContract$Bookmarks;->CONTENT_URI_DEFAULT_FOLDER:Landroid/net/Uri;

    iget-object v2, v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;->mAccountType:Ljava/lang/String;

    iget-object v3, v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;->mAccountName:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/android/browser/BookmarksLoader;->addAccount(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p1, p2}, Landroid/provider/BrowserContract$Bookmarks;->buildFolderUri(J)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method private onCurrentFolderFound()V
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iget-wide v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolder:Lcom/android/browser/addbookmark/FolderSpinner;

    invoke-virtual {v1, v5}, Lcom/android/browser/addbookmark/FolderSpinner;->setSelectionIgnoringSelectionChange(I)V

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-void

    :cond_0
    invoke-direct {p0, v5}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->setShowBookmarkIcon(Z)V

    goto :goto_0
.end method

.method private onRootFolderFound(J)V
    .locals 2
    .param p1    # J

    iput-wide p1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    iget-wide v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    iput-wide v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->setupTopCrumb()V

    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->onCurrentFolderFound()V

    return-void
.end method

.method private save()Z
    .locals 9

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_0

    move v0, v5

    :goto_0
    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v0, :cond_1

    iget-object v5, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mTitle:Landroid/widget/EditText;

    const v7, 0x7f0c0089

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    :goto_1
    return v6

    :cond_0
    move v0, v6

    goto :goto_0

    :cond_1
    invoke-direct {p0, v4}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->addFolderToCurrent(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v7, -0x1

    cmp-long v7, v1, v7

    if-nez v7, :cond_2

    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->displayToastForExistingFolder()V

    goto :goto_1

    :cond_2
    const/4 v6, -0x1

    invoke-virtual {p0, v6}, Landroid/app/Activity;->setResult(I)V

    move v6, v5

    goto :goto_1
.end method

.method private setShowBookmarkIcon(Z)V
    .locals 3
    .param p1    # Z

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    :goto_0
    iget-object v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mTopLevelLabel:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private setShowFolderNamer(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsFolderNamerShowing:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsFolderNamerShowing:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamerHolder:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    :goto_0
    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAdapter:Lcom/android/browser/AddBookmarkFolderForOP01Menu$FolderAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamerHolder:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    goto :goto_0
.end method

.method private setupTopCrumb()V
    .locals 6

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    invoke-virtual {v1}, Lcom/android/browser/BreadCrumbView;->clear()V

    const v1, 0x7f0c0096

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    const/4 v2, 0x0

    new-instance v3, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;

    iget-wide v4, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    invoke-direct {v3, v0, v4, v5}, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/browser/BreadCrumbView;->pushView(Ljava/lang/String;ZLjava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mTopLevelLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mTopLevelLabel:Landroid/widget/TextView;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    return-void
.end method

.method private showRemoveButton()V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x7f0d0032

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0d0033

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRemoveLink:Landroid/view/View;

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRemoveLink:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRemoveLink:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private showWarningDialog()V
    .locals 3

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mWarningDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mWarningDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mWarningDialog:Landroid/app/AlertDialog;

    const v1, 0x7f0c0039

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mWarningDialog:Landroid/app/AlertDialog;

    const v1, 0x7f0c0038

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mWarningDialog:Landroid/app/AlertDialog;

    const v1, 0x7f0c003a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/browser/AddBookmarkFolderForOP01Menu$3;

    invoke-direct {v2, p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu$3;-><init>(Lcom/android/browser/AddBookmarkFolderForOP01Menu;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mWarningDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method

.method private switchToDefaultView(Z)V
    .locals 8
    .param p1    # Z

    const/16 v4, 0x8

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderSelector:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mDefaultView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbHolder:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFakeTitleHolder:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_2

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    invoke-virtual {v3}, Lcom/android/browser/BreadCrumbView;->getTopData()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;

    iget-wide v3, v1, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;->mId:J

    iput-wide v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iget-wide v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iget-wide v5, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolder:Lcom/android/browser/addbookmark/FolderSpinner;

    invoke-virtual {v3, v7}, Lcom/android/browser/addbookmark/FolderSpinner;->setSelectionIgnoringSelectionChange(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderAdapter:Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

    iget-object v4, v1, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;->mName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->setOtherFolderDisplayText(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v3, "browser/AddBookmarkFolderForOP01Menu"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mCurrentFolder:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iget-wide v5, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolder:Lcom/android/browser/addbookmark/FolderSpinner;

    invoke-virtual {v3, v7}, Lcom/android/browser/addbookmark/FolderSpinner;->setSelectionIgnoringSelectionChange(I)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    invoke-virtual {v3}, Lcom/android/browser/BreadCrumbView;->getTopData()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    move-object v3, v0

    check-cast v3, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;

    iget-wide v3, v3, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;->mId:J

    iget-wide v5, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderAdapter:Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

    check-cast v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;

    iget-object v4, v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;->mName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->setOtherFolderDisplayText(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->setupTopCrumb()V

    invoke-virtual {p0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method private switchToFolderSelector()V
    .locals 3

    const/16 v1, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFakeTitleHolder:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mDefaultView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderSelector:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbHolder:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method


# virtual methods
.method moveCursorToFolder(Landroid/database/Cursor;JI)V
    .locals 5
    .param p1    # Landroid/database/Cursor;
    .param p2    # J
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/AssertionError;
        }
    .end annotation

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "No folders in the database!"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    :cond_0
    invoke-interface {p1, p4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    cmp-long v2, v0, p2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Folder(id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") holding this bookmark does not exist!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    :cond_2
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mButton:Landroid/widget/TextView;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderSelector:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsFolderNamerShowing:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->completeOrCancelFolderNaming(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v1}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->switchToDefaultView(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->save()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCancelButton:Landroid/view/View;

    if-ne p1, v0, :cond_6

    iget-boolean v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsFolderNamerShowing:Z

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->completeOrCancelFolderNaming(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderSelector:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0, v2}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->switchToDefaultView(Z)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderCancel:Landroid/view/View;

    if-ne p1, v0, :cond_0

    invoke-direct {p0, v1}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->completeOrCancelFolderNaming(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/4 v12, 0x0

    const v9, 0x7f0c0073

    const/16 v8, 0x8

    const/4 v11, 0x0

    const/4 v10, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v10}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mMap:Landroid/os/Bundle;

    const v6, 0x7f040012

    invoke-virtual {p0, v6}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f0d0031

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFakeTitle:Landroid/widget/TextView;

    const v6, 0x7f0d0030

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFakeTitleHolder:Landroid/view/View;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFakeTitle:Landroid/widget/TextView;

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mWarningDialog:Landroid/app/AlertDialog;

    const v6, 0x7f0d0007

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mTitle:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-array v0, v10, [Landroid/text/InputFilter;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0009

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    new-instance v6, Lcom/android/browser/AddBookmarkFolderForOP01Menu$2;

    invoke-direct {v6, p0, v3, v3}, Lcom/android/browser/AddBookmarkFolderForOP01Menu$2;-><init>(Lcom/android/browser/AddBookmarkFolderForOP01Menu;II)V

    aput-object v6, v0, v11

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    const v6, 0x7f0d0038

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAddress:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAddress:Landroid/widget/EditText;

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    const v6, 0x7f0d0036

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    const v6, 0x7f0d003f

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mButton:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mButton:Landroid/widget/TextView;

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v6, 0x7f0d003e

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCancelButton:Landroid/view/View;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCancelButton:Landroid/view/View;

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v6, 0x7f0d003a

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/browser/addbookmark/FolderSpinner;

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolder:Lcom/android/browser/addbookmark/FolderSpinner;

    new-instance v6, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

    invoke-direct {v6, p0, v11}, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderAdapter:Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolder:Lcom/android/browser/addbookmark/FolderSpinner;

    iget-object v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderAdapter:Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolder:Lcom/android/browser/addbookmark/FolderSpinner;

    invoke-virtual {v6, p0}, Lcom/android/browser/addbookmark/FolderSpinner;->setOnSetSelectionListener(Lcom/android/browser/addbookmark/FolderSpinner$OnSetSelectionListener;)V

    const v6, 0x7f0d0034

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mDefaultView:Landroid/view/View;

    const v6, 0x7f0d003b

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderSelector:Landroid/view/View;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f040023

    invoke-virtual {v6, v7, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamerHolder:Landroid/view/View;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamerHolder:Landroid/view/View;

    const v7, 0x7f0d0062

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    invoke-virtual {v6, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamerHolder:Landroid/view/View;

    const v7, 0x7f0d0063

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderCancel:Landroid/view/View;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderCancel:Landroid/view/View;

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v6, 0x7f0d002f

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAddNewFolder:Landroid/view/View;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAddNewFolder:Landroid/view/View;

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    const v6, 0x7f0d002e

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAddSeparator:Landroid/view/View;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAddSeparator:Landroid/view/View;

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    const v6, 0x7f0d0024

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/browser/BreadCrumbView;

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    invoke-virtual {v6, v10}, Lcom/android/browser/BreadCrumbView;->setUseBackButton(Z)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    invoke-virtual {v6, p0}, Lcom/android/browser/BreadCrumbView;->setController(Lcom/android/browser/BreadCrumbView$Controller;)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02002e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    const v6, 0x7f0d0016

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbHolder:Landroid/view/View;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/android/browser/BreadCrumbView;->setMaxVisible(I)V

    new-instance v6, Lcom/android/browser/AddBookmarkFolderForOP01Menu$FolderAdapter;

    invoke-direct {v6, p0, p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu$FolderAdapter;-><init>(Lcom/android/browser/AddBookmarkFolderForOP01Menu;Landroid/content/Context;)V

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAdapter:Lcom/android/browser/AddBookmarkFolderForOP01Menu$FolderAdapter;

    const v6, 0x7f0d003c

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/browser/AddBookmarkPage$CustomListView;

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    const v6, 0x7f0d003d

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    invoke-virtual {v6, v1}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    iget-object v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAdapter:Lcom/android/browser/AddBookmarkFolderForOP01Menu$FolderAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    invoke-virtual {v6, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    iget-object v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    invoke-virtual {v6, v7}, Lcom/android/browser/AddBookmarkPage$CustomListView;->addEditText(Landroid/widget/EditText;)V

    new-instance v6, Landroid/widget/ArrayAdapter;

    const v7, 0x1090008

    invoke-direct {v6, p0, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    const v7, 0x1090009

    invoke-virtual {v6, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const v6, 0x7f0d0039

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Spinner;

    iput-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountSpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountSpinner:Landroid/widget/Spinner;

    iget-object v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, p0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->isInTouchMode()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mButton:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_0
    invoke-direct {p0, v11}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->setShowFolderNamer(Z)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(I)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mListView:Lcom/android/browser/AddBookmarkPage$CustomListView;

    invoke-virtual {v2, v6}, Landroid/view/inputmethod/InputMethodManager;->focusIn(Landroid/view/View;)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    invoke-virtual {v2, v6, v10}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "parent"

    const-wide/16 v8, -0x1

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iget-wide v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iput-wide v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mOriginalFolder:J

    const-string v6, "browser/AddBookmarkFolderForOP01Menu"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mCurrentFolder:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mRootFolder:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    iget-wide v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    const-wide/16 v8, 0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolder:Lcom/android/browser/addbookmark/FolderSpinner;

    invoke-virtual {v6, v10}, Lcom/android/browser/addbookmark/FolderSpinner;->setSelectionIgnoringSelectionChange(I)V

    iget-object v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderAdapter:Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

    iget-wide v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mOriginalFolder:J

    invoke-direct {p0, v7, v8}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->getNameFromId(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->setOtherFolderDisplayText(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v6

    invoke-virtual {v6, v11, v12, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Asking for nonexistant loader!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    new-instance v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$AccountsLoader;

    invoke-direct {v0, p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu$AccountsLoader;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    :pswitch_1
    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v3, v2

    const-string v0, "title"

    aput-object v0, v3, v6

    const/4 v0, 0x2

    const-string v1, "folder"

    aput-object v1, v3, v0

    const-string v4, "folder != 0 AND _id != ?"

    new-array v5, v6, [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mMap:Landroid/os/Bundle;

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCrumbs:Lcom/android/browser/BreadCrumbView;

    invoke-virtual {v0}, Lcom/android/browser/BreadCrumbView;->getTopData()Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_0

    check-cast v9, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;

    iget-wide v7, v9, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;->mId:J

    :goto_1
    new-instance v0, Landroid/content/CursorLoader;

    invoke-direct {p0, v7, v8}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->getUriForFolder(J)Landroid/net/Uri;

    move-result-object v2

    const-string v6, "_id ASC"

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-wide v7, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderNamer:Landroid/widget/EditText;

    if-ne p1, v2, :cond_1

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_0

    if-nez p2, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-direct {p0, v1}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->completeOrCancelFolderNaming(Z)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const v1, 0x1020014

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p4, p5}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->descendInto(Ljava/lang/String;J)V

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountSpinner:Landroid/widget/Spinner;

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;

    iget-wide v0, v2, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;->rootFolderId:J

    iget-wide v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0, v1}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->onRootFolderFound(J)V

    iget-object v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderAdapter:Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

    invoke-virtual {v2}, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->clearRecentFolder()V

    :cond_0
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    new-instance v1, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;

    invoke-direct {v1, p0, p2}, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/LoaderManager;->destroyLoader(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mEditInfoLoaderCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAdapter:Lcom/android/browser/AddBookmarkFolderForOP01Menu$FolderAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAdapter:Lcom/android/browser/AddBookmarkFolderForOP01Menu$FolderAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method

.method public onSetSelection(J)V
    .locals 7
    .param p1    # J

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v2, "browser/AddBookmarkFolderForOP01Menu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSetSelection id :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    long-to-int v0, p1

    iput-boolean v5, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsFolderChanged:Z

    iput-boolean v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsOtherFolderSelected:Z

    iput-boolean v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsRecentFolder:Z

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-wide v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mRootFolder:J

    iput-wide v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iget-wide v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iput-wide v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mOriginalFolder:J

    goto :goto_0

    :pswitch_2
    iput-boolean v5, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsOtherFolderSelected:Z

    invoke-direct {p0}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->switchToFolderSelector()V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mFolderAdapter:Lcom/android/browser/addbookmark/FolderSpinnerAdapter;

    invoke-virtual {v2}, Lcom/android/browser/addbookmark/FolderSpinnerAdapter;->recentFolderId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iget-wide v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mCurrentFolder:J

    iput-wide v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mOriginalFolder:J

    iput-boolean v5, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsRecentFolder:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v5, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onTop(Lcom/android/browser/BreadCrumbView;ILjava/lang/Object;)V
    .locals 7
    .param p1    # Lcom/android/browser/BreadCrumbView;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    const/4 v5, 0x1

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object v2, p3

    check-cast v2, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;

    iget-wide v0, v2, Lcom/android/browser/AddBookmarkFolderForOP01Menu$Folder;->mId:J

    invoke-virtual {p0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v3

    check-cast v3, Landroid/content/CursorLoader;

    invoke-direct {p0, v0, v1}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->getUriForFolder(J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/content/CursorLoader;->setUri(Landroid/net/Uri;)V

    invoke-virtual {v3}, Landroid/content/Loader;->forceLoad()V

    iget-boolean v6, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mIsFolderNamerShowing:Z

    if-eqz v6, :cond_1

    invoke-direct {p0, v5}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->completeOrCancelFolderNaming(Z)V

    :cond_1
    if-ne p2, v5, :cond_2

    :goto_1
    invoke-direct {p0, v5}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->setShowBookmarkIcon(Z)V

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method setAccount(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;

    iget-object v2, v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;->mAccountName:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;->mAccountType:Ljava/lang/String;

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->mAccountSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-wide v2, v0, Lcom/android/browser/AddBookmarkFolderForOP01Menu$BookmarkAccount;->rootFolderId:J

    invoke-direct {p0, v2, v3}, Lcom/android/browser/AddBookmarkFolderForOP01Menu;->onRootFolderFound(J)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
