.class Lcom/android/browser/SystemAllowGeolocationOrigins;
.super Ljava/lang/Object;
.source "SystemAllowGeolocationOrigins.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/SystemAllowGeolocationOrigins$SettingObserver;
    }
.end annotation


# static fields
.field private static final LAST_READ_ALLOW_GEOLOCATION_ORIGINS:Ljava/lang/String; = "last_read_allow_geolocation_origins"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mMaybeApplySetting:Ljava/lang/Runnable;

.field private final mSettingObserver:Lcom/android/browser/SystemAllowGeolocationOrigins$SettingObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/browser/SystemAllowGeolocationOrigins$1;

    invoke-direct {v0, p0}, Lcom/android/browser/SystemAllowGeolocationOrigins$1;-><init>(Lcom/android/browser/SystemAllowGeolocationOrigins;)V

    iput-object v0, p0, Lcom/android/browser/SystemAllowGeolocationOrigins;->mMaybeApplySetting:Ljava/lang/Runnable;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/SystemAllowGeolocationOrigins;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/browser/SystemAllowGeolocationOrigins$SettingObserver;

    invoke-direct {v0, p0}, Lcom/android/browser/SystemAllowGeolocationOrigins$SettingObserver;-><init>(Lcom/android/browser/SystemAllowGeolocationOrigins;)V

    iput-object v0, p0, Lcom/android/browser/SystemAllowGeolocationOrigins;->mSettingObserver:Lcom/android/browser/SystemAllowGeolocationOrigins$SettingObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/SystemAllowGeolocationOrigins;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/browser/SystemAllowGeolocationOrigins;

    invoke-direct {p0}, Lcom/android/browser/SystemAllowGeolocationOrigins;->getSystemSetting()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/android/browser/SystemAllowGeolocationOrigins;->parseAllowGeolocationOrigins(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/browser/SystemAllowGeolocationOrigins;Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/android/browser/SystemAllowGeolocationOrigins;
    .param p1    # Ljava/util/Set;
    .param p2    # Ljava/util/Set;

    invoke-direct {p0, p1, p2}, Lcom/android/browser/SystemAllowGeolocationOrigins;->setMinus(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/browser/SystemAllowGeolocationOrigins;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/android/browser/SystemAllowGeolocationOrigins;
    .param p1    # Ljava/util/Set;

    invoke-direct {p0, p1}, Lcom/android/browser/SystemAllowGeolocationOrigins;->removeOrigins(Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/browser/SystemAllowGeolocationOrigins;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/android/browser/SystemAllowGeolocationOrigins;
    .param p1    # Ljava/util/Set;

    invoke-direct {p0, p1}, Lcom/android/browser/SystemAllowGeolocationOrigins;->addOrigins(Ljava/util/Set;)V

    return-void
.end method

.method private addOrigins(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Landroid/webkit/GeolocationPermissions;->getInstance()Landroid/webkit/GeolocationPermissions;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/webkit/GeolocationPermissions;->allow(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getSystemSetting()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/android/browser/SystemAllowGeolocationOrigins;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "allowed_geolocation_origins"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method private static parseAllowGeolocationOrigins(Ljava/lang/String;)Ljava/util/HashSet;
    .locals 6
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "\\s+"

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method private removeOrigins(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Landroid/webkit/GeolocationPermissions;->getInstance()Landroid/webkit/GeolocationPermissions;

    move-result-object v2

    new-instance v3, Lcom/android/browser/SystemAllowGeolocationOrigins$2;

    invoke-direct {v3, p0, v1}, Lcom/android/browser/SystemAllowGeolocationOrigins$2;-><init>(Lcom/android/browser/SystemAllowGeolocationOrigins;Ljava/lang/String;)V

    invoke-virtual {v2, v1, v3}, Landroid/webkit/GeolocationPermissions;->getAllowed(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setMinus(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TA;>;",
            "Ljava/util/Set",
            "<TA;>;)",
            "Ljava/util/Set",
            "<TA;>;"
        }
    .end annotation

    new-instance v2, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v2
.end method


# virtual methods
.method maybeApplySettingAsync()V
    .locals 1

    iget-object v0, p0, Lcom/android/browser/SystemAllowGeolocationOrigins;->mMaybeApplySetting:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/android/browser/BackgroundHandler;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public start()V
    .locals 4

    const-string v1, "allowed_geolocation_origins"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/SystemAllowGeolocationOrigins;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/browser/SystemAllowGeolocationOrigins;->mSettingObserver:Lcom/android/browser/SystemAllowGeolocationOrigins$SettingObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0}, Lcom/android/browser/SystemAllowGeolocationOrigins;->maybeApplySettingAsync()V

    return-void
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Lcom/android/browser/SystemAllowGeolocationOrigins;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/SystemAllowGeolocationOrigins;->mSettingObserver:Lcom/android/browser/SystemAllowGeolocationOrigins$SettingObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method
