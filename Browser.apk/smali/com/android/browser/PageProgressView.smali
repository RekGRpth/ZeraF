.class public Lcom/android/browser/PageProgressView;
.super Landroid/widget/ImageView;
.source "PageProgressView.java"


# static fields
.field private static final DELAY:I = 0x28

.field public static final MAX_PROGRESS:I = 0x2710

.field private static final MSG_UPDATE:I = 0x2a

.field private static final STEPS:I = 0xa


# instance fields
.field private mBounds:Landroid/graphics/Rect;

.field private mCurrentProgress:I

.field private mHandler:Landroid/os/Handler;

.field private mIncrement:I

.field private mTargetProgress:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/android/browser/PageProgressView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/android/browser/PageProgressView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/android/browser/PageProgressView;->init(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/PageProgressView;)I
    .locals 1
    .param p0    # Lcom/android/browser/PageProgressView;

    iget v0, p0, Lcom/android/browser/PageProgressView;->mCurrentProgress:I

    return v0
.end method

.method static synthetic access$002(Lcom/android/browser/PageProgressView;I)I
    .locals 0
    .param p0    # Lcom/android/browser/PageProgressView;
    .param p1    # I

    iput p1, p0, Lcom/android/browser/PageProgressView;->mCurrentProgress:I

    return p1
.end method

.method static synthetic access$100(Lcom/android/browser/PageProgressView;)I
    .locals 1
    .param p0    # Lcom/android/browser/PageProgressView;

    iget v0, p0, Lcom/android/browser/PageProgressView;->mTargetProgress:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/browser/PageProgressView;)I
    .locals 1
    .param p0    # Lcom/android/browser/PageProgressView;

    iget v0, p0, Lcom/android/browser/PageProgressView;->mIncrement:I

    return v0
.end method

.method static synthetic access$300(Lcom/android/browser/PageProgressView;)Landroid/graphics/Rect;
    .locals 1
    .param p0    # Lcom/android/browser/PageProgressView;

    iget-object v0, p0, Lcom/android/browser/PageProgressView;->mBounds:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/browser/PageProgressView;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/browser/PageProgressView;

    iget-object v0, p0, Lcom/android/browser/PageProgressView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/android/browser/PageProgressView;->mBounds:Landroid/graphics/Rect;

    iput v1, p0, Lcom/android/browser/PageProgressView;->mCurrentProgress:I

    iput v1, p0, Lcom/android/browser/PageProgressView;->mTargetProgress:I

    new-instance v0, Lcom/android/browser/PageProgressView$1;

    invoke-direct {v0, p0}, Lcom/android/browser/PageProgressView$1;-><init>(Lcom/android/browser/PageProgressView;)V

    iput-object v0, p0, Lcom/android/browser/PageProgressView;->mHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/PageProgressView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/browser/PageProgressView;->mBounds:Landroid/graphics/Rect;

    iput v3, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/browser/PageProgressView;->mBounds:Landroid/graphics/Rect;

    sub-int v1, p4, p2

    iget v2, p0, Lcom/android/browser/PageProgressView;->mCurrentProgress:I

    mul-int/2addr v1, v2

    div-int/lit16 v1, v1, 0x2710

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/browser/PageProgressView;->mBounds:Landroid/graphics/Rect;

    iput v3, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/browser/PageProgressView;->mBounds:Landroid/graphics/Rect;

    sub-int v1, p5, p3

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method setProgress(I)V
    .locals 3
    .param p1    # I

    const/16 v2, 0x2a

    iget v0, p0, Lcom/android/browser/PageProgressView;->mTargetProgress:I

    iput v0, p0, Lcom/android/browser/PageProgressView;->mCurrentProgress:I

    iput p1, p0, Lcom/android/browser/PageProgressView;->mTargetProgress:I

    iget v0, p0, Lcom/android/browser/PageProgressView;->mTargetProgress:I

    iget v1, p0, Lcom/android/browser/PageProgressView;->mCurrentProgress:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/android/browser/PageProgressView;->mIncrement:I

    iget-object v0, p0, Lcom/android/browser/PageProgressView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/browser/PageProgressView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
