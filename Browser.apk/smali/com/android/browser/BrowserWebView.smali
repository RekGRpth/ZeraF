.class public Lcom/android/browser/BrowserWebView;
.super Landroid/webkit/WebView;
.source "BrowserWebView.java"

# interfaces
.implements Landroid/webkit/WebViewClassic$TitleBarDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/BrowserWebView$OnScrollChangedListener;
    }
.end annotation


# instance fields
.field private mBackgroundRemoved:Z

.field private mOnScrollChangedListener:Lcom/android/browser/BrowserWebView$OnScrollChangedListener;

.field private mTitleBar:Lcom/android/browser/TitleBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/BrowserWebView;->mBackgroundRemoved:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/BrowserWebView;->mBackgroundRemoved:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/util/Map;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p5}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/util/Map;Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/BrowserWebView;->mBackgroundRemoved:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .param p4    # Z

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/BrowserWebView;->mBackgroundRemoved:Z

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    invoke-static {}, Lcom/android/browser/BrowserSettings;->getInstance()Lcom/android/browser/BrowserSettings;

    move-result-object v0

    invoke-virtual {p0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/browser/BrowserSettings;->stopManagingSettings(Landroid/webkit/WebSettings;)V

    invoke-super {p0}, Landroid/webkit/WebView;->destroy()V

    return-void
.end method

.method public drawContent(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p0, p1}, Lcom/android/browser/BrowserWebView;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public getTitleHeight()I
    .locals 1

    iget-object v0, p0, Lcom/android/browser/BrowserWebView;->mTitleBar:Lcom/android/browser/TitleBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/BrowserWebView;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v0}, Lcom/android/browser/TitleBar;->getEmbeddedHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTitleBar()Z
    .locals 1

    iget-object v0, p0, Lcom/android/browser/BrowserWebView;->mTitleBar:Lcom/android/browser/TitleBar;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/webkit/WebView;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/browser/BrowserWebView;->mBackgroundRemoved:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/browser/BrowserWebView;->mBackgroundRemoved:Z

    new-instance v0, Lcom/android/browser/BrowserWebView$1;

    invoke-direct {v0, p0}, Lcom/android/browser/BrowserWebView$1;-><init>(Lcom/android/browser/BrowserWebView;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;->onScrollChanged(IIII)V

    iget-object v0, p0, Lcom/android/browser/BrowserWebView;->mTitleBar:Lcom/android/browser/TitleBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/BrowserWebView;->mTitleBar:Lcom/android/browser/TitleBar;

    invoke-virtual {v0}, Lcom/android/browser/TitleBar;->onScrollChanged()V

    :cond_0
    iget-object v0, p0, Lcom/android/browser/BrowserWebView;->mOnScrollChangedListener:Lcom/android/browser/BrowserWebView$OnScrollChangedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/browser/BrowserWebView;->mOnScrollChangedListener:Lcom/android/browser/BrowserWebView$OnScrollChangedListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/browser/BrowserWebView$OnScrollChangedListener;->onScrollChanged(IIII)V

    :cond_1
    return-void
.end method

.method public onSetEmbeddedTitleBar(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public setOnScrollChangedListener(Lcom/android/browser/BrowserWebView$OnScrollChangedListener;)V
    .locals 0
    .param p1    # Lcom/android/browser/BrowserWebView$OnScrollChangedListener;

    iput-object p1, p0, Lcom/android/browser/BrowserWebView;->mOnScrollChangedListener:Lcom/android/browser/BrowserWebView$OnScrollChangedListener;

    return-void
.end method

.method public setTitleBar(Lcom/android/browser/TitleBar;)V
    .locals 0
    .param p1    # Lcom/android/browser/TitleBar;

    iput-object p1, p0, Lcom/android/browser/BrowserWebView;->mTitleBar:Lcom/android/browser/TitleBar;

    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    return v0
.end method
