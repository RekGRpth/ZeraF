.class public Lcom/android/browser/PreloadController;
.super Ljava/lang/Object;
.source "PreloadController.java"

# interfaces
.implements Lcom/android/browser/WebViewController;


# static fields
.field private static final LOGD_ENABLED:Z = false

.field private static final LOGTAG:Ljava/lang/String; = "PreloadController"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/PreloadController;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public attachSubWindow(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public bookmarkedStatusHasChanged(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public canAllocateMemory()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public closeTab(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public createSubWindow(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public dismissSubWindow(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public doUpdateVisitedHistory(Lcom/android/browser/Tab;Z)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Z

    return-void
.end method

.method public endActionMode()V
    .locals 0

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/PreloadController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDefaultVideoPoster()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTabControl()Lcom/android/browser/TabControl;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoLoadingProgressView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getVisitedHistory(Landroid/webkit/ValueCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public getWebViewFactory()Lcom/android/browser/WebViewFactory;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public hideAutoLogin(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public hideCustomView()V
    .locals 0

    return-void
.end method

.method public onDownloadStart(Lcom/android/browser/Tab;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # J

    return-void
.end method

.method public onFavicon(Lcom/android/browser/Tab;Landroid/webkit/WebView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Landroid/webkit/WebView;
    .param p3    # Landroid/graphics/Bitmap;

    return-void
.end method

.method public onPageFinished(Lcom/android/browser/Tab;)V
    .locals 1
    .param p1    # Lcom/android/browser/Tab;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/browser/Tab;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    :cond_0
    return-void
.end method

.method public onPageStarted(Lcom/android/browser/Tab;Landroid/webkit/WebView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Landroid/webkit/WebView;
    .param p3    # Landroid/graphics/Bitmap;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/webkit/WebView;->clearHistory()V

    :cond_0
    return-void
.end method

.method public onProgressChanged(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public onReceivedHttpAuthRequest(Lcom/android/browser/Tab;Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Landroid/webkit/WebView;
    .param p3    # Landroid/webkit/HttpAuthHandler;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    return-void
.end method

.method public onReceivedTitle(Lcom/android/browser/Tab;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onSetWebView(Lcom/android/browser/Tab;Landroid/webkit/WebView;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Landroid/webkit/WebView;

    return-void
.end method

.method public onShowPopupWindowAttempt(Lcom/android/browser/Tab;ZLandroid/os/Message;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Z
    .param p3    # Landroid/os/Message;

    return-void
.end method

.method public onUnhandledKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onUpdatedSecurityState(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public onUserCanceledSsl(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public openTab(Ljava/lang/String;Lcom/android/browser/Tab;ZZ)Lcom/android/browser/Tab;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/browser/Tab;
    .param p3    # Z
    .param p4    # Z

    const/4 v0, 0x0

    return-object v0
.end method

.method public openTab(Ljava/lang/String;ZZZ)Lcom/android/browser/Tab;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z

    const/4 v0, 0x0

    return-object v0
.end method

.method public setupAutoFill(Landroid/os/Message;)V
    .locals 0
    .param p1    # Landroid/os/Message;

    return-void
.end method

.method public shouldCaptureThumbnails()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public shouldOverrideKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public shouldOverrideUrlLoading(Lcom/android/browser/Tab;Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Landroid/webkit/WebView;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public shouldShowErrorConsole()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public showAutoLogin(Lcom/android/browser/Tab;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;

    return-void
.end method

.method public showCustomView(Lcom/android/browser/Tab;Landroid/view/View;ILandroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 0
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # Landroid/webkit/WebChromeClient$CustomViewCallback;

    return-void
.end method

.method public showSslCertificateOnError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/webkit/SslErrorHandler;
    .param p3    # Landroid/net/http/SslError;

    return-void
.end method

.method public switchToTab(Lcom/android/browser/Tab;)Z
    .locals 1
    .param p1    # Lcom/android/browser/Tab;

    const/4 v0, 0x0

    return v0
.end method
