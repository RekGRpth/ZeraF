.class Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView;
.super Landroid/widget/ListView;
.source "ErrorConsoleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/ErrorConsoleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ErrorConsoleListView"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;
    }
.end annotation


# instance fields
.field private mConsoleMessages:Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;

    invoke-direct {v0, p1}, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView;->mConsoleMessages:Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;

    iget-object v0, p0, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView;->mConsoleMessages:Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;

    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method


# virtual methods
.method public addErrorMessage(Landroid/webkit/ConsoleMessage;)V
    .locals 1
    .param p1    # Landroid/webkit/ConsoleMessage;

    iget-object v0, p0, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView;->mConsoleMessages:Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;

    invoke-virtual {v0, p1}, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;->add(Landroid/webkit/ConsoleMessage;)V

    iget-object v0, p0, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView;->mConsoleMessages:Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;

    invoke-virtual {v0}, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;->getCount()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setSelection(I)V

    return-void
.end method

.method public clearErrorMessages()V
    .locals 1

    iget-object v0, p0, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView;->mConsoleMessages:Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;

    invoke-virtual {v0}, Lcom/android/browser/ErrorConsoleView$ErrorConsoleListView$ErrorConsoleMessageList;->clear()V

    return-void
.end method
