.class public Lcom/android/browser/TabBar;
.super Landroid/widget/LinearLayout;
.source "TabBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/TabBar$TabView;
    }
.end annotation


# static fields
.field private static final PROGRESS_MAX:I = 0x64


# instance fields
.field private mActiveDrawable:Landroid/graphics/drawable/Drawable;

.field private final mActiveMatrix:Landroid/graphics/Matrix;

.field private mActiveShader:Landroid/graphics/BitmapShader;

.field private final mActiveShaderPaint:Landroid/graphics/Paint;

.field private mActivity:Landroid/app/Activity;

.field private mAddTabOverlap:I

.field private mButtonWidth:I

.field private mCurrentTextureHeight:I

.field private mCurrentTextureWidth:I

.field private final mFocusPaint:Landroid/graphics/Paint;

.field private mInactiveDrawable:Landroid/graphics/drawable/Drawable;

.field private final mInactiveMatrix:Landroid/graphics/Matrix;

.field private mInactiveShader:Landroid/graphics/BitmapShader;

.field private final mInactiveShaderPaint:Landroid/graphics/Paint;

.field private mNewTab:Landroid/widget/ImageButton;

.field private mTabControl:Lcom/android/browser/TabControl;

.field private mTabMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/android/browser/Tab;",
            "Lcom/android/browser/TabBar$TabView;",
            ">;"
        }
    .end annotation
.end field

.field private mTabOverlap:I

.field private mTabSliceWidth:I

.field private mTabWidth:I

.field private mTabs:Lcom/android/browser/TabScrollView;

.field private mUi:Lcom/android/browser/XLargeUi;

.field private mUiController:Lcom/android/browser/UiController;

.field private mUseQuickControls:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/android/browser/UiController;Lcom/android/browser/XLargeUi;)V
    .locals 5
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/android/browser/UiController;
    .param p3    # Lcom/android/browser/XLargeUi;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput v3, p0, Lcom/android/browser/TabBar;->mCurrentTextureWidth:I

    iput v3, p0, Lcom/android/browser/TabBar;->mCurrentTextureHeight:I

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/browser/TabBar;->mActiveShaderPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/browser/TabBar;->mInactiveShaderPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/browser/TabBar;->mFocusPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/android/browser/TabBar;->mActiveMatrix:Landroid/graphics/Matrix;

    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/android/browser/TabBar;->mInactiveMatrix:Landroid/graphics/Matrix;

    iput-object p1, p0, Lcom/android/browser/TabBar;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/android/browser/TabBar;->mUiController:Lcom/android/browser/UiController;

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->getTabControl()Lcom/android/browser/TabControl;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/TabBar;->mTabControl:Lcom/android/browser/TabControl;

    iput-object p3, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {p1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/android/browser/TabBar;->mTabWidth:I

    const v2, 0x7f020004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/TabBar;->mActiveDrawable:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f020012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/TabBar;->mInactiveDrawable:Landroid/graphics/drawable/Drawable;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/android/browser/TabBar;->mTabMap:Ljava/util/Map;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040034

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v2, 0x7f0b001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0, v3, v2, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    const v2, 0x7f0d002b

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/browser/TabScrollView;

    iput-object v2, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    const v2, 0x7f0d005e

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/android/browser/TabBar;->mNewTab:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/android/browser/TabBar;->mNewTab:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->getTabs()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/browser/TabBar;->updateTabs(Ljava/util/List;)V

    const/4 v2, -0x1

    iput v2, p0, Lcom/android/browser/TabBar;->mButtonWidth:I

    const v2, 0x7f0b0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/android/browser/TabBar;->mTabOverlap:I

    const v2, 0x7f0b0003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/android/browser/TabBar;->mAddTabOverlap:I

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/android/browser/TabBar;->mTabSliceWidth:I

    iget-object v2, p0, Lcom/android/browser/TabBar;->mActiveShaderPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mActiveShaderPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mInactiveShaderPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mInactiveShaderPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mFocusPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mFocusPaint:Landroid/graphics/Paint;

    const v3, 0x7f0b0005

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mFocusPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mFocusPaint:Landroid/graphics/Paint;

    const v3, 0x7f0a0009

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/TabBar;)I
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget v0, p0, Lcom/android/browser/TabBar;->mTabOverlap:I

    return v0
.end method

.method static synthetic access$100(Lcom/android/browser/TabBar;)I
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget v0, p0, Lcom/android/browser/TabBar;->mTabSliceWidth:I

    return v0
.end method

.method static synthetic access$1000(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Landroid/graphics/drawable/Drawable;
    .param p1    # I
    .param p2    # I

    invoke-static {p0, p1, p2}, Lcom/android/browser/TabBar;->getDrawableAsBitmap(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/browser/TabBar;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mInactiveDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/browser/TabBar;)Landroid/graphics/BitmapShader;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mActiveShader:Landroid/graphics/BitmapShader;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/android/browser/TabBar;Landroid/graphics/BitmapShader;)Landroid/graphics/BitmapShader;
    .locals 0
    .param p0    # Lcom/android/browser/TabBar;
    .param p1    # Landroid/graphics/BitmapShader;

    iput-object p1, p0, Lcom/android/browser/TabBar;->mActiveShader:Landroid/graphics/BitmapShader;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/android/browser/TabBar;)Landroid/graphics/Paint;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mActiveShaderPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/browser/TabBar;)Landroid/graphics/BitmapShader;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mInactiveShader:Landroid/graphics/BitmapShader;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/browser/TabBar;Landroid/graphics/BitmapShader;)Landroid/graphics/BitmapShader;
    .locals 0
    .param p0    # Lcom/android/browser/TabBar;
    .param p1    # Landroid/graphics/BitmapShader;

    iput-object p1, p0, Lcom/android/browser/TabBar;->mInactiveShader:Landroid/graphics/BitmapShader;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/android/browser/TabBar;)Landroid/graphics/Paint;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mInactiveShaderPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/browser/TabBar;)Landroid/graphics/Matrix;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mActiveMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/browser/TabBar;)Landroid/graphics/Matrix;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mInactiveMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/browser/TabBar;)Landroid/graphics/Paint;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mFocusPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/browser/TabBar;)Lcom/android/browser/TabScrollView;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/browser/TabBar;)Lcom/android/browser/XLargeUi;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/browser/TabBar;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mTabMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/browser/TabBar;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/browser/TabBar;)I
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget v0, p0, Lcom/android/browser/TabBar;->mTabWidth:I

    return v0
.end method

.method static synthetic access$500(Lcom/android/browser/TabBar;)Lcom/android/browser/TabControl;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mTabControl:Lcom/android/browser/TabControl;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/browser/TabBar;)Lcom/android/browser/UiController;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mUiController:Lcom/android/browser/UiController;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/browser/TabBar;)I
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget v0, p0, Lcom/android/browser/TabBar;->mCurrentTextureWidth:I

    return v0
.end method

.method static synthetic access$702(Lcom/android/browser/TabBar;I)I
    .locals 0
    .param p0    # Lcom/android/browser/TabBar;
    .param p1    # I

    iput p1, p0, Lcom/android/browser/TabBar;->mCurrentTextureWidth:I

    return p1
.end method

.method static synthetic access$800(Lcom/android/browser/TabBar;)I
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget v0, p0, Lcom/android/browser/TabBar;->mCurrentTextureHeight:I

    return v0
.end method

.method static synthetic access$802(Lcom/android/browser/TabBar;I)I
    .locals 0
    .param p0    # Lcom/android/browser/TabBar;
    .param p1    # I

    iput p1, p0, Lcom/android/browser/TabBar;->mCurrentTextureHeight:I

    return p1
.end method

.method static synthetic access$900(Lcom/android/browser/TabBar;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Lcom/android/browser/TabBar;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mActiveDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private animateTabIn(Lcom/android/browser/Tab;Lcom/android/browser/TabBar$TabView;)V
    .locals 3
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Lcom/android/browser/TabBar$TabView;

    const-string v1, "scaleX"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {p2, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/android/browser/TabBar$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/browser/TabBar$2;-><init>(Lcom/android/browser/TabBar;Lcom/android/browser/Tab;Lcom/android/browser/TabBar$TabView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data
.end method

.method private animateTabOut(Lcom/android/browser/Tab;Lcom/android/browser/TabBar$TabView;)V
    .locals 7
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Lcom/android/browser/TabBar$TabView;

    const/4 v6, 0x2

    const-string v4, "scaleX"

    new-array v5, v6, [F

    fill-array-data v5, :array_0

    invoke-static {p2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-string v4, "scaleY"

    new-array v5, v6, [F

    fill-array-data v5, :array_1

    invoke-static {p2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-string v4, "alpha"

    new-array v5, v6, [F

    fill-array-data v5, :array_2

    invoke-static {p2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v5, 0x1

    aput-object v3, v4, v5

    aput-object v0, v4, v6

    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    const-wide/16 v4, 0x96

    invoke-virtual {v1, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v4, Lcom/android/browser/TabBar$1;

    invoke-direct {v4, p0, p2, p1}, Lcom/android/browser/TabBar$1;-><init>(Lcom/android/browser/TabBar;Lcom/android/browser/TabBar$TabView;Lcom/android/browser/Tab;)V

    invoke-virtual {v1, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :array_0
    .array-data 4
        0x3f800000
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method private buildTabView(Lcom/android/browser/Tab;)Lcom/android/browser/TabBar$TabView;
    .locals 2
    .param p1    # Lcom/android/browser/Tab;

    new-instance v0, Lcom/android/browser/TabBar$TabView;

    iget-object v1, p0, Lcom/android/browser/TabBar;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/browser/TabBar$TabView;-><init>(Lcom/android/browser/TabBar;Landroid/content/Context;Lcom/android/browser/Tab;)V

    iget-object v1, p0, Lcom/android/browser/TabBar;->mTabMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private static getDrawableAsBitmap(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/Bitmap;
    .locals 4
    .param p0    # Landroid/graphics/drawable/Drawable;
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x0

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v3, v3, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method private isLoading()Z
    .locals 2

    iget-object v1, p0, Lcom/android/browser/TabBar;->mTabControl:Lcom/android/browser/TabControl;

    invoke-virtual {v1}, Lcom/android/browser/TabControl;->getCurrentTab()Lcom/android/browser/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/browser/Tab;->inPageLoad()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private showUrlBar()V
    .locals 1

    iget-object v0, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v0}, Lcom/android/browser/XLargeUi;->stopWebViewScrolling()V

    iget-object v0, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v0}, Lcom/android/browser/BaseUi;->showTitleBar()V

    return-void
.end method


# virtual methods
.method getTabCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/browser/TabBar;->mTabMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/browser/TabBar;->mNewTab:Landroid/widget/ImageButton;

    if-ne v2, p1, :cond_1

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->openTabToHomePage()Lcom/android/browser/Tab;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    invoke-virtual {v2}, Lcom/android/browser/TabScrollView;->getSelectedTab()Landroid/view/View;

    move-result-object v2

    if-ne v2, p1, :cond_5

    iget-boolean v2, p0, Lcom/android/browser/TabBar;->mUseQuickControls:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->isTitleBarShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/android/browser/TabBar;->isLoading()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->stopEditingUrl()V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->hideTitleBar()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v2}, Lcom/android/browser/XLargeUi;->stopWebViewScrolling()V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v2, v3, v3}, Lcom/android/browser/XLargeUi;->editUrl(ZZ)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->isTitleBarShowing()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/android/browser/TabBar;->isLoading()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->stopEditingUrl()V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->hideTitleBar()V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/android/browser/TabBar;->showUrlBar()V

    goto :goto_0

    :cond_5
    instance-of v2, p1, Lcom/android/browser/TabBar$TabView;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lcom/android/browser/TabBar$TabView;

    iget-object v1, v2, Lcom/android/browser/TabBar$TabView;->mTab:Lcom/android/browser/Tab;

    iget-object v2, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    invoke-virtual {v2, p1}, Lcom/android/browser/TabScrollView;->getChildIndex(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    invoke-virtual {v2, v0}, Lcom/android/browser/TabScrollView;->setSelectedTab(I)V

    iget-object v2, p0, Lcom/android/browser/TabBar;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2, v1}, Lcom/android/browser/UiController;->switchToTab(Lcom/android/browser/Tab;)Z

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v1, p0, Lcom/android/browser/TabBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0b0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/browser/TabBar;->mTabWidth:I

    iget-object v1, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    invoke-virtual {v1}, Lcom/android/browser/TabScrollView;->updateLayout()V

    return-void
.end method

.method public onFavicon(Lcom/android/browser/Tab;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/browser/TabBar;->mTabMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/TabBar$TabView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/browser/TabBar;->mUi:Lcom/android/browser/XLargeUi;

    invoke-virtual {v1, p2}, Lcom/android/browser/XLargeUi;->getFaviconDrawable(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/browser/TabBar$TabView;->setFavicon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    iget-object v4, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v4, p4, p2

    sub-int v3, v4, v0

    iget-boolean v4, p0, Lcom/android/browser/TabBar;->mUseQuickControls:Z

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    iput v4, p0, Lcom/android/browser/TabBar;->mButtonWidth:I

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    add-int v5, v0, v2

    sub-int v6, p5, p3

    invoke-virtual {v4, v0, v1, v5, v6}, Landroid/view/ViewGroup;->layout(IIII)V

    iget-boolean v4, p0, Lcom/android/browser/TabBar;->mUseQuickControls:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/browser/TabBar;->mNewTab:Landroid/widget/ImageButton;

    add-int v5, v0, v2

    iget v6, p0, Lcom/android/browser/TabBar;->mAddTabOverlap:I

    sub-int/2addr v5, v6

    add-int v6, v0, v2

    iget v7, p0, Lcom/android/browser/TabBar;->mButtonWidth:I

    add-int/2addr v6, v7

    iget v7, p0, Lcom/android/browser/TabBar;->mAddTabOverlap:I

    sub-int/2addr v6, v7

    sub-int v7, p5, p3

    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/view/View;->layout(IIII)V

    :cond_1
    return-void

    :cond_2
    iget-object v4, p0, Lcom/android/browser/TabBar;->mNewTab:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget v5, p0, Lcom/android/browser/TabBar;->mAddTabOverlap:I

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/android/browser/TabBar;->mButtonWidth:I

    sub-int v4, v3, v2

    iget v5, p0, Lcom/android/browser/TabBar;->mButtonWidth:I

    if-ge v4, v5, :cond_0

    iget v4, p0, Lcom/android/browser/TabBar;->mButtonWidth:I

    sub-int v2, v3, v4

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget-boolean v1, p0, Lcom/android/browser/TabBar;->mUseQuickControls:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/browser/TabBar;->mAddTabOverlap:I

    sub-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public onNewTab(Lcom/android/browser/Tab;)V
    .locals 1
    .param p1    # Lcom/android/browser/Tab;

    invoke-direct {p0, p1}, Lcom/android/browser/TabBar;->buildTabView(Lcom/android/browser/Tab;)Lcom/android/browser/TabBar$TabView;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/browser/TabBar;->animateTabIn(Lcom/android/browser/Tab;Lcom/android/browser/TabBar$TabView;)V

    return-void
.end method

.method public onRemoveTab(Lcom/android/browser/Tab;)V
    .locals 2
    .param p1    # Lcom/android/browser/Tab;

    iget-object v1, p0, Lcom/android/browser/TabBar;->mTabMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/TabBar$TabView;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/android/browser/TabBar;->animateTabOut(Lcom/android/browser/Tab;Lcom/android/browser/TabBar$TabView;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/browser/TabBar;->mTabMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public onSetActiveTab(Lcom/android/browser/Tab;)V
    .locals 2
    .param p1    # Lcom/android/browser/Tab;

    iget-object v0, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    iget-object v1, p0, Lcom/android/browser/TabBar;->mTabControl:Lcom/android/browser/TabControl;

    invoke-virtual {v1, p1}, Lcom/android/browser/TabControl;->getTabPosition(Lcom/android/browser/Tab;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/browser/TabScrollView;->setSelectedTab(I)V

    return-void
.end method

.method public onUrlAndTitle(Lcom/android/browser/Tab;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v1, p0, Lcom/android/browser/TabBar;->mTabMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/TabBar$TabView;

    if-eqz v0, :cond_1

    if-eqz p3, :cond_2

    invoke-virtual {v0, p3}, Lcom/android/browser/TabBar$TabView;->setDisplayTitle(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/android/browser/TabBar$TabView;->access$2100(Lcom/android/browser/TabBar$TabView;)V

    :cond_1
    return-void

    :cond_2
    if-eqz p2, :cond_0

    invoke-static {p2}, Lcom/android/browser/UrlUtils;->stripUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/browser/TabBar$TabView;->setDisplayTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method setUseQuickControls(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/browser/TabBar;->mUseQuickControls:Z

    iget-object v1, p0, Lcom/android/browser/TabBar;->mNewTab:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/android/browser/TabBar;->mUseQuickControls:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateTabs(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/browser/Tab;",
            ">;)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    invoke-virtual {v3}, Lcom/android/browser/TabScrollView;->clearTabs()V

    iget-object v3, p0, Lcom/android/browser/TabBar;->mTabMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/browser/Tab;

    invoke-direct {p0, v1}, Lcom/android/browser/TabBar;->buildTabView(Lcom/android/browser/Tab;)Lcom/android/browser/TabBar$TabView;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    invoke-virtual {v3, v2}, Lcom/android/browser/TabScrollView;->addTab(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/browser/TabBar;->mTabs:Lcom/android/browser/TabScrollView;

    iget-object v4, p0, Lcom/android/browser/TabBar;->mTabControl:Lcom/android/browser/TabControl;

    invoke-virtual {v4}, Lcom/android/browser/TabControl;->getCurrentPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/browser/TabScrollView;->setSelectedTab(I)V

    return-void
.end method
