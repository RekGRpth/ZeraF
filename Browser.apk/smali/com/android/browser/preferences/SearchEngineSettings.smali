.class public Lcom/android/browser/preferences/SearchEngineSettings;
.super Landroid/preference/PreferenceFragment;
.source "SearchEngineSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final ACTION_QUICKSEARCHBOX_SEARCH_ENGINE_CHANGED:Ljava/lang/String; = "com.android.quicksearchbox.SEARCH_ENGINE_CHANGED"

.field private static final BAIDU:Ljava/lang/String; = "baidu"

.field private static final DBG:Z = true

.field private static final GOOGLE:Ljava/lang/String; = "google"

.field private static final PREF_SYNC_SEARCH_ENGINE:Ljava/lang/String; = "syc_search_engine"

.field private static final XLOG:Ljava/lang/String; = "browser/SearchEngineSettings"


# instance fields
.field private mActivity:Landroid/preference/PreferenceActivity;

.field private mBrowserSmallFeatureEx:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

.field private mCheckBoxPref:Landroid/preference/CheckBoxPreference;

.field private mEntries:[Ljava/lang/String;

.field private mEntryFavicon:[Ljava/lang/String;

.field private mEntryValues:[Ljava/lang/String;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mRadioPrefs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/browser/preferences/RadioPreference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mBrowserSmallFeatureEx:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    return-void
.end method

.method private broadcastSearchEngineChangedExternal(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.quicksearchbox.SEARCH_ENGINE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.quicksearchbox"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mBrowserSmallFeatureEx:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    iget-object v2, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "search_engine"

    const-string v4, "baidu"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;->setIntentSearchEngineExtra(Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "search_engine"

    iget-object v2, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "search_engine"

    const-string v4, "google"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string v1, "browser/SearchEngineSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Broadcasting: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private createPreferenceHierarchy()Landroid/preference/PreferenceScreen;
    .locals 9

    const v8, 0x7f0c0021

    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v6

    iget-object v7, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    new-instance v0, Landroid/preference/PreferenceCategory;

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-direct {v0, v6}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0c001f

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    new-instance v6, Landroid/preference/CheckBoxPreference;

    iget-object v7, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-direct {v6, v7}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    const-string v7, "toggle_consistency"

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    const v7, 0x7f0c0020

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v8}, Landroid/preference/TwoStatePreference;->setSummaryOn(I)V

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v8}, Landroid/preference/TwoStatePreference;->setSummaryOff(I)V

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v7, "syc_search_engine"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    new-instance v4, Landroid/preference/PreferenceCategory;

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-direct {v4, v6}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0c00bd

    invoke-virtual {v4, v6}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    const/4 v1, 0x0

    :goto_0
    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntries:[Ljava/lang/String;

    array-length v6, v6

    if-ge v1, v6, :cond_0

    new-instance v2, Lcom/android/browser/preferences/RadioPreference;

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-direct {v2, v6}, Lcom/android/browser/preferences/RadioPreference;-><init>(Landroid/content/Context;)V

    const v6, 0x7f04002b

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setWidgetLayoutResource(I)V

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntries:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setOrder(I)V

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {v4, v2}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    iget-object v6, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mRadioPrefs:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public static getSearchEngineInfos(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/common/search/SearchEngineInfo;",
            ">;"
        }
    .end annotation

    const-string v2, "search"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchManager;

    const-string v2, "search_engine"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/common/search/ISearchEngineManager;

    invoke-interface {v0}, Lcom/mediatek/common/search/ISearchEngineManager;->getAvailableSearchEngines()Ljava/util/List;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mRadioPrefs:Ljava/util/List;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    check-cast v10, Landroid/preference/PreferenceActivity;

    iput-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v10}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v10

    iput-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    const/4 v9, -0x1

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v10}, Lcom/mediatek/browser/ext/Extensions;->getSmallFeaturePlugin(Landroid/content/Context;)Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    move-result-object v10

    iput-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mBrowserSmallFeatureEx:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mBrowserSmallFeatureEx:Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;

    iget-object v11, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v10, v11}, Lcom/mediatek/browser/ext/IBrowserSmallFeatureEx;->getSearchEngine(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_0

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v11, "search_engine"

    const-string v12, "google"

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :cond_0
    const/4 v3, 0x0

    const-string v4, ""

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    const-string v11, "search_engine"

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/common/search/ISearchEngineManager;

    invoke-interface {v6, v7}, Lcom/mediatek/common/search/ISearchEngineManager;->getSearchEngineByName(Ljava/lang/String;)Lcom/mediatek/common/search/SearchEngineInfo;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/mediatek/common/search/SearchEngineInfo;->getFaviconUri()Ljava/lang/String;

    move-result-object v4

    :goto_0
    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v10}, Lcom/android/browser/preferences/SearchEngineSettings;->getSearchEngineInfos(Landroid/content/Context;)Ljava/util/List;

    move-result-object v8

    const/4 v2, 0x0

    if-eqz v8, :cond_1

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    :cond_1
    new-array v10, v2, [Ljava/lang/String;

    iput-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryValues:[Ljava/lang/String;

    new-array v10, v2, [Ljava/lang/String;

    iput-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntries:[Ljava/lang/String;

    new-array v10, v2, [Ljava/lang/String;

    iput-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryFavicon:[Ljava/lang/String;

    const-string v10, ""

    invoke-interface {v6, v10, v4}, Lcom/mediatek/common/search/ISearchEngineManager;->getBestMatchSearchEngine(Ljava/lang/String;Ljava/lang/String;)Lcom/mediatek/common/search/SearchEngineInfo;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/mediatek/common/search/SearchEngineInfo;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {v5}, Lcom/mediatek/common/search/SearchEngineInfo;->getName()Ljava/lang/String;

    move-result-object v7

    const/4 v3, 0x1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_5

    iget-object v11, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryValues:[Ljava/lang/String;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mediatek/common/search/SearchEngineInfo;

    invoke-virtual {v10}, Lcom/mediatek/common/search/SearchEngineInfo;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v11, v1

    iget-object v11, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntries:[Ljava/lang/String;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mediatek/common/search/SearchEngineInfo;

    invoke-virtual {v10}, Lcom/mediatek/common/search/SearchEngineInfo;->getLabel()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v11, v1

    iget-object v11, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryFavicon:[Ljava/lang/String;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mediatek/common/search/SearchEngineInfo;

    invoke-virtual {v10}, Lcom/mediatek/common/search/SearchEngineInfo;->getFaviconUri()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v11, v1

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryValues:[Ljava/lang/String;

    aget-object v10, v10, v1

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    move v9, v1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v11, "search_engine_favicon"

    const-string v12, ""

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/android/browser/preferences/SearchEngineSettings;->createPreferenceHierarchy()Landroid/preference/PreferenceScreen;

    move-result-object v10

    invoke-virtual {p0, v10}, Landroid/preference/PreferenceFragment;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    const/4 v10, -0x1

    if-eq v9, v10, :cond_9

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mRadioPrefs:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/browser/preferences/RadioPreference;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/android/browser/preferences/RadioPreference;->setChecked(Z)V

    :cond_6
    :goto_2
    if-eqz v3, :cond_7

    const/4 v10, -0x1

    if-eq v9, v10, :cond_7

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v10, "search_engine"

    iget-object v11, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryValues:[Ljava/lang/String;

    aget-object v11, v11, v9

    invoke-interface {v0, v10, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v10, "search_engine_favicon"

    iget-object v11, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryFavicon:[Ljava/lang/String;

    aget-object v11, v11, v9

    invoke-interface {v0, v10, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_7
    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v10}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v10

    if-eqz v10, :cond_8

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-direct {p0, v10}, Lcom/android/browser/preferences/SearchEngineSettings;->broadcastSearchEngineChangedExternal(Landroid/content/Context;)V

    :cond_8
    return-void

    :cond_9
    if-lez v2, :cond_6

    const/4 v9, 0x0

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v2, :cond_b

    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryValues:[Ljava/lang/String;

    aget-object v10, v10, v1

    const-string v11, "google"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    move v9, v1

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_b
    iget-object v10, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mRadioPrefs:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/browser/preferences/RadioPreference;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/android/browser/preferences/RadioPreference;->setChecked(Z)V

    const/4 v3, 0x1

    goto :goto_2
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    iget-object v1, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "syc_search_engine"

    iget-object v2, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v1, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-direct {p0, v1}, Lcom/android/browser/preferences/SearchEngineSettings;->broadcastSearchEngineChangedExternal(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7
    .param p1    # Landroid/preference/Preference;

    const/4 v6, 0x1

    iget-object v3, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mRadioPrefs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/browser/preferences/RadioPreference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/browser/preferences/RadioPreference;->setChecked(Z)V

    goto :goto_0

    :cond_0
    move-object v3, p1

    check-cast v3, Lcom/android/browser/preferences/RadioPreference;

    invoke-virtual {v3, v6}, Lcom/android/browser/preferences/RadioPreference;->setChecked(Z)V

    iget-object v3, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "search_engine"

    iget-object v4, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryValues:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getOrder()I

    move-result v5

    aget-object v4, v4, v5

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v3, "search_engine_favicon"

    iget-object v4, p0, Lcom/android/browser/preferences/SearchEngineSettings;->mEntryFavicon:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getOrder()I

    move-result v5

    aget-object v4, v4, v5

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return v6
.end method
