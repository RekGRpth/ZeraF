.class final Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PartnerBookmarksProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DatabaseHelper"
.end annotation


# static fields
.field private static final ACTIVE_CONFIGURATION_PREFNAME:Ljava/lang/String; = "config"

.field private static final DATABASE_FILENAME:Ljava/lang/String; = "partnerBookmarks.db"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final PREFERENCES_FILENAME:Ljava/lang/String; = "pbppref"


# instance fields
.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field final synthetic this$0:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;


# direct methods
.method public constructor <init>(Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;Landroid/content/Context;)V
    .locals 3
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->this$0:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;

    const-string v0, "partnerBookmarks.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    const-string v0, "pbppref"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->sharedPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method private addDefaultBookmarks(Landroid/database/sqlite/SQLiteDatabase;JJ)Z
    .locals 32
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # J
    .param p4    # J

    move-wide/from16 v5, p4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->this$0:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v28, 0x7f020001

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v9

    array-length v0, v9

    move/from16 v21, v0

    const/high16 v28, 0x7f020000

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v19

    const/16 v16, 0x0

    :try_start_0
    new-instance v17, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v28, "bookmarks"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v28, "_id"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    const-string v28, "title"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const-string v28, "url"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    const-string v28, "type"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const-string v28, "parent"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    const-string v28, "favicon"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    const-string v28, "touchicon"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    const/4 v14, 0x0

    :goto_0
    add-int/lit8 v28, v14, 0x1

    move/from16 v0, v28

    move/from16 v1, v21

    if-ge v0, v1, :cond_5

    add-int/lit8 v28, v14, 0x1

    aget-object v4, v9, v28

    aget-object v28, v9, v14

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x0

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/TypedArray;->length()I

    move-result v28

    move/from16 v0, v28

    if-ge v14, v0, :cond_0

    const/16 v28, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v28

    invoke-virtual {v0, v14, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v13

    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v13}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->readRaw(Landroid/content/res/Resources;I)[B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v11

    :cond_0
    :goto_1
    const/16 v23, 0x0

    add-int/lit8 v28, v14, 0x1

    :try_start_3
    invoke-virtual/range {v19 .. v19}, Landroid/content/res/TypedArray;->length()I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_1

    add-int/lit8 v28, v14, 0x1

    const/16 v29, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v25

    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->readRaw(Landroid/content/res/Resources;I)[B
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v23

    :cond_1
    :goto_2
    :try_start_5
    invoke-virtual/range {v17 .. v17}, Landroid/database/DatabaseUtils$InsertHelper;->prepareForInsert()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v15, v5, v6}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1, v7}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-virtual {v0, v1, v8}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    const/16 v28, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v26

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/database/DatabaseUtils$InsertHelper;->bind(II)V

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-wide/from16 v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    if-eqz v11, :cond_2

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v11}, Landroid/database/DatabaseUtils$InsertHelper;->bind(I[B)V

    :cond_2
    if-eqz v23, :cond_3

    move-object/from16 v0, v17

    move/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/database/DatabaseUtils$InsertHelper;->bind(I[B)V

    :cond_3
    const-wide/16 v28, 0x1

    add-long v5, v5, v28

    invoke-virtual/range {v17 .. v17}, Landroid/database/DatabaseUtils$InsertHelper;->execute()J

    move-result-wide v28

    const-wide/16 v30, -0x1

    cmp-long v28, v28, v30

    if-nez v28, :cond_4

    const-string v28, "PartnerBookmarksProvider"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Failed to insert bookmark "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/16 v28, 0x0

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual/range {v17 .. v17}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    :goto_3
    return v28

    :catch_0
    move-exception v10

    :try_start_6
    const-string v28, "PartnerBookmarksProvider"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Failed to read favicon for "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-static {v0, v1, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v28

    move-object/from16 v16, v17

    :goto_4
    invoke-virtual/range {v19 .. v19}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual/range {v16 .. v16}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    throw v28

    :catch_1
    move-exception v10

    :try_start_7
    const-string v28, "PartnerBookmarksProvider"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Failed to read touchicon for "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-static {v0, v1, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    :cond_4
    add-int/lit8 v14, v14, 0x2

    goto/16 :goto_0

    :cond_5
    invoke-virtual/range {v19 .. v19}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual/range {v17 .. v17}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    const/16 v28, 0x1

    goto :goto_3

    :catchall_1
    move-exception v28

    goto :goto_4
.end method

.method private addRootFolder(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Z
    .locals 6
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # J
    .param p4    # Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "title"

    invoke-virtual {v0, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "parent"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "type"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "bookmarks"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private createDefaultBookmarks(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 12
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->this$0:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;

    invoke-virtual {v0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v0, 0x7f030000

    :try_start_0
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    const v0, 0x7f020001

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v6

    array-length v0, v6

    if-lt v0, v11, :cond_2

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge v0, v11, :cond_0

    const-string v0, "PartnerBookmarksProvider"

    const-string v1, "bookmarks_folder_name was not specified; bailing out"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v10

    :goto_0
    return v0

    :cond_0
    const-wide/16 v0, 0x1

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->addRootFolder(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "PartnerBookmarksProvider"

    const-string v1, "failed to insert root folder; bailing out"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v10

    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x2

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->addDefaultBookmarks(Landroid/database/sqlite/SQLiteDatabase;JJ)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "PartnerBookmarksProvider"

    const-string v1, "failed to insert bookmarks; bailing out"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v10

    goto :goto_0

    :cond_2
    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->setActiveConfiguration(Landroid/content/res/Configuration;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v11

    goto :goto_0

    :catch_0
    move-exception v8

    const-string v0, "PartnerBookmarksProvider"

    const-string v1, "failed to fetch resources; bailing out"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v10

    goto :goto_0
.end method

.method private createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "CREATE TABLE bookmarks(_id INTEGER NOT NULL DEFAULT 0,title TEXT,url TEXT,type INTEGER NOT NULL DEFAULT 0,parent INTEGER,favicon BLOB,touchicon BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private dropTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "DROP TABLE IF EXISTS bookmarks"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private getConfigSignature(Landroid/content/res/Configuration;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mmc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/content/res/Configuration;->mcc:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-mnc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/content/res/Configuration;->mnc:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-loc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private readRaw(Landroid/content/res/Resources;I)[B
    .locals 5
    .param p1    # Landroid/content/res/Resources;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p2, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v4, 0x1000

    :try_start_0
    new-array v1, v4, [B

    :goto_1
    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    throw v4

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    goto :goto_0
.end method

.method private setActiveConfiguration(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    iget-object v1, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "config"

    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->getConfigSignature(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->createDefaultBookmarks(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->dropTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->dropTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->dropTable(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public declared-synchronized prepareForConfiguration(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1    # Landroid/content/res/Configuration;

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->this$0:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;

    invoke-static {v3}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->access$000(Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;)Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->getConfigSignature(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "config"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "bookmarks"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-direct {p0, v1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->createDefaultBookmarks(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "bookmarks"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
