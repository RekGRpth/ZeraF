.class public final Lcom/android/deskclock/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AlarmAlertFullScreenTheme:I = 0x7f10000c

.field public static final AlarmAlertFullScreenWindowTitle:I = 0x7f10000b

.field public static final AlarmClockTheme:I = 0x7f100006

.field public static final ButtonStripLeft:I = 0x7f10000f

.field public static final ButtonStripMiddle:I = 0x7f100010

.field public static final ButtonStripRight:I = 0x7f100011

.field public static final DeskClock:I = 0x7f100001

.field public static final DeskClockBarStyle:I = 0x7f100004

.field public static final DeskClockDropDownListView:I = 0x7f100005

.field public static final DeskClockTabBarStyle:I = 0x7f100003

.field public static final DeskClockTabStyle:I = 0x7f100002

.field public static final RoundTouchButton:I = 0x7f10000e

.field public static final ScreensaverActivityTheme:I = 0x7f10000d

.field public static final SetAlarmTheme:I = 0x7f10000a

.field public static final SettingsTheme:I = 0x7f100007

.field public static final SettingsTheme_ActionBar:I = 0x7f100008

.field public static final SettingsTheme_TextAppearance_ActionBar_Title:I = 0x7f100009

.field public static final TextAppearance:I = 0x7f100032

.field public static final TextAppearance_StatusBar:I = 0x7f100033

.field public static final TextAppearance_StatusBar_EventContent:I = 0x7f100034

.field public static final TextAppearance_StatusBar_EventContent_Line2:I = 0x7f100036

.field public static final TextAppearance_StatusBar_EventContent_Time:I = 0x7f100037

.field public static final TextAppearance_StatusBar_EventContent_Title:I = 0x7f100035

.field public static final TimePickerDialog:I = 0x7f100031

.field public static final ToastBarStyle:I = 0x7f100038

.field public static final alarm_label:I = 0x7f100021

.field public static final alarm_label_bold:I = 0x7f100023

.field public static final alarm_label_not_caps:I = 0x7f100022

.field public static final alarm_list_left_column:I = 0x7f100012

.field public static final big_bold:I = 0x7f10001d

.field public static final big_thin:I = 0x7f10001f

.field public static final body:I = 0x7f100024

.field public static final body_bold:I = 0x7f100026

.field public static final body_not_caps:I = 0x7f100025

.field public static final bold_button:I = 0x7f10002a

.field public static final button:I = 0x7f100027

.field public static final city_name:I = 0x7f10002d

.field public static final city_time:I = 0x7f10002e

.field public static final clock:I = 0x7f100000

.field public static final dialog_button:I = 0x7f100029

.field public static final dialpad:I = 0x7f10002b

.field public static final dialpad_ampm:I = 0x7f10002c

.field public static final header:I = 0x7f10001b

.field public static final header_not_caps:I = 0x7f10001c

.field public static final label:I = 0x7f100019

.field public static final label_not_caps:I = 0x7f10001a

.field public static final medium_bold:I = 0x7f100014

.field public static final medium_light:I = 0x7f100017

.field public static final small_bold:I = 0x7f100013

.field public static final small_light:I = 0x7f100016

.field public static final tablet_dialpad:I = 0x7f10002f

.field public static final tablet_dialpad_ampm:I = 0x7f100030

.field public static final timer_label:I = 0x7f100028

.field public static final widget_big_bold:I = 0x7f10001e

.field public static final widget_big_thin:I = 0x7f100020

.field public static final widget_medium_bold:I = 0x7f100015

.field public static final widget_medium_light:I = 0x7f100018


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
