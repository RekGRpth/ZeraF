.class public Lcom/android/deskclock/ScreensaverActivity;
.super Landroid/app/Activity;
.source "ScreensaverActivity.java"


# static fields
.field static final DEBUG:Z = false

.field static final DEFAULT_CLOCK_STYLE:Ljava/lang/String; = "digital"

.field static final TAG:Ljava/lang/String; = "DeskClock/ScreensaverActivity"


# instance fields
.field private mAnalogClock:Landroid/view/View;

.field private mClockStyle:Ljava/lang/String;

.field private mContentView:Landroid/view/View;

.field private mDateFormat:Ljava/lang/String;

.field private mDateFormatForAccessibility:Ljava/lang/String;

.field private mDigitalClock:Landroid/view/View;

.field private final mFlags:I

.field private final mHandler:Landroid/os/Handler;

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private final mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

.field private mPluggedIn:Z

.field private mQuarterlyIntent:Landroid/app/PendingIntent;

.field private mSaverView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mPluggedIn:Z

    const v0, 0x480081

    iput v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mFlags:I

    new-instance v0, Lcom/android/deskclock/ScreensaverActivity$1;

    invoke-direct {v0, p0}, Lcom/android/deskclock/ScreensaverActivity$1;-><init>(Lcom/android/deskclock/ScreensaverActivity;)V

    iput-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    return-void
.end method

.method static synthetic access$002(Lcom/android/deskclock/ScreensaverActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/deskclock/ScreensaverActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/deskclock/ScreensaverActivity;->mPluggedIn:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/deskclock/ScreensaverActivity;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/ScreensaverActivity;

    invoke-direct {p0}, Lcom/android/deskclock/ScreensaverActivity;->setWakeLock()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/deskclock/ScreensaverActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/deskclock/ScreensaverActivity;

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mDateFormat:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/deskclock/ScreensaverActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/deskclock/ScreensaverActivity;

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mDateFormatForAccessibility:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/deskclock/ScreensaverActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/deskclock/ScreensaverActivity;

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mContentView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/deskclock/ScreensaverActivity;)Landroid/app/PendingIntent;
    .locals 1
    .param p0    # Lcom/android/deskclock/ScreensaverActivity;

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mQuarterlyIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/deskclock/ScreensaverActivity;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .locals 0
    .param p0    # Lcom/android/deskclock/ScreensaverActivity;
    .param p1    # Landroid/app/PendingIntent;

    iput-object p1, p0, Lcom/android/deskclock/ScreensaverActivity;->mQuarterlyIntent:Landroid/app/PendingIntent;

    return-object p1
.end method

.method private layoutClockSaver()V
    .locals 3

    const v0, 0x7f040012

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0e001a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mDigitalClock:Landroid/view/View;

    const v0, 0x7f0e003a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mAnalogClock:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/deskclock/ScreensaverActivity;->setClockStyle()V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mSaverView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mContentView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mContentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mSaverView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mSaverView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mContentView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/deskclock/ScreensaverActivity;->mSaverView:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;->registerViews(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mContentView:Landroid/view/View;

    const/16 v1, 0x405

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mDateFormat:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mDateFormatForAccessibility:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/deskclock/ScreensaverActivity;->mContentView:Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/android/deskclock/Utils;->updateDate(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mContentView:Landroid/view/View;

    invoke-static {p0, v0}, Lcom/android/deskclock/Utils;->refreshAlarm(Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method private setClockStyle()V
    .locals 3

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mDigitalClock:Landroid/view/View;

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mAnalogClock:Landroid/view/View;

    const-string v2, "clock_style"

    invoke-static {p0, v0, v1, v2}, Lcom/android/deskclock/Utils;->setClockStyle(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    const v0, 0x7f0e0039

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mSaverView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mSaverView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mDigitalClock:Landroid/view/View;

    if-ne v0, v1, :cond_0

    const-string v0, "digital"

    :goto_0
    iput-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mClockStyle:Ljava/lang/String;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mSaverView:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/android/deskclock/Utils;->dimClockView(ZLandroid/view/View;)V

    return-void

    :cond_0
    const-string v0, "analog"

    goto :goto_0
.end method

.method private setWakeLock()V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v2, v2, 0x400

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-boolean v2, p0, Lcom/android/deskclock/ScreensaverActivity;->mPluggedIn:Z

    if-eqz v2, :cond_0

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, 0x480081

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void

    :cond_0
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, -0x480082

    and-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/android/deskclock/ScreensaverActivity;->layoutClockSaver()V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mQuarterlyIntent:Landroid/app/PendingIntent;

    invoke-static {p0, v0}, Lcom/android/deskclock/Utils;->cancelAlarmOnQuarterHour(Landroid/content/Context;Landroid/app/PendingIntent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 6

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v3, 0x0

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3, v4}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "plugged"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v2, :cond_0

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    :cond_0
    :goto_0
    iput-boolean v2, p0, Lcom/android/deskclock/ScreensaverActivity;->mPluggedIn:Z

    const/high16 v2, 0x7f0d0000

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/deskclock/ScreensaverActivity;->mDateFormat:Ljava/lang/String;

    const v2, 0x7f0d0001

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/deskclock/ScreensaverActivity;->mDateFormatForAccessibility:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/deskclock/ScreensaverActivity;->setWakeLock()V

    invoke-direct {p0}, Lcom/android/deskclock/ScreensaverActivity;->layoutClockSaver()V

    iget-object v2, p0, Lcom/android/deskclock/ScreensaverActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/deskclock/ScreensaverActivity;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-static {p0}, Lcom/android/deskclock/Utils;->startAlarmOnQuarterHour(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/android/deskclock/ScreensaverActivity;->mQuarterlyIntent:Landroid/app/PendingIntent;

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.deskclock.ON_QUARTER_HOUR"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/deskclock/ScreensaverActivity;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/deskclock/ScreensaverActivity;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method
