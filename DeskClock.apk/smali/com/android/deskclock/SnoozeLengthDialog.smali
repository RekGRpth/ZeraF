.class public Lcom/android/deskclock/SnoozeLengthDialog;
.super Landroid/preference/DialogPreference;
.source "SnoozeLengthDialog.java"


# static fields
.field private static final DEFAULT_SNOOZE_TIME:Ljava/lang/String; = "10"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mNumberPickerMinutesView:Landroid/widget/TextView;

.field private mNumberPickerView:Landroid/widget/NumberPicker;

.field private mSnoozeMinutes:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mContext:Landroid/content/Context;

    const v0, 0x7f040020

    invoke-virtual {p0, v0}, Landroid/preference/DialogPreference;->setDialogLayoutResource(I)V

    const v0, 0x7f0d0032

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setTitle(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/SnoozeLengthDialog;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/SnoozeLengthDialog;

    invoke-direct {p0}, Lcom/android/deskclock/SnoozeLengthDialog;->updateDays()V

    return-void
.end method

.method private updateDays()V
    .locals 4

    iget-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerMinutesView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0001

    iget-object v3, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerView:Landroid/widget/NumberPicker;

    invoke-virtual {v3}, Landroid/widget/NumberPicker;->getValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public getCurrentValue()I
    .locals 1

    iget v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mSnoozeMinutes:I

    return v0
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    const v0, 0x7f0e0058

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerMinutesView:Landroid/widget/TextView;

    const v0, 0x7f0e005d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerView:Landroid/widget/NumberPicker;

    iget-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerView:Landroid/widget/NumberPicker;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMinValue(I)V

    iget-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerView:Landroid/widget/NumberPicker;

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    iget-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerView:Landroid/widget/NumberPicker;

    iget v1, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mSnoozeMinutes:I

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    invoke-direct {p0}, Lcom/android/deskclock/SnoozeLengthDialog;->updateDays()V

    iget-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerView:Landroid/widget/NumberPicker;

    new-instance v1, Lcom/android/deskclock/SnoozeLengthDialog$1;

    invoke-direct {v1, p0}, Lcom/android/deskclock/SnoozeLengthDialog$1;-><init>(Lcom/android/deskclock/SnoozeLengthDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerView:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->clearFocus()V

    iget-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mNumberPickerView:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getValue()I

    move-result v0

    iput v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mSnoozeMinutes:I

    iget v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mSnoozeMinutes:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->persistString(Ljava/lang/String;)Z

    invoke-virtual {p0}, Lcom/android/deskclock/SnoozeLengthDialog;->setSummary()V

    :cond_0
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 2
    .param p1    # Landroid/app/AlertDialog$Builder;

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0032

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 2
    .param p1    # Z
    .param p2    # Ljava/lang/Object;

    if-eqz p1, :cond_1

    const-string v1, "10"

    invoke-virtual {p0, v1}, Landroid/preference/Preference;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mSnoozeMinutes:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mSnoozeMinutes:I

    :cond_2
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->persistString(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public setSummary()V
    .locals 4

    iget-object v0, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0f0000

    iget v2, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mSnoozeMinutes:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/deskclock/SnoozeLengthDialog;->mSnoozeMinutes:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
