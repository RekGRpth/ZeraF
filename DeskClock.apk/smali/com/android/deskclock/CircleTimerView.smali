.class public Lcom/android/deskclock/CircleTimerView;
.super Landroid/view/View;
.source "CircleTimerView.java"


# static fields
.field public static final PREF_CTV_ACCUM_TIME:Ljava/lang/String; = "_ctv_accum_time"

.field public static final PREF_CTV_CURRENT_INTERVAL:Ljava/lang/String; = "_ctv_current_interval"

.field public static final PREF_CTV_INTERVAL:Ljava/lang/String; = "_ctv_interval"

.field public static final PREF_CTV_INTERVAL_START:Ljava/lang/String; = "_ctv_interval_start"

.field public static final PREF_CTV_MARKER_TIME:Ljava/lang/String; = "_ctv_marker_time"

.field public static final PREF_CTV_PAUSED:Ljava/lang/String; = "_ctv_paused"

.field public static final PREF_CTV_TIMER_MODE:Ljava/lang/String; = "_ctv_timer_mode"

.field private static mCircleXCenterLeftPadding:F

.field private static mDiamondStrokeSize:F

.field private static mMarkerStrokeSize:F

.field private static mStrokeSize:F


# instance fields
.field private mAccumulatedTime:J

.field private mAnimate:Z

.field private final mArcRect:Landroid/graphics/RectF;

.field private mCurrentIntervalTime:J

.field private final mFill:Landroid/graphics/Paint;

.field private mIntervalStartTime:J

.field private mIntervalTime:J

.field private mMarkerTime:J

.field private final mPaint:Landroid/graphics/Paint;

.field private mPaused:Z

.field private mRadiusOffset:F

.field private mRectHalfWidth:F

.field private mRedColor:I

.field private mResources:Landroid/content/res/Resources;

.field private mScreenDensity:F

.field private mTimerMode:Z

.field private mWhiteColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/deskclock/CircleTimerView;->mCircleXCenterLeftPadding:F

    const/high16 v0, 0x40800000

    sput v0, Lcom/android/deskclock/CircleTimerView;->mStrokeSize:F

    const/high16 v0, 0x41400000

    sput v0, Lcom/android/deskclock/CircleTimerView;->mDiamondStrokeSize:F

    const/high16 v0, 0x40000000

    sput v0, Lcom/android/deskclock/CircleTimerView;->mMarkerStrokeSize:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/CircleTimerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const-wide/16 v4, -0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-wide v2, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalTime:J

    iput-wide v4, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    iput-wide v4, p0, Lcom/android/deskclock/CircleTimerView;->mMarkerTime:J

    iput-wide v2, p0, Lcom/android/deskclock/CircleTimerView;->mCurrentIntervalTime:J

    iput-wide v2, p0, Lcom/android/deskclock/CircleTimerView;->mAccumulatedTime:J

    iput-boolean v1, p0, Lcom/android/deskclock/CircleTimerView;->mPaused:Z

    iput-boolean v1, p0, Lcom/android/deskclock/CircleTimerView;->mAnimate:Z

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mFill:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    const/high16 v0, 0x40c00000

    iput v0, p0, Lcom/android/deskclock/CircleTimerView;->mRectHalfWidth:F

    iput-boolean v1, p0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    invoke-direct {p0, p1}, Lcom/android/deskclock/CircleTimerView;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x1

    const/high16 v3, 0x40000000

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mResources:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f090016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-object v1, p0, Lcom/android/deskclock/CircleTimerView;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f090015

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sub-float/2addr v0, v1

    div-float/2addr v0, v3

    sput v0, Lcom/android/deskclock/CircleTimerView;->mCircleXCenterLeftPadding:F

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f09001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/android/deskclock/CircleTimerView;->mStrokeSize:F

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f090019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/android/deskclock/CircleTimerView;->mDiamondStrokeSize:F

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f09001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/android/deskclock/CircleTimerView;->mMarkerStrokeSize:F

    sget v0, Lcom/android/deskclock/CircleTimerView;->mStrokeSize:F

    sget v1, Lcom/android/deskclock/CircleTimerView;->mDiamondStrokeSize:F

    sget v2, Lcom/android/deskclock/CircleTimerView;->mMarkerStrokeSize:F

    invoke-static {v0, v1, v2}, Lcom/android/deskclock/Utils;->calculateRadiusOffset(FFF)F

    move-result v0

    iput v0, p0, Lcom/android/deskclock/CircleTimerView;->mRadiusOffset:F

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0b0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/deskclock/CircleTimerView;->mWhiteColor:I

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0b0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/deskclock/CircleTimerView;->mRedColor:I

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/android/deskclock/CircleTimerView;->mScreenDensity:F

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mFill:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mFill:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mFill:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/deskclock/CircleTimerView;->mRedColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget v0, Lcom/android/deskclock/CircleTimerView;->mDiamondStrokeSize:F

    div-float/2addr v0, v3

    iput v0, p0, Lcom/android/deskclock/CircleTimerView;->mRectHalfWidth:F

    return-void
.end method


# virtual methods
.method public abortIntervalAnimation()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/deskclock/CircleTimerView;->mAnimate:Z

    return-void
.end method

.method public clearSharedPref(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sw_start_time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "sw_accum_time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "sw_state"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_paused"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_interval"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_interval_start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_current_interval"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_accum_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_marker_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_timer_mode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method protected drawRedDiamond(Landroid/graphics/Canvas;FIIF)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # F
    .param p3    # I
    .param p4    # I
    .param p5    # F

    const/high16 v3, 0x43b40000

    const/high16 v2, 0x43870000

    iget-object v0, p0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/deskclock/CircleTimerView;->mRedColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-boolean v0, p0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    if-eqz v0, :cond_0

    mul-float v0, p2, v3

    sub-float v6, v2, v0

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    float-to-double v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v7

    int-to-float v0, p3

    float-to-double v1, p5

    invoke-static {v7, v8}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v1, v3

    double-to-float v1, v1

    add-float/2addr v0, v1

    int-to-float v1, p4

    float-to-double v2, p5

    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, 0x42340000

    add-float/2addr v0, v6

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    iget v0, p0, Lcom/android/deskclock/CircleTimerView;->mRectHalfWidth:F

    neg-float v1, v0

    iget v0, p0, Lcom/android/deskclock/CircleTimerView;->mRectHalfWidth:F

    neg-float v2, v0

    iget v3, p0, Lcom/android/deskclock/CircleTimerView;->mRectHalfWidth:F

    iget v4, p0, Lcom/android/deskclock/CircleTimerView;->mRectHalfWidth:F

    iget-object v5, p0, Lcom/android/deskclock/CircleTimerView;->mFill:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_0
    mul-float v0, p2, v3

    add-float v6, v2, v0

    goto :goto_0
.end method

.method public isAnimating()Z
    .locals 4

    iget-wide v0, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 19
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v4, v1, 0x1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v5, v1, 0x2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    sget v2, Lcom/android/deskclock/CircleTimerView;->mStrokeSize:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/deskclock/CircleTimerView;->mRadiusOffset:F

    sub-float v6, v1, v2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/deskclock/CircleTimerView;->mRadiusOffset:F

    add-float/2addr v1, v6

    float-to-int v4, v1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    if-eqz v1, :cond_0

    int-to-float v1, v4

    sget v2, Lcom/android/deskclock/CircleTimerView;->mCircleXCenterLeftPadding:F

    add-float/2addr v1, v2

    float-to-int v4, v1

    :cond_0
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    const-wide/16 v7, -0x1

    cmp-long v1, v1, v7

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/deskclock/CircleTimerView;->mWhiteColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v1, v4

    int-to-float v2, v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    if-eqz v1, :cond_1

    const/4 v3, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/android/deskclock/CircleTimerView;->drawRedDiamond(Landroid/graphics/Canvas;FIIF)V

    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/deskclock/CircleTimerView;->mAnimate:Z

    if-eqz v1, :cond_2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->invalidate()V

    :cond_2
    return-void

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/deskclock/CircleTimerView;->mAnimate:Z

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    sub-long/2addr v1, v7

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/deskclock/CircleTimerView;->mAccumulatedTime:J

    add-long/2addr v1, v7

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/android/deskclock/CircleTimerView;->mCurrentIntervalTime:J

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    int-to-float v2, v5

    sub-float/2addr v2, v6

    iput v2, v1, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    int-to-float v2, v5

    add-float/2addr v2, v6

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    int-to-float v2, v4

    sub-float/2addr v2, v6

    iput v2, v1, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    int-to-float v2, v4

    add-float/2addr v2, v6

    iput v2, v1, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/deskclock/CircleTimerView;->mCurrentIntervalTime:J

    long-to-float v1, v1

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/deskclock/CircleTimerView;->mIntervalTime:J

    long-to-float v2, v7

    div-float v3, v1, v2

    const/high16 v1, 0x3f800000

    cmpl-float v1, v3, v1

    if-lez v1, :cond_5

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    if-eqz v1, :cond_5

    const/high16 v3, 0x3f800000

    :cond_5
    const/high16 v2, 0x3f800000

    const/high16 v1, 0x3f800000

    cmpl-float v1, v3, v1

    if-lez v1, :cond_7

    const/high16 v1, 0x3f800000

    :goto_1
    sub-float v14, v2, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/deskclock/CircleTimerView;->mRedColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    const/high16 v9, 0x43870000

    neg-float v1, v3

    const/high16 v2, 0x43b40000

    mul-float v10, v1, v2

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    sget v2, Lcom/android/deskclock/CircleTimerView;->mStrokeSize:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/deskclock/CircleTimerView;->mWhiteColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    const/high16 v9, 0x43870000

    const/high16 v1, 0x43b40000

    mul-float v10, v14, v1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    :goto_3
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/deskclock/CircleTimerView;->mMarkerTime:J

    const-wide/16 v7, -0x1

    cmp-long v1, v1, v7

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    cmpl-float v1, v6, v1

    if-lez v1, :cond_6

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/deskclock/CircleTimerView;->mIntervalTime:J

    const-wide/16 v7, 0x0

    cmp-long v1, v1, v7

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    sget v2, Lcom/android/deskclock/CircleTimerView;->mMarkerStrokeSize:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/deskclock/CircleTimerView;->mMarkerTime:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/deskclock/CircleTimerView;->mIntervalTime:J

    rem-long/2addr v1, v7

    long-to-float v1, v1

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/deskclock/CircleTimerView;->mIntervalTime:J

    long-to-float v2, v7

    div-float/2addr v1, v2

    const/high16 v2, 0x43b40000

    mul-float v13, v1, v2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    const/high16 v1, 0x43870000

    add-float v9, v1, v13

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/deskclock/CircleTimerView;->mScreenDensity:F

    const-wide v10, 0x4076800000000000L

    float-to-double v15, v6

    const-wide v17, 0x400921fb54442d18L

    mul-double v15, v15, v17

    div-double/2addr v10, v15

    double-to-float v2, v10

    mul-float v10, v1, v2

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    :cond_6
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/android/deskclock/CircleTimerView;->drawRedDiamond(Landroid/graphics/Canvas;FIIF)V

    goto/16 :goto_0

    :cond_7
    move v1, v3

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    const/high16 v9, 0x43870000

    const/high16 v1, 0x43b40000

    mul-float v10, v3, v1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/deskclock/CircleTimerView;->mArcRect:Landroid/graphics/RectF;

    const/high16 v1, 0x43870000

    const/high16 v2, 0x3f800000

    sub-float/2addr v2, v14

    const/high16 v7, 0x43b40000

    mul-float/2addr v2, v7

    add-float v9, v1, v2

    const/high16 v1, 0x43b40000

    mul-float v10, v14, v1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/deskclock/CircleTimerView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto/16 :goto_3
.end method

.method public pauseIntervalAnimation()V
    .locals 6

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/deskclock/CircleTimerView;->mAnimate:Z

    iget-wide v0, p0, Lcom/android/deskclock/CircleTimerView;->mAccumulatedTime:J

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/deskclock/CircleTimerView;->mAccumulatedTime:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/deskclock/CircleTimerView;->mPaused:Z

    return-void
.end method

.method public readFromSharedPref(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 7
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const-wide/16 v5, 0x0

    const-wide/16 v3, -0x1

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_paused"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/deskclock/CircleTimerView;->mPaused:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_interval"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalTime:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_interval_start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_current_interval"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/deskclock/CircleTimerView;->mCurrentIntervalTime:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_accum_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/deskclock/CircleTimerView;->mAccumulatedTime:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_marker_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/deskclock/CircleTimerView;->mMarkerTime:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_timer_mode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    iget-wide v1, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/deskclock/CircleTimerView;->mPaused:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/android/deskclock/CircleTimerView;->mAnimate:Z

    return-void
.end method

.method public reset()V
    .locals 2

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    iput-wide v0, p0, Lcom/android/deskclock/CircleTimerView;->mMarkerTime:J

    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    return-void
.end method

.method public setIntervalTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalTime:J

    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    return-void
.end method

.method public setMarkerTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/deskclock/CircleTimerView;->mMarkerTime:J

    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    return-void
.end method

.method public setPassedTime(JZ)V
    .locals 2
    .param p1    # J
    .param p3    # Z

    iput-wide p1, p0, Lcom/android/deskclock/CircleTimerView;->mAccumulatedTime:J

    iput-wide p1, p0, Lcom/android/deskclock/CircleTimerView;->mCurrentIntervalTime:J

    if-eqz p3, :cond_0

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    return-void
.end method

.method public setTimerMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    return-void
.end method

.method public startIntervalAnimation()V
    .locals 2

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/deskclock/CircleTimerView;->mAnimate:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/deskclock/CircleTimerView;->mPaused:Z

    return-void
.end method

.method public stopIntervalAnimation()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/deskclock/CircleTimerView;->mAnimate:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/deskclock/CircleTimerView;->mAccumulatedTime:J

    return-void
.end method

.method public writeToSharedPref(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_paused"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/deskclock/CircleTimerView;->mPaused:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_interval"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_interval_start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/deskclock/CircleTimerView;->mIntervalStartTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_current_interval"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/deskclock/CircleTimerView;->mCurrentIntervalTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_accum_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/deskclock/CircleTimerView;->mAccumulatedTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_marker_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/deskclock/CircleTimerView;->mMarkerTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ctv_timer_mode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/deskclock/CircleTimerView;->mTimerMode:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
