.class public Lcom/android/deskclock/Screensaver;
.super Landroid/service/dreams/DreamService;
.source "Screensaver.java"


# static fields
.field static final DEBUG:Z = false

.field static final TAG:Ljava/lang/String; = "DeskClock/Screensaver"


# instance fields
.field private mAnalogClock:Landroid/view/View;

.field private mContentView:Landroid/view/View;

.field private mDigitalClock:Landroid/view/View;

.field private final mHandler:Landroid/os/Handler;

.field private final mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

.field private mSaverView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/service/dreams/DreamService;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/deskclock/Screensaver;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    iget-object v1, p0, Lcom/android/deskclock/Screensaver;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/deskclock/Screensaver;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    return-void
.end method

.method private layoutClockSaver()V
    .locals 3

    const v0, 0x7f040012

    invoke-virtual {p0, v0}, Landroid/service/dreams/DreamService;->setContentView(I)V

    const v0, 0x7f0e001a

    invoke-virtual {p0, v0}, Landroid/service/dreams/DreamService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/Screensaver;->mDigitalClock:Landroid/view/View;

    const v0, 0x7f0e003a

    invoke-virtual {p0, v0}, Landroid/service/dreams/DreamService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/Screensaver;->mAnalogClock:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/deskclock/Screensaver;->setClockStyle()V

    iget-object v0, p0, Lcom/android/deskclock/Screensaver;->mSaverView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/android/deskclock/Screensaver;->mContentView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/deskclock/Screensaver;->mSaverView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/deskclock/Screensaver;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    iget-object v1, p0, Lcom/android/deskclock/Screensaver;->mContentView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/deskclock/Screensaver;->mSaverView:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;->registerViews(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method private setClockStyle()V
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/deskclock/Screensaver;->mDigitalClock:Landroid/view/View;

    iget-object v3, p0, Lcom/android/deskclock/Screensaver;->mAnalogClock:Landroid/view/View;

    const-string v4, "screensaver_clock_style"

    invoke-static {p0, v2, v3, v4}, Lcom/android/deskclock/Utils;->setClockStyle(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    const v2, 0x7f0e0039

    invoke-virtual {p0, v2}, Landroid/service/dreams/DreamService;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/deskclock/Screensaver;->mSaverView:Landroid/view/View;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "screensaver_night_mode"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v2, p0, Lcom/android/deskclock/Screensaver;->mSaverView:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/android/deskclock/Utils;->dimClockView(ZLandroid/view/View;)V

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {p0, v1}, Landroid/service/dreams/DreamService;->setScreenBright(Z)V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/service/dreams/DreamService;->onAttachedToWindow()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/service/dreams/DreamService;->setInteractive(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/service/dreams/DreamService;->setFullscreen(Z)V

    invoke-direct {p0}, Lcom/android/deskclock/Screensaver;->layoutClockSaver()V

    iget-object v0, p0, Lcom/android/deskclock/Screensaver;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/deskclock/Screensaver;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/android/deskclock/Screensaver;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/deskclock/Screensaver;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/android/deskclock/Screensaver;->layoutClockSaver()V

    iget-object v0, p0, Lcom/android/deskclock/Screensaver;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/deskclock/Screensaver;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/service/dreams/DreamService;->onCreate()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/deskclock/Screensaver;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/deskclock/Screensaver;->mMoveSaverRunnable:Lcom/android/deskclock/Utils$ScreensaverMoveSaverRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
