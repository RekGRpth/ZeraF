.class public Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;
.super Ljava/lang/Object;
.source "SwipeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;
    }
.end annotation


# static fields
.field static final ALPHA_FADE_END:F = 0.7f

.field public static ALPHA_FADE_START:F = 0.0f

.field private static final CONSTRAIN_SWIPE:Z = true

.field private static final DEBUG_INVALIDATE:Z = false

.field private static DEFAULT_ESCAPE_ANIMATION_DURATION:I = 0x0

.field private static DISMISS_ANIMATION_DURATION:I = 0x0

.field private static final DISMISS_IF_SWIPED_FAR_ENOUGH:Z = true

.field private static final FACTOR:F = 1.2f

.field private static final FADE_OUT_DURING_SWIPE:Z = true

.field private static final LOG_SWIPE_DISMISS_VELOCITY:Z = false

.field private static MAX_DISMISS_VELOCITY:I = 0x0

.field private static MAX_ESCAPE_ANIMATION_DURATION:I = 0x0

.field private static MIN_LOCK:F = 0.0f

.field private static MIN_SWIPE:F = 0.0f

.field private static MIN_VERT:F = 0.0f

.field private static final PROTECTION_PADDING:I = 0x32

.field private static SNAP_ANIM_LEN:I = 0x0

.field private static SWIPE_ESCAPE_VELOCITY:I = 0x0

.field private static SWIPE_SCROLL_SLOP:I = 0x0

.field static final TAG:Ljava/lang/String; = "com.android.systemui.SwipeHelper"

.field public static final X:I = 0x0

.field public static final Y:I = 0x1

.field private static sLinearInterpolator:Landroid/view/animation/LinearInterpolator;


# instance fields
.field private final mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

.field private mCanCurrViewBeDimissed:Z

.field private mCurrAnimView:Landroid/view/View;

.field private mCurrView:Landroid/view/View;

.field private mDensityScale:F

.field private mDragging:Z

.field private mInitialTouchPosX:F

.field private mInitialTouchPosY:F

.field private mLastY:F

.field private mMinAlpha:F

.field private mPagingTouchSlop:F

.field private mProtected:Z

.field private mStartAlpha:F

.field private final mSwipeDirection:I

.field private final mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    const/4 v0, -0x1

    sput v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:I

    const/4 v0, 0x0

    sput v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->ALPHA_FADE_START:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;FF)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;
    .param p4    # F
    .param p5    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v1, 0x3e99999a

    iput v1, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mMinAlpha:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mProtected:Z

    iput-object p3, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    iput p2, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mSwipeDirection:I

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    iput p4, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDensityScale:F

    iput p5, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mPagingTouchSlop:F

    sget v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:I

    const v1, 0x7f080002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->DEFAULT_ESCAPE_ANIMATION_DURATION:I

    const v1, 0x7f080003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MAX_ESCAPE_ANIMATION_DURATION:I

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MAX_DISMISS_VELOCITY:I

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->SNAP_ANIM_LEN:I

    const v1, 0x7f080006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->DISMISS_ANIMATION_DURATION:I

    const v1, 0x7f080007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->SWIPE_SCROLL_SLOP:I

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MIN_SWIPE:F

    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MIN_VERT:F

    const v1, 0x7f090002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MIN_LOCK:F

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;

    iget-object v0, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;)Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;
    .locals 1
    .param p0    # Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;

    iget-object v0, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;Landroid/view/View;)F
    .locals 1
    .param p0    # Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;)F
    .locals 1
    .param p0    # Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;

    iget v0, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mStartAlpha:F

    return v0
.end method

.method private createDismissAnimation(Landroid/view/View;FI)Landroid/animation/ObjectAnimator;
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # F
    .param p3    # I

    invoke-direct {p0, p1, p2}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget-object v1, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    int-to-long v1, p3

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget v1, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mSwipeDirection:I

    if-nez v1, :cond_0

    const-string v1, "translationX"

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p2, v2, v3

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v1, "translationY"

    goto :goto_0
.end method

.method private determineDuration(Landroid/view/View;FF)I
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # F
    .param p3    # F

    sget v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MAX_ESCAPE_ANIMATION_DURATION:I

    const/4 v1, 0x0

    cmpl-float v1, p3, v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    sub-float v1, p2, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x447a0000

    mul-float/2addr v1, v2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->DEFAULT_ESCAPE_ANIMATION_DURATION:I

    goto :goto_0
.end method

.method private determinePos(Landroid/view/View;F)F
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # F

    const/4 v2, 0x0

    const/4 v0, 0x0

    cmpg-float v1, p2, v2

    if-ltz v1, :cond_1

    cmpl-float v1, p2, v2

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_1

    :cond_0
    cmpl-float v1, p2, v2

    if-nez v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    iget v1, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mSwipeDirection:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v1

    neg-float v0, v1

    :goto_0
    return v0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v0

    goto :goto_0
.end method

.method private dismissChild(Landroid/view/View;F)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget-object v5, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    invoke-interface {v5, p1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    iget-object v5, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    invoke-interface {v5, p1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v2

    invoke-direct {p0, v1, p2}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->determinePos(Landroid/view/View;F)F

    move-result v4

    invoke-direct {p0, v1, v4, p2}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->determineDuration(Landroid/view/View;FF)I

    move-result v3

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    invoke-direct {p0, v1, v4, v3}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->createDismissAnimation(Landroid/view/View;FI)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v5, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$1;

    invoke-direct {v5, p0, v1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$1;-><init>(Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;Landroid/view/View;)V

    invoke-virtual {v0, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v5, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$2;

    invoke-direct {v5, p0, v2, v1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$2;-><init>(Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method private getAlphaForOffset(Landroid/view/View;)F
    .locals 6
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    const v4, 0x3f333333

    mul-float v0, v4, v3

    iget v2, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mStartAlpha:F

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    sget v4, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_1

    iget v4, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mStartAlpha:F

    sget v5, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v5, v3

    sub-float v5, v1, v5

    div-float/2addr v5, v0

    sub-float v2, v4, v5

    :cond_0
    :goto_0
    iget v4, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mMinAlpha:F

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    return v4

    :cond_1
    iget v4, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mStartAlpha:F

    sget v5, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->ALPHA_FADE_START:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, v3

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    iget v4, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mStartAlpha:F

    sget v5, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v5, v3

    add-float/2addr v5, v1

    div-float/2addr v5, v0

    add-float v2, v4, v5

    goto :goto_0
.end method

.method private getPerpendicularVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1    # Landroid/view/VelocityTracker;

    iget v0, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private getSize(Landroid/view/View;)F
    .locals 1
    .param p1    # Landroid/view/View;

    iget v0, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private getVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1    # Landroid/view/VelocityTracker;

    iget v0, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    goto :goto_0
.end method

.method public static invalidateGlobalRegion(Landroid/view/View;)V
    .locals 5
    .param p0    # Landroid/view/View;

    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {p0, v0}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/RectF;)V

    return-void
.end method

.method public static invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/RectF;)V
    .locals 5
    .param p0    # Landroid/view/View;
    .param p1    # Landroid/graphics/RectF;

    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    check-cast p0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget v0, p1, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    iget v2, p1, Landroid/graphics/RectF;->right:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setTranslation(Landroid/view/View;F)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget v0, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 18
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDragging:Z

    :goto_1
    return v15

    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mLastY:F

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDragging:Z

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    move-object/from16 v0, p1

    invoke-interface {v15, v0}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v15}, Landroid/view/VelocityTracker;->clear()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v15, :cond_0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    const v16, 0x7f0e001b

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v9

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v14

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    :goto_2
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v15

    add-int/2addr v9, v15

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v15

    add-int/2addr v14, v15

    invoke-virtual {v10}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v15

    invoke-virtual {v10}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v16

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_1

    const/4 v10, 0x0

    goto :goto_2

    :cond_1
    invoke-virtual {v10}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v15

    add-int/2addr v15, v14

    add-int/lit8 v2, v15, 0x32

    add-int/lit8 v14, v14, -0x32

    add-int/lit8 v9, v9, -0x32

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v13

    int-to-float v15, v9

    cmpl-float v15, v12, v15

    if-lez v15, :cond_3

    int-to-float v15, v14

    cmpl-float v15, v13, v15

    if-lez v15, :cond_3

    int-to-float v15, v2

    cmpg-float v15, v13, v15

    if-gez v15, :cond_3

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mProtected:Z

    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-interface/range {v15 .. v16}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getAlpha()F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mStartAlpha:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-interface/range {v15 .. v16}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCanCurrViewBeDimissed:Z

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mInitialTouchPosX:F

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mInitialTouchPosY:F

    goto/16 :goto_0

    :cond_3
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mProtected:Z

    goto :goto_3

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v15, :cond_5

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mLastY:F

    const/16 v16, 0x0

    cmpl-float v15, v15, v16

    if-ltz v15, :cond_4

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDragging:Z

    if-nez v15, :cond_4

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mInitialTouchPosY:F

    sub-float v15, v5, v15

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mInitialTouchPosX:F

    sub-float v15, v4, v15

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v7

    sget v15, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->SWIPE_SCROLL_SLOP:I

    int-to-float v15, v15

    cmpl-float v15, v8, v15

    if-lez v15, :cond_4

    const v15, 0x3f99999a

    mul-float/2addr v15, v7

    cmpl-float v15, v8, v15

    if-lez v15, :cond_4

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mLastY:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    invoke-interface {v15}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->onScroll()V

    const/4 v15, 0x0

    goto/16 :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mInitialTouchPosX:F

    sub-float v6, v11, v15

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mPagingTouchSlop:F

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-interface/range {v16 .. v17}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->onBeginDrag(Landroid/view/View;)V

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDragging:Z

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getTranslationX()F

    move-result v16

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mInitialTouchPosX:F

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mInitialTouchPosY:F

    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mLastY:F

    goto/16 :goto_0

    :pswitch_2
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDragging:Z

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    const/high16 v15, -0x40800000

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mLastY:F

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 24
    .param p1    # Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDragging:Z

    move/from16 v18, v0

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mProtected:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1

    :cond_0
    const/16 v18, 0x0

    :goto_0
    return v18

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_1
    const/16 v18, 0x1

    goto :goto_0

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mInitialTouchPosX:F

    move/from16 v19, v0

    sub-float v7, v18, v19

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mInitialTouchPosY:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDragging:Z

    move/from16 v18, v0

    if-nez v18, :cond_3

    sget v18, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MIN_VERT:F

    cmpl-float v18, v8, v18

    if-lez v18, :cond_3

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v18

    sget v19, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MIN_LOCK:F

    cmpg-float v18, v18, v19

    if-gez v18, :cond_3

    const v18, 0x3f99999a

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v19

    mul-float v18, v18, v19

    cmpl-float v18, v8, v18

    if-lez v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->onScroll()V

    const/16 v18, 0x0

    goto :goto_0

    :cond_3
    sget v13, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MIN_SWIPE:F

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v13

    if-gez v18, :cond_4

    const/16 v18, 0x1

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v18

    if-nez v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v15

    const v18, 0x3e19999a

    mul-float v11, v18, v15

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v15

    if-ltz v18, :cond_8

    const/16 v18, 0x0

    cmpl-float v18, v7, v18

    if-lez v18, :cond_7

    move v7, v11

    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v7}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->setTranslation(Landroid/view/View;F)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCanCurrViewBeDimissed:Z

    move/from16 v18, v0

    if-eqz v18, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setAlpha(F)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->invalidateGlobalRegion(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_7
    neg-float v7, v11

    goto :goto_2

    :cond_8
    div-float v18, v7, v15

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L

    mul-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v18, v0

    mul-float v7, v11, v18

    goto :goto_2

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    sget v18, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->MAX_DISMISS_VELOCITY:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDensityScale:F

    move/from16 v19, v0

    mul-float v12, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    const/16 v19, 0x3e8

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    sget v18, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDensityScale:F

    move/from16 v19, v0

    mul-float v10, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->getVelocity(Landroid/view/VelocityTracker;)F

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->getPerpendicularVelocity(Landroid/view/VelocityTracker;)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getTranslationX()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v6

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide v20, 0x3fd999999999999aL

    float-to-double v0, v6

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    cmpl-double v18, v18, v20

    if-lez v18, :cond_a

    const/4 v4, 0x1

    :goto_3
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v10

    if-lez v18, :cond_d

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v18

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v19

    cmpl-float v18, v18, v19

    if-lez v18, :cond_d

    const/16 v18, 0x0

    cmpl-float v18, v17, v18

    if-lez v18, :cond_b

    const/16 v18, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getTranslationX()F

    move-result v19

    const/16 v20, 0x0

    cmpl-float v19, v19, v20

    if-lez v19, :cond_c

    const/16 v19, 0x1

    :goto_5
    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_d

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide v20, 0x3fa999999999999aL

    float-to-double v0, v6

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    cmpl-double v18, v18, v20

    if-lez v18, :cond_d

    const/4 v5, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v18

    if-eqz v18, :cond_e

    if-nez v5, :cond_9

    if-eqz v4, :cond_e

    :cond_9
    const/4 v9, 0x1

    :goto_7
    if-eqz v9, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v18, v0

    if-eqz v5, :cond_f

    :goto_8
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->dismissChild(Landroid/view/View;F)V

    goto/16 :goto_1

    :cond_a
    const/4 v4, 0x0

    goto :goto_3

    :cond_b
    const/16 v18, 0x0

    goto :goto_4

    :cond_c
    const/16 v19, 0x0

    goto :goto_5

    :cond_d
    const/4 v5, 0x0

    goto :goto_6

    :cond_e
    const/4 v9, 0x0

    goto :goto_7

    :cond_f
    const/16 v17, 0x0

    goto :goto_8

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->snapChild(Landroid/view/View;F)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setDensityScale(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mDensityScale:F

    return-void
.end method

.method public setMinAlpha(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mMinAlpha:F

    return-void
.end method

.method public setPagingTouchSlop(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mPagingTouchSlop:F

    return-void
.end method

.method public snapChild(Landroid/view/View;F)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget-object v4, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->mCallback:Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v2

    const/4 v4, 0x0

    invoke-direct {p0, v1, v4}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget v3, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;->SNAP_ANIM_LEN:I

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v4, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$3;

    invoke-direct {v4, p0, v2, v1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$3;-><init>(Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v4, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$4;

    invoke-direct {v4, p0, v1}, Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper$4;-><init>(Lcom/android/deskclock/widget/swipeablelistview/SwipeHelper;Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method
