.class Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;
.super Landroid/widget/BaseAdapter;
.source "CitiesActivity.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/worldclock/CitiesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CityAdapter"
.end annotation


# static fields
.field private static final DELETED_ENTRY:Ljava/lang/String; = "C0"


# instance fields
.field private mAllTheCitiesList:[Ljava/lang/Object;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mIs24HoursMode:Z

.field private mSectionHeaders:[Ljava/lang/Object;

.field private mSectionPositions:[Ljava/lang/Object;

.field private final mSelectedCitiesList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/deskclock/worldclock/CityObj;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/deskclock/worldclock/CitiesActivity;


# direct methods
.method public constructor <init>(Lcom/android/deskclock/worldclock/CitiesActivity;Landroid/content/Context;Ljava/util/HashMap;Landroid/view/LayoutInflater;)V
    .locals 3
    .param p2    # Landroid/content/Context;
    .param p4    # Landroid/view/LayoutInflater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/deskclock/worldclock/CityObj;",
            ">;",
            "Landroid/view/LayoutInflater;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->this$0:Lcom/android/deskclock/worldclock/CitiesActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-direct {p0, p2}, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->loadCitiesDataBase(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSelectedCitiesList:Ljava/util/HashMap;

    iput-object p4, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/deskclock/worldclock/CitiesActivity;->access$002(Lcom/android/deskclock/worldclock/CitiesActivity;Ljava/util/Calendar;)Ljava/util/Calendar;

    invoke-static {p1}, Lcom/android/deskclock/worldclock/CitiesActivity;->access$000(Lcom/android/deskclock/worldclock/CitiesActivity;)Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {p0, p2}, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->set24HoursMode(Landroid/content/Context;)V

    return-void
.end method

.method private loadCitiesDataBase(Landroid/content/Context;)V
    .locals 13
    .param p1    # Landroid/content/Context;

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-static {p1}, Lcom/android/deskclock/Utils;->loadCitiesDataBase(Landroid/content/Context;)[Lcom/android/deskclock/worldclock/CityObj;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v7, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_1
    array-length v8, v6

    if-ge v2, v8, :cond_3

    aget-object v0, v6, v2

    iget-object v8, v0, Lcom/android/deskclock/worldclock/CityObj;->mCityId:Ljava/lang/String;

    const-string v9, "C0"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget-object v8, v0, Lcom/android/deskclock/worldclock/CityObj;->mCityName:Ljava/lang/String;

    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, v0, Lcom/android/deskclock/worldclock/CityObj;->mCityName:Ljava/lang/String;

    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Lcom/android/deskclock/worldclock/CityObj;

    invoke-direct {v8, v7, v12, v12}, Lcom/android/deskclock/worldclock/CityObj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    :cond_2
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v8

    iput-object v8, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionHeaders:[Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v8

    iput-object v8, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v8

    iput-object v8, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getPositionForSection(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    aget-object v0, v0, p1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    aget-object v1, v1, v0

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt p1, v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    add-int/lit8 v2, v0, 0x1

    aget-object v1, v1, v2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge p1, v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt p1, v1, :cond_2

    iget-object v1, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionPositions:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSectionHeaders:[Ljava/lang/Object;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v9, 0x7f0e002a

    const v8, 0x7f0e0029

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    if-eqz v5, :cond_0

    if-ltz p1, :cond_0

    iget-object v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    array-length v5, v5

    if-lt p1, v5, :cond_1

    :cond_0
    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_1
    iget-object v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    aget-object v0, v5, p1

    check-cast v0, Lcom/android/deskclock/worldclock/CityObj;

    iget-object v5, v0, Lcom/android/deskclock/worldclock/CityObj;->mCityId:Ljava/lang/String;

    if-nez v5, :cond_4

    if-eqz p2, :cond_2

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-nez v5, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f04000c

    invoke-virtual {v5, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_3
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v5, v0, Lcom/android/deskclock/worldclock/CityObj;->mCityName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    move-object v5, p2

    goto :goto_0

    :cond_4
    if-eqz p2, :cond_5

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-nez v5, :cond_6

    :cond_5
    iget-object v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f04000d

    invoke-virtual {v5, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_6
    iget-object v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->this$0:Lcom/android/deskclock/worldclock/CitiesActivity;

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v5, 0x7f0e002b

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0e002c

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mSelectedCitiesList:Ljava/util/HashMap;

    iget-object v6, v0, Lcom/android/deskclock/worldclock/CityObj;->mCityId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->this$0:Lcom/android/deskclock/worldclock/CitiesActivity;

    invoke-virtual {v1, v5}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->this$0:Lcom/android/deskclock/worldclock/CitiesActivity;

    invoke-static {v5}, Lcom/android/deskclock/worldclock/CitiesActivity;->access$000(Lcom/android/deskclock/worldclock/CitiesActivity;)Ljava/util/Calendar;

    move-result-object v5

    iget-object v6, v0, Lcom/android/deskclock/worldclock/CityObj;->mTimeZone:Ljava/lang/String;

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    iget-boolean v5, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mIs24HoursMode:Z

    if-eqz v5, :cond_7

    const-string v5, "k:mm"

    :goto_2
    iget-object v6, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->this$0:Lcom/android/deskclock/worldclock/CitiesActivity;

    invoke-static {v6}, Lcom/android/deskclock/worldclock/CitiesActivity;->access$000(Lcom/android/deskclock/worldclock/CitiesActivity;)Ljava/util/Calendar;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v0, Lcom/android/deskclock/worldclock/CityObj;->mCityName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    const-string v5, "h:mmaa"

    goto :goto_2
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mAllTheCitiesList:[Ljava/lang/Object;

    aget-object v0, v0, p1

    check-cast v0, Lcom/android/deskclock/worldclock/CityObj;

    iget-object v0, v0, Lcom/android/deskclock/worldclock/CityObj;->mCityId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public set24HoursMode(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Lcom/android/deskclock/Alarms;->get24HourMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/deskclock/worldclock/CitiesActivity$CityAdapter;->mIs24HoursMode:Z

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
