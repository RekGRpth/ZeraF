.class public final Lcom/android/deskclock/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final alarm_set:I = 0x7f0a000a

.field public static final auto_silence_entries:I = 0x7f0a000b

.field public static final auto_silence_values:I = 0x7f0a000c

.field public static final cities_id:I = 0x7f0a0002

.field public static final cities_names:I = 0x7f0a0000

.field public static final cities_tz:I = 0x7f0a0001

.field public static final clock_style_entries:I = 0x7f0a0012

.field public static final clock_style_values:I = 0x7f0a0013

.field public static final dismiss_descriptions:I = 0x7f0a0007

.field public static final dismiss_direction_descriptions:I = 0x7f0a0008

.field public static final dismiss_drawables:I = 0x7f0a0006

.field public static final snooze_dismiss_descriptions:I = 0x7f0a0004

.field public static final snooze_dismiss_direction_descriptions:I = 0x7f0a0005

.field public static final snooze_dismiss_drawables:I = 0x7f0a0003

.field public static final snooze_duration_entries:I = 0x7f0a0016

.field public static final snooze_duration_values:I = 0x7f0a0017

.field public static final stopwatch_format_set:I = 0x7f0a000f

.field public static final sw_lap_number_set:I = 0x7f0a0010

.field public static final sw_share_strings:I = 0x7f0a0011

.field public static final timer_notifications:I = 0x7f0a0009

.field public static final timezone_labels:I = 0x7f0a0014

.field public static final timezone_values:I = 0x7f0a0015

.field public static final volume_button_setting_entries:I = 0x7f0a000d

.field public static final volume_button_setting_values:I = 0x7f0a000e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
