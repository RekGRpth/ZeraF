.class public final Lcom/android/deskclock/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final actionable_toast_row:I = 0x7f040000

.field public static final alarm_alert:I = 0x7f040001

.field public static final alarm_alert_fullscreen:I = 0x7f040002

.field public static final alarm_alert_power_on:I = 0x7f040003

.field public static final alarm_alert_power_on_fullscreen:I = 0x7f040004

.field public static final alarm_clock:I = 0x7f040005

.field public static final alarm_label:I = 0x7f040006

.field public static final alarm_time:I = 0x7f040007

.field public static final analog_appwidget:I = 0x7f040008

.field public static final blank_footer_view:I = 0x7f040009

.field public static final blank_header_view:I = 0x7f04000a

.field public static final cities_activity:I = 0x7f04000b

.field public static final city_list_header:I = 0x7f04000c

.field public static final city_list_item:I = 0x7f04000d

.field public static final clock_fragment:I = 0x7f04000e

.field public static final context_menu_header:I = 0x7f04000f

.field public static final day_button:I = 0x7f040010

.field public static final desk_clock:I = 0x7f040011

.field public static final desk_clock_saver:I = 0x7f040012

.field public static final desk_clock_time_date:I = 0x7f040013

.field public static final desk_clock_time_small:I = 0x7f040014

.field public static final desk_clock_weather:I = 0x7f040015

.field public static final digital_appwidget:I = 0x7f040016

.field public static final digital_widget_time:I = 0x7f040017

.field public static final glow_pad_view:I = 0x7f040018

.field public static final label_dialog:I = 0x7f040019

.field public static final lap_view:I = 0x7f04001a

.field public static final main_clock_frame:I = 0x7f04001b

.field public static final mtk_repeat_layout:I = 0x7f04001c

.field public static final popup_window_item:I = 0x7f04001d

.field public static final set_alarm:I = 0x7f04001e

.field public static final set_alarm_action_bar:I = 0x7f04001f

.field public static final snooze_length_picker:I = 0x7f040020

.field public static final stopwatch_fragment:I = 0x7f040021

.field public static final stopwatch_notif_collapsed:I = 0x7f040022

.field public static final stopwatch_notif_expanded:I = 0x7f040023

.field public static final three_keys_view:I = 0x7f040024

.field public static final three_keys_view_ampm:I = 0x7f040025

.field public static final time_picker_dialog:I = 0x7f040026

.field public static final time_picker_view:I = 0x7f040027

.field public static final time_setup_view:I = 0x7f040028

.field public static final timer_alert_full_screen:I = 0x7f040029

.field public static final timer_fragment:I = 0x7f04002a

.field public static final timer_list_item:I = 0x7f04002b

.field public static final world_clock_item:I = 0x7f04002c

.field public static final world_clock_label:I = 0x7f04002d

.field public static final world_clock_list_item:I = 0x7f04002e

.field public static final world_clock_remote_list_item:I = 0x7f04002f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
