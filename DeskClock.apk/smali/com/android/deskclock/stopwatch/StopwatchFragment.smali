.class public Lcom/android/deskclock/stopwatch/StopwatchFragment;
.super Lcom/android/deskclock/DeskClockFragment;
.source "StopwatchFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/deskclock/stopwatch/StopwatchFragment$ImageLabelAdapter;,
        Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;,
        Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;
    }
.end annotation


# static fields
.field private static final ACCUM_TIME_KEY:Ljava/lang/String; = "accum_time"

.field private static final LAPS_KEY:Ljava/lang/String; = "laps"

.field private static final START_TIME_KEY:Ljava/lang/String; = "start_time"

.field private static final STATE_KEY:Ljava/lang/String; = "state"

.field private static final TAG:Ljava/lang/String; = "StopwatchFragment"

.field public static mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;


# instance fields
.field mAccumulatedTime:J

.field private mCenterButton:Landroid/widget/TextView;

.field private mLapsList:Landroid/widget/ListView;

.field private mLeftButton:Landroid/widget/ImageButton;

.field private mShareButton:Landroid/widget/ImageButton;

.field private mSharePopup:Landroid/widget/ListPopupWindow;

.field mStartTime:J

.field mState:I

.field private mTime:Lcom/android/deskclock/CircleTimerView;

.field private mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

.field mTimeUpdateThread:Ljava/lang/Runnable;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/android/deskclock/DeskClockFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    iput-wide v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mStartTime:J

    iput-wide v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    new-instance v0, Lcom/android/deskclock/stopwatch/StopwatchFragment$6;

    invoke-direct {v0, p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$6;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V

    iput-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeUpdateThread:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/stopwatch/StopwatchFragment;J)V
    .locals 0
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->addLapTime(J)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->doLap()V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/deskclock/stopwatch/StopwatchFragment;J)V
    .locals 0
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->updateCurrentLap(J)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->releaseWakeLock()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->showSharePopup()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->rightButtonAction()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/deskclock/stopwatch/StopwatchFragment;)Landroid/widget/ListPopupWindow;
    .locals 1
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/deskclock/stopwatch/StopwatchFragment;Landroid/widget/ListPopupWindow;)Landroid/widget/ListPopupWindow;
    .locals 0
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;
    .param p1    # Landroid/widget/ListPopupWindow;

    iput-object p1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    return-object p1
.end method

.method static synthetic access$600(Lcom/android/deskclock/stopwatch/StopwatchFragment;)Landroid/content/Intent;
    .locals 1
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->getShareIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/deskclock/stopwatch/StopwatchFragment;)Lcom/android/deskclock/CircleTimerView;
    .locals 1
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/deskclock/stopwatch/StopwatchFragment;)Lcom/android/deskclock/timer/CountingTimerView;
    .locals 1
    .param p0    # Lcom/android/deskclock/stopwatch/StopwatchFragment;

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    return-object v0
.end method

.method private acquireWakeLock()V
    .locals 3

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v1, 0x2000000a

    const-string v2, "StopwatchFragment"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    return-void
.end method

.method private addLapTime(J)V
    .locals 13
    .param p1    # J

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v1}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getCount()I

    move-result v12

    iget-wide v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mStartTime:J

    sub-long v4, p1, v4

    iget-wide v6, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    add-long v2, v4, v6

    if-nez v12, :cond_1

    new-instance v0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;JJ)V

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v1, v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->addLap(Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;)V

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    new-instance v4, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    const-wide/16 v6, 0x0

    move-object v5, p0

    move-wide v8, v2

    invoke-direct/range {v4 .. v9}, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;JJ)V

    invoke-virtual {v1, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->addLap(Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v1, v2, v3}, Lcom/android/deskclock/CircleTimerView;->setIntervalTime(J)V

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v1, v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->updateTimeFormats(Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;)Z

    :goto_0
    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v1}, Lcom/android/deskclock/CircleTimerView;->stopIntervalAnimation()V

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->reachedMaxLaps()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v1}, Lcom/android/deskclock/CircleTimerView;->startIntervalAnimation()V

    :cond_0
    return-void

    :cond_1
    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iget-wide v4, v1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    sub-long v10, v2, v4

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iput-wide v10, v1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mLapTime:J

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iput-wide v2, v1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    new-instance v4, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;JJ)V

    invoke-virtual {v1, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->addLap(Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v1, v10, v11}, Lcom/android/deskclock/CircleTimerView;->setMarkerTime(J)V

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-static {v1}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->access$700(Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;)V

    goto :goto_0
.end method

.method private doLap()V
    .locals 1

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->showLaps()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setButtons(I)V

    return-void
.end method

.method private doStart(J)V
    .locals 3
    .param p1    # J

    const/4 v2, 0x1

    iput-wide p1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mStartTime:J

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->startUpdateThread()V

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/deskclock/timer/CountingTimerView;->blinkTimeStr(Z)V

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Lcom/android/deskclock/CircleTimerView;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Lcom/android/deskclock/CircleTimerView;->startIntervalAnimation()V

    :cond_0
    invoke-direct {p0, v2}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setButtons(I)V

    iput v2, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    return-void
.end method

.method private doStop()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->stopUpdateThread()V

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Lcom/android/deskclock/CircleTimerView;->pauseIntervalAnimation()V

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    iget-wide v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/android/deskclock/timer/CountingTimerView;->setTime(JZZ)V

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-virtual {v0, v3}, Lcom/android/deskclock/timer/CountingTimerView;->blinkTimeStr(Z)V

    iget-wide v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    invoke-direct {p0, v0, v1}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->updateCurrentLap(J)V

    invoke-direct {p0, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setButtons(I)V

    iput v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    return-void
.end method

.method private getLapShareTimes([J)[J
    .locals 9
    .param p1    # [J

    if-nez p1, :cond_1

    const/4 v4, 0x0

    :cond_0
    return-object v4

    :cond_1
    array-length v3, p1

    new-array v4, v3, [J

    const-wide/16 v5, 0x0

    add-int/lit8 v2, v3, -0x1

    :goto_0
    if-ltz v2, :cond_0

    aget-wide v0, p1, v2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "lap "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/deskclock/Log;->v(Ljava/lang/String;)V

    sub-long v7, v0, v5

    aput-wide v7, v4, v2

    move-wide v5, v0

    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method private getShareIntent()Landroid/content/Intent;
    .locals 5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/deskclock/stopwatch/Stopwatches;->getShareTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-virtual {v3}, Lcom/android/deskclock/timer/CountingTimerView;->getTimeString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getLapTimes()[J

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->getLapShareTimes([J)[J

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/android/deskclock/stopwatch/Stopwatches;->buildShareResults(Landroid/content/Context;Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private reachedMaxLaps()Z
    .locals 2

    sget-object v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getCount()I

    move-result v0

    const/16 v1, 0x63

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readFromSharedPref(Landroid/content/SharedPreferences;)V
    .locals 12
    .param p1    # Landroid/content/SharedPreferences;

    const-string v9, "sw_start_time"

    const-wide/16 v10, 0x0

    invoke-interface {p1, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    iput-wide v9, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mStartTime:J

    const-string v9, "sw_accum_time"

    const-wide/16 v10, 0x0

    invoke-interface {p1, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    iput-wide v9, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    const-string v9, "sw_state"

    const/4 v10, 0x0

    invoke-interface {p1, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    const-string v9, "sw_lap_num"

    const/4 v10, 0x0

    invoke-interface {p1, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sget-object v9, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    if-eqz v9, :cond_2

    sget-object v9, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v9}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getLapTimes()[J

    move-result-object v6

    if-eqz v6, :cond_0

    array-length v9, v6

    if-ge v9, v5, :cond_2

    :cond_0
    new-array v4, v5, [J

    const-wide/16 v7, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sw_lap_time_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v3, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v9, 0x0

    invoke-interface {p1, v0, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    sub-int v9, v5, v3

    add-int/lit8 v9, v9, -0x1

    sub-long v10, v1, v7

    aput-wide v10, v4, v9

    move-wide v7, v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    sget-object v9, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v9, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->setLapTimes([J)V

    :cond_2
    const-string v9, "sw_update_circle"

    const/4 v10, 0x1

    invoke-interface {p1, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_3

    iget v9, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_4

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->doStop()V

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget v9, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_5

    iget-wide v9, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mStartTime:J

    invoke-direct {p0, v9, v10}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->doStart(J)V

    goto :goto_1

    :cond_5
    iget v9, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    if-nez v9, :cond_3

    invoke-virtual {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->doReset()V

    goto :goto_1
.end method

.method private releaseWakeLock()V
    .locals 1

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void
.end method

.method private rightButtonAction()V
    .locals 10

    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v4

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v3, Landroid/content/Intent;

    const-class v6, Lcom/android/deskclock/stopwatch/StopwatchService;

    invoke-direct {v3, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "message_time"

    invoke-virtual {v3, v6, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v6, "show_notification"

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget v6, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    packed-switch v6, :pswitch_data_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Illegal state "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " while pressing the right stopwatch button"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/deskclock/Log;->wtf(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/android/deskclock/Utils;->getTimeNow()J

    move-result-wide v1

    iget-wide v6, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    iget-wide v8, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mStartTime:J

    sub-long v8, v1, v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->doStop()V

    const-string v6, "stop_stopwatch"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->releaseWakeLock()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v4, v5}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->doStart(J)V

    const-string v6, "start_stopwatch"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->acquireWakeLock()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setButton(Landroid/widget/ImageButton;IIZI)V
    .locals 1
    .param p1    # Landroid/widget/ImageButton;
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .param p5    # I

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1, p5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1, p4}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method private setButtons(I)V
    .locals 12
    .param p1    # I

    const v2, 0x7f0d005c

    const v11, 0x7f0d005a

    const v3, 0x7f020045

    const/4 v9, 0x1

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLeftButton:Landroid/widget/ImageButton;

    const/4 v5, 0x4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setButton(Landroid/widget/ImageButton;IIZI)V

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mCenterButton:Landroid/widget/TextView;

    invoke-direct {p0, v0, v11}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setStartStopText(Landroid/widget/TextView;I)V

    invoke-direct {p0, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->showShareButton(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v6, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLeftButton:Landroid/widget/ImageButton;

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->reachedMaxLaps()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_1
    move-object v5, p0

    move v7, v2

    move v8, v3

    move v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setButton(Landroid/widget/ImageButton;IIZI)V

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mCenterButton:Landroid/widget/TextView;

    const v1, 0x7f0d005b

    invoke-direct {p0, v0, v1}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setStartStopText(Landroid/widget/TextView;I)V

    invoke-direct {p0, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->showShareButton(Z)V

    goto :goto_0

    :cond_0
    move v9, v4

    goto :goto_1

    :pswitch_2
    iget-object v6, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLeftButton:Landroid/widget/ImageButton;

    const v7, 0x7f0d005d

    const v8, 0x7f02005b

    move-object v5, p0

    move v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setButton(Landroid/widget/ImageButton;IIZI)V

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mCenterButton:Landroid/widget/TextView;

    invoke-direct {p0, v0, v11}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setStartStopText(Landroid/widget/TextView;I)V

    invoke-direct {p0, v9}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->showShareButton(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setStartStopText(Landroid/widget/TextView;I)V
    .locals 2
    .param p1    # Landroid/widget/TextView;
    .param p2    # I

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showLaps()V
    .locals 2

    sget-object v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsList:Landroid/widget/ListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private showShareButton(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mShareButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mShareButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mShareButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private showSharePopup()V
    .locals 26

    invoke-direct/range {p0 .. p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->getShareIntent()Landroid/content/Intent;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v24

    const/high16 v3, 0x10000

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/16 v23, 0x0

    :goto_1
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v23

    if-ge v0, v3, :cond_2

    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v21

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v19

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v3

    const/4 v5, 0x4

    if-le v3, v5, :cond_1

    const/4 v3, 0x3

    move/from16 v0, v23

    if-ge v0, v3, :cond_1

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object/from16 v0, v22

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v22

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v23, v23, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x4

    if-le v3, v5, :cond_3

    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0d0083

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x106000d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v3}, Landroid/widget/ListPopupWindow;->dismiss()V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    :cond_4
    new-instance v3, Landroid/widget/ListPopupWindow;

    invoke-direct {v3, v4}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mShareButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v5}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    new-instance v2, Lcom/android/deskclock/stopwatch/StopwatchFragment$ImageLabelAdapter;

    const v5, 0x7f04001d

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/android/deskclock/stopwatch/StopwatchFragment$ImageLabelAdapter;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x4

    if-le v3, v5, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    new-instance v10, Lcom/android/deskclock/stopwatch/StopwatchFragment$ImageLabelAdapter;

    const v13, 0x7f04001d

    move-object/from16 v11, p0

    move-object v12, v4

    move-object/from16 v16, v8

    move-object/from16 v17, v9

    move-object/from16 v18, v2

    invoke-direct/range {v10 .. v18}, Lcom/android/deskclock/stopwatch/StopwatchFragment$ImageLabelAdapter;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/deskclock/stopwatch/StopwatchFragment$ImageLabelAdapter;)V

    invoke-virtual {v3, v10}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    new-instance v5, Lcom/android/deskclock/stopwatch/StopwatchFragment$4;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$4;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V

    invoke-virtual {v3, v5}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    new-instance v5, Lcom/android/deskclock/stopwatch/StopwatchFragment$5;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$5;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V

    invoke-virtual {v3, v5}, Landroid/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v10, 0x7f090013

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v5}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v3}, Landroid/widget/ListPopupWindow;->show()V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v3, v2}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_2
.end method

.method private startUpdateThread()V
    .locals 2

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeUpdateThread:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private stopUpdateThread()V
    .locals 2

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeUpdateThread:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private updateCurrentLap(J)V
    .locals 3
    .param p1    # J

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v1}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;

    iget-wide v1, v1, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    sub-long v1, p1, v1

    iput-wide v1, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mLapTime:J

    iput-wide p1, v0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method private writeToSharedPref(Landroid/content/SharedPreferences;)V
    .locals 13
    .param p1    # Landroid/content/SharedPreferences;

    const-wide/16 v11, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "sw_start_time"

    iget-wide v5, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mStartTime:J

    invoke-interface {v0, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v4, "sw_accum_time"

    iget-wide v5, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    invoke-interface {v0, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v4, "sw_state"

    iget v5, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    sget-object v4, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->getLapTimes()[J

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v4, "sw_lap_num"

    array-length v5, v3

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v1, 0x0

    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sw_lap_time_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v3

    sub-int/2addr v5, v1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aget-wide v4, v3, v1

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    if-ne v4, v10, :cond_2

    const-string v4, "notif_clock_base"

    iget-wide v5, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mStartTime:J

    iget-wide v7, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    sub-long/2addr v5, v7

    invoke-interface {v0, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v4, "notif_clock_elapsed"

    invoke-interface {v0, v4, v11, v12}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v4, "notif_clock_running"

    invoke-interface {v0, v4, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_1
    :goto_1
    const-string v4, "sw_update_circle"

    invoke-interface {v0, v4, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_2
    iget v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    const-string v4, "notif_clock_elapsed"

    iget-wide v5, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    invoke-interface {v0, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v4, "notif_clock_base"

    invoke-interface {v0, v4, v11, v12}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v4, "notif_clock_running"

    invoke-interface {v0, v4, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :cond_3
    iget v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    if-nez v4, :cond_1

    const-string v4, "notif_clock_base"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v4, "notif_clock_running"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v4, "notif_clock_elapsed"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1
.end method


# virtual methods
.method public doReset()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lcom/android/deskclock/Utils;->clearSwSharedPref(Landroid/content/SharedPreferences;)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    const-string v2, "sw"

    invoke-virtual {v1, v0, v2}, Lcom/android/deskclock/CircleTimerView;->clearSharedPref(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    sget-object v1, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v1}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;->clearLaps()V

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->showLaps()V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v1}, Lcom/android/deskclock/CircleTimerView;->stopIntervalAnimation()V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v1}, Lcom/android/deskclock/CircleTimerView;->reset()V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    iget-wide v2, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    invoke-virtual {v1, v2, v3, v5, v5}, Lcom/android/deskclock/timer/CountingTimerView;->setTime(JZZ)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-virtual {v1, v4}, Lcom/android/deskclock/timer/CountingTimerView;->blinkTimeStr(Z)V

    invoke-direct {p0, v4}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setButtons(I)V

    iput v4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v4, 0x7f0e0062

    const v2, 0x7f0e0061

    const v1, 0x7f0e0060

    const v6, 0x7f0e005f

    const/4 v7, 0x0

    const v3, 0x7f040021

    invoke-virtual {p1, v3, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLeftButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLeftButton:Landroid/widget/ImageButton;

    new-instance v5, Lcom/android/deskclock/stopwatch/StopwatchFragment$1;

    invoke-direct {v5, p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$1;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mCenterButton:Landroid/widget/TextView;

    const v3, 0x7f0e0063

    invoke-virtual {v10, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mShareButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mShareButton:Landroid/widget/ImageButton;

    new-instance v5, Lcom/android/deskclock/stopwatch/StopwatchFragment$2;

    invoke-direct {v5, p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$2;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v10, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/android/deskclock/timer/CountingTimerView;

    new-instance v3, Lcom/android/deskclock/stopwatch/StopwatchFragment$3;

    invoke-direct {v3, p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment$3;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V

    invoke-virtual {v9, v3}, Lcom/android/deskclock/timer/CountingTimerView;->registerVirtualButtonAction(Ljava/lang/Runnable;)V

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mCenterButton:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Lcom/android/deskclock/timer/CountingTimerView;->registerStopTextView(Landroid/widget/TextView;)V

    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Lcom/android/deskclock/timer/CountingTimerView;->setVirtualButtonEnabled(Z)V

    invoke-virtual {v10, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/deskclock/CircleTimerView;

    iput-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v10, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/deskclock/timer/CountingTimerView;

    iput-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    const v3, 0x7f0e0064

    invoke-virtual {v10, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsList:Landroid/widget/ListView;

    invoke-virtual {v3, v7}, Landroid/widget/ListView;->setDividerHeight(I)V

    new-instance v3, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v3, p0, v5}, Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;-><init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;Landroid/content/Context;)V

    sput-object v3, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsList:Landroid/widget/ListView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsList:Landroid/widget/ListView;

    sget-object v5, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mLapsAdapter:Lcom/android/deskclock/stopwatch/StopwatchFragment$LapsListAdapter;

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    const v3, 0x7f0e005e

    invoke-virtual {v10, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/CircleButtonsLinearLayout;

    const v3, 0x7f0e0063

    const v5, 0x7f090047

    const v6, 0x7f09004a

    move v8, v7

    invoke-virtual/range {v0 .. v8}, Lcom/android/deskclock/CircleButtonsLinearLayout;->setCircleTimerViewIds(IIIIIIII)V

    return-object v10
.end method

.method public onPageChanged(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->acquireWakeLock()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->releaseWakeLock()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    iget v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->stopUpdateThread()V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0, v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->writeToSharedPref(Landroid/content/SharedPreferences;)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    const-string v2, "sw"

    invoke-virtual {v1, v0, v2}, Lcom/android/deskclock/CircleTimerView;->writeToSharedPref(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/deskclock/timer/CountingTimerView;->blinkTimeStr(Z)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mSharePopup:Landroid/widget/ListPopupWindow;

    :cond_1
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/android/deskclock/DeskClock;

    invoke-virtual {v1, p0}, Lcom/android/deskclock/DeskClock;->unregisterPageChangedListener(Lcom/android/deskclock/DeskClockFragment;)V

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->releaseWakeLock()V

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0, v0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->readFromSharedPref(Landroid/content/SharedPreferences;)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    const-string v2, "sw"

    invoke-virtual {v1, v0, v2}, Lcom/android/deskclock/CircleTimerView;->readFromSharedPref(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v1}, Landroid/view/View;->postInvalidate()V

    iget v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    invoke-direct {p0, v1}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->setButtons(I)V

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    iget-wide v2, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    invoke-virtual {v1, v2, v3, v5, v5}, Lcom/android/deskclock/timer/CountingTimerView;->setTime(JZZ)V

    iget v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    if-ne v1, v5, :cond_2

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->acquireWakeLock()V

    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->startUpdateThread()V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->showLaps()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/android/deskclock/DeskClock;

    invoke-virtual {v1, p0}, Lcom/android/deskclock/DeskClock;->registerPageChangedListener(Lcom/android/deskclock/DeskClockFragment;)V

    const-string v1, "sw_lap_num"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v1}, Lcom/android/deskclock/CircleTimerView;->stopIntervalAnimation()V

    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    return-void

    :cond_2
    iget v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-wide v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mAccumulatedTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTimeText:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-virtual {v1, v5}, Lcom/android/deskclock/timer/CountingTimerView;->blinkTimeStr(Z)V

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "sw_lap_num"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "sw_lap_time_"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/deskclock/stopwatch/StopwatchFragment;->readFromSharedPref(Landroid/content/SharedPreferences;)V

    const-string v0, "sw_update_circle"

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment;->mTime:Lcom/android/deskclock/CircleTimerView;

    const-string v1, "sw"

    invoke-virtual {v0, p1, v1}, Lcom/android/deskclock/CircleTimerView;->readFromSharedPref(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
