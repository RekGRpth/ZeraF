.class Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;
.super Ljava/lang/Object;
.source "StopwatchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/stopwatch/StopwatchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Lap"
.end annotation


# instance fields
.field public mLapTime:J

.field public mTotalTime:J

.field final synthetic this$0:Lcom/android/deskclock/stopwatch/StopwatchFragment;


# direct methods
.method constructor <init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;)V
    .locals 2

    const-wide/16 v0, 0x0

    iput-object p1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->this$0:Lcom/android/deskclock/stopwatch/StopwatchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mLapTime:J

    iput-wide v0, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    return-void
.end method

.method constructor <init>(Lcom/android/deskclock/stopwatch/StopwatchFragment;JJ)V
    .locals 0
    .param p2    # J
    .param p4    # J

    iput-object p1, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->this$0:Lcom/android/deskclock/stopwatch/StopwatchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mLapTime:J

    iput-wide p4, p0, Lcom/android/deskclock/stopwatch/StopwatchFragment$Lap;->mTotalTime:J

    return-void
.end method
