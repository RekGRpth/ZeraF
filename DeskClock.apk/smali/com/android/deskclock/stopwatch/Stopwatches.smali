.class public Lcom/android/deskclock/stopwatch/Stopwatches;
.super Ljava/lang/Object;
.source "Stopwatches.java"


# static fields
.field public static final KEY:Ljava/lang/String; = "sw"

.field public static final KILL_NOTIF:Ljava/lang/String; = "kill_notification"

.field public static final LAP_STOPWATCH:Ljava/lang/String; = "lap_stopwatch"

.field public static final MAX_LAPS:I = 0x63

.field public static final MESSAGE_TIME:Ljava/lang/String; = "message_time"

.field public static final NOTIF_CLOCK_BASE:Ljava/lang/String; = "notif_clock_base"

.field public static final NOTIF_CLOCK_ELAPSED:Ljava/lang/String; = "notif_clock_elapsed"

.field public static final NOTIF_CLOCK_RUNNING:Ljava/lang/String; = "notif_clock_running"

.field public static final PREF_ACCUM_TIME:Ljava/lang/String; = "sw_accum_time"

.field public static final PREF_LAP_NUM:Ljava/lang/String; = "sw_lap_num"

.field public static final PREF_LAP_TIME:Ljava/lang/String; = "sw_lap_time_"

.field public static final PREF_START_TIME:Ljava/lang/String; = "sw_start_time"

.field public static final PREF_STATE:Ljava/lang/String; = "sw_state"

.field public static final PREF_UPDATE_CIRCLE:Ljava/lang/String; = "sw_update_circle"

.field public static final RESET_AND_LAUNCH_STOPWATCH:Ljava/lang/String; = "reset_and_launch_stopwatch"

.field public static final RESET_STOPWATCH:Ljava/lang/String; = "reset_stopwatch"

.field public static final SHARE_STOPWATCH:Ljava/lang/String; = "share_stopwatch"

.field public static final SHOW_NOTIF:Ljava/lang/String; = "show_notification"

.field public static final START_STOPWATCH:Ljava/lang/String; = "start_stopwatch"

.field public static final STOPWATCH_RESET:I = 0x0

.field public static final STOPWATCH_RUNNING:I = 0x1

.field public static final STOPWATCH_STOPPED:I = 0x2

.field public static final STOP_STOPWATCH:Ljava/lang/String; = "stop_stopwatch"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildShareResults(Landroid/content/Context;J[J)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # [J

    invoke-static {p1, p2}, Lcom/android/deskclock/stopwatch/Stopwatches;->getTimeText(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p3}, Lcom/android/deskclock/stopwatch/Stopwatches;->buildShareResults(Landroid/content/Context;Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static buildShareResults(Landroid/content/Context;Ljava/lang/String;[J)Ljava/lang/String;
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # [J

    const/4 v10, 0x1

    const/4 v4, 0x0

    const v5, 0x7f0d0066

    new-array v6, v10, [Ljava/lang/Object;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-nez p2, :cond_0

    move v1, v4

    :goto_0
    if-nez v1, :cond_1

    move-object v3, v2

    :goto_1
    return-object v3

    :cond_0
    array-length v1, p2

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f0d0067

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    :goto_2
    if-gt v0, v1, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%d. %s\n"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    sub-int v8, v1, v0

    aget-wide v8, p2, v8

    invoke-static {v8, v9}, Lcom/android/deskclock/stopwatch/Stopwatches;->getTimeText(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move-object v3, v2

    goto :goto_1
.end method

.method public static formatTimeText(JLjava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p0    # J
    .param p2    # Ljava/lang/String;

    const-wide/16 v9, 0x0

    cmp-long v9, p0, v9

    if-gez v9, :cond_0

    const-wide/16 p0, 0x0

    :cond_0
    const-wide/16 v9, 0x3e8

    div-long v6, p0, v9

    const-wide/16 v9, 0x3e8

    mul-long/2addr v9, v6

    sub-long v9, p0, v9

    const-wide/16 v11, 0xa

    div-long v2, v9, v11

    const-wide/16 v9, 0x3c

    div-long v4, v6, v9

    const-wide/16 v9, 0x3c

    mul-long/2addr v9, v4

    sub-long/2addr v6, v9

    const-wide/16 v9, 0x3c

    div-long v0, v4, v9

    const-wide/16 v9, 0x3c

    mul-long/2addr v9, v0

    sub-long/2addr v4, v9

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {p2, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public static getShareTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    array-length v3, v0

    int-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    aget-object v1, v0, v1

    return-object v1
.end method

.method public static getTimeText(J)Ljava/lang/String;
    .locals 13
    .param p0    # J

    const-wide/16 v9, 0x0

    cmp-long v9, p0, v9

    if-gez v9, :cond_0

    const-wide/16 p0, 0x0

    :cond_0
    const-wide/16 v9, 0x3e8

    div-long v6, p0, v9

    const-wide/16 v9, 0x3e8

    mul-long/2addr v9, v6

    sub-long v9, p0, v9

    const-wide/16 v11, 0xa

    div-long v2, v9, v11

    const-wide/16 v9, 0x3c

    div-long v4, v6, v9

    const-wide/16 v9, 0x3c

    mul-long/2addr v9, v4

    sub-long/2addr v6, v9

    const-wide/16 v9, 0x3c

    div-long v0, v4, v9

    const-wide/16 v9, 0x3c

    mul-long/2addr v9, v0

    sub-long/2addr v4, v9

    const-wide/16 v9, 0x63

    cmp-long v9, v0, v9

    if-lez v9, :cond_1

    const-wide/16 v0, 0x0

    :cond_1
    const-wide/16 v9, 0xa

    cmp-long v9, v0, v9

    if-ltz v9, :cond_2

    const-string v9, "%02dh %02dm %02ds .%02d"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    :goto_0
    return-object v8

    :cond_2
    const-wide/16 v9, 0x0

    cmp-long v9, v0, v9

    if-lez v9, :cond_3

    const-string v9, "%01dh %02dm %02ds .%02d"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    :cond_3
    const-wide/16 v9, 0xa

    cmp-long v9, v4, v9

    if-ltz v9, :cond_4

    const-string v9, "%02dm %02ds .%02d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    :cond_4
    const-string v9, "%02dm %02ds .%02d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method
