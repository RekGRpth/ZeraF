.class public final Lcom/android/deskclock/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/deskclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abbrev_wday_month_day_no_year:I = 0x7f0d0000

.field public static final add_alarm:I = 0x7f0d000f

.field public static final alarm_alert_alert_silenced:I = 0x7f0d0021

.field public static final alarm_alert_dismiss_text:I = 0x7f0d0020

.field public static final alarm_alert_snooze_set:I = 0x7f0d0023

.field public static final alarm_alert_snooze_text:I = 0x7f0d0022

.field public static final alarm_alert_snooze_until:I = 0x7f0d0024

.field public static final alarm_button_description:I = 0x7f0d0041

.field public static final alarm_command_summary_format:I = 0x7f0d0009

.field public static final alarm_command_summary_format_land:I = 0x7f0d000a

.field public static final alarm_deleted:I = 0x7f0d0096

.field public static final alarm_in_silent_mode_summary:I = 0x7f0d00a7

.field public static final alarm_in_silent_mode_title:I = 0x7f0d00a6

.field public static final alarm_klaxon_service_desc:I = 0x7f0d004b

.field public static final alarm_list_title:I = 0x7f0d000e

.field public static final alarm_notify_snooze_label:I = 0x7f0d003c

.field public static final alarm_notify_snooze_text:I = 0x7f0d003d

.field public static final alarm_notify_text:I = 0x7f0d003b

.field public static final alarm_off:I = 0x7f0d0075

.field public static final alarm_on:I = 0x7f0d0074

.field public static final alarm_repeat:I = 0x7f0d001c

.field public static final alarm_settings:I = 0x7f0d0082

.field public static final alarm_undo:I = 0x7f0d0095

.field public static final alarm_vibrate:I = 0x7f0d001b

.field public static final alarm_volume_summary:I = 0x7f0d00a8

.field public static final alarm_volume_title:I = 0x7f0d0039

.field public static final alarms_selected:I = 0x7f0d009b

.field public static final alert:I = 0x7f0d001d

.field public static final all_timers_stopped_notif:I = 0x7f0d008d

.field public static final analog_gadget:I = 0x7f0d002f

.field public static final app_label:I = 0x7f0d000d

.field public static final auto_silence_never:I = 0x7f0d0035

.field public static final auto_silence_summary:I = 0x7f0d0034

.field public static final auto_silence_title:I = 0x7f0d0033

.field public static final automatic_home_clock:I = 0x7f0d0078

.field public static final automatic_home_clock_summary:I = 0x7f0d0079

.field public static final battery_charging_level:I = 0x7f0d0049

.field public static final button_alarms:I = 0x7f0d0053

.field public static final button_cities:I = 0x7f0d0054

.field public static final button_menu:I = 0x7f0d0055

.field public static final cities_activity_title:I = 0x7f0d0073

.field public static final city_delete_confirmation:I = 0x7f0d009e

.field public static final clock_instructions:I = 0x7f0d002e

.field public static final clock_settings:I = 0x7f0d0076

.field public static final clock_style:I = 0x7f0d0077

.field public static final collapse_alarm:I = 0x7f0d0094

.field public static final control_set_alarm:I = 0x7f0d004e

.field public static final control_set_alarm_with_existing:I = 0x7f0d004f

.field public static final day:I = 0x7f0d0025

.field public static final day_concat:I = 0x7f0d002d

.field public static final days:I = 0x7f0d0026

.field public static final default_clock_style:I = 0x7f0d0002

.field public static final default_label:I = 0x7f0d0019

.field public static final default_ringtone_setting_title:I = 0x7f0d0040

.field public static final delete:I = 0x7f0d0038

.field public static final delete_alarm:I = 0x7f0d0012

.field public static final delete_alarm_confirm:I = 0x7f0d0015

.field public static final deleted_message:I = 0x7f0d009c

.field public static final description_direction_down:I = 0x7f0d008a

.field public static final description_direction_left:I = 0x7f0d0088

.field public static final description_direction_right:I = 0x7f0d0087

.field public static final description_direction_up:I = 0x7f0d0089

.field public static final desk_clock_button_description:I = 0x7f0d0046

.field public static final desk_clock_help_url:I = 0x7f0d0084

.field public static final digital_gadget:I = 0x7f0d009f

.field public static final disable_alarm:I = 0x7f0d0014

.field public static final done:I = 0x7f0d0036

.field public static final enable_alarm:I = 0x7f0d0013

.field public static final every_day:I = 0x7f0d002b

.field public static final expand_alarm:I = 0x7f0d0093

.field public static final full_wday_month_day_no_year:I = 0x7f0d0001

.field public static final gallery_button_description:I = 0x7f0d0042

.field public static final help:I = 0x7f0d0030

.field public static final hide_clock:I = 0x7f0d0017

.field public static final home_button_description:I = 0x7f0d0045

.field public static final home_label:I = 0x7f0d0072

.field public static final home_time_zone:I = 0x7f0d007a

.field public static final home_time_zone_title:I = 0x7f0d007b

.field public static final hour:I = 0x7f0d0027

.field public static final hours:I = 0x7f0d0028

.field public static final hours_label:I = 0x7f0d005f

.field public static final hours_label_description:I = 0x7f0d0062

.field public static final label:I = 0x7f0d0018

.field public static final label_description:I = 0x7f0d0047

.field public static final label_unlabeled:I = 0x7f0d009a

.field public static final loading_ringtone:I = 0x7f0d004c

.field public static final menu_clock:I = 0x7f0d0051

.field public static final menu_desk_clock:I = 0x7f0d0010

.field public static final menu_edit_alarm:I = 0x7f0d0011

.field public static final menu_item_dock_settings:I = 0x7f0d00a9

.field public static final menu_item_help:I = 0x7f0d0057

.field public static final menu_item_night_mode:I = 0x7f0d0058

.field public static final menu_item_settings:I = 0x7f0d0056

.field public static final menu_stopwatch:I = 0x7f0d0052

.field public static final menu_timer:I = 0x7f0d0050

.field public static final minute:I = 0x7f0d0029

.field public static final minutes:I = 0x7f0d002a

.field public static final minutes_label:I = 0x7f0d0060

.field public static final minutes_label_description:I = 0x7f0d0063

.field public static final music_button_description:I = 0x7f0d0043

.field public static final never:I = 0x7f0d002c

.field public static final next_alarm_description:I = 0x7f0d0099

.field public static final next_timer_notif:I = 0x7f0d008f

.field public static final night_mode_summary:I = 0x7f0d0092

.field public static final night_mode_title:I = 0x7f0d0091

.field public static final nightmode_button_description:I = 0x7f0d0044

.field public static final power_off:I = 0x7f0d0004

.field public static final power_on:I = 0x7f0d0003

.field public static final repeat_everyday:I = 0x7f0d000b

.field public static final repeat_none:I = 0x7f0d000c

.field public static final repeat_weekdays:I = 0x7f0d0005

.field public static final revert:I = 0x7f0d0037

.field public static final ringtone:I = 0x7f0d001e

.field public static final ringtone_description:I = 0x7f0d0048

.field public static final screensaver_settings:I = 0x7f0d0090

.field public static final seconds_label:I = 0x7f0d0061

.field public static final seconds_label_description:I = 0x7f0d0064

.field public static final see_all:I = 0x7f0d0083

.field public static final set_alarm:I = 0x7f0d001a

.field public static final settings:I = 0x7f0d0031

.field public static final show_clock:I = 0x7f0d0016

.field public static final silent_alarm_summary:I = 0x7f0d003a

.field public static final slash:I = 0x7f0d0097

.field public static final snooze_duration_title:I = 0x7f0d0032

.field public static final stop_watch:I = 0x7f0d0006

.field public static final stopwatch_service_desc:I = 0x7f0d0085

.field public static final sw_lap_button:I = 0x7f0d005c

.field public static final sw_notification_lap_number:I = 0x7f0d0068

.field public static final sw_reset_button:I = 0x7f0d005d

.field public static final sw_resume_button:I = 0x7f0d0059

.field public static final sw_share_button:I = 0x7f0d005e

.field public static final sw_share_laps:I = 0x7f0d0067

.field public static final sw_share_main:I = 0x7f0d0066

.field public static final sw_start_button:I = 0x7f0d005a

.field public static final sw_stop_button:I = 0x7f0d005b

.field public static final swn_stopped:I = 0x7f0d0086

.field public static final time:I = 0x7f0d001f

.field public static final time_picker_00_label:I = 0x7f0d0080

.field public static final time_picker_30_label:I = 0x7f0d0081

.field public static final time_picker_ampm_label:I = 0x7f0d007f

.field public static final time_picker_cancel:I = 0x7f0d007c

.field public static final time_picker_set:I = 0x7f0d007d

.field public static final time_picker_time_seperator:I = 0x7f0d007e

.field public static final timer_add_timer:I = 0x7f0d0069

.field public static final timer_cancel:I = 0x7f0d006f

.field public static final timer_delete:I = 0x7f0d006b

.field public static final timer_delete_confirmation:I = 0x7f0d009d

.field public static final timer_notification_label:I = 0x7f0d0071

.field public static final timer_plus_one:I = 0x7f0d006c

.field public static final timer_reset:I = 0x7f0d006e

.field public static final timer_ring_service_desc:I = 0x7f0d004d

.field public static final timer_start:I = 0x7f0d006a

.field public static final timer_stop:I = 0x7f0d006d

.field public static final timer_stopped:I = 0x7f0d008b

.field public static final timer_times_up:I = 0x7f0d0070

.field public static final timers_in_use:I = 0x7f0d008e

.field public static final timers_stopped:I = 0x7f0d008c

.field public static final voice_ui_content_text:I = 0x7f0d0008

.field public static final voice_ui_title:I = 0x7f0d0007

.field public static final volume_button_dialog_title:I = 0x7f0d003f

.field public static final volume_button_setting_summary:I = 0x7f0d00a5

.field public static final volume_button_setting_title:I = 0x7f0d003e

.field public static final weather_fetch_failure:I = 0x7f0d004a

.field public static final widget_12_hours_format_h:I = 0x7f0d00a0

.field public static final widget_12_hours_format_m:I = 0x7f0d00a2

.field public static final widget_12_hours_format_no_ampm_m:I = 0x7f0d00a1

.field public static final widget_24_hours_format_h:I = 0x7f0d00a3

.field public static final widget_24_hours_format_m:I = 0x7f0d00a4

.field public static final world_day_of_week_label:I = 0x7f0d0098

.field public static final zero:I = 0x7f0d0065


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
