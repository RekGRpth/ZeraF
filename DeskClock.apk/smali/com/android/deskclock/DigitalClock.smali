.class public Lcom/android/deskclock/DigitalClock;
.super Landroid/widget/LinearLayout;
.source "DigitalClock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/deskclock/DigitalClock$FormatChangeObserver;,
        Lcom/android/deskclock/DigitalClock$AmPm;
    }
.end annotation


# static fields
.field private static final HOURS:Ljava/lang/String; = "h"

.field private static final HOURS_24:Ljava/lang/String; = "kk"

.field private static final MINUTES:Ljava/lang/String; = ":mm"

#.field private static sRobotoThin:Landroid/graphics/Typeface;


# instance fields
.field private mAmPm:Lcom/android/deskclock/DigitalClock$AmPm;

.field private mAttached:Z

.field private mCalendar:Ljava/util/Calendar;

.field private mFormatChangeObserver:Landroid/database/ContentObserver;

.field private final mHandler:Landroid/os/Handler;

.field private mHoursFormat:Ljava/lang/String;

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mLive:Z

.field private mTimeDisplayHours:Landroid/widget/TextView;

.field private mTimeDisplayMinutes:Landroid/widget/TextView;

.field private mTimeZoneId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/DigitalClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/deskclock/DigitalClock;->mLive:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/deskclock/DigitalClock;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/deskclock/DigitalClock$1;

    invoke-direct {v0, p0}, Lcom/android/deskclock/DigitalClock$1;-><init>(Lcom/android/deskclock/DigitalClock;)V

    iput-object v0, p0, Lcom/android/deskclock/DigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

#    sget-object v0, Lcom/android/deskclock/DigitalClock;->sRobotoThin:Landroid/graphics/Typeface;

#    if-nez v0, :cond_0

#    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

#    move-result-object v0

#    const-string v1, "fonts/Roboto-Thin.ttf"

#    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

#    move-result-object v0

#    sput-object v0, Lcom/android/deskclock/DigitalClock;->sRobotoThin:Landroid/graphics/Typeface;

#    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/deskclock/DigitalClock;)Z
    .locals 1
    .param p0    # Lcom/android/deskclock/DigitalClock;

    iget-boolean v0, p0, Lcom/android/deskclock/DigitalClock;->mLive:Z

    return v0
.end method

.method static synthetic access$102(Lcom/android/deskclock/DigitalClock;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0    # Lcom/android/deskclock/DigitalClock;
    .param p1    # Ljava/util/Calendar;

    iput-object p1, p0, Lcom/android/deskclock/DigitalClock;->mCalendar:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/deskclock/DigitalClock;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/DigitalClock;

    invoke-direct {p0}, Lcom/android/deskclock/DigitalClock;->updateTime()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/deskclock/DigitalClock;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/deskclock/DigitalClock;

    iget-object v0, p0, Lcom/android/deskclock/DigitalClock;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/deskclock/DigitalClock;)V
    .locals 0
    .param p0    # Lcom/android/deskclock/DigitalClock;

    invoke-direct {p0}, Lcom/android/deskclock/DigitalClock;->setDateFormat()V

    return-void
.end method

.method private setDateFormat()V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/deskclock/Alarms;->get24HourMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "kk"

    :goto_0
    iput-object v0, p0, Lcom/android/deskclock/DigitalClock;->mHoursFormat:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/deskclock/DigitalClock;->mAmPm:Lcom/android/deskclock/DigitalClock$AmPm;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/deskclock/Alarms;->get24HourMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/android/deskclock/DigitalClock$AmPm;->setShowAmPm(Z)V

    return-void

    :cond_0
    const-string v0, "h"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updateTime()V
    .locals 6

    iget-boolean v3, p0, Lcom/android/deskclock/DigitalClock;->mLive:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mCalendar:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :cond_0
    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mTimeZoneId:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mCalendar:Ljava/util/Calendar;

    iget-object v4, p0, Lcom/android/deskclock/DigitalClock;->mTimeZoneId:Ljava/lang/String;

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mHoursFormat:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/deskclock/DigitalClock;->mCalendar:Ljava/util/Calendar;

    invoke-static {v3, v4}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mTimeDisplayHours:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v3, ":mm"

    iget-object v4, p0, Lcom/android/deskclock/DigitalClock;->mCalendar:Ljava/util/Calendar;

    invoke-static {v3, v4}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mTimeDisplayMinutes:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mCalendar:Ljava/util/Calendar;

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-nez v3, :cond_3

    const/4 v1, 0x1

    :goto_0
    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mAmPm:Lcom/android/deskclock/DigitalClock$AmPm;

    invoke-virtual {v3, v1}, Lcom/android/deskclock/DigitalClock$AmPm;->setIsMorning(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/android/deskclock/Alarms;->get24HourMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mAmPm:Lcom/android/deskclock/DigitalClock$AmPm;

    invoke-virtual {v3}, Lcom/android/deskclock/DigitalClock$AmPm;->getAmPmText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {p0, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    iget-boolean v1, p0, Lcom/android/deskclock/DigitalClock;->mAttached:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v4, p0, Lcom/android/deskclock/DigitalClock;->mAttached:Z

    iget-boolean v1, p0, Lcom/android/deskclock/DigitalClock;->mLive:Z

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/deskclock/DigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    new-instance v1, Lcom/android/deskclock/DigitalClock$FormatChangeObserver;

    invoke-direct {v1, p0}, Lcom/android/deskclock/DigitalClock$FormatChangeObserver;-><init>(Lcom/android/deskclock/DigitalClock;)V

    iput-object v1, p0, Lcom/android/deskclock/DigitalClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/deskclock/DigitalClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lcom/android/deskclock/DigitalClock;->updateTime()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    iget-boolean v0, p0, Lcom/android/deskclock/DigitalClock;->mAttached:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/deskclock/DigitalClock;->mAttached:Z

    iget-boolean v0, p0, Lcom/android/deskclock/DigitalClock;->mLive:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/deskclock/DigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/deskclock/DigitalClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0e0009

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/deskclock/DigitalClock;->mTimeDisplayHours:Landroid/widget/TextView;

    const v0, 0x7f0e000a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/deskclock/DigitalClock;->mTimeDisplayMinutes:Landroid/widget/TextView;

#    iget-object v0, p0, Lcom/android/deskclock/DigitalClock;->mTimeDisplayMinutes:Landroid/widget/TextView;

#    sget-object v1, Lcom/android/deskclock/DigitalClock;->sRobotoThin:Landroid/graphics/Typeface;

#    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    new-instance v0, Lcom/android/deskclock/DigitalClock$AmPm;

    invoke-direct {v0, p0}, Lcom/android/deskclock/DigitalClock$AmPm;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/deskclock/DigitalClock;->mAmPm:Lcom/android/deskclock/DigitalClock$AmPm;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/DigitalClock;->mCalendar:Ljava/util/Calendar;

    invoke-direct {p0}, Lcom/android/deskclock/DigitalClock;->setDateFormat()V

    return-void
.end method

.method setLive(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/deskclock/DigitalClock;->mLive:Z

    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/deskclock/DigitalClock;->mTimeZoneId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/deskclock/DigitalClock;->updateTime()V

    return-void
.end method

.method public updateTime(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    iput-object v0, p0, Lcom/android/deskclock/DigitalClock;->mCalendar:Ljava/util/Calendar;

    invoke-direct {p0}, Lcom/android/deskclock/DigitalClock;->updateTime()V

    return-void
.end method

.method updateTime(Ljava/util/Calendar;)V
    .locals 0
    .param p1    # Ljava/util/Calendar;

    iput-object p1, p0, Lcom/android/deskclock/DigitalClock;->mCalendar:Ljava/util/Calendar;

    invoke-direct {p0}, Lcom/android/deskclock/DigitalClock;->updateTime()V

    return-void
.end method
