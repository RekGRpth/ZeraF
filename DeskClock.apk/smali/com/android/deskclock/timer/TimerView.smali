.class public Lcom/android/deskclock/timer/TimerView;
.super Landroid/widget/LinearLayout;
.source "TimerView.java"


# static fields
#.field private static sAndroidClockMonoThin:Landroid/graphics/Typeface;


# instance fields
.field private final mGrayColor:I

.field private mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

.field private mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

.field private mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

.field private mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

.field private mOriginalHoursTypeface:Landroid/graphics/Typeface;

.field private mSeconds:Landroid/widget/TextView;

.field private final mWhiteColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

#    sget-object v0, Lcom/android/deskclock/timer/TimerView;->sAndroidClockMonoThin:Landroid/graphics/Typeface;

#    if-nez v0, :cond_0

#    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

#    move-result-object v0

#    const-string v1, "fonts/AndroidClockMono-Thin.ttf"

#    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

#    move-result-object v0

#    sput-object v0, Lcom/android/deskclock/timer/TimerView;->sAndroidClockMonoThin:Landroid/graphics/Typeface;

#    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/deskclock/timer/TimerView;->mWhiteColor:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/deskclock/timer/TimerView;->mGrayColor:I

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0e0076

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/ZeroTopPaddingTextView;

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const v0, 0x7f0e0079

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/ZeroTopPaddingTextView;

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const v0, 0x7f0e0077

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/ZeroTopPaddingTextView;

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const v0, 0x7f0e007a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/ZeroTopPaddingTextView;

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const v0, 0x7f0e0083

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mSeconds:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mOriginalHoursTypeface:Landroid/graphics/Typeface;

    :cond_0
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mSeconds:Landroid/widget/TextView;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    if-eqz v0, :cond_1

#    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

#    sget-object v1, Lcom/android/deskclock/timer/TimerView;->sAndroidClockMonoThin:Landroid/graphics/Typeface;

#    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    invoke-virtual {v0}, Lcom/android/deskclock/ZeroTopPaddingTextView;->updatePadding()V

    :cond_1
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    if-eqz v0, :cond_2

#    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

#    sget-object v1, Lcom/android/deskclock/timer/TimerView;->sAndroidClockMonoThin:Landroid/graphics/Typeface;

#    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    invoke-virtual {v0}, Lcom/android/deskclock/ZeroTopPaddingTextView;->updatePadding()V

    :cond_2
    :goto_0
    return-void

    :cond_3
#    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mSeconds:Landroid/widget/TextView;

#    sget-object v1, Lcom/android/deskclock/timer/TimerView;->sAndroidClockMonoThin:Landroid/graphics/Typeface;

#    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method

.method public setTime(IIIII)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    if-eqz v0, :cond_0

    const/4 v0, -0x2

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    if-eqz v0, :cond_1

    if-ne p2, v6, :cond_7

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

#    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

#    sget-object v1, Lcom/android/deskclock/timer/TimerView;->sAndroidClockMonoThin:Landroid/graphics/Typeface;

#    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget v1, p0, Lcom/android/deskclock/timer/TimerView;->mGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    invoke-virtual {v0}, Lcom/android/deskclock/ZeroTopPaddingTextView;->updatePadding()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    if-eqz v0, :cond_2

    if-ne p3, v6, :cond_8

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget v1, p0, Lcom/android/deskclock/timer/TimerView;->mGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    if-eqz v0, :cond_3

    if-ne p4, v6, :cond_9

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget v1, p0, Lcom/android/deskclock/timer/TimerView;->mGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mSeconds:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mSeconds:Landroid/widget/TextView;

    const-string v1, "%02d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    return-void

    :cond_5
    if-ne p1, v6, :cond_6

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

#    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

#    sget-object v1, Lcom/android/deskclock/timer/TimerView;->sAndroidClockMonoThin:Landroid/graphics/Typeface;

#    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget v1, p0, Lcom/android/deskclock/timer/TimerView;->mGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    invoke-virtual {v0}, Lcom/android/deskclock/ZeroTopPaddingTextView;->updatePadding()V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const-string v1, "%d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerView;->mOriginalHoursTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget v1, p0, Lcom/android/deskclock/timer/TimerView;->mWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    invoke-virtual {v0}, Lcom/android/deskclock/ZeroTopPaddingTextView;->updatePadding()V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const-string v1, "%d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerView;->mOriginalHoursTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget v1, p0, Lcom/android/deskclock/timer/TimerView;->mWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mHoursOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    invoke-virtual {v0}, Lcom/android/deskclock/ZeroTopPaddingTextView;->updatePadding()V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget v1, p0, Lcom/android/deskclock/timer/TimerView;->mWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesTens:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const-string v1, "%d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_9
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    const-string v1, "%d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerView;->mMinutesOnes:Lcom/android/deskclock/ZeroTopPaddingTextView;

    iget v1, p0, Lcom/android/deskclock/timer/TimerView;->mWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3
.end method
