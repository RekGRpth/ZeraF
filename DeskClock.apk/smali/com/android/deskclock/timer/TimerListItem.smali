.class public Lcom/android/deskclock/timer/TimerListItem;
.super Landroid/widget/LinearLayout;
.source "TimerListItem.java"


# instance fields
.field mCircleView:Lcom/android/deskclock/CircleTimerView;

.field mTimerLength:J

.field mTimerText:Lcom/android/deskclock/timer/CountingTimerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/deskclock/timer/TimerListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04002b

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method


# virtual methods
.method public done()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Lcom/android/deskclock/CircleTimerView;->stopIntervalAnimation()V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/deskclock/timer/CountingTimerView;->redTimeStr(ZZ)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0e0075

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/timer/CountingTimerView;

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    const v0, 0x7f0e0091

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/CircleTimerView;

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/deskclock/CircleTimerView;->setTimerMode(Z)V

    return-void
.end method

.method public pause()V
    .locals 3

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Lcom/android/deskclock/CircleTimerView;->pauseIntervalAnimation()V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/deskclock/timer/CountingTimerView;->redTimeStr(ZZ)V

    return-void
.end method

.method public set(JJZ)V
    .locals 3
    .param p1    # J
    .param p3    # J
    .param p5    # Z

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    if-nez v0, :cond_0

    const v0, 0x7f0e0091

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/CircleTimerView;

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/deskclock/CircleTimerView;->setTimerMode(Z)V

    :cond_0
    iput-wide p1, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerLength:J

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    iget-wide v1, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerLength:J

    invoke-virtual {v0, v1, v2}, Lcom/android/deskclock/CircleTimerView;->setIntervalTime(J)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    sub-long v1, p1, p3

    invoke-virtual {v0, v1, v2, p5}, Lcom/android/deskclock/CircleTimerView;->setPassedTime(JZ)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setAnimatedHeight(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public setCircleBlink(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLength(J)V
    .locals 3
    .param p1    # J

    iput-wide p1, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerLength:J

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    iget-wide v1, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerLength:J

    invoke-virtual {v0, v1, v2}, Lcom/android/deskclock/CircleTimerView;->setIntervalTime(J)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setTextBlink(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/deskclock/timer/CountingTimerView;->showTime(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTime(JZ)V
    .locals 2
    .param p1    # J
    .param p3    # Z

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    if-nez v0, :cond_0

    const v0, 0x7f0e0075

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/deskclock/timer/CountingTimerView;

    iput-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    :cond_0
    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/android/deskclock/timer/CountingTimerView;->setTime(JZZ)V

    return-void
.end method

.method public start()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Lcom/android/deskclock/CircleTimerView;->startIntervalAnimation()V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-virtual {v0, v1, v2}, Lcom/android/deskclock/timer/CountingTimerView;->redTimeStr(ZZ)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-virtual {v0, v2}, Lcom/android/deskclock/timer/CountingTimerView;->showTime(Z)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public stop()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Lcom/android/deskclock/CircleTimerView;->stopIntervalAnimation()V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-virtual {v0, v1, v2}, Lcom/android/deskclock/timer/CountingTimerView;->redTimeStr(ZZ)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-virtual {v0, v2}, Lcom/android/deskclock/timer/CountingTimerView;->showTime(Z)V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public timesUp()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mCircleView:Lcom/android/deskclock/CircleTimerView;

    invoke-virtual {v0}, Lcom/android/deskclock/CircleTimerView;->abortIntervalAnimation()V

    iget-object v0, p0, Lcom/android/deskclock/timer/TimerListItem;->mTimerText:Lcom/android/deskclock/timer/CountingTimerView;

    invoke-virtual {v0, v1, v1}, Lcom/android/deskclock/timer/CountingTimerView;->redTimeStr(ZZ)V

    return-void
.end method
