.class public Lcom/android/musicfx/seekbar/SeekBar;
.super Lcom/android/musicfx/seekbar/AbsSeekBar;
.source "SeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;
    }
.end annotation


# instance fields
.field private mOnSeekBarChangeListener:Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/musicfx/seekbar/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x101007b

    invoke-direct {p0, p1, p2, v0}, Lcom/android/musicfx/seekbar/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/musicfx/seekbar/AbsSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method onProgressRefresh(FZ)V
    .locals 2
    .param p1    # F
    .param p2    # Z

    invoke-super {p0, p1, p2}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onProgressRefresh(FZ)V

    iget-object v0, p0, Lcom/android/musicfx/seekbar/SeekBar;->mOnSeekBarChangeListener:Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicfx/seekbar/SeekBar;->mOnSeekBarChangeListener:Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getProgress()I

    move-result v1

    invoke-interface {v0, p0, v1, p2}, Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Lcom/android/musicfx/seekbar/SeekBar;IZ)V

    :cond_0
    return-void
.end method

.method onStartTrackingTouch()V
    .locals 1

    invoke-super {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onStartTrackingTouch()V

    iget-object v0, p0, Lcom/android/musicfx/seekbar/SeekBar;->mOnSeekBarChangeListener:Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicfx/seekbar/SeekBar;->mOnSeekBarChangeListener:Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;

    invoke-interface {v0, p0}, Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;->onStartTrackingTouch(Lcom/android/musicfx/seekbar/SeekBar;)V

    :cond_0
    return-void
.end method

.method onStopTrackingTouch()V
    .locals 1

    invoke-super {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onStopTrackingTouch()V

    iget-object v0, p0, Lcom/android/musicfx/seekbar/SeekBar;->mOnSeekBarChangeListener:Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicfx/seekbar/SeekBar;->mOnSeekBarChangeListener:Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;

    invoke-interface {v0, p0}, Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;->onStopTrackingTouch(Lcom/android/musicfx/seekbar/SeekBar;)V

    :cond_0
    return-void
.end method

.method public setOnSeekBarChangeListener(Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;)V
    .locals 0
    .param p1    # Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;

    iput-object p1, p0, Lcom/android/musicfx/seekbar/SeekBar;->mOnSeekBarChangeListener:Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;

    return-void
.end method
