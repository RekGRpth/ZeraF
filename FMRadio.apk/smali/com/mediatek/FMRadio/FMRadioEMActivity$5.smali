.class Lcom/mediatek/FMRadio/FMRadioEMActivity$5;
.super Ljava/lang/Thread;
.source "FMRadioEMActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/FMRadio/FMRadioEMActivity;->readTickEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/FMRadio/FMRadioEMActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    const-string v5, "FmRx/EM"

    const-string v6, ">>> tick envent Thread run()"

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$3200(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "FmRx/EM"

    const-string v6, "<<< tick envent Thread run()"

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$1300(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v0, Landroid/os/Bundle;

    const/4 v5, 0x3

    invoke-direct {v0, v5}, Landroid/os/Bundle;-><init>(I)V

    const-string v5, "RDS_BLER_STRING"

    const-string v6, "%d%%"

    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-virtual {v8}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->readRdsBler()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "RDS_RSSI_STRING"

    const-string v6, "%d"

    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-virtual {v8}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->readRssi()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "RDS_STEREMONO_STRING"

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-virtual {v5}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->getStereoMono()Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "Stereo"

    :goto_1
    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "N/A"

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$3300(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Landroid/widget/RadioButton;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    iget-object v6, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-virtual {v6}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->readCapArray()I

    move-result v6

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$3400(Lcom/mediatek/FMRadio/FMRadioEMActivity;I)Ljava/lang/String;

    move-result-object v1

    :cond_1
    const-string v5, "RDS_CAPARRAY_STRING"

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$600(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/mediatek/FMRadio/FMRadioEMActivity$5;->this$0:Lcom/mediatek/FMRadio/FMRadioEMActivity;

    invoke-static {v5}, Lcom/mediatek/FMRadio/FMRadioEMActivity;->access$600(Lcom/mediatek/FMRadio/FMRadioEMActivity;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    const/16 v3, 0x3e8

    const-wide/16 v5, 0x3e8

    :try_start_0
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const-string v5, "FmRx/EM"

    const-string v6, "get tick information"

    invoke-static {v5, v6}, Lcom/mediatek/FMRadio/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    const-string v5, "Mono"

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2
.end method
