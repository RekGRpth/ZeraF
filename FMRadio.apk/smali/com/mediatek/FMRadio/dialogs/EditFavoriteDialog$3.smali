.class Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$3;
.super Ljava/lang/Object;
.source "EditFavoriteDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;


# direct methods
.method constructor <init>(Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$3;->this$0:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 11
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v10, 0x0

    iget-object v8, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$3;->this$0:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;

    invoke-virtual {v8}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v8, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$3;->this$0:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;

    invoke-static {v8}, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;->access$100(Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :goto_1
    const/16 v2, 0x64

    const/high16 v8, 0x42c80000

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v7

    iget-object v8, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$3;->this$0:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;

    invoke-virtual {v8}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v8

    check-cast v8, Landroid/app/AlertDialog;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    const/4 v0, 0x5

    rem-int/lit8 v8, v7, 0x5

    if-nez v8, :cond_2

    if-eqz v6, :cond_0

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :cond_2
    if-eqz v6, :cond_3

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_3
    iget-object v8, p0, Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog$3;->this$0:Lcom/mediatek/FMRadio/dialogs/EditFavoriteDialog;

    invoke-virtual {v8}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v8, 0x7f040025

    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
