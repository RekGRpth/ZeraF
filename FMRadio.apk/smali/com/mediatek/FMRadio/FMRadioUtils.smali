.class public Lcom/mediatek/FMRadio/FMRadioUtils;
.super Ljava/lang/Object;
.source "FMRadioUtils.java"


# static fields
.field public static final CONVERT_RATE:I = 0xa

.field public static final DEFAULT_STATION:I = 0x3e8

.field public static final DEFAULT_STATION_FLOAT:F

.field public static final HIGHEST_STATION:I = 0x4b0

.field public static final LOWEST_STATION:I = 0x258

.field public static final STEP:I = 0x1

.field private static final TAG:Ljava/lang/String; = "FmRx/Utils"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x3e8

    invoke-static {v0}, Lcom/mediatek/FMRadio/FMRadioUtils;->computeFrequency(I)F

    move-result v0

    sput v0, Lcom/mediatek/FMRadio/FMRadioUtils;->DEFAULT_STATION_FLOAT:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static computeDecreaseStation(I)I
    .locals 2
    .param p0    # I

    add-int/lit8 v0, p0, -0x1

    const/16 v1, 0x258

    if-ge v0, v1, :cond_0

    const/16 v0, 0x4b0

    :cond_0
    return v0
.end method

.method public static computeFrequency(I)F
    .locals 2
    .param p0    # I

    int-to-float v0, p0

    const/high16 v1, 0x41200000

    div-float/2addr v0, v1

    return v0
.end method

.method public static computeIncreaseStation(I)I
    .locals 2
    .param p0    # I

    add-int/lit8 v0, p0, 0x1

    const/16 v1, 0x4b0

    if-le v0, v1, :cond_0

    const/16 v0, 0x258

    :cond_0
    return v0
.end method

.method public static computeStation(F)I
    .locals 1
    .param p0    # F

    const/high16 v0, 0x41200000

    mul-float/2addr v0, p0

    float-to-int v0, v0

    return v0
.end method

.method public static formatStation(I)Ljava/lang/String;
    .locals 7
    .param p0    # I

    int-to-float v2, p0

    const/high16 v3, 0x41200000

    div-float v0, v2, v3

    const/4 v1, 0x0

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static isValidStation(I)Z
    .locals 5
    .param p0    # I

    const/4 v1, 0x0

    const/4 v0, 0x5

    const/16 v2, 0x258

    if-lt p0, v2, :cond_0

    const/16 v2, 0x4b0

    if-gt p0, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "FmRx/Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isValidStation: freq = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", valid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/FMRadio/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
