.class Lcom/android/music/TrackBrowserActivity$4;
.super Ljava/lang/Object;
.source "TrackBrowserActivity.java"

# interfaces
.implements Lcom/android/music/TouchInterceptor$DropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/TrackBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/music/TrackBrowserActivity;


# direct methods
.method constructor <init>(Lcom/android/music/TrackBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drop(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v2}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v2

    instance-of v2, v2, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v2}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    invoke-virtual {v0, p1, p2}, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;->moveItem(II)V

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-virtual {v2}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    check-cast v2, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v2}, Lcom/android/music/TrackBrowserActivity;->access$800(Lcom/android/music/TrackBrowserActivity;)Landroid/widget/ListView;

    move-result-object v2

    check-cast v2, Lcom/android/music/TouchInterceptor;

    invoke-virtual {v2}, Lcom/android/music/TouchInterceptor;->resetPredrawStatus()V

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-virtual {v2}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/AbsListView;->invalidateViews()V

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/music/TrackBrowserActivity;->access$902(Lcom/android/music/TrackBrowserActivity;Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v3}, Lcom/android/music/TrackBrowserActivity;->access$1000(Lcom/android/music/TrackBrowserActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4, p1, p2}, Landroid/provider/MediaStore$Audio$Playlists$Members;->moveItem(Landroid/content/ContentResolver;JII)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$4;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v2}, Lcom/android/music/TrackBrowserActivity;->access$800(Lcom/android/music/TrackBrowserActivity;)Landroid/widget/ListView;

    move-result-object v2

    check-cast v2, Lcom/android/music/TouchInterceptor;

    invoke-virtual {v2}, Lcom/android/music/TouchInterceptor;->resetPredrawStatus()V

    :cond_1
    const-string v2, "TrackBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "drop: from = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", to = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isSuccesss = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
