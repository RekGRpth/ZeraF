.class public Lcom/mediatek/engineermode/wifi/WiFiTestActivity;
.super Landroid/app/Activity;
.source "WiFiTestActivity.java"


# static fields
.field static final ATPARAM_INDEX_ANTENNA:I = 0x5

.field static final ATPARAM_INDEX_BANDWIDTH:I = 0xf

.field static final ATPARAM_INDEX_COMMAND:I = 0x1

.field static final ATPARAM_INDEX_CWMODE:I = 0x41

.field static final ATPARAM_INDEX_GI:I = 0x10

.field static final ATPARAM_INDEX_PACKCONTENT:I = 0xc

.field static final ATPARAM_INDEX_PACKCOUNT:I = 0x7

.field static final ATPARAM_INDEX_PACKINTERVAL:I = 0x8

.field static final ATPARAM_INDEX_PACKLENGTH:I = 0x6

.field static final ATPARAM_INDEX_POWER:I = 0x2

.field static final ATPARAM_INDEX_PREAMBLE:I = 0x4

.field static final ATPARAM_INDEX_QOS_QUEUE:I = 0xe

.field static final ATPARAM_INDEX_RATE:I = 0x3

.field static final ATPARAM_INDEX_RETRY_LIMIT:I = 0xd

.field static final ATPARAM_INDEX_TEMP_COMPENSATION:I = 0x9

.field static final ATPARAM_INDEX_TRANSMITCOUNT:I = 0x20

.field static final ATPARAM_INDEX_TXOP_LIMIT:I = 0xa

.field static final CHANNEL_0:I = 0x0

.field static final CHANNEL_1:I = 0x1

.field static final CHANNEL_11:I = 0xb

.field static final CHANNEL_12:I = 0xc

.field static final CHANNEL_13:I = 0xd

.field static final CHANNEL_14:I = 0xe

.field static final DIALOG_WIFI_ERROR:I = 0x3

.field static final DIALOG_WIFI_FAIL:I = 0x2

.field private static final TAG:Ljava/lang/String; = "EM/WiFiTest"


# instance fields
.field private mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/wifi/WiFiTestActivity;)Lcom/mediatek/engineermode/wifi/WiFiStateManager;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/wifi/WiFiTestActivity;

    iget-object v0, p0, Lcom/mediatek/engineermode/wifi/WiFiTestActivity;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    return-object v0
.end method


# virtual methods
.method protected checkWiFiChipState()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTestActivity;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/wifi/WiFiStateManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTestActivity;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/wifi/WiFiTestActivity;->mWiFiStateManager:Lcom/mediatek/engineermode/wifi/WiFiStateManager;

    invoke-virtual {v1, p0}, Lcom/mediatek/engineermode/wifi/WiFiStateManager;->checkState(Landroid/content/Context;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :sswitch_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_0
        -0x3 -> :sswitch_2
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_1
        0x5920 -> :sswitch_0
        0x6620 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f0801d3

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v2, "EM/WiFiTest"

    const-string v3, "error dialog ID"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0801cc

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0801cd

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/mediatek/engineermode/wifi/WiFiTestActivity$1;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/wifi/WiFiTestActivity$1;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTestActivity;)V

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0801ce

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0801cf

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/mediatek/engineermode/wifi/WiFiTestActivity$2;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/wifi/WiFiTestActivity$2;-><init>(Lcom/mediatek/engineermode/wifi/WiFiTestActivity;)V

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    invoke-virtual {p0}, Lcom/mediatek/engineermode/wifi/WiFiTestActivity;->checkWiFiChipState()V

    return-void
.end method

.method protected updateWifiChannel(Lcom/mediatek/engineermode/wifi/ChannelInfo;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;)V
    .locals 5
    .param p1    # Lcom/mediatek/engineermode/wifi/ChannelInfo;
    .param p3    # Landroid/widget/Spinner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/engineermode/wifi/ChannelInfo;",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/Spinner;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x0

    const-string v1, "EM/WiFiTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enter updateWifiChannel: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->getChannelIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v1, Lcom/mediatek/engineermode/wifi/EMWifi;->sIsInitialed:Z

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->getChannelIndex()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p3, v4}, Landroid/widget/AbsSpinner;->setSelection(I)V

    :cond_0
    invoke-virtual {p2, v4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p1, Lcom/mediatek/engineermode/wifi/ChannelInfo;->mChannelSelect:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/mediatek/engineermode/wifi/ChannelInfo;->getChannelFreq()I

    move-result v0

    int-to-long v1, v0

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/wifi/EMWifi;->setChannel(J)I

    const-string v1, "EM/WiFiTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The channel freq ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v1, "EM/WiFiTest"

    const-string v2, "Wifi is not initialized"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method
