.class Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;
.super Landroid/os/Handler;
.source "ConfigFD.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/fastdormancy/ConfigFD;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const/high16 v7, 0x800000

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-string v3, "EM_FD"

    const-string v5, "Receive msg from modem"

    invoke-static {v3, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v3, "EM_FD"

    const-string v5, "Receive EVENT_FD_QUERY_SIM1:"

    invoke-static {v3, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v3, :cond_6

    iget-object v5, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/String;

    check-cast v3, [Ljava/lang/String;

    invoke-static {v5, v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$102(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    iget-object v5, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    iget-object v6, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v6}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$500(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$102(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-lez v3, :cond_5

    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ge v1, v3, :cond_1

    const-string v3, "EM_FD"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mReturnData["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v6}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v4

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ne v3, v2, :cond_2

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$202(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;I)I

    :goto_2
    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$200(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)I

    move-result v3

    and-int/2addr v3, v7

    if-ne v3, v7, :cond_3

    :goto_3
    const-string v3, "EM_FD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "value = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v5}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$200(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_4

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$000(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)Landroid/widget/RadioGroup;

    move-result-object v3

    const v4, 0x7f0b0162

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->check(I)V

    const-string v3, "EM_FD"

    const-string v4, "check off"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    iget-object v5, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v5}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$100(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v3, v5}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$202(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;I)I

    goto :goto_2

    :cond_3
    move v2, v4

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-static {v3}, Lcom/mediatek/engineermode/fastdormancy/ConfigFD;->access$000(Lcom/mediatek/engineermode/fastdormancy/ConfigFD;)Landroid/widget/RadioGroup;

    move-result-object v3

    const v4, 0x7f0b0161

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->check(I)V

    const-string v3, "EM_FD"

    const-string v4, "check on"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v3, "EM_FD"

    const-string v4, "Received data is null"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const-string v3, "EM_FD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Receive EVENT_FD_QUERY: exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    const-string v3, "EM_FD"

    const-string v5, "Receive EVENT_FD_SET:"

    invoke-static {v3, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    const-string v5, "success!"

    invoke-static {v3, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/mediatek/engineermode/fastdormancy/ConfigFD$1;->this$0:Lcom/mediatek/engineermode/fastdormancy/ConfigFD;

    invoke-virtual {v3, v4}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
