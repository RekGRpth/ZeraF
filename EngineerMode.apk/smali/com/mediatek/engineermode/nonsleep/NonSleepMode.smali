.class public Lcom/mediatek/engineermode/nonsleep/NonSleepMode;
.super Landroid/app/Activity;
.source "NonSleepMode.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "EM/NonSleep"


# instance fields
.field private mSetButton:Landroid/widget/Button;

.field private mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    return-void
.end method

.method private static isServiceRunning(Landroid/content/Context;Ljava/lang/Class;)Z
    .locals 7
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Service;",
            ">;)Z"
        }
    .end annotation

    const/4 v3, 0x0

    const-string v6, "activity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/16 v4, 0x64

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v4, :cond_0

    add-int/lit8 v4, v4, 0x32

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v5

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_1

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v6, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v3, 0x1

    :cond_1
    return v3

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const v2, 0x7f08003c

    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    const v1, 0x7f08003b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    const-class v1, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;

    invoke-virtual {v0, v1}, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;->acquire(Ljava/lang/Class;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;->release()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030066

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b0385

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-class v0, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-static {p0, v0}, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->isServiceRunning(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const-class v0, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-static {p0, v0}, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->isServiceRunning(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->stopService(Landroid/content/Intent;)Z

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    move-object v0, p2

    check-cast v0, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService$LocalBinder;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService$LocalBinder;->getService()Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    iget-object v1, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mWakeLockServ:Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    const v2, 0x7f08003b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    const v2, 0x7f08003c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "EM/NonSleep"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onStart()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v1, p0, Lcom/mediatek/engineermode/nonsleep/NonSleepMode;->mSetButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/nonsleep/EMWakeLockService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p0, v1}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-virtual {p0, p0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
