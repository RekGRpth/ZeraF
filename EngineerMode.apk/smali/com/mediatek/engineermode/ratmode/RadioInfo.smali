.class public Lcom/mediatek/engineermode/ratmode/RadioInfo;
.super Landroid/app/Activity;
.source "RadioInfo.java"


# static fields
.field private static final EVENT_QUERY_PREFERRED_TYPE_DONE:I = 0x3e8

.field private static final EVENT_SET_PREFERRED_TYPE_DONE:I = 0x3e9

.field private static final GSM_ONLY_INDEX:I = 0x3

.field private static final GSM_ONLY_VALUE:I = 0x1

.field private static final GSM_WCDMA_AUTO_INDEX:I = 0x0

.field private static final GSM_WCDMA_AUTO_VALUE:I = 0x3

.field private static final MODEM_MASK_TDSCDMA:I = 0x8

.field private static final MODEM_MASK_WCDMA:I = 0x4

.field private static final NOT_SPECIFIED_INDEX:I = 0x4

.field private static final NOT_SPECIFIED_VALUE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "EM/RATMode_RadioInfo"

.field private static final WCDMA_ONLY_INDEX:I = 0x2

.field private static final WCDMA_ONLY_VALUE:I = 0x2

.field private static final WCDMA_PREFERRED_INDEX:I = 0x1

.field private static final WCDMA_PREFERRED_VALUE:I


# instance fields
.field private mFilter:Landroid/content/IntentFilter;

.field private mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

.field private final mHandler:Landroid/os/Handler;

.field private mIsTddType:Z

.field private mModeType:I

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field mPreferredNetworkHandler:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mPreferredNetworkType:Landroid/widget/Spinner;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private final mSimId:I

.field private mStatusEfRat:I

.field private mTelephonyExt:Lcom/mediatek/common/telephony/ITelephonyExt;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkType:Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPhone:Lcom/android/internal/telephony/Phone;

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mFilter:Landroid/content/IntentFilter;

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mReceiver:Landroid/content/BroadcastReceiver;

    const/16 v0, 0x200

    iput v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mStatusEfRat:I

    iput v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mSimId:I

    iput v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mModeType:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mIsTddType:Z

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mTelephonyExt:Lcom/mediatek/common/telephony/ITelephonyExt;

    new-instance v0, Lcom/mediatek/engineermode/ratmode/RadioInfo$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/ratmode/RadioInfo$1;-><init>(Lcom/mediatek/engineermode/ratmode/RadioInfo;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/engineermode/ratmode/RadioInfo$2;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/ratmode/RadioInfo$2;-><init>(Lcom/mediatek/engineermode/ratmode/RadioInfo;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkHandler:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/widget/Spinner;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/ratmode/RadioInfo;

    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkType:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Lcom/android/internal/telephony/gemini/GeminiPhone;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/ratmode/RadioInfo;

    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/ratmode/RadioInfo;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/ratmode/RadioInfo;

    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/ratmode/RadioInfo;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/ratmode/RadioInfo;

    iget v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mModeType:I

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/ratmode/RadioInfo;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/ratmode/RadioInfo;

    iget v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mStatusEfRat:I

    return v0
.end method

.method static synthetic access$402(Lcom/mediatek/engineermode/ratmode/RadioInfo;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/ratmode/RadioInfo;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mStatusEfRat:I

    return p1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const v5, 0x1090009

    const v3, 0x1090008

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030070

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-static {}, Lcom/mediatek/engineermode/bandselect/BandSelect;->getModemType()Z

    move-result v1

    iput-boolean v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mIsTddType:Z

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const v1, 0x7f0b03b9

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkType:Landroid/widget/Spinner;

    iget-boolean v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mIsTddType:Z

    if-eqz v1, :cond_1

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkType:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkType:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkHandler:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getPreferredNetworkTypeGemini(Landroid/os/Message;I)V

    const-class v1, Lcom/mediatek/common/telephony/ITelephonyExt;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/mediatek/common/MediatekClassFactory;->createInstance(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/common/telephony/ITelephonyExt;

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mTelephonyExt:Lcom/mediatek/common/telephony/ITelephonyExt;

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mTelephonyExt:Lcom/mediatek/common/telephony/ITelephonyExt;

    invoke-interface {v1}, Lcom/mediatek/common/telephony/ITelephonyExt;->isRatMenuControlledBySIM()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.ACTION_EF_RAT_CONTENT_NOTIFY"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mFilter:Landroid/content/IntentFilter;

    new-instance v1, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/ratmode/RadioInfo$3;-><init>(Lcom/mediatek/engineermode/ratmode/RadioInfo;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mReceiver:Landroid/content/BroadcastReceiver;

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkType:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "EM/RATMode_RadioInfo"

    const-string v1, "Enter onResume"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->get3GCapabilitySIM()I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mModeType:I

    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mTelephonyExt:Lcom/mediatek/common/telephony/ITelephonyExt;

    invoke-interface {v0}, Lcom/mediatek/common/telephony/ITelephonyExt;->isRatMenuControlledBySIM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/engineermode/ratmode/RadioInfo;->updateUi()V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mTelephonyExt:Lcom/mediatek/common/telephony/ITelephonyExt;

    invoke-interface {v0}, Lcom/mediatek/common/telephony/ITelephonyExt;->isRatMenuControlledBySIM()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->get3GCapabilitySIM()I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mModeType:I

    const/4 v0, -0x1

    iget v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mModeType:I

    if-ne v0, v1, :cond_1

    const-string v0, "EM/RATMode_RadioInfo"

    const-string v1, "3G off!"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget v1, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mModeType:I

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getEfRatBalancing(I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mStatusEfRat:I

    const-string v0, "EM/RATMode_RadioInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gemini support, statusEfRat: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mStatusEfRat:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " modetype: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mModeType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mTelephonyExt:Lcom/mediatek/common/telephony/ITelephonyExt;

    invoke-interface {v0}, Lcom/mediatek/common/telephony/ITelephonyExt;->isRatMenuControlledBySIM()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method protected updateUi()V
    .locals 6

    const/16 v5, 0x100

    const/4 v1, 0x0

    iget v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mStatusEfRat:I

    if-ne v5, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "EM/RATMode_RadioInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "update UI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkType:Landroid/widget/Spinner;

    if-eqz v0, :cond_2

    move v2, v1

    :goto_1
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mStatusEfRat:I

    if-ne v5, v2, :cond_3

    const v2, 0x7f080459

    :goto_2
    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    if-eqz v0, :cond_0

    const/4 v2, -0x1

    iget v3, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mModeType:I

    if-ne v2, v3, :cond_0

    const v2, 0x7f080456

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/mediatek/engineermode/ratmode/RadioInfo;->mPreferredNetworkType:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    goto :goto_1

    :cond_3
    const v2, 0x7f08045a

    goto :goto_2
.end method

.method public writePreferred(I)V
    .locals 4
    .param p1    # I

    const-string v2, "RATMode"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    const-string v2, "ModeType"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method
