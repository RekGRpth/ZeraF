.class public Lcom/mediatek/engineermode/ChipSupport;
.super Ljava/lang/Object;
.source "ChipSupport.java"


# static fields
.field public static final HAVE_MATV_FEATURE:I = 0x5

.field public static final MTK_6516_SUPPORT:I = 0x2

.field public static final MTK_6572_SUPPORT:I = 0x20

.field public static final MTK_6573_SUPPORT:I = 0x1

.field public static final MTK_6575_SUPPORT:I = 0x4

.field public static final MTK_6577_SUPPORT:I = 0x8

.field public static final MTK_6582_SUPPORT:I = 0x12

.field public static final MTK_6589_SUPPORT:I = 0x10

.field public static final MTK_AGPS_APP:I = 0x3

.field public static final MTK_BT_SUPPORT:I = 0x6

.field public static final MTK_FM_SUPPORT:I = 0x0

.field public static final MTK_FM_TX_SUPPORT:I = 0x1

.field public static final MTK_GPS_SUPPORT:I = 0x4

.field public static final MTK_NFC_SUPPORT:I = 0x9

.field public static final MTK_RADIO_SUPPORT:I = 0x2

.field public static final MTK_TTY_SUPPORT:I = 0x8

.field public static final MTK_UNKNOWN_SUPPORT:I = 0x0

.field public static final MTK_WLAN_SUPPORT:I = 0x7


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "em_support_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native getChip()I
.end method

.method public static isCurrentChipEquals(I)Z
    .locals 2
    .param p0    # I

    invoke-static {}, Lcom/mediatek/engineermode/ChipSupport;->getChip()I

    move-result v0

    if-ne v0, p0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isCurrentChipHigher(IZ)Z
    .locals 3
    .param p0    # I
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/mediatek/engineermode/ChipSupport;->getChip()I

    move-result v0

    if-eqz p1, :cond_2

    if-lt v0, p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    if-gt v0, p0, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public static native isFeatureSupported(I)Z
.end method
