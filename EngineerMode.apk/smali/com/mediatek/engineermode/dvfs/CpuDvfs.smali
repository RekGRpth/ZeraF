.class public Lcom/mediatek/engineermode/dvfs/CpuDvfs;
.super Landroid/app/Activity;
.source "CpuDvfs.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final CAT:Ljava/lang/String; = "cat "

.field private static final CMD_PERFORMANCE_GOVERNOR:Ljava/lang/String; = "echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

.field private static final CMD_SET_CPU_SPEED:Ljava/lang/String; = "echo %1$s > /proc/cpufreq/cpufreq_cur_freq"

.field private static final ECHO:Ljava/lang/String; = "echo"

.field private static final FS_CUR_FREQ:Ljava/lang/String; = "/proc/cpufreq/cpufreq_cur_freq"

.field private static final FS_GOVERNOR_DEFAULT:Ljava/lang/String; = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

.field private static final FS_GOVERNOR_LIST:Ljava/lang/String; = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors"

.field private static final FS_SET_SPEED:Ljava/lang/String; = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed"

.field private static final TAG:Ljava/lang/String; = "CpuDvfs"


# instance fields
.field private mBtnSetFreq:Landroid/widget/Button;

.field private mEditCpuClockVal:Landroid/widget/EditText;

.field private mFreqMax:J

.field private mFreqMin:J

.field private mGovernorItems:[Ljava/lang/CharSequence;

.field private mLVCpuGovernor:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-wide v0, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMin:J

    iput-wide v0, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMax:J

    return-void
.end method

.method private execCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, -0x1

    const-string v3, "CpuDvfs"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[cmd]:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {p1}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    if-nez v2, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v1

    const-string v3, "CpuDvfs"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[output]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    const-string v3, "CpuDvfs"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getGovernorAvailList()[Ljava/lang/CharSequence;
    .locals 7

    const/4 v6, 0x0

    const-string v1, "cat  /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors"

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    new-array v0, v6, [Ljava/lang/CharSequence;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, " +"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v5, v3

    if-lez v5, :cond_2

    array-length v5, v3

    new-array v0, v5, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    :goto_1
    array-length v5, v3

    if-ge v2, v5, :cond_0

    aget-object v5, v3, v2

    aput-object v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    new-array v0, v6, [Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method private getGovernorIndex([Ljava/lang/CharSequence;)I
    .locals 5
    .param p1    # [Ljava/lang/CharSequence;

    const/4 v3, -0x1

    if-nez p1, :cond_1

    move v1, v3

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v0, "cat  /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    :goto_1
    array-length v4, p1

    if-ge v1, v4, :cond_2

    aget-object v4, p1, v1

    invoke-virtual {v4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v3

    goto :goto_0
.end method

.method private initUIByData()V
    .locals 14

    const-string v0, "cat /proc/cpufreq/cpufreq_cur_freq"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_1

    const-string v10, "Feature Fail or Don\'t Support!"

    const/4 v11, 0x0

    invoke-static {p0, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    const-string v10, " *\n *"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v10, 0x0

    aget-object v1, v3, v10

    const-string v10, " *: *"

    invoke-virtual {v1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mEditCpuClockVal:Landroid/widget/EditText;

    const/4 v11, 0x1

    aget-object v11, v8, v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v7, 0x1

    :goto_0
    array-length v10, v3

    if-ge v7, v10, :cond_0

    aget-object v10, v3, v7

    const-string v11, " +"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, -0x1

    const/4 v10, 0x1

    :try_start_0
    aget-object v10, v4, v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    :goto_1
    iget-wide v10, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMax:J

    const-wide/16 v12, -0x1

    cmp-long v10, v10, v12

    if-nez v10, :cond_4

    iput-wide v5, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMax:J

    :cond_2
    :goto_2
    iget-wide v10, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMin:J

    const-wide/16 v12, -0x1

    cmp-long v10, v10, v12

    if-nez v10, :cond_5

    iput-wide v5, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMin:J

    :cond_3
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v10, "CpuDvfs"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "NumberFormatExcaption: parse available freq fail: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x1

    aget-object v12, v4, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    iget-wide v10, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMax:J

    cmp-long v10, v5, v10

    if-lez v10, :cond_2

    iput-wide v5, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMax:J

    goto :goto_2

    :cond_5
    iget-wide v10, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMin:J

    cmp-long v10, v5, v10

    if-gez v10, :cond_3

    iput-wide v5, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMin:J

    goto :goto_3
.end method

.method private setCpuSpeed(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v0, "echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "echo %1$s > /proc/cpufreq/cpufreq_cur_freq"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method private setGovernor(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "echo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CpuDvfs"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SetGovernor: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->execCommand(Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method private showSelectDialog(Ljava/lang/String;[Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # Landroid/content/DialogInterface$OnClickListener;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private validateSetting()Z
    .locals 9

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-wide/16 v0, -0x1

    :try_start_0
    iget-object v6, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mEditCpuClockVal:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    iget-wide v6, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMin:J

    cmp-long v6, v0, v6

    if-ltz v6, :cond_0

    iget-wide v6, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMax:J

    cmp-long v6, v0, v6

    if-lez v6, :cond_1

    :cond_0
    const v6, 0x7f0805c8

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-wide v7, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMin:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v5

    iget-wide v7, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mFreqMax:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    move v4, v5

    :cond_1
    :goto_0
    return v4

    :catch_0
    move-exception v2

    const v4, 0x7f0805c6

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    move v4, v5

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mGovernorItems:[Ljava/lang/CharSequence;

    aget-object v0, v1, p2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->setGovernor(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0b00da

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->validateSetting()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mEditCpuClockVal:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->setCpuSpeed(Ljava/lang/String;)V

    const v1, 0x7f0805c7

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f03001f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const v3, 0x7f0b00d8

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mLVCpuGovernor:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mLVCpuGovernor:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v3, 0x7f0b00d9

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mEditCpuClockVal:Landroid/widget/EditText;

    const v3, 0x7f0b00da

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mBtnSetFreq:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mBtnSetFreq:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0805c4

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v3, 0x1090003

    invoke-direct {v0, p0, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mLVCpuGovernor:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->initUIByData()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    packed-switch p3, :pswitch_data_0

    const-string v1, "CpuDvfs"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->getGovernorAvailList()[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mGovernorItems:[Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mGovernorItems:[Ljava/lang/CharSequence;

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->getGovernorIndex([Ljava/lang/CharSequence;)I

    move-result v0

    const v1, 0x7f0805c5

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->mGovernorItems:[Ljava/lang/CharSequence;

    invoke-direct {p0, v1, v2, v0, p0}, Lcom/mediatek/engineermode/dvfs/CpuDvfs;->showSelectDialog(Ljava/lang/String;[Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
