.class Lcom/mediatek/engineermode/GPRS$6;
.super Landroid/os/Handler;
.source "GPRS.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/GPRS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/GPRS;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/GPRS;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$600(Lcom/mediatek/engineermode/GPRS;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Attached"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Attached succeeded."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Attached"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Attache failed."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Detached"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Detached succeeded."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Detached"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Detached failed."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Fast Dormancy"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Fast Dormancy command succeeded."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Fast Dormancy"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "GPRS Fast Dormancy command failed."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$800(Lcom/mediatek/engineermode/GPRS;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "PDP Activate"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "PDP Activate succeeded."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "PDP Activate"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "PDP Activate failed."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "PDP Deactivate"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "PDP Deactivate succeeded."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "PDP Deactivate"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "PDP Deactivate failed."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "Send Data"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "Send Data succeeded."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_8
    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "Send Data"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "Send Data failed."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/GPRS;->access$802(Lcom/mediatek/engineermode/GPRS;Z)Z

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "IMEI WRITE"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "The IMEI is writen successfully."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_9
    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "IMEI WRITE"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "Fail to write IMEI due to radio unavailable or something else."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/GPRS$6;->this$0:Lcom/mediatek/engineermode/GPRS;

    invoke-static {v1}, Lcom/mediatek/engineermode/GPRS;->access$700(Lcom/mediatek/engineermode/GPRS;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_2
    .end packed-switch
.end method
