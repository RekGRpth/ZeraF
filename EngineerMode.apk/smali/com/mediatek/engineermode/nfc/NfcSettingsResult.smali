.class public Lcom/mediatek/engineermode/nfc/NfcSettingsResult;
.super Landroid/app/Activity;
.source "NfcSettingsResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/nfc/NfcSettingsResult$1;,
        Lcom/mediatek/engineermode/nfc/NfcSettingsResult$OnClickListenerSpecial;
    }
.end annotation


# static fields
.field private static final NUMER_3:I = 0x3

.field private static final NUMER_4:I = 0x4

.field private static final NUMER_5:I = 0x5

.field private static final NUMER_6:I = 0x6

.field private static final NUMER_7:I = 0x7

.field public static final TAG:Ljava/lang/String; = "EM/nfc"


# instance fields
.field private mCardModeArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/engineermode/nfc/ModeMap;",
            ">;"
        }
    .end annotation
.end field

.field private mReaderModeArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/engineermode/nfc/ModeMap;",
            ">;"
        }
    .end annotation
.end field

.field private mTextFWVersion:Landroid/widget/TextView;

.field private mTextHWVersion:Landroid/widget/TextView;

.field private mTextSWVersion:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mTextFWVersion:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mTextHWVersion:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mTextSWVersion:Landroid/widget/TextView;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    return-void
.end method

.method private initUI()V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v3, 0x7f0b030f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mTextFWVersion:Landroid/widget/TextView;

    const v3, 0x7f0b0310

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mTextHWVersion:Landroid/widget/TextView;

    const v3, 0x7f0b0311

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mTextSWVersion:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0314

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v7}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0316

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v8}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0318

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v9}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b031a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v10}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b031c

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v11}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b031e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x5

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0320

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x6

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0322

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x7

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0315

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v7}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0317

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v8}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0319

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v9}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b031b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v10}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b031d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v11}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b031f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x5

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0321

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x6

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0323

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x7

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/mediatek/engineermode/nfc/NfcSettingsResult$OnClickListenerSpecial;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/nfc/NfcSettingsResult$OnClickListenerSpecial;-><init>(Lcom/mediatek/engineermode/nfc/NfcSettingsResult;Lcom/mediatek/engineermode/nfc/NfcSettingsResult$1;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/nfc/ModeMap;

    iget-object v3, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/nfc/ModeMap;

    iget-object v3, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private setCurrentMode(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mReaderModeArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/nfc/ModeMap;

    iget v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mBit:I

    shl-int v2, v3, v2

    and-int/2addr v2, p1

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mCardModeArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/nfc/ModeMap;

    iget v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mBit:I

    shl-int v2, v3, v2

    and-int/2addr v2, p2

    if-nez v2, :cond_4

    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_4
    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1

    :cond_5
    return-void
.end method

.method private setDisplayUI()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {}, Lcom/mediatek/engineermode/nfc/NfcRespMap;->getInst()Lcom/mediatek/engineermode/nfc/NfcRespMap;

    move-result-object v1

    const-string v2, "nfc.settings"

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/nfc/NfcRespMap;->take(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_setting_response;

    if-nez v0, :cond_0

    const-string v1, "EM/nfc"

    const-string v2, "Take NfcRespMap.KEY_SETTINGS is null"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mTextFWVersion:Landroid/widget/TextView;

    const-string v2, "0x%X"

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_setting_response;->fw_ver:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mTextHWVersion:Landroid/widget/TextView;

    const-string v2, "0x%X"

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_setting_response;->hw_ver:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->mTextSWVersion:Landroid/widget/TextView;

    const-string v2, "0x%X"

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_setting_response;->sw_ver:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_setting_response;->reader_mode:I

    iget v2, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_setting_response;->card_mode:I

    invoke-direct {p0, v1, v2}, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->setCurrentMode(II)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v0, 0x7f03005e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    const-string v0, "EM/nfc"

    const-string v1, "NfcSettingsResult onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->initUI()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSettingsResult;->setDisplayUI()V

    return-void
.end method
