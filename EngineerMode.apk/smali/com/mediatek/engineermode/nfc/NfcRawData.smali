.class public Lcom/mediatek/engineermode/nfc/NfcRawData;
.super Landroid/app/Activity;
.source "NfcRawData.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final EVENT_OP_FIN:I = 0x67

.field private static final EVENT_OP_MSG:I = 0x68

.field private static final EVENT_OP_START:I = 0x65

.field private static final EVENT_OP_STOP:I = 0x66

.field private static final EVENT_UPDATE_RADIO_UI:I = 0x69

.field private static final NUM_3:I = 0x3

.field private static final NUM_3F:I = 0x3

.field private static final NUM_4F:I = 0x4

.field private static final TAG:Ljava/lang/String; = "EM/nfc"

.field private static final TEST_ID_ALWAYSE_ON_WITH:I = 0x3e9

.field private static final TEST_ID_ALWAYSE_ON_WO:I = 0x3ea

.field private static final TEST_ID_ANTENNA_SELF:I = 0x3ef

.field private static final TEST_ID_CARD_EMUL_MODE:I = 0x3eb

.field private static final TEST_ID_P2P_MODE:I = 0x3ed

.field private static final TEST_ID_READER_MODE:I = 0x3ec

.field private static final TEST_ID_SWP_SELF:I = 0x3ee

.field private static final TEST_ID_UID_RW:I = 0x3f0

.field private static final TEST_START:I = 0x1

.field private static final TEST_STOP:I


# instance fields
.field private mAlwayseOnBitrateAppendix:Landroid/widget/Spinner;

.field private mAlwayseOnModuleAppendix:Landroid/widget/Spinner;

.field private mBtnStart:Landroid/widget/Button;

.field private mBtnStop:Landroid/widget/Button;

.field private mCardEmulModeProtAppendix:Landroid/widget/Spinner;

.field private mCardEmulModeTypeAppendix:Landroid/widget/Spinner;

.field private mHander:Landroid/os/Handler;

.field private mInProgress:Z

.field private mInStartTest:Z

.field private mRadioGpMode:Landroid/widget/RadioGroup;

.field private mRadioItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/RadioButton;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectionId:I

.field private mTextResult:Landroid/widget/TextView;

.field private mViewAlwayson:Landroid/view/View;

.field private mViewCardEmul:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStart:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStop:Landroid/widget/Button;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    iput v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mSelectionId:I

    iput-boolean v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mInStartTest:Z

    iput-boolean v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mInProgress:Z

    new-instance v0, Lcom/mediatek/engineermode/nfc/NfcRawData$2;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/nfc/NfcRawData$2;-><init>(Lcom/mediatek/engineermode/nfc/NfcRawData;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mHander:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/nfc/NfcRawData;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;

    iget v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mSelectionId:I

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/nfc/NfcRawData;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mSelectionId:I

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/nfc/NfcRawData;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/nfc/NfcRawData;->addAppendix(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/nfc/NfcRawData;Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_response;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;
    .param p1    # Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_response;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/nfc/NfcRawData;->handleResponse(Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_response;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/nfc/NfcRawData;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/nfc/NfcRawData;->enableAllRadioBox(Z)V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/nfc/NfcRawData;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mHander:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/engineermode/nfc/NfcRawData;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/nfc/NfcRawData;->threadSendMsg(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1402(Lcom/mediatek/engineermode/nfc/NfcRawData;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mInStartTest:Z

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/nfc/NfcRawData;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/nfc/NfcRawData;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStop:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/engineermode/nfc/NfcRawData;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mInProgress:Z

    return p1
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/nfc/NfcRawData;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/nfc/NfcRawData;->testing(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/nfc/NfcRawData;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mTextResult:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/nfc/NfcRawData;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/nfc/NfcRawData;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcRawData;->disableForTemp()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/nfc/NfcRawData;Z)Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/nfc/NfcRawData;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/nfc/NfcRawData;->makeRequest(Z)Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;

    move-result-object v0

    return-object v0
.end method

.method private addAppendix(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    const/16 v1, 0x8

    const/16 v0, 0x3e9

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3ea

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mViewAlwayson:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mViewCardEmul:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x3eb

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mViewAlwayson:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mViewCardEmul:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mViewAlwayson:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mViewCardEmul:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private closeNFCServiceAtStart()V
    .locals 3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->disable()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EM/nfc"

    const-string v2, "Nfc service set off."

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "EM/nfc"

    const-string v2, "Nfc service set off Fail."

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "EM/nfc"

    const-string v2, "Nfc service is off"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private disableForTemp()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "EM/nfc"

    const-string v1, "disableForTemp"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method private enableAllRadioBox(Z)V
    .locals 3
    .param p1    # Z

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/16 v2, 0x69

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "UP"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mHander:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private handleResponse(Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_response;)V
    .locals 5
    .param p1    # Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_response;

    iget-object v3, p1, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_response;->target:Ljava/lang/Object;

    instance-of v3, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_response;

    if-eqz v3, :cond_2

    iget-object v2, p1, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_response;->target:Ljava/lang/Object;

    check-cast v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_response;

    iget v3, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_response;->result:I

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_response;->result:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/nfc/NfcRawData;->threadSendMsg(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UID Test Result: \nData["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_response;->data:[B

    invoke-static {v4}, Lcom/mediatek/engineermode/nfc/NfcUtils;->printArray(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/nfc/NfcRawData;->threadSendMsg(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v3, p1, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_response;->target:Ljava/lang/Object;

    instance-of v3, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_response;

    if-eqz v3, :cond_0

    iget-object v0, p1, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_response;->target:Ljava/lang/Object;

    check-cast v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_response;

    iget v3, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_response;->result:I

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR: result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_response;->result:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/nfc/NfcRawData;->threadSendMsg(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v3, "Result: OK"

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/nfc/NfcRawData;->threadSendMsg(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initSpinner()V
    .locals 8

    const v7, 0x1090009

    const v6, 0x1090008

    const/4 v5, 0x0

    const v4, 0x7f0b02fd

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mAlwayseOnModuleAppendix:Landroid/widget/Spinner;

    const v4, 0x7f0b02fe

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mAlwayseOnBitrateAppendix:Landroid/widget/Spinner;

    const v4, 0x7f0b0300

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mCardEmulModeTypeAppendix:Landroid/widget/Spinner;

    const v4, 0x7f0b0301

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mCardEmulModeProtAppendix:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v4, 0x7f060062

    invoke-direct {v1, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v4, 0x7f060063

    invoke-direct {v0, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x7f060064

    invoke-direct {v3, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v4, 0x7f060065

    invoke-direct {v2, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v1, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v0, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v3, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v2, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mAlwayseOnModuleAppendix:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mAlwayseOnBitrateAppendix:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mCardEmulModeTypeAppendix:Landroid/widget/Spinner;

    invoke-virtual {v4, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mCardEmulModeProtAppendix:Landroid/widget/Spinner;

    invoke-virtual {v4, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mAlwayseOnModuleAppendix:Landroid/widget/Spinner;

    invoke-virtual {v4, v5}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mAlwayseOnBitrateAppendix:Landroid/widget/Spinner;

    invoke-virtual {v4, v5}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mCardEmulModeTypeAppendix:Landroid/widget/Spinner;

    invoke-virtual {v4, v5}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mCardEmulModeProtAppendix:Landroid/widget/Spinner;

    invoke-virtual {v4, v5}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void
.end method

.method private initUI()V
    .locals 4

    const v3, 0x7f0b02f6

    const/4 v2, 0x1

    const v0, 0x7f0b0302

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStart:Landroid/widget/Button;

    const v0, 0x7f0b0303

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStop:Landroid/widget/Button;

    const v0, 0x7f0b02f3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioGpMode:Landroid/widget/RadioGroup;

    const v0, 0x7f0b0304

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mTextResult:Landroid/widget/TextView;

    const v0, 0x7f0b02fc

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mViewAlwayson:Landroid/view/View;

    const v0, 0x7f0b02ff

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mViewCardEmul:Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    const v0, 0x7f0b02f4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    const v0, 0x7f0b02f5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    const v0, 0x7f0b02f7

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    const v0, 0x7f0b02f8

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    const v0, 0x7f0b02f9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    const v0, 0x7f0b02fa

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioItems:Ljava/util/ArrayList;

    const v0, 0x7f0b02fb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStop:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioGpMode:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/mediatek/engineermode/nfc/NfcRawData$1;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/nfc/NfcRawData$1;-><init>(Lcom/mediatek/engineermode/nfc/NfcRawData;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcRawData;->initSpinner()V

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/nfc/NfcRawData;->enableAllRadioBox(Z)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcRawData;->disableForTemp()V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioGpMode:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->check(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStop:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method private makeRequest(Z)Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;
    .locals 14
    .param p1    # Z

    new-instance v7, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;

    invoke-direct {v7}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;-><init>()V

    iget v9, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mSelectionId:I

    iput v9, v7, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;->which:I

    iget v9, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mSelectionId:I

    packed-switch v9, :pswitch_data_0

    :goto_0
    return-object v7

    :pswitch_0
    new-instance v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;

    invoke-direct {v5}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;-><init>()V

    if-eqz p1, :cond_1

    const/4 v9, 0x1

    :goto_1
    iput v9, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;->action:I

    const/4 v9, 0x1

    iput v9, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;->type:I

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mAlwayseOnModuleAppendix:Landroid/widget/Spinner;

    invoke-virtual {v9}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v9

    int-to-byte v1, v9

    const/4 v9, -0x1

    if-ne v1, v9, :cond_2

    const/4 v9, 0x0

    :goto_2
    iput-byte v9, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;->modulation_type:B

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mAlwayseOnBitrateAppendix:Landroid/widget/Spinner;

    invoke-virtual {v9}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v9

    int-to-byte v0, v9

    const/4 v9, -0x1

    if-ne v0, v9, :cond_0

    const/4 v1, 0x0

    :cond_0
    iput-byte v1, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;->bitrate:B

    iput-object v5, v7, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;->target:Ljava/lang/Object;

    const-string v9, "EM/nfc"

    const-string v10, "action %d, type %d, modulation %d, bitrate %d"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;->action:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget v13, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;->type:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    iget-byte v13, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;->modulation_type:B

    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x3

    iget-byte v13, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tx_alwayson_request;->bitrate:B

    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    :cond_2
    move v9, v1

    goto :goto_2

    :pswitch_1
    new-instance v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;

    invoke-direct {v2}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;-><init>()V

    if-eqz p1, :cond_3

    const/4 v9, 0x1

    :goto_3
    iput v9, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;->action:I

    const/4 v9, 0x1

    iput v9, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;->type:I

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mCardEmulModeTypeAppendix:Landroid/widget/Spinner;

    invoke-virtual {v9}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v8

    const/4 v9, 0x1

    shl-int/2addr v9, v8

    int-to-short v9, v9

    iput-short v9, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;->technology:S

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mCardEmulModeProtAppendix:Landroid/widget/Spinner;

    invoke-virtual {v9}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    const/4 v9, 0x1

    shl-int/2addr v9, v6

    int-to-short v9, v9

    iput-short v9, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;->protocols:S

    iput-object v2, v7, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;->target:Ljava/lang/Object;

    const-string v9, "EM/nfc"

    const-string v10, "action %d, type %d, technology %d, protocols %d"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;->action:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget v13, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;->type:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    iget-short v13, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;->technology:S

    invoke-static {v13}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x3

    iget-short v13, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_card_emulation_request;->protocols:S

    invoke-static {v13}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    const/4 v9, 0x0

    goto :goto_3

    :pswitch_2
    new-instance v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_request;

    invoke-direct {v4}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_request;-><init>()V

    if-eqz p1, :cond_4

    const/4 v9, 0x1

    :goto_4
    iput v9, v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_request;->action:I

    const/4 v9, 0x1

    iput v9, v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_request;->type:I

    const/4 v9, 0x1

    iput v9, v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_request;->uid_type:I

    iget-object v9, v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_request;->data:[B

    const/4 v10, 0x0

    const/4 v11, 0x1

    aput-byte v11, v9, v10

    iget-object v9, v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_request;->data:[B

    const/4 v10, 0x1

    const/4 v11, 0x2

    aput-byte v11, v9, v10

    iget-object v9, v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_request;->data:[B

    const/4 v10, 0x2

    const/4 v11, 0x3

    aput-byte v11, v9, v10

    iget-object v9, v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_uid_request;->data:[B

    const/4 v10, 0x3

    const/4 v11, 0x4

    aput-byte v11, v9, v10

    iput-object v4, v7, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;->target:Ljava/lang/Object;

    goto/16 :goto_0

    :cond_4
    const/4 v9, 0x0

    goto :goto_4

    :pswitch_3
    new-instance v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_request;

    invoke-direct {v3}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_request;-><init>()V

    if-eqz p1, :cond_5

    const/4 v9, 0x1

    :goto_5
    iput v9, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_request;->action:I

    const/4 v9, 0x1

    iput v9, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_script_request;->type:I

    iput-object v3, v7, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_test_request;->target:Ljava/lang/Object;

    goto/16 :goto_0

    :cond_5
    const/4 v9, 0x0

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private testing(Z)V
    .locals 4
    .param p1    # Z

    const/16 v3, 0x3f0

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mTextResult:Landroid/widget/TextView;

    const-string v1, "Waiting..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mInStartTest:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mSelectionId:I

    if-ne v0, v3, :cond_1

    const-string v0, "EM/nfc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ID = 1008, Select ID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mSelectionId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "EM/nfc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ++++ mInStartTest = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mInStartTest:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mInProgress = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mInProgress:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mInProgress:Z

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    iget v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mSelectionId:I

    if-ne v0, v3, :cond_2

    new-instance v0, Lcom/mediatek/engineermode/nfc/NfcRawData$3;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/engineermode/nfc/NfcRawData$3;-><init>(Lcom/mediatek/engineermode/nfc/NfcRawData;Z)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void

    :cond_1
    const-string v0, "Operation is still in progress. Try later."

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/nfc/NfcRawData;->threadSendMsg(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mInStartTest:Z

    new-instance v0, Lcom/mediatek/engineermode/nfc/NfcRawData$4;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/engineermode/nfc/NfcRawData$4;-><init>(Lcom/mediatek/engineermode/nfc/NfcRawData;Z)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private threadSendMsg(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/16 v2, 0x68

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MSG"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mHander:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStart:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mRadioGpMode:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Test Mode is not selected."

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mHander:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mBtnStop:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcRawData;->mHander:Landroid/os/Handler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    const-string v0, "EM/nfc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ASSERT. Ghost view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EM/nfc"

    const-string v1, "NfcRawData onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f03005c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcRawData;->closeNFCServiceAtStart()V

    const-string v0, "EM/nfc"

    const-string v1, "NfcRawData onResume"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcRawData;->initUI()V

    return-void
.end method
