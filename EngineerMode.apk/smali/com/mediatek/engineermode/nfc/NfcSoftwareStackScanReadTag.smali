.class public Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;
.super Landroid/app/Activity;
.source "NfcSoftwareStackScanReadTag.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag$1;,
        Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag$OnClickListenerSpecial;
    }
.end annotation


# static fields
.field public static final NUM_3:I = 0x3

.field public static final TAG:Ljava/lang/String; = "EM/nfc"

.field public static final TAG_TYPE_MC1K:I = 0x2

.field public static final TAG_TYPE_MC4K:I = 0x3

.field public static final TAG_TYPE_NDEF:I = 0x1

.field public static final TAG_TYPE_NONE:I


# instance fields
.field private mBtn1KOK:Landroid/widget/Button;

.field private mBtn4KOK:Landroid/widget/Button;

.field private mCurrentTag:I

.field private mEditBlock:Landroid/widget/EditText;

.field private mEditPageAddress:Landroid/widget/EditText;

.field private mEditPageI:Landroid/widget/EditText;

.field private mEditPageII:Landroid/widget/EditText;

.field private mEditPageIII:Landroid/widget/EditText;

.field private mEditPageIV:Landroid/widget/EditText;

.field private mEditPayloadASCII:Landroid/widget/EditText;

.field private mEditPayloadHex:Landroid/widget/EditText;

.field private mEditSector:Landroid/widget/EditText;

.field private mEditSectorInfo:Landroid/widget/EditText;

.field private mTagTypeArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/engineermode/nfc/ModeMap;",
            ">;"
        }
    .end annotation
.end field

.field private mTextLang:Landroid/widget/TextView;

.field private mTextPayloadLen:Landroid/widget/TextView;

.field private mTextRecFlags:Landroid/widget/TextView;

.field private mTextRecId:Landroid/widget/TextView;

.field private mTextRecTnf:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mCurrentTag:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTagTypeArray:Ljava/util/ArrayList;

    return-void
.end method

.method private initUI()V
    .locals 7

    const v3, 0x7f0b034f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTextLang:Landroid/widget/TextView;

    const v3, 0x7f0b0352

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTextRecFlags:Landroid/widget/TextView;

    const v3, 0x7f0b0353

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTextRecId:Landroid/widget/TextView;

    const v3, 0x7f0b0354

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTextRecTnf:Landroid/widget/TextView;

    const v3, 0x7f0b0355

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTextPayloadLen:Landroid/widget/TextView;

    const v3, 0x7f0b0356

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPayloadHex:Landroid/widget/EditText;

    const v3, 0x7f0b0357

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPayloadASCII:Landroid/widget/EditText;

    const v3, 0x7f0b0359

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPageAddress:Landroid/widget/EditText;

    const v3, 0x7f0b035b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPageI:Landroid/widget/EditText;

    const v3, 0x7f0b035d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPageII:Landroid/widget/EditText;

    const v3, 0x7f0b035f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPageIII:Landroid/widget/EditText;

    const v3, 0x7f0b0361

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPageIV:Landroid/widget/EditText;

    const v3, 0x7f0b0364

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditSector:Landroid/widget/EditText;

    const v3, 0x7f0b0365

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditBlock:Landroid/widget/EditText;

    const v3, 0x7f0b0366

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditSectorInfo:Landroid/widget/EditText;

    const v3, 0x7f0b0362

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mBtn1KOK:Landroid/widget/Button;

    const v3, 0x7f0b0367

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mBtn4KOK:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTagTypeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b034d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x0

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTagTypeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b034e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x1

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTagTypeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0350

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x2

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTagTypeArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0351

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x3

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag$OnClickListenerSpecial;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag$OnClickListenerSpecial;-><init>(Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag$1;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTagTypeArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/nfc/ModeMap;

    iget-object v3, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mBtn1KOK:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->selectView()V

    return-void
.end method

.method private readMC1K()I
    .locals 6

    const/4 v4, 0x1

    new-instance v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;

    invoke-direct {v2}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;-><init>()V

    iput v4, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;->read_type:I

    iget-object v5, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPageAddress:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;->address:I

    invoke-static {v2}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass;->readTag(Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;)Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v5, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->status:I

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v4, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare1K;

    check-cast v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare1K;

    iget v0, v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare1K;->address:I

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPageAddress:Landroid/widget/EditText;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare1K;

    check-cast v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare1K;

    iget-object v1, v4, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare1K;->data:[S

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPageI:Landroid/widget/EditText;

    invoke-static {v1}, Lcom/mediatek/engineermode/nfc/NfcUtils;->printArray(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    goto :goto_0
.end method

.method private readMC4K()I
    .locals 7

    new-instance v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;

    invoke-direct {v2}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;-><init>()V

    const/4 v5, 0x2

    iput v5, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;->read_type:I

    iget-object v5, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditBlock:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;->block:I

    iget-object v5, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditSector:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, v2, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;->sector:I

    invoke-static {v2}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass;->readTag(Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;)Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v5, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->status:I

    if-eqz v5, :cond_1

    :cond_0
    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_1
    iget-object v5, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare4K;

    check-cast v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare4K;

    iget v0, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare4K;->block:I

    iget-object v5, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditBlock:Landroid/widget/EditText;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare4K;

    check-cast v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare4K;

    iget v4, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare4K;->sector:I

    iget-object v5, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditSector:Landroid/widget/EditText;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v3, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare4K;

    check-cast v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare4K;

    iget-object v1, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_Mifare4K;->data:[S

    iget-object v5, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditSectorInfo:Landroid/widget/EditText;

    invoke-static {v1}, Lcom/mediatek/engineermode/nfc/NfcUtils;->printArray(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v5, 0x0

    goto :goto_0
.end method

.method private readNDEF()I
    .locals 11

    new-instance v7, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;

    invoke-direct {v7}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;-><init>()V

    const/16 v9, 0x9

    iput v9, v7, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;->read_type:I

    invoke-static {v7}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass;->readTag(Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_request;)Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;

    move-result-object v8

    if-eqz v8, :cond_0

    iget v9, v8, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->status:I

    if-eqz v9, :cond_1

    :cond_0
    const/4 v9, 0x1

    :goto_0
    return v9

    :cond_1
    iget-object v9, v8, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    iget v3, v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;->ndef_type:I

    iget-object v9, v8, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    iget v4, v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;->recordFlage:I

    iget-object v9, v8, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    iget v5, v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;->recordId:I

    iget-object v9, v8, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    iget v6, v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;->recordInfo:I

    iget-object v9, v8, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    iget v2, v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;->length:I

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->setCurrentMode(I)V

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTextRecFlags:Landroid/widget/TextView;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTextRecId:Landroid/widget/TextView;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTextRecTnf:Landroid/widget/TextView;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTextPayloadLen:Landroid/widget/TextView;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, v8, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    iget-object v1, v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;->dataHex:[S

    iget-object v9, v8, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_response;->target:Ljava/lang/Object;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    check-cast v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;

    iget-object v0, v9, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_read_ndef;->dataAscii:[C

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPayloadASCII:Landroid/widget/EditText;

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPayloadHex:Landroid/widget/EditText;

    invoke-static {v1}, Lcom/mediatek/engineermode/nfc/NfcUtils;->printArray(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v9, 0x0

    goto :goto_0
.end method

.method private selectView()V
    .locals 8

    const/16 v7, 0x8

    const v5, 0x7f0b034c

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0b0358

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0b0363

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {}, Lcom/mediatek/engineermode/nfc/NfcRespMap;->getInst()Lcom/mediatek/engineermode/nfc/NfcRespMap;

    move-result-object v5

    const-string v6, "nfc.software_stack.scan_complete"

    invoke-virtual {v5, v6}, Lcom/mediatek/engineermode/nfc/NfcRespMap;->take(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_dis_notif_response;

    if-nez v1, :cond_0

    const-string v5, "EM/nfc"

    const-string v6, "selectView(): Take NfcRespMap.KEY_SS_SCAN_COMPLETE is null"

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v5, v1, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_dis_notif_response;->target:Ljava/lang/Object;

    check-cast v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;

    iget v0, v5, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_tag_det_response;->card_type:I

    sparse-switch v0, :sswitch_data_0

    const-string v5, "EM/nfc"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "selectView() garbage card_type:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_0
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    const/4 v5, 0x2

    iput v5, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mCurrentTag:I

    goto :goto_0

    :sswitch_1
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    const/4 v5, 0x3

    iput v5, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mCurrentTag:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    const/4 v5, 0x1

    iput v5, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mCurrentTag:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch
.end method

.method private setCurrentMode(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mTagTypeArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/nfc/ModeMap;

    iget v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mBit:I

    shl-int v2, v4, v2

    and-int/2addr v2, p1

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    const-string v2, "EM/nfc"

    const-string v3, "NfcSoftwareStackScanReadNDEF onClick"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mBtn1KOK:Landroid/widget/Button;

    if-ne p1, v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditPageAddress:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08059c

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->readMC1K()I

    :cond_3
    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mBtn4KOK:Landroid/widget/Button;

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditBlock:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mEditSector:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_4

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    if-eqz v1, :cond_4

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08059d

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->readMC4K()I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const-string v0, "EM/nfc"

    const-string v1, "NfcSoftwareStackScanReadTag onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f030063

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->initUI()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->mCurrentTag:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackScanReadTag;->readNDEF()I

    :cond_0
    return-void
.end method
