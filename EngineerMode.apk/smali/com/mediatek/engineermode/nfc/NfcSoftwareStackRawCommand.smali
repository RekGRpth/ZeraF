.class public Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;
.super Landroid/app/Activity;
.source "NfcSoftwareStackRawCommand.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final TAG:Ljava/lang/String; = "EM/nfc"


# instance fields
.field private mBtnExample:Landroid/widget/Button;

.field private mBtnSend:Landroid/widget/Button;

.field private mEditCommand:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private initUI()V
    .locals 1

    const v0, 0x7f0b033d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->mBtnSend:Landroid/widget/Button;

    const v0, 0x7f0b033e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->mBtnExample:Landroid/widget/Button;

    const v0, 0x7f0b033c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->mEditCommand:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->mBtnSend:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->mBtnExample:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private onSend()Z
    .locals 2

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->mEditCommand:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EM/nfc"

    invoke-static {v1, v0}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    return v1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const-string v0, "EM/nfc"

    const-string v1, "NfcSoftwareStackRawCommand onClick"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->mBtnSend:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->onSend()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->mBtnExample:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->mEditCommand:Landroid/widget/EditText;

    const-string v1, "00 A4 04 00 0E A0 00 00 00 18 30 80 05 00 65 63 68 6F 00 00"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v0, 0x7f030061

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    const-string v0, "EM/nfc"

    const-string v1, "NfcSoftwareStackRawCommand onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackRawCommand;->initUI()V

    return-void
.end method
