.class public Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;
.super Landroid/app/Activity;
.source "NfcSoftwareStackSecure.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure$1;,
        Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure$OnClickListenerSpecial;
    }
.end annotation


# static fields
.field private static final DIALOG_EXCEPTION:I = 0x0

.field public static final SE_MODE_NOTSUPPORT:I = 0x0

.field public static final SE_MODE_SE1:I = 0x1

.field public static final SE_MODE_SE2:I = 0x2

.field public static final SE_MODE_STATUS_OFF:I = 0x0

.field public static final SE_MODE_STATUS_VIRTUAL:I = 0x1

.field public static final SE_MODE_STATUS_WIRED:I = 0x2

.field public static final TAG:Ljava/lang/String; = "EM/nfc"


# instance fields
.field private mBtnSet:Landroid/widget/Button;

.field private mDisplayChkBoxArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/engineermode/nfc/ModeMap;",
            ">;"
        }
    .end annotation
.end field

.field private mDummyTag:Landroid/widget/CheckBox;

.field private mEleInfo:Landroid/widget/TextView;

.field private mGpSE1:Landroid/widget/RadioGroup;

.field private mGpSE2:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDisplayChkBoxArray:Ljava/util/ArrayList;

    return-void
.end method

.method private getSEIdx(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0377
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private initUI()V
    .locals 8

    const/4 v7, 0x1

    const v3, 0x7f0b0375

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mEleInfo:Landroid/widget/TextView;

    const v3, 0x7f0b0374

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDummyTag:Landroid/widget/CheckBox;

    const v3, 0x7f0b037e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mBtnSet:Landroid/widget/Button;

    const v3, 0x7f0b0376

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioGroup;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE1:Landroid/widget/RadioGroup;

    const v3, 0x7f0b037a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioGroup;

    iput-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE2:Landroid/widget/RadioGroup;

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDisplayChkBoxArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b037f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x0

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDisplayChkBoxArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0381

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-direct {v5, v3, v7}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDisplayChkBoxArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0383

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x2

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDisplayChkBoxArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0380

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x3

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDisplayChkBoxArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0382

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x4

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDisplayChkBoxArray:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/engineermode/nfc/ModeMap;

    const v3, 0x7f0b0384

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    const/4 v6, 0x5

    invoke-direct {v5, v3, v6}, Lcom/mediatek/engineermode/nfc/ModeMap;-><init>(Landroid/widget/CheckBox;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure$OnClickListenerSpecial;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure$OnClickListenerSpecial;-><init>(Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure$1;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDisplayChkBoxArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/nfc/ModeMap;

    iget-object v3, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDummyTag:Landroid/widget/CheckBox;

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDummyTag:Landroid/widget/CheckBox;

    invoke-virtual {v3, v7}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method

.method private readSecureEleInfo()V
    .locals 5

    const/4 v4, 0x1

    invoke-static {}, Lcom/mediatek/engineermode/nfc/NfcRespMap;->getInst()Lcom/mediatek/engineermode/nfc/NfcRespMap;

    move-result-object v1

    const-string v2, "nfc.software_stack.reg_notif"

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/nfc/NfcRespMap;->take(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;

    if-nez v0, :cond_0

    const-string v1, "EM/nfc"

    const-string v2, "Take NfcRespMap.KEY_SS_REGISTER_NOTIF is null"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v1, "EM/nfc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "se "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->se:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " se_status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->se_status:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->se:I

    if-nez v1, :cond_2

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080596

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_1
    iget v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->length:I

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mEleInfo:Landroid/widget/TextView;

    const-string v2, "No Info."

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    const-string v1, "EM/nfc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mask "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->se_type:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->se_type:I

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->setCurrentSEEventMask(I)V

    goto :goto_0

    :cond_2
    iget v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->se:I

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE1:Landroid/widget/RadioGroup;

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->setRadioGpEnable(Landroid/widget/RadioGroup;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE1:Landroid/widget/RadioGroup;

    iget v2, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->se_status:I

    invoke-direct {p0, v1, v2}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->setSEIdx(Landroid/widget/RadioGroup;I)V

    goto :goto_1

    :cond_3
    iget v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->se:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE2:Landroid/widget/RadioGroup;

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->setRadioGpEnable(Landroid/widget/RadioGroup;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE2:Landroid/widget/RadioGroup;

    iget v2, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->se_status:I

    invoke-direct {p0, v1, v2}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->setSEIdx(Landroid/widget/RadioGroup;I)V

    goto :goto_1

    :cond_4
    iget-object v1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->data:[B

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mEleInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/String;

    iget-object v3, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_reg_notif_response;->data:[B

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mEleInfo:Landroid/widget/TextView;

    const-string v2, "Broken Info."

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private setCurrentSEEventMask(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mDisplayChkBoxArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/nfc/ModeMap;

    iget v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mBit:I

    shl-int v2, v4, v2

    and-int/2addr v2, p1

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    iget-object v2, v1, Lcom/mediatek/engineermode/nfc/ModeMap;->mChkBox:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private setRadioGpEnable(Landroid/widget/RadioGroup;)V
    .locals 10
    .param p1    # Landroid/widget/RadioGroup;

    const/4 v9, 0x1

    const/4 v8, 0x0

    const v6, 0x7f0b0377

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const v6, 0x7f0b0378

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    const v6, 0x7f0b0379

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    const v6, 0x7f0b037b

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    const v6, 0x7f0b037c

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    const v6, 0x7f0b037d

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE1:Landroid/widget/RadioGroup;

    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v7

    if-ne v6, v7, :cond_1

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE2:Landroid/widget/RadioGroup;

    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v7

    if-ne v6, v7, :cond_0

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private setSEIdx(Landroid/widget/RadioGroup;I)V
    .locals 2
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE1:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    packed-switch p2, :pswitch_data_0

    invoke-virtual {p1}, Landroid/widget/RadioGroup;->clearCheck()V

    :goto_0
    return-void

    :pswitch_0
    const v0, 0x7f0b0377

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b0378

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b0379

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :cond_0
    packed-switch p2, :pswitch_data_1

    invoke-virtual {p1}, Landroid/widget/RadioGroup;->clearCheck()V

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0b037b

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0b037c

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0b037d

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setSEOption(I)I
    .locals 3
    .param p1    # I

    new-instance v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_se_set_request;

    invoke-direct {v0}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_se_set_request;-><init>()V

    iput p1, v0, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_se_set_request;->set_SEtype:I

    invoke-static {v0}, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass;->setSEOption(Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_se_set_request;)Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_se_set_response;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    iget v2, v1, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_se_set_response;->status:I

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    iget v2, v1, Lcom/mediatek/engineermode/nfc/NfcNativeCallClass$nfc_se_set_response;->status:I

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x1

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mBtnSet:Landroid/widget/Button;

    if-ne p1, v2, :cond_0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE1:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE1:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->getSEIdx(I)I

    move-result v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->setSEOption(I)I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x104000a

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE2:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mGpSE2:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->getSEIdx(I)I

    move-result v0

    goto :goto_0

    :cond_2
    if-ne v5, v1, :cond_3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Response is null."

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Response status is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v0, 0x7f030065

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    const-string v0, "EM/nfc"

    const-string v1, "NfcSoftwareStackSecure onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->initUI()V

    iget-object v0, p0, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->mBtnSet:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f08058c

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f080592

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/nfc/NfcSoftwareStackSecure;->readSecureEleInfo()V

    return-void
.end method
