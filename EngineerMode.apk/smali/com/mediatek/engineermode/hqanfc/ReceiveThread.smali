.class public Lcom/mediatek/engineermode/hqanfc/ReceiveThread;
.super Ljava/lang/Object;
.source "ReceiveThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInputStream:Ljava/io/DataInputStream;

.field private mRunning:Z


# direct methods
.method public constructor <init>(Ljava/io/DataInputStream;Landroid/content/Context;)V
    .locals 1
    .param p1    # Ljava/io/DataInputStream;
    .param p2    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mInputStream:Ljava/io/DataInputStream;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mRunning:Z

    return-void
.end method


# virtual methods
.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mRunning:Z

    return v0
.end method

.method public run()V
    .locals 14

    iget-object v11, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mInputStream:Ljava/io/DataInputStream;

    if-nez v11, :cond_1

    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "[ReceiveThread]The dispatcher or stream object is null!"

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mRunning:Z

    iget-object v11, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/mediatek/engineermode/hqanfc/NfcCommandHandler;->getInstance(Landroid/content/Context;)Lcom/mediatek/engineermode/hqanfc/NfcCommandHandler;

    move-result-object v3

    :goto_1
    iget-boolean v11, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mRunning:Z

    if-eqz v11, :cond_5

    const/16 v11, 0x400

    :try_start_0
    new-array v0, v11, [B

    iget-object v11, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v11, v0}, Ljava/io/DataInputStream;->read([B)I

    const/4 v5, 0x0

    :goto_2
    array-length v11, v0

    if-ge v5, v11, :cond_2

    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    aget-byte v13, v0, v5

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_2
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "done receive"

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v11, 0x4

    new-array v10, v11, [B

    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    invoke-static {v10}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$DataConvert;->byteToInt([B)I

    move-result v9

    const-string v11, "EM/HQA/NFC"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[ReceiveThread:info]Recieved data message type is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v11, 0x78

    if-le v9, v11, :cond_3

    const-string v11, "EM/HQA/NFC"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[ReceiveThread]receive message is not the correct msg and the content: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mRunning:Z

    const-string v11, "EM/HQA/NFC"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[ReceiveThread]receive thread IOException: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_3
    const/4 v11, 0x4

    :try_start_1
    new-array v6, v11, [B

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    invoke-static {v6}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$DataConvert;->byteToInt([B)I

    move-result v8

    const-string v11, "EM/HQA/NFC"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[ReceiveThread:info]Recieved data message lenght is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v8, :cond_4

    new-instance v7, Lcom/mediatek/engineermode/hqanfc/NfcCommand;

    const/4 v11, 0x0

    invoke-direct {v7, v9, v11}, Lcom/mediatek/engineermode/hqanfc/NfcCommand;-><init>(ILjava/nio/ByteBuffer;)V

    :goto_3
    invoke-virtual {v3, v7}, Lcom/mediatek/engineermode/hqanfc/NfcCommandHandler;->execute(Lcom/mediatek/engineermode/hqanfc/NfcCommand;)Z

    goto/16 :goto_1

    :cond_4
    new-array v2, v8, [B

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    new-instance v7, Lcom/mediatek/engineermode/hqanfc/NfcCommand;

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-direct {v7, v9, v11}, Lcom/mediatek/engineermode/hqanfc/NfcCommand;-><init>(ILjava/nio/ByteBuffer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_5
    iget-boolean v11, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mRunning:Z

    if-nez v11, :cond_0

    invoke-virtual {v3}, Lcom/mediatek/engineermode/hqanfc/NfcCommandHandler;->destroy()V

    goto/16 :goto_0
.end method

.method public setRunning(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->mRunning:Z

    return-void
.end method
