.class Lcom/mediatek/engineermode/hqanfc/RwFunction$2;
.super Landroid/os/Handler;
.source "RwFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/hqanfc/RwFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/hqanfc/RwFunction;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    const/4 v1, 0x0

    const/16 v2, 0x12c

    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$100(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermOptRsp;->mResult:I

    packed-switch v2, :pswitch_data_0

    const-string v1, "Rw Format Rsp Result: ERROR"

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    return-void

    :pswitch_0
    const-string v1, "Rw Format Rsp Result: SUCCESS"

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$400(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$500(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$600(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_1
    const-string v1, "Rw Format Rsp Result: FAIL"

    goto :goto_0

    :cond_1
    const/16 v2, 0x64

    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$300(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mResult:I

    packed-switch v2, :pswitch_data_1

    const-string v1, "ReaderMode Ntf Result: ERROR"

    goto :goto_0

    :pswitch_2
    const-string v1, "ReaderMode Ntf Result: CONNECT"

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$300(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mIsNdef:I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$300(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mIsNdef:I

    if-eq v2, v4, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$300(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mIsNdef:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "reader_mode_rsp_array"

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v3}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$000(Lcom/mediatek/engineermode/hqanfc/RwFunction;)[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v2, "reader_mode_rsp_ndef"

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-static {v3}, Lcom/mediatek/engineermode/hqanfc/RwFunction;->access$300(Lcom/mediatek/engineermode/hqanfc/RwFunction;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;

    move-result-object v3

    iget v3, v3, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermNtf;->mIsNdef:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    const-class v3, Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :pswitch_3
    const-string v1, "ReaderMode Ntf Result: DISCONNECT"

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/RwFunction$2;->this$0:Lcom/mediatek/engineermode/hqanfc/RwFunction;

    invoke-virtual {v2}, Landroid/app/Activity;->onBackPressed()V

    goto/16 :goto_0

    :pswitch_4
    const-string v1, "ReaderMode Ntf Result: FAIL"

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
