.class public Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;
.super Ljava/lang/Object;
.source "NfcCommand.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/hqanfc/NfcCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BitMapValue"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFunctionValue([Landroid/widget/CheckBox;)I
    .locals 5
    .param p0    # [Landroid/widget/CheckBox;

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    aget-object v1, p0, v3

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    or-int/2addr v0, v1

    aget-object v1, p0, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v4

    :goto_1
    or-int/2addr v0, v1

    aget-object v1, p0, v4

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v3, 0x4

    :cond_0
    or-int/2addr v0, v3

    return v0

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1
.end method

.method public static getSwioValue([Landroid/widget/CheckBox;)I
    .locals 5
    .param p0    # [Landroid/widget/CheckBox;

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    aget-object v1, p0, v3

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    or-int/2addr v0, v1

    aget-object v1, p0, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v4

    :goto_1
    or-int/2addr v0, v1

    aget-object v1, p0, v4

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v3, 0x4

    :cond_0
    or-int/2addr v0, v3

    return v0

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1
.end method

.method public static getTypeAbDataRateValue([Landroid/widget/CheckBox;)I
    .locals 5
    .param p0    # [Landroid/widget/CheckBox;

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    aget-object v1, p0, v3

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    or-int/2addr v0, v1

    aget-object v1, p0, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v4

    :goto_1
    or-int/2addr v0, v1

    aget-object v1, p0, v4

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    :goto_2
    or-int/2addr v0, v1

    const/4 v1, 0x3

    aget-object v1, p0, v1

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v3, 0x8

    :cond_0
    or-int/2addr v0, v3

    return v0

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method public static getTypeFDataRateValue([Landroid/widget/CheckBox;)I
    .locals 3
    .param p0    # [Landroid/widget/CheckBox;

    const/4 v2, 0x0

    const/4 v0, 0x0

    aget-object v1, p0, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    :goto_0
    or-int/2addr v0, v1

    const/4 v1, 0x1

    aget-object v1, p0, v1

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x4

    :cond_0
    or-int/2addr v0, v2

    return v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public static getTypeVDataRateValue([Landroid/widget/CheckBox;)I
    .locals 3
    .param p0    # [Landroid/widget/CheckBox;

    const/4 v2, 0x0

    const/4 v0, 0x0

    aget-object v1, p0, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x10

    :goto_0
    or-int/2addr v0, v1

    const/4 v1, 0x1

    aget-object v1, p0, v1

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v2, 0x20

    :cond_0
    or-int/2addr v0, v2

    return v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public static getTypeValue([Landroid/widget/CheckBox;)I
    .locals 6
    .param p0    # [Landroid/widget/CheckBox;

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    aget-object v1, p0, v3

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    or-int/2addr v0, v1

    aget-object v1, p0, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v4

    :goto_1
    or-int/2addr v0, v1

    aget-object v1, p0, v4

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v5

    :goto_2
    or-int/2addr v0, v1

    const/4 v1, 0x3

    aget-object v1, p0, v1

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x8

    :goto_3
    or-int/2addr v0, v1

    aget-object v1, p0, v5

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v1, 0x10

    :goto_4
    or-int/2addr v0, v1

    const/4 v1, 0x5

    aget-object v1, p0, v1

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v3, 0x20

    :cond_0
    or-int/2addr v0, v3

    return v0

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_4
.end method
