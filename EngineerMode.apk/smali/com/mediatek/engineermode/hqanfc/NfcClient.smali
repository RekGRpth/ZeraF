.class public Lcom/mediatek/engineermode/hqanfc/NfcClient;
.super Ljava/lang/Object;
.source "NfcClient.java"


# static fields
.field public static final DEFAULT_PORT:I = 0x1d4c

.field public static final DEFAULT_TIMEOUT:I = 0xfa0

.field private static final LOCALHOST_IP_ADDRESS:Ljava/lang/String; = "127.0.0.1"

.field private static final MAX_DISCON_TIMES:I = 0x5

.field private static final SLEEP_TIME:I = 0x64

.field private static sInstance:Lcom/mediatek/engineermode/hqanfc/NfcClient;


# instance fields
.field private mConnected:Z

.field protected mInputStream:Ljava/io/DataInputStream;

.field protected mOutputStream:Ljava/io/DataOutputStream;

.field protected mPollingThr:Lcom/mediatek/engineermode/hqanfc/ReceiveThread;

.field protected mSocket:Ljava/net/Socket;

.field protected mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mConnected:Z

    return-void
.end method

.method public static getInstance()Lcom/mediatek/engineermode/hqanfc/NfcClient;
    .locals 1

    sget-object v0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sInstance:Lcom/mediatek/engineermode/hqanfc/NfcClient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/NfcClient;

    invoke-direct {v0}, Lcom/mediatek/engineermode/hqanfc/NfcClient;-><init>()V

    sput-object v0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sInstance:Lcom/mediatek/engineermode/hqanfc/NfcClient;

    :cond_0
    sget-object v0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sInstance:Lcom/mediatek/engineermode/hqanfc/NfcClient;

    return-object v0
.end method


# virtual methods
.method public closeConnection()Z
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    const-string v3, "EM/HQA/NFC"

    const-string v4, "[NfcClient]closeConnection()."

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "EM/HQA/NFC"

    const-string v4, "[NfcClient]close connection fail"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mPollingThr:Lcom/mediatek/engineermode/hqanfc/ReceiveThread;

    invoke-virtual {v3, v2}, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->setRunning(Z)V

    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mPollingThr:Lcom/mediatek/engineermode/hqanfc/ReceiveThread;

    invoke-virtual {v3}, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x5

    if-ge v0, v3, :cond_2

    const-wide/16 v3, 0x64

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mPollingThr:Lcom/mediatek/engineermode/hqanfc/ReceiveThread;

    invoke-virtual {v3}, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;->isRunning()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/FilterInputStream;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mInputStream:Ljava/io/DataInputStream;

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v3}, Ljava/io/FilterOutputStream;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mOutputStream:Ljava/io/DataOutputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    if-eqz v3, :cond_5

    :try_start_2
    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    iput-object v6, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    :cond_5
    sput-object v6, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sInstance:Lcom/mediatek/engineermode/hqanfc/NfcClient;

    invoke-virtual {p0, v2}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->setConnected(Z)V

    const-string v2, "EM/HQA/NFC"

    const-string v3, "[NfcClient]close connection success"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "EM/HQA/NFC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[NfcClient]closeConnection IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :catch_2
    move-exception v1

    const-string v3, "EM/HQA/NFC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[NfcClient]closeConnection finally IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public createConnection(Landroid/content/Context;)Z
    .locals 8
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x0

    const-string v4, "EM/HQA/NFC"

    const-string v5, "[NfcClient]createConnection()."

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v3, "EM/HQA/NFC"

    const-string v4, "[NfcClient]createConnection: has connected"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v2

    :cond_0
    :try_start_0
    new-instance v4, Ljava/net/Socket;

    const-string v5, "127.0.0.1"

    const/16 v6, 0x1d4c

    invoke-direct {v4, v5, v6}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    iput-object v4, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    iget-object v4, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    if-nez v4, :cond_1

    move v2, v3

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "EM/HQA/NFC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[NfcClient]createConnection UnknownHostException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v7, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v4, "EM/HQA/NFC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[NfcClient]createConnection IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v7, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    goto :goto_1

    :cond_1
    :try_start_1
    new-instance v4, Ljava/io/DataOutputStream;

    iget-object v5, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v4, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mOutputStream:Ljava/io/DataOutputStream;

    new-instance v4, Ljava/io/DataInputStream;

    iget-object v5, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v4, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mInputStream:Ljava/io/DataInputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    invoke-virtual {p0, v2}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->setConnected(Z)V

    new-instance v3, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;

    iget-object v4, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mInputStream:Ljava/io/DataInputStream;

    invoke-direct {v3, v4, p1}, Lcom/mediatek/engineermode/hqanfc/ReceiveThread;-><init>(Ljava/io/DataInputStream;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mPollingThr:Lcom/mediatek/engineermode/hqanfc/ReceiveThread;

    new-instance v3, Ljava/lang/Thread;

    iget-object v4, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mPollingThr:Lcom/mediatek/engineermode/hqanfc/ReceiveThread;

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mThread:Ljava/lang/Thread;

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v2, "EM/HQA/NFC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[NfcClient]getStream IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_2
    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->close()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    move v2, v3

    goto/16 :goto_0

    :catch_3
    move-exception v1

    iput-object v7, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    goto :goto_2
.end method

.method public isConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mConnected:Z

    return v0
.end method

.method public declared-synchronized sendCommand(ILcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;)I
    .locals 8
    .param p1    # I
    .param p2    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;

    const/4 v4, -0x1

    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mSocket:Ljava/net/Socket;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mOutputStream:Ljava/io/DataOutputStream;

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->isConnected()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    const-string v5, "EM/HQA/NFC"

    const-string v6, "[NfcClient]send command fail"

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move p1, v4

    :goto_0
    monitor-exit p0

    return p1

    :cond_1
    :try_start_1
    const-string v5, "EM/HQA/NFC"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[NfcClient]Send command type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    if-nez p2, :cond_2

    const/16 v5, 0x8

    :try_start_2
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {p1}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$DataConvert;->intToLH(I)[B

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    const/4 v5, 0x0

    invoke-static {v5}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$DataConvert;->intToLH(I)[B

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    :goto_1
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    array-length v5, v5

    if-ge v2, v5, :cond_3

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    aget-byte v7, v7, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p2}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;->getContentSize()I

    move-result v5

    add-int/lit8 v5, v5, 0x8

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {p1}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$DataConvert;->intToLH(I)[B

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    invoke-virtual {p2}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;->getContentSize()I

    move-result v5

    add-int/lit8 v3, v5, 0x8

    const-string v5, "EM/HQA/NFC"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[NfcClient]Send command lenght: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$DataConvert;->intToLH(I)[B

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    invoke-virtual {p2, v0}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;->writeRaw(Ljava/nio/ByteBuffer;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_3
    const-string v5, "EM/HQA/NFC"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[NfcClient]sendCommand IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move p1, v4

    goto/16 :goto_0

    :cond_3
    :try_start_4
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "done send"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/OutputStream;->write([B)V

    iget-object v5, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public setConnected(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/hqanfc/NfcClient;->mConnected:Z

    return-void
.end method
