.class public Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;
.super Landroid/app/Activity;
.source "PollingLoopMode.java"


# static fields
.field private static final CHECKBOXS_NUMBER:I = 0x2a

.field private static final CHECKBOX_CARD_MODE:I = 0x21

.field private static final CHECKBOX_CARD_SWIO1:I = 0x22

.field private static final CHECKBOX_CARD_SWIO2:I = 0x23

.field private static final CHECKBOX_CARD_SWIOSE:I = 0x24

.field private static final CHECKBOX_CARD_TYPEA:I = 0x25

.field private static final CHECKBOX_CARD_TYPEB:I = 0x26

.field private static final CHECKBOX_CARD_TYPEB2:I = 0x27

.field private static final CHECKBOX_CARD_TYPEF:I = 0x28

.field private static final CHECKBOX_CARD_VITRUAL_CARD:I = 0x29

.field private static final CHECKBOX_P2P_ACTIVE_MODE:I = 0x17

.field private static final CHECKBOX_P2P_DISABLE_CARD:I = 0x1a

.field private static final CHECKBOX_P2P_INITIATOR:I = 0x18

.field private static final CHECKBOX_P2P_MODE:I = 0x13

.field private static final CHECKBOX_P2P_PASSIVE_MODE:I = 0x16

.field private static final CHECKBOX_P2P_TARGET:I = 0x19

.field private static final CHECKBOX_P2P_TYPEA:I = 0x14

.field private static final CHECKBOX_P2P_TYPEA_106:I = 0x1b

.field private static final CHECKBOX_P2P_TYPEA_212:I = 0x1c

.field private static final CHECKBOX_P2P_TYPEA_424:I = 0x1d

.field private static final CHECKBOX_P2P_TYPEA_848:I = 0x1e

.field private static final CHECKBOX_P2P_TYPEF:I = 0x15

.field private static final CHECKBOX_P2P_TYPEF_212:I = 0x1f

.field private static final CHECKBOX_P2P_TYPEF_424:I = 0x20

.field private static final CHECKBOX_READER_KOVIO:I = 0x6

.field private static final CHECKBOX_READER_MODE:I = 0x0

.field private static final CHECKBOX_READER_TYPEA:I = 0x1

.field private static final CHECKBOX_READER_TYPEA_106:I = 0x7

.field private static final CHECKBOX_READER_TYPEA_212:I = 0x8

.field private static final CHECKBOX_READER_TYPEA_424:I = 0x9

.field private static final CHECKBOX_READER_TYPEA_848:I = 0xa

.field private static final CHECKBOX_READER_TYPEB:I = 0x2

.field private static final CHECKBOX_READER_TYPEB2:I = 0x5

.field private static final CHECKBOX_READER_TYPEB_106:I = 0xb

.field private static final CHECKBOX_READER_TYPEB_212:I = 0xc

.field private static final CHECKBOX_READER_TYPEB_424:I = 0xd

.field private static final CHECKBOX_READER_TYPEB_848:I = 0xe

.field private static final CHECKBOX_READER_TYPEF:I = 0x3

.field private static final CHECKBOX_READER_TYPEF_212:I = 0xf

.field private static final CHECKBOX_READER_TYPEF_424:I = 0x10

.field private static final CHECKBOX_READER_TYPEV:I = 0x4

.field private static final CHECKBOX_READER_TYPEV_166:I = 0x11

.field private static final CHECKBOX_READER_TYPEV_2648:I = 0x12

.field private static final DIALOG_ID_RESULT:I = 0x0

.field private static final DIALOG_ID_WAIT:I = 0x1

.field private static final HANDLER_MSG_GET_NTF:I = 0x64

.field private static final HANDLER_MSG_GET_RSP:I = 0xc8


# instance fields
.field private mBtnClearAll:Landroid/widget/Button;

.field private mBtnReturn:Landroid/widget/Button;

.field private mBtnRunInBack:Landroid/widget/Button;

.field private mBtnSelectAll:Landroid/widget/Button;

.field private mBtnStart:Landroid/widget/Button;

.field private final mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private mEnableBackKey:Z

.field private final mHandler:Landroid/os/Handler;

.field private mNtfContent:Ljava/lang/String;

.field private mPollingNty:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingNty;

.field private mPollingRsp:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingRsp;

.field private mRbPollingSelectListen:Landroid/widget/RadioButton;

.field private mRbPollingSelectPause:Landroid/widget/RadioButton;

.field private mRbTypeVDualSubcarrier:Landroid/widget/RadioButton;

.field private mRbTypeVSubcarrier:Landroid/widget/RadioButton;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRgPollingSelect:Landroid/widget/RadioGroup;

.field private mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

.field private mRspArray:[B

.field private mSettingsCkBoxs:[Landroid/widget/CheckBox;

.field private mTvPeriod:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x2a

    new-array v0, v0, [Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mEnableBackKey:Z

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode$1;-><init>(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode$2;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode$2;-><init>(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode$3;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode$3;-><init>(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode$4;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode$4;-><init>(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)[B
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRspArray:[B

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;[B)[B
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;
    .param p1    # [B

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRspArray:[B

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingRsp;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mPollingRsp:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingRsp;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->doTestAction(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingRsp;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingRsp;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;
    .param p1    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingRsp;

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mPollingRsp:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingRsp;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnSelectAll:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->changeAllSelect(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnClearAll:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnReturn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnRunInBack:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingNty;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mPollingNty:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingNty;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingNty;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingNty;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;
    .param p1    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingNty;

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mPollingNty:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingNty;

    return-object p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->setButtonsStatus(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)[Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbTypeVSubcarrier:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbTypeVDualSubcarrier:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;

    invoke-direct {p0}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->checkRoleSelect()Z

    move-result v0

    return v0
.end method

.method private changeAllSelect(Z)V
    .locals 4
    .param p1    # Z

    const-string v1, "EM/HQA/NFC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[PollingLoopMode]changeDisplay status is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

    const v2, 0x7f0b021b

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->check(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRgPollingSelect:Landroid/widget/RadioGroup;

    const v2, 0x7f0b0208

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->check(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

    const v2, 0x7f0b021a

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->check(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRgPollingSelect:Landroid/widget/RadioGroup;

    const v2, 0x7f0b0207

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->check(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbPollingSelectListen:Landroid/widget/RadioButton;

    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbPollingSelectPause:Landroid/widget/RadioButton;

    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbTypeVDualSubcarrier:Landroid/widget/RadioButton;

    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbTypeVSubcarrier:Landroid/widget/RadioButton;

    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_1
.end method

.method private checkRoleSelect()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x18

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x19

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x16

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x17

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method private doTestAction(Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->sendCommand(Ljava/lang/Boolean;)V

    return-void
.end method

.method private fillRequest(Ljava/lang/Boolean;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;)V
    .locals 15
    .param p1    # Ljava/lang/Boolean;
    .param p2    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;

    if-nez p1, :cond_0

    const/4 v12, 0x2

    move-object/from16 v0, p2

    iput v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mAction:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mP2pmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    const/4 v13, 0x2

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mAction:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mReadermReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    const/4 v13, 0x2

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mAction:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mCardmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;

    const/4 v13, 0x2

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mAction:I

    :goto_0
    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbPollingSelectListen:Landroid/widget/RadioButton;

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v12, 0x0

    :goto_1
    move-object/from16 v0, p2

    iput v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mPhase:I

    :try_start_0
    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mTvPeriod:Landroid/widget/EditText;

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    move-object/from16 v0, p2

    iput v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mPeriod:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const/4 v12, 0x3

    new-array v3, v12, [Landroid/widget/CheckBox;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    aput-object v13, v3, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x21

    aget-object v13, v13, v14

    aput-object v13, v3, v12

    const/4 v12, 0x2

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x13

    aget-object v13, v13, v14

    aput-object v13, v3, v12

    invoke-static {v3}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getFunctionValue([Landroid/widget/CheckBox;)I

    move-result v12

    move-object/from16 v0, p2

    iput v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mEnableFunc:I

    const/4 v4, 0x0

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x14

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_3

    const/4 v12, 0x1

    :goto_3
    or-int/2addr v4, v12

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x15

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_4

    const/4 v12, 0x4

    :goto_4
    or-int/2addr v4, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mP2pmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    iput v4, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mSupportType:I

    const/4 v12, 0x4

    new-array v9, v12, [Landroid/widget/CheckBox;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x1b

    aget-object v13, v13, v14

    aput-object v13, v9, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x1c

    aget-object v13, v13, v14

    aput-object v13, v9, v12

    const/4 v12, 0x2

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x1d

    aget-object v13, v13, v14

    aput-object v13, v9, v12

    const/4 v12, 0x3

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x1e

    aget-object v13, v13, v14

    aput-object v13, v9, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mP2pmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    invoke-static {v9}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeAbDataRateValue([Landroid/widget/CheckBox;)I

    move-result v13

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mTypeADataRate:I

    const/4 v12, 0x2

    new-array v11, v12, [Landroid/widget/CheckBox;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x1f

    aget-object v13, v13, v14

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x20

    aget-object v13, v13, v14

    aput-object v13, v11, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mP2pmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    invoke-static {v11}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeFDataRateValue([Landroid/widget/CheckBox;)I

    move-result v13

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mTypeFDataRate:I

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mP2pmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x1a

    aget-object v12, v12, v14

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_5

    const/4 v12, 0x1

    :goto_5
    iput v12, v13, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mIsDisableCardM:I

    const/4 v4, 0x0

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x18

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_6

    const/4 v12, 0x1

    :goto_6
    or-int/2addr v4, v12

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x19

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_7

    const/4 v12, 0x2

    :goto_7
    or-int/2addr v4, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mP2pmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    iput v4, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mRole:I

    const/4 v4, 0x0

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x16

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_8

    const/4 v12, 0x1

    :goto_8
    or-int/2addr v4, v12

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x17

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_9

    const/4 v12, 0x2

    :goto_9
    or-int/2addr v4, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mP2pmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    iput v4, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mMode:I

    const/4 v12, 0x6

    new-array v10, v12, [Landroid/widget/CheckBox;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v14, 0x1

    aget-object v13, v13, v14

    aput-object v13, v10, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v14, 0x2

    aget-object v13, v13, v14

    aput-object v13, v10, v12

    const/4 v12, 0x2

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v14, 0x3

    aget-object v13, v13, v14

    aput-object v13, v10, v12

    const/4 v12, 0x3

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v14, 0x4

    aget-object v13, v13, v14

    aput-object v13, v10, v12

    const/4 v12, 0x4

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v14, 0x5

    aget-object v13, v13, v14

    aput-object v13, v10, v12

    const/4 v12, 0x5

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v14, 0x6

    aget-object v13, v13, v14

    aput-object v13, v10, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mReadermReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    invoke-static {v10}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeValue([Landroid/widget/CheckBox;)I

    move-result v13

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mSupportType:I

    const/4 v12, 0x4

    new-array v5, v12, [Landroid/widget/CheckBox;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v14, 0x7

    aget-object v13, v13, v14

    aput-object v13, v5, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x8

    aget-object v13, v13, v14

    aput-object v13, v5, v12

    const/4 v12, 0x2

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x9

    aget-object v13, v13, v14

    aput-object v13, v5, v12

    const/4 v12, 0x3

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0xa

    aget-object v13, v13, v14

    aput-object v13, v5, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mReadermReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    invoke-static {v5}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeAbDataRateValue([Landroid/widget/CheckBox;)I

    move-result v13

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeADataRate:I

    const/4 v12, 0x4

    new-array v6, v12, [Landroid/widget/CheckBox;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0xb

    aget-object v13, v13, v14

    aput-object v13, v6, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0xc

    aget-object v13, v13, v14

    aput-object v13, v6, v12

    const/4 v12, 0x2

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0xd

    aget-object v13, v13, v14

    aput-object v13, v6, v12

    const/4 v12, 0x3

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0xe

    aget-object v13, v13, v14

    aput-object v13, v6, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mReadermReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    invoke-static {v6}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeAbDataRateValue([Landroid/widget/CheckBox;)I

    move-result v13

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeBDataRate:I

    const/4 v12, 0x2

    new-array v7, v12, [Landroid/widget/CheckBox;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0xf

    aget-object v13, v13, v14

    aput-object v13, v7, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x10

    aget-object v13, v13, v14

    aput-object v13, v7, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mReadermReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    invoke-static {v7}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeFDataRateValue([Landroid/widget/CheckBox;)I

    move-result v13

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeFDataRate:I

    const/4 v12, 0x2

    new-array v8, v12, [Landroid/widget/CheckBox;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x11

    aget-object v13, v13, v14

    aput-object v13, v8, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x12

    aget-object v13, v13, v14

    aput-object v13, v8, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mReadermReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    invoke-static {v8}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeVDataRateValue([Landroid/widget/CheckBox;)I

    move-result v13

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeVDataRate:I

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mReadermReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbTypeVSubcarrier:Landroid/widget/RadioButton;

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_a

    const/4 v12, 0x0

    :goto_a
    iput v12, v13, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mTypeVSubcarrier:I

    const/4 v1, 0x0

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x25

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_b

    const/4 v12, 0x1

    :goto_b
    or-int/2addr v1, v12

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x26

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_c

    const/4 v12, 0x2

    :goto_c
    or-int/2addr v1, v12

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x28

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_d

    const/4 v12, 0x4

    :goto_d
    or-int/2addr v1, v12

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x27

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_e

    const/16 v12, 0x10

    :goto_e
    or-int/2addr v1, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mCardmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;

    iput v1, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mSupportType:I

    const/4 v1, 0x0

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x22

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_f

    const/4 v12, 0x1

    :goto_f
    or-int/2addr v1, v12

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x23

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_10

    const/4 v12, 0x2

    :goto_10
    or-int/2addr v1, v12

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v13, 0x24

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_11

    const/4 v12, 0x4

    :goto_11
    or-int/2addr v1, v12

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mCardmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;

    iput v1, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mSwNum:I

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mCardmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;

    iget-object v12, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v14, 0x29

    aget-object v12, v12, v14

    invoke-virtual {v12}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_12

    const/4 v12, 0x1

    :goto_12
    iput v12, v13, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mFgVirtualCard:I

    return-void

    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    if-eqz v12, :cond_1

    const/4 v12, 0x0

    move-object/from16 v0, p2

    iput v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mAction:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mP2pmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    const/4 v13, 0x0

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mAction:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mReadermReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    const/4 v13, 0x0

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mAction:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mCardmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;

    const/4 v13, 0x0

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mAction:I

    goto/16 :goto_0

    :cond_1
    const/4 v12, 0x1

    move-object/from16 v0, p2

    iput v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mAction:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mP2pmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    const/4 v13, 0x1

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mAction:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mReadermReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;

    const/4 v13, 0x1

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsReadermReq;->mAction:I

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;->mCardmReq:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;

    const/4 v13, 0x1

    iput v13, v12, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mAction:I

    goto/16 :goto_0

    :cond_2
    const/4 v12, 0x1

    goto/16 :goto_1

    :catch_0
    move-exception v2

    const-string v12, "Please input the right Period."

    const/4 v13, 0x0

    invoke-static {p0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :cond_3
    const/4 v12, 0x0

    goto/16 :goto_3

    :cond_4
    const/4 v12, 0x0

    goto/16 :goto_4

    :cond_5
    const/4 v12, 0x0

    goto/16 :goto_5

    :cond_6
    const/4 v12, 0x0

    goto/16 :goto_6

    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_7

    :cond_8
    const/4 v12, 0x0

    goto/16 :goto_8

    :cond_9
    const/4 v12, 0x0

    goto/16 :goto_9

    :cond_a
    const/4 v12, 0x1

    goto/16 :goto_a

    :cond_b
    const/4 v12, 0x0

    goto/16 :goto_b

    :cond_c
    const/4 v12, 0x0

    goto/16 :goto_c

    :cond_d
    const/4 v12, 0x0

    goto/16 :goto_d

    :cond_e
    const/4 v12, 0x0

    goto/16 :goto_e

    :cond_f
    const/4 v12, 0x0

    goto/16 :goto_f

    :cond_10
    const/4 v12, 0x0

    goto/16 :goto_10

    :cond_11
    const/4 v12, 0x0

    goto/16 :goto_11

    :cond_12
    const/4 v12, 0x0

    goto :goto_12
.end method

.method private initComponents()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "EM/HQA/NFC"

    const-string v1, "[PollingLoopMode]initComponents"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0b0206

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRgPollingSelect:Landroid/widget/RadioGroup;

    const v0, 0x7f0b0207

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbPollingSelectListen:Landroid/widget/RadioButton;

    const v0, 0x7f0b0208

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbPollingSelectPause:Landroid/widget/RadioButton;

    const v0, 0x7f0b0209

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mTvPeriod:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b020a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v3

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v3

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b020b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x7

    const v0, 0x7f0b020c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x8

    const v0, 0x7f0b020d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x9

    const v0, 0x7f0b020e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xa

    const v0, 0x7f0b020f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b0210

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v4

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v4

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xb

    const v0, 0x7f0b0211

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xc

    const v0, 0x7f0b0212

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xd

    const v0, 0x7f0b0213

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xe

    const v0, 0x7f0b0214

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b0215

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v5

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v5

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xf

    const v0, 0x7f0b0216

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x10

    const v0, 0x7f0b0217

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b0218

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v6

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x11

    const v0, 0x7f0b021c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x12

    const v0, 0x7f0b021d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x5

    const v0, 0x7f0b021e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x6

    const v0, 0x7f0b021f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    const v0, 0x7f0b0219

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

    const v0, 0x7f0b021a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbTypeVSubcarrier:Landroid/widget/RadioButton;

    const v0, 0x7f0b021b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRbTypeVDualSubcarrier:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x13

    const v0, 0x7f0b0220

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v1, 0x13

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x14

    const v0, 0x7f0b0221

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v1, 0x14

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x1b

    const v0, 0x7f0b0222

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x1c

    const v0, 0x7f0b0223

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x1d

    const v0, 0x7f0b0224

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x1e

    const v0, 0x7f0b0225

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x15

    const v0, 0x7f0b0226

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v1, 0x15

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x1f

    const v0, 0x7f0b0227

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x20

    const v0, 0x7f0b0228

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x16

    const v0, 0x7f0b0229

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x17

    const v0, 0x7f0b022a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x18

    const v0, 0x7f0b022b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x19

    const v0, 0x7f0b022c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x1a

    const v0, 0x7f0b022d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x21

    const v0, 0x7f0b022e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v1, 0x21

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x22

    const v0, 0x7f0b022f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x23

    const v0, 0x7f0b0230

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x24

    const v0, 0x7f0b0231

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x25

    const v0, 0x7f0b0232

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x26

    const v0, 0x7f0b0233

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x27

    const v0, 0x7f0b0234

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x28

    const v0, 0x7f0b0235

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x29

    const v0, 0x7f0b0236

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    const v0, 0x7f0b0237

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnSelectAll:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnSelectAll:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0238

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnClearAll:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnClearAll:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0239

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnStart:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnStart:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b023a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnReturn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnReturn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b023b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnRunInBack:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnRunInBack:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnRunInBack:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRgTypeVRadioGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f0b021a

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mRgPollingSelect:Landroid/widget/RadioGroup;

    const v1, 0x7f0b0207

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mTvPeriod:Landroid/widget/EditText;

    const-string v1, "500"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mTvPeriod:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mTvPeriod:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    return-void
.end method

.method private sendCommand(Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;

    invoke-direct {v0}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->fillRequest(Ljava/lang/Boolean;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmPollingReq;)V

    invoke-static {}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->getInstance()Lcom/mediatek/engineermode/hqanfc/NfcClient;

    move-result-object v1

    const/16 v2, 0x6d

    invoke-virtual {v1, v2, v0}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sendCommand(ILcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;)I

    return-void
.end method

.method private setButtonsStatus(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08048e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnRunInBack:Landroid/widget/Button;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iput-boolean p1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mEnableBackKey:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnReturn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnSelectAll:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnClearAll:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08048f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mEnableBackKey:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030041

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->initComponents()V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->changeAllSelect(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x29

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.mediatek.hqanfc.110"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mediatek.hqanfc.117"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-ne v2, p1, :cond_1

    const/4 v1, 0x0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0804ba

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    if-nez p1, :cond_0

    const/4 v0, 0x0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0804bd

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mNtfContent:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PollingLoopMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
