.class public Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;
.super Landroid/app/Activity;
.source "CardEmulationMode.java"


# static fields
.field private static final DIALOG_ID_WAIT:I = 0x0

.field private static final HANDLER_MSG_GET_RSP:I = 0xc8


# instance fields
.field private mBtnClearAll:Landroid/widget/Button;

.field private mBtnReturn:Landroid/widget/Button;

.field private mBtnRunInBack:Landroid/widget/Button;

.field private mBtnSelectAll:Landroid/widget/Button;

.field private mBtnStart:Landroid/widget/Button;

.field private mCbSwio1:Landroid/widget/CheckBox;

.field private mCbSwio2:Landroid/widget/CheckBox;

.field private mCbSwioSe:Landroid/widget/CheckBox;

.field private mCbTypeA:Landroid/widget/CheckBox;

.field private mCbTypeB:Landroid/widget/CheckBox;

.field private mCbTypeB2:Landroid/widget/CheckBox;

.field private mCbTypeF:Landroid/widget/CheckBox;

.field private mCbVirtualCardFunct:Landroid/widget/CheckBox;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private mEnableBackKey:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mResponse:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmRsp;

.field private mRspArray:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mEnableBackKey:Z

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode$1;-><init>(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode$2;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode$2;-><init>(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode$3;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode$3;-><init>(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)[B
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mRspArray:[B

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;[B)[B
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;
    .param p1    # [B

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mRspArray:[B

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmRsp;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mResponse:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmRsp;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnRunInBack:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmRsp;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmRsp;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;
    .param p1    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmRsp;

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mResponse:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmRsp;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->setButtonsStatus(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->doTestAction(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnSelectAll:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->changeAllSelect(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnClearAll:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnReturn:Landroid/widget/Button;

    return-object v0
.end method

.method private changeAllSelect(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "EM/HQA/NFC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeDisplay status is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeA:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeB:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeB2:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeF:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbSwio1:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbSwio2:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbSwioSe:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbVirtualCardFunct:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method

.method private doTestAction(Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->sendCommand(Ljava/lang/Boolean;)V

    return-void
.end method

.method private fillRequest(Ljava/lang/Boolean;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;)V
    .locals 6
    .param p1    # Ljava/lang/Boolean;
    .param p2    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_0

    iput v4, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mAction:I

    :goto_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeA:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeB:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v4

    :goto_2
    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeF:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v5

    :goto_3
    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeB2:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v1, 0x10

    :goto_4
    or-int/2addr v0, v1

    iput v0, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mSupportType:I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbSwio1:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    :goto_5
    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbSwio2:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_7

    :goto_6
    or-int/2addr v0, v4

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbSwioSe:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_8

    :goto_7
    or-int/2addr v0, v5

    iput v0, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mSwNum:I

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbVirtualCardFunct:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_9

    :goto_8
    iput v2, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mFgVirtualCard:I

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iput v3, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mAction:I

    goto :goto_0

    :cond_1
    iput v2, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;->mAction:I

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_4

    :cond_6
    move v1, v3

    goto :goto_5

    :cond_7
    move v4, v3

    goto :goto_6

    :cond_8
    move v5, v3

    goto :goto_7

    :cond_9
    move v2, v3

    goto :goto_8
.end method

.method private initComponents()V
    .locals 2

    const-string v0, "EM/HQA/NFC"

    const-string v1, "initComponents"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0b01cb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeA:Landroid/widget/CheckBox;

    const v0, 0x7f0b01cc

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeB:Landroid/widget/CheckBox;

    const v0, 0x7f0b01cd

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeB2:Landroid/widget/CheckBox;

    const v0, 0x7f0b01ce

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbTypeF:Landroid/widget/CheckBox;

    const v0, 0x7f0b01c8

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbSwio1:Landroid/widget/CheckBox;

    const v0, 0x7f0b01c9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbSwio2:Landroid/widget/CheckBox;

    const v0, 0x7f0b01ca

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbSwioSe:Landroid/widget/CheckBox;

    const v0, 0x7f0b01cf

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbVirtualCardFunct:Landroid/widget/CheckBox;

    const v0, 0x7f0b01d0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnSelectAll:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnSelectAll:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b01d1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnClearAll:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnClearAll:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b01d2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnStart:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnStart:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b01d3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnReturn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnReturn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b01d4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnRunInBack:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnRunInBack:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnRunInBack:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method private sendCommand(Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;

    invoke-direct {v0}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->fillRequest(Ljava/lang/Boolean;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsCardmReq;)V

    invoke-static {}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->getInstance()Lcom/mediatek/engineermode/hqanfc/NfcClient;

    move-result-object v1

    const/16 v2, 0x6b

    invoke-virtual {v1, v2, v0}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sendCommand(ILcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;)I

    return-void
.end method

.method private setButtonsStatus(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08048e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnRunInBack:Landroid/widget/Button;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iput-boolean p1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mEnableBackKey:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnReturn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnSelectAll:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnClearAll:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08048f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mEnableBackKey:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f03003c

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->initComponents()V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->changeAllSelect(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mCbVirtualCardFunct:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.mediatek.hqanfc.108"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0804ba

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/CardEmulationMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
