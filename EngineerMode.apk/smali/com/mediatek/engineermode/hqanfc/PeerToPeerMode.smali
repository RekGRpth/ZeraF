.class public Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;
.super Landroid/app/Activity;
.source "PeerToPeerMode.java"


# static fields
.field private static final CHECKBOX_ACTIVE_MODE:I = 0x9

.field private static final CHECKBOX_DISABLE_CARD:I = 0xc

.field private static final CHECKBOX_INITIATOR:I = 0xa

.field private static final CHECKBOX_NUMBER:I = 0xd

.field private static final CHECKBOX_PASSIVE_MODE:I = 0x8

.field private static final CHECKBOX_TARGET:I = 0xb

.field private static final CHECKBOX_TYPEA:I = 0x0

.field private static final CHECKBOX_TYPEA_106:I = 0x1

.field private static final CHECKBOX_TYPEA_212:I = 0x2

.field private static final CHECKBOX_TYPEA_424:I = 0x3

.field private static final CHECKBOX_TYPEA_848:I = 0x4

.field private static final CHECKBOX_TYPEF:I = 0x5

.field private static final CHECKBOX_TYPEF_212:I = 0x6

.field private static final CHECKBOX_TYPEF_424:I = 0x7

.field private static final DIALOG_ID_RESULT:I = 0x0

.field private static final DIALOG_ID_WAIT:I = 0x1

.field private static final HANDLER_MSG_GET_NTF:I = 0xc9

.field private static final HANDLER_MSG_GET_RSP:I = 0xc8

.field protected static final KEY_P2P_RSP_ARRAY:Ljava/lang/String; = "p2p_rsp_array"


# instance fields
.field private mBtnClearAll:Landroid/widget/Button;

.field private mBtnReturn:Landroid/widget/Button;

.field private mBtnRunInBack:Landroid/widget/Button;

.field private mBtnSelectAll:Landroid/widget/Button;

.field private mBtnStart:Landroid/widget/Button;

.field private final mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private mEnableBackKey:Z

.field private final mHandler:Landroid/os/Handler;

.field private mNtfContent:Ljava/lang/String;

.field private mP2pNtf:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pNtf;

.field private mP2pRsp:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pRsp;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRspArray:[B

.field private mSettingsCkBoxs:[Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0xd

    new-array v0, v0, [Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mEnableBackKey:Z

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode$1;-><init>(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode$2;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode$2;-><init>(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode$3;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode$3;-><init>(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode$4;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode$4;-><init>(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)[B
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mRspArray:[B

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;[B)[B
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;
    .param p1    # [B

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mRspArray:[B

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pNtf;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mP2pNtf:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pNtf;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->changeAllSelect(Z)V

    return-void
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pNtf;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pNtf;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;
    .param p1    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pNtf;

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mP2pNtf:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pNtf;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnClearAll:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnReturn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnRunInBack:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pRsp;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mP2pRsp:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pRsp;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pRsp;)Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pRsp;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;
    .param p1    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pRsp;

    iput-object p1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mP2pRsp:Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pRsp;

    return-object p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->setButtonsStatus(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)[Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    invoke-direct {p0}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->checkRoleSelect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->doTestAction(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnSelectAll:Landroid/widget/Button;

    return-object v0
.end method

.method private changeAllSelect(Z)V
    .locals 4
    .param p1    # Z

    const-string v1, "EM/HQA/NFC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[PeerToPeerMode]changeAllSelect status is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private checkRoleSelect()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method private doTestAction(Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->sendCommand(Ljava/lang/Boolean;)V

    return-void
.end method

.method private fillRequest(Ljava/lang/Boolean;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;)V
    .locals 10
    .param p1    # Ljava/lang/Boolean;
    .param p2    # Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    const/4 v9, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_0

    iput v7, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mAction:I

    :goto_0
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v3, v3, v5

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    :goto_1
    or-int/2addr v0, v3

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v8, 0x5

    aget-object v3, v3, v8

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v6

    :goto_2
    or-int/2addr v0, v3

    iput v0, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mSupportType:I

    new-array v1, v6, [Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v3, v3, v4

    aput-object v3, v1, v5

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v3, v3, v7

    aput-object v3, v1, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v3, v3, v9

    aput-object v3, v1, v7

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v3, v3, v6

    aput-object v3, v1, v9

    invoke-static {v1}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeAbDataRateValue([Landroid/widget/CheckBox;)I

    move-result v3

    iput v3, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mTypeADataRate:I

    new-array v2, v7, [Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v6, 0x6

    aget-object v3, v3, v6

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v6, 0x7

    aget-object v3, v3, v6

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/mediatek/engineermode/hqanfc/NfcCommand$BitMapValue;->getTypeFDataRateValue([Landroid/widget/CheckBox;)I

    move-result v3

    iput v3, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mTypeFDataRate:I

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v6, 0xc

    aget-object v3, v3, v6

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    :goto_3
    iput v3, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mIsDisableCardM:I

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v6, 0xa

    aget-object v3, v3, v6

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_5

    move v3, v4

    :goto_4
    or-int/2addr v0, v3

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v6, 0xb

    aget-object v3, v3, v6

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v7

    :goto_5
    or-int/2addr v0, v3

    iput v0, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mRole:I

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v6, 0x8

    aget-object v3, v3, v6

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_7

    :goto_6
    or-int/2addr v0, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v4, 0x9

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_8

    :goto_7
    or-int/2addr v0, v7

    iput v0, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mMode:I

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    iput v5, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mAction:I

    goto/16 :goto_0

    :cond_1
    iput v4, p2, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;->mAction:I

    goto/16 :goto_0

    :cond_2
    move v3, v5

    goto/16 :goto_1

    :cond_3
    move v3, v5

    goto/16 :goto_2

    :cond_4
    move v3, v5

    goto :goto_3

    :cond_5
    move v3, v5

    goto :goto_4

    :cond_6
    move v3, v5

    goto :goto_5

    :cond_7
    move v4, v5

    goto :goto_6

    :cond_8
    move v7, v5

    goto :goto_7
.end method

.method private initComponents()V
    .locals 5

    const/4 v4, 0x5

    const/4 v3, 0x0

    const-string v0, "EM/HQA/NFC"

    const-string v1, "[PeerToPeerMode]initComponents"

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b01f1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v3

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v3

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x1

    const v0, 0x7f0b01f2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x2

    const v0, 0x7f0b01f3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x3

    const v0, 0x7f0b01f4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x4

    const v0, 0x7f0b01f5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const v0, 0x7f0b01f6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v4

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    aget-object v0, v0, v4

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x6

    const v0, 0x7f0b01f7

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/4 v2, 0x7

    const v0, 0x7f0b01f8

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x8

    const v0, 0x7f0b01f9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0x9

    const v0, 0x7f0b01fa

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xa

    const v0, 0x7f0b01fb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xb

    const v0, 0x7f0b01fc

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mSettingsCkBoxs:[Landroid/widget/CheckBox;

    const/16 v2, 0xc

    const v0, 0x7f0b01fd

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    aput-object v0, v1, v2

    const v0, 0x7f0b01fe

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnSelectAll:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnSelectAll:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b01ff

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnClearAll:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnClearAll:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0200

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnStart:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnStart:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0201

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnReturn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnReturn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b0202

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnRunInBack:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnRunInBack:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnRunInBack:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method private sendCommand(Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;

    new-instance v0, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;

    invoke-direct {v0}, Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->fillRequest(Ljava/lang/Boolean;Lcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmAlsP2pReq;)V

    invoke-static {}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->getInstance()Lcom/mediatek/engineermode/hqanfc/NfcClient;

    move-result-object v1

    const/16 v2, 0x69

    invoke-virtual {v1, v2, v0}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sendCommand(ILcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;)I

    return-void
.end method

.method private setButtonsStatus(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08048e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnRunInBack:Landroid/widget/Button;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iput-boolean p1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mEnableBackKey:Z

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnReturn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnSelectAll:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnClearAll:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08048f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mEnableBackKey:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f03003f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->initComponents()V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->changeAllSelect(Z)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.mediatek.hqanfc.106"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mediatek.hqanfc.119"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-ne v2, p1, :cond_1

    const/4 v1, 0x0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0804ba

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    if-nez p1, :cond_0

    const/4 v0, 0x0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0804bd

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mNtfContent:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/PeerToPeerMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
