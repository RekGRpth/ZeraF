.class public Lcom/mediatek/engineermode/hqanfc/NfcMainPage;
.super Landroid/preference/PreferenceActivity;
.source "NfcMainPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/hqanfc/NfcMainPage$ConnectServerTask;
    }
.end annotation


# static fields
.field private static final KILL_LIB_COMMAND:Ljava/lang/String; = "kill -9 libmtknfc_mw"

.field private static final START_LIB_COMMAND:Ljava/lang/String; = "./system/xbin/libmtknfc_mw"

.field public static final TAG:Ljava/lang/String; = "EM/HQA/NFC"


# instance fields
.field private mTask:Lcom/mediatek/engineermode/hqanfc/NfcMainPage$ConnectServerTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private closeNFCServiceAtStart()V
    .locals 3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->disable()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EM/HQA/NFC"

    const-string v2, "[NfcMainPage]Nfc service set off."

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "EM/HQA/NFC"

    const-string v2, "[NfcMainPage]Nfc service set off Fail."

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "EM/HQA/NFC"

    const-string v2, "[NfcMainPage]Nfc service is off"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private executeXbinFile(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v1, Lcom/mediatek/engineermode/hqanfc/NfcMainPage$1;

    invoke-direct {v1, p0, p1}, Lcom/mediatek/engineermode/hqanfc/NfcMainPage$1;-><init>(Lcom/mediatek/engineermode/hqanfc/NfcMainPage;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    const-wide/16 v1, 0x1f4

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "EM/HQA/NFC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[NfcMainPage]executeXbinFile InterruptedException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f040004

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    const-string v2, "./system/xbin/libmtknfc_mw"

    const/16 v3, 0x1f4

    invoke-direct {p0, v2, v3}, Lcom/mediatek/engineermode/hqanfc/NfcMainPage;->executeXbinFile(Ljava/lang/String;I)V

    new-instance v2, Lcom/mediatek/engineermode/hqanfc/NfcMainPage$ConnectServerTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/hqanfc/NfcMainPage$ConnectServerTask;-><init>(Lcom/mediatek/engineermode/hqanfc/NfcMainPage;Lcom/mediatek/engineermode/hqanfc/NfcMainPage$1;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/hqanfc/NfcMainPage;->mTask:Lcom/mediatek/engineermode/hqanfc/NfcMainPage$ConnectServerTask;

    iget-object v2, p0, Lcom/mediatek/engineermode/hqanfc/NfcMainPage;->mTask:Lcom/mediatek/engineermode/hqanfc/NfcMainPage$ConnectServerTask;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onDestroy()V
    .locals 3

    const-string v0, "EM/HQA/NFC"

    const-string v1, "[NfcMainPage]Nfc main page onDestroy()."

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->getInstance()Lcom/mediatek/engineermode/hqanfc/NfcClient;

    move-result-object v0

    const/16 v1, 0x78

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->sendCommand(ILcom/mediatek/engineermode/hqanfc/NfcEmReqRsp$NfcEmReq;)I

    invoke-static {}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->getInstance()Lcom/mediatek/engineermode/hqanfc/NfcClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/engineermode/hqanfc/NfcClient;->closeConnection()Z

    iget-object v0, p0, Lcom/mediatek/engineermode/hqanfc/NfcMainPage;->mTask:Lcom/mediatek/engineermode/hqanfc/NfcMainPage$ConnectServerTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    return-void
.end method
