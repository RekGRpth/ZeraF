.class public Lcom/mediatek/engineermode/camera/VideoPreview;
.super Landroid/view/SurfaceView;
.source "VideoPreview.java"


# static fields
.field private static final TILE_SIZE:I = 0x10


# instance fields
.field private mAspectRatio:F

.field private mHight:I

.field private mHorizontalTileSize:I

.field private mVerticalTileSize:I

.field private mWeith:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/16 v1, 0x10

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mAspectRatio:F

    iput v1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHorizontalTileSize:I

    iput v1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mVerticalTileSize:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/16 v1, 0x10

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mAspectRatio:F

    iput v1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHorizontalTileSize:I

    iput v1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mVerticalTileSize:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/16 v1, 0x10

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mAspectRatio:F

    iput v1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHorizontalTileSize:I

    iput v1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mVerticalTileSize:I

    return-void
.end method

.method private roundUpToTile(III)I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    add-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, p2

    mul-int/2addr v0, p2

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getCurrentH()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHight:I

    return v0
.end method

.method public getCurrentW()I
    .locals 1

    iget v0, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mWeith:I

    return v0
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    move v3, v4

    move v1, v2

    if-lez v3, :cond_3

    if-lez v1, :cond_3

    iget v5, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mWeith:I

    if-le v5, v3, :cond_2

    int-to-float v5, v3

    int-to-float v6, v1

    div-float v0, v5, v6

    iget v5, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mAspectRatio:F

    cmpg-float v5, v0, v5

    if-gez v5, :cond_1

    int-to-float v5, v3

    iget v6, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mAspectRatio:F

    div-float/2addr v5, v6

    float-to-int v1, v5

    :cond_0
    :goto_0
    iget v5, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHorizontalTileSize:I

    invoke-direct {p0, v3, v5, v4}, Lcom/mediatek/engineermode/camera/VideoPreview;->roundUpToTile(III)I

    move-result v3

    iget v5, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mVerticalTileSize:I

    invoke-direct {p0, v1, v5, v2}, Lcom/mediatek/engineermode/camera/VideoPreview;->roundUpToTile(III)I

    move-result v1

    :goto_1
    iput v3, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mWeith:I

    iput v1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHight:I

    invoke-virtual {p0, v3, v1}, Landroid/view/View;->setMeasuredDimension(II)V

    :goto_2
    return-void

    :cond_1
    iget v5, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mAspectRatio:F

    cmpl-float v5, v0, v5

    if-lez v5, :cond_0

    int-to-float v5, v1

    iget v6, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mAspectRatio:F

    mul-float/2addr v5, v6

    float-to-int v3, v5

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mWeith:I

    iget v1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHight:I

    goto :goto_1

    :cond_3
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onMeasure(II)V

    goto :goto_2
.end method

.method public setAspectRatio(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mAspectRatio:F

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setAspectRatio(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mWeith:I

    iput p2, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHight:I

    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/mediatek/engineermode/camera/VideoPreview;->setAspectRatio(F)V

    return-void
.end method

.method public setTileSize(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHorizontalTileSize:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mVerticalTileSize:I

    if-eq v0, p2, :cond_1

    :cond_0
    iput p1, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mHorizontalTileSize:I

    iput p2, p0, Lcom/mediatek/engineermode/camera/VideoPreview;->mVerticalTileSize:I

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_1
    return-void
.end method
