.class public Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;
.super Landroid/preference/ListPreference;
.source "NetworkModePreference.java"


# static fields
.field private static final DISPLAY_FIRST_FOUR:I = 0x1

.field private static final DISPLAY_FOUR_LENGTH:I = 0x4

.field private static final DISPLAY_LAST_FOUR:I = 0x2

.field private static final DISPLAY_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "NetworkModePreference"


# instance fields
.field private mColor:I

.field private final mContext:Landroid/content/Context;

.field private final mFlater:Landroid/view/LayoutInflater;

.field private mITelephony:Lcom/android/internal/telephony/ITelephony;

.field private mIsMultipleCards:Z

.field private mName:Ljava/lang/String;

.field private mNumDisplayFormat:I

.field private mSimNum:Ljava/lang/String;

.field private mStatus:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mFlater:Landroid/view/LayoutInflater;

    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mITelephony:Lcom/android/internal/telephony/ITelephony;

    return-void
.end method


# virtual methods
.method public getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2}, Landroid/preference/Preference;->getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iget-boolean v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mIsMultipleCards:Z

    if-eqz v10, :cond_f

    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mFlater:Landroid/view/LayoutInflater;

    const v11, 0x7f03006f

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v10, 0x1020016

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mName:Ljava/lang/String;

    if-eqz v10, :cond_0

    if-eqz v9, :cond_0

    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v10, "NetworkModePreference"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mName is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const v10, 0x1020010

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    if-eqz v8, :cond_2

    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_9

    :cond_1
    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    const v10, 0x7f0b03b4

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    if-eqz v2, :cond_3

    iget v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mStatus:I

    invoke-static {v10}, Lcom/mediatek/engineermode/digitalstandard/Utils;->getStatusResource(I)I

    move-result v3

    const/4 v10, -0x1

    if-ne v3, v10, :cond_a

    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    :goto_1
    const v10, 0x7f0b03b5

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    if-eqz v6, :cond_4

    const/16 v10, 0x8

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    const v10, 0x7f0b03b3

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    if-eqz v7, :cond_5

    iget v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mColor:I

    invoke-static {v10}, Lcom/mediatek/engineermode/digitalstandard/Utils;->getSimColorResource(I)I

    move-result v3

    if-gez v3, :cond_b

    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_5
    :goto_2
    const v10, 0x7f0b03b8

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    if-eqz v1, :cond_6

    const/16 v10, 0x8

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    const v10, 0x7f0b03b6

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    if-eqz v4, :cond_7

    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    if-eqz v10, :cond_7

    iget v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mNumDisplayFormat:I

    packed-switch v10, :pswitch_data_0

    :cond_7
    :goto_3
    iget v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mStatus:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_e

    const/4 v0, 0x0

    :goto_4
    if-eqz v5, :cond_8

    if-eqz v9, :cond_8

    if-eqz v8, :cond_8

    invoke-virtual {v5, v0}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string v10, "NetworkModePreference"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Radio state is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    :goto_5
    return-object v5

    :cond_9
    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v10, "NetworkModePreference"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mSimNum is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    :cond_b
    invoke-virtual {v7, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    :pswitch_0
    const/16 v10, 0x8

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :pswitch_1
    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x4

    if-lt v10, v11, :cond_c

    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x4

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_c
    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :pswitch_2
    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x4

    if-lt v10, v11, :cond_d

    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    iget-object v11, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x4

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_d
    iget-object v10, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_e
    const/4 v0, 0x1

    goto/16 :goto_4

    :cond_f
    invoke-virtual {p0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {p0, v10}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setMultiple(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    iput-boolean p1, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mIsMultipleCards:Z

    iget-boolean v2, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mIsMultipleCards:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v2, "NetworkModePreference"

    const-string v3, "can not get slot 1 sim info"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mIsMultipleCards:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mSimNum:Ljava/lang/String;

    iget v2, v1, Landroid/provider/Telephony$SIMInfo;->mColor:I

    iput v2, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mColor:I

    iget-object v2, v1, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mName:Ljava/lang/String;

    iget v2, v1, Landroid/provider/Telephony$SIMInfo;->mDispalyNumberFormat:I

    iput v2, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mNumDisplayFormat:I

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mITelephony:Lcom/android/internal/telephony/ITelephony;

    iget v3, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-interface {v2, v3}, Lcom/android/internal/telephony/ITelephony;->getSimIndicatorStateGemini(I)I

    move-result v2

    iput v2, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mStatus:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iput v4, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mStatus:I

    const-string v2, "NetworkModePreference"

    const-string v3, "catch RemoteException"

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->mStatus:I

    invoke-virtual {p0}, Landroid/preference/Preference;->notifyChanged()V

    return-void
.end method

.method public updateSummary()V
    .locals 1

    invoke-virtual {p0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
