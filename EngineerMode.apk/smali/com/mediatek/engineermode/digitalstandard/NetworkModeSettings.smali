.class public Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;
.super Landroid/preference/PreferenceActivity;
.source "NetworkModeSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final DIALOG_NETWORK_MODE_CHANGE:I = 0x3f0

.field private static final KEY_NETWORK_MODE_SETTING:Ljava/lang/String; = "gsm_umts_preferred_network_mode_key"

.field public static final NETWORK_MODE_CHANGE_BROADCAST:Ljava/lang/String; = "com.android.phone.NETWORK_MODE_CHANGE"

.field public static final NETWORK_MODE_CHANGE_RESPONSE:Ljava/lang/String; = "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

.field public static final OLD_NETWORK_MODE:Ljava/lang/String; = "com.android.phone.OLD_NETWORK_MODE"

.field private static final PREFERRED_NT_MODE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "NetworkModeSettings"


# instance fields
.field private mGeminiPhone:Lcom/android/internal/telephony/Phone;

.field private mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

.field private final mSimReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    new-instance v0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings$1;-><init>(Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mSimReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;)Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;

    iget-object v0, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f040007

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "gsm_umts_preferred_network_mode_key"

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    iput-object v1, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    iget-object v1, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->setMultiple(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mSimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mGeminiPhone:Lcom/android/internal/telephony/Phone;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x3f0

    if-ne v1, p1, :cond_0

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08027f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mSimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v4, "gsm_umts_preferred_network_mode_key"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "preferred_network_mode"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/engineermode/digitalstandard/Utils;->getNetworkMode(I)I

    move-result v2

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "preferred_network_mode"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    if-eq v2, v3, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.android.phone.NETWORK_MODE_CHANGE"

    const/4 v5, 0x0

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v4, "com.android.phone.OLD_NETWORK_MODE"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.android.phone.NETWORK_MODE_CHANGE"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "simId"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v4, 0x3f0

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v4, "NetworkModeSettings"

    const-string v5, "Send broadcast of com.android.phone.NETWORK_MODE_CHANGE"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v4, 0x1

    return v4
.end method

.method protected onResume()V
    .locals 6

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "preferred_network_mode"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v3, 0x2

    if-gt v0, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mGeminiPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->isSimInsert()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const/4 v3, 0x5

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v4

    if-ne v3, v4, :cond_1

    const/4 v2, 0x1

    :cond_1
    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/NetworkModeSettings;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    return-void
.end method
