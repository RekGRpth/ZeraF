.class public Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;
.super Landroid/preference/PreferenceActivity;
.source "DigitalStandard.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final DIALOG_NETWORK_MODE_CHANGE:I = 0x3f0

.field private static final KEY_GENERAL_SETTINGS_CATEGORY:Ljava/lang/String; = "general_settings"

.field private static final KEY_NETWORK_MODE_SETTING:Ljava/lang/String; = "gsm_umts_preferred_network_mode_key"

.field private static final KEY_NETWORK_MODE_SETTING_GEMINI:Ljava/lang/String; = "gsm_umts_preferred_network_mode_gemini_key"

.field private static final NETMODESUMMARY_LENGTH:I = 0x3

.field private static final NETWORK_MODE_CHANGE_BROADCAST:Ljava/lang/String; = "com.android.phone.NETWORK_MODE_CHANGE"

.field private static final NETWORK_MODE_CHANGE_RESPONSE:Ljava/lang/String; = "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

.field private static final NEW_NETWORK_MODE:Ljava/lang/String; = "NEW_NETWORK_MODE"

.field private static final OLD_NETWORK_MODE:Ljava/lang/String; = "com.android.phone.OLD_NETWORK_MODE"

.field static final PREFERRED_NT_MODE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DigitalStandard"


# instance fields
.field private mAirplaneMode:Z

.field private mGeminiPhone:Lcom/android/internal/telephony/Phone;

.field private mHasNetworkMode:Z

.field private mIsRegister:Z

.field private mIsSlot1Insert:Z

.field private mIsSlot2Insert:Z

.field private mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

.field private mNetworkModeGemini:Landroid/preference/PreferenceScreen;

.field private final mSimReceiver:Landroid/content/BroadcastReceiver;

.field private mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    new-instance v0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard$1;-><init>(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mSimReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mAirplaneMode:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mAirplaneMode:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mHasNetworkMode:Z

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mIsSlot1Insert:Z

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    iget-object v0, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;)Landroid/preference/PreferenceScreen;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;

    iget-object v0, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkModeGemini:Landroid/preference/PreferenceScreen;

    return-object v0
.end method

.method private isCallStateIdle()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v4, v3}, Lcom/mediatek/telephony/TelephonyManagerEx;->getCallState(I)I

    move-result v0

    iget-object v4, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v4, v2}, Lcom/mediatek/telephony/TelephonyManagerEx;->getCallState(I)I

    move-result v1

    const-string v4, "DigitalStandard"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stateSim1 is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " stateSim2 is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f04000a

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mGeminiPhone:Lcom/android/internal/telephony/Phone;

    iput-boolean v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mHasNetworkMode:Z

    iget-boolean v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mHasNetworkMode:Z

    if-nez v2, :cond_0

    const-string v2, "DigitalStandard"

    const-string v3, "PHONE_TYPE_CDMA is needed."

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v2, "general_settings"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    const-string v2, "gsm_umts_preferred_network_mode_key"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    iput-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    const-string v2, "gsm_umts_preferred_network_mode_gemini_key"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceScreen;

    iput-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkModeGemini:Landroid/preference/PreferenceScreen;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "android.intent.action.SIM_SETTING_INFO_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mGeminiPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->isSimInsert()Z

    move-result v2

    iput-boolean v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mIsSlot1Insert:Z

    iget-boolean v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mIsSlot1Insert:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkModeGemini:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v2, "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :goto_1
    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mSimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mIsRegister:Z

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkModeGemini:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x3f0

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08027f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    iget-boolean v0, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mIsRegister:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mSimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v4, "DigitalStandard"

    const-string v5, "Enter onPreferenceChange function."

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v4, "gsm_umts_preferred_network_mode_key"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "preferred_network_mode"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/engineermode/digitalstandard/Utils;->getNetworkMode(I)I

    move-result v2

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "preferred_network_mode"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    if-eq v2, v3, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.android.phone.NETWORK_MODE_CHANGE"

    const/4 v5, 0x0

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v4, "com.android.phone.OLD_NETWORK_MODE"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.android.phone.NETWORK_MODE_CHANGE"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "simId"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v4, 0x3f0

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    const/4 v4, 0x1

    return v4
.end method

.method protected onResume()V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-boolean v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mHasNetworkMode:Z

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->isCallStateIdle()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mIsSlot1Insert:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x5

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v4

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkModeGemini:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    :goto_0
    iget-boolean v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mIsSlot1Insert:Z

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mIsSlot2Insert:Z

    if-eqz v3, :cond_3

    const-string v3, "DigitalStandard"

    const-string v4, "Only support TD single card"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkModeGemini:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "preferred_network_mode"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ltz v1, :cond_1

    const/4 v3, 0x2

    if-gt v1, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/digitalstandard/DigitalStandard;->mNetworkMode:Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;

    invoke-virtual {v3}, Lcom/mediatek/engineermode/digitalstandard/NetworkModePreference;->updateSummary()V

    goto :goto_1
.end method
