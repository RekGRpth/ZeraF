.class public Lcom/mediatek/engineermode/digitalstandard/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field static final MAX_COLOR_VALUE:I = 0x7

.field static final MIN_COLOR_VALUE:I = 0x0

.field static final NETWORK_MODE_CHANGE_BROADCAST:Ljava/lang/String; = "com.android.phone.NETWORK_MODE_CHANGE"

.field static final NETWORK_MODE_CHANGE_RESPONSE:Ljava/lang/String; = "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

.field static final NEW_NETWORK_MODE:Ljava/lang/String; = "NEW_NETWORK_MODE"

.field static final OLD_NETWORK_MODE:Ljava/lang/String; = "com.android.phone.OLD_NETWORK_MODE"

.field static final TYPE_GPRS:I = 0x4

.field static final TYPE_SMS:I = 0x3

.field static final TYPE_VIDEOCALL:I = 0x2

.field static final TYPE_VOICECALL:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNetworkMode(I)I
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    move v0, p0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getSimColorResource(I)I
    .locals 1
    .param p0    # I

    if-ltz p0, :cond_0

    const/4 v0, 0x7

    if-gt p0, v0, :cond_0

    sget-object v0, Landroid/provider/Telephony;->SIMBackgroundRes:[I

    aget v0, v0, p0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static getStatusResource(I)I
    .locals 3
    .param p0    # I

    const-string v0, "DigitalStandard"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Utils gemini!!!!!!!!!!!!!state is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x2020112

    goto :goto_0

    :pswitch_2
    const v0, 0x20200ff

    goto :goto_0

    :pswitch_3
    const v0, 0x20200f8

    goto :goto_0

    :pswitch_4
    const v0, 0x2020119

    goto :goto_0

    :pswitch_5
    const v0, 0x2020117

    goto :goto_0

    :pswitch_6
    const v0, 0x20200ef

    goto :goto_0

    :pswitch_7
    const v0, 0x2020118

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
