.class public Lcom/mediatek/engineermode/networkinfo/NetworkInfo;
.super Landroid/app/Activity;
.source "NetworkInfo.java"


# static fields
.field private static final CHECK_INFOMATION_ID:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String; = "NetworkInfo"

.field public static final MODEM_FDD:I = 0x1

.field public static final MODEM_MASK_EDGE:I = 0x2

.field public static final MODEM_MASK_GPRS:I = 0x1

.field public static final MODEM_MASK_HSDPA:I = 0x10

.field public static final MODEM_MASK_HSUPA:I = 0x20

.field public static final MODEM_MASK_TDSCDMA:I = 0x8

.field public static final MODEM_MASK_WCDMA:I = 0x4

.field public static final MODEM_NO3G:I = 0x3

.field public static final MODEM_NONE:I = -0x1

.field public static final MODEM_TD:I = 0x2

.field public static final TOTAL_ITEM_NUM:I = 0xbe

.field private static sModemType:I


# instance fields
.field private mCheckBox:[Landroid/widget/CheckBox;

.field private mChecked:[I

.field private mSimType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->sModemType:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static getModemType()I
    .locals 3

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->getModemTypeFromSysProperty()I

    move-result v0

    sput v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->sModemType:I

    const-string v0, "NetworkInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "modem type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->sModemType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->sModemType:I

    return v0
.end method

.method private static getModemTypeFromSysProperty()I
    .locals 5

    const-string v4, "gsm.baseband.capability"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x3

    if-nez v3, :cond_0

    const/4 v2, 0x3

    :goto_0
    return v2

    :cond_0
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    and-int/lit8 v4, v1, 0x8

    if-nez v4, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    and-int/lit8 v4, v1, 0x4

    if-nez v4, :cond_2

    const/4 v2, 0x2

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v2, 0x3

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const/4 v11, 0x2

    const/16 v10, 0xbe

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x8

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->getModemTypeFromSysProperty()I

    move-result v5

    sput v5, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->sModemType:I

    const v5, 0x7f030057

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v5, "mSimType"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mSimType:I

    new-array v5, v10, [Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    new-array v5, v10, [I

    iput-object v5, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mChecked:[I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v10, :cond_0

    iget-object v5, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/4 v6, 0x0

    aput-object v6, v5, v0

    iget-object v5, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mChecked:[I

    aput v7, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const v5, 0x7f0b02be

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const v5, 0x7f0b02bf

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v9

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const v5, 0x7f0b02c0

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v11

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/4 v7, 0x3

    const v5, 0x7f0b02c1

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/4 v7, 0x4

    const v5, 0x7f0b02c2

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/4 v7, 0x5

    const v5, 0x7f0b02c3

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/4 v7, 0x6

    const v5, 0x7f0b02c4

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/4 v7, 0x7

    const v5, 0x7f0b02c5

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const v5, 0x7f0b02c6

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v8

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x9

    const v5, 0x7f0b02c7

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0xa

    const v5, 0x7f0b02c8

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0xb

    const v5, 0x7f0b02c9

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0xc

    const v5, 0x7f0b02ca

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0xd

    const v5, 0x7f0b02cb

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x35

    const v5, 0x7f0b02ce

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x3b

    const v5, 0x7f0b02cf

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x4b

    const v5, 0x7f0b02d0

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x4c

    const v5, 0x7f0b02cc

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x51

    const v5, 0x7f0b02d1

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x63

    const v5, 0x7f0b02d2

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x6f

    const v5, 0x7f0b02d3

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x9b

    const v5, 0x7f0b02d4

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    const v5, 0x7f0b02d5

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v5, 0x7f0b02d8

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0b02cd

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget v5, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mSimType:I

    if-eq v5, v9, :cond_1

    iget v5, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mSimType:I

    if-nez v5, :cond_4

    :cond_1
    sget v5, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->sModemType:I

    if-ne v5, v9, :cond_2

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x5a

    const v5, 0x7f0b02d6

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x8c

    const v5, 0x7f0b02d7

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    :goto_1
    return-void

    :cond_2
    sget v5, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->sModemType:I

    if-ne v5, v11, :cond_3

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0x82

    const v5, 0x7f0b02d9

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0xb9

    const v5, 0x7f0b02da

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0xba

    const v5, 0x7f0b02db

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0xbb

    const v5, 0x7f0b02dc

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0xbc

    const v5, 0x7f0b02dd

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    iget-object v6, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    const/16 v7, 0xbd

    const v5, 0x7f0b02de

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    aput-object v5, v6, v7

    goto :goto_1

    :cond_3
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_4
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x1

    const/4 v1, 0x0

    const v0, 0x7f08045c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    return v2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    return v3

    :pswitch_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v0, 0x0

    :goto_1
    const/16 v3, 0xbe

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    aget-object v3, v3, v0

    if-nez v3, :cond_0

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mCheckBox:[Landroid/widget/CheckBox;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mChecked:[I

    aput v4, v3, v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_2

    :cond_1
    iget-object v3, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mChecked:[I

    aput v5, v3, v0

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_3

    const v3, 0x7f08045d

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/mediatek/engineermode/networkinfo/NetworkInfoInfomation;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "mChecked"

    iget-object v4, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mChecked:[I

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string v3, "mSimType"

    iget v4, p0, Lcom/mediatek/engineermode/networkinfo/NetworkInfo;->mSimType:I

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
