.class public Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;
.super Ljava/lang/Object;
.source "AFMFunctionCallEx.java"


# static fields
.field private static final ERROR:Ljava/lang/String; = "ERROR "

.field public static final FUNCTION_EM_BASEBAND:I = 0x2711

.field public static final FUNCTION_EM_CPU_FREQ_TEST_CURRENCT:I = 0x4e23

.field public static final FUNCTION_EM_CPU_FREQ_TEST_START:I = 0x4e21

.field public static final FUNCTION_EM_CPU_FREQ_TEST_STOP:I = 0x4e22

.field public static final FUNCTION_EM_CPU_STRESS_TEST_APMCU:I = 0x9c41

.field public static final FUNCTION_EM_CPU_STRESS_TEST_BACKUP:I = 0x9c43

.field public static final FUNCTION_EM_CPU_STRESS_TEST_SWCODEC:I = 0x9c42

.field public static final FUNCTION_EM_CPU_STRESS_TEST_THERMAL:I = 0x9c44

.field public static final FUNCTION_EM_FB0_IOCTL:I = 0x7531

.field public static final RESULT_CONTINUE:I = 0x1

.field public static final RESULT_FIN:I = 0x0

.field public static final RESULT_IO_ERR:I = -0x1

.field private static final TAG:Ljava/lang/String; = "EM/functioncall"


# instance fields
.field private mSocket:Lcom/mediatek/engineermode/emsvr/Client;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->mSocket:Lcom/mediatek/engineermode/emsvr/Client;

    return-void
.end method

.method private endCallFunction()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->mSocket:Lcom/mediatek/engineermode/emsvr/Client;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/emsvr/Client;->stopClient()V

    return-void
.end method


# virtual methods
.method public getNextResult()Lcom/mediatek/engineermode/emsvr/FunctionReturn;
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Lcom/mediatek/engineermode/emsvr/FunctionReturn;

    invoke-direct {v1}, Lcom/mediatek/engineermode/emsvr/FunctionReturn;-><init>()V

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->mSocket:Lcom/mediatek/engineermode/emsvr/Client;

    invoke-virtual {v2}, Lcom/mediatek/engineermode/emsvr/Client;->read()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    iget-object v2, v1, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput v2, v1, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    invoke-direct {p0}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->endCallFunction()V

    :goto_0
    return-object v1

    :cond_0
    const/4 v2, 0x1

    iput v2, v1, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->endCallFunction()V

    iput v3, v1, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    const-string v2, ""

    iput-object v2, v1, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-direct {p0}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->endCallFunction()V

    const/4 v2, -0x1

    iput v2, v1, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    goto :goto_0
.end method

.method public startCallFunctionStringReturn(I)Z
    .locals 5
    .param p1    # I

    const/4 v1, 0x0

    new-instance v2, Lcom/mediatek/engineermode/emsvr/Client;

    invoke-direct {v2}, Lcom/mediatek/engineermode/emsvr/Client;-><init>()V

    iput-object v2, p0, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->mSocket:Lcom/mediatek/engineermode/emsvr/Client;

    iget-object v2, p0, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->mSocket:Lcom/mediatek/engineermode/emsvr/Client;

    invoke-virtual {v2}, Lcom/mediatek/engineermode/emsvr/Client;->startClient()V

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->mSocket:Lcom/mediatek/engineermode/emsvr/Client;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mediatek/engineermode/emsvr/Client;->writeFunctionNo(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "EM/functioncall"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "StartCallFunctionStringReturnEXP "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public writeParamInt(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->mSocket:Lcom/mediatek/engineermode/emsvr/Client;

    invoke-virtual {v2, p1}, Lcom/mediatek/engineermode/emsvr/Client;->writeParamInt(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public writeParamNo(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->mSocket:Lcom/mediatek/engineermode/emsvr/Client;

    invoke-virtual {v2, p1}, Lcom/mediatek/engineermode/emsvr/Client;->writeParamNo(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public writeParamString(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->mSocket:Lcom/mediatek/engineermode/emsvr/Client;

    invoke-virtual {v2, p1}, Lcom/mediatek/engineermode/emsvr/Client;->writeParamString(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method
