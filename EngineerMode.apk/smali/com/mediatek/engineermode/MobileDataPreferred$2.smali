.class Lcom/mediatek/engineermode/MobileDataPreferred$2;
.super Ljava/lang/Object;
.source "MobileDataPreferred.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/engineermode/MobileDataPreferred;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/MobileDataPreferred;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/MobileDataPreferred;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/MobileDataPreferred$2;->this$0:Lcom/mediatek/engineermode/MobileDataPreferred;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/engineermode/MobileDataPreferred$2;->this$0:Lcom/mediatek/engineermode/MobileDataPreferred;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gprs_transfer_setting"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/MobileDataPreferred$2;->this$0:Lcom/mediatek/engineermode/MobileDataPreferred;

    invoke-static {v1}, Lcom/mediatek/engineermode/MobileDataPreferred;->access$100(Lcom/mediatek/engineermode/MobileDataPreferred;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/MobileDataPreferred$2;->this$0:Lcom/mediatek/engineermode/MobileDataPreferred;

    invoke-static {v1}, Lcom/mediatek/engineermode/MobileDataPreferred;->access$100(Lcom/mediatek/engineermode/MobileDataPreferred;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/android/internal/telephony/ITelephony;->setGprsTransferTypeGemini(II)V

    iget-object v1, p0, Lcom/mediatek/engineermode/MobileDataPreferred$2;->this$0:Lcom/mediatek/engineermode/MobileDataPreferred;

    invoke-static {v1}, Lcom/mediatek/engineermode/MobileDataPreferred;->access$100(Lcom/mediatek/engineermode/MobileDataPreferred;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/android/internal/telephony/ITelephony;->setGprsTransferTypeGemini(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "EM/CallDataPreferred"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
