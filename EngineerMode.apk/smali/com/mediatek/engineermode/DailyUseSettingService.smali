.class public Lcom/mediatek/engineermode/DailyUseSettingService;
.super Landroid/app/Service;
.source "DailyUseSettingService.java"


# static fields
.field private static final DATA_SIZE:I = 0x5a4

.field private static final IPO_DISABLE:I = 0x0

.field private static final IPO_ENABLE:I = 0x1

.field private static final KEY_IVSR:Ljava/lang/String; = "ivsr"

.field private static final KEY_QUICK_BOOT:Ljava/lang/String; = "quick_boot"

.field private static final KEY_VM_LOG:Ljava/lang/String; = "vm_log"

.field private static final SET_SPEECH_VM_ENABLE:I = 0x60

.field private static final SYSTEM_PROP_NAME:Ljava/lang/String; = "ril.em.dailyuse"

.field private static final TAG:Ljava/lang/String; = "EM/SettingService"

.field private static final VALUE_0:Ljava/lang/String; = "0"

.field private static final VALUE_1:Ljava/lang/String; = "1"

.field private static final VM_ENABLE:I = 0x0

.field private static final VM_EPL_ENABLE:I = 0x1

.field private static final VM_LOG_POS:I = 0x5a0


# instance fields
.field private mIvsr:Z

.field private mQuickBoot:Z

.field private mVmLog:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-boolean v0, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mQuickBoot:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mIvsr:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mVmLog:Z

    return-void
.end method

.method private doSettings()V
    .locals 12

    const-string v7, "EM/SettingService"

    const-string v8, "Quick boot: %1$s, IVSR: %2$s, VM Log: %3$s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-boolean v11, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mQuickBoot:Z

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-boolean v11, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mIvsr:Z

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    iget-boolean v11, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mVmLog:Z

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    const/4 v4, 0x1

    const/4 v6, 0x1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "ipo_setting"

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    const/4 v2, 0x1

    :goto_0
    iget-boolean v7, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mQuickBoot:Z

    if-ne v2, v7, :cond_1

    const-string v7, "EM/SettingService"

    const-string v8, "Skip quick boot setting"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "ivsr_setting"

    const-wide/16 v9, 0x0

    invoke-static {v7, v8, v9, v10}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v7

    const-wide/16 v9, 0x1

    cmp-long v7, v7, v9

    if-nez v7, :cond_3

    const/4 v3, 0x1

    :goto_2
    iget-boolean v7, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mIvsr:Z

    if-ne v3, v7, :cond_4

    const-string v7, "EM/SettingService"

    const-string v8, "Skip IVSR setting"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    const/16 v7, 0x5a4

    new-array v0, v7, [B

    const/16 v7, 0x5a4

    invoke-static {v0, v7}, Landroid/media/AudioSystem;->getEmParameter([BI)I

    move-result v5

    if-nez v5, :cond_8

    iget-boolean v7, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mVmLog:Z

    if-eqz v7, :cond_6

    const/16 v7, 0x5a0

    aget-byte v8, v0, v7

    or-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    aput-byte v8, v0, v7

    :goto_4
    const/16 v7, 0x5a4

    invoke-static {v0, v7}, Landroid/media/AudioSystem;->setEmParameter([BI)I

    move-result v5

    if-nez v5, :cond_7

    iget-boolean v7, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mVmLog:Z

    if-eqz v7, :cond_7

    const/16 v7, 0x60

    const/4 v8, 0x0

    invoke-static {v7, v8}, Landroid/media/AudioSystem;->setAudioCommand(II)I

    move-result v5

    :goto_5
    if-nez v5, :cond_9

    const/4 v6, 0x1

    :goto_6
    if-eqz v6, :cond_a

    if-eqz v4, :cond_a

    if-eqz v1, :cond_a

    const-string v7, "Daily Use Setting Succeed"

    :goto_7
    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    const-string v7, "EM/SettingService"

    const-string v8, "Quick boot set: %1$s, IVSR set: %2$s, VM Log set: %3$s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "ril.em.dailyuse"

    if-eqz v6, :cond_b

    if-eqz v4, :cond_b

    if-eqz v1, :cond_b

    const-string v7, "1"

    :goto_8
    invoke-static {v8, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "ipo_setting"

    iget-boolean v7, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mQuickBoot:Z

    if-eqz v7, :cond_2

    const/4 v7, 0x1

    :goto_9
    invoke-static {v8, v9, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v1

    goto/16 :goto_1

    :cond_2
    const/4 v7, 0x0

    goto :goto_9

    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_4
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "ivsr_setting"

    iget-boolean v7, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mIvsr:Z

    if-eqz v7, :cond_5

    const-wide/16 v7, 0x1

    :goto_a
    invoke-static {v9, v10, v7, v8}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    move-result v4

    goto/16 :goto_3

    :cond_5
    const-wide/16 v7, 0x0

    goto :goto_a

    :cond_6
    const/16 v7, 0x5a0

    aget-byte v8, v0, v7

    and-int/lit8 v8, v8, -0x2

    int-to-byte v8, v8

    aput-byte v8, v0, v7

    goto/16 :goto_4

    :cond_7
    const-string v7, "EM/SettingService"

    const-string v8, "Set EM parameter fail"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_8
    const-string v7, "EM/SettingService"

    const-string v8, "Get EM parameter fail"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_6

    :cond_a
    const-string v7, "Daily Use Setting Fail"

    goto/16 :goto_7

    :cond_b
    const-string v7, "0"

    goto :goto_8
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "quick_boot"

    iget-boolean v2, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mQuickBoot:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mQuickBoot:Z

    const-string v1, "ivsr"

    iget-boolean v2, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mIvsr:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mIvsr:Z

    const-string v1, "vm_log"

    iget-boolean v2, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mVmLog:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/mediatek/engineermode/DailyUseSettingService;->mVmLog:Z

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/engineermode/DailyUseSettingService;->doSettings()V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    const/4 v1, 0x2

    return v1
.end method
