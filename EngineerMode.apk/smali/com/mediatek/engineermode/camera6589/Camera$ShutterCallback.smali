.class final Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Landroid/hardware/Camera$ShutterCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera6589/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ShutterCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera6589/Camera;


# direct methods
.method private constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p2    # Lcom/mediatek/engineermode/camera6589/Camera$1;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;)V

    return-void
.end method


# virtual methods
.method public onShutter()V
    .locals 5

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3802(Lcom/mediatek/engineermode/camera6589/Camera;J)J

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3800(Lcom/mediatek/engineermode/camera6589/Camera;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v3}, Lcom/mediatek/engineermode/camera6589/Camera;->access$4000(Lcom/mediatek/engineermode/camera6589/Camera;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3902(Lcom/mediatek/engineermode/camera6589/Camera;J)J

    const-string v0, "test/camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mShutterLag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$ShutterCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$3900(Lcom/mediatek/engineermode/camera6589/Camera;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
