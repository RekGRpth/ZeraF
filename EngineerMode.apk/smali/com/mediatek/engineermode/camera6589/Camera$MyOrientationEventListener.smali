.class Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;
.super Landroid/view/OrientationEventListener;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera6589/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyOrientationEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera6589/Camera;


# direct methods
.method public constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 3
    .param p1    # I

    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$5100(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {p1, v2}, Lcom/mediatek/engineermode/camera6589/Util;->roundOrientation(II)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$5102(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$5100(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v2

    add-int v0, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$5200(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1, v0}, Lcom/mediatek/engineermode/camera6589/Camera;->access$5202(Lcom/mediatek/engineermode/camera6589/Camera;I)I

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/Camera$MyOrientationEventListener;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$5200(Lcom/mediatek/engineermode/camera6589/Camera;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/camera6589/Camera;->access$5300(Lcom/mediatek/engineermode/camera6589/Camera;I)V

    goto :goto_0
.end method
