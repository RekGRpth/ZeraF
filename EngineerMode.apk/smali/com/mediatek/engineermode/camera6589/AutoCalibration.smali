.class public Lcom/mediatek/engineermode/camera6589/AutoCalibration;
.super Landroid/app/Activity;
.source "AutoCalibration.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final DIALOG_ISO_SPEED:I = 0x5

.field private static final HDR_KEY:Ljava/lang/String; = "mediatek.hdr.debug"

.field private static final ISO_STRS_ARRAY:[Ljava/lang/String;

.field public static final PREFERENCE_KEY:Ljava/lang/String; = "camera_inter_settings"

.field private static final TAG:Ljava/lang/String; = "EM/AutoCalibration"


# instance fields
.field private mAfAuto:Landroid/widget/RadioButton;

.field private mAfBracket:Landroid/widget/RadioButton;

.field private mAfBracketInterval:Landroid/widget/Spinner;

.field private mAfBracketLayout:Landroid/widget/LinearLayout;

.field private mAfBracketRange:Landroid/widget/EditText;

.field private mAfFullScan:Landroid/widget/RadioButton;

.field private mAfMode:I

.field private mAfModeStatus:Z

.field private mAfSpecialIso:I

.field private mAfThrough:Landroid/widget/RadioButton;

.field private mAfThroughDirec:Landroid/widget/Spinner;

.field private mAfThroughInterval:Landroid/widget/EditText;

.field private mAfThroughLayout:Landroid/widget/LinearLayout;

.field private mCaptureListView:Landroid/widget/ListView;

.field private mCaptureMode:I

.field private mFlickerSpinner:Landroid/widget/Spinner;

.field private mHdrSpinner:Landroid/widget/Spinner;

.field private mIsoListView:Landroid/widget/ListView;

.field private mIsoValueStr:Ljava/lang/String;

.field private mMulFrameCaptureNum:Landroid/widget/Spinner;

.field private mMulFrameLayout:Landroid/widget/LinearLayout;

.field private mMulFrameMode:Landroid/widget/RadioButton;

.field private mMulISOFlags:[Z

.field private mNormalCaptureLayout:Landroid/widget/LinearLayout;

.field private mNormalCaptureNum:Landroid/widget/Spinner;

.field private mNormalCaptureSize:Landroid/widget/Spinner;

.field private mNormalCaptureType:Landroid/widget/Spinner;

.field private mNormalMode:Landroid/widget/RadioButton;

.field private mRadioListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mStrobeModeSpinner:Landroid/widget/Spinner;

.field private mThroughFocsuEndPos:Landroid/widget/EditText;

.field private mThroughFocsuStartPos:Landroid/widget/EditText;

.field private mThroughFocusEnd:Landroid/widget/LinearLayout;

.field private mThroughFocusStart:Landroid/widget/LinearLayout;

.field private mVideoClipLayout:Landroid/widget/LinearLayout;

.field private mVideoClipResolution:Landroid/widget/Spinner;

.field private mVideoCliplMode:Landroid/widget/RadioButton;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "100"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "150"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "200"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "300"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "400"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "600"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "800"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "1200"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "1600"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "2400"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "3200"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->ISO_STRS_ARRAY:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0xe

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfModeStatus:Z

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfSpecialIso:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureMode:I

    iput v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfMode:I

    new-instance v0, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration$1;-><init>(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mRadioListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalMode:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mVideoCliplMode:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfMode:I

    return v0
.end method

.method static synthetic access$1002(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfMode:I

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[I[Landroid/widget/RadioButton;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # [I
    .param p2    # [Landroid/widget/RadioButton;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->setAfLayout([I[Landroid/widget/RadioButton;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfModeStatus:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfModeStatus:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->statusChangesByAf(Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)[Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfSpecialIso:I

    return v0
.end method

.method static synthetic access$1502(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfSpecialIso:I

    return p1
.end method

.method static synthetic access$1600(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mIsoValueStr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mIsoValueStr:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->ISO_STRS_ARRAY:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[Z)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # [Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->getArrayValue([Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/mediatek/engineermode/camera6589/AutoCalibration;ILjava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->putStrInPreference(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameMode:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocusStart:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocusEnd:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/camera6589/AutoCalibration;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureMode:I

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/camera6589/AutoCalibration;[I[Landroid/widget/RadioButton;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # [I
    .param p2    # [Landroid/widget/RadioButton;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->setCaptureLayout([I[Landroid/widget/RadioButton;)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/camera6589/AutoCalibration;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->setAfModeAccessble(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfAuto:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracket:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfFullScan:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/camera6589/AutoCalibration;

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThrough:Landroid/widget/RadioButton;

    return-object v0
.end method

.method private getArrayValue([Z)Ljava/lang/String;
    .locals 5
    .param p1    # [Z

    const-string v1, ""

    const/4 v0, 0x2

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    aget-boolean v2, p1, v0

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->ISO_STRS_ARRAY:[Ljava/lang/String;

    add-int/lit8 v4, v0, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    const v2, 0x7f0804f6

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    const-string v1, "0,"

    :cond_2
    return-object v1
.end method

.method private inintComponents()V
    .locals 7

    const v6, 0x1090003

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    const v1, 0x7f0b029a

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalMode:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalMode:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mRadioListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v1, 0x7f0b029f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameMode:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameMode:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mRadioListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameMode:Landroid/widget/RadioButton;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0b02a2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mVideoCliplMode:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mVideoCliplMode:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mRadioListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v1, 0x7f0b029b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalCaptureLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0b029c

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalCaptureSize:Landroid/widget/Spinner;

    const v1, 0x7f0b029d

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalCaptureType:Landroid/widget/Spinner;

    const v1, 0x7f0b029e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalCaptureNum:Landroid/widget/Spinner;

    const v1, 0x7f0b02a0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0b02a1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameCaptureNum:Landroid/widget/Spinner;

    const v1, 0x7f0b02a3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mVideoClipLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0b02a4

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mVideoClipResolution:Landroid/widget/Spinner;

    const v1, 0x7f0b02a9

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfAuto:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfAuto:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mRadioListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v1, 0x7f0b02ae

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfFullScan:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfFullScan:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mRadioListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v1, 0x7f0b02aa

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracket:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracket:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mRadioListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v1, 0x7f0b02af

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThrough:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThrough:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mRadioListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v1, 0x7f0b02ab

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracketLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0b02ad

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracketInterval:Landroid/widget/Spinner;

    const v1, 0x7f0b02ac

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracketRange:Landroid/widget/EditText;

    const v1, 0x7f0b02b0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThroughLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0b02b2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThroughDirec:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThroughDirec:Landroid/widget/Spinner;

    new-instance v2, Lcom/mediatek/engineermode/camera6589/AutoCalibration$5;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration$5;-><init>(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v1, 0x7f0b02b1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThroughInterval:Landroid/widget/EditText;

    const v1, 0x7f0b02b3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocusStart:Landroid/widget/LinearLayout;

    const v1, 0x7f0b02b5

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocusEnd:Landroid/widget/LinearLayout;

    const v1, 0x7f0b02b4

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocsuStartPos:Landroid/widget/EditText;

    const v1, 0x7f0b02b6

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocsuEndPos:Landroid/widget/EditText;

    const v1, 0x7f0b02a5

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mIsoListView:Landroid/widget/ListView;

    new-instance v0, Landroid/widget/ArrayAdapter;

    new-array v1, v4, [Ljava/lang/String;

    const v2, 0x7f0804e9

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-direct {v0, p0, v6, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mIsoListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mIsoListView:Landroid/widget/ListView;

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->setLvHeight(Landroid/widget/ListView;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mIsoListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v1, 0x7f0b02a6

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mFlickerSpinner:Landroid/widget/Spinner;

    const v1, 0x7f0b02a7

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mStrobeModeSpinner:Landroid/widget/Spinner;

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060059

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v6, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const v1, 0x7f0b02b7

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureListView:Landroid/widget/ListView;

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->setLvHeight(Landroid/widget/ListView;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v1, 0x7f0b02a8

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mHdrSpinner:Landroid/widget/Spinner;

    const-string v1, "1"

    const-string v2, "mediatek.hdr.debug"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mHdrSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v4}, Landroid/widget/AbsSpinner;->setSelection(I)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mHdrSpinner:Landroid/widget/Spinner;

    new-instance v2, Lcom/mediatek/engineermode/camera6589/AutoCalibration$6;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration$6;-><init>(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mHdrSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v3}, Landroid/widget/AbsSpinner;->setSelection(I)V

    goto :goto_0
.end method

.method private putInPreference(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, "EM/AutoCalibration"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "putInPreference key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v4, "camera_inter_settings"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v4, "EM/AutoCalibration"

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private putStrInPreference(ILjava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, "EM/AutoCalibration"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "putInPreference key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v4, "camera_inter_settings"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v4, "EM/AutoCalibration"

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private putValuesToPreference()Z
    .locals 9

    const v8, 0x7f0804d2

    const/16 v7, 0x1ff

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v5, "camera_inter_settings"

    invoke-virtual {p0, v5, v4}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const v5, 0x7f0804cf

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureMode:I

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget v5, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureMode:I

    if-nez v5, :cond_1

    const v5, 0x7f0804d0

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalCaptureSize:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v5, 0x7f0804d1

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalCaptureType:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalCaptureNum:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :goto_0
    const v5, 0x7f0804d5

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mFlickerSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v5, 0x7f0804d6

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mStrobeModeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v5, 0x7f0804d9

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfMode:I

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v5, 0x7f0804db

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracketInterval:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v5, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracketRange:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_0

    if-le v2, v7, :cond_3

    :cond_0
    const v5, 0x7f0804f2

    invoke-static {p0, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    move v3, v4

    :goto_1
    return v3

    :cond_1
    iget v5, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureMode:I

    if-ne v5, v3, :cond_2

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameCaptureNum:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_2
    const v5, 0x7f0804d3

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mVideoClipResolution:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_0

    :cond_3
    const v5, 0x7f0804da

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v5, 0x7f0804dc

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThroughDirec:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v5, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThroughDirec:Landroid/widget/Spinner;

    invoke-virtual {v5}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_4

    const v5, 0x7f0804de

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocsuStartPos:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v5, 0x7f0804df

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocsuEndPos:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_4
    iget-object v5, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThroughInterval:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v2, v3, :cond_5

    if-le v2, v7, :cond_6

    :cond_5
    const v5, 0x7f0804f3

    invoke-static {p0, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    move v3, v4

    goto/16 :goto_1

    :cond_6
    const v4, 0x7f0804dd

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_1
.end method

.method private setAfLayout([I[Landroid/widget/RadioButton;)V
    .locals 7
    .param p1    # [I
    .param p2    # [Landroid/widget/RadioButton;

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracketLayout:Landroid/widget/LinearLayout;

    aget v5, p1, v6

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThroughLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object v0, p2

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-virtual {v3, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setAfModeAccessble(Z)V
    .locals 7
    .param p1    # Z

    const/4 v5, 0x3

    new-array v1, v5, [Landroid/widget/RadioButton;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracket:Landroid/widget/RadioButton;

    aput-object v6, v1, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfFullScan:Landroid/widget/RadioButton;

    aput-object v6, v1, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThrough:Landroid/widget/RadioButton;

    aput-object v6, v1, v5

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setCaptureLayout([I[Landroid/widget/RadioButton;)V
    .locals 7
    .param p1    # [I
    .param p2    # [Landroid/widget/RadioButton;

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalCaptureLayout:Landroid/widget/LinearLayout;

    aget v5, p1, v6

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mVideoClipLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object v0, p2

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-virtual {v3, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setLvHeight(Landroid/widget/ListView;)V
    .locals 7
    .param p1    # Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v5, "EM/AutoCalibration"

    const-string v6, "no data in ListView"

    invoke-static {v5, v6}, Lcom/mediatek/engineermode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x0

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_1

    const/4 v5, 0x0

    invoke-interface {v0, v1, v5, p1}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6, v6}, Landroid/view/View;->measure(II)V

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v5

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    mul-int/2addr v5, v6

    add-int/2addr v5, v4

    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setStatusTodefault()V
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v3, "EM/AutoCalibration"

    const-string v4, "setStatusTodefault()"

    invoke-static {v3, v4}, Lcom/mediatek/engineermode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    iput v5, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureMode:I

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalMode:Landroid/widget/RadioButton;

    invoke-virtual {v3, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    aput-boolean v6, v3, v5

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    aput-boolean v5, v3, v6

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    const/4 v4, 0x2

    aput-boolean v6, v3, v4

    const/4 v1, 0x3

    :goto_0
    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    array-length v3, v3

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    aput-boolean v5, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iput v5, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfMode:I

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfAuto:Landroid/widget/RadioButton;

    invoke-virtual {v3, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iput-boolean v6, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfModeStatus:Z

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfBracketRange:Landroid/widget/EditText;

    const-string v4, "0"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfThroughInterval:Landroid/widget/EditText;

    const-string v4, "1"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v3, "camera_inter_settings"

    invoke-virtual {p0, v3, v5}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const v3, 0x7f0804cf

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureMode:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f0804d0

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f0804d1

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f0804d2

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f0804d5

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f0804d6

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f0804d7

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f0804d8

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f0804d4

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->getArrayValue([Z)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const v3, 0x7f0804d9

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfMode:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocsuStartPos:Landroid/widget/EditText;

    const v4, 0x7f0804de

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocsuEndPos:Landroid/widget/EditText;

    const v4, 0x7f0804df

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x3ff

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocusStart:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mThroughFocusEnd:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private statusChangesByAf(Z)V
    .locals 6
    .param p1    # Z

    const v5, 0x7f0804d4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mStrobeModeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v4}, Landroid/widget/AbsSpinner;->setSelection(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mStrobeModeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulFrameMode:Landroid/widget/RadioButton;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mVideoCliplMode:Landroid/widget/RadioButton;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalCaptureNum:Landroid/widget/Spinner;

    invoke-virtual {v1, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mCaptureMode:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mNormalMode:Landroid/widget/RadioButton;

    invoke-virtual {v1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_0
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    aput-boolean v3, v1, v2

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    aput-boolean v2, v1, v3

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    aput-boolean v3, v1, v4

    const/4 v0, 0x3

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    aput-boolean v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->getArrayValue([Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v5, v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->putStrInPreference(ILjava/lang/String;)V

    :goto_1
    return-void

    :cond_2
    iput v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfSpecialIso:I

    const-string v1, "0"

    iput-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mIsoValueStr:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mIsoValueStr:Ljava/lang/String;

    invoke-direct {p0, v5, v1}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->putStrInPreference(ILjava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "EM/AutoCalibration"

    const-string v1, "onCreate."

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f030052

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->inintComponents()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->setStatusTodefault()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const-string v2, "EM/AutoCalibration"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateDialog id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "AF mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x5

    if-ne v2, p1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0804cd

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfMode:I

    if-nez v2, :cond_2

    const v2, 0x7f060050

    iget-object v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mMulISOFlags:[Z

    new-instance v4, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration$2;-><init>(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems(I[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_0
    const v2, 0x104000a

    new-instance v3, Lcom/mediatek/engineermode/camera6589/AutoCalibration$4;

    invoke-direct {v3, p0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration$4;-><init>(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    :cond_1
    return-object v1

    :cond_2
    const v2, 0x7f060051

    iget v3, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mAfSpecialIso:I

    new-instance v4, Lcom/mediatek/engineermode/camera6589/AutoCalibration$3;

    invoke-direct {v4, p0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration$3;-><init>(Lcom/mediatek/engineermode/camera6589/AutoCalibration;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const-string v1, "EM/AutoCalibration"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemClick view: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->mIsoListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_1

    if-nez p3, :cond_0

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p3, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/camera6589/AutoCalibration;->putValuesToPreference()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const-string v1, "EM/AutoCalibration"

    const-string v2, "Start captureIntent!"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
