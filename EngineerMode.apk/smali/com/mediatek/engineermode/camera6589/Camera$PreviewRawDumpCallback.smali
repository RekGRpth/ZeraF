.class final Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewRawDumpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/camera6589/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PreviewRawDumpCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/camera6589/Camera;


# direct methods
.method private constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/engineermode/camera6589/Camera;Lcom/mediatek/engineermode/camera6589/Camera$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/engineermode/camera6589/Camera;
    .param p2    # Lcom/mediatek/engineermode/camera6589/Camera$1;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;-><init>(Lcom/mediatek/engineermode/camera6589/Camera;)V

    return-void
.end method


# virtual methods
.method public onNotify(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const-string v0, "test/camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNotify code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/engineermode/Elog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/hardware/Camera$Parameters;->setRawDumpFlag(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v0, v0, Lcom/mediatek/engineermode/camera6589/ActivityBase;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v1}, Lcom/mediatek/engineermode/camera6589/Camera;->access$2100(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    iget-object v0, v0, Lcom/mediatek/engineermode/camera6589/Camera;->mFocusManager:Lcom/mediatek/engineermode/camera6589/FocusManager;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/camera6589/FocusManager;->capture()V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-virtual {v0, v3}, Lcom/mediatek/engineermode/camera6589/Camera;->onShutterButtonFocus(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1300(Lcom/mediatek/engineermode/camera6589/Camera;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/camera6589/Camera$PreviewRawDumpCallback;->this$0:Lcom/mediatek/engineermode/camera6589/Camera;

    invoke-static {v0}, Lcom/mediatek/engineermode/camera6589/Camera;->access$1400(Lcom/mediatek/engineermode/camera6589/Camera;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method
