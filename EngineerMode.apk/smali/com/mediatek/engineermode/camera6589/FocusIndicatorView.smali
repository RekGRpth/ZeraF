.class public Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;
.super Landroid/view/View;
.source "FocusIndicatorView.java"

# interfaces
.implements Lcom/mediatek/engineermode/camera6589/FocusIndicator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private setDrawable(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public showFail()V
    .locals 1

    const v0, 0x7f020008

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;->setDrawable(I)V

    return-void
.end method

.method public showStart()V
    .locals 1

    const v0, 0x7f02000a

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;->setDrawable(I)V

    return-void
.end method

.method public showSuccess()V
    .locals 1

    const v0, 0x7f020009

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/camera6589/FocusIndicatorView;->setDrawable(I)V

    return-void
.end method
