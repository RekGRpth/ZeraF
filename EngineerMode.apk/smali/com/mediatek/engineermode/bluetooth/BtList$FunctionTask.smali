.class public Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;
.super Landroid/os/AsyncTask;
.source "BtList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/bluetooth/BtList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FunctionTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/bluetooth/BtList;


# direct methods
.method public constructor <init>(Lcom/mediatek/engineermode/bluetooth/BtList;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 5
    .param p1    # [Ljava/lang/Void;

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    new-instance v2, Lcom/mediatek/engineermode/bluetooth/BtTest;

    invoke-direct {v2}, Lcom/mediatek/engineermode/bluetooth/BtTest;-><init>()V

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$002(Lcom/mediatek/engineermode/bluetooth/BtList;Lcom/mediatek/engineermode/bluetooth/BtTest;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$100(Lcom/mediatek/engineermode/bluetooth/BtList;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$102(Lcom/mediatek/engineermode/bluetooth/BtList;Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/BluetoothAdapter;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$100(Lcom/mediatek/engineermode/bluetooth/BtList;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$000(Lcom/mediatek/engineermode/bluetooth/BtList;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->isBLESupport()I

    move-result v1

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1, v3}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$202(Lcom/mediatek/engineermode/bluetooth/BtList;Z)Z

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$000(Lcom/mediatek/engineermode/bluetooth/BtList;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/engineermode/bluetooth/BtTest;->isComboSupport()I

    move-result v1

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1, v3}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$302(Lcom/mediatek/engineermode/bluetooth/BtList;Z)Z

    :goto_1
    const-string v1, "EM/BTList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BLE supported ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v3}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$200(Lcom/mediatek/engineermode/bluetooth/BtList;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v2}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$000(Lcom/mediatek/engineermode/bluetooth/BtList;)Lcom/mediatek/engineermode/bluetooth/BtTest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/engineermode/bluetooth/BtTest;->getChipId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$402(Lcom/mediatek/engineermode/bluetooth/BtList;I)I

    :goto_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1, v4}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$202(Lcom/mediatek/engineermode/bluetooth/BtList;Z)Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1, v4}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$302(Lcom/mediatek/engineermode/bluetooth/BtList;Z)Z

    goto :goto_1

    :cond_3
    const-wide/16 v1, 0x12c

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "EM/BTList"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 6
    .param p1    # Ljava/lang/Integer;

    const v5, 0x7f080291

    const/16 v4, 0x10

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$502(Lcom/mediatek/engineermode/bluetooth/BtList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$500(Lcom/mediatek/engineermode/bluetooth/BtList;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v3, 0x7f0802ad

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "EM/BTList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "chipId@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v3}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$400(Lcom/mediatek/engineermode/bluetooth/BtList;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "EM/BTList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "6620@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "EM/BTList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0x6620@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v2, 0x7f080292

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v2}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$400(Lcom/mediatek/engineermode/bluetooth/BtList;)I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$500(Lcom/mediatek/engineermode/bluetooth/BtList;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v3, 0x7f08029d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$500(Lcom/mediatek/engineermode/bluetooth/BtList;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v3, 0x7f0802aa

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$500(Lcom/mediatek/engineermode/bluetooth/BtList;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v3, 0x7f0802ab

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$200(Lcom/mediatek/engineermode/bluetooth/BtList;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$500(Lcom/mediatek/engineermode/bluetooth/BtList;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v3, 0x7f0802d1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$300(Lcom/mediatek/engineermode/bluetooth/BtList;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$500(Lcom/mediatek/engineermode/bluetooth/BtList;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v3, 0x7f080299

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$500(Lcom/mediatek/engineermode/bluetooth/BtList;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v3, 0x7f0802c4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v1}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$500(Lcom/mediatek/engineermode/bluetooth/BtList;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v3, 0x7f08060d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const v2, 0x1090003

    iget-object v3, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-static {v3}, Lcom/mediatek/engineermode/bluetooth/BtList;->access$500(Lcom/mediatek/engineermode/bluetooth/BtList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    invoke-virtual {v1, v0}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->this$0:Lcom/mediatek/engineermode/bluetooth/BtList;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->removeDialog(I)V

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/mediatek/engineermode/bluetooth/BtList$FunctionTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
