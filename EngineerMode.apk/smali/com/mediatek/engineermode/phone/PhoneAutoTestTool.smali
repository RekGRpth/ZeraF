.class public Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;
.super Landroid/app/Activity;
.source "PhoneAutoTestTool.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$EndCallHandler;
    }
.end annotation


# static fields
.field private static final DEFAULT_VALUE:I = 0xa

.field private static final END_CALL_TEST:I = 0x270d

.field private static final MAIN_INTENT_ACTION:Ljava/lang/String; = "android.phone.extra.VT_AUTO_TEST_TOOL"

.field private static final ONE_SECOND:I = 0x3e8

.field private static final START_REPEAT_MO_TEST:I = 0x270f

.field private static final TAG:Ljava/lang/String; = "PhoneAutoTestTool"

.field private static final TYPE_TEL:I = 0x3

.field private static sPhoneAutoTestLooper:Landroid/os/Looper;


# instance fields
.field private mButton01:Landroid/widget/Button;

.field private mButton02:Landroid/widget/Button;

.field private mDuration:Landroid/widget/EditText;

.field private mDurationValue:I

.field private mEndCallThreadHandler:Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$EndCallHandler;

.field mHandler:Landroid/os/Handler;

.field private mNumber:Landroid/widget/EditText;

.field private mNumberValue:Ljava/lang/String;

.field private mRepeatTime:Landroid/widget/EditText;

.field private mRepeatTimeNow:I

.field private mRepeatTimeValue:I

.field private mSim:Landroid/widget/EditText;

.field private mSimValue:I

.field private mType:Landroid/widget/EditText;

.field private mTypeValue:I

.field private mWait:Landroid/widget/EditText;

.field private mWaitValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->sPhoneAutoTestLooper:Landroid/os/Looper;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mButton01:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mButton02:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mNumber:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mNumberValue:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTime:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mDuration:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mWait:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mType:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mSim:Landroid/widget/EditText;

    new-instance v0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$1;-><init>(Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;

    invoke-direct {p0}, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->testRepeatCall()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;

    invoke-direct {p0}, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->runEndCall()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;

    iget v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mWaitValue:I

    return v0
.end method

.method private logValues()V
    .locals 3

    const-string v0, "PhoneAutoTestTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mNumberValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mNumberValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PhoneAutoTestTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRepeatTimeNow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeNow:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PhoneAutoTestTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRepeatTimeValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PhoneAutoTestTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDurationValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mDurationValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PhoneAutoTestTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mWaitValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mWaitValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PhoneAutoTestTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTypeValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mTypeValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PhoneAutoTestTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSimValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mSimValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private resetAutoTest()V
    .locals 3

    const/16 v2, 0xa

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mNumberValue:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeNow:I

    iput v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeValue:I

    iput v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mDurationValue:I

    iput v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mWaitValue:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mTypeValue:I

    iput v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mSimValue:I

    return-void
.end method

.method private runDialCall()V
    .locals 4

    const/4 v3, 0x0

    const-string v1, "PhoneAutoTestTool"

    const-string v2, "runDialCall() ! "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.phone.InCallScreen"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.CALL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x3

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mTypeValue:I

    if-eq v1, v2, :cond_1

    const-string v1, "tel"

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mNumberValue:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :goto_0
    const-string v1, "simId"

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mSimValue:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v1, 0x2

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mTypeValue:I

    if-ne v1, v2, :cond_0

    const-string v1, "is_vt_call"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_1
    const-string v1, "sip"

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mNumberValue:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private runEndCall()V
    .locals 4

    const-string v2, "PhoneAutoTestTool"

    const-string v3, "runEndCall() ! "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "phone"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mSimValue:I

    invoke-interface {v1, v2}, Lcom/android/internal/telephony/ITelephony;->endCallGemini(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "PhoneAutoTestTool"

    const-string v3, "endCallGemini() RemoteException"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private testRepeatCall()V
    .locals 4

    const-string v0, "PhoneAutoTestTool"

    const-string v1, "testRepeatCall() ! "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeNow:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeNow:I

    iget v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeNow:I

    iget v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeValue:I

    if-le v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->resetAutoTest()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "PhoneAutoTestTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "testRepeatCall() : total - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PhoneAutoTestTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "testRepeatCall() : now - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeNow:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->runDialCall()V

    iget-object v0, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mEndCallThreadHandler:Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$EndCallHandler;

    iget-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mEndCallThreadHandler:Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$EndCallHandler;

    const/16 v2, 0x270d

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mDurationValue:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    const/16 v7, 0x270f

    const/16 v6, 0x270d

    const/16 v5, 0xa

    const/4 v4, 0x0

    const-string v1, "PhoneAutoTestTool"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f0b0398

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mNumber:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mNumberValue:Ljava/lang/String;

    :try_start_0
    new-instance v1, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTime:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeValue:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    new-instance v1, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mDuration:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mDurationValue:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    new-instance v1, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mWait:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mWaitValue:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    new-instance v1, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mType:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mTypeValue:I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    new-instance v1, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mSim:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mSimValue:I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    invoke-direct {p0}, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->logValues()V

    iget-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_5
    return-void

    :catch_0
    move-exception v0

    iput v4, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTimeValue:I

    goto :goto_0

    :catch_1
    move-exception v0

    iput v5, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mDurationValue:I

    goto :goto_1

    :catch_2
    move-exception v0

    iput v5, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mWaitValue:I

    goto :goto_2

    :catch_3
    move-exception v0

    const/4 v1, 0x1

    iput v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mTypeValue:I

    goto :goto_3

    :catch_4
    move-exception v0

    iput v4, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mSimValue:I

    goto :goto_4

    :cond_1
    const v1, 0x7f0b0399

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mEndCallThreadHandler:Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$EndCallHandler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mEndCallThreadHandler:Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$EndCallHandler;

    iget-object v2, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mEndCallThreadHandler:Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$EndCallHandler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-direct {p0}, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->resetAutoTest()V

    goto :goto_5
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030069

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->resetAutoTest()V

    const v1, 0x7f0b0398

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mButton01:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mButton01:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b0399

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mButton02:Landroid/widget/Button;

    iget-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mButton02:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0b038d

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mNumber:Landroid/widget/EditText;

    const v1, 0x7f0b038f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mRepeatTime:Landroid/widget/EditText;

    const v1, 0x7f0b0391

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mDuration:Landroid/widget/EditText;

    const v1, 0x7f0b0393

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mWait:Landroid/widget/EditText;

    const v1, 0x7f0b0395

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mType:Landroid/widget/EditText;

    const v1, 0x7f0b0397

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mSim:Landroid/widget/EditText;

    const-class v2, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->sPhoneAutoTestLooper:Landroid/os/Looper;

    if-nez v1, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PhoneAutoTestHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    sput-object v1, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->sPhoneAutoTestLooper:Landroid/os/Looper;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v1, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$2;

    sget-object v2, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->sPhoneAutoTestLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$2;-><init>(Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/phone/PhoneAutoTestTool;->mEndCallThreadHandler:Lcom/mediatek/engineermode/phone/PhoneAutoTestTool$EndCallHandler;

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
