.class public Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
.super Landroid/app/Service;
.source "CpuStressTestService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;,
        Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;
    }
.end annotation


# static fields
.field public static final CORE_NUMBER_1:I = 0x1

.field public static final CORE_NUMBER_2:I = 0x2

.field public static final CORE_NUMBER_4:I = 0x4

.field protected static final CORE_NUM_MASK:I = 0x1c

.field private static final CPU_1_ONLINE_PATH:Ljava/lang/String; = "/sys/devices/system/cpu/cpu1/online"

.field private static final CPU_3_ONLINE_PATH:Ljava/lang/String; = "/sys/devices/system/cpu/cpu3/online"

.field private static final FAIL:Ljava/lang/String; = "FAIL"

.field private static final HANDLER_THREAD_NAME_APMCU:Ljava/lang/String; = "ApMcu"

.field private static final HANDLER_THREAD_NAME_BACKUPRESTORE:Ljava/lang/String; = "BackupRestore"

.field private static final HANDLER_THREAD_NAME_VIDEO:Ljava/lang/String; = "VideoCodec"

.field private static final INDEX_TEST_APMCU:I = 0x1

.field private static final INDEX_TEST_BACKUP:I = 0x3

.field private static final INDEX_TEST_RESTORE:I = 0x4

.field private static final INDEX_TEST_VIDEOCODEC:I = 0x2

.field private static final LOOPCOUNT_DEFAULT_VALUE:J = 0x5f5e0ffL

.field private static final PASS:Ljava/lang/String; = "PASS"

.field private static final PASS_89:Ljava/lang/String; = "Frame #1950"

.field public static final RESULT_CA9:Ljava/lang/String; = "result_ca9"

.field public static final RESULT_DHRY:Ljava/lang/String; = "result_dhry"

.field private static final RESULT_ERROR:Ljava/lang/String; = "ERROR"

.field public static final RESULT_FDCT:Ljava/lang/String; = "result_fdct"

.field public static final RESULT_IMDCT:Ljava/lang/String; = "result_imdct"

.field public static final RESULT_MEMCPY:Ljava/lang/String; = "result_memcpy"

.field public static final RESULT_NEON:Ljava/lang/String; = "result_neon"

.field public static final RESULT_PASS_CA9:Ljava/lang/String; = "result_pass_ca9"

.field public static final RESULT_PASS_DHRY:Ljava/lang/String; = "result_pass_dhry"

.field public static final RESULT_PASS_FDCT:Ljava/lang/String; = "result_pass_fdct"

.field public static final RESULT_PASS_IMDCT:Ljava/lang/String; = "result_pass_imdct"

.field public static final RESULT_PASS_MEMCPY:Ljava/lang/String; = "result_pass_memcpy"

.field public static final RESULT_PASS_NEON:Ljava/lang/String; = "result_pass_neon"

.field public static final RESULT_PASS_VIDEOCODEC:Ljava/lang/String; = "result_pass_video_codec"

.field private static final RESULT_SEPARATE:Ljava/lang/String; = ";"

.field public static final RESULT_VIDEOCODEC:Ljava/lang/String; = "result_video_codec"

.field private static final SKIP:Ljava/lang/String; = "is powered off"

.field private static final SKIP_89:Ljava/lang/String; = "Frame #"

.field private static final TAG:Ljava/lang/String; = "EM/CpuStressTestService"

.field protected static final THERMAL_ETC_FILE:Ljava/lang/String; = "/etc/.tp/.ht120.mtc"

.field private static final TIME_DELAYED:I = 0x64

.field public static final VALUE_ITERATION:Ljava/lang/String; = "iteration"

.field public static final VALUE_LOOPCOUNT:Ljava/lang/String; = "loopcount"

.field public static final VALUE_MASK:Ljava/lang/String; = "mask"

.field public static final VALUE_RESULT:Ljava/lang/String; = "result"

.field public static final VALUE_RUN:Ljava/lang/String; = "run"

.field public static sCoreNumber:I

.field protected static sIndexMode:I

.field protected static sIsThermalDisabled:Z

.field protected static sIsThermalSupport:Z


# instance fields
.field private final mBinder:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

.field private mHandlerApMcu:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

.field private mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

.field private final mHandlerThreadApMcu:Landroid/os/HandlerThread;

.field private final mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

.field private final mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

.field private mHandlerVideoCodec:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

.field private mIterationVideoCodec:I

.field private mLoopCountApMcu:J

.field private mLoopCountVideoCodec:J

.field private mResultApMcu:I

.field private mResultPassCa9:I

.field private mResultPassDhry:I

.field private mResultPassFdct:I

.field private mResultPassImdct:I

.field private mResultPassL2C:I

.field private mResultPassMemcpy:I

.field private mResultPassNeon:I

.field private mResultPassVideoCodec:I

.field private mResultTotalCa9:I

.field private mResultTotalDhry:I

.field private mResultTotalFdct:I

.field private mResultTotalImdct:I

.field private mResultTotalL2C:I

.field private mResultTotalMemcpy:I

.field private mResultTotalNeon:I

.field private mResultTotalVideoCodec:I

.field private mResultVideoCodec:I

.field private mTestApMcuMask:I

.field private mTestApMcuRunning:Z

.field protected mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

.field private mTestClockSwitchRunning:Z

.field private mTestVideoCodecRunning:Z

.field private mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

.field protected mWantStopApmcu:Z

.field protected mWantStopSwCodec:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    sput-boolean v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalSupport:Z

    sput-boolean v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalDisabled:Z

    sput v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const-wide/32 v3, 0x5f5e0ff

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-wide v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassL2C:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalL2C:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassDhry:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalDhry:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassMemcpy:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalMemcpy:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassFdct:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalFdct:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassImdct:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalImdct:I

    iput-wide v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mIterationVideoCodec:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopApmcu:Z

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopSwCodec:Z

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    new-instance v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mBinder:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ApMcu"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadApMcu:Landroid/os/HandlerThread;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "VideoCodec"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "BackupRestore"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerApMcu:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerVideoCodec:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;J)J
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    return-wide p1
.end method

.method static synthetic access$010(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)J
    .locals 4
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)J
    .locals 2
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;J)J
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    return-wide p1
.end method

.method static synthetic access$310(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)J
    .locals 4
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    return v0
.end method

.method static synthetic access$402(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    return p1
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doVideoCodecTest()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/CpuStressTestService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doBackupRestore(I)V

    return-void
.end method

.method private backup(I)V
    .locals 4
    .param p1    # I

    const-string v1, "EM/CpuStressTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter backup: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    add-int/lit8 v1, p1, 0x14

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private coreNum()I
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/system/cpu/cpu3/online"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/system/cpu/cpu1/online"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private dataGenerator(I)Landroid/os/Bundle;
    .locals 4
    .param p1    # I

    const-string v1, "EM/CpuStressTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dataGenerator index is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v1, "run"

    iget-boolean v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "loopcount"

    iget-wide v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "mask"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_neon"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_neon"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_ca9"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_ca9"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_dhry"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalDhry:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_dhry"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassDhry:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_memcpy"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalMemcpy:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_memcpy"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassMemcpy:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_fdct"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalFdct:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_fdct"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassFdct:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_imdct"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalImdct:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_imdct"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassImdct:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_1
    const-string v1, "run"

    iget-boolean v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "loopcount"

    iget-wide v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "iteration"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mIterationVideoCodec:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_video_codec"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "result_pass_video_codec"

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private doApMcuTest()V
    .locals 7

    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "enter doApMpuTest"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v5, v0, :cond_b

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const-string v0, "EM/CpuStressTestService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "iResultApMpu is 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :pswitch_0
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_1
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_2
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_3

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_3
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_4

    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_4
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_5

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_5
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_6
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_7

    invoke-direct {p0, v6}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_7
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_8
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_9

    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_9
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_a

    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_a
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto/16 :goto_0

    :cond_b
    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v2, v0, :cond_20

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_2
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_c

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_c
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_d

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_d
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_e

    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_e
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_f

    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_f
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_10

    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_10
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto/16 :goto_0

    :pswitch_3
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_11

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_11
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_12

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_12
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_13

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_13
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_14

    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_14
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_15

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_15
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto/16 :goto_0

    :pswitch_4
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_16

    invoke-direct {p0, v4}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_16
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_17

    invoke-direct {p0, v6}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_17
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_18

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_18
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_19

    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_19
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_1a

    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_1a
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto/16 :goto_0

    :pswitch_5
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1b

    invoke-direct {p0, v5}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_1b
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_1c

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_1c
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_1d

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_1d
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_1e

    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_1e
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    if-eqz v0, :cond_1f

    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_1f
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto/16 :goto_0

    :cond_20
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_21

    invoke-direct {p0, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    :cond_21
    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->doApMcuTest(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method

.method private doApMcuTest(I)V
    .locals 22
    .param p1    # I

    const-string v18, "EM/CpuStressTestService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "doApMpuTest index is: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const v18, 0x9c41

    const/16 v19, 0x1

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [I

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput p1, v20, v21

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->runCmdInNative(II[I)Ljava/lang/String;

    move-result-object v17

    const-string v18, "EM/CpuStressTestService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "doApMcuTest response: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v17, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    const-string v18, ";"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    const/4 v9, 0x1

    const/4 v13, 0x0

    :goto_1
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_4

    aget-object v18, v16, v13

    const-string v19, "PASS"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x0

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_2
    aget-object v18, v16, v13

    const-string v19, "is powered off"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x0

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const-string v18, "EM/CpuStressTestService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "NEON test, CPU"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " OFFLINE"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x0

    shl-int v19, v19, v20

    xor-int/lit8 v19, v19, -0x1

    and-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_2

    :cond_4
    if-eqz v9, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    goto/16 :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    const-string v18, ";"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    const/4 v4, 0x1

    const/4 v13, 0x0

    :goto_3
    array-length v0, v10

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_7

    aget-object v18, v10, v13

    const-string v19, "PASS"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x4

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_5
    aget-object v18, v10, v13

    const-string v19, "is powered off"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x4

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const-string v18, "EM/CpuStressTestService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "CA9 test, CPU"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " OFFLINE"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_6
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x4

    shl-int v19, v19, v20

    xor-int/lit8 v19, v19, -0x1

    and-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_4

    :cond_7
    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    goto/16 :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalDhry:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalDhry:I

    const-string v18, ";"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const/4 v5, 0x1

    const/4 v13, 0x0

    :goto_5
    array-length v0, v11

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_a

    aget-object v18, v11, v13

    const-string v19, "PASS"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x8

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_6
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    :cond_8
    aget-object v18, v11, v13

    const-string v19, "is powered off"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x8

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const-string v18, "EM/CpuStressTestService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "DHRY test, CPU"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " OFFLINE"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :cond_9
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x8

    shl-int v19, v19, v20

    xor-int/lit8 v19, v19, -0x1

    and-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_6

    :cond_a
    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassDhry:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassDhry:I

    goto/16 :goto_0

    :pswitch_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalMemcpy:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalMemcpy:I

    const-string v18, ";"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    const/4 v8, 0x1

    const/4 v13, 0x0

    :goto_7
    array-length v0, v15

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_d

    aget-object v18, v15, v13

    const-string v19, "PASS"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0xc

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_8
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    :cond_b
    aget-object v18, v15, v13

    const-string v19, "is powered off"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0xc

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const-string v18, "EM/CpuStressTestService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "MEMCPY test, CPU"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " OFFLINE"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    :cond_c
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0xc

    shl-int v19, v19, v20

    xor-int/lit8 v19, v19, -0x1

    and-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_8

    :cond_d
    if-eqz v8, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassMemcpy:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassMemcpy:I

    goto/16 :goto_0

    :pswitch_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalFdct:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalFdct:I

    const-string v18, ";"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    const/4 v6, 0x1

    const/4 v13, 0x0

    :goto_9
    array-length v0, v12

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_10

    aget-object v18, v12, v13

    const-string v19, "PASS"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x10

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_a
    add-int/lit8 v13, v13, 0x1

    goto :goto_9

    :cond_e
    aget-object v18, v12, v13

    const-string v19, "is powered off"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x10

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const-string v18, "EM/CpuStressTestService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "FDCT test, CPU"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " OFFLINE"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    :cond_f
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x10

    shl-int v19, v19, v20

    xor-int/lit8 v19, v19, -0x1

    and-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_a

    :cond_10
    if-eqz v6, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassFdct:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassFdct:I

    goto/16 :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalImdct:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalImdct:I

    const-string v18, ";"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    const/4 v7, 0x1

    const/4 v13, 0x0

    :goto_b
    array-length v0, v14

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_13

    aget-object v18, v14, v13

    const-string v19, "PASS"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x14

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    :goto_c
    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    :cond_11
    aget-object v18, v14, v13

    const-string v19, "is powered off"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x14

    shl-int v19, v19, v20

    or-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    const-string v18, "EM/CpuStressTestService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "IMDCT test, CPU"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " OFFLINE"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    :cond_12
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    move/from16 v18, v0

    const/16 v19, 0x1

    add-int/lit8 v20, v13, 0x14

    shl-int v19, v19, v20

    xor-int/lit8 v19, v19, -0x1

    and-int v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    goto :goto_c

    :cond_13
    if-eqz v7, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassImdct:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassImdct:I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private doBackupRestore(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    const-string v1, "EM/CpuStressTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter doBackupRestore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x9c43

    new-array v2, v4, [I

    const/4 v3, 0x0

    aput p1, v2, v3

    invoke-direct {p0, v1, v4, v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->runCmdInNative(II[I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EM/CpuStressTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doBackupRestore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private doVideoCodecTest()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x1

    const-string v7, "EM/CpuStressTestService"

    const-string v8, "enter doVideoCodecTest"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, ""

    const/4 v6, -0x1

    sget v7, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v9, v7, :cond_1

    sget v7, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v7, :pswitch_data_0

    :goto_0
    const/4 v7, -0x1

    if-ne v7, v6, :cond_3

    :cond_0
    :goto_1
    return-void

    :pswitch_0
    const/4 v6, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v6, 0x1

    goto :goto_0

    :cond_1
    sget v7, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v12, v7, :cond_2

    sget v7, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v7, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    const/4 v6, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v6, 0x0

    goto :goto_0

    :pswitch_4
    const/4 v6, 0x1

    goto :goto_0

    :pswitch_5
    const/4 v6, 0x2

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    :cond_3
    const v7, 0x9c42

    new-array v8, v9, [I

    aput v6, v8, v11

    aput v10, v8, v10

    invoke-direct {p0, v7, v9, v8}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->runCmdInNative(II[I)Ljava/lang/String;

    move-result-object v3

    const-string v7, "EM/CpuStressTestService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "doVideoCodecTest response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_0

    iget v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    const-string v7, ";"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x1

    array-length v7, v4

    if-lez v7, :cond_a

    sget v7, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v12, v7, :cond_6

    const/4 v1, 0x0

    :goto_2
    array-length v7, v4

    if-ge v1, v7, :cond_b

    aget-object v7, v4, v1

    const-string v8, "Frame #1950"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    add-int/lit8 v8, v1, 0x0

    shl-int v8, v10, v8

    or-int/2addr v7, v8

    iput v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    aget-object v7, v4, v1

    const-string v8, "Frame #"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    add-int/lit8 v8, v1, 0x0

    shl-int v8, v10, v8

    or-int/2addr v7, v8

    iput v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    const-string v7, "EM/CpuStressTestService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VideoCodec test, CPU"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " OFFLINE"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    iget v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    add-int/lit8 v8, v1, 0x0

    shl-int v8, v10, v8

    xor-int/lit8 v8, v8, -0x1

    and-int/2addr v7, v8

    iput v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    const-string v2, "Frame #1950"

    const-string v5, "Frame #"

    const/16 v7, 0x10

    invoke-static {v7, v10}, Lcom/mediatek/engineermode/ChipSupport;->isCurrentChipHigher(IZ)Z

    move-result v7

    if-nez v7, :cond_7

    const-string v2, "PASS"

    const-string v5, "is powered off"

    :cond_7
    const/4 v1, 0x0

    :goto_4
    array-length v7, v4

    if-ge v1, v7, :cond_b

    aget-object v7, v4, v1

    invoke-virtual {v7, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    iget v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    add-int/lit8 v8, v1, 0x0

    shl-int v8, v10, v8

    or-int/2addr v7, v8

    iput v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    aget-object v7, v4, v1

    invoke-virtual {v7, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_9

    iget v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    add-int/lit8 v8, v1, 0x0

    shl-int v8, v10, v8

    or-int/2addr v7, v8

    iput v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    const-string v7, "EM/CpuStressTestService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "VideoCodec test, CPU"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " OFFLINE"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_9
    iget v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    add-int/lit8 v8, v1, 0x0

    shl-int v8, v10, v8

    xor-int/lit8 v8, v8, -0x1

    and-int/2addr v7, v8

    iput v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    const/4 v0, 0x0

    goto :goto_5

    :cond_a
    iput v11, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    iget v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    sget v8, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    shl-int/lit8 v8, v8, 0x1c

    or-int/2addr v7, v8

    iput v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    const/4 v0, 0x0

    :cond_b
    if-eqz v0, :cond_0

    iget v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method

.method private restore(I)V
    .locals 4
    .param p1    # I

    const-string v1, "EM/CpuStressTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter restore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    add-int/lit8 v1, p1, 0x28

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private varargs runCmdInNative(II[I)Ljava/lang/String;
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;

    invoke-direct {v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;-><init>()V

    invoke-virtual {v2, p1}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->startCallFunctionStringReturn(I)Z

    move-result v7

    invoke-virtual {v2, p2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamNo(I)Z

    move-object v0, p3

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget v3, v0, v4

    invoke-virtual {v2, v3}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->writeParamInt(I)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    if-eqz v7, :cond_4

    :cond_1
    invoke-virtual {v2}, Lcom/mediatek/engineermode/emsvr/AFMFunctionCallEx;->getNextResult()Lcom/mediatek/engineermode/emsvr/FunctionReturn;

    move-result-object v6

    iget-object v8, v6, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_3

    :goto_1
    iget v8, v6, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_2

    const-string v8, "EM/CpuStressTestService"

    const-string v9, "AFMFunctionCallEx: RESULT_IO_ERR"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    const-string v10, "ERROR"

    invoke-virtual {v1, v8, v9, v10}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8

    :cond_3
    iget-object v8, v6, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnString:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v8, v6, Lcom/mediatek/engineermode/emsvr/FunctionReturn;->mReturnCode:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_1

    goto :goto_1

    :cond_4
    const-string v8, "EM/CpuStressTestService"

    const-string v9, "AFMFunctionCallEx return false"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "ERROR"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method


# virtual methods
.method public getData()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateData(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public isClockSwitchRun()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    return v0
.end method

.method public isTestRun()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mBinder:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate()V
    .locals 5

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->coreNum()I

    move-result v0

    sput v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    shl-int/lit8 v3, v0, 0x1c

    or-int/2addr v2, v3

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    iget v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    shl-int/lit8 v3, v0, 0x1c

    or-int/2addr v2, v3

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    new-instance v2, Ljava/io/File;

    const-string v3, "/etc/.tp/.ht120.mtc"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    sput-boolean v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIsThermalSupport:Z

    new-instance v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    invoke-direct {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;-><init>()V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadApMcu:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    new-instance v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadApMcu:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerApMcu:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

    new-instance v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerVideoCodec:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

    new-instance v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;-><init>(Lcom/mediatek/engineermode/cpustress/CpuStressTestService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerBackupRestore:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerBackupRestore;
    :try_end_0
    .catch Ljava/lang/IllegalThreadStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v2, "EM/CpuStressTestService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Core Number: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v1

    const-string v2, "EM/CpuStressTestService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Handler thread IllegalThreadStateException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f08012d

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->restore(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadApMcu:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadVideoCodec:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerThreadBackupRestore:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;->release()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "Enter onStartCommand"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    return v0
.end method

.method public setIndexMode(I)V
    .locals 3
    .param p1    # I

    const-string v0, "EM/CpuStressTestService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setIndexMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sIndexMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->backup(I)V

    :goto_1
    monitor-enter p0

    :try_start_0
    sput p1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    if-nez p1, :cond_2

    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->restore(I)V

    goto :goto_1

    :cond_2
    sget v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->restore(I)V

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->backup(I)V

    goto :goto_1
.end method

.method public startTest(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "Enter startTest"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ApMcu;

    if-eqz v0, :cond_2

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "startTest for ApMcu"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    if-eqz v0, :cond_1

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "ApMpu test is running"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "loopcount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountApMcu:J

    const-string v0, "mask"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuRunning:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopApmcu:Z

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    sget v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    shl-int/lit8 v1, v1, 0x1c

    or-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultApMcu:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalL2C:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassL2C:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalNeon:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassNeon:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalCa9:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassCa9:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassDhry:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalDhry:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassMemcpy:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalMemcpy:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassFdct:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalFdct:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassImdct:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalImdct:I

    invoke-virtual {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateWakeLock()V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerApMcu:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerApMcu;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    if-eqz v0, :cond_4

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "startTest for SwVideoCodec"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    if-eqz v0, :cond_3

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "VideoCodec test is running"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v0, "loopcount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mLoopCountVideoCodec:J

    const-string v0, "iteration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mIterationVideoCodec:I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestVideoCodecRunning:Z

    iput-boolean v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopSwCodec:Z

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    iget v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    sget v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    shl-int/lit8 v1, v1, 0x1c

    or-int/2addr v0, v1

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultVideoCodec:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultPassVideoCodec:I

    iput v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mResultTotalVideoCodec:I

    invoke-virtual {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateWakeLock()V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mHandlerVideoCodec:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$HandlerVideoCodec;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    if-eqz v0, :cond_0

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "startTest for ClockSwitch"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    invoke-virtual {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateWakeLock()V

    goto/16 :goto_0
.end method

.method public stopTest()V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "EM/CpuStressTestService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter stopTest, testObject is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ApMcu;

    if-eqz v0, :cond_1

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "stopTest for ApMcu"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopApmcu:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    if-eqz v0, :cond_2

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "stopTest for SwVideoCodec"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopSwCodec:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    if-eqz v0, :cond_0

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "stopTest for ClockSwitch"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClockSwitchRunning:Z

    goto :goto_0
.end method

.method public updateData(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    const-string v2, "EM/CpuStressTestService"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateData, data is null ? "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ApMcu;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->dataGenerator(I)Landroid/os/Bundle;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->dataGenerator(I)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/ApMcu;

    if-eqz v0, :cond_4

    const-string v0, "mask"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestApMcuMask:I

    :cond_3
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    instance-of v0, v0, Lcom/mediatek/engineermode/cpustress/SwVideoCodec;

    if-eqz v0, :cond_3

    const-string v0, "EM/CpuStressTestService"

    const-string v1, "VideoCodec test not need to update config"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public updateWakeLock()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->isTestRun()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    invoke-virtual {v0, p0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;->acquire(Landroid/content/Context;)V

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWakeLock:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;

    invoke-virtual {v0}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$WakeLock;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
