.class public Lcom/mediatek/engineermode/cpustress/ApMcu;
.super Lcom/mediatek/engineermode/cpustress/CpuStressCommon;
.source "ApMcu.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final MASK_CA9_0:I = 0x4

.field public static final MASK_CA9_1:I = 0x5

.field public static final MASK_CA9_2:I = 0x6

.field public static final MASK_CA9_3:I = 0x7

.field public static final MASK_CB:I = 0xf

.field public static final MASK_DHRY_0:I = 0x8

.field public static final MASK_DHRY_1:I = 0x9

.field public static final MASK_DHRY_2:I = 0xa

.field public static final MASK_DHRY_3:I = 0xb

.field public static final MASK_FDCT_0:I = 0x10

.field public static final MASK_FDCT_1:I = 0x11

.field public static final MASK_FDCT_2:I = 0x12

.field public static final MASK_FDCT_3:I = 0x13

.field public static final MASK_IMDCT_0:I = 0x14

.field public static final MASK_IMDCT_1:I = 0x15

.field public static final MASK_IMDCT_2:I = 0x16

.field public static final MASK_IMDCT_3:I = 0x17

.field public static final MASK_MEMCPY_0:I = 0xc

.field public static final MASK_MEMCPY_1:I = 0xd

.field public static final MASK_MEMCPY_2:I = 0xe

.field public static final MASK_MEMCPY_3:I = 0xf

.field public static final MASK_NEON_0:I = 0x0

.field public static final MASK_NEON_1:I = 0x1

.field public static final MASK_NEON_2:I = 0x2

.field public static final MASK_NEON_3:I = 0x3

.field private static final MAXPOWER_TYPE_7:Ljava/lang/String; = "CA7"

.field private static final MAXPOWER_TYPE_9:Ljava/lang/String; = "CA9"

.field private static final PERCENT:D = 100.0

.field private static final TAG:Ljava/lang/String; = "EM/CpuStress_ApMcu"

.field private static final TEST_ITEM:I = 0x6

.field static final sTestItemFirstMask:[I


# instance fields
.field private mBtnStart:Landroid/widget/Button;

.field private mCbArray:[Landroid/widget/CheckBox;

.field private mEtLoopCount:Landroid/widget/EditText;

.field private mTvArray:[Landroid/widget/TextView;

.field private mTvResult:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/engineermode/cpustress/ApMcu;->sTestItemFirstMask:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x4
        0x8
        0xc
        0x10
        0x14
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;-><init>()V

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    const/4 v0, 0x6

    new-array v0, v0, [Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    const/16 v0, 0x18

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResult:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/engineermode/cpustress/ApMcu;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/ApMcu;

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/ApMcu;->updateTestResult()V

    return-void
.end method

.method private showTestResult(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-gt v0, v1, :cond_2

    const/4 v2, 0x0

    :goto_2
    const/4 v3, 0x6

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    mul-int/lit8 v4, v2, 0x4

    add-int/2addr v4, v0

    aget-object v4, v3, v4

    const/4 v3, 0x1

    mul-int/lit8 v5, v2, 0x4

    add-int/2addr v5, v0

    shl-int/2addr v3, v5

    and-int/2addr v3, p2

    if-nez v3, :cond_0

    const v3, 0x7f080124

    :goto_3
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :pswitch_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x3

    goto :goto_0

    :cond_0
    const v3, 0x7f080123

    goto :goto_3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateCbStatus(I)V
    .locals 6
    .param p1    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v4, v1, v0

    const/16 v1, 0xf

    mul-int/lit8 v5, v0, 0x4

    shl-int/2addr v1, v5

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_1

    :cond_1
    return-void
.end method

.method private updateStartUi()V
    .locals 4

    const v3, 0x7f080122

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    const v2, 0x7f08013a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResult:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private updateTestCount(IIIIIIIIIIII)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # I
    .param p12    # I

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz p1, :cond_0

    const/4 v2, 0x4

    sget v3, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v2, v3, :cond_6

    const v2, 0x7f08013e

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    int-to-double v5, p2

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    int-to-double v7, p1

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    if-eqz p3, :cond_1

    const v2, 0x7f08013f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    int-to-double v5, p4

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    int-to-double v7, p3

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    if-eqz p5, :cond_2

    const v2, 0x7f080140

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    int-to-double v5, p6

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    int-to-double v7, p5

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    if-eqz p7, :cond_3

    const v2, 0x7f080141

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    move/from16 v0, p8

    int-to-double v5, v0

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    move/from16 v0, p7

    int-to-double v7, v0

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_3
    if-eqz p9, :cond_4

    const v2, 0x7f080142

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p10 .. p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    move/from16 v0, p10

    int-to-double v5, v0

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    move/from16 v0, p9

    int-to-double v7, v0

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_4
    if-eqz p11, :cond_5

    const v2, 0x7f080143

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    move/from16 v0, p12

    int-to-double v5, v0

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    move/from16 v0, p11

    int-to-double v7, v0

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_5
    const-string v2, "EM/CpuStress_ApMcu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "test result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResult:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_6
    const v2, 0x7f08013d

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    int-to-double v5, p2

    const-wide/high16 v7, 0x4059000000000000L

    mul-double/2addr v5, v7

    int-to-double v7, p1

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0
.end method

.method private updateTestResult()V
    .locals 28

    const-string v1, "EM/CpuStress_ApMcu"

    const-string v2, "Enter updateTestResult"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->getData()Landroid/os/Bundle;

    move-result-object v26

    if-eqz v26, :cond_0

    const-string v1, "result_neon"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v1, "result_ca9"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v1, "result_dhry"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    const-string v1, "result_memcpy"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    const-string v1, "result_fdct"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    const-string v1, "result_imdct"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    const-string v1, "run"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v1, "loopcount"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string v1, "mask"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v1, "result"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v12}, Lcom/mediatek/engineermode/cpustress/ApMcu;->updateTestUi(ZJIIIIIIII)V

    const-string v1, "result_pass_neon"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v15

    const-string v1, "result_pass_ca9"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v17

    const-string v1, "result_pass_dhry"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v19

    const-string v1, "result_pass_memcpy"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v21

    const-string v1, "result_pass_fdct"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v23

    const-string v1, "result_pass_imdct"

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v13, p0

    move v14, v7

    move/from16 v16, v8

    move/from16 v18, v9

    move/from16 v20, v10

    move/from16 v22, v11

    move/from16 v24, v12

    invoke-direct/range {v13 .. v25}, Lcom/mediatek/engineermode/cpustress/ApMcu;->updateTestCount(IIIIIIIIIIII)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v27

    const-string v1, "EM/CpuStress_ApMcu"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateTestUI NullPointerException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateTestUi(ZJIIIIIIII)V
    .locals 5
    .param p1    # Z
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # I

    const-string v2, "EM/CpuStress_ApMcu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateTestUI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    if-nez p1, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    if-eqz p1, :cond_4

    const v2, 0x7f08013a

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0, p4}, Lcom/mediatek/engineermode/cpustress/ApMcu;->updateCbStatus(I)V

    if-gtz p6, :cond_0

    if-gtz p7, :cond_0

    if-gtz p8, :cond_0

    if-gtz p9, :cond_0

    if-gtz p10, :cond_0

    if-lez p11, :cond_1

    :cond_0
    const/4 v2, 0x2

    sget v3, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v2, v3, :cond_e

    sget v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_2
    const/4 v0, 0x0

    :goto_3
    const/4 v2, 0x6

    if-ge v0, v2, :cond_13

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-nez v2, :cond_12

    :cond_2
    const/4 v1, 0x0

    :goto_4
    const/4 v2, 0x4

    if-ge v1, v2, :cond_12

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    mul-int/lit8 v3, v0, 0x4

    add-int/2addr v3, v1

    aget-object v2, v2, v3

    const v3, 0x7f080122

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const v2, 0x7f080139

    goto :goto_1

    :pswitch_0
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    and-int/lit8 v2, p5, 0x1

    if-nez v2, :cond_5

    const v2, 0x7f080124

    :goto_5
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v3, v2, v3

    and-int/lit8 v2, p5, 0x10

    if-nez v2, :cond_6

    const v2, 0x7f080124

    :goto_6
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x2

    :goto_7
    sget-object v2, Lcom/mediatek/engineermode/cpustress/ApMcu;->sTestItemFirstMask:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    mul-int/lit8 v3, v0, 0x4

    aget-object v3, v2, v3

    const/4 v2, 0x1

    sget-object v4, Lcom/mediatek/engineermode/cpustress/ApMcu;->sTestItemFirstMask:[I

    aget v4, v4, v0

    shl-int/2addr v2, v4

    and-int/2addr v2, p5

    if-nez v2, :cond_7

    const v2, 0x7f080124

    :goto_8
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_5
    const v2, 0x7f080123

    goto :goto_5

    :cond_6
    const v2, 0x7f080123

    goto :goto_6

    :cond_7
    const v2, 0x7f080123

    goto :goto_8

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    and-int/lit8 v2, p5, 0x1

    if-nez v2, :cond_8

    const v2, 0x7f080124

    :goto_9
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v3, v2, v3

    and-int/lit8 v2, p5, 0x10

    if-nez v2, :cond_9

    const v2, 0x7f080124

    :goto_a
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v3, 0x1

    aget-object v3, v2, v3

    and-int/lit8 v2, p5, 0x2

    if-nez v2, :cond_a

    const v2, 0x7f080124

    :goto_b
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v3, 0x5

    aget-object v3, v2, v3

    and-int/lit8 v2, p5, 0x20

    if-nez v2, :cond_b

    const v2, 0x7f080124

    :goto_c
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x2

    :goto_d
    sget-object v2, Lcom/mediatek/engineermode/cpustress/ApMcu;->sTestItemFirstMask:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    mul-int/lit8 v3, v0, 0x4

    aget-object v3, v2, v3

    const/4 v2, 0x1

    sget-object v4, Lcom/mediatek/engineermode/cpustress/ApMcu;->sTestItemFirstMask:[I

    aget v4, v4, v0

    shl-int/2addr v2, v4

    and-int/2addr v2, p5

    if-nez v2, :cond_c

    const v2, 0x7f080124

    :goto_e
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    mul-int/lit8 v3, v0, 0x4

    add-int/lit8 v3, v3, 0x1

    aget-object v3, v2, v3

    const/4 v2, 0x1

    sget-object v4, Lcom/mediatek/engineermode/cpustress/ApMcu;->sTestItemFirstMask:[I

    aget v4, v4, v0

    add-int/lit8 v4, v4, 0x1

    shl-int/2addr v2, v4

    and-int/2addr v2, p5

    if-nez v2, :cond_d

    const v2, 0x7f080124

    :goto_f
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_8
    const v2, 0x7f080123

    goto :goto_9

    :cond_9
    const v2, 0x7f080123

    goto :goto_a

    :cond_a
    const v2, 0x7f080123

    goto :goto_b

    :cond_b
    const v2, 0x7f080123

    goto :goto_c

    :cond_c
    const v2, 0x7f080123

    goto :goto_e

    :cond_d
    const v2, 0x7f080123

    goto :goto_f

    :cond_e
    const/4 v2, 0x4

    sget v3, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sCoreNumber:I

    if-ne v2, v3, :cond_f

    sget v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->sIndexMode:I

    invoke-direct {p0, v2, p5}, Lcom/mediatek/engineermode/cpustress/ApMcu;->showTestResult(II)V

    goto/16 :goto_2

    :cond_f
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    and-int/lit8 v2, p5, 0x1

    if-nez v2, :cond_10

    const v2, 0x7f080124

    :goto_10
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v3, v2, v3

    and-int/lit8 v2, p5, 0x10

    if-nez v2, :cond_11

    const v2, 0x7f080124

    :goto_11
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    :cond_10
    const v2, 0x7f080123

    goto :goto_10

    :cond_11
    const v2, 0x7f080123

    goto :goto_11

    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    :cond_13
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-boolean v2, v2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mWantStopApmcu:Z

    if-nez v2, :cond_14

    const/16 v2, 0x3e9

    invoke-virtual {p0, v2}, Landroid/app/Activity;->removeDialog(I)V

    :cond_14
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x6

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v3, 0xf

    mul-int/lit8 v4, v2, 0x4

    shl-int/2addr v3, v4

    :goto_1
    or-int/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    const-string v3, "EM/CpuStress_ApMcu"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCheckChanged, mask: 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "mask"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v3, v1}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateData(Landroid/os/Bundle;)Landroid/os/Bundle;

    :cond_2
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v7, 0x0

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    if-ne v6, v8, :cond_3

    const-string v6, "EM/CpuStress_ApMcu"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is clicked"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f080139

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-wide/16 v1, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v6, "loopcount"

    invoke-virtual {v3, v6, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const/4 v0, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v6, 0x6

    if-ge v4, v6, :cond_1

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v6, v6, v4

    invoke-virtual {v6}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v6, 0xf

    mul-int/lit8 v8, v4, 0x4

    shl-int/2addr v6, v8

    :goto_1
    or-int/2addr v0, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v5

    const-string v6, "EM/CpuStress_ApMcu"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Loopcount string: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v6, 0x7f08011e

    invoke-static {p0, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    :goto_2
    return-void

    :cond_0
    move v6, v7

    goto :goto_1

    :cond_1
    const-string v6, "mask"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v6, v3}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->startTest(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/ApMcu;->updateStartUi()V

    goto :goto_2

    :cond_2
    const/16 v6, 0x3e9

    invoke-virtual {p0, v6}, Landroid/app/Activity;->showDialog(I)V

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v6}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->stopTest()V

    goto :goto_2

    :cond_3
    const-string v6, "EM/CpuStress_ApMcu"

    const-string v7, "Unknown event"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030039

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const v2, 0x7f0b0195

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mEtLoopCount:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    const v2, 0x7f0b0196

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    aput-object v2, v3, v6

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    const v2, 0x7f0b019b

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    aput-object v2, v3, v5

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    const v2, 0x7f0b01a0

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    aput-object v2, v3, v8

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    const v2, 0x7f0b01a5

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    aput-object v2, v3, v9

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    const/4 v4, 0x4

    const v2, 0x7f0b01aa

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    const/4 v4, 0x5

    const v2, 0x7f0b01af

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    aput-object v2, v3, v4

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const v2, 0x7f0b0197

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v6

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const v2, 0x7f0b0198

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v5

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const v2, 0x7f0b0199

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v8

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const v2, 0x7f0b019a

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v9

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v4, 0x4

    const v2, 0x7f0b019c

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v4, 0x5

    const v2, 0x7f0b019d

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v4, 0x6

    const v2, 0x7f0b019e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/4 v4, 0x7

    const v2, 0x7f0b019f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const v2, 0x7f0b01a1

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v7

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0x9

    const v2, 0x7f0b01a2

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0xa

    const v2, 0x7f0b01a3

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0xb

    const v2, 0x7f0b01a4

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0xc

    const v2, 0x7f0b01a6

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0xd

    const v2, 0x7f0b01a7

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0xe

    const v2, 0x7f0b01a8

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0xf

    const v2, 0x7f0b01a9

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0x10

    const v2, 0x7f0b01ab

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0x11

    const v2, 0x7f0b01ac

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0x12

    const v2, 0x7f0b01ad

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0x13

    const v2, 0x7f0b01ae

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0x14

    const v2, 0x7f0b01b0

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0x15

    const v2, 0x7f0b01b1

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0x16

    const v2, 0x7f0b01b2

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    const/16 v4, 0x17

    const v2, 0x7f0b01b3

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    const v2, 0x7f0b01b4

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    const v2, 0x7f0b01b5

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvResult:Landroid/widget/TextView;

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x6

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v2, v2, v1

    invoke-virtual {v2, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v2, 0x10

    invoke-static {}, Lcom/mediatek/engineermode/ChipSupport;->getChip()I

    move-result v3

    if-le v2, v3, :cond_2

    const/16 v1, 0x8

    :goto_1
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mTvArray:[Landroid/widget/TextView;

    aget-object v2, v2, v1

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x2

    :goto_2
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v2, v2, v1

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v2, v2, v5

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v3, v3, v5

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CA9"

    const-string v5, "CA7"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ApMcu;->mCbArray:[Landroid/widget/CheckBox;

    aget-object v2, v2, v6

    const v3, 0x7f080133

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    new-instance v2, Lcom/mediatek/engineermode/cpustress/ApMcu$1;

    invoke-direct {v2, p0}, Lcom/mediatek/engineermode/cpustress/ApMcu$1;-><init>(Lcom/mediatek/engineermode/cpustress/ApMcu;)V

    iput-object v2, p0, Lcom/mediatek/engineermode/cpustress/CpuStressCommon;->mHandler:Landroid/os/Handler;

    return-void
.end method
