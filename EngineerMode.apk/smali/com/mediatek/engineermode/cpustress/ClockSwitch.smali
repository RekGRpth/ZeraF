.class public Lcom/mediatek/engineermode/cpustress/ClockSwitch;
.super Landroid/app/Activity;
.source "ClockSwitch.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;


# static fields
.field private static final COMMAND_GET_ALL_PLL:Ljava/lang/String; = "cat /proc/clkmgr/pll_fsel"

.field private static final CPU_MAX_FREQ_CMD:Ljava/lang/String; = "cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"

.field private static final CPU_MAX_FREQ_FORMAT:Ljava/lang/String; = "%1.1f"

.field private static final CPU_MAX_FREQ_ORI:Ljava/lang/String; = "1"

.field private static final CPU_SS_DEBUG_MODE:Ljava/lang/String; = "/proc/cpu_ss/cpu_ss_debug_mode"

.field private static final CPU_SS_MODE:Ljava/lang/String; = "/proc/cpu_ss/cpu_ss_mode"

.field private static final CPU_SS_PERIOD:Ljava/lang/String; = "/proc/cpu_ss/cpu_ss_period"

.field private static final CPU_SS_PERIOD_MODE:Ljava/lang/String; = "/proc/cpu_ss/cpu_ss_period_mode"

.field private static final DEFAULT_NSECOND:Ljava/lang/String; = "0"

.field private static final DEFAULT_SECOND:Ljava/lang/String; = "1"

.field private static final DIALOG_WAIT:I = 0x1

.field private static final FILES:[Ljava/lang/String;

.field private static final GET_COMMAND_FORMAT:Ljava/lang/String; = "cat %1$s"

.field private static final INDEX_QUERY_DEBUG_MODE:I = 0xd

.field private static final INDEX_QUERY_MODE:I = 0xa

.field private static final INDEX_QUERY_PERIOD:I = 0xb

.field private static final INDEX_QUERY_PERIOD_MODE:I = 0xc

.field private static final INDEX_SET_DEBUG_MODE:I = 0x3

.field private static final INDEX_SET_DEBUG_MODE_VALUE_D:Ljava/lang/String; = "disable"

.field private static final INDEX_SET_DEBUG_MODE_VALUE_E:Ljava/lang/String; = "enable"

.field private static final INDEX_SET_MODE:I = 0x0

.field private static final INDEX_SET_MODE_VALUE_0:Ljava/lang/String; = "0"

.field private static final INDEX_SET_MODE_VALUE_1:Ljava/lang/String; = "1"

.field private static final INDEX_SET_PERIOD:I = 0x1

.field private static final INDEX_SET_PERIOD_MODE:I = 0x2

.field private static final INDEX_SET_PERIOD_MODE_VALUE_D:Ljava/lang/String; = "disable"

.field private static final INDEX_SET_PERIOD_MODE_VALUE_E:Ljava/lang/String; = "enable"

.field private static final INDEX_SET_QUERY_DELTA:I = 0xa

.field private static final INDEX_UPDATE_PLL:I = 0x12

.field private static final MHZ:D = 1000000.0

.field private static final PLL_NAME_ARM:Ljava/lang/String; = "ARMPLL"

.field private static final PLL_VALUE_PATTERN:Ljava/lang/String; = "^[0-9a-fA-F]{1,16}$"

.field private static final PLL_VALUE_PRE:Ljava/lang/String; = "-1"

.field private static final QUERY_ALL_RECORD_MASK:I = 0xf

.field private static final REGEX_PLL_GROUP:Ljava/lang/String; = "\\[[\\s\\S]*?\\]"

.field private static final SET_COMMAND_FORMAT:Ljava/lang/String; = "echo %1$s > %2$s"

.field private static final TAG:Ljava/lang/String; = "EM/CpuStress_ClockSwitch"


# instance fields
.field private mArmPllId:I

.field private mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

.field private mBtnSet:Landroid/widget/Button;

.field private mBtnStart:Landroid/widget/Button;

.field private mBtnSwitchG:Landroid/widget/Button;

.field private mBtnSwitchM:Landroid/widget/Button;

.field private mCbDebugMsgEnable:Landroid/widget/CheckBox;

.field private mEtArmPllValue:Landroid/widget/EditText;

.field private mEtNSecond:Landroid/widget/EditText;

.field private mEtSecond:Landroid/widget/EditText;

.field private final mHandler:Landroid/os/Handler;

.field private mQueryRecordMask:I

.field private mTvDebugMsgEnable:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/proc/cpu_ss/cpu_ss_mode"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/proc/cpu_ss/cpu_ss_period"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "/proc/cpu_ss/cpu_ss_period_mode"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "/proc/cpu_ss/cpu_ss_debug_mode"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->FILES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mQueryRecordMask:I

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mCbDebugMsgEnable:Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mTvDebugMsgEnable:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchM:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtSecond:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtNSecond:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtArmPllValue:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSet:Landroid/widget/Button;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mArmPllId:I

    new-instance v0, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/cpustress/ClockSwitch$1;-><init>(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->FILES:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    iget v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mQueryRecordMask:I

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/engineermode/cpustress/ClockSwitch;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->getResponse(I)V

    return-void
.end method

.method static synthetic access$176(Lcom/mediatek/engineermode/cpustress/ClockSwitch;I)I
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mQueryRecordMask:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mQueryRecordMask:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/cpustress/ClockSwitch;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->updatePeriodView(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/engineermode/cpustress/ClockSwitch;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->updateAutoTestView(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mCbDebugMsgEnable:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtArmPllValue:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSet:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->updateArmPll()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/engineermode/cpustress/ClockSwitch;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->getCurrentStatus(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/engineermode/cpustress/ClockSwitch;Ljava/lang/String;I)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/cpustress/ClockSwitch;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->setCommand(Ljava/lang/String;I)V

    return-void
.end method

.method private getCurrentStatus(I)V
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, "EM/CpuStress_ClockSwitch"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCurrentStatus: index is error, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->getStatus(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getResponse(I)V
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, "EM/CpuStress_ClockSwitch"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getResponse: index is error, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->getStatus(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getStatus(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x1

    const-string v4, "EM/CpuStress_ClockSwitch"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Enter getStatus: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    const-string v4, "cat %1$s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->FILES:[Ljava/lang/String;

    rem-int/lit8 v8, p1, 0xa

    aget-object v7, v7, v8

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "EM/CpuStress_ClockSwitch"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCommand: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/IllegalFormatException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    if-eqz v0, :cond_0

    const v4, 0x7f080150

    invoke-static {p0, v4, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :goto_1
    return-void

    :catch_0
    move-exception v2

    const/4 v0, 0x1

    const-string v4, "EM/CpuStress_ClockSwitch"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Command format NullPointerException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v2

    const/4 v0, 0x1

    const-string v4, "EM/CpuStress_ClockSwitch"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Command format IllegalFormatException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    monitor-enter p0

    :try_start_1
    invoke-static {v1}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    :catch_2
    move-exception v2

    :try_start_3
    const-string v4, "EM/CpuStress_ClockSwitch"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exec command IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method private handleEvent(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v0, "EM/CpuStress_ClockSwitch"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/engineermode/cpustress/ClockSwitch$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/mediatek/engineermode/cpustress/ClockSwitch$3;-><init>(Lcom/mediatek/engineermode/cpustress/ClockSwitch;Ljava/lang/String;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private setArmPll()V
    .locals 10

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtArmPllValue:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, "^[0-9a-fA-F]{1,16}$"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "echo "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mArmPllId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " > /proc/clkmgr/pll_fsel"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    :try_start_0
    invoke-static {v0}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_0

    const/4 v5, 0x1

    :cond_0
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Set ARMPLL value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v5, :cond_1

    const-string v6, "success"

    :goto_1
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    :goto_2
    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->updateArmPll()V

    return-void

    :catch_0
    move-exception v1

    const-string v6, "EM/CpuStress_ClockSwitch"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setArmPll IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v6, "fail"

    goto :goto_1

    :cond_2
    const-string v6, "Input ARMPLL error"

    invoke-static {p0, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method

.method private setCommand(Ljava/lang/String;I)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    const-string v3, "echo %1$s > %2$s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->FILES:[Ljava/lang/String;

    rem-int/lit8 v7, p2, 0xa

    aget-object v6, v6, v7

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "EM/CpuStress_ClockSwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setCommand: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/IllegalFormatException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    if-eqz v0, :cond_0

    const v3, 0x7f080150

    invoke-static {p0, v3, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_1
    return-void

    :catch_0
    move-exception v2

    const/4 v0, 0x1

    const-string v3, "EM/CpuStress_ClockSwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Command format NullPointerException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v2

    const/4 v0, 0x1

    const-string v3, "EM/CpuStress_ClockSwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Command format IllegalFormatException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    monitor-enter p0

    :try_start_1
    invoke-static {v1}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    :catch_2
    move-exception v2

    :try_start_3
    const-string v3, "EM/CpuStress_ClockSwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exec command IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method private updateArmPll()V
    .locals 14

    const/4 v10, 0x1

    const/4 v8, 0x0

    const/4 v0, 0x0

    const-string v9, ""

    :try_start_0
    const-string v11, "cat /proc/clkmgr/pll_fsel"

    invoke-static {v11}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I

    move-result v11

    if-nez v11, :cond_4

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v8

    const-string v11, "\\[[\\s\\S]*?\\]"

    invoke-static {v11}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v11, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v1, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    if-nez v3, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    iput v11, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mArmPllId:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-ne v10, v3, :cond_2

    move-object v6, v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move-object v9, v1

    const/4 v3, 0x0

    const-string v11, "ARMPLL"

    invoke-virtual {v6, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    const-string v11, "EM/CpuStress_ClockSwitch"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "find ARMPLL: id "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mArmPllId:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " value "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    if-eqz v9, :cond_4

    const/4 v0, 0x1

    const-string v11, "-1"

    invoke-virtual {v9, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_5

    const-string v9, "-1"
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_1
    iget-object v11, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mHandler:Landroid/os/Handler;

    const/16 v12, 0x12

    invoke-virtual {v11, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    if-eqz v0, :cond_6

    :goto_2
    iput v10, v5, Landroid/os/Message;->arg1:I

    iput-object v9, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v10, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mHandler:Landroid/os/Handler;

    invoke-virtual {v10, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_5
    const/4 v11, 0x2

    :try_start_1
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v9, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v9

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v11, "EM/CpuStress_ClockSwitch"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "updateArmPll IOException: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    const/4 v10, 0x0

    goto :goto_2
.end method

.method private updateAutoTestView(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08014c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtSecond:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtNSecond:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchM:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnStart:Landroid/widget/Button;

    const v1, 0x7f08014b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtSecond:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtNSecond:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchM:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updatePeriodView(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/16 v6, 0x28

    const/4 v5, -0x1

    const-string v2, "EM/CpuStress_ClockSwitch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Enter updatePeriodView: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-eq v5, v0, :cond_0

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtSecond:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const/16 v2, 0x29

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    if-eq v5, v0, :cond_1

    if-eq v5, v1, :cond_1

    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtNSecond:Landroid/widget/EditText;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->isClockSwitchRun()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtSecond:Landroid/widget/EditText;

    const-string v3, "1"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtNSecond:Landroid/widget/EditText;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtSecond:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtSecond:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v2, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtNSecond:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtNSecond:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    return-void
.end method

.method private updateSwitchView(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchM:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchM:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mCbDebugMsgEnable:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    const-string v0, "EM/CpuStress_ClockSwitch"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CheckBox is checked: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_1

    const-string v0, "enable"

    :goto_0
    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->handleEvent(Ljava/lang/String;I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "disable"

    goto :goto_0

    :cond_2
    const-string v0, "EM/CpuStress_ClockSwitch"

    const-string v1, "Unknown event"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mTvDebugMsgEnable:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_1

    const-string v4, "EM/CpuStress_ClockSwitch"

    const-string v5, "TextView is clicked"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mCbDebugMsgEnable:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CompoundButton;->performClick()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_3

    const-string v4, "EM/CpuStress_ClockSwitch"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is clicked"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08014b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtSecond:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtNSecond:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v8}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->handleEvent(Ljava/lang/String;I)V

    const-string v4, "enable"

    invoke-direct {p0, v4, v9}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->handleEvent(Ljava/lang/String;I)V

    invoke-direct {p0, v8}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->updateAutoTestView(Z)V

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v4, v5}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->startTest(Landroid/os/Bundle;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v4, "EM/CpuStress_ClockSwitch"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "Time period value error"

    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_2
    const-string v4, "disable"

    invoke-direct {p0, v4, v9}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->handleEvent(Ljava/lang/String;I)V

    invoke-direct {p0, v7}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->updateAutoTestView(Z)V

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v4}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->stopTest()V

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-virtual {v4}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->updateWakeLock()V

    goto/16 :goto_0

    :cond_3
    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchM:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_4

    const-string v4, "EM/CpuStress_ClockSwitch"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchM:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is clicked"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "0"

    invoke-direct {p0, v4, v7}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->handleEvent(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_4
    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_5

    const-string v4, "EM/CpuStress_ClockSwitch"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is clicked"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "1"

    invoke-direct {p0, v4, v7}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->handleEvent(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_5
    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_6

    const-string v4, "EM/CpuStress_ClockSwitch"

    const-string v5, "Set ARM pll is clicked"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->setArmPll()V

    goto/16 :goto_0

    :cond_6
    const-string v4, "EM/CpuStress_ClockSwitch"

    const-string v5, "Unknown event"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/16 v5, 0x8

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f03003a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const v3, 0x7f0b01b7

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mCbDebugMsgEnable:Landroid/widget/CheckBox;

    const v3, 0x7f0b01b6

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mTvDebugMsgEnable:Landroid/widget/TextView;

    const v3, 0x7f0b01ba

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnStart:Landroid/widget/Button;

    const v3, 0x7f0b01bb

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchM:Landroid/widget/Button;

    const v3, 0x7f0b01bc

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    const v3, 0x7f0b01b8

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtSecond:Landroid/widget/EditText;

    const v3, 0x7f0b01b9

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtNSecond:Landroid/widget/EditText;

    const v3, 0x7f0b01be

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtArmPllValue:Landroid/widget/EditText;

    const v3, 0x7f0b01bf

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSet:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mCbDebugMsgEnable:Landroid/widget/CheckBox;

    invoke-virtual {v3, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mTvDebugMsgEnable:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnStart:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchM:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v3, 0x10

    invoke-static {}, Lcom/mediatek/engineermode/ChipSupport;->getChip()I

    move-result v4

    if-gt v3, v4, :cond_1

    :try_start_0
    const-string v3, "cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"

    invoke-static {v3}, Lcom/mediatek/engineermode/ShellExe;->execCommand(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/mediatek/engineermode/ShellExe;->getOutput()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const-string v3, "%1.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    int-to-double v6, v1

    const-wide v8, 0x412e848000000000L

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSwitchG:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v3, "EM/CpuStress_ClockSwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Get max freq IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const v3, 0x7f0b01bd

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mEtArmPllValue:Landroid/widget/EditText;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    const/4 v0, 0x0

    if-ne p1, v2, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080118

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f080119

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08011a

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f08011b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "EM/CpuStress_ClockSwitch"

    const-string v1, "Enter onServiceConnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    check-cast p2, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;

    invoke-virtual {p2}, Lcom/mediatek/engineermode/cpustress/CpuStressTestService$StressTestBinder;->getService()Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iget-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    iput-object p0, v0, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;->mTestClass:Lcom/mediatek/engineermode/cpustress/CpuStressTestService$ICpuStressTestComplete;

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "EM/CpuStress_ClockSwitch"

    const-string v1, "Enter onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mBoundService:Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    return-void
.end method

.method public onStart()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->showDialog(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/engineermode/cpustress/ClockSwitch;->mQueryRecordMask:I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/engineermode/cpustress/ClockSwitch$2;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/cpustress/ClockSwitch$2;-><init>(Lcom/mediatek/engineermode/cpustress/ClockSwitch;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/engineermode/cpustress/CpuStressTestService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, p0, v2}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-virtual {p0, p0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onUpdateTestResult()V
    .locals 2

    const-string v0, "EM/CpuStress_ClockSwitch"

    const-string v1, "Enter onupdateTestResult"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
