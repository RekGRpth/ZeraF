.class public Lcom/mediatek/engineermode/io/MsdcDrivSet;
.super Lcom/mediatek/engineermode/io/MsdcTest;
.source "MsdcDrivSet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DATA_BIT:I = 0xf

.field private static final OFFSET_EIGHT_BIT:I = 0x8

.field private static final OFFSET_FOUR_BIT:I = 0x4

.field private static final OFFSET_SIXTEEN_BIT:I = 0x10

.field private static final OFFSET_TWELVE_BIT:I = 0xc

.field private static final OFFSET_TWENTY_BIT:I = 0x14

.field private static final TAG:Ljava/lang/String; = "MSDC_IOCTL"


# instance fields
.field private mBtnGet:Landroid/widget/Button;

.field private mBtnSet:Landroid/widget/Button;

.field private mClkPdIndex:I

.field private mClkPdSpinner:Landroid/widget/Spinner;

.field private mClkPuIndex:I

.field private mClkPuSpinner:Landroid/widget/Spinner;

.field private mCmdPdIndex:I

.field private mCmdPdSpinner:Landroid/widget/Spinner;

.field private mCmdPuIndex:I

.field private mCmdPuSpinner:Landroid/widget/Spinner;

.field private mDataPdSpinner:Landroid/widget/Spinner;

.field private mDataPdndex:I

.field private mDataPuIndex:I

.field private mDataPuSpinner:Landroid/widget/Spinner;

.field private mHostIndex:I

.field private mHostSpinner:Landroid/widget/Spinner;

.field private mIsNewChip:Z

.field private final mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/mediatek/engineermode/io/MsdcTest;-><init>()V

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuIndex:I

    iput v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdndex:I

    iput-boolean v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mIsNewChip:Z

    new-instance v0, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;

    invoke-direct {v0, p0}, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;-><init>(Lcom/mediatek/engineermode/io/MsdcDrivSet;)V

    iput-object v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostIndex:I

    return p1
.end method

.method static synthetic access$102(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuIndex:I

    return p1
.end method

.method static synthetic access$202(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdIndex:I

    return p1
.end method

.method static synthetic access$302(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuIndex:I

    return p1
.end method

.method static synthetic access$402(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdIndex:I

    return p1
.end method

.method static synthetic access$502(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuIndex:I

    return p1
.end method

.method static synthetic access$602(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/io/MsdcDrivSet;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdndex:I

    return p1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 19
    .param p1    # Landroid/view/View;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_2

    const-string v1, "MSDC_IOCTL"

    const-string v2, "SD_IOCTL: click GetCurrent"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostIndex:I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/io/EmGpio;->newGetCurrent(II)I

    move-result v17

    const/4 v1, -0x1

    move/from16 v0, v17

    if-eq v0, v1, :cond_1

    and-int/lit8 v12, v17, 0xf

    shr-int/lit8 v1, v17, 0x4

    and-int/lit8 v11, v1, 0xf

    shr-int/lit8 v1, v17, 0x8

    and-int/lit8 v14, v1, 0xf

    shr-int/lit8 v1, v17, 0xc

    and-int/lit8 v13, v1, 0xf

    shr-int/lit8 v1, v17, 0x10

    and-int/lit8 v16, v1, 0xf

    shr-int/lit8 v1, v17, 0x14

    and-int/lit8 v15, v1, 0xf

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v12}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v14}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuSpinner:Landroid/widget/Spinner;

    move/from16 v0, v16

    invoke-virtual {v1, v0}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mIsNewChip:Z

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v11}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v13}, Landroid/widget/AbsSpinner;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v15}, Landroid/widget/AbsSpinner;->setSelection(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x6e

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostIndex:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuIndex:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdIndex:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuIndex:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdIndex:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuIndex:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdndex:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/mediatek/engineermode/io/EmGpio;->newSetCurrent(IIIIIIIIII)Z

    move-result v18

    if-eqz v18, :cond_3

    const/16 v1, 0x64

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_3
    const/16 v1, 0x65

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v8, 0x7f03005a

    invoke-virtual {p0, v8}, Landroid/app/Activity;->setContentView(I)V

    const/16 v8, 0x10

    invoke-static {}, Lcom/mediatek/engineermode/ChipSupport;->getChip()I

    move-result v9

    if-gt v8, v9, :cond_0

    const/4 v8, 0x1

    :goto_0
    iput-boolean v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mIsNewChip:Z

    const v8, 0x7f0b02f0

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnGet:Landroid/widget/Button;

    const v8, 0x7f0b02f1

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnSet:Landroid/widget/Button;

    const v8, 0x7f0b02e3

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostSpinner:Landroid/widget/Spinner;

    const v8, 0x7f0b02e5

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuSpinner:Landroid/widget/Spinner;

    const v8, 0x7f0b02e7

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdSpinner:Landroid/widget/Spinner;

    const v8, 0x7f0b02e9

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuSpinner:Landroid/widget/Spinner;

    const v8, 0x7f0b02eb

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdSpinner:Landroid/widget/Spinner;

    const v8, 0x7f0b02ed

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuSpinner:Landroid/widget/Spinner;

    const v8, 0x7f0b02ef

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    iput-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdSpinner:Landroid/widget/Spinner;

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnGet:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mBtnSet:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f06003b

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_1
    array-length v8, v3

    add-int/lit8 v8, v8, -0x1

    if-ge v2, v8, :cond_1

    aget-object v8, v3, v2

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_1
    const-string v8, "MSDC_IOCTL"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "New chip? "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mIsNewChip:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mIsNewChip:Z

    if-eqz v8, :cond_2

    array-length v8, v3

    add-int/lit8 v8, v8, -0x1

    aget-object v8, v3, v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-boolean v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mIsNewChip:Z

    if-eqz v8, :cond_3

    const/4 v7, 0x0

    const v8, 0x7f0b02e4

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f0800ec

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    const v8, 0x7f0b02e8

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f0800ed

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    const v8, 0x7f0b02ec

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f0800ee

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    const v8, 0x7f0b02e6

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    const v8, 0x7f0b02ea

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    const v8, 0x7f0b02ee

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdSpinner:Landroid/widget/Spinner;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdSpinner:Landroid/widget/Spinner;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdSpinner:Landroid/widget/Spinner;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    const/16 v8, 0x20

    invoke-static {v8}, Lcom/mediatek/engineermode/ChipSupport;->isCurrentChipEquals(I)Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v5, v8, -0x1

    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v9, 0x2

    if-le v8, v9, :cond_4

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    :cond_4
    const v8, 0x7f0b02e8

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuSpinner:Landroid/widget/Spinner;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    const v8, 0x7f0b02ec

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuSpinner:Landroid/widget/Spinner;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    const v8, 0x7f0b02e4

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const v10, 0x7f0800ec

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " / "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const v10, 0x7f0800ed

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " / "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const v10, 0x7f0800ee

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v8, 0x1090008

    invoke-direct {v1, p0, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const v8, 0x1090009

    invoke-virtual {v1, v8}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v8, 0x1090008

    const v9, 0x7f06003f

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, p0, v8, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v8, 0x1090009

    invoke-virtual {v0, v8}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPuSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mClkPdSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPuSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mCmdPdSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPuSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mDataPdSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v8, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet;->mHostSpinner:Landroid/widget/Spinner;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void
.end method
