.class Lcom/mediatek/engineermode/io/MsdcDrivSet$1;
.super Ljava/lang/Object;
.source "MsdcDrivSet.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/engineermode/io/MsdcDrivSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/engineermode/io/MsdcDrivSet;


# direct methods
.method constructor <init>(Lcom/mediatek/engineermode/io/MsdcDrivSet;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;->this$0:Lcom/mediatek/engineermode/io/MsdcDrivSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;->this$0:Lcom/mediatek/engineermode/io/MsdcDrivSet;

    invoke-static {v0, p3}, Lcom/mediatek/engineermode/io/MsdcDrivSet;->access$002(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;->this$0:Lcom/mediatek/engineermode/io/MsdcDrivSet;

    invoke-static {v0, p3}, Lcom/mediatek/engineermode/io/MsdcDrivSet;->access$102(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;->this$0:Lcom/mediatek/engineermode/io/MsdcDrivSet;

    invoke-static {v0, p3}, Lcom/mediatek/engineermode/io/MsdcDrivSet;->access$202(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;->this$0:Lcom/mediatek/engineermode/io/MsdcDrivSet;

    invoke-static {v0, p3}, Lcom/mediatek/engineermode/io/MsdcDrivSet;->access$302(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;->this$0:Lcom/mediatek/engineermode/io/MsdcDrivSet;

    invoke-static {v0, p3}, Lcom/mediatek/engineermode/io/MsdcDrivSet;->access$402(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;->this$0:Lcom/mediatek/engineermode/io/MsdcDrivSet;

    invoke-static {v0, p3}, Lcom/mediatek/engineermode/io/MsdcDrivSet;->access$502(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/mediatek/engineermode/io/MsdcDrivSet$1;->this$0:Lcom/mediatek/engineermode/io/MsdcDrivSet;

    invoke-static {v0, p3}, Lcom/mediatek/engineermode/io/MsdcDrivSet;->access$602(Lcom/mediatek/engineermode/io/MsdcDrivSet;I)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0b02e3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    const-string v0, "MSDC_IOCTL"

    const-string v1, "Spinner nothing selected"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
