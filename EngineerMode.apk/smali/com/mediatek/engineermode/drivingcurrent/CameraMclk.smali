.class public Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;
.super Landroid/app/Activity;
.source "CameraMclk.java"


# static fields
.field private static final FILE_NAME:Ljava/lang/String; = "mclkdriving"

.field private static final FILE_PATH:Ljava/lang/String; = "/data/data/com.mediatek.engineermode/sharefile"

.field private static final TAG:Ljava/lang/String; = "EM/CameraMCLK"


# instance fields
.field private mCurrentMCLK:Ljava/lang/String;

.field private mCurrentSpinner:Landroid/widget/Spinner;

.field private mFile:Ljava/io/File;

.field private mSwitchBtn:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "0"

    iput-object v0, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mCurrentMCLK:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mCurrentMCLK:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;

    invoke-direct {p0}, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->writeFile()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;)V
    .locals 0
    .param p0    # Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;

    invoke-direct {p0}, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->deleteFile()V

    return-void
.end method

.method private deleteFile()V
    .locals 4

    new-instance v1, Ljava/io/File;

    const-string v2, "/data/data/com.mediatek.engineermode/sharefile"

    const-string v3, "mclkdriving"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mFile:Ljava/io/File;

    iget-object v1, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "EM/CameraMCLK"

    const-string v2, "delete file error"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/data/com.mediatek.engineermode/sharefile"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "EM/CameraMCLK"

    const-string v2, "delete direct error"

    invoke-static {v1, v2}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private initListeners()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mCurrentSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk$1;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk$1;-><init>(Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mSwitchBtn:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk$2;

    invoke-direct {v1, p0}, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk$2;-><init>(Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private writeFile()V
    .locals 7

    const-string v4, "EM/CameraMCLK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "writeFile() currentMCLK :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mCurrentMCLK:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    const-string v4, "/data/data/com.mediatek.engineermode/sharefile"

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "EM/CameraMCLK"

    const-string v5, "Make dir error!"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v4, Ljava/io/File;

    const-string v5, "/data/data/com.mediatek.engineermode/sharefile"

    const-string v6, "mclkdriving"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mFile:Ljava/io/File;

    iget-object v4, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    const/4 v2, 0x0

    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mFile:Ljava/io/File;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v4, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mCurrentMCLK:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v2, v3

    :goto_1
    return-void

    :catch_0
    move-exception v1

    const-string v4, "EM/CameraMCLK"

    const-string v5, "create file error"

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    :goto_2
    const-string v4, "EM/CameraMCLK"

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_2
    move-exception v1

    :goto_3
    const-string v4, "EM/CameraMCLK"

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/engineermode/Elog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_3
    move-exception v1

    move-object v2, v3

    goto :goto_3

    :catch_4
    move-exception v1

    move-object v2, v3

    goto :goto_2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030026

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f0b011e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mCurrentSpinner:Landroid/widget/Spinner;

    const v0, 0x7f0b011f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mSwitchBtn:Landroid/widget/Button;

    const-string v0, "0"

    iput-object v0, p0, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->mCurrentMCLK:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/engineermode/drivingcurrent/CameraMclk;->initListeners()V

    return-void
.end method
