.class public Lcom/mediatek/nfc/dynamicload/NativeDynamicLoad;
.super Ljava/lang/Object;
.source "NativeDynamicLoad.java"


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "NativeDynamicLoad"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    :try_start_0
    const-string v1, "mtknfc_dynamic_load_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "NativeDynamicLoad"

    const-string v2, "NfcDynamicLoad library not found!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native doQueryVersion()I
.end method

.method public static queryVersion()I
    .locals 2

    const-string v0, "NativeDynamicLoad"

    const-string v1, "query version"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "NativeDynamicLoad"

    const-string v1, "NOT SUPPORT MTK NFC !"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    return v0
.end method
