.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/AssetResource;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$7000()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    return-object v0
.end method


# virtual methods
.method public addChild(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7202(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPurchase(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7300(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7302(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7300(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addTrailerId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7402(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7202(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7300(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7300(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7302(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7402(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBadge()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getBadge()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v0

    return-object v0
.end method

.method public getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v0

    return-object v0
.end method

.method public getParent()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getParent()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    return-object v0
.end method

.method public getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    return-object v0
.end method

.method public getVideo()Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getVideo()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method

.method public hasBadge()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasBadge()Z

    move-result v0

    return v0
.end method

.method public hasMetadata()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata()Z

    move-result v0

    return v0
.end method

.method public hasParent()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasParent()Z

    move-result v0

    return v0
.end method

.method public hasResourceId()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId()Z

    move-result v0

    return v0
.end method

.method public hasVideo()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasVideo()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeBadge(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasBadge()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v1

    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->newBuilder(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8202(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    :goto_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasBadge:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8102(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8202(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->hasResourceId()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->setResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->hasParent()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->getParent()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->setParent(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->addChild(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->hasMetadata()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->setMetadata(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->addPurchase(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    goto :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->hasVideo()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->getVideo()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    :cond_4
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->setVideo(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    goto/16 :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->addTrailerId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    goto/16 :goto_0

    :sswitch_8
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->hasBadge()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->getBadge()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;

    :cond_5
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->setBadge(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->mergeResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getParent()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->mergeParent(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    :cond_3
    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7202(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_5
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->mergeMetadata(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    :cond_6
    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7300(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7300(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7302(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;

    :cond_7
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7300(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7300(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_8
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasBadge()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getBadge()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->mergeBadge(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    :cond_9
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasVideo()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getVideo()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->mergeVideo(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    :cond_a
    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7402(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;

    :cond_b
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public mergeMetadata(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8000(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8000(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v1

    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->newBuilder(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8002(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    :goto_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7902(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8002(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    goto :goto_0
.end method

.method public mergeParent(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7800(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7800(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7802(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    :goto_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasParent:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7702(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7802(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    goto :goto_0
.end method

.method public mergeResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7600(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7600(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7602(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    :goto_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7502(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7602(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    goto :goto_0
.end method

.method public mergeVideo(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v1

    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->newBuilder(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8402(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    :goto_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasVideo:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8302(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8402(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    goto :goto_0
.end method

.method public setBadge(Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasBadge:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8102(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8202(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    return-object p0
.end method

.method public setMetadata(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7902(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8002(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    return-object p0
.end method

.method public setMetadata(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7902(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8002(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    return-object p0
.end method

.method public setParent(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasParent:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7702(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7802(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object p0
.end method

.method public setResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7502(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7602(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object p0
.end method

.method public setResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7502(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$7602(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object p0
.end method

.method public setVideo(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasVideo:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8302(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/AssetResource;->video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->access$8402(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-object p0
.end method
