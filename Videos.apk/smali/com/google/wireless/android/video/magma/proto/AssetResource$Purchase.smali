.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Purchase"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;


# instance fields
.field private hasIsPreferred:Z

.field private hasPurchaseStatus:Z

.field private hasPurchaseTimestampSec:Z

.field private hasPurchaseToken:Z

.field private hasPurchaseType:Z

.field private hasRentalExpirationTimestampSec:Z

.field private hasRentalLongTimerSec:Z

.field private hasRentalShortTimerSec:Z

.field private isPreferred_:Z

.field private memoizedSerializedSize:I

.field private purchaseStatus_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

.field private purchaseTimestampSec_:J

.field private purchaseToken_:Ljava/lang/String;

.field private purchaseType_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

.field private rentalExpirationTimestampSec_:J

.field private rentalLongTimerSec_:I

.field private rentalShortTimerSec_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred_:Z

    iput-wide v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec_:J

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec_:I

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec_:I

    iput-wide v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1    # Z

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred_:Z

    iput-wide v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec_:J

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec_:I

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec_:I

    iput-wide v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$4602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasIsPreferred:Z

    return p1
.end method

.method static synthetic access$4702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred_:Z

    return p1
.end method

.method static synthetic access$4802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseType:Z

    return p1
.end method

.method static synthetic access$4902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    return-object p1
.end method

.method static synthetic access$5002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseStatus:Z

    return p1
.end method

.method static synthetic access$5102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    return-object p1
.end method

.method static synthetic access$5202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseTimestampSec:Z

    return p1
.end method

.method static synthetic access$5302(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec_:J

    return-wide p1
.end method

.method static synthetic access$5402(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalLongTimerSec:Z

    return p1
.end method

.method static synthetic access$5502(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec_:I

    return p1
.end method

.method static synthetic access$5602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalShortTimerSec:Z

    return p1
.end method

.method static synthetic access$5702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec_:I

    return p1
.end method

.method static synthetic access$5802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalExpirationTimestampSec:Z

    return p1
.end method

.method static synthetic access$5902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec_:J

    return-wide p1
.end method

.method static synthetic access$6002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseToken:Z

    return p1
.end method

.method static synthetic access$6102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->RENTAL:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->INACTIVE:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;->access$4400()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getIsPreferred()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred_:Z

    return v0
.end method

.method public getPurchaseStatus()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    return-object v0
.end method

.method public getPurchaseTimestampSec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec_:J

    return-wide v0
.end method

.method public getPurchaseToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken_:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    return-object v0
.end method

.method public getRentalExpirationTimestampSec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec_:J

    return-wide v0
.end method

.method public getRentalLongTimerSec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec_:I

    return v0
.end method

.method public getRentalShortTimerSec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasIsPreferred()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getIsPreferred()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseType()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseStatus()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseStatus()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseTimestampSec()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseTimestampSec()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalLongTimerSec()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getRentalLongTimerSec()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalShortTimerSec()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getRentalShortTimerSec()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalExpirationTimestampSec()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getRentalExpirationTimestampSec()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseToken()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->memoizedSerializedSize:I

    move v1, v0

    goto/16 :goto_0
.end method

.method public hasIsPreferred()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasIsPreferred:Z

    return v0
.end method

.method public hasPurchaseStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseStatus:Z

    return v0
.end method

.method public hasPurchaseTimestampSec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseTimestampSec:Z

    return v0
.end method

.method public hasPurchaseToken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseToken:Z

    return v0
.end method

.method public hasPurchaseType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseType:Z

    return v0
.end method

.method public hasRentalExpirationTimestampSec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalExpirationTimestampSec:Z

    return v0
.end method

.method public hasRentalLongTimerSec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalLongTimerSec:Z

    return v0
.end method

.method public hasRentalShortTimerSec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalShortTimerSec:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasIsPreferred()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getIsPreferred()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseType()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseStatus()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseStatus()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Status;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseTimestampSec()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseTimestampSec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalLongTimerSec()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getRentalLongTimerSec()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalShortTimerSec()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getRentalShortTimerSec()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasRentalExpirationTimestampSec()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getRentalExpirationTimestampSec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->hasPurchaseToken()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->getPurchaseToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_7
    return-void
.end method
