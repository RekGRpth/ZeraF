.class public final Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "WishlistItemListResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
        "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->buildParsed()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->create()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;-><init>(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    return-object v0
.end method


# virtual methods
.method public addResource(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$300(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$302(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$300(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addResourceIds(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$400(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$402(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$400(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->build()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$300(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$300(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$302(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$400(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$400(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$402(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->create()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->addResourceIds(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->addResource(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1f7a -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$300(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$300(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$302(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$300(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$300(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$400(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$400(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$402(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse$Builder;->result:Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$400(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->access$400(Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
