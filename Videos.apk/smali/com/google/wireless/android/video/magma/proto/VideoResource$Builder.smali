.class public final Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "VideoResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/VideoResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
        "Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/wireless/android/video/magma/proto/VideoResource;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;)Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->buildParsed()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->create()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;-><init>()V

    new-instance v1, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource;-><init>(Lcom/google/wireless/android/video/magma/proto/VideoResource$1;)V

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-object v0
.end method


# virtual methods
.method public addFormat(Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2100(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2102(Lcom/google/wireless/android/video/magma/proto/VideoResource;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2100(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPinnedOnDevice(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2200(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2202(Lcom/google/wireless/android/video/magma/proto/VideoResource;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2200(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->build()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2100(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2100(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2102(Lcom/google/wireless/android/video/magma/proto/VideoResource;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2200(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2200(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2202(Lcom/google/wireless/android/video/magma/proto/VideoResource;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->create()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->clone()Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    return-object v0
.end method

.method public getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    return-object v0
.end method

.method public hasPlayback()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback()Z

    move-result v0

    return v0
.end method

.method public hasResourceId()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->hasResourceId()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->setResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Format$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->addFormat(Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->addPinnedOnDevice(Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->hasPlayback()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->setPlayback(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->setViewerStarted(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->setViewerLastStarted(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->mergeResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    :cond_2
    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2100(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2100(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2102(Lcom/google/wireless/android/video/magma/proto/VideoResource;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2100(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->format_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2100(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_4
    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2200(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2200(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2202(Lcom/google/wireless/android/video/magma/proto/VideoResource;Ljava/util/List;)Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2200(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2200(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_6
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->mergePlayback(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerStarted()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getViewerStarted()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->setViewerStarted(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerLastStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getViewerLastStarted()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->setViewerLastStarted(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;

    goto/16 :goto_0
.end method

.method public mergePlayback(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2600(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2600(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v1

    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->newBuilder(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2602(Lcom/google/wireless/android/video/magma/proto/VideoResource;Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    :goto_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2502(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2602(Lcom/google/wireless/android/video/magma/proto/VideoResource;Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    goto :goto_0
.end method

.method public mergeResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2400(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # getter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2400(Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    invoke-static {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->buildPartial()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2402(Lcom/google/wireless/android/video/magma/proto/VideoResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    :goto_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2302(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2402(Lcom/google/wireless/android/video/magma/proto/VideoResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    goto :goto_0
.end method

.method public setPlayback(Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2502(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback_:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2602(Lcom/google/wireless/android/video/magma/proto/VideoResource;Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    return-object p0
.end method

.method public setResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2302(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2402(Lcom/google/wireless/android/video/magma/proto/VideoResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object p0
.end method

.method public setResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasResourceId:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2302(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    invoke-static {v0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2402(Lcom/google/wireless/android/video/magma/proto/VideoResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object p0
.end method

.method public setViewerLastStarted(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerLastStarted:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2902(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted_:J
    invoke-static {v0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$3002(Lcom/google/wireless/android/video/magma/proto/VideoResource;J)J

    return-object p0
.end method

.method public setViewerStarted(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v1, 0x1

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasViewerStarted:Z
    invoke-static {v0, v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2702(Lcom/google/wireless/android/video/magma/proto/VideoResource;Z)Z

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Builder;->result:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    # setter for: Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted_:J
    invoke-static {v0, p1, p2}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->access$2802(Lcom/google/wireless/android/video/magma/proto/VideoResource;J)J

    return-object p0
.end method
