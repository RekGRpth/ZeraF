.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Metadata"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;


# instance fields
.field private contentRatings_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;",
            ">;"
        }
    .end annotation
.end field

.field private credits_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;",
            ">;"
        }
    .end annotation
.end field

.field private description_:Ljava/lang/String;

.field private durationSec_:I

.field private hasDescription:Z

.field private hasDurationSec:Z

.field private hasReleaseDateTimestampSec:Z

.field private hasSequenceNumber:Z

.field private hasTitle:Z

.field private images_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private poster_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;",
            ">;"
        }
    .end annotation
.end field

.field private releaseDateTimestampSec_:J

.field private sequenceNumber_:Ljava/lang/String;

.field private title_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec_:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec_:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$2900(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3000(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3302(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasTitle:Z

    return p1
.end method

.method static synthetic access$3402(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3502(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDescription:Z

    return p1
.end method

.method static synthetic access$3602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasReleaseDateTimestampSec:Z

    return p1
.end method

.method static synthetic access$3802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec_:J

    return-wide p1
.end method

.method static synthetic access$3902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDurationSec:Z

    return p1
.end method

.method static synthetic access$4002(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec_:I

    return p1
.end method

.method static synthetic access$4102(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasSequenceNumber:Z

    return p1
.end method

.method static synthetic access$4202(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->access$2700()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContentRatingsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings_:Ljava/util/List;

    return-object v0
.end method

.method public getCreditsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits_:Ljava/util/List;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getDurationSec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec_:I

    return v0
.end method

.method public getImages(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    return-object v0
.end method

.method public getImagesCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getImagesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images_:Ljava/util/List;

    return-object v0
.end method

.method public getPoster(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    return-object v0
.end method

.method public getPosterCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPosterList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster_:Ljava/util/List;

    return-object v0
.end method

.method public getReleaseDateTimestampSec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec_:J

    return-wide v0
.end method

.method public getSequenceNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasTitle()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDescription()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasReleaseDateTimestampSec()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getReleaseDateTimestampSec()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDurationSec()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDurationSec()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getPosterList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v4, 0x5

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getImagesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v4, 0x6

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasSequenceNumber()Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getSequenceNumber()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getCreditsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    const/16 v4, 0x8

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_3

    :cond_8
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getContentRatingsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    const/16 v4, 0x9

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_4

    :cond_9
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->memoizedSerializedSize:I

    move v3, v2

    goto/16 :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDescription:Z

    return v0
.end method

.method public hasDurationSec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDurationSec:Z

    return v0
.end method

.method public hasReleaseDateTimestampSec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasReleaseDateTimestampSec:Z

    return v0
.end method

.method public hasSequenceNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasSequenceNumber:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasTitle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDescription()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasReleaseDateTimestampSec()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getReleaseDateTimestampSec()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasDurationSec()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDurationSec()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getPosterList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getImagesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasSequenceNumber()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getSequenceNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getCreditsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getContentRatingsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    :cond_8
    return-void
.end method
