.class public final enum Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;
.super Ljava/lang/Enum;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

.field public static final enum EST:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

.field public static final enum RENTAL:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    const-string v1, "RENTAL"

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->RENTAL:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    const-string v1, "EST"

    invoke-direct {v0, v1, v2, v2, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->EST:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    new-array v0, v4, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->RENTAL:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->EST:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->$VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type$1;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type$1;-><init>()V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->index:I

    iput p4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->RENTAL:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->EST:Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;
    .locals 1

    const-class v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->$VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    invoke-virtual {v0}, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase$Type;->value:I

    return v0
.end method
