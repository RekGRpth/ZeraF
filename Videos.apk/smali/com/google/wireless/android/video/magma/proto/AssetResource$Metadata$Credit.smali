.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Credit"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;


# instance fields
.field private hasName:Z

.field private hasRole:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private role_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1602(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasName:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasRole:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->ACTOR:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;->access$1400()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getRole()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasRole()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->getRole()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasName:Z

    return v0
.end method

.method public hasRole()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasRole:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->hasRole()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->getRole()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    return-void
.end method
