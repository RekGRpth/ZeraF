.class public final Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AssetResourceId.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/AssetResourceId$1;,
        Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;,
        Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;


# instance fields
.field private hasId:Z

.field private hasMid:Z

.field private hasType:Z

.field private id_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private mid_:Ljava/lang/String;

.field private type_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$302(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasId:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasMid:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasType:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->MUSIC_ALBUM:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->access$100()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->mergeFrom(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getMid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasType()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasMid()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getMid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getType()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    return-object v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasId:Z

    return v0
.end method

.method public hasMid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasMid:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasType:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasType()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getMid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_2
    return-void
.end method
