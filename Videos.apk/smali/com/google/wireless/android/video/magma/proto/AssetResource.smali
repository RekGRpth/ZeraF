.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/AssetResource$1;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource;


# instance fields
.field private badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

.field private child_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResourceId;",
            ">;"
        }
    .end annotation
.end field

.field private hasBadge:Z

.field private hasMetadata:Z

.field private hasParent:Z

.field private hasResourceId:Z

.field private hasVideo:Z

.field private memoizedSerializedSize:I

.field private metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

.field private parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field private purchase_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;",
            ">;"
        }
    .end annotation
.end field

.field private resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field private trailerId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResourceId;",
            ">;"
        }
    .end annotation
.end field

.field private video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>(Z)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AndroidVideoData;->internalForceInit()V

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource$1;)V
    .locals 0
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$1;

    invoke-direct {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$7200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$7202(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$7300(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$7302(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$7400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$7402(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$7502(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId:Z

    return p1
.end method

.method static synthetic access$7600(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method

.method static synthetic access$7602(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object p1
.end method

.method static synthetic access$7702(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasParent:Z

    return p1
.end method

.method static synthetic access$7800(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method

.method static synthetic access$7802(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object p1
.end method

.method static synthetic access$7902(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata:Z

    return p1
.end method

.method static synthetic access$8000(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    return-object v0
.end method

.method static synthetic access$8002(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    return-object p1
.end method

.method static synthetic access$8102(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasBadge:Z

    return p1
.end method

.method static synthetic access$8200(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    return-object v0
.end method

.method static synthetic access$8202(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    return-object p1
.end method

.method static synthetic access$8302(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasVideo:Z

    return p1
.end method

.method static synthetic access$8400(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 1
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-object v0
.end method

.method static synthetic access$8402(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/VideoResource;)Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 0
    .param p0    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iput-object p1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->defaultInstance:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getDefaultInstance()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-void
.end method

.method public static newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    .locals 1

    # invokes: Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->create()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->access$7000()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBadge()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    return-object v0
.end method

.method public getChildList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResourceId;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child_:Ljava/util/List;

    return-object v0
.end method

.method public getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata_:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    return-object v0
.end method

.method public getParent()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method

.method public getPurchaseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase_:Ljava/util/List;

    return-object v0
.end method

.method public getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId_:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasParent()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getParent()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getChildList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getPurchaseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v4, 0x5

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasVideo()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getVideo()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getTrailerIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v4, 0x7

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasBadge()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getBadge()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_8
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->memoizedSerializedSize:I

    move v3, v2

    goto/16 :goto_0
.end method

.method public getTrailerIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResourceId;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId_:Ljava/util/List;

    return-object v0
.end method

.method public getVideo()Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video_:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    return-object v0
.end method

.method public hasBadge()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasBadge:Z

    return v0
.end method

.method public hasMetadata()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata:Z

    return v0
.end method

.method public hasParent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasParent:Z

    return v0
.end method

.method public hasResourceId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId:Z

    return v0
.end method

.method public hasVideo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasVideo:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasParent()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getParent()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getChildList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getPurchaseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasVideo()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getVideo()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getTrailerIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasBadge()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getBadge()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_7
    return-void
.end method
