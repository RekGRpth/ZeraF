.class public final enum Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;
.super Ljava/lang/Enum;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Role"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

.field public static final enum ACTOR:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

.field public static final enum DIRECTOR:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

.field public static final enum PRODUCER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

.field public static final enum WRITER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    const-string v1, "ACTOR"

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->ACTOR:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    const-string v1, "DIRECTOR"

    invoke-direct {v0, v1, v2, v2, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->DIRECTOR:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    const-string v1, "PRODUCER"

    invoke-direct {v0, v1, v3, v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->PRODUCER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    const-string v1, "WRITER"

    invoke-direct {v0, v1, v4, v4, v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->WRITER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    new-array v0, v6, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->ACTOR:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->DIRECTOR:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->PRODUCER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->WRITER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->$VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    new-instance v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role$1;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role$1;-><init>()V

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->index:I

    iput p4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->ACTOR:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->DIRECTOR:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->PRODUCER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->WRITER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;
    .locals 1

    const-class v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    return-object v0
.end method

.method public static values()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;
    .locals 1

    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->$VALUES:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    invoke-virtual {v0}, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit$Role;->value:I

    return v0
.end method
