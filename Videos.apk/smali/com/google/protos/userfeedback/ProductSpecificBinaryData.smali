.class public final Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ProductSpecificBinaryData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/userfeedback/ProductSpecificBinaryData$1;,
        Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;


# instance fields
.field private data_:Lcom/google/protobuf/ByteString;

.field private hasData:Z

.field private hasMimeType:Z

.field private hasName:Z

.field private memoizedSerializedSize:I

.field private mimeType_:Ljava/lang/String;

.field private name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;-><init>(Z)V

    sput-object v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->defaultInstance:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    invoke-static {}, Lcom/google/protos/userfeedback/UserfeedbackCommon;->internalForceInit()V

    sget-object v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->defaultInstance:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->mimeType_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->data_:Lcom/google/protobuf/ByteString;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/userfeedback/ProductSpecificBinaryData$1;)V
    .locals 0
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData$1;

    invoke-direct {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->mimeType_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->data_:Lcom/google/protobuf/ByteString;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$302(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasName:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasMimeType:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->mimeType_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasData:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
    .param p1    # Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->data_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
    .locals 1

    sget-object v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->defaultInstance:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    .locals 1

    # invokes: Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->create()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->access$100()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getData()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->data_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->mimeType_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasMimeType()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasData()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getData()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iput v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasData:Z

    return v0
.end method

.method public hasMimeType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasMimeType:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasName:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasMimeType()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasData()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getData()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    return-void
.end method
