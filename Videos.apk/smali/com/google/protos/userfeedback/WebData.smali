.class public final Lcom/google/protos/userfeedback/WebData;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "WebData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/userfeedback/WebData$1;,
        Lcom/google/protos/userfeedback/WebData$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/protos/userfeedback/WebData;


# instance fields
.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/userfeedback/WebData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/userfeedback/WebData;-><init>(Z)V

    sput-object v0, Lcom/google/protos/userfeedback/WebData;->defaultInstance:Lcom/google/protos/userfeedback/WebData;

    invoke-static {}, Lcom/google/protos/userfeedback/UserfeedbackWeb;->internalForceInit()V

    sget-object v0, Lcom/google/protos/userfeedback/WebData;->defaultInstance:Lcom/google/protos/userfeedback/WebData;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/WebData;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/WebData;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/protos/userfeedback/WebData;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/userfeedback/WebData$1;)V
    .locals 0
    .param p1    # Lcom/google/protos/userfeedback/WebData$1;

    invoke-direct {p0}, Lcom/google/protos/userfeedback/WebData;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/WebData;->memoizedSerializedSize:I

    return-void
.end method

.method public static getDefaultInstance()Lcom/google/protos/userfeedback/WebData;
    .locals 1

    sget-object v0, Lcom/google/protos/userfeedback/WebData;->defaultInstance:Lcom/google/protos/userfeedback/WebData;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/protos/userfeedback/WebData$Builder;
    .locals 1

    # invokes: Lcom/google/protos/userfeedback/WebData$Builder;->create()Lcom/google/protos/userfeedback/WebData$Builder;
    invoke-static {}, Lcom/google/protos/userfeedback/WebData$Builder;->access$100()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData$Builder;
    .locals 1
    .param p0    # Lcom/google/protos/userfeedback/WebData;

    invoke-static {}, Lcom/google/protos/userfeedback/WebData;->newBuilder()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/userfeedback/WebData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    iget v0, p0, Lcom/google/protos/userfeedback/WebData;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protos/userfeedback/WebData;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 0
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/WebData;->getSerializedSize()I

    return-void
.end method
