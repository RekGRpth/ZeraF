.class public final Lcom/google/protos/userfeedback/WebData$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "WebData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/WebData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/userfeedback/WebData;",
        "Lcom/google/protos/userfeedback/WebData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/protos/userfeedback/WebData;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/protos/userfeedback/WebData$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/userfeedback/WebData$Builder;->create()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/userfeedback/WebData$Builder;
    .locals 3

    new-instance v0, Lcom/google/protos/userfeedback/WebData$Builder;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/WebData$Builder;-><init>()V

    new-instance v1, Lcom/google/protos/userfeedback/WebData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protos/userfeedback/WebData;-><init>(Lcom/google/protos/userfeedback/WebData$1;)V

    iput-object v1, v0, Lcom/google/protos/userfeedback/WebData$Builder;->result:Lcom/google/protos/userfeedback/WebData;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/WebData$Builder;->build()Lcom/google/protos/userfeedback/WebData;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/userfeedback/WebData;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/WebData$Builder;->result:Lcom/google/protos/userfeedback/WebData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/WebData$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/WebData$Builder;->result:Lcom/google/protos/userfeedback/WebData;

    invoke-static {v0}, Lcom/google/protos/userfeedback/WebData$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/WebData$Builder;->buildPartial()Lcom/google/protos/userfeedback/WebData;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/userfeedback/WebData;
    .locals 3

    iget-object v1, p0, Lcom/google/protos/userfeedback/WebData$Builder;->result:Lcom/google/protos/userfeedback/WebData;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/WebData$Builder;->result:Lcom/google/protos/userfeedback/WebData;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protos/userfeedback/WebData$Builder;->result:Lcom/google/protos/userfeedback/WebData;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/WebData$Builder;->clone()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/WebData$Builder;->clone()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/WebData$Builder;->clone()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/userfeedback/WebData$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/userfeedback/WebData$Builder;->create()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protos/userfeedback/WebData$Builder;->result:Lcom/google/protos/userfeedback/WebData;

    invoke-virtual {v0, v1}, Lcom/google/protos/userfeedback/WebData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/WebData$Builder;->clone()Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/WebData$Builder;->result:Lcom/google/protos/userfeedback/WebData;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/WebData;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/WebData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/WebData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/WebData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/WebData$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/protos/userfeedback/WebData$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    :pswitch_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public mergeFrom(Lcom/google/protos/userfeedback/WebData;)Lcom/google/protos/userfeedback/WebData$Builder;
    .locals 1
    .param p1    # Lcom/google/protos/userfeedback/WebData;

    invoke-static {}, Lcom/google/protos/userfeedback/WebData;->getDefaultInstance()Lcom/google/protos/userfeedback/WebData;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :cond_0
    return-object p0
.end method
