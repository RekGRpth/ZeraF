.class public final Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ProductSpecificBinaryData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/protos/userfeedback/ProductSpecificBinaryData;",
        "Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->create()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    .locals 3

    new-instance v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;-><init>()V

    new-instance v1, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;-><init>(Lcom/google/protos/userfeedback/ProductSpecificBinaryData$1;)V

    iput-object v1, v0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->build()Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    invoke-static {v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->buildPartial()Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/protos/userfeedback/ProductSpecificBinaryData;
    .locals 3

    iget-object v1, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->clone()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->clone()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->clone()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->create()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    invoke-virtual {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->mergeFrom(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->clone()Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    invoke-virtual {v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->setName(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->setData(Lcom/google/protobuf/ByteString;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    .locals 1
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getDefaultInstance()Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->setName(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasMimeType()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->getData()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->setData(Lcom/google/protobuf/ByteString;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;

    goto :goto_0
.end method

.method public setData(Lcom/google/protobuf/ByteString;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/ByteString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasData:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->access$702(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->data_:Lcom/google/protobuf/ByteString;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->access$802(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public setMimeType(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasMimeType:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->access$502(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->mimeType_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->access$602(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    const/4 v1, 0x1

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->access$302(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Z)Z

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificBinaryData$Builder;->result:Lcom/google/protos/userfeedback/ProductSpecificBinaryData;

    # setter for: Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/protos/userfeedback/ProductSpecificBinaryData;->access$402(Lcom/google/protos/userfeedback/ProductSpecificBinaryData;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
