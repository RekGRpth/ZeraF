.class public final Lcom/google/protos/userfeedback/ProductSpecificData;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ProductSpecificData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protos/userfeedback/ProductSpecificData$1;,
        Lcom/google/protos/userfeedback/ProductSpecificData$Builder;,
        Lcom/google/protos/userfeedback/ProductSpecificData$Type;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/protos/userfeedback/ProductSpecificData;


# instance fields
.field private hasKey:Z

.field private hasType:Z

.field private hasValue:Z

.field private key_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private type_:Lcom/google/protos/userfeedback/ProductSpecificData$Type;

.field private value_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/userfeedback/ProductSpecificData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protos/userfeedback/ProductSpecificData;-><init>(Z)V

    sput-object v0, Lcom/google/protos/userfeedback/ProductSpecificData;->defaultInstance:Lcom/google/protos/userfeedback/ProductSpecificData;

    invoke-static {}, Lcom/google/protos/userfeedback/UserfeedbackWeb;->internalForceInit()V

    sget-object v0, Lcom/google/protos/userfeedback/ProductSpecificData;->defaultInstance:Lcom/google/protos/userfeedback/ProductSpecificData;

    invoke-direct {v0}, Lcom/google/protos/userfeedback/ProductSpecificData;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->key_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->value_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/userfeedback/ProductSpecificData$1;)V
    .locals 0
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificData$1;

    invoke-direct {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->key_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->value_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$302(Lcom/google/protos/userfeedback/ProductSpecificData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->hasKey:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/protos/userfeedback/ProductSpecificData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->key_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/protos/userfeedback/ProductSpecificData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->hasValue:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/protos/userfeedback/ProductSpecificData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->value_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/protos/userfeedback/ProductSpecificData;Z)Z
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->hasType:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/protos/userfeedback/ProductSpecificData;Lcom/google/protos/userfeedback/ProductSpecificData$Type;)Lcom/google/protos/userfeedback/ProductSpecificData$Type;
    .locals 0
    .param p0    # Lcom/google/protos/userfeedback/ProductSpecificData;
    .param p1    # Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    iput-object p1, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->type_:Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/protos/userfeedback/ProductSpecificData;
    .locals 1

    sget-object v0, Lcom/google/protos/userfeedback/ProductSpecificData;->defaultInstance:Lcom/google/protos/userfeedback/ProductSpecificData;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/protos/userfeedback/ProductSpecificData$Type;->STRING:Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    iput-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->type_:Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    return-void
.end method

.method public static newBuilder()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    .locals 1

    # invokes: Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->create()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;
    invoke-static {}, Lcom/google/protos/userfeedback/ProductSpecificData$Builder;->access$100()Lcom/google/protos/userfeedback/ProductSpecificData$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->key_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->hasKey()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->hasValue()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->hasType()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->getType()Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protos/userfeedback/ProductSpecificData$Type;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iput v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getType()Lcom/google/protos/userfeedback/ProductSpecificData$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->type_:Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->value_:Ljava/lang/String;

    return-object v0
.end method

.method public hasKey()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->hasKey:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->hasType:Z

    return v0
.end method

.method public hasValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->hasValue:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/userfeedback/ProductSpecificData;->hasKey:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/protos/userfeedback/ProductSpecificData;->getType()Lcom/google/protos/userfeedback/ProductSpecificData$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/userfeedback/ProductSpecificData$Type;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    return-void
.end method
