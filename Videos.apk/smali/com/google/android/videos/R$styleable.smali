.class public final Lcom/google/android/videos/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final EdgeFadingView:[I

.field public static final MediaRouteButton:[I

.field public static final MovieTableView:[I

.field public static final PlayerOverlaysLayout_Layout:[I

.field public static final SuggestionGridLayout:[I

.field public static final SuggestionGridLayout_Layout:[I

.field public static final TabRow:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x3

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/R$styleable;->EdgeFadingView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/videos/R$styleable;->MediaRouteButton:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/videos/R$styleable;->MovieTableView:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010016

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/videos/R$styleable;->PlayerOverlaysLayout_Layout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/videos/R$styleable;->SuggestionGridLayout:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/videos/R$styleable;->SuggestionGridLayout_Layout:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/videos/R$styleable;->TabRow:[I

    return-void

    :array_0
    .array-data 4
        0x10100d4
        0x10100e0
        0x7f01000b
    .end array-data

    :array_1
    .array-data 4
        0x101013f
        0x1010140
        0x7f010001
        0x7f010002
    .end array-data

    :array_2
    .array-data 4
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010009
        0x7f01000a
    .end array-data

    :array_3
    .array-data 4
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
    .end array-data

    :array_4
    .array-data 4
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
    .end array-data

    :array_5
    .array-data 4
        0x1010129
        0x7f010017
        0x7f010018
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
