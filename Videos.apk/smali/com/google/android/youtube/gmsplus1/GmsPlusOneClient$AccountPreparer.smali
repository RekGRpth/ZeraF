.class Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;
.super Ljava/lang/Object;
.source "GmsPlusOneClient.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountPreparer"
.end annotation


# instance fields
.field private final accountName:Ljava/lang/String;

.field private final plusClient:Lcom/google/android/gms/plus/PlusClient;

.field final synthetic this$0:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->this$0:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/plus/PlusClient;

    # getter for: Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->oauth2Scopes:[Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->access$000(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;)[Ljava/lang/String;

    move-result-object v5

    move-object v1, p2

    move-object v2, p3

    move-object v3, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/PlusClient;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    iput-object p3, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->accountName:Ljava/lang/String;

    return-void
.end method

.method private release()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->this$0:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    # getter for: Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->accountPreparers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->access$100(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->accountName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    return-void
.end method


# virtual methods
.method public init()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->this$0:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    # getter for: Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->accountPreparers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->access$100(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->accountName:Ljava/lang/String;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    return-void
.end method

.method public onConnected()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->this$0:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    # getter for: Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->knownEnabledAccounts:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->access$200(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->accountName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->release()V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->plusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->release()V

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->release()V

    return-void
.end method
