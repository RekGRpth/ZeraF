.class public Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;
.super Ljava/lang/Object;
.source "GmsPlusOneClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountStatusCollector;,
        Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;
    }
.end annotation


# instance fields
.field private final accountPreparers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;",
            ">;"
        }
    .end annotation
.end field

.field private final knownEnabledAccounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final oauth2Scopes:[Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 2
    .param p1    # [Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "https://www.googleapis.com/auth/pos"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "oauth2Scopes must contain at least Scope.PLUSONE_SERVICE"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->oauth2Scopes:[Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->knownEnabledAccounts:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->accountPreparers:Ljava/util/Map;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->oauth2Scopes:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->accountPreparers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->knownEnabledAccounts:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public createPlusClient(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/google/android/gms/plus/PlusClient;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p4    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    const-string v1, "context cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "accountName cannot be empty"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "connectionCallbacks cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "onConnectionFailedListener cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/plus/PlusClient;

    iget-object v5, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->oauth2Scopes:[Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/PlusClient;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->knownEnabledAccounts:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountStatusCollector;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountStatusCollector;-><init>(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Lcom/google/android/gms/plus/PlusClient;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    :cond_0
    return-object v0
.end method

.method public isPlusOneEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "accountName cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->knownEnabledAccounts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public prepareForAccount(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "accountName cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->knownEnabledAccounts:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;->accountPreparers:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;-><init>(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient$AccountPreparer;->init()V

    goto :goto_0
.end method
