.class public abstract Lcom/google/android/youtube/core/adapter/GroupingOutline;
.super Lcom/google/android/youtube/core/adapter/OutlineWrapper;
.source "GroupingOutline.java"


# instance fields
.field private final groupSize:I

.field private final viewType:Lcom/google/android/youtube/core/adapter/Outline$ViewType;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/adapter/Outline;Lcom/google/android/youtube/core/adapter/Outline$ViewType;I)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline;
    .param p2    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .param p3    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/OutlineWrapper;-><init>(Lcom/google/android/youtube/core/adapter/Outline;)V

    const-string v0, "viewType cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    iput-object v0, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->viewType:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    if-lez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "groupSize must be positive"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput p3, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->groupSize:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final endIndexOf(I)I
    .locals 2
    .param p1    # I

    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->groupSize:I

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/adapter/Outline;->getVisibleCount()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method protected getAllViewTypes(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->viewType:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCount()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/adapter/Outline;->getVisibleCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->groupSize:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->groupSize:I

    div-int/2addr v0, v1

    return v0
.end method

.method public getGroupSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->groupSize:I

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    iget v1, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->groupSize:I

    mul-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getViewType(I)Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->viewType:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/adapter/Outline;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/adapter/GroupingOutline;->endIndexOf(I)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/adapter/GroupingOutline;->startIndexOf(I)I

    move-result v1

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/adapter/Outline;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected final startIndexOf(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/core/adapter/GroupingOutline;->groupSize:I

    mul-int/2addr v0, p1

    return v0
.end method
