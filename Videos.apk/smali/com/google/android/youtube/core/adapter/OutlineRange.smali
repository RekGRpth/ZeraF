.class public Lcom/google/android/youtube/core/adapter/OutlineRange;
.super Lcom/google/android/youtube/core/adapter/OutlineWrapper;
.source "OutlineRange.java"


# instance fields
.field private actualEnd:I

.field private actualStart:I

.field private final definedEnd:I

.field private final definedStart:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/adapter/Outline;II)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/OutlineWrapper;-><init>(Lcom/google/android/youtube/core/adapter/Outline;)V

    if-ltz p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "start must be non-negative"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    if-lt p3, p2, :cond_1

    :goto_1
    const-string v0, "end must be after start"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput p2, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->definedStart:I

    iput p3, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->definedEnd:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/adapter/OutlineRange;->computeActualRange()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private computeActualRange()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/adapter/Outline;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->definedStart:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->actualStart:I

    iget v1, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->definedEnd:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->actualEnd:I

    return-void
.end method

.method private toWrappedPosition(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->actualStart:I

    add-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method protected getAllViewTypes(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/adapter/Outline;->getAllViewTypes(Ljava/util/Set;)V

    return-void
.end method

.method public getCount()I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->actualEnd:I

    iget v1, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->actualStart:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/OutlineRange;->toWrappedPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/OutlineRange;->toWrappedPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/OutlineRange;->toWrappedPosition(I)I

    move-result v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/youtube/core/adapter/Outline;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewType(I)Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/OutlineRange;->toWrappedPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline;->getViewType(I)Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-result-object v0

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/adapter/Outline;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlineRange;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/OutlineRange;->toWrappedPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method protected onWrappedOutlineChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/adapter/OutlineRange;->computeActualRange()V

    invoke-super {p0}, Lcom/google/android/youtube/core/adapter/OutlineWrapper;->onWrappedOutlineChanged()V

    return-void
.end method
