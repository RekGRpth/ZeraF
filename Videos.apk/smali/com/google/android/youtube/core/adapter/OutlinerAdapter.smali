.class public Lcom/google/android/youtube/core/adapter/OutlinerAdapter;
.super Landroid/widget/BaseAdapter;
.source "OutlinerAdapter.java"

# interfaces
.implements Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;


# instance fields
.field private final hasStableIds:Z

.field private outline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final viewTypes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private viewTypesFinalized:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Z)V
    .locals 0
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;-><init>(Z)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->setViewTypes(Ljava/util/Set;)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3
    .param p1    # Z

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypes:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypes:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/core/adapter/Outline;->IGNORE_VIEW_TYPE:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean p1, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->hasStableIds:Z

    return-void
.end method

.method private setViewTypes(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x1

    iget-boolean v2, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypesFinalized:Z

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    const-string v4, "setViewTypes called after view types are finalized"

    invoke-static {v2, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypes:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypes:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypes:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    iput-boolean v3, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypesFinalized:Z

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/adapter/Outline;->getVisibleCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/adapter/Outline;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/adapter/Outline;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/adapter/Outline;->getViewType(I)Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypes:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown view type \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" at position "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    return v2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/adapter/Outline;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypesFinalized:Z

    const-string v1, "OutlinerAdapter used before view types are known"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->hasStableIds:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/adapter/Outline;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/adapter/Outline;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public final onOutlineChanged(Lcom/google/android/youtube/core/adapter/Outline;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline;

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setOutline(Lcom/google/android/youtube/core/adapter/Outline;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline;

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->hasStableIds:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/adapter/Outline;->hasStableIds()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const-string v2, "outline must have stable IDs as required by this OutlinerAdapter"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/adapter/Outline;->getAllViewTypes(Ljava/util/Set;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypesFinalized:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->viewTypes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    const-string v2, "outline must not use unknown view types"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    :goto_1
    invoke-virtual {p1, p0}, Lcom/google/android/youtube/core/adapter/Outline;->setOnOutlineChangedListener(Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/adapter/Outline;->setOnOutlineChangedListener(Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->notifyDataSetChanged()V

    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->setViewTypes(Ljava/util/Set;)V

    goto :goto_1
.end method
