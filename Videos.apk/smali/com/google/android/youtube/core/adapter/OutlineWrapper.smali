.class public abstract Lcom/google/android/youtube/core/adapter/OutlineWrapper;
.super Lcom/google/android/youtube/core/adapter/Outline;
.source "OutlineWrapper.java"

# interfaces
.implements Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;


# instance fields
.field protected final wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/adapter/Outline;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline;

    invoke-direct {p0}, Lcom/google/android/youtube/core/adapter/Outline;-><init>()V

    const-string v0, "wrappedOutline cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/adapter/Outline;

    iput-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlineWrapper;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {p1, p0}, Lcom/google/android/youtube/core/adapter/Outline;->setOnOutlineChangedListener(Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;)V

    return-void
.end method


# virtual methods
.method public final onOutlineChanged(Lcom/google/android/youtube/core/adapter/Outline;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline;

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/OutlineWrapper;->wrappedOutline:Lcom/google/android/youtube/core/adapter/Outline;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/OutlineWrapper;->onWrappedOutlineChanged()V

    :cond_0
    return-void
.end method

.method protected onWrappedOutlineChanged()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/OutlineWrapper;->notifyOutlineChanged()V

    return-void
.end method
