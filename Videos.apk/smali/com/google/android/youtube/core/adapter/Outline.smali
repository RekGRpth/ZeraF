.class public abstract Lcom/google/android/youtube/core/adapter/Outline;
.super Ljava/lang/Object;
.source "Outline.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;,
        Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    }
.end annotation


# static fields
.field public static final IGNORE_VIEW_TYPE:Lcom/google/android/youtube/core/adapter/Outline$ViewType;


# instance fields
.field private isVisible:Z

.field private listener:Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v1, "IGNORE_VIEW_TYPE"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/adapter/Outline;->IGNORE_VIEW_TYPE:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/adapter/Outline;->isVisible:Z

    return-void
.end method


# virtual methods
.method protected abstract getAllViewTypes(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getCount()I
.end method

.method public abstract getItem(I)Ljava/lang/Object;
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public abstract getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract getViewType(I)Lcom/google/android/youtube/core/adapter/Outline$ViewType;
.end method

.method getVisibleCount()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/adapter/Outline;->isVisible:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/Outline;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/Outline;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " out of range 0.."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/Outline;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/adapter/Outline;->isVisible:Z

    return v0
.end method

.method protected final notifyOutlineChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/Outline;->listener:Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/Outline;->listener:Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;->onOutlineChanged(Lcom/google/android/youtube/core/adapter/Outline;)V

    :cond_0
    return-void
.end method

.method setOnOutlineChangedListener(Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;

    iput-object p1, p0, Lcom/google/android/youtube/core/adapter/Outline;->listener:Lcom/google/android/youtube/core/adapter/Outline$OnOutlineChangedListener;

    return-void
.end method

.method public final setVisible(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/adapter/Outline;->isVisible:Z

    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/Outline;->toggleVisibility()V

    :cond_0
    return-void
.end method

.method public final snapshotInto(Landroid/view/ViewGroup;)V
    .locals 3
    .param p1    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/Outline;->getCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/youtube/core/adapter/Outline;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final toggleVisibility()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/adapter/Outline;->isVisible:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/adapter/Outline;->isVisible:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/core/adapter/Outline;->notifyOutlineChanged()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
