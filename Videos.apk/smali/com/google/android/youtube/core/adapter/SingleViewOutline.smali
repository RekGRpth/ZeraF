.class public abstract Lcom/google/android/youtube/core/adapter/SingleViewOutline;
.super Lcom/google/android/youtube/core/adapter/Outline;
.source "SingleViewOutline.java"


# instance fields
.field private final viewType:Lcom/google/android/youtube/core/adapter/Outline$ViewType;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-direct {p0}, Lcom/google/android/youtube/core/adapter/Outline;-><init>()V

    const-string v0, "viewType cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    iput-object v0, p0, Lcom/google/android/youtube/core/adapter/SingleViewOutline;->viewType:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    return-void
.end method


# virtual methods
.method protected getAllViewTypes(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/core/adapter/Outline$ViewType;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/SingleViewOutline;->viewType:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final getCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getViewType(I)Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/adapter/SingleViewOutline;->viewType:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    return-object v0
.end method
