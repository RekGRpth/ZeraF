.class public final Lcom/google/android/youtube/core/client/AuthDecoratingRequester;
.super Ljava/lang/Object;
.source "AuthDecoratingRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lcom/google/android/youtube/core/async/NetworkRequest;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final authenticatedRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final retryStrategy:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
            "<TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/lang/String;)V
    .locals 1
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;",
            "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
            "<TR;>;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "retryStrategy cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->retryStrategy:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v0, "account cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->account:Ljava/lang/String;

    const-string v0, "targetRequester cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    invoke-static {v0, p3, p2}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;)Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->authenticatedRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method


# virtual methods
.method public request(Lcom/google/android/youtube/core/async/NetworkRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    move-object v0, p1

    iget-object v2, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v3, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->blockingGetUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Could not fetch userAuth"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0, v2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->retryStrategy:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    invoke-interface {v2, v0, v1}, Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;->recreateRequest(Ljava/lang/Object;Lcom/google/android/youtube/videos/accounts/UserAuth;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/NetworkRequest;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->authenticatedRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v3, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;

    invoke-direct {v3, p0, p2, p1}, Lcom/google/android/youtube/core/client/AuthDecoratingRequester$1;-><init>(Lcom/google/android/youtube/core/client/AuthDecoratingRequester;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/core/async/NetworkRequest;)V

    invoke-interface {v2, v0, v3}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    check-cast p1, Lcom/google/android/youtube/core/async/NetworkRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/client/AuthDecoratingRequester;->request(Lcom/google/android/youtube/core/async/NetworkRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
