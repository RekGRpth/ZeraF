.class public Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;
.super Ljava/lang/Object;
.source "BaseVss2ClientFactory.java"

# interfaces
.implements Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final executor:Ljava/util/concurrent/Executor;

.field private final random:Ljava/util/Random;

.field private final statParams:Lcom/google/android/youtube/core/client/StatParams;

.field private final syncPingRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Random;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/client/StatParams;)V
    .locals 2
    .param p1    # Ljava/util/Random;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Lorg/apache/http/client/HttpClient;
    .param p4    # Lcom/google/android/youtube/core/async/DeviceAuthorizer;
    .param p5    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p6    # Lcom/google/android/youtube/core/client/StatParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "httpClient cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    const-string v1, "deviceAuthorizer cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->createSyncPingRequester(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->syncPingRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v0, "executor cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->executor:Ljava/util/concurrent/Executor;

    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v0, "random cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->random:Ljava/util/Random;

    const-string v0, "statParams cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/StatParams;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->statParams:Lcom/google/android/youtube/core/client/StatParams;

    return-void
.end method

.method private static createSyncPingRequester(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;)Lcom/google/android/youtube/core/async/Requester;
    .locals 3
    .param p0    # Lorg/apache/http/client/HttpClient;
    .param p1    # Lcom/google/android/youtube/core/async/DeviceAuthorizer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/youtube/core/async/DeviceAuthorizer;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/async/HttpRequester;

    new-instance v1, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;

    sget-object v2, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v1, v2, p1}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Lcom/google/android/youtube/core/async/DeviceAuthorizer;)V

    sget-object v2, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;->VOID:Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/core/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)V

    return-object v0
.end method

.method private generateRandomId()Ljava/lang/String;
    .locals 2

    const/16 v1, 0xc

    new-array v0, v1, [B

    iget-object v1, p0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->random:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public final createForVideo(Ljava/lang/String;Ljava/lang/String;IZZZ)Lcom/google/android/youtube/core/client/VideoStats2Client;
    .locals 17
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z

    new-instance v1, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->executor:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->syncPingRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v11, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->generateRandomId()Ljava/lang/String;

    move-result-object v12

    if-eqz p6, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->generateRandomId()Ljava/lang/String;

    move-result-object v13

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/client/BaseVss2ClientFactory;->statParams:Lcom/google/android/youtube/core/client/StatParams;

    move-object/from16 v16, v0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-direct/range {v1 .. v16}, Lcom/google/android/youtube/core/client/DefaultVideoStats2Client;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/youtube/core/client/StatParams;)V

    return-object v1

    :cond_0
    const/4 v13, 0x0

    goto :goto_0
.end method
