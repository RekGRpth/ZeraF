.class public Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;
.super Ljava/lang/Object;
.source "GoogleAnalyticsClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/client/AnalyticsClient;


# instance fields
.field private category:Ljava/lang/String;

.field private final executor:Ljava/util/concurrent/Executor;

.field private final inSample:Z

.field private final mainThread:Ljava/lang/Thread;

.field private final tracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # I
    .param p8    # Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/Map;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # I
    .param p8    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v2, "executor can\'t be null"

    invoke-static {p2, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    iput-object v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->executor:Ljava/util/concurrent/Executor;

    const-string v2, "appVersion can\'t be null"

    invoke-static {p4, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v2, "deviceType can\'t be null"

    invoke-static {p3, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v2, "propertyId can\'t be null"

    invoke-static {p5, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    if-lez p6, :cond_2

    const/4 v2, 0x1

    :goto_0
    const-string v3, "updateSeconds must be > 0"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-interface/range {p9 .. p9}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v3, 0x4

    if-gt v2, v3, :cond_3

    const/4 v2, 0x1

    :goto_1
    const-string v3, "up to 4 custom vars are allowed"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    const/4 v2, 0x1

    if-lt p7, v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    const-string v3, "sampleRatio must be >= 1"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-static {p1, p7}, Lcom/google/android/youtube/core/utils/AnalyticsHelper;->inSample(Landroid/content/Context;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->inSample:Z

    iput-object p4, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->category:Ljava/lang/String;

    if-eqz p8, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->category:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->category:Ljava/lang/String;

    :cond_0
    const/4 v2, 0x1

    if-le p7, v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->category:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-s"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->category:Ljava/lang/String;

    :cond_1
    invoke-static {}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->getInstance()Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->tracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->tracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    iget-object v3, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->category:Ljava/lang/String;

    invoke-virtual {v2, p3, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setProductVersion(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->tracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {v2, p5, p6, p1}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->startNewSession(Ljava/lang/String;ILandroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->tracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v3, 0x1

    const-string v4, "Device"

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v4, p3, v5}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;I)Z

    invoke-interface/range {p9 .. p9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v4, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->tracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v2, v3, v6}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;I)Z

    goto :goto_3

    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->mainThread:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->category:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;)Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->tracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    return-object v0
.end method

.method private trackEvent(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->inSample:Z

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->mainThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient$1;-><init>(Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->tracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->category:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public varargs trackEventWithExtras(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    const/4 v3, -0x1

    if-eqz p2, :cond_0

    array-length v2, p2

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->trackEvent(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_1
    array-length v2, p2

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Event dropped. Extras must be in the format <key>, <value>, <key>, <value>... Incorrect: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v2, p2

    mul-int/lit8 v2, v2, 0x7

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_3

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v0, 0x1

    aget-object v2, p2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/youtube/core/client/GoogleAnalyticsClient;->trackEvent(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method
