.class public interface abstract Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;
.super Ljava/lang/Object;
.source "SubtitlesOverlay.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;


# virtual methods
.method public abstract clear()V
.end method

.method public abstract hide()V
.end method

.method public abstract setFontSizeLevel(I)V
.end method

.method public abstract update(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;",
            ">;)V"
        }
    .end annotation
.end method
