.class Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$2;
.super Ljava/lang/Object;
.source "RemoteControllerOverlay.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$2;->this$0:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1    # Landroid/view/animation/Animation;

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$2;->this$0:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->access$000(Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$2;->this$0:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;->HIDDEN:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    # setter for: Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoState:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->access$102(Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$2;->this$0:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setVisibility(I)V

    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1    # Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$2;->this$0:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;->FADING_OUT:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    # setter for: Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoState:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->access$102(Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    return-void
.end method
