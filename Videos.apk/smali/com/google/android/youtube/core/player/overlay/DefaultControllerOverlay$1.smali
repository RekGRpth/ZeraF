.class Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;
.super Ljava/lang/Object;
.source "DefaultControllerOverlay.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/SubtitleDialogHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showTrackSelector(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSubtitleDialogCanceled()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # invokes: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$200(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V

    return-void
.end method

.method public onTrackSelected(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # getter for: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$100(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;->onTrackSelected(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$1;->this$0:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    # invokes: Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V
    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->access$200(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V

    return-void
.end method
