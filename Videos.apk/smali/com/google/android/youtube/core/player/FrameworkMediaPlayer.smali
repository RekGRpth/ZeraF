.class public final Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;
.super Ljava/lang/Object;
.source "FrameworkMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements Lcom/google/android/youtube/core/player/MediaPlayerInterface;


# static fields
.field public static final SUPPORTED_MIME_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

.field private mediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "video/mp4"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "video/3gpp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    const-string v1, "application/x-mpegURL"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->SUPPORTED_MIME_TYPES:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    return-void
.end method


# virtual methods
.method public canDisconnectAtHighWaterMark()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAudioSessionIdV9()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getDuration()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onBufferingUpdate(Lcom/google/android/youtube/core/player/MediaPlayerInterface;I)V

    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onCompletion(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onError(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onInfo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onPrepared(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    :cond_0
    return-void
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onSeekComplete(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    :cond_0
    return-void
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onVideoSizeChanged(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V

    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    return-void
.end method

.method public prepareAsync()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    return-void
.end method

.method public release()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    return-void
.end method

.method public setAudioStreamType(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method public setDataSourceV14(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    return-void
.end method

.method public setScreenOnWhilePlaying(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    return-void
.end method

.method public setVideoScalingModeV16(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setVideoScalingMode(I)V

    return-void
.end method

.method public start()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    return-void
.end method
