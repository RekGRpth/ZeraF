.class public abstract Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;
.super Ljava/lang/Object;
.source "DelegatingMediaPlayer.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;
.implements Lcom/google/android/youtube/core/player/MediaPlayerInterface;


# instance fields
.field private final delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

.field private listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {p1, p0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setListener(Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;)V

    return-void
.end method


# virtual methods
.method public canDisconnectAtHighWaterMark()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->canDisconnectAtHighWaterMark()Z

    move-result v0

    return v0
.end method

.method public getAudioSessionIdV9()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->getAudioSessionIdV9()I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getDuration()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->getDuration()I

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->isPlaying()Z

    move-result v0

    return v0
.end method

.method protected final notifyBufferingUpdate(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onBufferingUpdate(Lcom/google/android/youtube/core/player/MediaPlayerInterface;I)V

    :cond_0
    return-void
.end method

.method protected final notifyCompletion()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onCompletion(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    :cond_0
    return-void
.end method

.method protected final notifyError(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onError(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final notifyInfo(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onInfo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final notifyPrepared()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onPrepared(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    :cond_0
    return-void
.end method

.method protected final notifySeekComplete()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onSeekComplete(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    :cond_0
    return-void
.end method

.method protected final notifyVideoSizeChanged(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;->onVideoSizeChanged(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V

    :cond_0
    return-void
.end method

.method public onBufferingUpdate(Lcom/google/android/youtube/core/player/MediaPlayerInterface;I)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->notifyBufferingUpdate(I)V

    return-void
.end method

.method public onCompletion(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->notifyCompletion()V

    return-void
.end method

.method public onError(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->notifyError(II)Z

    move-result v0

    return v0
.end method

.method public onInfo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->notifyInfo(II)Z

    move-result v0

    return v0
.end method

.method public onPrepared(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->notifyPrepared()V

    return-void
.end method

.method public onSeekComplete(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->notifySeekComplete()V

    return-void
.end method

.method public onVideoSizeChanged(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->notifyVideoSizeChanged(II)V

    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->pause()V

    return-void
.end method

.method public prepareAsync()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->prepareAsync()V

    return-void
.end method

.method public release()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->release()V

    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->seekTo(I)V

    return-void
.end method

.method public setAudioStreamType(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setAudioStreamType(I)V

    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method public setDataSourceV14(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setDataSourceV14(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setDisplay(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    return-void
.end method

.method public setScreenOnWhilePlaying(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setScreenOnWhilePlaying(Z)V

    return-void
.end method

.method public setVideoScalingModeV16(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->setVideoScalingModeV16(I)V

    return-void
.end method

.method public start()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->delegate:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->start()V

    return-void
.end method
