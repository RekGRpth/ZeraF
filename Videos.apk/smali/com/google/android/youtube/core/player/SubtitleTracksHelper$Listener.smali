.class public interface abstract Lcom/google/android/youtube/core/player/SubtitleTracksHelper$Listener;
.super Ljava/lang/Object;
.source "SubtitleTracksHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/player/SubtitleTracksHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onDefaultSubtitleTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
.end method

.method public abstract onSubtitleTracksError()V
.end method

.method public abstract onSubtitleTracksResponse(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onTrackSelectionDisabled()V
.end method

.method public abstract onTrackSelectionEnabled()V
.end method
