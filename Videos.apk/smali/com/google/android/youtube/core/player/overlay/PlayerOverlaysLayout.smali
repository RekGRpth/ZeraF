.class public Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;
.super Landroid/widget/RelativeLayout;
.source "PlayerOverlaysLayout.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/PlayerUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;,
        Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;,
        Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;
    }
.end annotation


# static fields
.field public static final ASPECT_RATIO:F = 1.777f


# instance fields
.field private horizontalOverscan:I

.field private makeSafeForOverscan:Z

.field private final overscanSafeOverlays:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/player/overlay/OverscanSafeOverlay;",
            ">;"
        }
    .end annotation
.end field

.field private playerViewListener:Lcom/google/android/youtube/core/player/PlayerUi$Listener;

.field private systemWindowInsetsListener:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;

.field private verticalOverscan:I

.field private videoView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->overscanSafeOverlays:Ljava/util/List;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setFocusable(Z)V

    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setDescendantFocusability(I)V

    return-void
.end method

.method private updateOverscan()V
    .locals 4

    const/4 v2, 0x0

    const v3, 0x3d99999a

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->makeSafeForOverscan:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-int v1, v1

    :goto_0
    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->horizontalOverscan:I

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->makeSafeForOverscan:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-int v2, v1

    :cond_0
    iput v2, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->verticalOverscan:I

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->overscanSafeOverlays:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->overscanSafeOverlays:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/overlay/OverscanSafeOverlay;

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->horizontalOverscan:I

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->verticalOverscan:I

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/player/overlay/OverscanSafeOverlay;->makeSafeForOverscan(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public varargs addOverlays([Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;)V
    .locals 8
    .param p1    # [Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;

    const/4 v0, 0x0

    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_2

    aget-object v1, p1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;->getView()Landroid/view/View;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Overlay "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " does not provide a View and LayoutParams"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    instance-of v5, v1, Lcom/google/android/youtube/core/player/overlay/OverscanSafeOverlay;

    if-eqz v5, :cond_1

    move-object v3, v1

    check-cast v3, Lcom/google/android/youtube/core/player/overlay/OverscanSafeOverlay;

    iget v5, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->horizontalOverscan:I

    iget v6, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->verticalOverscan:I

    invoke-interface {v3, v5, v6}, Lcom/google/android/youtube/core/player/overlay/OverscanSafeOverlay;->makeSafeForOverscan(II)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->overscanSafeOverlays:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    aget-object v5, p1, v0

    invoke-interface {v5}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;->generateLayoutParams()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v4, v2}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    return v0
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2
    .param p1    # Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->systemWindowInsetsListener:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->systemWindowInsetsListener:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;->onSystemWindowInsets(Landroid/graphics/Rect;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result v1

    return v1
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    const/4 v2, -0x2

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/util/AttributeSet;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 1
    .param p1    # Landroid/util/AttributeSet;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    const/high16 v8, -0x80000000

    const v11, 0x3fe374bc

    const/high16 v10, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    if-ne v6, v10, :cond_0

    if-ne v3, v10, :cond_0

    move v5, v7

    move v2, v4

    :goto_0
    invoke-static {v5, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->resolveSize(II)I

    move-result v5

    invoke-static {v2, p2}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->resolveSize(II)I

    move-result v2

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    return-void

    :cond_0
    if-eq v6, v10, :cond_1

    if-ne v6, v8, :cond_2

    if-nez v3, :cond_2

    :cond_1
    move v5, v7

    int-to-float v8, v5

    div-float/2addr v8, v11

    float-to-int v2, v8

    goto :goto_0

    :cond_2
    if-eq v3, v10, :cond_3

    if-ne v3, v8, :cond_4

    if-nez v6, :cond_4

    :cond_3
    move v2, v4

    int-to-float v8, v2

    mul-float/2addr v8, v11

    float-to-int v5, v8

    goto :goto_0

    :cond_4
    if-ne v6, v8, :cond_6

    if-ne v3, v8, :cond_6

    int-to-float v8, v4

    int-to-float v9, v7

    div-float/2addr v9, v11

    cmpg-float v8, v8, v9

    if-gez v8, :cond_5

    int-to-float v8, v4

    mul-float/2addr v8, v11

    float-to-int v5, v8

    move v2, v4

    goto :goto_0

    :cond_5
    move v5, v7

    int-to-float v8, v7

    div-float/2addr v8, v11

    float-to-int v2, v8

    goto :goto_0

    :cond_6
    const/4 v5, 0x0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->makeSafeForOverscan:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->updateOverscan()V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->playerViewListener:Lcom/google/android/youtube/core/player/PlayerUi$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->playerViewListener:Lcom/google/android/youtube/core/player/PlayerUi$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/PlayerUi$Listener;->onUnhandledTouchEvent()V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setListener(Lcom/google/android/youtube/core/player/PlayerUi$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/PlayerUi$Listener;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->playerViewListener:Lcom/google/android/youtube/core/player/PlayerUi$Listener;

    return-void
.end method

.method public setMakeSafeForOverscan(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->makeSafeForOverscan:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->makeSafeForOverscan:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->updateOverscan()V

    :cond_0
    return-void
.end method

.method public setSystemWindowInsetsListener(Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->systemWindowInsetsListener:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$SystemWindowInsetsListener;

    return-void
.end method

.method public setVideoView(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    const/4 v4, -0x2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->videoView:Landroid/view/View;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v3, "videoView has already been set"

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->videoView:Landroid/view/View;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->addRule(I)V

    invoke-virtual {p0, p1, v2, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method
