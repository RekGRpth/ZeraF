.class public interface abstract Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;
.super Ljava/lang/Object;
.source "MediaPlayerInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/player/MediaPlayerInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onBufferingUpdate(Lcom/google/android/youtube/core/player/MediaPlayerInterface;I)V
.end method

.method public abstract onCompletion(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
.end method

.method public abstract onError(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z
.end method

.method public abstract onInfo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z
.end method

.method public abstract onPrepared(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
.end method

.method public abstract onSeekComplete(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
.end method

.method public abstract onVideoSizeChanged(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V
.end method
