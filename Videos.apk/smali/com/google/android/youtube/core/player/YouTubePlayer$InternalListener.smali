.class Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;
.super Ljava/lang/Object;
.source "YouTubePlayer.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;
.implements Lcom/google/android/youtube/core/player/PlayerSurface$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/player/YouTubePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/YouTubePlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/YouTubePlayer;Lcom/google/android/youtube/core/player/YouTubePlayer$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/YouTubePlayer;
    .param p2    # Lcom/google/android/youtube/core/player/YouTubePlayer$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;-><init>(Lcom/google/android/youtube/core/player/YouTubePlayer;)V

    return-void
.end method

.method private maybePlayVideo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->readyToPlay()Z
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1900(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    move-result-object v1

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2000(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->seekTo(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingInit:Z
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2100(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V
    invoke-static {v1, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2300(Lcom/google/android/youtube/core/player/YouTubePlayer;I)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingInit:Z
    invoke-static {v1, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2102(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2000(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1800(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->setBuffering(Z)V
    invoke-static {v1, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2400(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->playVideo()V

    :cond_4
    return-void
.end method


# virtual methods
.method public onBufferingUpdate(Lcom/google/android/youtube/core/player/MediaPlayerInterface;I)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    move-result-object v1

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->bufferedPercent:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$2500(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/16 v1, 0x5a

    if-le p2, v1, :cond_1

    if-eq v0, p2, :cond_0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    :cond_0
    const/16 p2, 0x64

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    move-result-object v1

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->bufferedPercent:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$2500(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-void
.end method

.method public onCompletion(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->stopNotifying()V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    move-result-object v1

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    move-result-object v1

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->duration:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$200(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/16 v2, 0x64

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyProgress(III)V
    invoke-static {v1, v0, v2, v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2700(Lcom/google/android/youtube/core/player/YouTubePlayer;III)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->playing:Z
    invoke-static {v1, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2802(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/16 v2, 0x8

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V
    invoke-static {v1, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2300(Lcom/google/android/youtube/core/player/YouTubePlayer;I)V

    return-void
.end method

.method public onError(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z
    .locals 6
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I
    .param p3    # I

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->prepared:Z
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1300(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingInit:Z
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2102(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z
    invoke-static {v3, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2202(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MediaPlayer error during prepare [what="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", extra="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    :goto_0
    if-ne p2, v1, :cond_2

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->FATAL_ERROR_CODES:Ljava/util/Set;
    invoke-static {}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2900()Ljava/util/Set;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_4

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # operator++ for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retries:I
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$3008(Lcom/google/android/youtube/core/player/YouTubePlayer;)I

    move-result v3

    if-ge v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrying MediaPlayer error [retry="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retries:I
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$3000(Lcom/google/android/youtube/core/player/YouTubePlayer;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", max="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    const/16 v2, 0x64

    if-ne p2, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;
    invoke-static {v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/PlayerSurface;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/PlayerSurface;->recreateSurface()V

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyRecreatedMediaPlayerDueToError(II)V
    invoke-static {v2, p2, p3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$3100(Lcom/google/android/youtube/core/player/YouTubePlayer;II)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->live:Z
    invoke-static {v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2000(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$3200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->loadLiveVideo(Lcom/google/android/youtube/core/model/Stream;)V

    :goto_2
    return v1

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingInit:Z
    invoke-static {v3, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2102(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2202(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MediaPlayer error during playback [what="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", extra="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;
    invoke-static {v3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$3200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    invoke-static {v4}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    move-result-object v4

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v4}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/player/YouTubePlayer;->loadVideo(Lcom/google/android/youtube/core/model/Stream;I)V

    goto :goto_2

    :cond_4
    const-string v3, "Reporting MediaPlayer error"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingInit:Z
    invoke-static {v3, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2102(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retryingPlay:Z
    invoke-static {v3, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2202(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    iget-object v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->retries:I
    invoke-static {v3, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$3002(Lcom/google/android/youtube/core/player/YouTubePlayer;I)I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->stopVideo()V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyError(II)V
    invoke-static {v2, p2, p3}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$3300(Lcom/google/android/youtube/core/player/YouTubePlayer;II)V

    goto :goto_2
.end method

.method public onInfo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "media player info "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    sparse-switch p2, :sswitch_data_0

    :goto_0
    return v2

    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Buffering data from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->source:Lcom/google/android/youtube/core/model/Stream;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$3200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->setBuffering(Z)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2400(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->setBuffering(Z)V
    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2400(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/16 v1, 0xc

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->notifyListeners(I)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2300(Lcom/google/android/youtube/core/player/YouTubePlayer;I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2bd -> :sswitch_0
        0x2be -> :sswitch_1
        0x385 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPrepared(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->prepared:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1302(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->isWidevineOnGoogleTV()Z
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1400(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->duration:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$200(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/youtube/core/player/MediaPlayerInterface;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->maybePlayVideo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    return-void
.end method

.method public onSeekComplete(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->progressNotifier:Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1500(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;

    move-result-object v1

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->currentPosition:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;->access$100(Lcom/google/android/youtube/core/player/YouTubePlayer$ProgressNotifier;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->notifySeekEnd(I)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$2600(Lcom/google/android/youtube/core/player/YouTubePlayer;I)V

    return-void
.end method

.method public onVideoSizeChanged(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I
    .param p3    # I

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1600(Lcom/google/android/youtube/core/player/YouTubePlayer;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/PlayerSurface;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/PlayerSurface;->setVideoSize(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1800(Lcom/google/android/youtube/core/player/YouTubePlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->videoSizeKnown:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1802(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->maybePlayVideo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->uiHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1700(Lcom/google/android/youtube/core/player/YouTubePlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;-><init>(Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public surfaceChanged()V
    .locals 0

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    return-void
.end method

.method public surfaceCreated()V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->surfaceCreated:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$902(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->pendingStream:Lcom/google/android/youtube/core/model/Stream;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1000(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->pendingStream:Lcom/google/android/youtube/core/model/Stream;
    invoke-static {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1000(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    # invokes: Lcom/google/android/youtube/core/player/YouTubePlayer;->load(Lcom/google/android/youtube/core/model/Stream;)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1100(Lcom/google/android/youtube/core/player/YouTubePlayer;Lcom/google/android/youtube/core/model/Stream;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->pendingStream:Lcom/google/android/youtube/core/model/Stream;
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1002(Lcom/google/android/youtube/core/player/YouTubePlayer;Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    :cond_0
    return-void
.end method

.method public surfaceDestroyed()V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->blockingStopVideo()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/PlayerSurface;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/PlayerSurface;->closeShutter()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->surfaceCreated:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$902(Lcom/google/android/youtube/core/player/YouTubePlayer;Z)Z

    return-void
.end method
