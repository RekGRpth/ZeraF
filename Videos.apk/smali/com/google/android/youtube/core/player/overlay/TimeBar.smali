.class public Lcom/google/android/youtube/core/player/overlay/TimeBar;
.super Landroid/view/View;
.source "TimeBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;
    }
.end annotation


# static fields
.field private static final STATE_DISABLED:[I

.field private static final STATE_ENABLED:[I

.field private static final STATE_PRESSED:[I


# instance fields
.field private final bufferedBar:Landroid/graphics/Rect;

.field private final bufferedPaint:Landroid/graphics/Paint;

.field private bufferedPercent:I

.field private currentTime:I

.field private final listener:Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;

.field private final playedBar:Landroid/graphics/Rect;

.field private final playedPaint:Landroid/graphics/Paint;

.field private final progressBar:Landroid/graphics/Rect;

.field private final progressPaint:Landroid/graphics/Paint;

.field private final scrubber:Landroid/graphics/drawable/StateListDrawable;

.field private scrubberLeft:I

.field private final scrubberPadding:I

.field private scrubberTime:I

.field private scrubberTop:I

.field private scrubbing:Z

.field private scrubbingEnabled:Z

.field private showBuffered:Z

.field private showScrubber:Z

.field private showTimes:Z

.field private final timeBounds:Landroid/graphics/Rect;

.field private final timeTextPaint:Landroid/graphics/Paint;

.field private totalTime:I

.field private totalTimeString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x101009e

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->STATE_ENABLED:[I

    new-array v0, v3, [I

    const v1, -0x101009e

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->STATE_DISABLED:[I

    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->STATE_PRESSED:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;

    const/4 v5, -0x1

    const/4 v4, 0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->listener:Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;

    iput-boolean v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showTimes:Z

    iput-boolean v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showScrubber:Z

    iput-boolean v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showBuffered:Z

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedBar:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressPaint:Landroid/graphics/Paint;

    const v3, -0x7f7f80

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x41700000

    mul-float v1, v2, v3

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeBounds:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    const-string v3, "0:00:00"

    const/4 v4, 0x0

    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    const-wide/16 v2, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->stringForTime(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTimeString:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020097

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/StateListDrawable;

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    sget-object v3, Lcom/google/android/youtube/core/player/overlay/TimeBar;->STATE_DISABLED:[I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x41000000

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberPadding:I

    return-void
.end method

.method private getScrubberTime()I
    .locals 4

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberLeft:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTime:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private inProgressBar(FF)Z
    .locals 5
    .param p1    # F
    .param p2    # F

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTop:I

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v4

    add-int v2, v3, v4

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v4

    sub-int v0, v3, v4

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v4

    add-int v1, v3, v4

    int-to-float v3, v0

    cmpg-float v3, v3, p1

    if-gez v3, :cond_0

    int-to-float v3, v1

    cmpg-float v3, p1, v3

    if-gez v3, :cond_0

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTop:I

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberPadding:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpg-float v3, v3, p2

    if-gez v3, :cond_0

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberPadding:I

    add-int/2addr v3, v2

    int-to-float v3, v3

    cmpg-float v3, p2, v3

    if-gez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private positionScrubber(F)V
    .locals 4
    .param p1    # F

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v3

    div-int/lit8 v0, v3, 0x2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int v1, v3, v0

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v2, v3, v0

    float-to-int v3, p1

    sub-int/2addr v3, v0

    iput v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberLeft:I

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberLeft:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberLeft:I

    return-void
.end method

.method private stopScrubbing()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->updateScrubberState()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->invalidate()V

    return-void
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 10
    .param p1    # J

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    long-to-int v4, p1

    div-int/lit16 v3, v4, 0x3e8

    rem-int/lit8 v2, v3, 0x3c

    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    div-int/lit16 v0, v3, 0xe10

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTime:I

    const v5, 0x36ee80

    if-lt v4, v5, :cond_0

    const-string v4, "%d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_0
    const-string v4, "%02d:%02d"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private update()V
    .locals 7

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTime:I

    :goto_0
    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTime:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedPercent:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x64

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-long v3, v3

    iget v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->currentTime:I

    int-to-long v5, v5

    mul-long/2addr v3, v5

    iget v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTime:I

    int-to-long v5, v5

    div-long/2addr v3, v5

    long-to-int v3, v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    int-to-long v4, v0

    mul-long/2addr v2, v4

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTime:I

    int-to-long v4, v4

    div-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberLeft:I

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->invalidate()V

    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->currentTime:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberLeft:I

    goto :goto_1
.end method

.method private updateScrubberState()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTime:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbingEnabled:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbingEnabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->stopScrubbing()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->update()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->STATE_PRESSED:[I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbingEnabled:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->STATE_ENABLED:[I

    goto :goto_2

    :cond_3
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->STATE_DISABLED:[I

    goto :goto_2
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    invoke-static {}, Lcom/google/android/youtube/core/L;->t()V

    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showBuffered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedBar:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showScrubber:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberLeft:I

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTop:I

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberLeft:I

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTop:I

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showTimes:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTime:I

    int-to-long v0, v0

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->stringForTime(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTimeString:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_2
    return-void

    :cond_3
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->currentTime:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getDefaultVisibleHeight()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberPadding:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getScrubberHeight()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberPadding:I

    add-int/2addr v0, v1

    return v0
.end method

.method public isScrubbing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    return v0
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showTimes:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showScrubber:Z

    if-eqz v6, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getDefaultVisibleHeight()I

    move-result v4

    :goto_0
    invoke-static {v5, p1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getDefaultSize(II)I

    move-result v3

    invoke-static {v4, p2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setMeasuredDimension(II)V

    iget-boolean v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showTimes:Z

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showScrubber:Z

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {v6, v5, v5, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->update()V

    return-void

    :cond_1
    move v4, v5

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v5

    div-int/lit8 v1, v5, 0x3

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->timeBounds:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberPadding:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iput v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTop:I

    iget v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTop:I

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    add-int/lit8 v2, v5, -0x1

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getPaddingLeft()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getPaddingRight()I

    move-result v7

    sub-int v7, v3, v7

    sub-int/2addr v7, v1

    add-int/lit8 v8, v2, 0x4

    invoke-virtual {v5, v6, v2, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbingEnabled:Z

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v0, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v1, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2

    :pswitch_0
    int-to-float v3, v0

    int-to-float v4, v1

    invoke-direct {p0, v3, v4}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->inProgressBar(FF)Z

    move-result v3

    if-eqz v3, :cond_0

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    int-to-float v3, v0

    invoke-direct {p0, v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->positionScrubber(F)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getScrubberTime()I

    move-result v3

    iput v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTime:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->listener:Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;

    invoke-interface {v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;->onScrubbingStart()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->updateScrubberState()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->update()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->invalidate()V

    goto :goto_0

    :pswitch_1
    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    if-eqz v3, :cond_0

    int-to-float v3, v0

    invoke-direct {p0, v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->positionScrubber(F)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getScrubberTime()I

    move-result v3

    iput v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTime:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->update()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->invalidate()V

    goto :goto_0

    :pswitch_2
    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->stopScrubbing()V

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTime:I

    iput v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->currentTime:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->listener:Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getScrubberTime()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;->onScrubbingEnd(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public resetTime()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setTime(III)V

    return-void
.end method

.method public setBufferedPercent(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedPercent:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->update()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->updateScrubberState()V

    return-void
.end method

.method public setProgressColor(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->playedPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->update()V

    return-void
.end method

.method public setScrubberTime(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubberTime:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->update()V

    return-void
.end method

.method public setScrubbing(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    return-void
.end method

.method public setShowBuffered(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showBuffered:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->update()V

    return-void
.end method

.method public setShowScrubber(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showScrubber:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->listener:Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getScrubberTime()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar$Listener;->onScrubbingEnd(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->scrubbing:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->requestLayout()V

    return-void
.end method

.method public setShowTimes(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->showTimes:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->requestLayout()V

    return-void
.end method

.method public setTime(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->currentTime:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTime:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedPercent:I

    if-eq v0, p3, :cond_2

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTime:I

    if-eq v0, p2, :cond_1

    iput p2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTime:I

    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->stringForTime(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->totalTimeString:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->updateScrubberState()V

    :cond_1
    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->currentTime:I

    iput p3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->bufferedPercent:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->update()V

    :cond_2
    return-void
.end method
