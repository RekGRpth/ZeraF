.class public Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;
.super Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;
.source "TvWidevineMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;
    }
.end annotation


# static fields
.field public static final SUPPORTED_MIME_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

.field private fillingBuffer:Z

.field private final handler:Landroid/os/Handler;

.field private listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

.field private seekDesiredPos:I

.field private seekWhenPrepared:I

.field private state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "video/wvm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->SUPPORTED_MIME_TYPES:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;-><init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekDesiredPos:I

    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->IDLE:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->createBufferUnderrunTimeoutHandler()Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekDesiredPos:I

    return p1
.end method

.method private createBufferUnderrunTimeoutHandler()Ljava/lang/Runnable;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$2;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$2;-><init>(Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;)V

    return-object v0
.end method


# virtual methods
.method public getCurrentPosition()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekDesiredPos:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekDesiredPos:I

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->fillingBuffer:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInfo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z
    .locals 7
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x0

    const/4 v4, -0x1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->onInfo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z

    return v6

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->getDuration()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->getCurrentPosition()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-long v0, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v3, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v2, v3, :cond_1

    const-wide/32 v2, 0xea60

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->pause()V

    const/16 v2, 0x2bd

    invoke-super {p0, p1, v2, v4}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->onInfo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->fillingBuffer:Z

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

    const-wide/16 v4, 0x4e20

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-boolean v6, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->fillingBuffer:Z

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v3, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v2, v3, :cond_0

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->start()V

    const/16 v2, 0x2be

    invoke-super {p0, p1, v2, v4}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->onInfo(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2f1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSeekComplete(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->fillingBuffer:Z

    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekWhenPrepared:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekWhenPrepared:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekTo(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->PAUSED:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_2

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->start()V

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->pause()V

    :cond_0
    :goto_1
    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->onSeekComplete(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$1;-><init>(Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_0

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->start()V

    goto :goto_1
.end method

.method public pause()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->fillingBuffer:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->pause()V

    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->PAUSED:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    goto :goto_0
.end method

.method public release()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->bufferUnderrunTimeoutHandler:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->release()V

    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->IDLE:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    return-void
.end method

.method public seekTo(I)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    iput p1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekDesiredPos:I

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->fillingBuffer:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    if-eq v0, v1, :cond_1

    :cond_0
    iput p1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekWhenPrepared:I

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekWhenPrepared:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->fillingBuffer:Z

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->setListener(Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;)V

    iput-object p1, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->listener:Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    return-void
.end method

.method public start()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->fillingBuffer:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->start()V

    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->state:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekWhenPrepared:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekWhenPrepared:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer;->seekTo(I)V

    goto :goto_0
.end method
