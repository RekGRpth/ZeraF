.class public interface abstract Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper$Listener;
.super Ljava/lang/Object;
.source "MediaKeyHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/player/overlay/MediaKeyHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onCC()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onPlay()V
.end method

.method public abstract onScrubbingEnd(I)V
.end method

.method public abstract onScrubbingStart()V
.end method

.method public abstract onTogglePlayPause()V
.end method

.method public abstract onUpdateScrubberTime(I)V
.end method
