.class Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;
.super Ljava/lang/Object;
.source "YouTubePlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->onVideoSizeChanged(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;

.field final synthetic val$height:I

.field final synthetic val$mediaPlayer:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;->this$1:Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;

    iput-object p2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;->val$mediaPlayer:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    iput p3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;->val$width:I

    iput p4, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;->val$height:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;->this$1:Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;

    iget-object v0, v0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener;->this$0:Lcom/google/android/youtube/core/player/YouTubePlayer;

    # getter for: Lcom/google/android/youtube/core/player/YouTubePlayer;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;
    invoke-static {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->access$1200(Lcom/google/android/youtube/core/player/YouTubePlayer;)Lcom/google/android/youtube/core/player/PlayerSurface;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;->val$mediaPlayer:Lcom/google/android/youtube/core/player/MediaPlayerInterface;

    iget v2, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;->val$width:I

    iget v3, p0, Lcom/google/android/youtube/core/player/YouTubePlayer$InternalListener$1;->val$height:I

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/player/PlayerSurface;->setVideoSize(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)V

    return-void
.end method
