.class public Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
.super Landroid/widget/RelativeLayout;
.source "RemoteControllerOverlay.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;
.implements Lcom/google/android/youtube/core/player/overlay/OverscanSafeOverlay;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$3;,
        Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;
    }
.end annotation


# instance fields
.field private final authorView:Landroid/widget/TextView;

.field private final errorView:Landroid/widget/TextView;

.field private final fadeInAnimation:Landroid/view/animation/Animation;

.field private final fadeOutAnimation:Landroid/view/animation/Animation;

.field private final handler:Landroid/os/Handler;

.field private final playStatusView:Landroid/widget/ImageView;

.field private final spinner:Landroid/view/View;

.field private sticky:Z

.field private style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

.field private final titleView:Landroid/widget/TextView;

.field private final watchInfoOverlay:Landroid/widget/RelativeLayout;

.field private watchInfoState:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const-wide/16 v3, 0x3e8

    const/16 v2, 0x8

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04002f

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f07006b

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const v1, 0x7f07006d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->titleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const v1, 0x7f07006e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->authorView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const v1, 0x7f07006c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->playStatusView:Landroid/widget/ImageView;

    const v0, 0x7f070070

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->spinner:Landroid/view/View;

    const v0, 0x7f07006f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    const/high16 v0, 0x10a0000

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeInAnimation:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeInAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$1;-><init>(Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    const v0, 0x10a0001

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeOutAnimation:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeOutAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$2;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$2;-><init>(Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->handler:Landroid/os/Handler;

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;->HIDDEN:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoState:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->spinner:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setVisibility(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    .param p1    # Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoState:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->updateHideTime()V

    return-void
.end method

.method private fadeInVideoInfo()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showVideoInfo(Z)V

    return-void
.end method

.method private fadeOutVideoInfo()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private hideSpinner()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->spinner:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private hideVideoInfo()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;->HIDDEN:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoState:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    return-void
.end method

.method private showVideoInfo()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showVideoInfo(Z)V

    return-void
.end method

.method private showVideoInfo(Z)V
    .locals 2
    .param p1    # Z

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$3;->$SwitchMap$com$google$android$youtube$core$player$overlay$RemoteControllerOverlay$WatchInfoState:[I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoState:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;->SHOWN:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoState:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;->SHOWN:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoState:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay$WatchInfoState;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->updateHideTime()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->updateHideTime()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateHideTime()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->sticky:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->handler:Landroid/os/Handler;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public generateLayoutParams()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeOutVideoInfo()V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->spinner:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hideControls()V
    .locals 0

    return-void
.end method

.method public makeSafeForOverscan(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v1, p1, p2, p1, p2}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, p1, v2, v2, p2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->watchInfoOverlay:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public reset()V
    .locals 0

    return-void
.end method

.method public resetTime()V
    .locals 0

    return-void
.end method

.method public setCcEnabled(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setFullscreen(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setHQ(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setHQisHD(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setHasCc(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setHasNext(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setHasPrevious(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Listener;

    return-void
.end method

.method public setLoading()V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public setPlaying()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->hideSpinner()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->playStatusView:Landroid/widget/ImageView;

    const v1, 0x7f02005d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->AD:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    if-eq v0, v1, :cond_0

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->sticky:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->fadeInVideoInfo()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->hideVideoInfo()V

    goto :goto_0
.end method

.method public setPortrait(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setShowFullscreen(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    return-void
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setTimes(III)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public setVideoInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->titleView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->authorView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->MOVIE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->authorView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->authorView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->authorView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showControls()V
    .locals 0

    return-void
.end method

.method public showEnded()V
    .locals 0

    return-void
.end method

.method public showErrorMessage(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->hideVideoInfo()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->hideSpinner()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public showErrorMessage(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showErrorMessage(I)V

    return-void
.end method

.method public showErrorMessage(Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->hideVideoInfo()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->hideSpinner()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showPaused()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->hideSpinner()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->errorView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->playStatusView:Landroid/widget/ImageView;

    const v1, 0x7f02005c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->style:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->AD:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->sticky:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showVideoInfo()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->hideVideoInfo()V

    goto :goto_0
.end method

.method public showTrackSelector(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
