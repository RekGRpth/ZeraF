.class Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;
.super Landroid/view/SurfaceView;
.source "DefaultPlayerSurface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/player/DefaultPlayerSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalSurfaceView"
.end annotation


# instance fields
.field protected horizontalLetterboxFraction:F

.field final synthetic this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

.field protected verticalLetterboxFraction:F

.field private final zoomChangedRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-direct {p0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView$1;-><init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->zoomChangedRunnable:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$200(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v9

    invoke-static {v9, p1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->getDefaultSize(II)I

    move-result v4

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$300(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v9

    invoke-static {v9, p2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->getDefaultSize(II)I

    move-result v3

    move v8, v4

    move v1, v3

    const/high16 v2, 0x3f800000

    const/high16 v5, 0x3f800000

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$200(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v9

    if-lez v9, :cond_1

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$300(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v9

    if-lez v9, :cond_1

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$200(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v10}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$300(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v10

    int-to-float v10, v10

    div-float v6, v9, v10

    int-to-float v9, v8

    int-to-float v10, v1

    div-float v7, v9, v10

    div-float v9, v6, v7

    const/high16 v10, 0x3f800000

    sub-float v0, v9, v10

    const v9, 0x3c23d70a

    cmpl-float v9, v0, v9

    if-lez v9, :cond_4

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$300(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v9

    mul-int/2addr v9, v8

    iget-object v10, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v10}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$200(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v10

    div-int v1, v9, v10

    :cond_0
    :goto_0
    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # setter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayWidth:I
    invoke-static {v9, v8}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$402(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # setter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayHeight:I
    invoke-static {v9, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$502(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoomSupported:Z
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$600(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Z

    move-result v9

    if-eqz v9, :cond_1

    if-ge v8, v4, :cond_5

    int-to-float v9, v8

    int-to-float v10, v4

    div-float v2, v9, v10

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoom:I
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$700(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v9

    sub-int v10, v4, v8

    mul-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x64

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # setter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayWidth:I
    invoke-static {v9, v8}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$402(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    int-to-float v10, v8

    div-float/2addr v10, v6

    float-to-int v10, v10

    # setter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayHeight:I
    invoke-static {v9, v10}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$502(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I

    :cond_1
    :goto_1
    iget v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->horizontalLetterboxFraction:F

    cmpl-float v9, v9, v2

    if-nez v9, :cond_2

    iget v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->verticalLetterboxFraction:F

    cmpl-float v9, v9, v5

    if-eqz v9, :cond_3

    :cond_2
    iput v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->horizontalLetterboxFraction:F

    iput v5, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->verticalLetterboxFraction:F

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$100(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;

    move-result-object v9

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->zoomChangedRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->post(Ljava/lang/Runnable;)Z

    :cond_3
    invoke-static {v8, p1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->resolveSize(II)I

    move-result v8

    invoke-static {v1, p2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v8, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->setMeasuredDimension(II)V

    return-void

    :cond_4
    const v9, -0x43dc28f6

    cmpg-float v9, v0, v9

    if-gez v9, :cond_0

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$200(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v9

    mul-int/2addr v9, v1

    iget-object v10, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v10}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$300(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v10

    div-int v8, v9, v10

    goto :goto_0

    :cond_5
    if-ge v1, v3, :cond_1

    int-to-float v9, v1

    int-to-float v10, v3

    div-float v5, v9, v10

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->zoom:I
    invoke-static {v9}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$700(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I

    move-result v9

    sub-int v10, v3, v1

    mul-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x64

    add-int/2addr v1, v9

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    # setter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayHeight:I
    invoke-static {v9, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$502(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I

    iget-object v9, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    int-to-float v10, v1

    mul-float/2addr v10, v6

    float-to-int v10, v10

    # setter for: Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->videoDisplayWidth:I
    invoke-static {v9, v10}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->access$402(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I

    goto :goto_1
.end method
