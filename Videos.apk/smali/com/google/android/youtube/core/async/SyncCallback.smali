.class public final Lcom/google/android/youtube/core/async/SyncCallback;
.super Ljava/lang/Object;
.source "SyncCallback.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final conditionVar:Landroid/os/ConditionVariable;

.field private volatile exception:Ljava/lang/Exception;

.field private volatile response:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/SyncCallback;->conditionVar:Landroid/os/ConditionVariable;

    return-void
.end method


# virtual methods
.method public getResponse()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/async/SyncCallback;->conditionVar:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/SyncCallback;->exception:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/SyncCallback;->exception:Ljava/lang/Exception;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/SyncCallback;->response:Ljava/lang/Object;

    return-object v0
.end method

.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/SyncCallback;->response:Ljava/lang/Object;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/SyncCallback;->exception:Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/SyncCallback;->conditionVar:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    iput-object p2, p0, Lcom/google/android/youtube/core/async/SyncCallback;->response:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/SyncCallback;->exception:Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/SyncCallback;->conditionVar:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method
