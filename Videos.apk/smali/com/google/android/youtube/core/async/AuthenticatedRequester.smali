.class public final Lcom/google/android/youtube/core/async/AuthenticatedRequester;
.super Ljava/lang/Object;
.source "AuthenticatedRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final retryStrategy:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
            "<TR;>;"
        }
    .end annotation
.end field

.field private final target:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;)V
    .locals 0
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
            "<TR;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->retryStrategy:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/async/AuthenticatedRequester;)Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->retryStrategy:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/core/async/AuthenticatedRequester;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/core/async/AuthenticatedRequester;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public static create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;)Lcom/google/android/youtube/core/async/AuthenticatedRequester;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
            "<TR;>;)",
            "Lcom/google/android/youtube/core/async/AuthenticatedRequester",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;)V

    return-object v0
.end method


# virtual methods
.method public request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    new-instance v1, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;-><init>(Lcom/google/android/youtube/core/async/AuthenticatedRequester;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Object;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
