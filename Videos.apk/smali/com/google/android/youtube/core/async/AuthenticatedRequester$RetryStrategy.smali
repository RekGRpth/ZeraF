.class public interface abstract Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
.super Ljava/lang/Object;
.source "AuthenticatedRequester.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/async/AuthenticatedRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RetryStrategy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract canRetry(Ljava/lang/Object;Ljava/lang/Exception;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")Z"
        }
    .end annotation
.end method

.method public abstract extractUserAuth(Ljava/lang/Object;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;"
        }
    .end annotation
.end method

.method public abstract recreateRequest(Ljava/lang/Object;Lcom/google/android/youtube/videos/accounts/UserAuth;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            ")TR;"
        }
    .end annotation
.end method
