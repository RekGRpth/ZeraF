.class Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;
.super Ljava/lang/Object;
.source "AuthenticatedRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/async/AuthenticatedRequester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private retried:Z

.field final synthetic this$0:Lcom/google/android/youtube/core/async/AuthenticatedRequester;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$originalRequest:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/async/AuthenticatedRequester;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->this$0:Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->val$originalRequest:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->retried:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->this$0:Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    # getter for: Lcom/google/android/youtube/core/async/AuthenticatedRequester;->retryStrategy:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    invoke-static {v0}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->access$000(Lcom/google/android/youtube/core/async/AuthenticatedRequester;)Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;->canRetry(Ljava/lang/Object;Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->retried:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->this$0:Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    # getter for: Lcom/google/android/youtube/core/async/AuthenticatedRequester;->retryStrategy:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    invoke-static {v0}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->access$000(Lcom/google/android/youtube/core/async/AuthenticatedRequester;)Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;->extractUserAuth(Ljava/lang/Object;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v1, Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->this$0:Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    # getter for: Lcom/google/android/youtube/core/async/AuthenticatedRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    invoke-static {v2}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->access$100(Lcom/google/android/youtube/core/async/AuthenticatedRequester;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->invalidateToken(Lcom/google/android/youtube/videos/accounts/UserAuth;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->this$0:Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    # getter for: Lcom/google/android/youtube/core/async/AuthenticatedRequester;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    invoke-static {v2}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->access$100(Lcom/google/android/youtube/core/async/AuthenticatedRequester;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->blockingGetUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->this$0:Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    # getter for: Lcom/google/android/youtube/core/async/AuthenticatedRequester;->retryStrategy:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    invoke-static {v1}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->access$000(Lcom/google/android/youtube/core/async/AuthenticatedRequester;)Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;->recreateRequest(Ljava/lang/Object;Lcom/google/android/youtube/videos/accounts/UserAuth;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->this$0:Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    # getter for: Lcom/google/android/youtube/core/async/AuthenticatedRequester;->target:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v1}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->access$200(Lcom/google/android/youtube/core/async/AuthenticatedRequester;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    invoke-interface {v1, v0, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->val$originalRequest:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/AuthenticatedRequester$1;->val$originalRequest:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
