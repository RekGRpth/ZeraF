.class Lcom/google/android/youtube/core/async/AsyncRequester$1;
.super Ljava/lang/Object;
.source "AsyncRequester.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/async/AsyncRequester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/async/AsyncRequester;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$request:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/async/AsyncRequester;Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/AsyncRequester$1;->this$0:Lcom/google/android/youtube/core/async/AsyncRequester;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/AsyncRequester$1;->val$request:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/AsyncRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/core/async/AsyncRequester$1;->this$0:Lcom/google/android/youtube/core/async/AsyncRequester;

    # getter for: Lcom/google/android/youtube/core/async/AsyncRequester;->target:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v1}, Lcom/google/android/youtube/core/async/AsyncRequester;->access$000(Lcom/google/android/youtube/core/async/AsyncRequester;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/async/AsyncRequester$1;->val$request:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/AsyncRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "target requester should catch exception and pass to callback.onError"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/async/AsyncRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/AsyncRequester$1;->val$request:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
