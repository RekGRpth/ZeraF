.class public Lcom/google/android/youtube/core/async/GDataResponseException;
.super Lorg/apache/http/client/HttpResponseException;
.source "GDataResponseException.java"


# static fields
.field private static final YOUTUBE_SIGNUP_REQUIRED_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/GDataError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/GDataError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/google/android/youtube/core/async/GDataResponseException;->YOUTUBE_SIGNUP_REQUIRED_LIST:Ljava/util/List;

    sget-object v0, Lcom/google/android/youtube/core/async/GDataResponseException;->YOUTUBE_SIGNUP_REQUIRED_LIST:Ljava/util/List;

    new-instance v1, Lcom/google/android/youtube/core/model/GDataError;

    const-string v2, "yt:service"

    const-string v3, "youtube_signup_required"

    invoke-direct {v1, v2, v3, v4, v4}, Lcom/google/android/youtube/core/model/GDataError;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/GDataError;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", GData error(s):\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "none"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    if-nez p3, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/youtube/core/async/GDataResponseException;->errors:Ljava/util/List;

    return-void

    :cond_1
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method public static createYouTubeSignupRequired(ILjava/lang/String;)Lcom/google/android/youtube/core/async/GDataResponseException;
    .locals 2
    .param p0    # I
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/core/async/GDataResponseException;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataResponseException;->YOUTUBE_SIGNUP_REQUIRED_LIST:Ljava/util/List;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/youtube/core/async/GDataResponseException;-><init>(ILjava/lang/String;Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public containsDisabledInMaintenanceModeError()Z
    .locals 3

    const/4 v2, 0x0

    const-string v0, "yt:service"

    const-string v1, "disabled_in_maintenance_mode"

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public containsError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/GDataResponseException;->errors:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/GDataError;

    if-eqz p1, :cond_1

    iget-object v2, v0, Lcom/google/android/youtube/core/model/GDataError;->domain:Ljava/lang/String;

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v2, v0, Lcom/google/android/youtube/core/model/GDataError;->code:Ljava/lang/String;

    invoke-static {p2, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    if-eqz p3, :cond_3

    iget-object v2, v0, Lcom/google/android/youtube/core/model/GDataError;->location:Ljava/lang/String;

    invoke-static {p3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    if-eqz p4, :cond_4

    iget-object v2, v0, Lcom/google/android/youtube/core/model/GDataError;->internalReason:Ljava/lang/String;

    invoke-static {p4, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_5
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public containsInvalidUriError()Z
    .locals 3

    const/4 v2, 0x0

    const-string v0, "GData"

    const-string v1, "InvalidRequestUriException"

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public containsVideoIsPrivateError()Z
    .locals 4

    const-string v0, "GData"

    const-string v1, "ServiceForbiddenException"

    const/4 v2, 0x0

    const-string v3, "Private video"

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public containsYouTubeSignupRequiredError()Z
    .locals 4

    const/4 v3, 0x0

    const-string v0, "yt:service"

    const-string v1, "youtube_signup_required"

    invoke-virtual {p0, v0, v1, v3, v3}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GData"

    const-string v1, "InvalidRequestUriException"

    const-string v2, "Missing or invalid username."

    invoke-virtual {p0, v0, v1, v3, v2}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
