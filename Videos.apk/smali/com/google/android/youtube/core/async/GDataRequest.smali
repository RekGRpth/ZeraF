.class public final Lcom/google/android/youtube/core/async/GDataRequest;
.super Lcom/google/android/youtube/core/async/NetworkRequest;
.source "GDataRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/async/GDataRequest$Version;
    }
.end annotation


# static fields
.field public static final BASE_URI:Landroid/net/Uri;

.field public static final DEV_BASE_URI:Landroid/net/Uri;

.field public static final HTTPS_BASE_URI:Landroid/net/Uri;

.field public static final RETRY_STRATEGY:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final STAGE_BASE_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "gdata.youtube.com"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feeds"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "api"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/async/GDataRequest;->BASE_URI:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequest;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/async/GDataRequest;->HTTPS_BASE_URI:Landroid/net/Uri;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "stage.gdata.youtube.com"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feeds"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "api"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/async/GDataRequest;->STAGE_BASE_URI:Landroid/net/Uri;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "dev.gdata.youtube.com"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feeds"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "api"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/async/GDataRequest;->DEV_BASE_URI:Landroid/net/Uri;

    new-instance v0, Lcom/google/android/youtube/core/async/GDataRequest$1;

    invoke-direct {v0}, Lcom/google/android/youtube/core/async/GDataRequest$1;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/async/GDataRequest;->RETRY_STRATEGY:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p4    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[B)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/core/async/NetworkRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[BLcom/google/android/youtube/core/async/GDataRequest$1;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p3    # Ljava/util/Map;
    .param p4    # [B
    .param p5    # Lcom/google/android/youtube/core/async/GDataRequest$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    return-void
.end method

.method static synthetic access$000(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1
    .param p0    # Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/youtube/core/async/GDataRequest;->upgradeToHttps(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 2
    .param p0    # Landroid/net/Uri;

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {v0, p0, v1, v1, v1}, Lcom/google/android/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    return-object v0
.end method

.method public static create(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Landroid/net/Uri;
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    const/4 v2, 0x0

    const-string v0, "userAuth can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-static {p0}, Lcom/google/android/youtube/core/async/GDataRequest;->upgradeToHttps(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1, p1, v2, v2}, Lcom/google/android/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    return-object v0
.end method

.method private static upgradeToHttps(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p0    # Landroid/net/Uri;

    const-string v0, "http"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "dev.gdata.youtube.com"

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "stage.gdata.youtube.com"

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    :cond_0
    return-object p0
.end method
