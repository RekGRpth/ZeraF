.class public abstract Lcom/google/android/youtube/core/async/ThreadingCallback;
.super Ljava/lang/Object;
.source "ThreadingCallback.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/async/ThreadingCallback$1;,
        Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# static fields
.field private static final queue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final target:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/async/ThreadingCallback;->queue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Callback;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/ThreadingCallback;->target:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;

    invoke-static {p0}, Lcom/google/android/youtube/core/async/ThreadingCallback;->releaseRunnable(Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;)V

    return-void
.end method

.method private static obtainRunnable()Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable",
            "<TR;TE;>;"
        }
    .end annotation

    sget-object v1, Lcom/google/android/youtube/core/async/ThreadingCallback;->queue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;-><init>(Lcom/google/android/youtube/core/async/ThreadingCallback$1;)V

    goto :goto_0
.end method

.method private static releaseRunnable(Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable",
            "<**>;)V"
        }
    .end annotation

    :try_start_0
    sget-object v1, Lcom/google/android/youtube/core/async/ThreadingCallback;->queue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Interrupted when releasing runnable to the queue"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/youtube/core/async/ThreadingCallback;->obtainRunnable()Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ThreadingCallback;->target:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;->prepareForOnError(Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Object;Ljava/lang/Exception;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/async/ThreadingCallback;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/youtube/core/async/ThreadingCallback;->obtainRunnable()Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ThreadingCallback;->target:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/youtube/core/async/ThreadingCallback$CallbackRunnable;->prepareForOnResponse(Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/async/ThreadingCallback;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected abstract post(Ljava/lang/Runnable;)V
.end method
