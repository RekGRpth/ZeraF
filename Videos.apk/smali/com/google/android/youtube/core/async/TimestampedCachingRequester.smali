.class public abstract Lcom/google/android/youtube/core/async/TimestampedCachingRequester;
.super Ljava/lang/Object;
.source "TimestampedCachingRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;,
        Lcom/google/android/youtube/core/async/TimestampedCachingRequester$CachingCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final cache:Lcom/google/android/youtube/core/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/cache/Cache",
            "<TK;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final target:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final timeToLive:J


# direct methods
.method protected constructor <init>(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)V
    .locals 0
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/cache/Cache",
            "<TK;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<TE;>;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->cache:Lcom/google/android/youtube/core/cache/Cache;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    iput-wide p3, p0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->timeToLive:J

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/async/TimestampedCachingRequester;)Lcom/google/android/youtube/core/cache/Cache;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/async/TimestampedCachingRequester;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->cache:Lcom/google/android/youtube/core/cache/Cache;

    return-object v0
.end method

.method public static create(Lcom/google/android/youtube/core/cache/Cache;J)Lcom/google/android/youtube/core/async/TimestampedCachingRequester;
    .locals 2
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/cache/Cache",
            "<TR;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<TE;>;>;J)",
            "Lcom/google/android/youtube/core/async/TimestampedCachingRequester",
            "<TR;TR;TE;>;"
        }
    .end annotation

    const-string v0, "cache may not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x9a7ec800L

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "time to live must be >=0 and <= 2592000000"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/android/youtube/core/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;-><init>(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static create(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/TimestampedCachingRequester;
    .locals 2
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/cache/Cache",
            "<TR;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<TE;>;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;J)",
            "Lcom/google/android/youtube/core/async/TimestampedCachingRequester",
            "<TR;TR;TE;>;"
        }
    .end annotation

    const-string v0, "cache may not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "target may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x9a7ec800L

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "time to live must be >=0 and <= 2592000000"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/core/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;-><init>(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    iget-wide v3, p0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->timeToLive:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->cache:Lcom/google/android/youtube/core/cache/Cache;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->toKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/youtube/core/cache/Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Timestamped;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    if-eqz v0, :cond_0

    iget-wide v3, v0, Lcom/google/android/youtube/core/async/Timestamped;->timestamp:J

    cmp-long v3, v1, v3

    if-ltz v3, :cond_0

    iget-wide v3, v0, Lcom/google/android/youtube/core/async/Timestamped;->timestamp:J

    iget-wide v5, p0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->timeToLive:J

    add-long/2addr v3, v5

    cmp-long v3, v3, v1

    if-ltz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/youtube/core/async/Timestamped;->element:Ljava/lang/Object;

    invoke-interface {p2, p1, v3}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    new-instance v4, Lcom/google/android/youtube/core/async/TimestampedCachingRequester$CachingCallback;

    invoke-direct {v4, p0, p2}, Lcom/google/android/youtube/core/async/TimestampedCachingRequester$CachingCallback;-><init>(Lcom/google/android/youtube/core/async/TimestampedCachingRequester;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v3, p1, v4}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0

    :cond_1
    new-instance v3, Lcom/google/android/youtube/core/async/NotFoundException;

    invoke-direct {v3}, Lcom/google/android/youtube/core/async/NotFoundException;-><init>()V

    invoke-interface {p2, p1, v3}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected abstract toKey(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)TK;"
        }
    .end annotation
.end method
