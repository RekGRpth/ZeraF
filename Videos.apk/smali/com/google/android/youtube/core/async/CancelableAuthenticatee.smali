.class public Lcom/google/android/youtube/core/async/CancelableAuthenticatee;
.super Ljava/lang/Object;
.source "CancelableAuthenticatee.java"

# interfaces
.implements Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;


# instance fields
.field private volatile canceled:Z

.field private final target:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->target:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->canceled:Z

    return-void
.end method

.method public onAuthenticated(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->canceled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->target:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticated(Lcom/google/android/youtube/videos/accounts/UserAuth;)V

    :cond_0
    return-void
.end method

.method public onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->canceled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->target:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method public onNotAuthenticated()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->canceled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/CancelableAuthenticatee;->target:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated()V

    :cond_0
    return-void
.end method
