.class final Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;
.super Ljava/lang/Object;
.source "ConvertingRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/async/ConvertingRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ConvertingCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TS;TF;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final convertedRequest:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field private final originalCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final originalRequest:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private response:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TF;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/youtube/core/async/ConvertingRequester;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/ConvertingRequester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TS;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/youtube/core/async/ConvertingRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->convertedRequest:Ljava/lang/Object;

    iput-object p4, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;TF;)V"
        }
    .end annotation

    iput-object p2, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->response:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/youtube/core/async/ConvertingRequester;

    # getter for: Lcom/google/android/youtube/core/async/ConvertingRequester;->responseConverter:Lcom/google/android/youtube/core/converter/ResponseConverter;
    invoke-static {v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->access$000(Lcom/google/android/youtube/core/async/ConvertingRequester;)Lcom/google/android/youtube/core/converter/ResponseConverter;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/youtube/core/async/ConvertingRequester;

    # getter for: Lcom/google/android/youtube/core/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;
    invoke-static {v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->access$100(Lcom/google/android/youtube/core/async/ConvertingRequester;)Ljava/util/concurrent/Executor;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/youtube/core/async/ConvertingRequester;

    # getter for: Lcom/google/android/youtube/core/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;
    invoke-static {v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->access$100(Lcom/google/android/youtube/core/async/ConvertingRequester;)Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->run()V

    goto :goto_0

    :cond_1
    move-object v0, p2

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public run()V
    .locals 6

    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/youtube/core/async/ConvertingRequester;

    # getter for: Lcom/google/android/youtube/core/async/ConvertingRequester;->responseConverter:Lcom/google/android/youtube/core/converter/ResponseConverter;
    invoke-static {v2}, Lcom/google/android/youtube/core/async/ConvertingRequester;->access$000(Lcom/google/android/youtube/core/async/ConvertingRequester;)Lcom/google/android/youtube/core/converter/ResponseConverter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->response:Ljava/lang/Object;

    invoke-interface {v2, v3}, Lcom/google/android/youtube/core/converter/ResponseConverter;->convertResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    invoke-interface {v2, v3, v0}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/youtube/core/async/ConvertingRequester;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->convertedRequest:Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/youtube/core/async/ConvertingRequester;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->convertedRequest:Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/core/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Exception;)V

    goto :goto_0
.end method
