.class Lcom/google/android/youtube/core/cache/InMemoryLruCache$1;
.super Ljava/util/LinkedHashMap;
.source "InMemoryLruCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/cache/InMemoryLruCache;-><init>(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<TK;TE;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/cache/InMemoryLruCache;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/cache/InMemoryLruCache;IFZ)V
    .locals 0
    .param p2    # I
    .param p3    # F
    .param p4    # Z

    iput-object p1, p0, Lcom/google/android/youtube/core/cache/InMemoryLruCache$1;->this$0:Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TE;>;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/InMemoryLruCache$1;->this$0:Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    iget-object v0, v0, Lcom/google/android/youtube/core/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/cache/InMemoryLruCache$1;->this$0:Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    # getter for: Lcom/google/android/youtube/core/cache/InMemoryLruCache;->maxCacheSize:I
    invoke-static {v1}, Lcom/google/android/youtube/core/cache/InMemoryLruCache;->access$000(Lcom/google/android/youtube/core/cache/InMemoryLruCache;)I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/InMemoryLruCache$1;->this$0:Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/cache/InMemoryLruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
