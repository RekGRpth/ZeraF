.class public Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;
.super Lcom/google/android/youtube/core/cache/PersistentCache;
.source "SerializablesPersistentCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E::",
        "Ljava/io/Serializable;",
        ">",
        "Lcom/google/android/youtube/core/cache/PersistentCache",
        "<TK;TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/cache/PersistentCache;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/cache/PersistentCache;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected readElement(Ljava/io/BufferedInputStream;)Ljava/io/Serializable;
    .locals 7
    .param p1    # Ljava/io/BufferedInputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/BufferedInputStream;",
            ")TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, p1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/Serializable;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;->closeIfOpen(Ljava/io/InputStream;)V

    return-object v1

    :catch_0
    move-exception v0

    :goto_0
    :try_start_2
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ClassNotFound: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v4

    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;->closeIfOpen(Ljava/io/InputStream;)V

    throw v4

    :catchall_1
    move-exception v4

    move-object v2, v3

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v2, v3

    goto :goto_0
.end method

.method protected bridge synthetic readElement(Ljava/io/BufferedInputStream;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/io/BufferedInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;->readElement(Ljava/io/BufferedInputStream;)Ljava/io/Serializable;

    move-result-object v0

    return-object v0
.end method

.method protected writeElement(Ljava/io/Serializable;Ljava/io/BufferedOutputStream;)V
    .locals 3
    .param p2    # Ljava/io/BufferedOutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/io/BufferedOutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, p2}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    return-void

    :catchall_0
    move-exception v2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    throw v2

    :catchall_1
    move-exception v2

    move-object v0, v1

    goto :goto_0
.end method

.method protected bridge synthetic writeElement(Ljava/lang/Object;Ljava/io/BufferedOutputStream;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/io/BufferedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/io/Serializable;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;->writeElement(Ljava/io/Serializable;Ljava/io/BufferedOutputStream;)V

    return-void
.end method
