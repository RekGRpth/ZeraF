.class public abstract Lcom/google/android/youtube/core/cache/PersistentCache;
.super Ljava/lang/Object;
.source "PersistentCache.java"

# interfaces
.implements Lcom/google/android/youtube/core/cache/Cache;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/cache/Cache",
        "<TK;TE;>;"
    }
.end annotation


# instance fields
.field private final cachePath:Ljava/lang/String;

.field private final filenames:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final filter:Ljava/io/FilenameFilter;

.field private volatile initCalled:Z

.field private final initialized:Landroid/os/ConditionVariable;

.field private final suffix:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/cache/PersistentCache;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "cachePath may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(Z)V

    iput-object p1, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->cachePath:Ljava/lang/String;

    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->initialized:Landroid/os/ConditionVariable;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v0, "suffix cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->suffix:Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/core/cache/PersistentCache$4;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/core/cache/PersistentCache$4;-><init>(Lcom/google/android/youtube/core/cache/PersistentCache;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->filter:Ljava/io/FilenameFilter;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/cache/PersistentCache;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/cache/PersistentCache;

    invoke-direct {p0}, Lcom/google/android/youtube/core/cache/PersistentCache;->doInit()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/core/cache/PersistentCache;)Landroid/os/ConditionVariable;
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/cache/PersistentCache;

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->initialized:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method private assertInitWasStartedAndWaitForIt()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->initCalled:Z

    const-string v1, "init() must be called before calling to this method"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->initialized:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    return-void
.end method

.method public static cleanup(Ljava/lang/String;J)V
    .locals 19
    .param p0    # Ljava/lang/String;
    .param p1    # J

    const-wide/16 v15, 0x0

    cmp-long v15, p1, v15

    if-lez v15, :cond_1

    const/4 v15, 0x1

    :goto_0
    const-string v16, "limit may not be <= 0"

    invoke-static/range {v15 .. v16}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    const-string v15, "dirCache may not be empty"

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " is not a directory"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v10, Lcom/google/android/youtube/core/cache/PersistentCache$1;

    invoke-direct {v10}, Lcom/google/android/youtube/core/cache/PersistentCache$1;-><init>()V

    invoke-virtual {v4, v10}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v9

    if-nez v9, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v15, 0x0

    goto :goto_0

    :cond_2
    const/4 v14, 0x0

    move-object v2, v9

    array-length v12, v2

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v12, :cond_3

    aget-object v5, v2, v11

    int-to-long v15, v14

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v17

    add-long v15, v15, v17

    long-to-int v14, v15

    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_3
    int-to-long v15, v14

    cmp-long v15, v15, p1

    if-gez v15, :cond_4

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Cache is below limit, no need to shrink: [size="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", limit="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-wide/from16 v0, p1

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    new-instance v8, Ljava/util/HashMap;

    array-length v15, v9

    invoke-direct {v8, v15}, Ljava/util/HashMap;-><init>(I)V

    move-object v2, v9

    array-length v12, v2

    const/4 v11, 0x0

    :goto_3
    if-ge v11, v12, :cond_5

    aget-object v5, v2, v11

    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v8, v5, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :cond_5
    new-instance v15, Lcom/google/android/youtube/core/cache/PersistentCache$2;

    invoke-direct {v15, v8}, Lcom/google/android/youtube/core/cache/PersistentCache$2;-><init>(Ljava/util/HashMap;)V

    invoke-static {v9, v15}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    move v13, v14

    const/4 v3, 0x0

    move-object v2, v9

    array-length v12, v2

    const/4 v11, 0x0

    :goto_4
    if-ge v11, v12, :cond_0

    aget-object v5, v2, v11

    int-to-long v15, v13

    cmp-long v15, v15, p1

    if-ltz v15, :cond_7

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v15

    if-eqz v15, :cond_6

    int-to-long v15, v13

    sub-long/2addr v15, v6

    long-to-int v13, v15

    add-int/lit8 v3, v3, 0x1

    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    :cond_7
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Cache shrunk: [deleted="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", newSize="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", previousSize="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", limit="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-wide/from16 v0, p1

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private doInit()V
    .locals 8

    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->cachePath:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->filter:Ljava/io/FilenameFilter;

    invoke-virtual {v5, v6}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    iget-object v5, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static triggerCleanup(Ljava/util/concurrent/Executor;Ljava/lang/String;J)V
    .locals 1
    .param p0    # Ljava/util/concurrent/Executor;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    new-instance v0, Lcom/google/android/youtube/core/cache/PersistentCache$3;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/youtube/core/cache/PersistentCache$3;-><init>(Ljava/lang/String;J)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method protected final closeIfOpen(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final closeIfOpen(Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method final generateFilename(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TE;"
        }
    .end annotation

    const-string v6, "key may not be null"

    invoke-static {p1, v6}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/youtube/core/cache/PersistentCache;->assertInitWasStartedAndWaitForIt()V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/cache/PersistentCache;->generateFilename(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->cachePath:Ljava/lang/String;

    invoke-direct {v4, v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v3, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v7, 0x2000

    invoke-direct {v1, v6, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/cache/PersistentCache;->readElement(Ljava/io/BufferedInputStream;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/io/File;->setLastModified(J)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/cache/PersistentCache;->closeIfOpen(Ljava/io/InputStream;)V

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v2

    :goto_1
    :try_start_2
    iget-object v6, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error opening cache file (maybe removed). [filename="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/cache/PersistentCache;->closeIfOpen(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/cache/PersistentCache;->closeIfOpen(Ljava/io/InputStream;)V

    throw v6

    :catchall_1
    move-exception v6

    move-object v0, v1

    goto :goto_2

    :catch_1
    move-exception v2

    move-object v0, v1

    goto :goto_1
.end method

.method public init(Ljava/util/concurrent/Executor;)Lcom/google/android/youtube/core/cache/PersistentCache;
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/android/youtube/core/cache/PersistentCache",
            "<TK;TE;>;"
        }
    .end annotation

    const-string v0, "executor may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->initCalled:Z

    new-instance v0, Lcom/google/android/youtube/core/cache/PersistentCache$5;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/cache/PersistentCache$5;-><init>(Lcom/google/android/youtube/core/cache/PersistentCache;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-object p0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TE;)V"
        }
    .end annotation

    const-string v5, "key may not be null"

    invoke-static {p1, v5}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "element may not be null"

    invoke-static {p1, v5}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/youtube/core/cache/PersistentCache;->assertInitWasStartedAndWaitForIt()V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/cache/PersistentCache;->generateFilename(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v3, Ljava/io/File;

    iget-object v5, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->cachePath:Ljava/lang/String;

    invoke-direct {v3, v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v6, 0x2000

    invoke-direct {v1, v5, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0, p2, v1}, Lcom/google/android/youtube/core/cache/PersistentCache;->writeElement(Ljava/lang/Object;Ljava/io/BufferedOutputStream;)V

    iget-object v5, p0, Lcom/google/android/youtube/core/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v6, ""

    invoke-virtual {v5, v4, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/cache/PersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    move-object v0, v1

    :goto_0
    return-void

    :catch_0
    move-exception v2

    :goto_1
    :try_start_2
    const-string v5, "Error creating cache file."

    invoke-static {v5, v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/cache/PersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    :catch_1
    move-exception v2

    :goto_2
    :try_start_3
    const-string v5, "Error creating cache file."

    invoke-static {v5, v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/cache/PersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v5

    :goto_3
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/cache/PersistentCache;->closeIfOpen(Ljava/io/OutputStream;)V

    throw v5

    :catchall_1
    move-exception v5

    move-object v0, v1

    goto :goto_3

    :catch_2
    move-exception v2

    move-object v0, v1

    goto :goto_2

    :catch_3
    move-exception v2

    move-object v0, v1

    goto :goto_1
.end method

.method protected abstract readElement(Ljava/io/BufferedInputStream;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/BufferedInputStream;",
            ")TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract writeElement(Ljava/lang/Object;Ljava/io/BufferedOutputStream;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/io/BufferedOutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
