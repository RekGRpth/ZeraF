.class final Lcom/google/android/youtube/core/cache/PersistentCache$3;
.super Ljava/lang/Object;
.source "PersistentCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/cache/PersistentCache;->triggerCleanup(Ljava/util/concurrent/Executor;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cachePath:Ljava/lang/String;

.field final synthetic val$limit:J


# direct methods
.method constructor <init>(Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/cache/PersistentCache$3;->val$cachePath:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/youtube/core/cache/PersistentCache$3;->val$limit:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/cache/PersistentCache$3;->val$cachePath:Ljava/lang/String;

    iget-wide v1, p0, Lcom/google/android/youtube/core/cache/PersistentCache$3;->val$limit:J

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/cache/PersistentCache;->cleanup(Ljava/lang/String;J)V

    return-void
.end method
