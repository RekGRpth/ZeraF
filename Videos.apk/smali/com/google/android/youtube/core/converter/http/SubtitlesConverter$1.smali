.class Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$1;
.super Lcom/google/android/youtube/core/converter/XmlParser$BaseRule;
.source "SubtitlesConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->addFormat1Rules(Lcom/google/android/youtube/core/converter/Rules$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$1;->this$0:Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/XmlParser$BaseRule;-><init>()V

    return-void
.end method


# virtual methods
.method public end(Lcom/google/android/youtube/core/utils/Stack;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/utils/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/xml/sax/Attributes;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v5, 0x0

    const-class v0, Lcom/google/android/youtube/core/model/Subtitles$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/Stack;->peek(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Subtitles$Builder;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "start"

    aput-object v2, v1, v5

    # invokes: Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v1}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    # invokes: Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->secondsToMillis(F)I
    invoke-static {v1}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->access$100(F)I

    move-result v1

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "dur"

    aput-object v3, v2, v5

    # invokes: Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v2}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x40a00000

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Util;->parseFloat(Ljava/lang/String;F)F

    move-result v2

    # invokes: Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->secondsToMillis(F)I
    invoke-static {v2}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->access$100(F)I

    move-result v2

    const-string v3, "\n"

    const-string v4, "<br/>"

    invoke-virtual {p3, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    add-int/2addr v2, v1

    invoke-virtual {v0, v5, v3, v1, v2}, Lcom/google/android/youtube/core/model/Subtitles$Builder;->addLineToWindow(ILjava/lang/String;II)Lcom/google/android/youtube/core/model/Subtitles$Builder;

    return-void
.end method
