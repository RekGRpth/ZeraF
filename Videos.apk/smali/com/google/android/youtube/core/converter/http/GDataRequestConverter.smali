.class public Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;
.super Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;
.source "GDataRequestConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter",
        "<",
        "Lcom/google/android/youtube/core/async/GDataRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public final gdataVersion:Lcom/google/android/youtube/core/async/GDataRequest$Version;

.field public final uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/utils/UriRewriter;Lcom/google/android/youtube/core/async/GDataRequest$Version;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/converter/http/HttpMethod;
    .param p2    # Lcom/google/android/youtube/core/async/DeviceAuthorizer;
    .param p3    # Lcom/google/android/youtube/core/utils/UriRewriter;
    .param p4    # Lcom/google/android/youtube/core/async/GDataRequest$Version;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Lcom/google/android/youtube/core/async/DeviceAuthorizer;)V

    const-string v0, "uriRewriter can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/UriRewriter;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v0, "gdataVersion can\'t be empty"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest$Version;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;->gdataVersion:Lcom/google/android/youtube/core/async/GDataRequest$Version;

    return-void
.end method


# virtual methods
.method public convertRequest(Lcom/google/android/youtube/core/async/NetworkRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;->convertRequest(Lcom/google/android/youtube/core/async/NetworkRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    const-string v1, "GData-Version"

    iget-object v2, p0, Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;->gdataVersion:Lcom/google/android/youtube/core/async/GDataRequest$Version;

    iget-object v2, v2, Lcom/google/android/youtube/core/async/GDataRequest$Version;->headerValue:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected createHttpRequest(Lcom/google/android/youtube/core/async/NetworkRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/async/NetworkRequest;

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    if-nez v1, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;->method:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    iget-object v2, p1, Lcom/google/android/youtube/core/async/NetworkRequest;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
