.class public Lcom/google/android/youtube/core/converter/http/DevicePrivilegesListConverter;
.super Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;
.source "DevicePrivilegesListConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/GDataResponseConverter",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/youtube/core/model/DevicePrivileges;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final rules:Lcom/google/android/youtube/core/converter/Rules;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/XmlParser;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    new-instance v0, Lcom/google/android/youtube/core/converter/Rules$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;-><init>()V

    invoke-static {v0}, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesRulesHelper;->addDevicePriviledgesListRules(Lcom/google/android/youtube/core/converter/Rules$Builder;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;->build()Lcom/google/android/youtube/core/converter/Rules;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesListConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-void
.end method


# virtual methods
.method protected getRules()Lcom/google/android/youtube/core/converter/Rules;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesListConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-object v0
.end method
