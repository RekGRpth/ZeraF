.class final Lcom/google/android/youtube/core/converter/http/HttpResponseConverter$1;
.super Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;
.source "HttpResponseConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter$1;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Void;
    .locals 1
    .param p1    # Lorg/apache/http/HttpEntity;

    const/4 v0, 0x0

    return-object v0
.end method
