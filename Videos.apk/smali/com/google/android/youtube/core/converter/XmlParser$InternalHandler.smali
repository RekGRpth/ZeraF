.class final Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "XmlParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/converter/XmlParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "InternalHandler"
.end annotation


# instance fields
.field private final attrsStack:Lcom/google/android/youtube/core/utils/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/utils/Stack",
            "<",
            "Lorg/xml/sax/Attributes;",
            ">;"
        }
    .end annotation
.end field

.field private final charsStack:Lcom/google/android/youtube/core/utils/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/utils/Stack",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final path:Lcom/google/android/youtube/core/utils/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/utils/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public result:Ljava/lang/Object;

.field private final rules:Lcom/google/android/youtube/core/converter/Rules;

.field private final stack:Lcom/google/android/youtube/core/utils/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/utils/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/Rules;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/converter/Rules;

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->rules:Lcom/google/android/youtube/core/converter/Rules;

    new-instance v0, Lcom/google/android/youtube/core/utils/Stack;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->path:Lcom/google/android/youtube/core/utils/Stack;

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->path:Lcom/google/android/youtube/core/utils/Stack;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/Stack;->offer(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/youtube/core/utils/Stack;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->attrsStack:Lcom/google/android/youtube/core/utils/Stack;

    new-instance v0, Lcom/google/android/youtube/core/utils/Stack;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->charsStack:Lcom/google/android/youtube/core/utils/Stack;

    new-instance v0, Lcom/google/android/youtube/core/utils/Stack;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->stack:Lcom/google/android/youtube/core/utils/Stack;

    return-void
.end method


# virtual methods
.method public final characters([CII)V
    .locals 2
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->path:Lcom/google/android/youtube/core/utils/Stack;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/utils/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->rules:Lcom/google/android/youtube/core/converter/Rules;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/converter/Rules;->get(Ljava/lang/String;)Lcom/google/android/youtube/core/converter/XmlParser$Rule;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->charsStack:Lcom/google/android/youtube/core/utils/Stack;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/utils/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method public final endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->path:Lcom/google/android/youtube/core/utils/Stack;

    invoke-virtual {v5}, Lcom/google/android/youtube/core/utils/Stack;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->rules:Lcom/google/android/youtube/core/converter/Rules;

    invoke-virtual {v5, v2}, Lcom/google/android/youtube/core/converter/Rules;->get(Ljava/lang/String;)Lcom/google/android/youtube/core/converter/XmlParser$Rule;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->attrsStack:Lcom/google/android/youtube/core/utils/Stack;

    invoke-virtual {v5}, Lcom/google/android/youtube/core/utils/Stack;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xml/sax/Attributes;

    iget-object v5, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->charsStack:Lcom/google/android/youtube/core/utils/Stack;

    invoke-virtual {v5}, Lcom/google/android/youtube/core/utils/Stack;->poll()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->stack:Lcom/google/android/youtube/core/utils/Stack;

    invoke-virtual {v5}, Lcom/google/android/youtube/core/utils/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    :goto_0
    iput-object v3, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->result:Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->stack:Lcom/google/android/youtube/core/utils/Stack;

    invoke-interface {v4, v5, v0, v1}, Lcom/google/android/youtube/core/converter/XmlParser$Rule;->end(Lcom/google/android/youtube/core/utils/Stack;Lorg/xml/sax/Attributes;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->result:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/xml/sax/Attributes;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->path:Lcom/google/android/youtube/core/utils/Stack;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->path:Lcom/google/android/youtube/core/utils/Stack;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/utils/Stack;->offer(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->rules:Lcom/google/android/youtube/core/converter/Rules;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/converter/Rules;->get(Ljava/lang/String;)Lcom/google/android/youtube/core/converter/XmlParser$Rule;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->attrsStack:Lcom/google/android/youtube/core/utils/Stack;

    if-eqz p4, :cond_1

    new-instance v2, Lorg/xml/sax/helpers/AttributesImpl;

    invoke-direct {v2, p4}, Lorg/xml/sax/helpers/AttributesImpl;-><init>(Lorg/xml/sax/Attributes;)V

    :goto_0
    invoke-virtual {v3, v2}, Lcom/google/android/youtube/core/utils/Stack;->offer(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->charsStack:Lcom/google/android/youtube/core/utils/Stack;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/utils/Stack;->offer(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/youtube/core/converter/XmlParser$InternalHandler;->stack:Lcom/google/android/youtube/core/utils/Stack;

    invoke-interface {v1, v2, p4}, Lcom/google/android/youtube/core/converter/XmlParser$Rule;->start(Lcom/google/android/youtube/core/utils/Stack;Lorg/xml/sax/Attributes;)V

    :cond_0
    return-void

    :cond_1
    # getter for: Lcom/google/android/youtube/core/converter/XmlParser;->EMPTY_ATTRS:Lorg/xml/sax/Attributes;
    invoke-static {}, Lcom/google/android/youtube/core/converter/XmlParser;->access$100()Lorg/xml/sax/Attributes;

    move-result-object v2

    goto :goto_0
.end method
