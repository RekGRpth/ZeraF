.class public Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;
.super Lcom/google/android/youtube/core/converter/http/XmlResponseConverter;
.source "SubtitlesConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/XmlResponseConverter",
        "<",
        "Lcom/google/android/youtube/core/model/Subtitles;",
        ">;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Lcom/google/android/youtube/core/model/SubtitleTrack;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_DURATION_MILLIS:I


# instance fields
.field private final rules:Lcom/google/android/youtube/core/converter/Rules;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, 0x40a00000

    invoke-static {v0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->secondsToMillis(F)I

    move-result v0

    sput v0, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->DEFAULT_DURATION_MILLIS:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/converter/XmlParser;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/XmlResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    new-instance v0, Lcom/google/android/youtube/core/converter/Rules$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->addFormat1Rules(Lcom/google/android/youtube/core/converter/Rules$Builder;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->addFormat2Rules(Lcom/google/android/youtube/core/converter/Rules$Builder;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;->build()Lcom/google/android/youtube/core/converter/Rules;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-void
.end method

.method static synthetic access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/xml/sax/Attributes;
    .param p1    # [Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(F)I
    .locals 1
    .param p0    # F

    invoke-static {p0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->secondsToMillis(F)I

    move-result v0

    return v0
.end method

.method static synthetic access$200()I
    .locals 1

    sget v0, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->DEFAULT_DURATION_MILLIS:I

    return v0
.end method

.method static synthetic access$300(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->anchorPointFromRawValue(I)I

    move-result v0

    return v0
.end method

.method private addFormat1Rules(Lcom/google/android/youtube/core/converter/Rules$Builder;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/converter/Rules$Builder;

    const-string v0, "/transcript"

    new-instance v1, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$2;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$2;-><init>(Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    const-string v1, "/transcript/text"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$1;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$1;-><init>(Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    return-void
.end method

.method private addFormat2Rules(Lcom/google/android/youtube/core/converter/Rules$Builder;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/converter/Rules$Builder;

    const-string v0, "/timedtext"

    new-instance v1, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$5;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$5;-><init>(Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    const-string v1, "/timedtext/window"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$4;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$4;-><init>(Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    const-string v1, "/timedtext/text"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$3;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$3;-><init>(Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    return-void
.end method

.method private static anchorPointFromRawValue(I)I
    .locals 1
    .param p0    # I

    const/16 v0, 0x22

    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x11

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x12

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x14

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x21

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x24

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method private getSubtitleUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/StringBuilder;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://video.google.com/timedtext?"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "hl="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&v="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&type=track"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&lang="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {p3, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&format="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static varargs getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Lorg/xml/sax/Attributes;
    .param p1    # [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    aget-object v2, p1, v0

    invoke-interface {p0, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_1
    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static secondsToMillis(F)I
    .locals 1
    .param p0    # F

    const/high16 v0, 0x447a0000

    mul-float/2addr v0, p0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/core/model/SubtitleTrack;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->convertRequest(Lcom/google/android/youtube/core/model/SubtitleTrack;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/youtube/core/model/SubtitleTrack;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 5
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->isAutoTranslated()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iget v4, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->format:I

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->getSubtitleUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&tlang="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    return-object v2

    :cond_0
    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iget v4, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->format:I

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->getSubtitleUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/StringBuilder;

    move-result-object v0

    goto :goto_0
.end method

.method protected getRules()Lcom/google/android/youtube/core/converter/Rules;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-object v0
.end method
