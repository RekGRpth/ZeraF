.class public abstract Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;
.super Lcom/google/android/youtube/core/converter/http/XmlResponseConverter;
.source "GDataResponseConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/youtube/core/converter/http/XmlResponseConverter",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final errorRules:Lcom/google/android/youtube/core/converter/Rules;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/converter/Rules$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;-><init>()V

    const-string v1, "/errors"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$6;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$6;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    const-string v1, "/errors/error"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$5;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$5;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    const-string v1, "/errors/error/domain"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$4;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$4;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    const-string v1, "/errors/error/code"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$3;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$3;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    const-string v1, "/errors/error/location"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$2;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$2;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    const-string v1, "/errors/error/internalReason"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$1;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter$1;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/Rules$Builder;->add(Ljava/lang/String;Lcom/google/android/youtube/core/converter/XmlParser$Rule;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;->build()Lcom/google/android/youtube/core/converter/Rules;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;->errorRules:Lcom/google/android/youtube/core/converter/Rules;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/converter/XmlParser;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/XmlResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    return-void
.end method

.method private mayContainGDataErrors(ILorg/apache/http/Header;)Z
    .locals 2
    .param p1    # I
    .param p2    # Lorg/apache/http/Header;

    const/16 v0, 0x190

    if-eq p1, v0, :cond_0

    const/16 v0, 0x191

    if-eq p1, v0, :cond_0

    const/16 v0, 0x193

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1f7

    if-ne p1, v0, :cond_1

    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, "xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected createHttpException(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;
    .locals 7
    .param p1    # Lorg/apache/http/HttpResponse;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v1

    const-string v5, "Content-Type"

    invoke-interface {p1, v5}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v5

    invoke-direct {p0, v2, v5}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;->mayContainGDataErrors(ILorg/apache/http/Header;)Z

    move-result v5

    if-eqz v5, :cond_0

    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;->parser:Lcom/google/android/youtube/core/converter/XmlParser;

    sget-object v6, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;->errorRules:Lcom/google/android/youtube/core/converter/Rules;

    invoke-virtual {v5, v4, v6}, Lcom/google/android/youtube/core/converter/XmlParser;->parse(Ljava/io/InputStream;Lcom/google/android/youtube/core/converter/Rules;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_0

    new-instance v5, Lcom/google/android/youtube/core/async/GDataResponseException;

    invoke-direct {v5, v2, v1, v0}, Lcom/google/android/youtube/core/async/GDataResponseException;-><init>(ILjava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v5

    :catch_0
    move-exception v5

    :cond_0
    const/16 v5, 0x191

    if-ne v2, v5, :cond_1

    const-string v5, "NoLinkedYouTubeAccount"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/async/GDataResponseException;->createYouTubeSignupRequired(ILjava/lang/String;)Lcom/google/android/youtube/core/async/GDataResponseException;

    move-result-object v5

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/youtube/core/converter/http/XmlResponseConverter;->createHttpException(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;

    move-result-object v5

    goto :goto_0
.end method
