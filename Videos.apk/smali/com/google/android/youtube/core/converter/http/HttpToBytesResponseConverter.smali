.class public final Lcom/google/android/youtube/core/converter/http/HttpToBytesResponseConverter;
.super Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;
.source "HttpToBytesResponseConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
        "<[B>;"
    }
.end annotation


# instance fields
.field private final DEFAULT_STREAM_SIZE:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;-><init>()V

    const/16 v0, 0x4000

    iput v0, p0, Lcom/google/android/youtube/core/converter/http/HttpToBytesResponseConverter;->DEFAULT_STREAM_SIZE:I

    return-void
.end method


# virtual methods
.method protected bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/HttpToBytesResponseConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    return-object v0
.end method

.method protected convertResponseEntity(Lorg/apache/http/HttpEntity;)[B
    .locals 5
    .param p1    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v3

    long-to-int v2, v3

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    if-lez v2, :cond_1

    :goto_1
    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-interface {p1, v0}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    goto :goto_0

    :cond_1
    const/16 v2, 0x4000

    goto :goto_1
.end method
