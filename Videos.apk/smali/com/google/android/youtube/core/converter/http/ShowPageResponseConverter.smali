.class public Lcom/google/android/youtube/core/converter/http/ShowPageResponseConverter;
.super Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;
.source "ShowPageResponseConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/GDataResponseConverter",
        "<",
        "Lcom/google/android/youtube/core/model/Page",
        "<",
        "Lcom/google/android/youtube/core/model/Show;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final rules:Lcom/google/android/youtube/core/converter/Rules;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/XmlParser;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/GDataResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    const-string v1, "/feed"

    invoke-static {v1}, Lcom/google/android/youtube/core/converter/RulesHelper;->createBuilderWithPageRules(Ljava/lang/String;)Lcom/google/android/youtube/core/converter/Rules$Builder;

    move-result-object v0

    const-string v1, "/feed"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/converter/http/ShowRulesHelper;->addShowPageRules(Lcom/google/android/youtube/core/converter/Rules$Builder;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/Rules$Builder;->build()Lcom/google/android/youtube/core/converter/Rules;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/converter/http/ShowPageResponseConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-void
.end method


# virtual methods
.method protected getRules()Lcom/google/android/youtube/core/converter/Rules;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/ShowPageResponseConverter;->rules:Lcom/google/android/youtube/core/converter/Rules;

    return-object v0
.end method
