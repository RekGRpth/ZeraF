.class public Lcom/google/android/youtube/core/converter/http/DeviceRegistrationConverter;
.super Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;
.source "DeviceRegistrationConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
        "<",
        "Lcom/google/android/youtube/core/model/DeviceAuth;",
        ">;"
    }
.end annotation


# instance fields
.field private final developerSecret:[B


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1    # [B

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;-><init>()V

    const-string v0, "developerSecret cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/DeviceRegistrationConverter;->developerSecret:[B

    return-void
.end method

.method private decryptDeviceKey(Ljava/lang/String;)[B
    .locals 10
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    const/16 v9, 0x14

    const/4 v8, 0x0

    const/4 v0, 0x0

    :try_start_0
    const-string v7, "AES/ECB/PKCS5Padding"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/youtube/core/converter/http/DeviceRegistrationConverter;->developerSecret:[B

    invoke-static {v7, v8}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v1

    invoke-static {p1, v8}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    :try_start_1
    new-instance v6, Ljavax/crypto/spec/SecretKeySpec;

    const-string v7, "AES"

    invoke-direct {v6, v1, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const/4 v7, 0x2

    invoke-virtual {v0, v7, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    invoke-virtual {v0, v2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v3

    array-length v7, v3

    if-le v7, v9, :cond_0

    const/16 v7, 0x14

    new-array v5, v7, [B

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x14

    invoke-static {v3, v7, v5, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_4

    :goto_0
    return-object v5

    :catch_0
    move-exception v4

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v4}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_1
    move-exception v4

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v4}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :cond_0
    move-object v5, v3

    goto :goto_0

    :catch_2
    move-exception v4

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v4}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_3
    move-exception v4

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v4}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_4
    move-exception v4

    new-instance v7, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v7, v4}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v7
.end method


# virtual methods
.method protected convertResponseContent(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/DeviceAuth;
    .locals 6
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    invoke-virtual {v3, p1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v4, "DeviceId"

    invoke-virtual {v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "DeviceKey"

    invoke-virtual {v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/converter/http/DeviceRegistrationConverter;->decryptDeviceKey(Ljava/lang/String;)[B

    move-result-object v1

    new-instance v4, Lcom/google/android/youtube/core/model/DeviceAuth;

    invoke-direct {v4, v0, v1}, Lcom/google/android/youtube/core/model/DeviceAuth;-><init>(Ljava/lang/String;[B)V

    return-object v4

    :cond_0
    new-instance v4, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v5, "invalid device registration response"

    invoke-direct {v4, v5}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method protected bridge synthetic convertResponseContent(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/DeviceRegistrationConverter;->convertResponseContent(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/DeviceAuth;

    move-result-object v0

    return-object v0
.end method
