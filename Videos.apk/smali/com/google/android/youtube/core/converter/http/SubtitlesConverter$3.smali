.class Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$3;
.super Lcom/google/android/youtube/core/converter/XmlParser$BaseRule;
.source "SubtitlesConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->addFormat2Rules(Lcom/google/android/youtube/core/converter/Rules$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter$3;->this$0:Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/XmlParser$BaseRule;-><init>()V

    return-void
.end method


# virtual methods
.method public end(Lcom/google/android/youtube/core/utils/Stack;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/utils/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/xml/sax/Attributes;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-class v0, Lcom/google/android/youtube/core/model/Subtitles$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/Stack;->peek(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Subtitles$Builder;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "w"

    aput-object v2, v1, v7

    const-string v2, "win"

    aput-object v2, v1, v6

    # invokes: Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v1}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v7}, Lcom/google/android/youtube/core/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v1

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "t"

    aput-object v3, v2, v7

    const-string v3, "start"

    aput-object v3, v2, v6

    # invokes: Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v2}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-array v3, v4, [Ljava/lang/String;

    const-string v4, "d"

    aput-object v4, v3, v7

    const-string v4, "dur"

    aput-object v4, v3, v6

    # invokes: Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v3}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->DEFAULT_DURATION_MILLIS:I
    invoke-static {}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->access$200()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "\n"

    const-string v5, "<br/>"

    invoke-virtual {p3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/String;

    const-string v6, "append"

    aput-object v6, v5, v7

    # invokes: Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->getValue(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2, v5}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;->access$000(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    add-int/2addr v3, v2

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/google/android/youtube/core/model/Subtitles$Builder;->addLineToWindow(ILjava/lang/String;II)Lcom/google/android/youtube/core/model/Subtitles$Builder;

    :goto_0
    return-void

    :cond_0
    add-int/2addr v3, v2

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/google/android/youtube/core/model/Subtitles$Builder;->addAppendedLineToWindow(ILjava/lang/String;II)Lcom/google/android/youtube/core/model/Subtitles$Builder;

    goto :goto_0
.end method
