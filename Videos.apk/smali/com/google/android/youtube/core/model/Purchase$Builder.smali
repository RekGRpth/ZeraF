.class public final Lcom/google/android/youtube/core/model/Purchase$Builder;
.super Ljava/lang/Object;
.source "Purchase.java"

# interfaces
.implements Lcom/google/android/youtube/core/model/ModelBuilder;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/model/Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Lcom/google/android/youtube/core/model/ModelBuilder",
        "<",
        "Lcom/google/android/youtube/core/model/Purchase;",
        ">;"
    }
.end annotation


# instance fields
.field private expirationDate:Ljava/util/Date;

.field private id:Ljava/lang/String;

.field private itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

.field private itemUri:Landroid/net/Uri;

.field private pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

.field private purchaseDate:Ljava/util/Date;

.field private purchasedVideo:Lcom/google/android/youtube/core/model/Video;

.field private status:Lcom/google/android/youtube/core/model/Purchase$Status;

.field private streamable:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/youtube/core/model/Purchase$ItemType;->OTHER:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    sget-object v0, Lcom/google/android/youtube/core/model/Purchase$Status;->OTHER:Lcom/google/android/youtube/core/model/Purchase$Status;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->status:Lcom/google/android/youtube/core/model/Purchase$Status;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->streamable:Z

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .param p1    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->id:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Purchase$ItemType;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->asUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->itemUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->purchaseDate:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->expirationDate:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/PricingStructure;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Purchase$Status;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->status:Lcom/google/android/youtube/core/model/Purchase$Status;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->streamable:Z

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->purchasedVideo:Lcom/google/android/youtube/core/model/Video;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Purchase$Builder;->build()Lcom/google/android/youtube/core/model/Purchase;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1
    .param p1    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->itemUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->asString(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->purchaseDate:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->expirationDate:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->status:Lcom/google/android/youtube/core/model/Purchase$Status;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->streamable:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->purchasedVideo:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/core/model/Purchase;
    .locals 10

    new-instance v0, Lcom/google/android/youtube/core/model/Purchase;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->itemUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->purchaseDate:Ljava/util/Date;

    iget-object v5, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->expirationDate:Ljava/util/Date;

    iget-object v6, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    iget-object v7, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->status:Lcom/google/android/youtube/core/model/Purchase$Status;

    iget-boolean v8, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->streamable:Z

    iget-object v9, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->purchasedVideo:Lcom/google/android/youtube/core/model/Video;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/core/model/Purchase;-><init>(Ljava/lang/String;Lcom/google/android/youtube/core/model/Purchase$ItemType;Landroid/net/Uri;Ljava/util/Date;Ljava/util/Date;Lcom/google/android/youtube/core/model/PricingStructure;Lcom/google/android/youtube/core/model/Purchase$Status;ZLcom/google/android/youtube/core/model/Video;)V

    return-object v0
.end method

.method public bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Purchase$Builder;->build()Lcom/google/android/youtube/core/model/Purchase;

    move-result-object v0

    return-object v0
.end method

.method public expirationDate(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Purchase$Builder;
    .locals 0
    .param p1    # Ljava/util/Date;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->expirationDate:Ljava/util/Date;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Purchase$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public itemType(Lcom/google/android/youtube/core/model/Purchase$ItemType;)Lcom/google/android/youtube/core/model/Purchase$Builder;
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/model/Purchase$ItemType;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    return-object p0
.end method

.method public itemUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Purchase$Builder;
    .locals 0
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->itemUri:Landroid/net/Uri;

    return-object p0
.end method

.method public pricePaid(Lcom/google/android/youtube/core/model/PricingStructure;)Lcom/google/android/youtube/core/model/Purchase$Builder;
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/model/PricingStructure;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    return-object p0
.end method

.method public purchaseDate(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Purchase$Builder;
    .locals 0
    .param p1    # Ljava/util/Date;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->purchaseDate:Ljava/util/Date;

    return-object p0
.end method

.method public purchasedVideo(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/core/model/Purchase$Builder;
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/model/Video;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->purchasedVideo:Lcom/google/android/youtube/core/model/Video;

    return-object p0
.end method

.method public status(Lcom/google/android/youtube/core/model/Purchase$Status;)Lcom/google/android/youtube/core/model/Purchase$Builder;
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/model/Purchase$Status;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->status:Lcom/google/android/youtube/core/model/Purchase$Status;

    return-object p0
.end method

.method public streamable(Z)Lcom/google/android/youtube/core/model/Purchase$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/Purchase$Builder;->streamable:Z

    return-object p0
.end method
