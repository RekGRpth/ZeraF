.class public Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;
.super Ljava/lang/Object;
.source "SubtitleWindow.java"

# interfaces
.implements Lcom/google/android/youtube/core/model/ModelBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/model/SubtitleWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/model/ModelBuilder",
        "<",
        "Lcom/google/android/youtube/core/model/SubtitleWindow;",
        ">;"
    }
.end annotation


# instance fields
.field private final id:I

.field private final settingsTimelineBuilder:Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline$Builder;

.field private final textTimelineBuilder:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->id:I

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->textTimelineBuilder:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline$Builder;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->settingsTimelineBuilder:Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline$Builder;

    return-void
.end method


# virtual methods
.method public addAppendedLine(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->textTimelineBuilder:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->addAppendedLine(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;

    return-object p0
.end method

.method public addLine(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->textTimelineBuilder:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->addLine(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;

    return-object p0
.end method

.method public addSettings(ILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->settingsTimelineBuilder:Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline$Builder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline$Builder;->addSettings(ILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline$Builder;

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/core/model/SubtitleWindow;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindow;

    iget v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->id:I

    iget-object v2, p0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->textTimelineBuilder:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline$Builder;->build()Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->settingsTimelineBuilder:Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline$Builder;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline$Builder;->build()Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/SubtitleWindow;-><init>(ILcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;Lcom/google/android/youtube/core/model/SubtitleWindow$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;->build()Lcom/google/android/youtube/core/model/SubtitleWindow;

    move-result-object v0

    return-object v0
.end method
