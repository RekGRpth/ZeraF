.class public final Lcom/google/android/youtube/core/model/GDataError$Builder;
.super Ljava/lang/Object;
.source "GDataError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/model/GDataError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private code:Ljava/lang/String;

.field private domain:Ljava/lang/String;

.field private internalReason:Ljava/lang/String;

.field private location:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/core/model/GDataError;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/core/model/GDataError;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/GDataError$Builder;->domain:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/GDataError$Builder;->code:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/GDataError$Builder;->location:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/GDataError$Builder;->internalReason:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/GDataError;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public code(Ljava/lang/String;)Lcom/google/android/youtube/core/model/GDataError$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/GDataError$Builder;->code:Ljava/lang/String;

    return-object p0
.end method

.method public domain(Ljava/lang/String;)Lcom/google/android/youtube/core/model/GDataError$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/GDataError$Builder;->domain:Ljava/lang/String;

    return-object p0
.end method

.method public internalReason(Ljava/lang/String;)Lcom/google/android/youtube/core/model/GDataError$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/GDataError$Builder;->internalReason:Ljava/lang/String;

    return-object p0
.end method

.method public location(Ljava/lang/String;)Lcom/google/android/youtube/core/model/GDataError$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/GDataError$Builder;->location:Ljava/lang/String;

    return-object p0
.end method
