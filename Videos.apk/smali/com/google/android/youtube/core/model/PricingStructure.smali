.class public final Lcom/google/android/youtube/core/model/PricingStructure;
.super Ljava/lang/Object;
.source "PricingStructure.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/model/PricingStructure$Builder;,
        Lcom/google/android/youtube/core/model/PricingStructure$Type;
    }
.end annotation


# instance fields
.field public final duration:Lcom/google/android/youtube/core/model/Duration;

.field public final formats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private volatile hashCode:I

.field public final infoUri:Landroid/net/Uri;

.field public final offerId:Ljava/lang/String;

.field public final price:Lcom/google/android/youtube/core/model/Money;

.field public final type:Lcom/google/android/youtube/core/model/PricingStructure$Type;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/model/PricingStructure$Type;Lcom/google/android/youtube/core/model/Duration;Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/model/PricingStructure$Type;
    .param p2    # Lcom/google/android/youtube/core/model/Duration;
    .param p3    # Lcom/google/android/youtube/core/model/Money;
    .param p5    # Landroid/net/Uri;
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/model/PricingStructure$Type;",
            "Lcom/google/android/youtube/core/model/Duration;",
            "Lcom/google/android/youtube/core/model/Money;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    iput-object p2, p0, Lcom/google/android/youtube/core/model/PricingStructure;->duration:Lcom/google/android/youtube/core/model/Duration;

    iput-object p3, p0, Lcom/google/android/youtube/core/model/PricingStructure;->price:Lcom/google/android/youtube/core/model/Money;

    iput-object p4, p0, Lcom/google/android/youtube/core/model/PricingStructure;->formats:Ljava/util/List;

    iput-object p5, p0, Lcom/google/android/youtube/core/model/PricingStructure;->infoUri:Landroid/net/Uri;

    iput-object p6, p0, Lcom/google/android/youtube/core/model/PricingStructure;->offerId:Ljava/lang/String;

    return-void
.end method

.method public static createPurchase(Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure;
    .locals 7
    .param p0    # Lcom/google/android/youtube/core/model/Money;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/model/Money;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/youtube/core/model/PricingStructure;"
        }
    .end annotation

    const-string v0, "price may not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "formats may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/model/PricingStructure;

    sget-object v1, Lcom/google/android/youtube/core/model/PricingStructure$Type;->PURCHASE:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    const/4 v2, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/model/PricingStructure;-><init>(Lcom/google/android/youtube/core/model/PricingStructure$Type;Lcom/google/android/youtube/core/model/Duration;Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createRental(Lcom/google/android/youtube/core/model/Duration;Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure;
    .locals 7
    .param p0    # Lcom/google/android/youtube/core/model/Duration;
    .param p1    # Lcom/google/android/youtube/core/model/Money;
    .param p3    # Landroid/net/Uri;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/model/Duration;",
            "Lcom/google/android/youtube/core/model/Money;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/youtube/core/model/PricingStructure;"
        }
    .end annotation

    const-string v0, "duration may not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "price may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "formats may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/model/PricingStructure;

    sget-object v1, Lcom/google/android/youtube/core/model/PricingStructure$Type;->RENT:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/model/PricingStructure;-><init>(Lcom/google/android/youtube/core/model/PricingStructure$Type;Lcom/google/android/youtube/core/model/Duration;Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createSubscription(Lcom/google/android/youtube/core/model/Money;Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure;
    .locals 7
    .param p0    # Lcom/google/android/youtube/core/model/Money;
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const-string v0, "price may not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/model/PricingStructure;

    sget-object v1, Lcom/google/android/youtube/core/model/PricingStructure$Type;->SUBSCRIPTION:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    move-object v3, p0

    move-object v5, v2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/model/PricingStructure;-><init>(Lcom/google/android/youtube/core/model/PricingStructure$Type;Lcom/google/android/youtube/core/model/Duration;Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .param p1    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/PricingStructure;->buildUpon()Lcom/google/android/youtube/core/model/PricingStructure$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/PricingStructure$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->type(Lcom/google/android/youtube/core/model/PricingStructure$Type;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->duration:Lcom/google/android/youtube/core/model/Duration;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->duration(Lcom/google/android/youtube/core/model/Duration;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->infoUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->price:Lcom/google/android/youtube/core/model/Money;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price(Lcom/google/android/youtube/core/model/Money;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->formats:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats(Ljava/util/List;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->offerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId(Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/youtube/core/model/PricingStructure;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/core/model/PricingStructure;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/PricingStructure;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure;->duration:Lcom/google/android/youtube/core/model/Duration;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/PricingStructure;->duration:Lcom/google/android/youtube/core/model/Duration;

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure;->infoUri:Landroid/net/Uri;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/PricingStructure;->infoUri:Landroid/net/Uri;

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure;->price:Lcom/google/android/youtube/core/model/Money;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/PricingStructure;->price:Lcom/google/android/youtube/core/model/Money;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/model/Money;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure;->formats:Ljava/util/List;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/PricingStructure;->formats:Ljava/util/List;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure;->offerId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/PricingStructure;->offerId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/youtube/core/model/PricingStructure;->hashCode:I

    if-nez v0, :cond_1

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/PricingStructure$Type;->ordinal()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->duration:Lcom/google/android/youtube/core/model/Duration;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->duration:Lcom/google/android/youtube/core/model/Duration;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Duration;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v3, v1

    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->infoUri:Landroid/net/Uri;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->infoUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v3, v1

    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->formats:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->formats:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v3, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure;->offerId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/model/PricingStructure;->offerId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/youtube/core/model/PricingStructure;->price:Lcom/google/android/youtube/core/model/Money;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/Money;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    iput v0, p0, Lcom/google/android/youtube/core/model/PricingStructure;->hashCode:I

    :cond_1
    return v0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    sget-object v2, Lcom/google/android/youtube/core/model/PricingStructure$Type;->RENT:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    if-ne v0, v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "duration=["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/model/PricingStructure;->duration:Lcom/google/android/youtube/core/model/Duration;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "], "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "info=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->infoUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "money=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->price:Lcom/google/android/youtube/core/model/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "formats=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->formats:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "offerId=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure;->offerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
