.class public final Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;
.super Ljava/lang/Object;
.source "SubtitleWindowSnapshot.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot$1;,
        Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot$ParcelableCreator;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final settings:Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

.field public final snapshotTimeMillis:I

.field public final text:Ljava/lang/String;

.field public final windowId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot$ParcelableCreator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot$ParcelableCreator;-><init>(Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot$1;)V

    sput-object v0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Lcom/google/android/youtube/core/model/SubtitleWindowSettings;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->windowId:I

    iput p2, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->snapshotTimeMillis:I

    iput-object p3, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    const-class v0, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;-><init>(IILjava/lang/String;Lcom/google/android/youtube/core/model/SubtitleWindowSettings;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->snapshotTimeMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " settings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->snapshotTimeMillis:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
