.class public final Lcom/google/android/youtube/core/model/SubtitleWindow;
.super Ljava/lang/Object;
.source "SubtitleWindow.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/model/SubtitleWindow$1;,
        Lcom/google/android/youtube/core/model/SubtitleWindow$Builder;
    }
.end annotation


# instance fields
.field private final id:I

.field final settingsTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

.field final textTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;


# direct methods
.method private constructor <init>(ILcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;
    .param p3    # Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/core/model/SubtitleWindow;->id:I

    iput-object p2, p0, Lcom/google/android/youtube/core/model/SubtitleWindow;->textTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    iput-object p3, p0, Lcom/google/android/youtube/core/model/SubtitleWindow;->settingsTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

    return-void
.end method

.method synthetic constructor <init>(ILcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;Lcom/google/android/youtube/core/model/SubtitleWindow$1;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;
    .param p3    # Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;
    .param p4    # Lcom/google/android/youtube/core/model/SubtitleWindow$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/core/model/SubtitleWindow;-><init>(ILcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;)V

    return-void
.end method


# virtual methods
.method public getSettingsAt(I)Lcom/google/android/youtube/core/model/SubtitleWindowSettings;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindow;->settingsTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;->getSubtitleWindowSettingsAt(I)Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    move-result-object v0

    return-object v0
.end method

.method public getSubtitleWindowSnapshotAt(I)Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;
    .locals 4
    .param p1    # I

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;

    iget v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindow;->id:I

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/model/SubtitleWindow;->getTextAt(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/model/SubtitleWindow;->getSettingsAt(I)Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    move-result-object v3

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;-><init>(IILjava/lang/String;Lcom/google/android/youtube/core/model/SubtitleWindowSettings;)V

    return-object v0
.end method

.method public getTextAt(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SubtitleWindow;->textTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;->getSubtitleWindowTextAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindow;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindow;->textTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " settings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SubtitleWindow;->settingsTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
