.class public final Lcom/google/android/youtube/core/model/StreamExtra$Builder;
.super Ljava/lang/Object;
.source "StreamExtra.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/model/StreamExtra;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private lastModified:Ljava/lang/String;

.field private sizeInBytes:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .param p1    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->sizeInBytes:J

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->lastModified:Ljava/lang/String;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->build()Lcom/google/android/youtube/core/model/StreamExtra;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2
    .param p1    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-wide v0, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->sizeInBytes:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->lastModified:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/core/model/StreamExtra;
    .locals 4

    iget-wide v0, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->sizeInBytes:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->lastModified:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/model/StreamExtra;->NO_EXTRA:Lcom/google/android/youtube/core/model/StreamExtra;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/model/StreamExtra;

    iget-wide v1, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->sizeInBytes:J

    iget-object v3, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->lastModified:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/model/StreamExtra;-><init>(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public lastModified(Ljava/lang/String;)Lcom/google/android/youtube/core/model/StreamExtra$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->lastModified:Ljava/lang/String;

    return-object p0
.end method

.method public sizeInBytes(J)Lcom/google/android/youtube/core/model/StreamExtra$Builder;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/youtube/core/model/StreamExtra$Builder;->sizeInBytes:J

    return-object p0
.end method
