.class final Lcom/google/android/youtube/core/model/SubtitleWindowSettings$ParcelableCreator;
.super Ljava/lang/Object;
.source "SubtitleWindowSettings.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/core/model/SubtitleWindowSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ParcelableCreator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/youtube/core/model/SubtitleWindowSettings;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/model/SubtitleWindowSettings$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleWindowSettings$1;

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/SubtitleWindowSettings$ParcelableCreator;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/youtube/core/model/SubtitleWindowSettings;
    .locals 2
    .param p1    # Landroid/os/Parcel;

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;-><init>(Landroid/os/Parcel;Lcom/google/android/youtube/core/model/SubtitleWindowSettings$1;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/model/SubtitleWindowSettings$ParcelableCreator;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/youtube/core/model/SubtitleWindowSettings;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/model/SubtitleWindowSettings$ParcelableCreator;->newArray(I)[Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    move-result-object v0

    return-object v0
.end method
