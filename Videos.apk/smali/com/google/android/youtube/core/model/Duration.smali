.class public final Lcom/google/android/youtube/core/model/Duration;
.super Ljava/lang/Object;
.source "Duration.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field public final days:F

.field private volatile hashCode:I

.field public final hours:F

.field public final minutes:F

.field public final months:F

.field public final seconds:F

.field public final weeks:F

.field public final years:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "([+-.\\d]+[A-Z])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/model/Duration;->PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(FFFFFFF)V
    .locals 5
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F
    .param p6    # F
    .param p7    # F

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    cmpl-float v0, p1, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "years may not be negative"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    cmpl-float v0, p2, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "months may not be negative"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    cmpl-float v0, p3, v4

    if-ltz v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "weeks may not be negative"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    cmpl-float v0, p4, v4

    if-ltz v0, :cond_3

    move v0, v1

    :goto_3
    const-string v3, "days may not be negative"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    cmpl-float v0, p5, v4

    if-ltz v0, :cond_4

    move v0, v1

    :goto_4
    const-string v3, "hours may not be negative"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    cmpl-float v0, p6, v4

    if-ltz v0, :cond_5

    move v0, v1

    :goto_5
    const-string v3, "minutes may not be negative"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    cmpl-float v0, p7, v4

    if-ltz v0, :cond_6

    :goto_6
    const-string v0, "seconds may not be negative"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/youtube/core/model/Duration;->years:F

    iput p2, p0, Lcom/google/android/youtube/core/model/Duration;->months:F

    iput p3, p0, Lcom/google/android/youtube/core/model/Duration;->weeks:F

    iput p4, p0, Lcom/google/android/youtube/core/model/Duration;->days:F

    iput p5, p0, Lcom/google/android/youtube/core/model/Duration;->hours:F

    iput p6, p0, Lcom/google/android/youtube/core/model/Duration;->minutes:F

    iput p7, p0, Lcom/google/android/youtube/core/model/Duration;->seconds:F

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v1, v2

    goto :goto_6
.end method

.method private static parseFloat(Ljava/lang/String;)F
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/16 v1, 0x2c

    const/16 v2, 0x2e

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Duration;
    .locals 18
    .param p0    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v1, "[PT]"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    array-length v1, v12

    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v1, v0, :cond_0

    const/4 v1, 0x1

    aget-object v9, v12, v1

    :goto_0
    array-length v1, v12

    const/16 v16, 0x2

    move/from16 v0, v16

    if-le v1, v0, :cond_1

    const/4 v1, 0x2

    aget-object v13, v12, v1

    :goto_1
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/youtube/core/model/Duration;->PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    :goto_2
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v11}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v10

    const/4 v1, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/model/Duration;->parseFloat(Ljava/lang/String;)F

    move-result v15

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v10, v1}, Ljava/lang/String;->charAt(I)C

    move-result v14

    sparse-switch v14, :sswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Invalid unit: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v9, 0x0

    goto :goto_0

    :cond_1
    const/4 v13, 0x0

    goto :goto_1

    :sswitch_0
    move v2, v15

    goto :goto_2

    :sswitch_1
    move v3, v15

    goto :goto_2

    :sswitch_2
    move v4, v15

    goto :goto_2

    :sswitch_3
    move v5, v15

    goto :goto_2

    :cond_2
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/android/youtube/core/model/Duration;->PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    :goto_3
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v11}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v10

    const/4 v1, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/model/Duration;->parseFloat(Ljava/lang/String;)F

    move-result v15

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v10, v1}, Ljava/lang/String;->charAt(I)C

    move-result v14

    sparse-switch v14, :sswitch_data_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Invalid unit: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_4
    move v6, v15

    goto :goto_3

    :sswitch_5
    move v7, v15

    goto :goto_3

    :sswitch_6
    move v8, v15

    goto :goto_3

    :cond_3
    new-instance v1, Lcom/google/android/youtube/core/model/Duration;

    invoke-direct/range {v1 .. v8}, Lcom/google/android/youtube/core/model/Duration;-><init>(FFFFFFF)V

    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x44 -> :sswitch_3
        0x4d -> :sswitch_1
        0x57 -> :sswitch_2
        0x59 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x48 -> :sswitch_4
        0x4d -> :sswitch_5
        0x53 -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/youtube/core/model/Duration;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/core/model/Duration;

    iget v3, p0, Lcom/google/android/youtube/core/model/Duration;->years:F

    iget v4, v0, Lcom/google/android/youtube/core/model/Duration;->years:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/android/youtube/core/model/Duration;->months:F

    iget v4, v0, Lcom/google/android/youtube/core/model/Duration;->months:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/android/youtube/core/model/Duration;->weeks:F

    iget v4, v0, Lcom/google/android/youtube/core/model/Duration;->weeks:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/android/youtube/core/model/Duration;->days:F

    iget v4, v0, Lcom/google/android/youtube/core/model/Duration;->days:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/android/youtube/core/model/Duration;->hours:F

    iget v4, v0, Lcom/google/android/youtube/core/model/Duration;->hours:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/android/youtube/core/model/Duration;->minutes:F

    iget v4, v0, Lcom/google/android/youtube/core/model/Duration;->minutes:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/android/youtube/core/model/Duration;->seconds:F

    iget v4, v0, Lcom/google/android/youtube/core/model/Duration;->seconds:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/google/android/youtube/core/model/Duration;->hashCode:I

    if-nez v0, :cond_0

    const/16 v0, 0x11

    iget v1, p0, Lcom/google/android/youtube/core/model/Duration;->years:F

    float-to-int v1, v1

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->months:F

    float-to-int v2, v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->weeks:F

    float-to-int v2, v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->days:F

    float-to-int v2, v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->hours:F

    float-to-int v2, v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->minutes:F

    float-to-int v2, v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->seconds:F

    float-to-int v2, v2

    add-int v0, v1, v2

    iput v0, p0, Lcom/google/android/youtube/core/model/Duration;->hashCode:I

    :cond_0
    return v0
.end method

.method public inSeconds()J
    .locals 10

    const-wide/high16 v8, 0x4038000000000000L

    const-wide/high16 v6, 0x404e000000000000L

    iget v0, p0, Lcom/google/android/youtube/core/model/Duration;->seconds:F

    float-to-double v0, v0

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->minutes:F

    float-to-double v2, v2

    mul-double/2addr v2, v6

    add-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->hours:F

    float-to-double v2, v2

    mul-double/2addr v2, v6

    mul-double/2addr v2, v6

    add-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->days:F

    float-to-double v2, v2

    mul-double/2addr v2, v8

    mul-double/2addr v2, v6

    mul-double/2addr v2, v6

    add-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->weeks:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x401c000000000000L

    mul-double/2addr v2, v4

    mul-double/2addr v2, v8

    mul-double/2addr v2, v6

    mul-double/2addr v2, v6

    add-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->months:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x403f000000000000L

    mul-double/2addr v2, v4

    mul-double/2addr v2, v8

    mul-double/2addr v2, v6

    mul-double/2addr v2, v6

    add-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/youtube/core/model/Duration;->years:F

    float-to-double v2, v2

    const-wide v4, 0x4076d00000000000L

    mul-double/2addr v2, v4

    mul-double/2addr v2, v8

    mul-double/2addr v2, v6

    mul-double/2addr v2, v6

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    return-wide v0
.end method
