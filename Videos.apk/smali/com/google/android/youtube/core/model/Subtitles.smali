.class public final Lcom/google/android/youtube/core/model/Subtitles;
.super Ljava/lang/Object;
.source "Subtitles.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/model/Subtitles$1;,
        Lcom/google/android/youtube/core/model/Subtitles$Builder;
    }
.end annotation


# instance fields
.field private transient eventTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final windows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleWindow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleWindow;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Subtitles;->windows:Ljava/util/List;

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/Subtitles;->initEventTimes()V

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/google/android/youtube/core/model/Subtitles$1;)V
    .locals 0
    .param p1    # Ljava/util/List;
    .param p2    # Lcom/google/android/youtube/core/model/Subtitles$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/Subtitles;-><init>(Ljava/util/List;)V

    return-void
.end method

.method private initEventTimes()V
    .locals 4

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/youtube/core/model/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/core/model/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/model/SubtitleWindow;

    iget-object v3, v2, Lcom/google/android/youtube/core/model/SubtitleWindow;->textTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;->startTimes:Ljava/util/List;

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    iget-object v3, v2, Lcom/google/android/youtube/core/model/SubtitleWindow;->textTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;->endTimes:Ljava/util/List;

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    iget-object v3, v2, Lcom/google/android/youtube/core/model/SubtitleWindow;->settingsTimeline:Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;->startTimes:Ljava/util/List;

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/Subtitles;->eventTimes:Ljava/util/List;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0
    .param p1    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/Subtitles;->initEventTimes()V

    return-void
.end method


# virtual methods
.method public getEventTimes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Subtitles;->eventTimes:Ljava/util/List;

    return-object v0
.end method

.method public getSubtitleWindowSnapshotsAt(I)Ljava/util/List;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/core/model/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/model/SubtitleWindow;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/model/SubtitleWindow;->getSubtitleWindowSnapshotAt(I)Lcom/google/android/youtube/core/model/SubtitleWindowSnapshot;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/core/model/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Subtitles;->windows:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/model/SubtitleWindow;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/SubtitleWindow;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
