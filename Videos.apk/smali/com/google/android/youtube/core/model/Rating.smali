.class public Lcom/google/android/youtube/core/model/Rating;
.super Ljava/lang/Object;
.source "Rating.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final classification:Ljava/lang/String;

.field private volatile hashCode:I

.field public final scheme:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "scheme may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Rating;->scheme:Ljava/lang/String;

    const-string v0, "classification may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Rating;->classification:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p1, :cond_2

    instance-of v3, p1, Lcom/google/android/youtube/core/model/Rating;

    if-nez v3, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/core/model/Rating;

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Rating;->scheme:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Rating;->scheme:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Rating;->classification:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Rating;->classification:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/google/android/youtube/core/model/Rating;->hashCode:I

    if-nez v0, :cond_0

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Rating;->scheme:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Rating;->classification:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    iput v0, p0, Lcom/google/android/youtube/core/model/Rating;->hashCode:I

    :cond_0
    return v0
.end method
