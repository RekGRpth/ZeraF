.class Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;
.super Ljava/lang/Object;
.source "BytesToPipeConverter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/core/v9/BytesToPipeConverter;->writeDataInBackground(Ljava/io/OutputStream;[B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/core/v9/BytesToPipeConverter;

.field final synthetic val$data:[B

.field final synthetic val$out:Ljava/io/OutputStream;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/v9/BytesToPipeConverter;Ljava/io/OutputStream;[B)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;->this$0:Lcom/google/android/youtube/core/v9/BytesToPipeConverter;

    iput-object p2, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    iput-object p3, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;->val$data:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;->val$data:[B

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Exception closing pipe"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v1, "Exception writing to pipe"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v1, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "Exception closing pipe"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_4
    iget-object v2, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;->val$out:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_1
    throw v1

    :catch_3
    move-exception v0

    const-string v2, "Exception closing pipe"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
