.class public Lcom/google/android/youtube/core/v9/BytesToPipeConverter;
.super Ljava/lang/Object;
.source "BytesToPipeConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/ResponseConverter",
        "<[B",
        "Landroid/os/ParcelFileDescriptor;",
        ">;"
    }
.end annotation


# instance fields
.field public final writeExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter;->writeExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private writeDataInBackground(Ljava/io/OutputStream;[B)V
    .locals 2
    .param p1    # Ljava/io/OutputStream;
    .param p2    # [B

    iget-object v0, p0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter;->writeExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/core/v9/BytesToPipeConverter$1;-><init>(Lcom/google/android/youtube/core/v9/BytesToPipeConverter;Ljava/io/OutputStream;[B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public convertResponse([B)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    new-instance v2, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-direct {v2, v3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    invoke-direct {p0, v2, p1}, Lcom/google/android/youtube/core/v9/BytesToPipeConverter;->writeDataInBackground(Ljava/io/OutputStream;[B)V

    const/4 v2, 0x0

    aget-object v2, v1, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v2, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/v9/BytesToPipeConverter;->convertResponse([B)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method
