.class public interface abstract Lcom/google/android/youtube/core/utils/NetworkStatus;
.super Ljava/lang/Object;
.source "NetworkStatus.java"


# virtual methods
.method public abstract getNetworkType()Ljava/lang/String;
.end method

.method public abstract isChargeableNetwork()Z
.end method

.method public abstract isFastNetwork()Z
.end method

.method public abstract isMobileNetwork()Z
.end method

.method public abstract isMobileNetworkCapable()Z
.end method

.method public abstract isNetworkAvailable()Z
.end method
