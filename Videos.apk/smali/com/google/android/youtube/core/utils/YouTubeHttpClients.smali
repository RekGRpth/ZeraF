.class public final Lcom/google/android/youtube/core/utils/YouTubeHttpClients;
.super Ljava/lang/Object;
.source "YouTubeHttpClients.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/utils/YouTubeHttpClients$CookieStoreHttpClient;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createClientConnectionManager(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 5
    .param p0    # Lorg/apache/http/params/HttpParams;

    new-instance v0, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v3

    const/16 v4, 0x50

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v3

    const/16 v4, 0x1bb

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    new-instance v1, Lorg/apache/http/conn/params/ConnPerRouteBean;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {p0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    new-instance v1, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v1, p0, v0}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    return-object v1
.end method

.method public static createCookieStoreHttpClient(Ljava/lang/String;Lorg/apache/http/client/CookieStore;)Lorg/apache/http/client/HttpClient;
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Lorg/apache/http/client/CookieStore;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/YouTubeHttpClients;->createHttpParams(Ljava/lang/String;)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/YouTubeHttpClients;->createClientConnectionManager(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    new-instance v0, Lcom/google/android/youtube/core/utils/YouTubeHttpClients$CookieStoreHttpClient;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/youtube/core/utils/YouTubeHttpClients$CookieStoreHttpClient;-><init>(Lorg/apache/http/client/CookieStore;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    new-instance v3, Lcom/google/android/youtube/core/converter/http/GzipResponseInterceptor;

    invoke-direct {v3}, Lcom/google/android/youtube/core/converter/http/GzipResponseInterceptor;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/utils/YouTubeHttpClients$CookieStoreHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    return-object v0
.end method

.method public static createDefaultHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;
    .locals 4
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/YouTubeHttpClients;->createHttpParams(Ljava/lang/String;)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/YouTubeHttpClients;->createClientConnectionManager(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    new-instance v3, Lcom/google/android/youtube/core/converter/http/GzipResponseInterceptor;

    invoke-direct {v3}, Lcom/google/android/youtube/core/converter/http/GzipResponseInterceptor;-><init>()V

    invoke-virtual {v0, v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    return-object v0
.end method

.method private static createHttpParams(Ljava/lang/String;)Lorg/apache/http/params/HttpParams;
    .locals 3
    .param p0    # Ljava/lang/String;

    const/16 v2, 0x4e20

    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    invoke-static {v0, p0}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    return-object v0
.end method
