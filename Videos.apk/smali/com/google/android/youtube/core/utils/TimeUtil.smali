.class public Lcom/google/android/youtube/core/utils/TimeUtil;
.super Ljava/lang/Object;
.source "TimeUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getRemainingDays(JJ)I
    .locals 4
    .param p0    # J
    .param p2    # J

    sub-long v0, p0, p2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const/4 v2, -0x1

    :goto_0
    return v2

    :cond_0
    const-wide/32 v2, 0x5265c00

    div-long v2, v0, v2

    long-to-int v2, v2

    goto :goto_0
.end method

.method public static getTimeToExpirationString(JJLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 10
    .param p0    # J
    .param p2    # J
    .param p4    # Landroid/content/res/Resources;

    const/4 v4, 0x1

    const/4 v5, 0x0

    cmp-long v3, p2, p0

    if-ltz v3, :cond_0

    const v3, 0x7f0a0127

    invoke-virtual {p4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    sub-long v6, p0, p2

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    long-to-int v2, v6

    div-int/lit8 v1, v2, 0x3c

    if-lez v1, :cond_2

    rem-int/lit8 v3, v2, 0x3c

    if-lez v3, :cond_1

    move v3, v4

    :goto_1
    add-int/2addr v1, v3

    div-int/lit8 v0, v1, 0x18

    if-lez v0, :cond_4

    rem-int/lit8 v3, v1, 0x18

    if-lez v3, :cond_3

    move v3, v4

    :goto_2
    add-int/2addr v0, v3

    if-lez v0, :cond_5

    const v3, 0x7f0e0007

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p4, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    move v3, v5

    goto :goto_1

    :cond_2
    move v3, v5

    goto :goto_1

    :cond_3
    move v3, v5

    goto :goto_2

    :cond_4
    move v3, v5

    goto :goto_2

    :cond_5
    if-lez v1, :cond_6

    const v3, 0x7f0e0008

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p4, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_6
    const/16 v3, 0xa

    if-le v2, v3, :cond_7

    const v3, 0x7f0a0125

    invoke-virtual {p4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_7
    const v3, 0x7f0a0126

    invoke-virtual {p4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
