.class public final Lcom/google/android/youtube/core/utils/WorkScheduler;
.super Ljava/lang/Object;
.source "WorkScheduler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/core/utils/WorkScheduler$Client;
    }
.end annotation


# instance fields
.field private final handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/youtube/core/utils/WorkScheduler$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/utils/WorkScheduler$1;-><init>(Lcom/google/android/youtube/core/utils/WorkScheduler;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/WorkScheduler;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/utils/WorkScheduler;Lcom/google/android/youtube/core/utils/WorkScheduler$Client;II)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/core/utils/WorkScheduler;
    .param p1    # Lcom/google/android/youtube/core/utils/WorkScheduler$Client;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/core/utils/WorkScheduler;->scheduleNext(Lcom/google/android/youtube/core/utils/WorkScheduler$Client;II)V

    return-void
.end method

.method private scheduleNext(Lcom/google/android/youtube/core/utils/WorkScheduler$Client;II)V
    .locals 8
    .param p1    # Lcom/google/android/youtube/core/utils/WorkScheduler$Client;
    .param p2    # I
    .param p3    # I

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/core/utils/WorkScheduler;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v7, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    const/4 v3, -0x1

    if-eq p3, v3, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    int-to-long v5, p2

    sub-long/2addr v3, v5

    long-to-int v0, v3

    sub-int v1, p3, v0

    iget-object v3, p0, Lcom/google/android/youtube/core/utils/WorkScheduler;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v7, p2, v7, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    if-lez v1, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/core/utils/WorkScheduler;->handler:Landroid/os/Handler;

    int-to-long v4, v1

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/core/utils/WorkScheduler;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method


# virtual methods
.method public schedule(Lcom/google/android/youtube/core/utils/WorkScheduler$Client;II)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/core/utils/WorkScheduler$Client;
    .param p2    # I
    .param p3    # I

    const-string v1, "client cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    int-to-long v3, p2

    sub-long/2addr v1, v3

    long-to-int v0, v1

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/youtube/core/utils/WorkScheduler;->scheduleNext(Lcom/google/android/youtube/core/utils/WorkScheduler$Client;II)V

    return-void
.end method

.method public unschedule(Lcom/google/android/youtube/core/utils/WorkScheduler$Client;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/utils/WorkScheduler$Client;

    iget-object v0, p0, Lcom/google/android/youtube/core/utils/WorkScheduler;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    return-void
.end method
