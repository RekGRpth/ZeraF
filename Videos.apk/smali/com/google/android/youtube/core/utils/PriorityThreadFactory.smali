.class public Lcom/google/android/youtube/core/utils/PriorityThreadFactory;
.super Ljava/lang/Object;
.source "PriorityThreadFactory.java"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# instance fields
.field private final defaultFactory:Ljava/util/concurrent/ThreadFactory;

.field private final threadPriority:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;->threadPriority:I

    invoke-static {}, Ljava/util/concurrent/Executors;->defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;->defaultFactory:Ljava/util/concurrent/ThreadFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/core/utils/PriorityThreadFactory;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/utils/PriorityThreadFactory;

    iget v0, p0, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;->threadPriority:I

    return v0
.end method


# virtual methods
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;->defaultFactory:Ljava/util/concurrent/ThreadFactory;

    new-instance v1, Lcom/google/android/youtube/core/utils/PriorityThreadFactory$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/core/utils/PriorityThreadFactory$1;-><init>(Lcom/google/android/youtube/core/utils/PriorityThreadFactory;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ThreadFactory;->newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v0

    return-object v0
.end method
