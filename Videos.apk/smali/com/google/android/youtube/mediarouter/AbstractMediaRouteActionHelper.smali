.class public abstract Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;
.super Ljava/lang/Object;
.source "AbstractMediaRouteActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$1;,
        Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private liveAudioRouteCount:I

.field private final mediaRouteCategories:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mediaRouteCount:I

.field private final mediaRouter:Ljava/lang/Object;

.field private final mediaRouterCallback:Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;

.field private final supportedRouteTypes:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat;->forApplication(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouter:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;-><init>(Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$1;)V

    iput-object v0, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouterCallback:Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;

    iput p2, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->supportedRouteTypes:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouteCount:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouteCategories:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;

    invoke-direct {p0}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->updateRouteCount()V

    return-void
.end method

.method private updateRouteCount()V
    .locals 7

    iget-object v5, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouter:Ljava/lang/Object;

    invoke-static {v5}, Lcom/android/athome/picker/media/MediaRouterCompat;->getRouteCount(Ljava/lang/Object;)I

    move-result v3

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouteCategories:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->clear()V

    const/4 v5, 0x0

    iput v5, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->liveAudioRouteCount:I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v5, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouter:Ljava/lang/Object;

    invoke-static {v5, v1}, Lcom/android/athome/picker/media/MediaRouterCompat;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getSupportedTypes(Ljava/lang/Object;)I

    move-result v4

    iget v5, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->supportedRouteTypes:I

    and-int/2addr v5, v4

    if-eqz v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    iget-object v5, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouteCategories:Ljava/util/Set;

    invoke-static {v2}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getCategory(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    and-int/lit8 v5, v4, 0x1

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->liveAudioRouteCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->liveAudioRouteCount:I

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget v5, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouteCount:I

    if-eq v5, v0, :cond_3

    iput v0, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouteCount:I

    invoke-virtual {p0}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->invalidate()V

    :cond_3
    return-void
.end method


# virtual methods
.method public final destroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouter:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouterCallback:Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/MediaRouterCompat;->removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    return-void
.end method

.method public final init()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouter:Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->supportedRouteTypes:I

    iget-object v2, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouterCallback:Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper$MediaRouteCallback;

    invoke-static {v0, v1, v2}, Lcom/android/athome/picker/media/MediaRouterCompat;->addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    invoke-direct {p0}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->updateRouteCount()V

    return-void
.end method

.method public final invalidate()V
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->mediaRouteCount:I

    if-le v1, v0, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;->onVisibility(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract onVisibility(Z)V
.end method
