.class public Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;
.super Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;
.source "DirectMediaRouteActionHelper.java"


# instance fields
.field private forceHidden:Z

.field private final mediaRouteButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/view/View;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/mediarouter/AbstractMediaRouteActionHelper;-><init>(Landroid/content/Context;I)V

    iput-object p3, p0, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;->mediaRouteButton:Landroid/view/View;

    invoke-static {p3, p2}, Lcom/android/athome/picker/media/MediaRouteButtonCompat;->setRouteTypes(Landroid/view/View;I)V

    return-void
.end method


# virtual methods
.method protected onVisibility(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;->mediaRouteButton:Landroid/view/View;

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;->forceHidden:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setForceHidden(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;->forceHidden:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;->forceHidden:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/mediarouter/DirectMediaRouteActionHelper;->invalidate()V

    :cond_0
    return-void
.end method
