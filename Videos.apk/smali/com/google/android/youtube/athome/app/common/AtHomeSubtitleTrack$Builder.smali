.class public Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;
.super Ljava/lang/Object;
.source "AtHomeSubtitleTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private languageCode:Ljava/lang/String;

.field private trackName:Ljava/lang/String;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->languageCode:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->trackName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$1;)V

    return-object v0
.end method

.method public languageCode(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->languageCode:Ljava/lang/String;

    return-object p0
.end method

.method public trackName(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->trackName:Ljava/lang/String;

    return-object p0
.end method

.method public videoId(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->videoId:Ljava/lang/String;

    return-object p0
.end method
