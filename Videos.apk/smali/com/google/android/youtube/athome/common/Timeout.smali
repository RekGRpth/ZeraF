.class public abstract Lcom/google/android/youtube/athome/common/Timeout;
.super Ljava/lang/Object;
.source "Timeout.java"


# instance fields
.field private final handler:Landroid/os/Handler;

.field private final runnable:Ljava/lang/Runnable;

.field private final timeoutMillis:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;J)V
    .locals 2
    .param p1    # Landroid/os/Handler;
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "handler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/athome/common/Timeout;->handler:Landroid/os/Handler;

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "timeout must be strictly positive"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iput-wide p2, p0, Lcom/google/android/youtube/athome/common/Timeout;->timeoutMillis:J

    new-instance v0, Lcom/google/android/youtube/athome/common/Timeout$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/athome/common/Timeout$1;-><init>(Lcom/google/android/youtube/athome/common/Timeout;)V

    iput-object v0, p0, Lcom/google/android/youtube/athome/common/Timeout;->runnable:Ljava/lang/Runnable;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract onTimeout()V
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/athome/common/Timeout;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/athome/common/Timeout;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public start()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/youtube/athome/common/Timeout;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/athome/common/Timeout;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/athome/common/Timeout;->runnable:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/youtube/athome/common/Timeout;->timeoutMillis:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
