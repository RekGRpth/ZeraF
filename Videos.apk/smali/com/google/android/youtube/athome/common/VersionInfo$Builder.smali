.class public Lcom/google/android/youtube/athome/common/VersionInfo$Builder;
.super Ljava/lang/Object;
.source "VersionInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/athome/common/VersionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private appVersion:Ljava/lang/String;

.field private buildId:Ljava/lang/String;

.field private maxApiVersion:I

.field private minApiVersion:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public appVersion(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->appVersion:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/athome/common/VersionInfo;
    .locals 6

    new-instance v0, Lcom/google/android/youtube/athome/common/VersionInfo;

    iget v1, p0, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->minApiVersion:I

    iget v2, p0, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->maxApiVersion:I

    iget-object v3, p0, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->buildId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->appVersion:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/athome/common/VersionInfo;-><init>(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/athome/common/VersionInfo$1;)V

    return-object v0
.end method

.method public buildId(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->buildId:Ljava/lang/String;

    return-object p0
.end method

.method public maxApiVersion(I)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->maxApiVersion:I

    return-object p0
.end method

.method public minApiVersion(I)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->minApiVersion:I

    return-object p0
.end method
