.class public Lcom/google/android/youtube/athome/common/SafeRpcData;
.super Ljava/lang/Object;
.source "SafeRpcData.java"


# instance fields
.field private final data:Landroid/support/place/rpc/RpcData;


# direct methods
.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 0
    .param p1    # Landroid/support/place/rpc/RpcData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/athome/common/SafeRpcData;->data:Landroid/support/place/rpc/RpcData;

    return-void
.end method


# virtual methods
.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/athome/common/SafeRpcData;->data:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v1, p1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/support/place/rpc/RpcData$KeyNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    :goto_0
    return p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/support/place/rpc/Flattenable;",
            ">(",
            "Ljava/lang/String;",
            "Landroid/support/place/rpc/Flattenable$Creator",
            "<TT;>;)TT;"
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/athome/common/SafeRpcData;->data:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v1, p1, p2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;
    :try_end_0
    .catch Landroid/support/place/rpc/RpcData$KeyNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInteger(Ljava/lang/String;I)I
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/athome/common/SafeRpcData;->data:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v1, p1}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/support/place/rpc/RpcData$KeyNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    :goto_0
    return p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getList(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/athome/common/SafeRpcData;->data:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v1, p1}, Landroid/support/place/rpc/RpcData;->getList(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/support/place/rpc/RpcData$KeyNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    :goto_0
    return-object p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/athome/common/SafeRpcData;->data:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v1, p1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/support/place/rpc/RpcData$KeyNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method
