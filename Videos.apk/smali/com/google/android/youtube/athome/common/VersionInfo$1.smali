.class final Lcom/google/android/youtube/athome/common/VersionInfo$1;
.super Lcom/google/android/youtube/athome/common/SafeFlattenableCreator;
.source "VersionInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/athome/common/VersionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/athome/common/SafeFlattenableCreator",
        "<",
        "Lcom/google/android/youtube/athome/common/VersionInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/athome/common/SafeFlattenableCreator;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createFromSafeRpcData(Lcom/google/android/youtube/athome/common/SafeRpcData;)Landroid/support/place/rpc/Flattenable;
    .locals 1
    .param p1    # Lcom/google/android/youtube/athome/common/SafeRpcData;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/athome/common/VersionInfo$1;->createFromSafeRpcData(Lcom/google/android/youtube/athome/common/SafeRpcData;)Lcom/google/android/youtube/athome/common/VersionInfo;

    move-result-object v0

    return-object v0
.end method

.method protected createFromSafeRpcData(Lcom/google/android/youtube/athome/common/SafeRpcData;)Lcom/google/android/youtube/athome/common/VersionInfo;
    .locals 3
    .param p1    # Lcom/google/android/youtube/athome/common/SafeRpcData;

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;-><init>()V

    const-string v1, "minApiVersion"

    invoke-virtual {p1, v1, v2}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getInteger(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->minApiVersion(I)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    move-result-object v0

    const-string v1, "maxApiVersion"

    invoke-virtual {p1, v1, v2}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getInteger(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->maxApiVersion(I)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    move-result-object v0

    const-string v1, "buildId"

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->buildId(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    move-result-object v0

    const-string v1, "appVersion"

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->appVersion(Ljava/lang/String;)Lcom/google/android/youtube/athome/common/VersionInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/common/VersionInfo$Builder;->build()Lcom/google/android/youtube/athome/common/VersionInfo;

    move-result-object v0

    return-object v0
.end method
