.class public Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
.super Ljava/lang/Object;
.source "AtHomeVideoInfo.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/support/place/rpc/Flattenable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$Builder;
    }
.end annotation


# static fields
.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/place/rpc/Flattenable$Creator",
            "<",
            "Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final currentPositionMillis:I

.field public final durationMillis:I

.field public final title:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$1;

    invoke-direct {v0}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$1;-><init>()V

    sput-object v0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->videoId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->title:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->currentPositionMillis:I

    iput p4, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->durationMillis:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/youtube/athome/common/AtHomeVideoInfo$1;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v1

    array-length v2, v1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2
    .param p1    # Landroid/support/place/rpc/RpcData;

    const-string v0, "video_id"

    iget-object v1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "title"

    iget-object v1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "currentPosition"

    iget v1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->currentPositionMillis:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "duration"

    iget v1, p0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->durationMillis:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    return-void
.end method
