.class public abstract Lcom/google/android/youtube/athome/common/SafeFlattenableCreator;
.super Ljava/lang/Object;
.source "SafeFlattenableCreator.java"

# interfaces
.implements Landroid/support/place/rpc/Flattenable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/support/place/rpc/Flattenable;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/support/place/rpc/Flattenable$Creator",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromRpcData(Landroid/support/place/rpc/RpcData;)Landroid/support/place/rpc/Flattenable;
    .locals 1
    .param p1    # Landroid/support/place/rpc/RpcData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/place/rpc/RpcData;",
            ")TT;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/athome/common/SafeRpcData;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/athome/common/SafeRpcData;-><init>(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/athome/common/SafeFlattenableCreator;->createFromSafeRpcData(Lcom/google/android/youtube/athome/common/SafeRpcData;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    return-object v0
.end method

.method protected abstract createFromSafeRpcData(Lcom/google/android/youtube/athome/common/SafeRpcData;)Landroid/support/place/rpc/Flattenable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/athome/common/SafeRpcData;",
            ")TT;"
        }
    .end annotation
.end method
