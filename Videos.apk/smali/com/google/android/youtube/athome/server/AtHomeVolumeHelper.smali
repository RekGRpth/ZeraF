.class public Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;
.super Ljava/lang/Object;
.source "AtHomeVolumeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$1;,
        Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;,
        Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;
    }
.end annotation


# static fields
.field private static final setMasterMuteMethod:Ljava/lang/reflect/Method;


# instance fields
.field private final audioManager:Landroid/media/AudioManager;

.field private final context:Landroid/content/Context;

.field private final listener:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;

.field private final volumeBroadcastReceiver:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->getSetMasterMuteMethod()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->setMasterMuteMethod:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->context:Landroid/content/Context;

    const-string v0, "listener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->listener:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->audioManager:Landroid/media/AudioManager;

    new-instance v0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;-><init>(Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$1;)V

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->volumeBroadcastReceiver:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;)Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;
    .locals 1
    .param p0    # Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->listener:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;

    return-object v0
.end method

.method private static getSetMasterMuteMethod()Ljava/lang/reflect/Method;
    .locals 5

    :try_start_0
    const-class v0, Landroid/media/AudioManager;

    const-string v1, "setMasterMute"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "Unable to find setMasterMute() method"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setMasterMute(Z)V
    .locals 6
    .param p1    # Z

    sget-object v1, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->setMasterMuteMethod:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    :try_start_0
    sget-object v1, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->setMasterMuteMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Bad method invocation when calling setMasterMute()"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public getVolume()I
    .locals 4

    const/high16 v3, -0x80000000

    iget-object v2, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    mul-int/lit8 v2, v1, 0x64

    div-int/2addr v2, v0

    return v2
.end method

.method public onStart()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->volumeBroadcastReceiver:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;->register()V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->volumeBroadcastReceiver:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;->unregister()V

    return-void
.end method

.method public setVolume(I)V
    .locals 5
    .param p1    # I

    const/high16 v4, -0x80000000

    iget-object v2, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    mul-int v2, p1, v0

    div-int/lit8 v1, v2, 0x64

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->setMasterMute(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v4, v1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method
