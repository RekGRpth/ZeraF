.class public abstract Lcom/google/android/youtube/athome/server/BinderActivity;
.super Landroid/app/Activity;
.source "BinderActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/youtube/athome/server/ActivityBinder;",
        ">",
        "Landroid/app/Activity;"
    }
.end annotation


# instance fields
.field private handler:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/athome/server/ActivityBinderHandler",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract onBind()Lcom/google/android/youtube/athome/server/ActivityBinder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->handler:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "should called setHandler in onCreate()"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->handler:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/athome/server/ActivityBinderHandler;->setBinderActivity(Lcom/google/android/youtube/athome/server/BinderActivity;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->handler:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "should called setHandler in onCreate()"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->handler:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/athome/server/ActivityBinderHandler;->setBinderActivity(Lcom/google/android/youtube/athome/server/BinderActivity;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onUnbind()V
    .locals 0

    return-void
.end method

.method protected setHandler(Lcom/google/android/youtube/athome/server/ActivityBinderHandler;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/athome/server/ActivityBinderHandler",
            "<TT;>;)V"
        }
    .end annotation

    const-string v0, "handler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->handler:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    return-void
.end method
