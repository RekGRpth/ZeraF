.class public interface abstract Lcom/google/android/youtube/athome/server/ActivityBinderConnection;
.super Ljava/lang/Object;
.source "ActivityBinderConnection.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/youtube/athome/server/ActivityBinder;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onActivityConnected(Landroid/content/ComponentName;Lcom/google/android/youtube/athome/server/ActivityBinder;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ComponentName;",
            "TT;)V"
        }
    .end annotation
.end method

.method public abstract onActivityDisconnected(Landroid/content/ComponentName;Lcom/google/android/youtube/athome/server/ActivityBinder;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ComponentName;",
            "TT;)V"
        }
    .end annotation
.end method
