.class Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AtHomeVolumeHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumeBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;->this$0:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;
    .param p2    # Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;-><init>(Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;->this$0:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    # getter for: Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->listener:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->access$200(Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;)Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;->this$0:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->getVolume()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$Listener;->onVolumeChanged(I)V

    return-void
.end method

.method public register()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;->this$0:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    # getter for: Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->access$100(Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.media.MASTER_VOLUME_CHANGED_ACTION"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public unregister()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper$VolumeBroadcastReceiver;->this$0:Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;

    # getter for: Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;->access$100(Lcom/google/android/youtube/athome/server/AtHomeVolumeHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
