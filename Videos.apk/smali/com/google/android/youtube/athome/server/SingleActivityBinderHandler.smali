.class public Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;
.super Ljava/lang/Object;
.source "SingleActivityBinderHandler.java"

# interfaces
.implements Lcom/google/android/youtube/athome/server/ActivityBinderHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/youtube/athome/server/ActivityBinder;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/athome/server/ActivityBinderHandler",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private binder:Lcom/google/android/youtube/athome/server/ActivityBinder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private binderActivity:Lcom/google/android/youtube/athome/server/BinderActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/athome/server/BinderActivity",
            "<TT;>;"
        }
    .end annotation
.end field

.field private componentName:Landroid/content/ComponentName;

.field private connection:Lcom/google/android/youtube/athome/server/ActivityBinderConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/athome/server/ActivityBinderConnection",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public bindActivity(Landroid/content/Intent;Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Lcom/google/android/youtube/athome/server/ActivityBinderConnection",
            "<TT;>;)V"
        }
    .end annotation

    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->componentName:Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->bindCurrentActivity(Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V

    return-void
.end method

.method public bindCurrentActivity(Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/athome/server/ActivityBinderConnection",
            "<TT;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->connection:Lcom/google/android/youtube/athome/server/ActivityBinderConnection;

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binderActivity:Lcom/google/android/youtube/athome/server/BinderActivity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binder:Lcom/google/android/youtube/athome/server/ActivityBinder;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binderActivity:Lcom/google/android/youtube/athome/server/BinderActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/server/BinderActivity;->onBind()Lcom/google/android/youtube/athome/server/ActivityBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binder:Lcom/google/android/youtube/athome/server/ActivityBinder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->componentName:Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binder:Lcom/google/android/youtube/athome/server/ActivityBinder;

    invoke-interface {p1, v0, v1}, Lcom/google/android/youtube/athome/server/ActivityBinderConnection;->onActivityConnected(Landroid/content/ComponentName;Lcom/google/android/youtube/athome/server/ActivityBinder;)V

    :cond_1
    return-void
.end method

.method public setBinderActivity(Lcom/google/android/youtube/athome/server/BinderActivity;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/athome/server/BinderActivity",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binderActivity:Lcom/google/android/youtube/athome/server/BinderActivity;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binderActivity:Lcom/google/android/youtube/athome/server/BinderActivity;

    if-nez v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    const-string v1, "should reset the binder actvitity to null first; did you forget setBinderActivity(null)  in onPause()?"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->connection:Lcom/google/android/youtube/athome/server/ActivityBinderConnection;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binderActivity:Lcom/google/android/youtube/athome/server/BinderActivity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->connection:Lcom/google/android/youtube/athome/server/ActivityBinderConnection;

    iget-object v1, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->componentName:Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binder:Lcom/google/android/youtube/athome/server/ActivityBinder;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/athome/server/ActivityBinderConnection;->onActivityDisconnected(Landroid/content/ComponentName;Lcom/google/android/youtube/athome/server/ActivityBinder;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binder:Lcom/google/android/youtube/athome/server/ActivityBinder;

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/athome/server/BinderActivity;->onBind()Lcom/google/android/youtube/athome/server/ActivityBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binder:Lcom/google/android/youtube/athome/server/ActivityBinder;

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->connection:Lcom/google/android/youtube/athome/server/ActivityBinderConnection;

    iget-object v1, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->componentName:Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binder:Lcom/google/android/youtube/athome/server/ActivityBinder;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/athome/server/ActivityBinderConnection;->onActivityConnected(Landroid/content/ComponentName;Lcom/google/android/youtube/athome/server/ActivityBinder;)V

    :cond_3
    iput-object p1, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binderActivity:Lcom/google/android/youtube/athome/server/BinderActivity;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public unbindActivity(Lcom/google/android/youtube/athome/server/ActivityBinderConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/athome/server/ActivityBinderConnection",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->connection:Lcom/google/android/youtube/athome/server/ActivityBinderConnection;

    if-eq v0, p1, :cond_0

    const-string v0, "Trying to unbind connection that is not bound."

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binderActivity:Lcom/google/android/youtube/athome/server/BinderActivity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->binderActivity:Lcom/google/android/youtube/athome/server/BinderActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/server/BinderActivity;->onUnbind()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;->connection:Lcom/google/android/youtube/athome/server/ActivityBinderConnection;

    goto :goto_0
.end method
