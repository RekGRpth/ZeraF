.class public Lcom/google/android/youtube/athome/server/IdleTimeout;
.super Lcom/google/android/youtube/athome/common/Timeout;
.source "IdleTimeout.java"


# instance fields
.field private final activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;J)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/os/Handler;
    .param p3    # J

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/youtube/athome/common/Timeout;-><init>(Landroid/os/Handler;J)V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/athome/server/IdleTimeout;->activity:Landroid/app/Activity;

    return-void
.end method


# virtual methods
.method public onBusy()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/athome/server/IdleTimeout;->reset()V

    return-void
.end method

.method public onIdle()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/athome/server/IdleTimeout;->start()V

    return-void
.end method

.method protected onTimeout()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/IdleTimeout;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method
