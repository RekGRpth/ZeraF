.class public interface abstract Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;
.super Ljava/lang/Object;
.source "SettingsActivityCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SettingsContainer"
.end annotation


# virtual methods
.method public abstract addPreferencesFromResource(I)V
.end method

.method public abstract findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;
.end method

.method public abstract getPreferenceManager()Landroid/preference/PreferenceManager;
.end method

.method public abstract getPreferenceScreen()Landroid/preference/PreferenceScreen;
.end method
