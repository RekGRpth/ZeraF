.class Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$3;
.super Ljava/lang/Object;
.source "WatchDetailsActivity.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$3;->this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Ljava/lang/Exception;

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$3;->onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$3;->this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    # invokes: Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onDownloadStatusCursor(Landroid/database/Cursor;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->access$600(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$3;->onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
