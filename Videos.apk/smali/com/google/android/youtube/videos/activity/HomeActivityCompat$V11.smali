.class Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;
.super Lcom/google/android/youtube/videos/activity/HomeActivityCompat;
.source "HomeActivityCompat.java"

# interfaces
.implements Landroid/app/ActionBar$OnNavigationListener;
.implements Landroid/app/ActionBar$TabListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/HomeActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V11"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11$TabAdapter;
    }
.end annotation


# instance fields
.field private final actionBar:Landroid/app/ActionBar;

.field private final activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

.field private final isTabletOrTv:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/VideosApplication;Lcom/google/android/youtube/videos/activity/HomeActivity;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/VideosApplication;
    .param p2    # Lcom/google/android/youtube/videos/activity/HomeActivity;

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const v3, 0x7f0a0007

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/VideosApplication;->isTablet()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    iput-boolean v1, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->isTabletOrTv:Z

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V
    .locals 1
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;
    .param p3    # Z

    if-eqz p3, :cond_0

    const v0, 0x7f0f0005

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :cond_0
    return-void
.end method

.method public onNavigationItemSelected(IJ)Z
    .locals 1
    .param p1    # I
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->selectPage(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getNavigationMode()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getNavigationItemCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_0
    return-void
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->selectPage(I)V

    return-void
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    return-void
.end method

.method public onTabsChanged(Lvedroid/support/v4/app/FragmentPagerAdapter;I)V
    .locals 9
    .param p1    # Lvedroid/support/v4/app/FragmentPagerAdapter;
    .param p2    # I

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v6}, Landroid/app/ActionBar;->removeAllTabs()V

    invoke-virtual {p1}, Lvedroid/support/v4/app/FragmentPagerAdapter;->getCount()I

    move-result v6

    if-le v6, v4, :cond_7

    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v4, :cond_1

    move v1, v4

    :goto_0
    iget-boolean v6, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->isTabletOrTv:Z

    if-eqz v6, :cond_3

    iget-object v7, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    sget v6, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v8, 0x10

    if-lt v6, v8, :cond_2

    move v6, v4

    :goto_1
    invoke-virtual {v7, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    :goto_2
    iget-boolean v6, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->isTabletOrTv:Z

    if-nez v6, :cond_0

    if-eqz v1, :cond_4

    :cond_0
    move v3, v4

    :goto_3
    if-eqz v3, :cond_5

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Landroid/app/ActionBar;->setNavigationMode(I)V

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {p1}, Lvedroid/support/v4/app/FragmentPagerAdapter;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_6

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v4}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v4

    invoke-virtual {p1, v0}, Lvedroid/support/v4/app/FragmentPagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v4, v2, v5}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_1
    move v1, v5

    goto :goto_0

    :cond_2
    move v6, v5

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v6, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_2

    :cond_4
    move v3, v5

    goto :goto_3

    :cond_5
    iget-object v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v5, v4}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    new-instance v5, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11$TabAdapter;

    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    invoke-direct {v5, v6, p1}, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11$TabAdapter;-><init>(Landroid/view/LayoutInflater;Lvedroid/support/v4/app/FragmentPagerAdapter;)V

    invoke-virtual {v4, v5, p0}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    :cond_6
    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v4, p2}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :goto_5
    return-void

    :cond_7
    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v6, v5}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v5, p0, Lcom/google/android/youtube/videos/activity/HomeActivityCompat$V11;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v5, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_5
.end method
