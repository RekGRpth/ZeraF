.class Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;
.super Lcom/google/android/youtube/videos/activity/WatchActivityCompat;
.source "WatchActivityCompat.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/WatchActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V8"
.end annotation


# instance fields
.field private actionBar:Landroid/view/View;

.field private actionBarTitleView:Landroid/widget/TextView;

.field private final activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

.field private final lockedOrientation:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/activity/WatchActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;-><init>(Lcom/google/android/youtube/videos/activity/WatchActivity;I)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/youtube/videos/activity/WatchActivity;I)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/activity/WatchActivity;
    .param p2    # I

    const/16 v1, 0x400

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    iput p2, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->lockedOrientation:I

    return-void
.end method

.method private updateOrientation()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->isConnectedToAtHomeScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->setRequestedOrientation(I)V

    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->lockedOrientation:I

    goto :goto_0
.end method


# virtual methods
.method public onAtHomeConnectorChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->updateOrientation()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->onUpPressed()V

    return-void
.end method

.method public onCreate()V
    .locals 9

    const/4 v8, 0x0

    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    const v7, 0x7f070018

    invoke-virtual {v6, v7}, Lcom/google/android/youtube/videos/activity/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->actionBar:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->actionBar:Landroid/view/View;

    const v7, 0x7f070019

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->actionBarTitleView:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->actionBar:Landroid/view/View;

    const v7, 0x7f070010

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->updateOrientation()V

    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    const v7, 0x7f070080

    invoke-virtual {v6, v7}, Lcom/google/android/youtube/videos/activity/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    iget-object v6, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090018

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v5}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {v5, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    iget-boolean v6, v4, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->isInsetView:Z

    if-eqz v6, :cond_0

    invoke-virtual {v3, v8, v0, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onUserInteractionExpected()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->actionBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onUserInteractionNotExpected()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->actionBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onVideoTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V8;->actionBarTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
