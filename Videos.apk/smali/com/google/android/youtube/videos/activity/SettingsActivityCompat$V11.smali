.class Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$V11;
.super Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;
.source "SettingsActivityCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V11"
.end annotation


# instance fields
.field private final activity:Lcom/google/android/youtube/videos/activity/SettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/SettingsActivity;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method

.method public static setIntentExtras(Landroid/content/Intent;)V
    .locals 3

    const-string v0, ":android:no_headers"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/youtube/videos/activity/GeneralPrefsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
