.class public Lcom/google/android/youtube/videos/activity/WatchActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "WatchActivity.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/activity/WatchActivity$1;,
        Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/app/FragmentActivity;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private application:Lcom/google/android/youtube/videos/VideosApplication;

.field private atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

.field private atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

.field private config:Lcom/google/android/youtube/videos/Config;

.field private director:Lcom/google/android/youtube/videos/player/Director;

.field private eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private hasBeenStopped:Z

.field private keyguardManager:Landroid/app/KeyguardManager;

.field private knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

.field private mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

.field private moviesConnectorListener:Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;

.field private preferences:Landroid/content/SharedPreferences;

.field private resumed:Z

.field private seasonId:Ljava/lang/String;

.field private videoId:Ljava/lang/String;

.field private videoStats2ClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

.field private watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/activity/WatchActivity;)Lcom/google/android/youtube/videos/activity/WatchActivityCompat;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/activity/WatchActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchActivity;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->resumed:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/activity/WatchActivity;)Landroid/app/KeyguardManager;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->keyguardManager:Landroid/app/KeyguardManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/activity/WatchActivity;)Lcom/google/android/youtube/videos/player/Director;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/activity/WatchActivity;ZZ)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchActivity;
    .param p1    # Z
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchActivity;->initDirector(ZZ)V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video_id"

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private initDirector(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->isConnectedToAtHomeScreen()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/player/Director;->initAtHomePlayback(Z)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;->initDirectorForLocalPlayback(Lcom/google/android/youtube/videos/player/Director;ZZ)V

    goto :goto_1
.end method


# virtual methods
.method isConnectedToAtHomeScreen()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->canControlPlayback()Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lvedroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/Director;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onBackPressed()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 29
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v28

    const-string v1, "authAccount"

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->account:Ljava/lang/String;

    const-string v1, "video_id"

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    const-string v1, "season_id"

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->seasonId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->account:Ljava/lang/String;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid arguments format: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "videoId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/youtube/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    const-string v1, "keyguard"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->keyguardManager:Landroid/app/KeyguardManager;

    new-instance v1, Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;-><init>(Lcom/google/android/youtube/videos/activity/WatchActivity;Lcom/google/android/youtube/videos/activity/WatchActivity$1;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->moviesConnectorListener:Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;->create(Lcom/google/android/youtube/videos/activity/WatchActivity;)Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    const v1, 0x7f04003a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/activity/WatchActivity;->setContentView(I)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderCompat;->create(Landroid/app/Activity;)Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/VideosApplication;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getKnowledgeClient()Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    const v1, 0x7f070080

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/activity/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getAtHomeInstance()Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    new-instance v1, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/Config;->atHomeVolumeStepPercent()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2, v4}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;I)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getVideoStats2ClientFactory()Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->videoStats2ClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    new-instance v1, Lcom/google/android/youtube/videos/player/Director;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->preferences:Landroid/content/SharedPreferences;

    invoke-interface/range {v27 .. v27}, Lcom/google/android/youtube/videos/api/ApiRequesters;->getVideoGetRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->videoStats2ClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStore()Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStoreSync()Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getDrmManager()Lcom/google/android/youtube/videos/drm/DrmManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getStreamingStatusNotifier()Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getStreamsSelector()Lcom/google/android/youtube/videos/StreamsSelector;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->legacyDownloadsHaveAppLevelDrm()Z

    move-result v20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/Config;->maxResumeTimeValidityMillis()J

    move-result-wide v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->seasonId:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->account:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v26, v0

    move-object/from16 v4, p0

    invoke-direct/range {v1 .. v26}, Lcom/google/android/youtube/videos/player/Director;-><init>(Lcom/google/android/youtube/videos/VideosApplication;Lcom/google/android/youtube/core/player/PlayerView;Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/Requesters;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;Lcom/google/android/youtube/videos/player/Director$Listener;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/StreamsSelector;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/videos/tagging/KnowledgeClient;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;->onCreate()V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1, p1, v0, p0}, Lcom/google/android/youtube/videos/VideosApplication;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->isFinishing()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/player/Director;->release(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;->destroy()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;->onDestroy()V

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onDestroy()V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchActivity;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/player/Director;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/player/Director;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    invoke-static {p1, p0, v0}, Lcom/google/android/youtube/videos/VideosApplication;->onCommonOptionsItemSelected(Landroid/view/MenuItem;Landroid/app/Activity;Lcom/google/android/youtube/videos/logging/EventLogger;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->resumed:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->resumed:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->hasBeenStopped:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/Director;->reset()V

    :cond_0
    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onPause()V

    return-void
.end method

.method onPresentationDisplayRouteChanged()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->resumed:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->isConnectedToAtHomeScreen()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/Director;->reset()V

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/activity/WatchActivity;->initDirector(ZZ)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchActivity;->onResponse(Ljava/lang/String;Ljava/lang/Void;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Ljava/lang/Void;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Void;

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->hasBeenStopped:Z

    return-void
.end method

.method protected onResume()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "account does not exist: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->seasonId:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPremiumWatchPageOpened(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->hasBeenStopped:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-direct {p0, v0, v2}, Lcom/google/android/youtube/videos/activity/WatchActivity;->initDirector(ZZ)V

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->resumed:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->register()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->moviesConnectorListener:Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->startListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->knowledgeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getWishlistStoreSync()Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->syncWishlist(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->moviesConnectorListener:Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->stopListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;->onStop()V

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onStop()V

    return-void
.end method

.method onUpPressed()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->finish()V

    return-void
.end method

.method public showControls()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/Director;->showControls()V

    return-void
.end method
