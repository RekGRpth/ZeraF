.class interface abstract Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;
.super Ljava/lang/Object;
.source "WatchDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "DownloadStatusQuery"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "pinned"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "pinning_status"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "pinning_status_reason"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "pinning_download_size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "download_bytes_downloaded"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "pinning_drm_error_code"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    return-void
.end method
