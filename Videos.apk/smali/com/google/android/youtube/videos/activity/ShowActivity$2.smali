.class Lcom/google/android/youtube/videos/activity/ShowActivity$2;
.super Ljava/lang/Object;
.source "ShowActivity.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/activity/ShowActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/activity/ShowActivity;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/activity/ShowActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/youtube/videos/activity/ShowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private processResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/youtube/videos/activity/ShowActivity;

    # getter for: Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->access$200(Lcom/google/android/youtube/videos/activity/ShowActivity;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/youtube/videos/activity/ShowActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/youtube/videos/activity/ShowActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->access$202(Lcom/google/android/youtube/videos/activity/ShowActivity;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/youtube/videos/activity/ShowActivity;

    # invokes: Lcom/google/android/youtube/videos/activity/ShowActivity;->onPurchaseCursor(Landroid/database/Cursor;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/activity/ShowActivity;->access$300(Lcom/google/android/youtube/videos/activity/ShowActivity;Landroid/database/Cursor;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/youtube/videos/activity/ShowActivity;

    # getter for: Lcom/google/android/youtube/videos/activity/ShowActivity;->pendingPurchaseRequest:Z
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->access$400(Lcom/google/android/youtube/videos/activity/ShowActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/youtube/videos/activity/ShowActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/youtube/videos/activity/ShowActivity;->pendingPurchaseRequest:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->access$402(Lcom/google/android/youtube/videos/activity/ShowActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->this$0:Lcom/google/android/youtube/videos/activity/ShowActivity;

    # invokes: Lcom/google/android/youtube/videos/activity/ShowActivity;->refreshPurchase()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/ShowActivity;->access$500(Lcom/google/android/youtube/videos/activity/ShowActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Ljava/lang/Exception;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->processResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Landroid/database/Cursor;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->processResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/ShowActivity$2;->onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
