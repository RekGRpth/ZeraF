.class public Lcom/google/android/youtube/videos/activity/ShowsFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "ShowsFragment.java"


# instance fields
.field private showsHelper:Lcom/google/android/youtube/videos/ui/ShowsHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 20
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lvedroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/ShowsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getApplication()Landroid/app/Application;

    move-result-object v19

    check-cast v19, Lcom/google/android/youtube/videos/VideosApplication;

    new-instance v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/ShowsFragment;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getShowsCursorHelper()Lcom/google/android/youtube/videos/ui/CursorHelper;

    move-result-object v6

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v7

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getPosterStore()Lcom/google/android/youtube/videos/store/PosterStore;

    move-result-object v8

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getDrmManager()Lcom/google/android/youtube/videos/drm/DrmManager;

    move-result-object v9

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getErrorHelper()Lcom/google/android/youtube/core/ErrorHelper;

    move-result-object v10

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getSyncHelper()Lcom/google/android/youtube/videos/ui/SyncHelper;

    move-result-object v11

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Lcom/google/android/youtube/videos/activity/HomeActivity;->areVerticalsEnabled(I)Z

    move-result v12

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/activity/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v13, 0x7f0d0009

    invoke-virtual {v5, v13}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v14

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;

    move-result-object v15

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getRecommendationsRequestFactory()Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    move-result-object v16

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/videos/Config;->useGDataShowSuggestions()Z

    move-result v17

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v18

    move-object v5, v3

    invoke-direct/range {v1 .. v18}, Lcom/google/android/youtube/videos/ui/ShowsHelper;-><init>(Lcom/google/android/youtube/videos/Config;Lvedroid/support/v4/app/FragmentActivity;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/core/ErrorHelper;Lcom/google/android/youtube/videos/ui/SyncHelper;ZILcom/google/android/youtube/videos/Requesters;Lcom/google/android/youtube/videos/api/ApiRequesters;Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;ZLcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/activity/ShowsFragment;->showsHelper:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowsFragment;->showsHelper:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v0, 0x7f040033

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowsFragment;->showsHelper:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/ShowsFragment;->showsHelper:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->onStop()V

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onStop()V

    return-void
.end method
