.class public Lcom/google/android/youtube/videos/activity/GeneralPrefsFragment;
.super Landroid/preference/PreferenceFragment;
.source "GeneralPrefsFragment.java"

# interfaces
.implements Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->configureSettings(Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;)V

    return-void
.end method
