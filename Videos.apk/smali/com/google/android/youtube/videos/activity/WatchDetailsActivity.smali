.class public Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "WatchDetailsActivity.java"

# interfaces
.implements Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;
.implements Lcom/google/android/youtube/videos/store/Database$Listener;
.implements Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;
.implements Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;,
        Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private allowDownloads:Z

.field private application:Lcom/google/android/youtube/videos/VideosApplication;

.field private atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

.field private atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

.field private currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

.field private database:Lcom/google/android/youtube/videos/store/Database;

.field private downloadStatusCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

.field private haveVideo:Z

.field private initializedSuggestions:Z

.field private mainLayout:Landroid/view/View;

.field private mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

.field private pendingPurchaseRequest:Z

.field private pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

.field private pinned:Ljava/lang/Boolean;

.field private pinningStatus:Ljava/lang/Integer;

.field private pinningStatusReason:Ljava/lang/Integer;

.field private playPanelHelper:Lcom/google/android/youtube/videos/ui/PlayPanelHelper;

.field private posterCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

.field private purchaseCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private purchaseSyncCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private startDownload:Z

.field private started:Z

.field private statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

.field private suggestionsHelper:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

.field private syncCompleted:Z

.field private syncException:Ljava/lang/Exception;

.field private title:Ljava/lang/String;

.field private videoId:Ljava/lang/String;

.field private watchDetailsActivityCompat:Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;

.field private watchInfoHelper:Lcom/google/android/youtube/videos/ui/WatchInfoHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onSyncSuccess()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onSyncError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onPurchaseCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pendingPurchaseRequest:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pendingPurchaseRequest:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->refreshPurchase()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onDownloadStatusCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)Lcom/google/android/youtube/videos/ui/PlayPanelHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->playPanelHelper:Lcom/google/android/youtube/videos/ui/PlayPanelHelper;

    return-object v0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video_id"

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-static {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "start_download"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method private onDownloadStatusCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->playPanelHelper:Lcom/google/android/youtube/videos/ui/PlayPanelHelper;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->readDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->onVideoDownloadStatus(Lcom/google/android/youtube/videos/store/VideoDownloadStatus;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private onPurchaseCursor(Landroid/database/Cursor;)V
    .locals 18
    .param p1    # Landroid/database/Cursor;

    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    sget v4, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->TITLE:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->title:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/videos/Config;->surroundSoundFormats()Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoHasStreamFormats(Landroid/database/Cursor;Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoHasPurchased(Landroid/database/Cursor;Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v16, 0x1

    :goto_0
    new-instance v3, Lcom/google/android/youtube/videos/store/VideoMetadata;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->title:Ljava/lang/String;

    sget v6, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->DESCRIPTION:I

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->RELEASE_YEAR:I

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x0

    :goto_1
    sget v8, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->VIDEO_DURATION_SECONDS:I

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    sget v9, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->RELATED_URI:I

    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->readUri(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v9

    sget v10, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->DIRECTORS:I

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/android/youtube/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v10

    sget v11, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->WRITERS:I

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/google/android/youtube/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v11

    sget v12, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->ACTORS:I

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lcom/google/android/youtube/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v12

    sget v13, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->PRODUCERS:I

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/google/android/youtube/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v13

    sget v14, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->RATINGS:I

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/android/youtube/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v15}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v15

    invoke-interface {v15}, Lcom/google/android/youtube/videos/Config;->hdStreamingFormats()Ljava/util/List;

    move-result-object v15

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoHasPurchased(Landroid/database/Cursor;Ljava/util/List;)Z

    move-result v15

    invoke-direct/range {v3 .. v16}, Lcom/google/android/youtube/videos/store/VideoMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZ)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->watchInfoHelper:Lcom/google/android/youtube/videos/ui/WatchInfoHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->showVideo(Ljava/lang/String;Lcom/google/android/youtube/videos/store/VideoMetadata;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->playPanelHelper:Lcom/google/android/youtube/videos/ui/PlayPanelHelper;

    invoke-direct/range {p0 .. p1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->readDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->onVideoDownloadStatus(Lcom/google/android/youtube/videos/store/VideoDownloadStatus;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->playPanelHelper:Lcom/google/android/youtube/videos/ui/PlayPanelHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->setTitle(Ljava/lang/String;)V

    iget-object v4, v3, Lcom/google/android/youtube/videos/store/VideoMetadata;->relatedUri:Landroid/net/Uri;

    if-nez v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->initEmpty()V

    :cond_0
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->startDownload:Z

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/youtube/videos/ui/PinHelper;->pinVideo(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->startDownload:Z

    :cond_1
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->haveVideo:Z

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->updateVisibilities()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    return-void

    :cond_3
    const/16 v16, 0x0

    goto/16 :goto_0

    :cond_4
    :try_start_1
    sget v7, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->RELEASE_YEAR:I

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->initializedSuggestions:Z

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    iget-object v6, v3, Lcom/google/android/youtube/videos/store/VideoMetadata;->relatedUri:Landroid/net/Uri;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->init(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->initializedSuggestions:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v4

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    throw v4
.end method

.method private onSyncError(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->syncCompleted:Z

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->syncException:Ljava/lang/Exception;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->updateVisibilities()V

    return-void
.end method

.method private onSyncSuccess()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->syncCompleted:Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->updateVisibilities()V

    return-void
.end method

.method private readDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/store/VideoDownloadStatus;
    .locals 7
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinningStatus:Ljava/lang/Integer;

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinningStatusReason:Ljava/lang/Integer;

    new-instance v0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinningStatus:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinningStatusReason:Ljava/lang/Integer;

    const/4 v4, 0x5

    invoke-static {p1, v4}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x3

    invoke-static {p1, v5}, Lcom/google/android/youtube/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x4

    invoke-static {p1, v6}, Lcom/google/android/youtube/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;-><init>(ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)V

    return-object v0
.end method

.method private refreshDownloadStatus()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->downloadStatusCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method private refreshPurchase()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pendingPurchaseRequest:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method private startSync()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->syncCompleted:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->syncException:Ljava/lang/Exception;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->updateVisibilities()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, p0, p0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getUserAuth(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    return-void
.end method

.method private updateVisibilities()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->haveVideo:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->mainLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->mainLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->syncCompleted:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->syncException:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->syncException:Ljava/lang/Exception;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ErrorHelper;->humanize(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    const v1, 0x7f0a0085

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setLoading()V

    goto :goto_0
.end method

.method private videoHasPurchased(Landroid/database/Cursor;Ljava/util/List;)Z
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    sget v1, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->FORMATS:I

    invoke-static {p1, v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntegerList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->retainAll(Ljava/util/Collection;)Z

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private videoHasStreamFormats(Landroid/database/Cursor;Ljava/util/List;)Z
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    sget v1, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->STREAM_FORMATS:I

    invoke-static {p1, v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntegerList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->retainAll(Ljava/util/Collection;)Z

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onAuthenticated(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseSyncCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Landroid/util/Pair;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onSyncError(Ljava/lang/Exception;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 20
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v19

    const-string v2, "authAccount"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    const-string v2, "start_download"

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->startDownload:Z

    const-string v2, "video_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    if-nez v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid arguments format: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    const v2, 0x7f04003c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->setContentView(I)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;->create(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->watchDetailsActivityCompat:Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderCompat;->create(Landroid/app/Activity;)Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    const v2, 0x7f070082

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->mainLayout:Landroid/view/View;

    const v2, 0x7f070081

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v2, v1}, Lcom/google/android/youtube/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/StatusHelper;->init()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/VideosApplication;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->database:Lcom/google/android/youtube/videos/store/Database;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStoreSync()Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStore()Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getPosterStore()Lcom/google/android/youtube/videos/store/PosterStore;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getErrorHelper()Lcom/google/android/youtube/core/ErrorHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->errorHelper:Lcom/google/android/youtube/core/ErrorHelper;

    new-instance v2, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;

    const v3, 0x1020002

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getGmsPlusOneClient()Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    const v8, 0x7f070083

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->ratingScheme()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v10

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;-><init>(Lvedroid/support/v4/app/FragmentActivity;Landroid/view/View;Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Lcom/google/android/youtube/core/utils/NetworkStatus;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->watchInfoHelper:Lcom/google/android/youtube/videos/ui/WatchInfoHelper;

    new-instance v2, Lcom/google/android/youtube/videos/ui/PinHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/youtube/videos/ui/PinHelper;-><init>(Lvedroid/support/v4/app/FragmentActivity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/NetworkStatus;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    new-instance v2, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    const v3, 0x7f070094

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/videos/api/ApiRequesters;->getRecommendationsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getRecommendationsRequestFactory()Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Requesters;->getPosterArtRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v7

    const v3, 0x7f070095

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const v9, 0x7f0a0076

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getErrorHelper()Lcom/google/android/youtube/core/ErrorHelper;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v13, 0x7f0d0005

    invoke-virtual {v3, v13}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v14, 0x7f0d0006

    invoke-virtual {v3, v14}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->useGDataRelatedMovieSuggestions()Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Requesters;->getSuggestedMoviesRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v16

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v16}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;-><init>(Landroid/app/Activity;Landroid/widget/LinearLayout;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;Lcom/google/android/youtube/core/async/Requester;Landroid/view/View;ILcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;Ljava/lang/String;IIZLcom/google/android/youtube/core/async/Requester;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->allowDownloads()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->allowDownloads:Z

    new-instance v2, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;

    const v3, 0x7f070091

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->allowDownloads:Z

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v3, v1, v4}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->playPanelHelper:Lcom/google/android/youtube/videos/ui/PlayPanelHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->setHiddenStateForMovie(Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance v2, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$1;-><init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseSyncCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v2, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$2;-><init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->purchaseCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v2, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$3;-><init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->downloadStatusCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v2, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$4;-><init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->posterCallback:Lcom/google/android/youtube/core/async/Callback;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->playPanelHelper:Lcom/google/android/youtube/videos/ui/PlayPanelHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02004d

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->onThumbnail(Landroid/graphics/Bitmap;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->posterCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/videos/store/PosterStore;->getVideoPoster(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getAtHomeInstance()Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v18

    new-instance v2, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-interface/range {v18 .. v18}, Lcom/google/android/youtube/videos/Config;->atHomeVolumeStepPercent()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    new-instance v17, Landroid/content/Intent;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v2, "start_download"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->setIntent(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1, p1, v0, p0}, Lcom/google/android/youtube/videos/VideosApplication;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1, p1, v0, p0}, Lcom/google/android/youtube/videos/VideosApplication;->createSearchMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V

    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->mediaRouteProvider:Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;->destroy()V

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onNotAuthenticated()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->finish()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->watchDetailsActivityCompat:Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v7

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v6

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-static {p1, p0, v6}, Lcom/google/android/youtube/videos/VideosApplication;->onCommonOptionsItemSelected(Landroid/view/MenuItem;Landroid/app/Activity;Lcom/google/android/youtube/videos/logging/EventLogger;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v7

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/ui/PinHelper;->pinVideo(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "download"

    invoke-interface {v6, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMenuItemSelected(Ljava/lang/String;)V

    move v0, v7

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->title:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinningStatusReason:Ljava/lang/Integer;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/PinHelper;->showPinningDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    const-string v0, "removeDownload"

    invoke-interface {v6, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMenuItemSelected(Ljava/lang/String;)V

    move v0, v7

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0700a5
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPinClick(Lcom/google/android/youtube/videos/store/VideoDownloadStatus;)V
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

    iget-boolean v0, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinned:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->title:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/PinHelper;->showPinningDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    iget-object v5, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->drmErrorCode:Ljava/lang/Integer;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/PinHelper;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/ui/PinHelper;->pinVideo(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPinClick()V

    goto :goto_0
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->refreshDownloadStatus()V

    :cond_0
    return-void
.end method

.method public onPlay()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->isAtHomeDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/google/android/youtube/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->posterCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/videos/store/PosterStore;->getVideoPoster(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    const v1, 0x7f0700a6

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->allowDownloads:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->allowDownloads:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v1, 0x7f0700a5

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->allowDownloads:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->allowDownloads:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pinned:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    move v3, v2

    :cond_0
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    return v2

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method public onPurchasesUpdated()V
    .locals 0

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onVideoDetailsPageOpened(Ljava/lang/String;)V

    return-void
.end method

.method public onRetry()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->startSync()V

    return-void
.end method

.method public onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "account does not exist: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->refreshPurchase()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->register()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->onStart()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->startSync()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->started:Z

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->started:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->started:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->atHomeHelper:Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeHelper;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->removeListener(Lcom/google/android/youtube/videos/store/Database$Listener;)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->onStop()V

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->initializedSuggestions:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->currentPurchaseRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->pendingPurchaseRequest:Z

    :cond_0
    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onStop()V

    return-void
.end method

.method onUpPressed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->account:Ljava/lang/String;

    const-string v1, "movies"

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->finish()V

    return-void
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->refreshPurchase()V

    :cond_0
    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    return-void
.end method
