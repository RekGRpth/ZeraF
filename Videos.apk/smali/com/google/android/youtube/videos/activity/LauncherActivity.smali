.class public Lcom/google/android/youtube/videos/activity/LauncherActivity;
.super Landroid/app/Activity;
.source "LauncherActivity.java"


# instance fields
.field private application:Lcom/google/android/youtube/videos/VideosApplication;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static createEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/activity/LauncherActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createEpisodeIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/activity/ShowActivity;->createEpisodeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createHomeIntent(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-class v1, Lcom/google/android/youtube/videos/activity/HomeActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createMovieDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/activity/LauncherActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createMovieIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createSeasonIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/ShowActivity;->createSeasonIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createShowIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/ShowActivity;->createShowIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createWatchEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "start_playback"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createWatchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createWatchMovieDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createMovieDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "start_playback"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getAccount(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Intent;

    const-string v3, "intent_extra_data_key"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_0
    const-string v3, "authAccount"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v3, "u"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method private static getBooleanQueryParameter(Landroid/net/Uri;Ljava/lang/String;Z)Z
    .locals 2
    .param p0    # Landroid/net/Uri;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return p2

    :cond_0
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->toLowerInvariant(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move p2, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private handleLegacyIntent(Lcom/google/android/youtube/videos/VideosApplication;Landroid/content/Intent;)V
    .locals 13
    .param p1    # Lcom/google/android/youtube/videos/VideosApplication;
    .param p2    # Landroid/content/Intent;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->getAccount(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    const-string v12, "intent_extra_data_key"

    invoke-virtual {p2, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const-string v12, "download_video_id"

    invoke-virtual {p2, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_7

    const/4 v5, 0x1

    :cond_0
    :goto_0
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_1

    const-string v12, "details_video_id"

    invoke-virtual {p2, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_1
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_3

    if-eqz v7, :cond_3

    const-string v12, "v"

    invoke-virtual {v7, v12}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    const-string v12, "start_playback"

    if-nez v1, :cond_2

    const/4 v10, 0x1

    :cond_2
    invoke-virtual {p2, v12, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    :cond_3
    if-nez v7, :cond_8

    move-object v4, v11

    :goto_1
    const-string v10, "season_id"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    if-eqz v7, :cond_4

    const-string v10, "season_id"

    invoke-virtual {v7, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string v10, "tvseason-"

    invoke-virtual {v2, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/16 v10, 0x9

    invoke-virtual {v2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    :cond_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9

    :cond_5
    const-string v8, "shows"

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/VideosApplication;->hasPremiumError()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-direct {p0, v0, v8}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startHome(Landroid/content/Intent;)V

    :goto_3
    return-void

    :cond_7
    const-string v12, "video_id"

    invoke-virtual {p2, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_0

    const-string v12, "start_playback"

    invoke-virtual {p2, v12, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    goto/16 :goto_0

    :cond_8
    const-string v10, "p"

    invoke-virtual {v7, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_9
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v8, "movies"

    goto :goto_2

    :cond_a
    move-object v8, v11

    goto :goto_2

    :cond_b
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v10

    invoke-virtual {v10, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_c

    invoke-direct {p0, v11, v8}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    :cond_c
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/VideosApplication;->getSignInManager()Lcom/google/android/youtube/videos/accounts/SignInManager;

    move-result-object v10

    invoke-virtual {v10, v0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    if-eqz v6, :cond_e

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_d

    invoke-direct {p0, v0, v9}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startMoviePlayback(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_d
    invoke-direct {p0, v0, v9, v3}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startEpisodePlayback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_e
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_10

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_f

    invoke-direct {p0, v0, v9, v5}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startMovieDetails(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_3

    :cond_f
    invoke-direct {p0, v0, v9, v3, v5}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startEpisodeDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_3

    :cond_10
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_11

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startSeasonDetails(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_11
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_12

    invoke-direct {p0, v0, v4}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startShowDetails(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_12
    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startHome(Landroid/content/Intent;)V

    goto :goto_3
.end method

.method private handlePlayUriIntent(Lcom/google/android/youtube/videos/VideosApplication;Landroid/content/Intent;)V
    .locals 10
    .param p1    # Lcom/google/android/youtube/videos/VideosApplication;
    .param p2    # Landroid/content/Intent;

    const/4 v9, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x2

    if-lt v7, v8, :cond_0

    const-string v7, "movies"

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startHome(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->getAccount(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/VideosApplication;->hasPremiumError()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-direct {p0, v0, v5}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    :cond_3
    const/4 v7, 0x0

    invoke-direct {p0, v7, v5}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v7, "movies"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    const-string v7, "v"

    invoke-virtual {v3, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    const-string v7, "play"

    invoke-static {v3, v7, v9}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->getBooleanQueryParameter(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-direct {p0, v0, v6}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startMoviePlayback(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v7, "dl"

    invoke-static {v3, v7, v9}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->getBooleanQueryParameter(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v7

    invoke-direct {p0, v0, v6, v7}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startMovieDetails(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_6
    const-string v7, "movies"

    invoke-direct {p0, v0, v7}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const-string v7, "shows"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    const-string v7, "sh"

    invoke-virtual {v3, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "se"

    invoke-virtual {v3, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v7, "v"

    invoke-virtual {v3, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_9

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_9

    const-string v7, "play"

    invoke-static {v3, v7, v9}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->getBooleanQueryParameter(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-direct {p0, v0, v6, v1}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startEpisodePlayback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v7, "dl"

    invoke-static {v3, v7, v9}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->getBooleanQueryParameter(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v7

    invoke-direct {p0, v0, v6, v1, v7}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startEpisodeDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_9
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a

    invoke-direct {p0, v0, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startShowDetails(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-string v7, "shows"

    invoke-direct {p0, v0, v7}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v7, "local"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    const-string v7, "shows"

    invoke-direct {p0, v0, v7}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startHome(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private handleSearchIntent(Landroid/content/Intent;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "intent_extra_data_key"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    if-nez v3, :cond_1

    const-string v4, "query"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "movies"

    invoke-static {p0, v1, v4, v0}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForSearch(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    const-string v4, "inAppSearch"

    invoke-interface {p2, v4, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForSearch(Ljava/lang/String;Z)V

    :goto_0
    if-nez v2, :cond_0

    const v4, 0x7f0a0078

    const/4 v5, 0x1

    invoke-static {p0, v4, v5}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/store/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {p0, v3, v0}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForUri(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/tv/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "inAppSearch"

    invoke-interface {p2, v4, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForShow(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const-string v4, "inAppSearch"

    invoke-interface {p2, v4, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForMovie(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected intent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startActivityIntents([Landroid/content/Intent;)V
    .locals 6
    .param p1    # [Landroid/content/Intent;

    sget v4, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivityIntentsV11([Landroid/content/Intent;)V

    :cond_0
    return-void

    :cond_1
    move-object v0, p1

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private startActivityIntentsV11([Landroid/content/Intent;)V
    .locals 0
    .param p1    # [Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    return-void
.end method

.method private startEpisodeDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "shows"

    invoke-direct {p0, p1, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0, p1, p3, p2, p4}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createEpisodeIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivityIntents([Landroid/content/Intent;)V

    return-void
.end method

.method private startEpisodePlayback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/content/Intent;

    const-string v1, "shows"

    invoke-direct {p0, p1, v1}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p3, p2, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createEpisodeIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createWatchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivityIntents([Landroid/content/Intent;)V

    return-void
.end method

.method private startHome(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/LauncherActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPromoManager()Lcom/google/android/youtube/videos/PromoManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/PromoManager;->incrementLaunchCount()V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createHomeIntent(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private startMovieDetails(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "movies"

    invoke-direct {p0, p1, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createMovieIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivityIntents([Landroid/content/Intent;)V

    return-void
.end method

.method private startMoviePlayback(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/content/Intent;

    const-string v1, "movies"

    invoke-direct {p0, p1, v1}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createMovieIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createWatchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivityIntents([Landroid/content/Intent;)V

    return-void
.end method

.method private startSeasonDetails(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "shows"

    invoke-direct {p0, p1, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createSeasonIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivityIntents([Landroid/content/Intent;)V

    return-void
.end method

.method private startShowDetails(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "shows"

    invoke-direct {p0, p1, v2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createShowIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivityIntents([Landroid/content/Intent;)V

    return-void
.end method

.method private startVertical(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/VideosApplication;

    iput-object v4, p0, Lcom/google/android/youtube/videos/activity/LauncherActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/LauncherActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "android.intent.action.SEARCH"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->handleSearchIntent(Landroid/content/Intent;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/LauncherActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->updatePremiumErrorMessage()V

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    const/4 v2, 0x0

    if-eqz v3, :cond_1

    const-string v4, "ref"

    invoke-virtual {v3, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_1
    invoke-interface {v0, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->setReferer(Ljava/lang/String;)V

    if-eqz v3, :cond_2

    const-string v4, "play.google.com"

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/LauncherActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-direct {p0, v4, v1}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->handlePlayUriIntent(Lcom/google/android/youtube/videos/VideosApplication;Landroid/content/Intent;)V

    :goto_1
    invoke-interface {v0, p0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onVideosStart(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/activity/LauncherActivity;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-direct {p0, v4, v1}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->handleLegacyIntent(Lcom/google/android/youtube/videos/VideosApplication;Landroid/content/Intent;)V

    goto :goto_1
.end method
