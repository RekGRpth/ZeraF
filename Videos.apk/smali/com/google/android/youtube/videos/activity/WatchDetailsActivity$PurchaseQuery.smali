.class interface abstract Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;
.super Ljava/lang/Object;
.source "WatchDetailsActivity.java"

# interfaces
.implements Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "PurchaseQuery"
.end annotation


# static fields
.field public static final ACTORS:I

.field public static final COLUMNS:[Ljava/lang/String;

.field public static final DESCRIPTION:I

.field public static final DIRECTORS:I

.field public static final FORMATS:I

.field public static final PRODUCERS:I

.field public static final RATINGS:I

.field public static final RELATED_URI:I

.field public static final RELEASE_YEAR:I

.field public static final STREAM_FORMATS:I

.field public static final TITLE:I

.field public static final VIDEO_DURATION_SECONDS:I

.field public static final WRITERS:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "title"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "writers"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "directors"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "actors"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "producers"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "release_year"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "duration_seconds"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "related_uri"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "ratings"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "formats"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "stream_formats"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->extend([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x0

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->TITLE:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->DESCRIPTION:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->WRITERS:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x3

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->DIRECTORS:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->ACTORS:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x5

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->PRODUCERS:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x6

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->RELEASE_YEAR:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x7

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->VIDEO_DURATION_SECONDS:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x8

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->RELATED_URI:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x9

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->RATINGS:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xa

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->FORMATS:I

    sget-object v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xb

    sput v0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$PurchaseQuery;->STREAM_FORMATS:I

    return-void
.end method
