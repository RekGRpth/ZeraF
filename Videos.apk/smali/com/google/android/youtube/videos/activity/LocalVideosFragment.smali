.class public Lcom/google/android/youtube/videos/activity/LocalVideosFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "LocalVideosFragment.java"


# instance fields
.field private helper:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

.field private thumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->thumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Lvedroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v5

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Landroid/view/View;Landroid/os/Bundle;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->helper:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getLocalVideoThumbnailStore()Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->thumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v0, 0x7f040019

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->helper:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->helper:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->helper:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->init()V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/LocalVideosFragment;->helper:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->reset()V

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onStop()V

    return-void
.end method
