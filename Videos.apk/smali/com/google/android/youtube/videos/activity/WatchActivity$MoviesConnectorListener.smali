.class Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;
.super Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$BaseAtHomeConnectorListener;
.source "WatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/WatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoviesConnectorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/activity/WatchActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$BaseAtHomeConnectorListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/activity/WatchActivity;Lcom/google/android/youtube/videos/activity/WatchActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/activity/WatchActivity;
    .param p2    # Lcom/google/android/youtube/videos/activity/WatchActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;-><init>(Lcom/google/android/youtube/videos/activity/WatchActivity;)V

    return-void
.end method


# virtual methods
.method public onConnectorChanged(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
    .param p2    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivity;

    # getter for: Lcom/google/android/youtube/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/youtube/videos/activity/WatchActivityCompat;
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->access$100(Lcom/google/android/youtube/videos/activity/WatchActivity;)Lcom/google/android/youtube/videos/activity/WatchActivityCompat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat;->onAtHomeConnectorChanged()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivity;

    # getter for: Lcom/google/android/youtube/videos/activity/WatchActivity;->resumed:Z
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->access$200(Lcom/google/android/youtube/videos/activity/WatchActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivity;

    # getter for: Lcom/google/android/youtube/videos/activity/WatchActivity;->keyguardManager:Landroid/app/KeyguardManager;
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->access$300(Lcom/google/android/youtube/videos/activity/WatchActivity;)Landroid/app/KeyguardManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivity;

    # getter for: Lcom/google/android/youtube/videos/activity/WatchActivity;->director:Lcom/google/android/youtube/videos/player/Director;
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->access$400(Lcom/google/android/youtube/videos/activity/WatchActivity;)Lcom/google/android/youtube/videos/player/Director;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/player/Director;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivity$MoviesConnectorListener;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivity;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/google/android/youtube/videos/activity/WatchActivity;->initDirector(ZZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/videos/activity/WatchActivity;->access$500(Lcom/google/android/youtube/videos/activity/WatchActivity;ZZ)V

    :cond_0
    return-void
.end method
