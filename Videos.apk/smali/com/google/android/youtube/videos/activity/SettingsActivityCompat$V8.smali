.class Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$V8;
.super Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;
.source "SettingsActivityCompat.java"

# interfaces
.implements Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V8"
.end annotation


# instance fields
.field private final activity:Lcom/google/android/youtube/videos/activity/SettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/SettingsActivity;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-virtual {p1, p0}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->configureSettings(Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$SettingsContainer;)V

    return-void
.end method

.method public static setIntentExtras(Landroid/content/Intent;)V
    .locals 0
    .param p0    # Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public addPreferencesFromResource(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->addPreferencesFromResource(I)V

    return-void
.end method

.method public findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method public getPreferenceManager()Landroid/preference/PreferenceManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    return-object v0
.end method

.method public getPreferenceScreen()Landroid/preference/PreferenceScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/SettingsActivityCompat$V8;->activity:Lcom/google/android/youtube/videos/activity/SettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method
