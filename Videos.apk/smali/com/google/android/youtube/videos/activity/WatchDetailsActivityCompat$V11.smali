.class Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat$V11;
.super Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;
.source "WatchDetailsActivityCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V11"
.end annotation


# instance fields
.field private final activity:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v2, 0x7f0a0007

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    const/16 v1, 0x1c

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivityCompat$V11;->activity:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onUpPressed()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
