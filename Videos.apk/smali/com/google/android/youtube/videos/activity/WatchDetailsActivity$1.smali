.class Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$1;
.super Ljava/lang/Object;
.source "WatchDetailsActivity.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Landroid/util/Pair",
        "<",
        "Lcom/google/android/youtube/videos/accounts/UserAuth;",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$1;->this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/util/Pair;Ljava/lang/Exception;)V
    .locals 1
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$1;->this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    # invokes: Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onSyncError(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->access$100(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Landroid/util/Pair;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$1;->onError(Landroid/util/Pair;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Landroid/util/Pair;Ljava/lang/Void;)V
    .locals 1
    .param p2    # Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Void;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$1;->this$0:Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;

    # invokes: Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->onSyncSuccess()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->access$000(Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Landroid/util/Pair;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity$1;->onResponse(Landroid/util/Pair;Ljava/lang/Void;)V

    return-void
.end method
