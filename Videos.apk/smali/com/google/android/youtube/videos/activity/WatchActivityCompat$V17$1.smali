.class Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17$1;
.super Landroid/media/MediaRouter$SimpleCallback;
.source "WatchActivityCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17$1;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;

    invoke-direct {p0}, Landroid/media/MediaRouter$SimpleCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onRoutePresentationDisplayChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17$1;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;

    # getter for: Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->access$100(Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-ne p2, v0, :cond_0

    invoke-virtual {p2}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17$1;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;

    iget-object v0, v0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->activity:Lcom/google/android/youtube/videos/activity/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/activity/WatchActivity;->onPresentationDisplayRouteChanged()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17$1;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;

    # invokes: Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->updateDisplays()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->access$000(Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;)V

    goto :goto_0
.end method

.method public onRouteSelected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # I
    .param p3    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17$1;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;

    # invokes: Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->updateDisplays()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->access$000(Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;)V

    return-void
.end method

.method public onRouteUnselected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p1    # Landroid/media/MediaRouter;
    .param p2    # I
    .param p3    # Landroid/media/MediaRouter$RouteInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17$1;->this$0:Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;

    # invokes: Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->updateDisplays()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;->access$000(Lcom/google/android/youtube/videos/activity/WatchActivityCompat$V17;)V

    return-void
.end method
