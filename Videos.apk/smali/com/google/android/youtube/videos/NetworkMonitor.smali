.class public Lcom/google/android/youtube/videos/NetworkMonitor;
.super Landroid/content/BroadcastReceiver;
.source "NetworkMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/NetworkMonitor$Listener;
    }
.end annotation


# instance fields
.field private connected:Z

.field private final connectivityManager:Landroid/net/ConnectivityManager;

.field private final context:Landroid/content/Context;

.field private final listener:Lcom/google/android/youtube/videos/NetworkMonitor$Listener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/videos/NetworkMonitor$Listener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/NetworkMonitor$Listener;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->context:Landroid/content/Context;

    const-string v0, "listener cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/NetworkMonitor$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->listener:Lcom/google/android/youtube/videos/NetworkMonitor$Listener;

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->connectivityManager:Landroid/net/ConnectivityManager;

    return-void
.end method

.method private update()Z
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    :goto_0
    iget-boolean v4, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->connected:Z

    if-eq v4, v0, :cond_1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->connected:Z

    :goto_1
    return v2

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method


# virtual methods
.method public isConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->connected:Z

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/NetworkMonitor;->update()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->listener:Lcom/google/android/youtube/videos/NetworkMonitor$Listener;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->connected:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/NetworkMonitor$Listener;->onNetworkConnectivityChanged(Z)V

    :cond_0
    return-void
.end method

.method public register()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->context:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/NetworkMonitor;->update()Z

    return-void
.end method

.method public unregister()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/NetworkMonitor;->context:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
