.class public Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;
.super Landroid/widget/CursorAdapter;
.source "LocalVideosAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter$Query;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final missingBitmap:Landroid/graphics/Bitmap;

.field private final thumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

.field private final timeFormat:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/graphics/Bitmap;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    iput-object p1, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->activity:Landroid/app/Activity;

    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->dateFormat:Ljava/text/DateFormat;

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->timeFormat:Ljava/text/DateFormat;

    iput-object p2, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->missingBitmap:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->thumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    return-void
.end method

.method private formatDateTime(J)Ljava/lang/String;
    .locals 3
    .param p1    # J

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->timeFormat:Ljava/text/DateFormat;

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    move-object v4, p1

    check-cast v4, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;

    const/4 v8, 0x0

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v8, 0x4

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const-wide/16 v8, 0x0

    cmp-long v8, v5, v8

    if-nez v8, :cond_0

    const/4 v8, 0x5

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    :cond_0
    const/4 v8, 0x6

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v1, v8

    const/4 v8, 0x2

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/16 v8, 0xa

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v8, "/"

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/16 v8, 0x2f

    invoke-virtual {v0, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    :cond_1
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/16 v8, 0x9

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    :cond_2
    if-lez v1, :cond_3

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->secondsToDurationString(I)Ljava/lang/String;

    move-result-object v8

    :goto_0
    const/4 v9, 0x3

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-direct {p0, v9, v10}, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->formatDateTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v7, v8, v9}, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;->setInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->activity:Landroid/app/Activity;

    iget-object v9, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->thumbnailStore:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    new-instance v10, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    invoke-direct {v10, v2, v3, v5, v6}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;-><init>(JJ)V

    iget-object v11, p0, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->missingBitmap:Landroid/graphics/Bitmap;

    invoke-static {v8, v4, v9, v10, v11}, Lcom/google/android/youtube/videos/ui/BitmapLoader;->setBitmapAsync(Landroid/app/Activity;Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/youtube/core/async/Requester;Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    return-void

    :cond_3
    const-string v8, ""

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/videos/ui/LocalVideoItemView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
