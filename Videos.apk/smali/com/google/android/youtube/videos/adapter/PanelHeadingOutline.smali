.class public Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;
.super Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;
.source "PanelHeadingOutline.java"


# instance fields
.field private final title:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # I
    .param p3    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;-><init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    iput-object p4, p0, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;->title:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .param p3    # Ljava/lang/String;

    const v0, 0x7f040028

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method
