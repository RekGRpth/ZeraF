.class public interface abstract Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter$Query;
.super Ljava/lang/Object;
.source "OtherEpisodesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Query"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_id AS _id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "episode_number_text"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/adapter/OtherEpisodesAdapter$Query;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
