.class public Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;
.super Landroid/widget/CursorAdapter;
.source "MyShowsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/adapter/MyShowsAdapter$Query;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private isNetworkConnected:Z

.field private final missingPoster:Landroid/graphics/Bitmap;

.field private final showPosterRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/store/PosterStore;
    .param p3    # Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->activity:Landroid/app/Activity;

    const-string v0, "missingPoster cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->missingPoster:Landroid/graphics/Bitmap;

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/store/PosterStore;->getShowPosterRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->showPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->showPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->missingPoster:Landroid/graphics/Bitmap;

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/youtube/videos/ui/BitmapLoader;->setBitmapAsync(Landroid/app/Activity;Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/youtube/core/async/Requester;Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->isNetworkConnected:Z

    if-nez v1, :cond_0

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->getNumberOfPinnedEpisodes(Landroid/database/Cursor;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->setDimmedStyle(Z)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNumberOfPinnedEpisodes(Landroid/database/Cursor;)I
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getShowId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public setNetworkConnected(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->isNetworkConnected:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->isNetworkConnected:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
