.class public Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;
.super Landroid/widget/CursorAdapter;
.source "MyEpisodesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter$Query;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final allowDownloads:Z

.field private isNetworkConnected:Z

.field private lastWatchedVideoId:Ljava/lang/String;

.field private final missingPoster:Landroid/graphics/Bitmap;

.field private final videoPosterRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/graphics/Bitmap;Z)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/store/PosterStore;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->activity:Landroid/app/Activity;

    const-string v0, "missingPoster cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->missingPoster:Landroid/graphics/Bitmap;

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/store/PosterStore;->getVideoPosterRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->videoPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    iput-boolean p4, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->allowDownloads:Z

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    move-object v3, p1

    check-cast v3, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;

    const/4 v8, 0x1

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->setEpisodeNumber(Ljava/lang/String;)V

    const/4 v8, 0x2

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->setTitle(Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->lastWatchedVideoId:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    invoke-virtual {v3, v8}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->setPlayButtonVisible(Z)V

    iget-object v8, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->activity:Landroid/app/Activity;

    iget-object v9, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->videoPosterRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v10, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->missingPoster:Landroid/graphics/Bitmap;

    invoke-static {v8, v3, v9, v7, v10}, Lcom/google/android/youtube/videos/ui/BitmapLoader;->setBitmapAsync(Landroid/app/Activity;Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/youtube/core/async/Requester;Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->isPinned(Landroid/database/Cursor;)Z

    move-result v4

    const/16 v8, 0x8

    invoke-static {p3, v8}, Lcom/google/android/youtube/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v1

    const/4 v8, 0x7

    invoke-static {p3, v8}, Lcom/google/android/youtube/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x64

    mul-long/2addr v8, v10

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    div-long/2addr v8, v10

    long-to-int v6, v8

    :goto_0
    const/4 v8, 0x4

    invoke-static {p3, v8}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v5

    iget-boolean v8, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->allowDownloads:Z

    invoke-virtual {v3, v8, v4, v5, v6}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->setPinningInfo(ZZLjava/lang/Integer;I)V

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_1

    const/4 v2, 0x1

    :goto_1
    iget-boolean v8, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->isNetworkConnected:Z

    if-nez v8, :cond_2

    if-nez v2, :cond_2

    const/4 v8, 0x1

    :goto_2
    invoke-virtual {v3, v8}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;->setDimmedStyle(Z)V

    return-void

    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPinned(Landroid/database/Cursor;)Z
    .locals 2
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/videos/ui/PurchasedEpisodeItemView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public setLastWatchedVideoId(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->lastWatchedVideoId:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->lastWatchedVideoId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setNetworkConnected(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->isNetworkConnected:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->isNetworkConnected:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/adapter/MyEpisodesAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
