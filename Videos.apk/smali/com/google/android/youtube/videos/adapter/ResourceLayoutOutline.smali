.class public Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;
.super Lcom/google/android/youtube/core/adapter/SingleViewOutline;
.source "ResourceLayoutOutline.java"


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;

.field private final layoutResId:I


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;)V
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # I
    .param p3    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-direct {p0, p3}, Lcom/google/android/youtube/core/adapter/SingleViewOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    const-string v0, "inflater cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;->inflater:Landroid/view/LayoutInflater;

    iput p2, p0, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;->layoutResId:I

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;->inflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;->layoutResId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    return-object p2
.end method
