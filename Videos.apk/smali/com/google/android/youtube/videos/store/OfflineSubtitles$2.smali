.class Lcom/google/android/youtube/videos/store/OfflineSubtitles$2;
.super Ljava/lang/Object;
.source "OfflineSubtitles.java"

# interfaces
.implements Lcom/google/android/youtube/core/client/SubtitlesClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/OfflineSubtitles;->createStoringSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/OfflineSubtitles;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles$2;->this$0:Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public requestSubtitleTracks(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;>;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/converter/http/SubtitleTracksConverter$CallbackWrapper;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/core/converter/http/SubtitleTracksConverter$CallbackWrapper;-><init>(Lcom/google/android/youtube/core/async/Callback;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles$2;->this$0:Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    # getter for: Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitleTracksStorer:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->access$300(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public requestSubtitles(Lcom/google/android/youtube/core/model/SubtitleTrack;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            "Lcom/google/android/youtube/core/model/Subtitles;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles$2;->this$0:Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    # getter for: Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesStorer:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->access$200(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
