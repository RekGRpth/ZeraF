.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$1;
.super Ljava/lang/Object;
.source "PurchaseStoreSync.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/youtube/core/model/Season;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;

.field final synthetic val$this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$1;->this$1:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$1;->val$this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$1;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$1;->this$1:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;

    # invokes: Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->onError(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->access$1100(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/google/android/youtube/core/model/Season;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$1;->onResponse(Ljava/lang/String;Lcom/google/android/youtube/core/model/Season;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Lcom/google/android/youtube/core/model/Season;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/core/model/Season;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask$1;->this$1:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;

    # invokes: Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->onSeason(Lcom/google/android/youtube/core/model/Season;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->access$1000(Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;Lcom/google/android/youtube/core/model/Season;)V

    return-void
.end method
