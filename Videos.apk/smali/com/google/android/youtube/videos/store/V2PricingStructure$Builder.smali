.class public final Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;
.super Ljava/lang/Object;
.source "V2PricingStructure.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/V2PricingStructure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x1e071c8e1879bf27L


# instance fields
.field private duration:Lcom/google/android/youtube/core/model/Duration;

.field private formats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private infoUri:Landroid/net/Uri;

.field private price:Lcom/google/android/youtube/core/model/Money;

.field private type:Lcom/google/android/youtube/core/model/PricingStructure$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .param p1    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/PricingStructure$Type;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Duration;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->asUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Money;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->formats:Ljava/util/List;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->build()Lcom/google/android/youtube/core/model/PricingStructure;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2
    .param p1    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "should never be serialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/core/model/PricingStructure;
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->formats:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->formats:Ljava/util/List;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    sget-object v2, Lcom/google/android/youtube/core/model/PricingStructure$Type;->RENT:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/youtube/core/model/PricingStructure;->createRental(Lcom/google/android/youtube/core/model/Duration;Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure;

    move-result-object v1

    :goto_1
    return-object v1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/V2PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    invoke-static {v1, v0, v2, v4}, Lcom/google/android/youtube/core/model/PricingStructure;->createPurchase(Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure;

    move-result-object v1

    goto :goto_1
.end method
