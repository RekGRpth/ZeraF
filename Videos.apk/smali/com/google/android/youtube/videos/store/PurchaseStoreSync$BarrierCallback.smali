.class final Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;
.super Ljava/lang/Object;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BarrierCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private invokedCallback:Z

.field private maxErrorLevel:I

.field private maxException:Ljava/lang/Exception;

.field private pending:I

.field private final request:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "request cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->request:Ljava/lang/Object;

    const-string v0, "callback cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Callback;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->callback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method public static create(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;",
            "Ljava/lang/Void;",
            ">;)",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<TR;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;-><init>(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized decrement()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->invokedCallback:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    const-string v3, "decrement called after callback was invoked"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->pending:I

    if-lez v2, :cond_2

    :goto_1
    const-string v1, "decrement called when pending was equal to 0"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->pending:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->pending:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->invokedCallback:Z

    iget v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->maxErrorLevel:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->request:Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_2
    monitor-exit p0

    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->request:Ljava/lang/Object;

    new-instance v2, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;

    iget v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->maxErrorLevel:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->maxException:Ljava/lang/Exception;

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;-><init>(ILjava/lang/Exception;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized increment()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->invokedCallback:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "increment called after callback was invoked"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->pending:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->pending:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onError(ILjava/lang/Exception;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/Exception;

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->invokedCallback:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    const-string v3, "onError called after callback was invoked"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->pending:I

    if-lez v2, :cond_2

    :goto_1
    const-string v1, "onError called when pending was equal to 0"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    const-string v0, "exception cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->maxErrorLevel:I

    if-le p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->maxErrorLevel:I

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->maxException:Ljava/lang/Exception;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
