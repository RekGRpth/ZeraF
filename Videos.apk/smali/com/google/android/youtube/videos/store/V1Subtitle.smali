.class public Lcom/google/android/youtube/videos/store/V1Subtitle;
.super Ljava/lang/Object;
.source "V1Subtitle.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/V1Subtitle$Line;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x34fd265c9966bc5cL


# instance fields
.field private final lines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/youtube/videos/store/V1Subtitle$Line;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/V1Subtitle;->lines:Ljava/util/ArrayList;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 7

    new-instance v0, Lcom/google/android/youtube/core/model/Subtitles$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/Subtitles$Builder;-><init>()V

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/store/V1Subtitle;->lines:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/V1Subtitle;->lines:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/store/V1Subtitle$Line;

    const/4 v3, 0x0

    iget-object v4, v2, Lcom/google/android/youtube/videos/store/V1Subtitle$Line;->text:Ljava/lang/String;

    iget v5, v2, Lcom/google/android/youtube/videos/store/V1Subtitle$Line;->startTimeMillis:I

    iget v6, v2, Lcom/google/android/youtube/videos/store/V1Subtitle$Line;->endTimeMillis:I

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/google/android/youtube/core/model/Subtitles$Builder;->addLineToWindow(ILjava/lang/String;II)Lcom/google/android/youtube/core/model/Subtitles$Builder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Subtitles$Builder;->build()Lcom/google/android/youtube/core/model/Subtitles;

    move-result-object v3

    return-object v3
.end method
