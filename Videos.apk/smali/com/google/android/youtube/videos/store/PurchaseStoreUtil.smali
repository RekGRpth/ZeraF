.class public Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;
.super Ljava/lang/Object;
.source "PurchaseStoreUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/PurchaseStoreUtil$1;
    }
.end annotation


# static fields
.field private static ITEM_URI_PATTERN:Ljava/util/regex/Pattern;

.field private static OUTCOME_INSERTED_AS_NEW:I

.field private static OUTCOME_NOT_WRITTEN:I

.field private static OUTCOME_REPLACED_OLDER_PURCHASE:I

.field private static OUTCOME_UPDATED_SAME_PURCHASE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_INSERTED_AS_NEW:I

    const/4 v0, 0x1

    sput v0, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_UPDATED_SAME_PURCHASE:I

    const/4 v0, 0x2

    sput v0, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_REPLACED_OLDER_PURCHASE:I

    const/4 v0, 0x3

    sput v0, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_NOT_WRITTEN:I

    const-string v0, "/(?:videos|seasons|shows|shows/[-_A-Za-z0-9]+/content)/([-_A-Za-z0-9]+)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->ITEM_URI_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildSeasonContentValues(Lcom/google/android/youtube/core/model/Season;Lcom/google/android/youtube/core/model/Show;JZ)Landroid/content/ContentValues;
    .locals 4
    .param p0    # Lcom/google/android/youtube/core/model/Season;
    .param p1    # Lcom/google/android/youtube/core/model/Show;
    .param p2    # J
    .param p4    # Z

    const/4 v3, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "season_synced_timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "season_id"

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Season;->selfUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "season_title"

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Season;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "show_id"

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Season;->showUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "show_title"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Show;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "show_description"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Show;->description:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Show;->hqPosterUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    const-string v1, "show_poster_uri"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Show;->hqPosterUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "show_poster_synced"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p1, Lcom/google/android/youtube/core/model/Show;->posterUri:Landroid/net/Uri;

    if-eqz v1, :cond_1

    const-string v1, "show_poster_uri"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Show;->posterUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "show_poster_synced"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_1
    const-string v1, "show_poster_synced"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method public static buildVideoContentValues(Lcom/google/android/youtube/core/model/Video;JZ)Landroid/content/ContentValues;
    .locals 14
    .param p0    # Lcom/google/android/youtube/core/model/Video;
    .param p1    # J
    .param p3    # Z

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v11, "video_synced_timestamp"

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v11, "video_id"

    iget-object v12, p0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "title"

    iget-object v12, p0, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "description"

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->synopsis:Ljava/lang/String;

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->synopsis:Ljava/lang/String;

    :goto_0
    invoke-virtual {v10, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->pro:Lcom/google/android/youtube/core/model/Video$Pro;

    iget-object v2, v11, Lcom/google/android/youtube/core/model/Video$Pro;->credits:Ljava/util/Map;

    if-eqz v2, :cond_0

    const-string v12, "writers"

    const-string v11, "Writer"

    invoke-interface {v2, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-static {v10, v12, v11}, Lcom/google/android/youtube/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    const-string v12, "directors"

    const-string v11, "Director"

    invoke-interface {v2, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-static {v10, v12, v11}, Lcom/google/android/youtube/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    const-string v12, "actors"

    const-string v11, "Actor"

    invoke-interface {v2, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-static {v10, v12, v11}, Lcom/google/android/youtube/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    const-string v12, "producers"

    const-string v11, "Producer"

    invoke-interface {v2, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-static {v10, v12, v11}, Lcom/google/android/youtube/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/model/Video;->contentRatings:Ljava/util/List;

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v11

    mul-int/lit8 v11, v11, 0x2

    invoke-direct {v3, v11}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v11

    if-ge v4, v11, :cond_2

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/youtube/core/model/Rating;

    iget-object v11, v7, Lcom/google/android/youtube/core/model/Rating;->scheme:Ljava/lang/String;

    invoke-interface {v3, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v11, v7, Lcom/google/android/youtube/core/model/Rating;->classification:Ljava/lang/String;

    invoke-interface {v3, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v11, "ratings"

    invoke-static {v10, v11, v3}, Lcom/google/android/youtube/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    const-string v11, "duration_seconds"

    iget v12, p0, Lcom/google/android/youtube/core/model/Video;->duration:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "claimed"

    iget-boolean v12, p0, Lcom/google/android/youtube/core/model/Video;->claimed:Z

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v11, "subtitle_mode"

    invoke-static {p0}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->getSubtitleMode(Lcom/google/android/youtube/core/model/Video;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "default_subtitle_language"

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->getDefaultSubtitleLanguageCode()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "captions_track_uri"

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->captionTracksUri:Landroid/net/Uri;

    if-nez v11, :cond_6

    const/4 v11, 0x0

    :goto_2
    invoke-virtual {v10, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "related_uri"

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    if-nez v11, :cond_7

    const/4 v11, 0x0

    :goto_3
    invoke-virtual {v10, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x0

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->movie:Lcom/google/android/youtube/core/model/Video$Movie;

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->movie:Lcom/google/android/youtube/core/model/Video$Movie;

    iget-object v6, v11, Lcom/google/android/youtube/core/model/Video$Movie;->posterUri:Landroid/net/Uri;

    :cond_3
    :goto_4
    const-string v12, "poster_uri"

    if-nez v6, :cond_a

    const/4 v11, 0x0

    :goto_5
    invoke-virtual {v10, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "poster_synced"

    if-nez v6, :cond_b

    const/4 v11, 0x1

    :goto_6
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v10, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->pro:Lcom/google/android/youtube/core/model/Video$Pro;

    iget-object v11, v11, Lcom/google/android/youtube/core/model/Video$Pro;->releaseDate:Ljava/util/Date;

    if-eqz v11, :cond_c

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->pro:Lcom/google/android/youtube/core/model/Video$Pro;

    iget-object v11, v11, Lcom/google/android/youtube/core/model/Video$Pro;->releaseDate:Ljava/util/Date;

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const-string v11, "release_year"

    const/4 v12, 0x1

    invoke-virtual {v0, v12}, Ljava/util/Calendar;->get(I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_7
    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    if-eqz v11, :cond_4

    const-string v11, "episode_number_text"

    iget-object v12, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    iget-object v12, v12, Lcom/google/android/youtube/core/model/Video$Episode;->number:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "episode_season_id"

    iget-object v12, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    iget-object v12, v12, Lcom/google/android/youtube/core/model/Video$Episode;->seasonUri:Landroid/net/Uri;

    invoke-static {v12}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->publishedDate:Ljava/util/Date;

    if-eqz v11, :cond_5

    const-string v11, "publish_timestamp"

    iget-object v12, p0, Lcom/google/android/youtube/core/model/Video;->publishedDate:Ljava/util/Date;

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_5
    new-instance v9, Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->size()I

    move-result v11

    invoke-direct {v9, v11}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/youtube/core/model/Stream;

    iget v11, v8, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_6
    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->captionTracksUri:Landroid/net/Uri;

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_2

    :cond_7
    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_3

    :cond_8
    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    if-eqz v11, :cond_3

    if-eqz p3, :cond_9

    iget-object v11, p0, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    if-eqz v11, :cond_9

    iget-object v6, p0, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    goto/16 :goto_4

    :cond_9
    iget-object v6, p0, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    goto/16 :goto_4

    :cond_a
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_5

    :cond_b
    const/4 v11, 0x0

    goto/16 :goto_6

    :cond_c
    const-string v11, "release_year"

    invoke-virtual {v10, v11}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_d
    const-string v11, "stream_formats"

    invoke-static {v10, v11, v9}, Lcom/google/android/youtube/videos/utils/DbUtils;->putIntegerList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    return-object v10
.end method

.method private static canHandle(Lcom/google/android/youtube/core/model/Purchase;)Z
    .locals 2
    .param p0    # Lcom/google/android/youtube/core/model/Purchase;

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    sget-object v1, Lcom/google/android/youtube/core/model/Purchase$ItemType;->MOVIE:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    sget-object v1, Lcom/google/android/youtube/core/model/Purchase$ItemType;->SEASON:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase;->itemUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Purchase;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getSubtitleMode(Lcom/google/android/youtube/core/model/Video;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/core/model/Video;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/Video;->showSubtitlesAlways:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/Video;->showSubtitlesByDefault:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static itemTypeToInteger(Lcom/google/android/youtube/core/model/Purchase$ItemType;)I
    .locals 2
    .param p0    # Lcom/google/android/youtube/core/model/Purchase$ItemType;

    sget-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil$1;->$SwitchMap$com$google$android$youtube$core$model$Purchase$ItemType:[I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Purchase$ItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, -0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/net/Uri;

    const/4 v1, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    sget-object v2, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->ITEM_URI_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static maybeStorePurchase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/youtube/core/model/Purchase;)I
    .locals 12
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/core/model/Purchase;

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    iget-object v5, p2, Lcom/google/android/youtube/core/model/Purchase;->itemUri:Landroid/net/Uri;

    invoke-static {v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Purchase;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    iget-object v2, v3, Lcom/google/android/youtube/core/model/PricingStructure;->price:Lcom/google/android/youtube/core/model/Money;

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "account"

    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "item_id"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "item_type"

    iget-object v9, p2, Lcom/google/android/youtube/core/model/Purchase;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    invoke-static {v9}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemTypeToInteger(Lcom/google/android/youtube/core/model/Purchase$ItemType;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "item_uri"

    iget-object v5, p2, Lcom/google/android/youtube/core/model/Purchase;->itemUri:Landroid/net/Uri;

    if-nez v5, :cond_2

    move-object v5, v6

    :goto_0
    invoke-virtual {v4, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "purchase_id"

    iget-object v9, p2, Lcom/google/android/youtube/core/model/Purchase;->id:Ljava/lang/String;

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "purchase_type"

    iget-object v9, p2, Lcom/google/android/youtube/core/model/Purchase;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    iget-object v9, v9, Lcom/google/android/youtube/core/model/PricingStructure;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    invoke-static {v9}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->purchaseTypeToInteger(Lcom/google/android/youtube/core/model/PricingStructure$Type;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "status"

    iget-object v9, p2, Lcom/google/android/youtube/core/model/Purchase;->status:Lcom/google/android/youtube/core/model/Purchase$Status;

    invoke-static {v9}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->purchaseStatusToInteger(Lcom/google/android/youtube/core/model/Purchase$Status;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "purchase_timestamp"

    iget-object v9, p2, Lcom/google/android/youtube/core/model/Purchase;->purchaseDate:Ljava/util/Date;

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "expiration_timestamp"

    iget-object v5, p2, Lcom/google/android/youtube/core/model/Purchase;->expirationDate:Ljava/util/Date;

    if-nez v5, :cond_3

    move-object v5, v6

    :goto_1
    invoke-virtual {v4, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "purchase_duration_seconds"

    iget-object v5, p2, Lcom/google/android/youtube/core/model/Purchase;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/PricingStructure;->duration:Lcom/google/android/youtube/core/model/Duration;

    if-nez v5, :cond_4

    move-object v5, v6

    :goto_2
    invoke-virtual {v4, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "formats"

    iget-object v9, p2, Lcom/google/android/youtube/core/model/Purchase;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    iget-object v9, v9, Lcom/google/android/youtube/core/model/PricingStructure;->formats:Ljava/util/List;

    invoke-static {v4, v5, v9}, Lcom/google/android/youtube/videos/utils/DbUtils;->putIntegerList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    const-string v9, "streamable"

    iget-boolean v5, p2, Lcom/google/android/youtube/core/model/Purchase;->streamable:Z

    if-eqz v5, :cond_5

    move v5, v7

    :goto_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "price_value"

    iget v9, v2, Lcom/google/android/youtube/core/model/Money;->value:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "price_currency"

    iget-object v9, v2, Lcom/google/android/youtube/core/model/Money;->currency:Ljava/util/Currency;

    invoke-virtual {v9}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "pricing_info_uri"

    iget-object v5, v3, Lcom/google/android/youtube/core/model/PricingStructure;->infoUri:Landroid/net/Uri;

    if-nez v5, :cond_6

    move-object v5, v6

    :goto_4
    invoke-virtual {v4, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, p2, v0, p0, v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->maybeWritePurchaseRow(Ljava/lang/String;Lcom/google/android/youtube/core/model/Purchase;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v1

    sget v5, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_INSERTED_AS_NEW:I

    if-eq v1, v5, :cond_0

    sget v5, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_REPLACED_OLDER_PURCHASE:I

    if-ne v1, v5, :cond_1

    :cond_0
    sget-object v5, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil$1;->$SwitchMap$com$google$android$youtube$core$model$Purchase$ItemType:[I

    iget-object v9, p2, Lcom/google/android/youtube/core/model/Purchase;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    invoke-virtual {v9}, Lcom/google/android/youtube/core/model/Purchase$ItemType;->ordinal()I

    move-result v9

    aget v5, v5, v9

    packed-switch v5, :pswitch_data_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected item type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p2, Lcom/google/android/youtube/core/model/Purchase;->itemType:Lcom/google/android/youtube/core/model/Purchase$ItemType;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    :cond_1
    :goto_5
    return v1

    :cond_2
    iget-object v5, p2, Lcom/google/android/youtube/core/model/Purchase;->itemUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    :cond_3
    iget-object v5, p2, Lcom/google/android/youtube/core/model/Purchase;->expirationDate:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto/16 :goto_1

    :cond_4
    iget-object v5, p2, Lcom/google/android/youtube/core/model/Purchase;->pricePaid:Lcom/google/android/youtube/core/model/PricingStructure;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/PricingStructure;->duration:Lcom/google/android/youtube/core/model/Duration;

    invoke-virtual {v5}, Lcom/google/android/youtube/core/model/Duration;->inSeconds()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto/16 :goto_2

    :cond_5
    move v5, v8

    goto :goto_3

    :cond_6
    iget-object v5, v3, Lcom/google/android/youtube/core/model/PricingStructure;->infoUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    :pswitch_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "pinning_account"

    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "pinning_video_id"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "video_userdata"

    const/4 v7, 0x4

    invoke-virtual {p0, v5, v6, v4, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_5

    :pswitch_1
    const-string v5, "INSERT OR IGNORE INTO video_userdata (pinning_account, pinning_video_id) SELECT ?, video_id FROM videos WHERE episode_season_id = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    aput-object p1, v6, v8

    aput-object v0, v6, v7

    invoke-virtual {p0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static maybeWritePurchaseRow(Ljava/lang/String;Lcom/google/android/youtube/core/model/Purchase;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    .locals 9
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/youtube/core/model/Purchase;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/database/sqlite/SQLiteDatabase;
    .param p4    # Landroid/content/ContentValues;

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-array v2, v8, [Ljava/lang/String;

    aput-object p0, v2, v5

    aput-object p2, v2, v6

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Purchase;->id:Ljava/lang/String;

    aput-object v3, v2, v7

    const-string v3, "purchases"

    const-string v4, "account = ? AND item_id = ? AND purchase_id = ?"

    invoke-virtual {p3, v3, p4, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    sget v3, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_UPDATED_SAME_PURCHASE:I

    :goto_0
    return v3

    :cond_0
    const-string v3, "hidden"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-array v2, v8, [Ljava/lang/String;

    aput-object p0, v2, v5

    aput-object p2, v2, v6

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Purchase;->purchaseDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const-string v3, "purchases"

    const-string v4, "account = ? AND item_id = ? AND purchase_timestamp < ?"

    invoke-virtual {p3, v3, p4, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    sget v3, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_REPLACED_OLDER_PURCHASE:I

    goto :goto_0

    :cond_1
    :try_start_0
    const-string v3, "purchases"

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v4, p4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    sget v3, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_INSERTED_AS_NEW:I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sget v3, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_NOT_WRITTEN:I

    goto :goto_0
.end method

.method private static purchaseStatusToInteger(Lcom/google/android/youtube/core/model/Purchase$Status;)I
    .locals 2
    .param p0    # Lcom/google/android/youtube/core/model/Purchase$Status;

    sget-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil$1;->$SwitchMap$com$google$android$youtube$core$model$Purchase$Status:[I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Purchase$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_4
    const/4 v0, -0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static purchaseTypeToInteger(Lcom/google/android/youtube/core/model/PricingStructure$Type;)I
    .locals 2
    .param p0    # Lcom/google/android/youtube/core/model/PricingStructure$Type;

    sget-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil$1;->$SwitchMap$com$google$android$youtube$core$model$PricingStructure$Type:[I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/PricingStructure$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static readUri(Landroid/database/Cursor;I)Landroid/net/Uri;
    .locals 1
    .param p0    # Landroid/database/Cursor;
    .param p1    # I

    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private static removePurchase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/youtube/core/model/Purchase;)V
    .locals 6
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/core/model/Purchase;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Purchase;->itemUri:Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "purchases"

    const-string v2, "account = ? AND item_id = ? AND purchase_id = ?"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v4, 0x2

    iget-object v5, p2, Lcom/google/android/youtube/core/model/Purchase;->id:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static updateStoredPurchases(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;
    .locals 12
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/youtube/core/model/Purchase;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/Purchase;",
            ">;"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/model/Purchase;

    invoke-static {v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->canHandle(Lcom/google/android/youtube/core/model/Purchase;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v6, v4, Lcom/google/android/youtube/core/model/Purchase;->status:Lcom/google/android/youtube/core/model/Purchase$Status;

    sget-object v8, Lcom/google/android/youtube/core/model/Purchase$Status;->ACTIVE:Lcom/google/android/youtube/core/model/Purchase$Status;

    if-eq v6, v8, :cond_1

    sget-object v8, Lcom/google/android/youtube/core/model/Purchase$Status;->PENDING:Lcom/google/android/youtube/core/model/Purchase$Status;

    if-eq v6, v8, :cond_1

    iget-object v8, v4, Lcom/google/android/youtube/core/model/Purchase;->expirationDate:Ljava/util/Date;

    if-eqz v8, :cond_2

    iget-object v8, v4, Lcom/google/android/youtube/core/model/Purchase;->expirationDate:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    sub-long v8, v1, v8

    const-wide/32 v10, 0x5265c00

    cmp-long v8, v8, v10

    if-gez v8, :cond_2

    :cond_1
    const/4 v5, 0x1

    :goto_1
    if-eqz v5, :cond_3

    invoke-static {p0, p1, v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->maybeStorePurchase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/youtube/core/model/Purchase;)I

    move-result v3

    sget v8, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->OUTCOME_NOT_WRITTEN:I

    if-eq v3, v8, :cond_0

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    invoke-static {p0, p1, v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->removePurchase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/youtube/core/model/Purchase;)V

    goto :goto_0

    :cond_4
    return-object v7
.end method
