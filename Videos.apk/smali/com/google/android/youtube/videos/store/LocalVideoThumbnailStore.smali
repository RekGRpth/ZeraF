.class public Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;
.super Ljava/lang/Object;
.source "LocalVideoThumbnailStore.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<",
        "Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field private static final CLEANUP_PROJECTION:[Ljava/lang/String;

.field private static final bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;


# instance fields
.field private final bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

.field private final bitmapConverter:Lcom/google/android/youtube/core/converter/ResponseConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/converter/ResponseConverter",
            "<[B",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final bitmapDirectory:Ljava/io/File;

.field private final context:Landroid/content/Context;

.field private final executor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const-string v1, "date_modified"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->CLEANUP_PROJECTION:[Ljava/lang/String;

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;

    sget-object v0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;

    iput v4, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    sget-object v0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    sget-object v0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/io/File;Lcom/google/android/youtube/core/converter/ResponseConverter;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Ljava/io/File;
    .param p5    # Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/io/File;",
            "Lcom/google/android/youtube/core/converter/ResponseConverter",
            "<[B",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/youtube/videos/utils/BitmapLruCache;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->context:Landroid/content/Context;

    const-string v0, "executor cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->executor:Ljava/util/concurrent/Executor;

    const-string v0, "bitmapDirectory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDirectory:Ljava/io/File;

    const-string v0, "bitmapConverter cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/ResponseConverter;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapConverter:Lcom/google/android/youtube/core/converter/ResponseConverter;

    const-string v0, "bitmapCache cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->CLEANUP_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;JJ)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;
    .param p1    # J
    .param p3    # J

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->toFile(JJ)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDirectory:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/io/File;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;
    .param p1    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;
    .param p3    # Ljava/io/File;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->fetchThumbnailFromMediaStore(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->getStoredBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    return-object v0
.end method

.method private fetchThumbnailFromMediaStore(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Ljava/io/File;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 11
    .param p1    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v10, 0x100

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-wide v6, p1, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->id:J

    const/4 v8, 0x1

    sget-object v9, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDecodeOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v5, v6, v7, v8, v9}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Couldn\'t get thumbnail for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, p1, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->id:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-gt v5, v10, :cond_1

    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    mul-int/lit16 v5, v5, 0x100

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    div-int v4, v5, v6

    const/4 v5, 0x0

    invoke-static {v2, v10, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {v3, v5, v6, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    :try_start_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {p0, p2, v5}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->storeBytes(Ljava/io/File;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v2, v3

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to store thumbnail in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private fetchThumbnailFromMediaStore(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/io/File;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;
    .param p3    # Ljava/io/File;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->fetchThumbnailFromMediaStore(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Ljava/io/File;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    invoke-virtual {v2, p4, v0}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2, p1, v0}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-interface {p2, p1, v1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private getStoredBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v5

    long-to-int v2, v5

    new-array v0, v2, [B

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    sub-int v5, v2, v4

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/FileInputStream;->read([BII)I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapConverter:Lcom/google/android/youtube/core/converter/ResponseConverter;

    invoke-interface {v5, v0}, Lcom/google/android/youtube/core/converter/ResponseConverter;->convertResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    return-object v5

    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    throw v5
.end method

.method private final storeBytes(Ljava/io/File;[B)V
    .locals 4
    .param p1    # Ljava/io/File;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to create directory: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDirectory:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/io/FileOutputStream;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    :try_start_0
    invoke-virtual {v0, p2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    throw v1
.end method

.method private toBitmapCacheKey(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p1, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->modifiedTimestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".l"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private toFile(JJ)Ljava/io/File;
    .locals 4
    .param p1    # J
    .param p3    # J

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final cleanup()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$1;-><init>(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final request(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->toBitmapCacheKey(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    invoke-interface {p2, p1, v1}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;

    invoke-direct {v3, p0, p1, p2, v0}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;-><init>(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    check-cast p1, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->request(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
