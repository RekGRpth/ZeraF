.class public Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
.super Ljava/lang/Object;
.source "StoreStatusMonitor.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/youtube/videos/store/Database$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/StoreStatusMonitor$PurchasesQuery;,
        Lcom/google/android/youtube/videos/store/StoreStatusMonitor$Listener;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final handler:Landroid/os/Handler;

.field private hasChanges:Z

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/store/StoreStatusMonitor$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private final purchasesCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final statuses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistStore:Lcom/google/android/youtube/videos/store/WishlistStore;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/WishlistStore;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/store/Database;
    .param p3    # Lcom/google/android/youtube/videos/store/PurchaseStore;
    .param p4    # Lcom/google/android/youtube/videos/store/WishlistStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->handler:Landroid/os/Handler;

    const-string v0, "database cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v0, "purchaseStore cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/PurchaseStore;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    const-string v0, "wishlistStore cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/WishlistStore;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->wishlistStore:Lcom/google/android/youtube/videos/store/WishlistStore;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->listeners:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    new-instance v0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor$1;-><init>(Lcom/google/android/youtube/videos/store/StoreStatusMonitor;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->purchasesCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor$2;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor$2;-><init>(Lcom/google/android/youtube/videos/store/StoreStatusMonitor;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->wishlistCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->processPurchasesQueryResult(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->processWishlistQueryResult(Landroid/database/Cursor;)V

    return-void
.end method

.method public static final isPurchased(I)Z
    .locals 1
    .param p0    # I

    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final isWishlisted(I)Z
    .locals 1
    .param p0    # I

    and-int/lit8 v0, p0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeNotifyListeners()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->handler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->listeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/StoreStatusMonitor$Listener;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor$Listener;->onStoreStatusChanged(Lcom/google/android/youtube/videos/store/StoreStatusMonitor;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private mergeQueryResult(Ljava/util/Set;I)V
    .locals 9
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;I)V"
        }
    .end annotation

    const/4 v8, 0x1

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    and-int/2addr v6, p2

    if-eq v6, p2, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    or-int/2addr v6, p2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v8, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->hasChanges:Z

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ne v6, p2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    iput-boolean v8, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->hasChanges:Z

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    iput-boolean v8, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->hasChanges:Z

    :cond_4
    iget-boolean v6, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->hasChanges:Z

    if-eqz v6, :cond_5

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->maybeNotifyListeners()V

    :cond_5
    return-void
.end method

.method private processPurchasesQueryResult(Landroid/database/Cursor;)V
    .locals 8
    .param p1    # Landroid/database/Cursor;

    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_1
    const/4 v4, 0x1

    :try_start_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->handler:Landroid/os/Handler;

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private processWishlistQueryResult(Landroid/database/Cursor;)V
    .locals 7
    .param p1    # Landroid/database/Cursor;

    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v2

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {p1, v3}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->handler:Landroid/os/Handler;

    const/4 v4, 0x3

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private queryPurchases()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/videos/store/StoreStatusMonitor$PurchasesQuery;->COLUMNS:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createActivePurchasesRequestForUser(Ljava/lang/String;[Ljava/lang/String;J)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->purchasesCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method

.method private queryWishlist()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->wishlistStore:Lcom/google/android/youtube/videos/store/WishlistStore;

    new-instance v1, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->wishlistCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/store/WishlistStore;->loadWishlist(Lcom/google/android/youtube/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/youtube/videos/store/StoreStatusMonitor$Listener;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->listeners:Ljava/util/List;

    const-string v1, "listener cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getStatus(Ljava/lang/String;I)I
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    if-nez v4, :cond_0

    :goto_0
    return v2

    :cond_0
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->queryPurchases()V

    move v2, v3

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->handler:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->queryWishlist()V

    move v2, v3

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/Set;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->mergeQueryResult(Ljava/util/Set;I)V

    move v2, v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public init(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->reset()V

    const-string v0, "account cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->update()V

    return-void
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onPurchasesUpdated()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->queryPurchases()V

    return-void
.end method

.method public onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->queryPurchases()V

    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->queryPurchases()V

    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->queryWishlist()V

    return-void
.end method

.method public removeListener(Lcom/google/android/youtube/videos/store/StoreStatusMonitor$Listener;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->listeners:Ljava/util/List;

    const-string v1, "listener cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->account:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->removeListener(Lcom/google/android/youtube/videos/store/Database$Listener;)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->statuses:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->hasChanges:Z

    :cond_0
    return-void
.end method

.method public update()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->queryPurchases()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;->queryWishlist()V

    return-void
.end method
