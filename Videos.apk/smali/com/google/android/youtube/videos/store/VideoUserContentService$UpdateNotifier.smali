.class public Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;
.super Ljava/lang/Object;
.source "VideoUserContentService.java"

# interfaces
.implements Lcom/google/android/youtube/videos/store/Database$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/VideoUserContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateNotifier"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final notifyIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;->context:Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.play.CONTENT_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "Play.DataType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Play.BackendId"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;->notifyIntent:Landroid/content/Intent;

    return-void
.end method

.method private notifyContentChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;->notifyIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;->notifyContentChanged()V

    return-void
.end method

.method public onPurchasesUpdated()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;->notifyContentChanged()V

    return-void
.end method

.method public onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;->notifyContentChanged()V

    return-void
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;->notifyContentChanged()V

    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    return-void
.end method
