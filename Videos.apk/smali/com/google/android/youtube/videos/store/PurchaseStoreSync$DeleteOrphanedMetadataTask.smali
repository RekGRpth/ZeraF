.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteOrphanedMetadataTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V
    .locals 0
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    return-void
.end method

.method private removeOrphanedPosters()V
    .locals 11

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "posters"

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->POSTERS_VIDEO_ID_COLUMN:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$2000()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "NOT EXISTS (SELECT NULL FROM videos WHERE video_id = poster_video_id)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$RemovePosterTask;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->priority:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    const/4 v4, 0x0

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->videoPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;
    invoke-static {v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$2100(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/FileStore;

    move-result-object v5

    const-string v6, "videos"

    const-string v7, "video_id"

    const-string v8, "posters"

    const-string v9, "poster_video_id"

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$RemovePosterTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$RemovePosterTask;->schedule()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "show_posters"

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->SHOW_POSTERS_SHOW_ID_COLUMN:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$2200()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "NOT EXISTS (SELECT NULL FROM seasons WHERE show_id = poster_show_id)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :goto_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$RemovePosterTask;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->priority:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    const/4 v4, 0x0

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->showPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;
    invoke-static {v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$2300(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/FileStore;

    move-result-object v5

    const-string v6, "seasons"

    const-string v7, "show_id"

    const-string v8, "show_posters"

    const-string v9, "poster_show_id"

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$RemovePosterTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$RemovePosterTask;->schedule()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 7

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    const-string v2, "seasons"

    const-string v3, "NOT EXISTS (SELECT NULL FROM purchases WHERE item_type = 2 AND item_id = season_id) AND NOT EXISTS (SELECT NULL FROM videos, purchases ON season_id = episode_season_id AND item_type = 1 AND item_id = video_id)"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "videos"

    const-string v3, "NOT EXISTS (SELECT NULL FROM purchases WHERE item_type = 1 AND item_id = video_id) AND NOT EXISTS (SELECT NULL FROM seasons WHERE season_id = episode_season_id)"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v0, v5, v3}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->removeOrphanedPosters()V

    new-instance v2, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->priority:I

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    :cond_0
    return-void

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0, v5, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->removeOrphanedPosters()V

    new-instance v3, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->priority:I

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    :cond_1
    throw v2
.end method
