.class public Lcom/google/android/youtube/videos/store/WishlistStoreSync;
.super Ljava/lang/Object;
.source "WishlistStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;,
        Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;
    }
.end annotation


# static fields
.field private static final ACCOUNT_COLUMN:[Ljava/lang/String;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final executor:Ljava/util/concurrent/Executor;

.field private final wishlistRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/WishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "wishlist_account"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->ACCOUNT_COLUMN:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p3    # Lcom/google/android/youtube/videos/store/Database;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/videos/store/Database;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/WishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->executor:Ljava/util/concurrent/Executor;

    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v0, "database cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-static {p4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->wishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->wishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/store/Database;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->ACCOUNT_COLUMN:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/store/WishlistStoreSync;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method


# virtual methods
.method public cleanup(ILcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/core/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync$CleanupTask;-><init>(Lcom/google/android/youtube/videos/store/WishlistStoreSync;ILcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public syncWishlist(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<-",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync$SyncWishlistTask;-><init>(Lcom/google/android/youtube/videos/store/WishlistStoreSync;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
