.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnpinUnneededDownloadsTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private final whereArgs:[Ljava/lang/String;

.field private final whereClause:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V
    .locals 1
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    const-string v0, "pinned AND (hidden OR status IS NULL OR status != 2)"

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereClause:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereArgs:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 3
    .param p2    # I
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    const-string v0, "pinning_account = ? AND (pinned AND (hidden OR status IS NULL OR status != 2))"

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereClause:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v0, "account cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v1, v2

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereArgs:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 12

    const/4 v5, 0x0

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "(video_userdata LEFT JOIN videos ON pinning_video_id = video_id) LEFT JOIN purchases ON account = pinning_account AND item_id = (CASE item_type WHEN 1 THEN video_id WHEN 2 THEN episode_season_id END)"

    sget-object v2, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$ShouldUnpinQuery;->PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereClause:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->whereArgs:[Ljava/lang/String;

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v8, v11, v1}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->setPinned(Lcom/google/android/youtube/videos/store/Database;Ljava/lang/String;Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    or-int/2addr v10, v0

    goto :goto_0

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    if-eqz v10, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$2400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$2400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    if-eqz v10, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$2400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$2400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_2
    throw v0
.end method
