.class interface abstract Lcom/google/android/youtube/videos/store/PurchaseStoreSync$MissingVideoPosterQuery;
.super Ljava/lang/Object;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "MissingVideoPosterQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "poster_uri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$MissingVideoPosterQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
