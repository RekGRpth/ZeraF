.class Lcom/google/android/youtube/videos/store/PosterStore$3;
.super Ljava/lang/Object;
.source "PosterStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/PosterStore;->getPosterBytes(Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PosterStore;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$fileStore:Lcom/google/android/youtube/videos/store/FileStore;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->this$0:Lcom/google/android/youtube/videos/store/PosterStore;

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$fileStore:Lcom/google/android/youtube/videos/store/FileStore;

    iput-object p3, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$fileName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$fileStore:Lcom/google/android/youtube/videos/store/FileStore;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$fileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/store/FileStore;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$fileName:Ljava/lang/String;

    new-instance v4, Lcom/google/android/youtube/videos/store/PosterStore$NoStoredPosterException;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$fileName:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/google/android/youtube/videos/store/PosterStore$NoStoredPosterException;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$fileName:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$3;->val$fileName:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
