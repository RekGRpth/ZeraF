.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncVideoMetadataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/youtube/core/model/Video;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 1
    .param p2    # I
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    const-string v0, "videoId cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->videoId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncVideoRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$700(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->videoId:Ljava/lang/String;

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->maybeReportRequiredDataLevelError(Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->onResponse(Ljava/lang/String;Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Lcom/google/android/youtube/core/model/Video;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/core/model/Video;

    const/4 v8, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Video;->movie:Lcom/google/android/youtube/core/model/Video$Movie;

    if-nez v3, :cond_0

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Video "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " contains neither movie nor episode metadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->useHqThumbnails:Z
    invoke-static {v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$800(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Z

    move-result v5

    invoke-static {p2, v3, v4, v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->buildVideoContentValues(Lcom/google/android/youtube/core/model/Video;JZ)Landroid/content/ContentValues;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    const-string v3, "videos"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v3, v1, v0, v8, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    if-eqz v3, :cond_1

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Video$Episode;->seasonUri:Landroid/net/Uri;

    if-eqz v3, :cond_1

    new-instance v3, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->priority:I

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-object v7, p2, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    iget-object v7, v7, Lcom/google/android/youtube/core/model/Video$Episode;->seasonUri:Landroid/net/Uri;

    invoke-static {v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->schedule()V

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->priority:I

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-object v6, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncVideoPoster(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    invoke-static {v3, v4, v5, v6}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$900(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v4, v1, v0, v8, v5}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method
