.class public Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
.super Ljava/lang/Object;
.source "PurchaseStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PurchaseRequest"
.end annotation


# instance fields
.field public final account:Ljava/lang/String;

.field private final cancellationSignalHolder:Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

.field private final columns:[Ljava/lang/String;

.field private final distinct:Z

.field private final groupClause:Ljava/lang/String;

.field private final limit:I

.field private final orderClause:Ljava/lang/String;

.field private final tables:Ljava/lang/String;

.field private final whereArgs:[Ljava/lang/String;

.field private final whereClause:Ljava/lang/String;


# direct methods
.method constructor <init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 11
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # [Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # I

    const/4 v10, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)V

    return-void
.end method

.method constructor <init>(ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)V
    .locals 4
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # [Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # I
    .param p10    # Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "whereClause cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account cannot be empty"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->account:Ljava/lang/String;

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->distinct:Z

    const-string v1, "tables cannot be empty"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->tables:Ljava/lang/String;

    const-string v1, "columns cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->columns:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "account = ? AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereClause:Ljava/lang/String;

    if-nez p6, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    aput-object p3, v1, v3

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;

    :cond_0
    iput-object p7, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->groupClause:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->orderClause:Ljava/lang/String;

    iput p9, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->limit:I

    iput-object p10, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->cancellationSignalHolder:Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    return-void

    :cond_1
    array-length v1, p6

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;

    aput-object p3, v1, v3

    const/4 v0, 0x0

    :goto_0
    array-length v1, p6

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    aget-object v3, p6, v0

    aput-object v3, v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # I

    const/4 v9, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)V

    return-void
.end method

.method constructor <init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)V
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # I
    .param p9    # Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->account:Ljava/lang/String;

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->distinct:Z

    const-string v0, "tables cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->tables:Ljava/lang/String;

    const-string v0, "columns cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->columns:[Ljava/lang/String;

    const-string v0, "whereClause cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereClause:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->groupClause:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->orderClause:Ljava/lang/String;

    iput p8, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->limit:I

    iput-object p9, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->cancellationSignalHolder:Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->distinct:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->tables:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->columns:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereClause:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->groupClause:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->orderClause:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->limit:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/youtube/videos/store/CancellationSignalHolder;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->cancellationSignalHolder:Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    return-object v0
.end method
