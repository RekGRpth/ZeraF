.class public Lcom/google/android/youtube/videos/store/OfflineSubtitles;
.super Lcom/google/android/youtube/core/client/BaseClient;
.source "OfflineSubtitles.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final storingSubtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

.field private final subtitleTracksRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;>;"
        }
    .end annotation
.end field

.field private final subtitleTracksStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;>;"
        }
    .end annotation
.end field

.field private final subtitleTracksStorer:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;>;"
        }
    .end annotation
.end field

.field private final subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

.field private final subtitlesRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            "Lcom/google/android/youtube/core/model/Subtitles;",
            ">;"
        }
    .end annotation
.end field

.field private final subtitlesStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            "Lcom/google/android/youtube/core/model/Subtitles;",
            ">;"
        }
    .end annotation
.end field

.field private final subtitlesStorer:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            "Lcom/google/android/youtube/core/model/Subtitles;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/XmlParser;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Lorg/apache/http/client/HttpClient;
    .param p4    # Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/youtube/core/client/BaseClient;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/XmlParser;)V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->getSubtitleTracksStore()Lcom/google/android/youtube/videos/store/AbstractFileStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitleTracksStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->createSubtitleTracksStorer()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitleTracksStorer:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->createSubtitleTracksRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitleTracksRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->getSubtitlesStore()Lcom/google/android/youtube/videos/store/AbstractFileStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->createSubtitlesStorer()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesStorer:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->createSubtitlesRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->createSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->createStoringSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->storingSubtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitleTracksRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesStorer:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/OfflineSubtitles;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitleTracksStorer:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/OfflineSubtitles;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/utils/OfflineUtil$MediaNotMountedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->getSubtitlesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private createStoringSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/store/OfflineSubtitles$2;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles$2;-><init>(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)V

    return-object v0
.end method

.method private createSubtitleTracksRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;>;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/core/converter/http/SubtitleTracksConverter;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/SubtitleTracksConverter;-><init>()V

    invoke-virtual {p0, v1, v1}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitleTracksStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    invoke-static {v5}, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->create(Lcom/google/android/youtube/videos/store/AbstractFileStore;)Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    move-result-object v4

    new-instance v2, Lcom/google/android/youtube/core/async/FallbackRequester;

    invoke-direct {v2, v4, v3}, Lcom/google/android/youtube/core/async/FallbackRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    return-object v0
.end method

.method private createSubtitleTracksStorer()Lcom/google/android/youtube/core/async/Requester;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;>;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/core/converter/http/SubtitleTracksConverter;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/SubtitleTracksConverter;-><init>()V

    invoke-virtual {p0, v1, v1}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitleTracksStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    invoke-static {v4, v2}, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->create(Lcom/google/android/youtube/videos/store/AbstractFileStore;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    return-object v0
.end method

.method private createSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/store/OfflineSubtitles$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles$1;-><init>(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)V

    return-object v0
.end method

.method private createSubtitlesRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            "Lcom/google/android/youtube/core/model/Subtitles;",
            ">;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {v1, v5}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    invoke-virtual {p0, v1, v1}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    invoke-static {v5}, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->create(Lcom/google/android/youtube/videos/store/AbstractFileStore;)Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    move-result-object v4

    new-instance v2, Lcom/google/android/youtube/core/async/FallbackRequester;

    invoke-direct {v2, v4, v3}, Lcom/google/android/youtube/core/async/FallbackRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    return-object v0
.end method

.method private createSubtitlesStorer()Lcom/google/android/youtube/core/async/Requester;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            "Lcom/google/android/youtube/core/model/Subtitles;",
            ">;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {v1, v4}, Lcom/google/android/youtube/core/converter/http/SubtitlesConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    invoke-virtual {p0, v1, v1}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesStore:Lcom/google/android/youtube/videos/store/AbstractFileStore;

    invoke-static {v4, v2}, Lcom/google/android/youtube/videos/async/StoreCachingRequester;->create(Lcom/google/android/youtube/videos/store/AbstractFileStore;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/videos/async/StoreCachingRequester;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    return-object v0
.end method

.method private getSubtitleTracksStore()Lcom/google/android/youtube/videos/store/AbstractFileStore;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/store/OfflineSubtitles$3;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles$3;-><init>(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)V

    return-object v0
.end method

.method private getSubtitlesDir()Ljava/io/File;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/utils/OfflineUtil$MediaNotMountedException;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->getRootFilesDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const-string v2, "subtitles"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private getSubtitlesStore()Lcom/google/android/youtube/videos/store/AbstractFileStore;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/videos/store/AbstractFileStore",
            "<",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            "Lcom/google/android/youtube/core/model/Subtitles;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/store/OfflineSubtitles$4;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/store/OfflineSubtitles$4;-><init>(Lcom/google/android/youtube/videos/store/OfflineSubtitles;)V

    return-object v0
.end method


# virtual methods
.method public getStoringSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->storingSubtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    return-object v0
.end method

.method public getSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/OfflineSubtitles;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    return-object v0
.end method
