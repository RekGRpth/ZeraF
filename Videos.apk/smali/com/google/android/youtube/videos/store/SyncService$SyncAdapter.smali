.class Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/SyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/SyncService;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/SyncService;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/youtube/videos/store/SyncService;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/ContentProviderClient;
    .param p5    # Landroid/content/SyncResult;

    if-eqz p2, :cond_0

    const-string v1, "initialize"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/google/android/youtube/videos/store/SyncService;->enableSyncIfAccountUninitialized(Landroid/accounts/Account;)Z
    invoke-static {p1}, Lcom/google/android/youtube/videos/store/SyncService;->access$000(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/youtube/videos/store/SyncService;

    # getter for: Lcom/google/android/youtube/videos/store/SyncService;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/SyncService;->access$100(Lcom/google/android/youtube/videos/store/SyncService;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->blockingGetUserAuth(Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cannot sync unauthenticated account: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "video"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/youtube/videos/store/SyncService;

    const-string v2, "video"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/youtube/videos/store/SyncService;->syncVideoPurchase(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V
    invoke-static {v1, v0, v2, p5}, Lcom/google/android/youtube/videos/store/SyncService;->access$200(Lcom/google/android/youtube/videos/store/SyncService;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V

    goto :goto_0

    :cond_2
    const-string v1, "season"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/youtube/videos/store/SyncService;

    const-string v2, "season"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/youtube/videos/store/SyncService;->syncSeasonPurchase(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V
    invoke-static {v1, v0, v2, p5}, Lcom/google/android/youtube/videos/store/SyncService;->access$300(Lcom/google/android/youtube/videos/store/SyncService;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;Landroid/content/SyncResult;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/videos/store/SyncService$SyncAdapter;->this$0:Lcom/google/android/youtube/videos/store/SyncService;

    # invokes: Lcom/google/android/youtube/videos/store/SyncService;->syncAllPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;Landroid/content/SyncResult;)V
    invoke-static {v1, v0, p5}, Lcom/google/android/youtube/videos/store/SyncService;->access$400(Lcom/google/android/youtube/videos/store/SyncService;Lcom/google/android/youtube/videos/accounts/UserAuth;Landroid/content/SyncResult;)V

    goto :goto_0
.end method
