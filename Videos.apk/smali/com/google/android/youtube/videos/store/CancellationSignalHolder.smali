.class public Lcom/google/android/youtube/videos/store/CancellationSignalHolder;
.super Ljava/lang/Object;
.source "CancellationSignalHolder.java"


# instance fields
.field public final cancellationSignal:Landroid/os/CancellationSignal;


# direct methods
.method public constructor <init>(Landroid/os/CancellationSignal;)V
    .locals 1
    .param p1    # Landroid/os/CancellationSignal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/CancellationSignal;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/CancellationSignalHolder;->cancellationSignal:Landroid/os/CancellationSignal;

    return-void
.end method
