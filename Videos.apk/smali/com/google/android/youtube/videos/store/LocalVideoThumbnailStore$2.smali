.class Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;
.super Ljava/lang/Object;
.source "LocalVideoThumbnailStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->request(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

.field final synthetic val$bitmapCacheKey:Ljava/lang/String;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    iput-object p3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p4, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$bitmapCacheKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    iget-wide v4, v4, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->id:J

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    iget-wide v6, v6, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->modifiedTimestamp:J

    # invokes: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->toFile(JJ)Ljava/io/File;
    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$200(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;JJ)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$bitmapCacheKey:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->fetchThumbnailFromMediaStore(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v3, v4, v5, v2, v6}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$400(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/io/File;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    # invokes: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->getStoredBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;
    invoke-static {v3, v2}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$500(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    # getter for: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$600(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$bitmapCacheKey:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    invoke-interface {v3, v4, v0}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to read poster data for video "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$bitmapCacheKey:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->fetchThumbnailFromMediaStore(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v3, v4, v5, v2, v6}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$400(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to decode poster data for video "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$request:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$2;->val$bitmapCacheKey:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->fetchThumbnailFromMediaStore(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v3, v4, v5, v2, v6}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$400(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;Lcom/google/android/youtube/core/async/Callback;Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method
