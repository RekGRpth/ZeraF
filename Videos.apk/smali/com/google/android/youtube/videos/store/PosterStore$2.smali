.class Lcom/google/android/youtube/videos/store/PosterStore$2;
.super Ljava/lang/Object;
.source "PosterStore.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/PosterStore;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PosterStore;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/PosterStore;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PosterStore$2;->this$0:Lcom/google/android/youtube/videos/store/PosterStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PosterStore$2;->request(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public request(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PosterStore$2;->this$0:Lcom/google/android/youtube/videos/store/PosterStore;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/store/PosterStore;->getShowPoster(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
