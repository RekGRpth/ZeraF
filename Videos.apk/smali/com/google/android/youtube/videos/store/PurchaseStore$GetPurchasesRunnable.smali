.class Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;
.super Ljava/lang/Object;
.source "PurchaseStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetPurchasesRunnable"
.end annotation


# instance fields
.field protected final callback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field protected final db:Landroid/database/sqlite/SQLiteDatabase;

.field protected final request:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

.field protected final sql:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 8
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;->db:Landroid/database/sqlite/SQLiteDatabase;

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;->request:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iput-object p3, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;->callback:Lcom/google/android/youtube/core/async/Callback;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->distinct:Z
    invoke-static {p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$000(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Z

    move-result v0

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->tables:Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$100(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->columns:[Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$200(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)[Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereClause:Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$300(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->groupClause:Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;

    move-result-object v4

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->orderClause:Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$500(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->limit:I
    invoke-static {p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$600(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)I

    move-result v7

    if-lez v7, :cond_0

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->limit:I
    invoke-static {p2}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$600(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    :goto_0
    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;->sql:Ljava/lang/String;

    return-void

    :cond_0
    move-object v7, v5

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;->callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;->request:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;->db:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;->sql:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStore$GetPurchasesRunnable;->request:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;->access$700(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
