.class Lcom/google/android/youtube/videos/store/PosterStore$4;
.super Ljava/lang/Object;
.source "PosterStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/PosterStore;->getPoster(Ljava/lang/String;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PosterStore;

.field final synthetic val$bitmapCacheKey:Ljava/lang/String;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$fileStore:Lcom/google/android/youtube/videos/store/FileStore;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->this$0:Lcom/google/android/youtube/videos/store/PosterStore;

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$fileStore:Lcom/google/android/youtube/videos/store/FileStore;

    iput-object p3, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$fileName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p5, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$bitmapCacheKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    :try_start_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$fileStore:Lcom/google/android/youtube/videos/store/FileStore;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$fileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/videos/store/FileStore;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$fileName:Ljava/lang/String;

    new-instance v5, Lcom/google/android/youtube/videos/store/PosterStore$NoStoredPosterException;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$fileName:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/google/android/youtube/videos/store/PosterStore$NoStoredPosterException;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4, v5}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->this$0:Lcom/google/android/youtube/videos/store/PosterStore;

    # getter for: Lcom/google/android/youtube/videos/store/PosterStore;->bitmapConverter:Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/PosterStore;->access$000(Lcom/google/android/youtube/videos/store/PosterStore;)Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;->convertResponse([B)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->this$0:Lcom/google/android/youtube/videos/store/PosterStore;

    # getter for: Lcom/google/android/youtube/videos/store/PosterStore;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/PosterStore;->access$100(Lcom/google/android/youtube/videos/store/PosterStore;)Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$bitmapCacheKey:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$fileName:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$fileName:Ljava/lang/String;

    invoke-interface {v3, v4, v1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PosterStore$4;->val$fileName:Ljava/lang/String;

    invoke-interface {v3, v4, v1}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
