.class public interface abstract Lcom/google/android/youtube/videos/store/Database$Listener;
.super Ljava/lang/Object;
.source "Database.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/Database;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onPosterUpdated(Ljava/lang/String;)V
.end method

.method public abstract onPurchasesUpdated()V
.end method

.method public abstract onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onShowPosterUpdated(Ljava/lang/String;)V
.end method

.method public abstract onVideoMetadataUpdated(Ljava/lang/String;)V
.end method

.method public abstract onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onWishlistUpdated()V
.end method
