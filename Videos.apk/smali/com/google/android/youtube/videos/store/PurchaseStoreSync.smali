.class public Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.super Ljava/lang/Object;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$2;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$RemovePosterTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$ShouldUnpinQuery;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$MissingShowPosterQuery;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$MissingVideoPosterQuery;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SeasonMetadataQuery;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$VideoMetadataQuery;,
        Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;
    }
.end annotation


# static fields
.field private static final ACCOUNT_COLUMN:[Ljava/lang/String;

.field private static final POSTERS_VIDEO_ID_COLUMN:[Ljava/lang/String;

.field private static final SHOW_POSTERS_SHOW_ID_COLUMN:[Ljava/lang/String;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final config:Lcom/google/android/youtube/videos/Config;

.field private final context:Landroid/content/Context;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final executor:Ljava/util/concurrent/Executor;

.field private final nextTicket:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final showPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

.field private final syncEpisodesRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation
.end field

.field private final syncMyPurchasesRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Purchase;",
            ">;>;"
        }
    .end annotation
.end field

.field private final syncPosterArtRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation
.end field

.field private final syncSeasonRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Season;",
            ">;"
        }
    .end annotation
.end field

.field private final syncShowRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;"
        }
    .end annotation
.end field

.field private final syncVideoRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation
.end field

.field private final useHqThumbnails:Z

.field private final videoPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "account"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->ACCOUNT_COLUMN:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "poster_video_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->POSTERS_VIDEO_ID_COLUMN:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "poster_show_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->SHOW_POSTERS_SHOW_ID_COLUMN:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/videos/store/FileStore;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;ZLcom/google/android/youtube/videos/Config;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p4    # Lcom/google/android/youtube/videos/store/Database;
    .param p5    # Lcom/google/android/youtube/videos/store/FileStore;
    .param p6    # Lcom/google/android/youtube/videos/store/FileStore;
    .param p13    # Z
    .param p14    # Lcom/google/android/youtube/videos/Config;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/videos/store/Database;",
            "Lcom/google/android/youtube/videos/store/FileStore;",
            "Lcom/google/android/youtube/videos/store/FileStore;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Season;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Purchase;",
            ">;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;Z",
            "Lcom/google/android/youtube/videos/Config;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "context cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;

    const-string v1, "preferences cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "accountManagerWrapper cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v1, "database cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/Database;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v1, "videoPosterFileStore cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/FileStore;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->videoPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    const-string v1, "showPosterFileStore cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/FileStore;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->showPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    const-string v1, "syncVideoRequester cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/Requester;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncVideoRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "syncSeasonRequester cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/Requester;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncSeasonRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "syncShowRequester cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/Requester;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncShowRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "syncPosterArtRequester cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/Requester;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPosterArtRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "syncMyPurchasesRequester cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/Requester;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncMyPurchasesRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "syncEpisodesRequester cannot be null"

    move-object/from16 v0, p12

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/Requester;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncEpisodesRequester:Lcom/google/android/youtube/core/async/Requester;

    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->useHqThumbnails:Z

    const-string v1, "config cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/Config;

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->config:Lcom/google/android/youtube/videos/Config;

    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x5

    const/4 v3, 0x5

    const-wide/16 v4, 0x1e

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    new-instance v8, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;

    const/16 v9, 0xa

    invoke-direct {v8, v9}, Lcom/google/android/youtube/core/utils/PriorityThreadFactory;-><init>(I)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->nextTicket:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->nextTicket:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncSeasonRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncShowRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncShowPoster(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncEpisodesRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPosterArtRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$1800()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->ACCOUNT_COLUMN:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->executor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$2000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->POSTERS_VIDEO_ID_COLUMN:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/FileStore;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->videoPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    return-object v0
.end method

.method static synthetic access$2200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->SHOW_POSTERS_SHOW_ID_COLUMN:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/FileStore;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->showPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncMyPurchasesRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncVideo(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Z)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncSeason(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncVideoRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->useHqThumbnails:Z

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncVideoPoster(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    return-void
.end method

.method private maybeSyncEpisodePosters(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "NOT poster_synced AND episode_season_id = ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncVideoPosters(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private maybeSyncSeason(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Z)V
    .locals 19
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    const/16 v18, 0x0

    const-wide/16 v16, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "seasons"

    sget-object v6, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SeasonMetadataQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "season_id = ?"

    const/4 v9, 0x1

    new-array v8, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p3, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    :try_start_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v7

    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    if-eqz v18, :cond_1

    new-instance v4, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncSeasonMetadataTask;->schedule()V

    :goto_1
    return-void

    :cond_0
    const/16 v18, 0x1

    move-wide/from16 v7, v16

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_1
    if-nez p4, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/Config;->resyncSeasonEpisodesAfterMillis()J

    move-result-wide v9

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->shouldResync(JJJ)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    new-instance v9, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;

    move-object/from16 v10, p0

    move/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    invoke-direct/range {v9 .. v14}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->schedule()V

    :goto_2
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v14}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncShowPoster(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-direct/range {p0 .. p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncEpisodePosters(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private maybeSyncShowPoster(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 11
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "seasons"

    sget-object v2, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$MissingShowPosterQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "NOT show_poster_synced AND show_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p3, v4, v6

    const-string v8, "1"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {p0, p1, p2, p3, v10}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->scheduleSyncShowPosterTask(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private maybeSyncVideo(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 12
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "videos"

    sget-object v2, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$VideoMetadataQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "video_id = ?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object p3, v4, v11

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->resyncVideoAfterMillis()J

    move-result-wide v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->shouldResync(JJJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    if-eqz v10, :cond_1

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncVideoMetadataTask;->schedule()V

    :goto_1
    return-void

    :cond_0
    const/4 v10, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1, p2, v9, v11}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncSeason(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Z)V

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncVideoPoster(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private maybeSyncVideoPoster(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "NOT poster_synced AND video_id = ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncVideoPosters(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private maybeSyncVideoPosters(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 11
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "videos"

    sget-object v2, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$MissingVideoPosterQuery;->PROJECTION:[Ljava/lang/String;

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {p0, p1, p2, v10, v9}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->scheduleSyncVideoPosterTask(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private scheduleSyncShowPosterTask(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 13
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->showPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    const-string v7, "seasons"

    const-string v8, "show_poster_synced"

    const-string v9, "show_id"

    const-string v10, "show_posters"

    const-string v11, "poster_show_id"

    const/4 v12, 0x7

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->schedule()V

    return-void
.end method

.method private scheduleSyncVideoPosterTask(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 13
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->videoPosterFileStore:Lcom/google/android/youtube/videos/store/FileStore;

    const-string v7, "videos"

    const-string v8, "poster_synced"

    const-string v9, "video_id"

    const-string v10, "posters"

    const-string v11, "poster_video_id"

    const/4 v12, 0x2

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/videos/store/FileStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPosterTask;->schedule()V

    return-void
.end method

.method private shouldResync(JJJ)Z
    .locals 2
    .param p1    # J
    .param p3    # J
    .param p5    # J

    cmp-long v0, p3, p1

    if-gtz v0, :cond_0

    add-long v0, p3, p5

    cmp-long v0, v0, p1

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cleanup()V
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->schedule()V

    return-void
.end method

.method public cleanup(ILcom/google/android/youtube/core/async/Callback;)V
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->create(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->schedule()V

    return-void
.end method

.method public setHiddenStateForMovie(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v6, "account = ? AND item_type = 1 AND item_id = ? AND EXISTS (SELECT NULL FROM videos WHERE video_id = item_id AND episode_season_id IS NULL)"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->schedule()V

    return-void
.end method

.method public setHiddenStateForShow(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v6, "account = ? AND CASE item_type WHEN 2 THEN EXISTS (SELECT NULL FROM seasons WHERE season_id = item_id AND show_id = :itemid) WHEN 1 THEN EXISTS (SELECT NULL FROM videos,seasons WHERE episode_season_id = season_id AND video_id = item_id AND show_id = :itemid) END"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SetHiddenStateTask;->schedule()V

    return-void
.end method

.method public syncPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;ZLcom/google/android/youtube/core/async/Callback;)V
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Z",
            "Lcom/google/android/youtube/core/async/Callback",
            "<-",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;

    const/4 v2, 0x2

    new-instance v1, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;

    invoke-direct {v1, p0, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$1;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-static {p1, v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->create(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->schedule()V

    return-void
.end method

.method public syncPurchasesForSeason(Landroid/util/Pair;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<-",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;

    const/4 v2, 0x1

    invoke-static {p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->create(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    move-result-object v3

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v4, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-static {v1, v4, v5}, Lcom/google/android/youtube/videos/async/GDataRequests;->getMySeasonPurchasesRequest(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Lcom/google/android/youtube/core/async/GDataRequest;Z)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesTask;->schedule()V

    return-void
.end method

.method public syncPurchasesForVideo(Landroid/util/Pair;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<-",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;

    const/4 v2, 0x1

    invoke-static {p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;->create(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    move-result-object v3

    iget-object v4, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v5, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncPurchasesForVideoTask;->schedule()V

    return-void
.end method
