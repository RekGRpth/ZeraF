.class public Lcom/google/android/youtube/videos/store/VideoUserContentService;
.super Landroid/app/Service;
.source "VideoUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/store/VideoUserContentService$VideoUserContentBinder;,
        Lcom/google/android/youtube/videos/store/VideoUserContentService$UpdateNotifier;
    }
.end annotation


# instance fields
.field private binder:Lcom/google/android/youtube/videos/store/VideoUserContentService$VideoUserContentBinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/VideoUserContentService;->binder:Lcom/google/android/youtube/videos/store/VideoUserContentService$VideoUserContentBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcom/google/android/youtube/videos/store/VideoUserContentService$VideoUserContentBinder;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/store/VideoUserContentService$VideoUserContentBinder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/VideoUserContentService;->binder:Lcom/google/android/youtube/videos/store/VideoUserContentService$VideoUserContentBinder;

    return-void
.end method
