.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CleanupTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V
    .locals 0
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    return-void
.end method

.method private removeOldPurchases(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 9
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/32 v7, 0x5265c00

    sub-long v1, v5, v7

    const-string v5, "purchases"

    const-string v6, "status != 2 AND status != 1 AND (expiration_timestamp IS NULL OR expiration_timestamp < ?)"

    new-array v7, v3, [Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {p1, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method private removePurchasesForUnknownAccounts(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 18
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    const-string v3, "purchases"

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->ACCOUNT_COLUMN:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$1800()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "account"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    :goto_0
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$1900(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v15

    const/4 v13, 0x0

    :goto_1
    array-length v2, v15

    if-ge v13, v2, :cond_1

    aget-object v2, v15, v13

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v11, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_1
    const/16 v16, 0x0

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v2, "purchases"

    const-string v3, "account = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    add-int v16, v16, v2

    goto :goto_2

    :cond_2
    if-lez v16, :cond_3

    const/4 v2, 0x1

    :goto_3
    return v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_3
.end method


# virtual methods
.method protected doSync()V
    .locals 8

    const/4 v3, 0x4

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->removePurchasesForUnknownAccounts(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v5

    or-int/2addr v0, v5

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->removeOldPurchases(Landroid/database/sqlite/SQLiteDatabase;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    or-int/2addr v0, v5

    const/4 v1, 0x1

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v5}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v5

    if-eqz v0, :cond_1

    :goto_0
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v5, v2, v1, v3, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v1, :cond_0

    new-instance v3, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->priority:I

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->schedule()V

    new-instance v3, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->priority:I

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    :cond_0
    return-void

    :cond_1
    move v3, v4

    goto :goto_0

    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v6}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v6

    if-eqz v0, :cond_3

    :goto_1
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v6, v2, v1, v3, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-eqz v1, :cond_2

    new-instance v3, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->priority:I

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-direct {v3, v4, v6, v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$DeleteOrphanedMetadataTask;->schedule()V

    new-instance v3, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->priority:I

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$CleanupTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    invoke-direct {v3, v4, v6, v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    :cond_2
    throw v5

    :cond_3
    move v3, v4

    goto :goto_1
.end method
