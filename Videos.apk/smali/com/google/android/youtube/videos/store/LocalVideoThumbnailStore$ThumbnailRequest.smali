.class public final Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;
.super Ljava/lang/Object;
.source "LocalVideoThumbnailStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ThumbnailRequest"
.end annotation


# instance fields
.field public final id:J

.field public final modifiedTimestamp:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 0
    .param p1    # J
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->id:J

    iput-wide p3, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->modifiedTimestamp:J

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;

    iget-wide v2, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->id:J

    iget-wide v4, v0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->id:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->modifiedTimestamp:J

    iget-wide v4, v0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->modifiedTimestamp:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->id:J

    iget-wide v2, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$ThumbnailRequest;->id:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
