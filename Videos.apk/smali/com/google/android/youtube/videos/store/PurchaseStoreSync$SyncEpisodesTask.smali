.class Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;
.super Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;
.source "PurchaseStoreSync.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncEpisodesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/core/async/GDataRequest;",
        "Lcom/google/android/youtube/core/model/Page",
        "<",
        "Lcom/google/android/youtube/core/model/Video;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final request:Lcom/google/android/youtube/core/async/GDataRequest;

.field private final seasonId:Ljava/lang/String;

.field private final showId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p2    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-static {p4}, Lcom/google/android/youtube/videos/async/GDataRequests;->getSeasonEpisodesRequest(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 7
    .param p2    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    invoke-static {p6}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequest;)V
    .locals 1
    .param p2    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/youtube/core/async/GDataRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;)V

    const-string v0, "seasonId cannot be empty"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->seasonId:Ljava/lang/String;

    const-string v0, "showId cannot be empty"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->showId:Ljava/lang/String;

    const-string v0, "request cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->request:Lcom/google/android/youtube/core/async/GDataRequest;

    return-void
.end method

.method private finalizeSeason()V
    .locals 12

    const/4 v11, 0x6

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "episodes_synced"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "seasons"

    const-string v4, "season_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->seasonId:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v3

    new-array v4, v10, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->seasonId:Ljava/lang/String;

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->showId:Ljava/lang/String;

    aput-object v5, v4, v9

    invoke-virtual {v3, v1, v0, v11, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v4

    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->seasonId:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->showId:Ljava/lang/String;

    aput-object v6, v5, v9

    invoke-virtual {v4, v1, v0, v11, v5}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method


# virtual methods
.method protected doSync()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncEpisodesRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$1600(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->request:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public onError(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/core/async/GDataRequest;
    .param p2    # Ljava/lang/Exception;

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->maybeReportRequiredDataLevelError(Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->onError(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V
    .locals 15
    .param p1    # Lcom/google/android/youtube/core/async/GDataRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v14, Lcom/google/android/youtube/core/model/Video;->episode:Lcom/google/android/youtube/core/model/Video$Episode;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->useHqThumbnails:Z
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$800(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Z

    move-result v1

    invoke-static {v14, v9, v10, v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->buildVideoContentValues(Lcom/google/android/youtube/core/model/Video;JZ)Landroid/content/ContentValues;

    move-result-object v13

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    const/4 v11, 0x0

    :try_start_0
    const-string v1, "videos"

    const/4 v2, 0x0

    invoke-virtual {v12, v1, v2, v13}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const-string v1, "INSERT OR IGNORE INTO video_userdata (pinning_account, pinning_video_id) SELECT account, ? FROM purchases WHERE item_type = 2 AND item_id = ?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, v14, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->seasonId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v12, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v11, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v14, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v12, v11, v2, v3}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->priority:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-object v4, v14, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->maybeSyncVideoPoster(ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$900(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->access$400(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v12, v11, v3, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video entry "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v14, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in episode feed of season "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->seasonId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not an episode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->this$0:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget v3, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->priority:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->callback:Lcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->seasonId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->showId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;-><init>(Lcom/google/android/youtube/videos/store/PurchaseStoreSync;ILcom/google/android/youtube/videos/store/PurchaseStoreSync$BarrierCallback;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->schedule()V

    :goto_1
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->finalizeSeason()V

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncEpisodesTask;->onResponse(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    return-void
.end method
