.class Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$1;
.super Ljava/lang/Object;
.source "LocalVideoThumbnailStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->cleanup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$1;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$1;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    # getter for: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$100(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    # getter for: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->CLEANUP_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$000()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    :goto_0
    if-nez v7, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v8

    const-string v0, "Error querying the media store"

    invoke-static {v0, v8}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$1;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    # invokes: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->toFile(JJ)Ljava/io/File;
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$200(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;JJ)Ljava/io/File;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore$1;->this$0:Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;

    # getter for: Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->bitmapDirectory:Ljava/io/File;
    invoke-static {v0}, Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;->access$300(Lcom/google/android/youtube/videos/store/LocalVideoThumbnailStore;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v10

    if-eqz v10, :cond_3

    move-object v6, v10

    array-length v13, v6

    const/4 v12, 0x0

    :goto_3
    if-ge v12, v13, :cond_3

    aget-object v9, v6, v12

    invoke-interface {v11, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method
