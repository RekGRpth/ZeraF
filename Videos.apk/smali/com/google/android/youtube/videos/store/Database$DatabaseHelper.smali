.class Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "Database.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/store/Database;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/logging/EventLogger;

    const/4 v0, 0x0

    const/16 v1, 0xa

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object p1, p0, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    return-void
.end method

.method private recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Util;->wipeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p1}, Lcom/google/android/youtube/videos/store/V10;->createDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upgrading database from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    const/16 v1, 0xa

    if-ne p3, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v1, p2, p3}, Lcom/google/android/youtube/videos/logging/EventLogger;->onDatabaseUpgrade(II)V

    const/16 v1, 0x3e8

    if-lt p2, v1, :cond_1

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    packed-switch p2, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->context:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV1(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "Failed to upgrade the database"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p2, p3, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onDatabaseUpgradeError(IILjava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/store/Database$DatabaseHelper;->context:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV2(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :pswitch_2
    invoke-static {p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV3(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :pswitch_3
    invoke-static {p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV4(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :pswitch_4
    invoke-static {p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV5(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :pswitch_5
    invoke-static {p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV6(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :pswitch_6
    invoke-static {p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV7(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :pswitch_7
    invoke-static {p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV8(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :pswitch_8
    invoke-static {p1}, Lcom/google/android/youtube/videos/store/V10;->upgradeFromV9(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
