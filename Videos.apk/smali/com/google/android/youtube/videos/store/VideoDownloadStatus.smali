.class public Lcom/google/android/youtube/videos/store/VideoDownloadStatus;
.super Ljava/lang/Object;
.source "VideoDownloadStatus.java"


# instance fields
.field public final bytesDownloaded:Ljava/lang/Long;

.field public final downloadSize:Ljava/lang/Long;

.field public final drmErrorCode:Ljava/lang/Integer;

.field public final pinned:Z

.field public final pinningStatus:Ljava/lang/Integer;

.field public final pinningStatusReason:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0
    .param p1    # Z
    .param p2    # Ljava/lang/Integer;
    .param p3    # Ljava/lang/Integer;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Ljava/lang/Long;
    .param p6    # Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinned:Z

    iput-object p2, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->drmErrorCode:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    iput-object p6, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->bytesDownloaded:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public getDownloadStatusText(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1    # Landroid/content/Context;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinned:Z

    iget-object v2, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatusReason:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->drmErrorCode:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    iget-object v6, p0, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->bytesDownloaded:Ljava/lang/Long;

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/youtube/videos/pinning/PinningStatusHelper;->getDownloadStatusText(Landroid/content/Context;ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
