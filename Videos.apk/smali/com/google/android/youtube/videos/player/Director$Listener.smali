.class public interface abstract Lcom/google/android/youtube/videos/player/Director$Listener;
.super Ljava/lang/Object;
.source "Director.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/Director;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onPause()V
.end method

.method public abstract onPlay()V
.end method

.method public abstract onUserInteractionExpected()V
.end method

.method public abstract onUserInteractionNotExpected()V
.end method

.method public abstract onVideoTitle(Ljava/lang/String;)V
.end method
