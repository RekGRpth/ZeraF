.class final Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PlayerCallback"
.end annotation


# instance fields
.field private final excessiveBufferingThreshold:I

.field private final immersionHelper:Lcom/google/android/youtube/core/utils/ImmersionHelper;

.field private final playbackBeginingThresholdMillis:J

.field final synthetic this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

.field private final timeIntervalOfCountingBufferingEventsMillis:J


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Lcom/google/android/youtube/videos/Config;)V
    .locals 2
    .param p2    # Lcom/google/android/youtube/videos/Config;

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/youtube/core/utils/ImmersionHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {p1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/utils/ImmersionHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->immersionHelper:Lcom/google/android/youtube/core/utils/ImmersionHelper;

    invoke-interface {p2}, Lcom/google/android/youtube/videos/Config;->bufferingEventsBeforeQualityDrop()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->excessiveBufferingThreshold:I

    invoke-interface {p2}, Lcom/google/android/youtube/videos/Config;->bufferingWindowMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->timeIntervalOfCountingBufferingEventsMillis:J

    invoke-interface {p2}, Lcom/google/android/youtube/videos/Config;->bufferingSettleMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->playbackBeginingThresholdMillis:J

    return-void
.end method

.method private filterOldBufferingEvents()V
    .locals 9

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    const/4 v1, 0x0

    iget-object v7, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;
    invoke-static {v7}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;

    iget-wide v7, v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;->upperBound:J

    sub-long v7, v3, v7

    add-long/2addr v5, v7

    iget-wide v7, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->timeIntervalOfCountingBufferingEventsMillis:J

    cmp-long v7, v5, v7

    if-lez v7, :cond_1

    :cond_0
    :goto_1
    iget-object v7, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;
    invoke-static {v7}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-ge v1, v7, :cond_2

    iget-object v7, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;
    invoke-static {v7}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_1

    :cond_1
    iget-wide v3, v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;->lowerBound:J

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private handlePlayerError(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    const/4 v10, 0x0

    const/4 v2, 0x1

    const v9, 0x7f0a0110

    const/4 v3, 0x1

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getStream()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    const v9, 0x7f0a00ac

    const/4 v4, 0x0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2100(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2000(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z
    invoke-static {v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Z

    move-result v5

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;
    invoke-static {v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/model/VideoStreams;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    iget v5, v5, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    :goto_1
    move v6, p1

    move v7, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackError(Ljava/lang/String;ZZIIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v1

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->resumeTimeMillis:I
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$402(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;I)I

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->setStreaming(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-virtual {v0, v9, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v0, v8, v3}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->showErrorMessage(Ljava/lang/String;Z)V

    return-void

    :cond_1
    if-ne p1, v2, :cond_0

    const/4 v4, 0x1

    const v9, 0x7f0a0111

    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_AUTHENTICATION_FAILURE:I

    if-ne p2, v0, :cond_2

    const v9, 0x7f0a0080

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_STREAMING_UNAVAILABLE:I

    if-ne p2, v0, :cond_3

    const v9, 0x7f0a0089

    goto :goto_0

    :cond_3
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACK:I

    if-ne p2, v0, :cond_4

    const v9, 0x7f0a0087

    goto :goto_0

    :cond_4
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    if-ne p2, v0, :cond_5

    const v9, 0x7f0a0088

    goto :goto_0

    :cond_5
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_TERMINATE_REQUESTED:I

    if-ne p2, v0, :cond_6

    const v9, 0x7f0a0113

    goto :goto_0

    :cond_6
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    if-ne p2, v0, :cond_7

    const v9, 0x7f0a008f

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_7
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_UNUSUAL_ACTIVITY:I

    if-ne p2, v0, :cond_8

    const v9, 0x7f0a008e

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_8
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    if-ne p2, v0, :cond_9

    const v9, 0x7f0a0086

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_9
    const/16 v0, -0x7d2

    if-eq p2, v0, :cond_a

    const/16 v0, -0x7d1

    if-ne p2, v0, :cond_b

    :cond_a
    const v9, 0x7f0a0083

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_b
    const/16 v0, -0x3ec

    if-ne p2, v0, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getStream()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-nez v0, :cond_0

    const v9, 0x7f0a0113

    goto/16 :goto_0

    :cond_c
    const/16 v0, -0x3e81

    if-ne p2, v0, :cond_d

    const v9, 0x7f0a00ac

    goto/16 :goto_0

    :cond_d
    const/16 v0, -0x3ea

    if-eq p2, v0, :cond_e

    const/16 v0, -0x3eb

    if-ne p2, v0, :cond_f

    :cond_e
    const v9, 0x7f0a0112

    goto/16 :goto_0

    :cond_f
    const/16 v0, 0x20

    if-eq p2, v0, :cond_10

    const/16 v0, -0x3ed

    if-ne p2, v0, :cond_11

    :cond_10
    const v9, 0x7f0a0113

    goto/16 :goto_0

    :cond_11
    const/16 v0, -0x3f2

    if-ne p2, v0, :cond_0

    const v9, 0x7f0a0114

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_12
    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;
    invoke-static {v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/model/VideoStreams;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->lo:Lcom/google/android/youtube/core/model/Stream;

    iget v5, v5, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    goto/16 :goto_1
.end method

.method private performQualityDropIfNeeded()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->alreadyDroppedQuality:Z
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2600(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hq:Z
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/model/VideoStreams;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/youtube/core/model/VideoStreams;->supportsQualityToggle:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playStartedTimestamp:J
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2700(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)J

    move-result-wide v2

    sub-long v2, v0, v2

    iget-wide v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->playbackBeginingThresholdMillis:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/util/LinkedList;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;

    invoke-direct {v3, v0, v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->filterOldBufferingEvents()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    iget v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->excessiveBufferingThreshold:I

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->loadedStream:Lcom/google/android/youtube/core/model/Stream;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/Stream;->isHD()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0a00bc

    invoke-static {v2, v3, v7}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->alreadyDroppedQuality:Z
    invoke-static {v2, v7}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2602(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->buffering:Z
    invoke-static {v2, v6}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1702(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2100(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onQualityDropped()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # invokes: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->changeQuality(Z)V
    invoke-static {v2, v6}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)V

    goto :goto_0
.end method

.method private updateImmersion(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->immersionHelper:Lcom/google/android/youtube/core/utils/ImmersionHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/ImmersionHelper;->setImmersive(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->immersionHelper:Lcom/google/android/youtube/core/utils/ImmersionHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/ImmersionHelper;->setImmersive(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 9
    .param p1    # Landroid/os/Message;

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->updateImmersion(Landroid/os/Message;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$600(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    move-result-object v4

    sget-object v5, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->LOADED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    if-ne v4, v5, :cond_0

    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v8

    :pswitch_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$700(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->hasStartTimestampMsec()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$700(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStartTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$700(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->clearStopTimestampMsec()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    :cond_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackPlay()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->setPlaying()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->pendingLocalShortClockActivation:Z
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # invokes: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->persistLocalShortClockActivation()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1000(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->pendingLocalShortClockActivation:Z
    invoke-static {v4, v7}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$902(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z

    :cond_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1100(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1100(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->hide()V

    :cond_3
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1300(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)I

    move-result v4

    if-le v4, v1, :cond_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playedFromMillis:I
    invoke-static {v4, v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1302(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;I)I

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$700(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStopTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackSuspended()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->showPaused()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->onPlaybackStopped()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->hideSubtitles()V

    goto/16 :goto_0

    :pswitch_3
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->playbackBuilder:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$700(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStopTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackPaused()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->showPaused()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->onPlaybackStopped()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->freezeSubtitles()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v5

    # invokes: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->updateKnowledge(I)V
    invoke-static {v4, v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1600(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;I)V

    goto/16 :goto_0

    :pswitch_4
    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v0, p1, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    int-to-long v5, v2

    invoke-interface {v4, v5, v6}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackProgress(J)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setTimes(III)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->syncSubtitles(I)V

    :cond_5
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # invokes: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->updateKnowledge(I)V
    invoke-static {v4, v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1600(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;I)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    sget-object v5, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->ENDED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;
    invoke-static {v4, v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$602(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;)Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackEnded()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->onPlaybackStopped()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->hideSubtitles()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :pswitch_6
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    sget-object v5, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->ERROR:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->state:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;
    invoke-static {v4, v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$602(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;)Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1400(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/StreamingWarningHelper;->onPlaybackStopped()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->hideSubtitles()V

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->handlePlayerError(II)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->arg1:I

    int-to-long v5, v5

    invoke-interface {v4, v5, v6}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackSeek(J)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->buffering:Z
    invoke-static {v4, v8}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1702(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->setLoading()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1500(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->freezeSubtitles()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->performQualityDropIfNeeded()V

    goto/16 :goto_0

    :pswitch_9
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->buffering:Z
    invoke-static {v4, v7}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1702(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hqToggleRequested:Z
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1800(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->setPlaying()V

    :goto_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->bufferingEvents:Ljava/util/LinkedList;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1900(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iput-wide v5, v4, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$TimeRange;->upperBound:J

    goto/16 :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->showPaused()V

    goto :goto_1

    :cond_7
    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # setter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->hqToggleRequested:Z
    invoke-static {v4, v7}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$1802(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Z)Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->pause()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->play()V

    goto :goto_1

    :pswitch_a
    iget v4, p1, Landroid/os/Message;->arg1:I

    const/16 v5, 0x64

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2100(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2000(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackMediaServerDiedError(Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_8
        :pswitch_9
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method
