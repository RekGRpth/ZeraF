.class public Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;
.super Ljava/lang/Object;
.source "DownloadProgressNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;
    }
.end annotation


# instance fields
.field private final file:Ljava/io/File;

.field private final listener:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;

.field private scheduler:Ljava/util/concurrent/ScheduledExecutorService;

.field private final totalFileSize:J


# direct methods
.method public constructor <init>(Landroid/net/Uri;JLcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # J
    .param p4    # Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Uri is not a file"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->file:Ljava/io/File;

    iput-wide p2, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->totalFileSize:J

    iput-object p4, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->listener:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;)J
    .locals 2
    .param p0    # Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->updateFileSize()J

    move-result-wide v0

    return-wide v0
.end method

.method private updateFileSize()J
    .locals 8

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->totalFileSize:J

    const-wide/16 v6, 0x0

    cmp-long v0, v3, v6

    if-lez v0, :cond_1

    const-wide/16 v3, 0x64

    mul-long/2addr v3, v1

    iget-wide v6, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->totalFileSize:J

    div-long/2addr v3, v6

    long-to-int v5, v3

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->listener:Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;

    iget-wide v3, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->totalFileSize:J

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$Listener;->onDownloadProgress(JJI)V

    iget-wide v3, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->totalFileSize:J

    cmp-long v0, v1, v3

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->stop()V

    :cond_0
    return-wide v1

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCurrentFileSize()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public getPercentDownloaded()I
    .locals 4

    iget-wide v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->totalFileSize:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->getCurrentFileSize()J

    move-result-wide v0

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->totalFileSize:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized start()V
    .locals 9

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v1, :cond_1

    :goto_0
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->updateFileSize()J

    move-result-wide v7

    iget-wide v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->totalFileSize:J

    cmp-long v0, v7, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier$1;-><init>(Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
