.class Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->persistLocalShortClockActivation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

.field final synthetic val$expirationTimestamp:J

.field final synthetic val$key:Lcom/google/android/youtube/videos/pinning/DownloadKey;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;JLcom/google/android/youtube/videos/pinning/DownloadKey;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    iput-wide p2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;->val$expirationTimestamp:J

    iput-object p4, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;->val$key:Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-wide/16 v1, 0x7d0

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "license_expiration_timestamp"

    iget-wide v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;->val$expirationTimestamp:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "license_force_sync"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$100(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;->val$key:Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v1, v2, v0}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$2;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method
