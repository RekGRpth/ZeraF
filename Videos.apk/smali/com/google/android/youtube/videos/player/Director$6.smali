.class Lcom/google/android/youtube/videos/player/Director$6;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/player/Director;->initCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/player/Director;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$6;->this$0:Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$6;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # invokes: Lcom/google/android/youtube/videos/player/Director;->onNoOfflineStream()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$1400(Lcom/google/android/youtube/videos/player/Director;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$6;->onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$6;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # invokes: Lcom/google/android/youtube/videos/player/Director;->onStoredPurchaseCursor(Landroid/database/Cursor;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/player/Director;->access$1300(Lcom/google/android/youtube/videos/player/Director;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$6;->onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
