.class public Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;
.super Landroid/app/Presentation;
.source "LocalPlaybackViewHolder.java"

# interfaces
.implements Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SecondaryScreenViewHolder"
.end annotation


# instance fields
.field private playerView:Lcom/google/android/youtube/core/player/PlayerView;

.field private remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

.field private subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/Display;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/Display;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation

    const v1, 0x7f0b0022

    invoke-direct {p0, p1, p2, v1}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;I)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$InvalidDisplayException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException;

    invoke-virtual {v0}, Landroid/view/WindowManager$InvalidDisplayException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public getPlayerView()Lcom/google/android/youtube/core/player/PlayerView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    return-object v0
.end method

.method public getRemoteControllerOverlay()Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    return-object v0
.end method

.method public getSubtitlesOverlay()Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    return-object v0
.end method

.method public knowledgeEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/app/Presentation;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/player/PlayerView;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/player/PlayerView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v1, v4}, Lcom/google/android/youtube/core/player/PlayerView;->setMakeSafeForOverscan(Z)V

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    new-array v2, v4, [Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/youtube/core/player/overlay/SubtitlesOverlay;

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/PlayerView;->addOverlays([Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;)V

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->MOVIE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    new-array v2, v4, [Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->remoteControllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/PlayerView;->addOverlays([Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public release()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->cancel()V

    return-void
.end method
