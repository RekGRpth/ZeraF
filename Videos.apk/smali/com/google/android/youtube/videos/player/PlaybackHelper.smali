.class public interface abstract Lcom/google/android/youtube/videos/player/PlaybackHelper;
.super Ljava/lang/Object;
.source "PlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException;
    }
.end annotation


# virtual methods
.method public abstract init()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation
.end method

.method public abstract isSecure()Z
.end method

.method public abstract load(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/List;Lcom/google/android/youtube/core/model/VideoStreams;IIZZJZ)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/youtube/core/model/VideoStreams;",
            "IIZZJZ)V"
        }
    .end annotation
.end method

.method public abstract onBackPressed()Z
.end method

.method public abstract onHQ()V
.end method

.method public abstract onScrubbingStart()V
.end method

.method public abstract onSeekTo(I)V
.end method

.method public abstract onVideoTitle(Ljava/lang/String;)V
.end method

.method public abstract pause()V
.end method

.method public abstract play()V
.end method

.method public abstract release(Z)V
.end method

.method public abstract reset()V
.end method

.method public abstract selectTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
.end method

.method public abstract setKeepScreenOn(Z)V
.end method

.method public abstract setLoading()V
.end method

.method public abstract showErrorMessage(Ljava/lang/String;Z)V
.end method

.method public abstract showPaused()V
.end method
