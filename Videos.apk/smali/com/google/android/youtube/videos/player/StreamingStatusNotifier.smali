.class public Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;
.super Ljava/lang/Object;
.source "StreamingStatusNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;
    }
.end annotation


# instance fields
.field private final listeners:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private streaming:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    return-void
.end method


# virtual methods
.method public declared-synchronized addListener(Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    const-string v1, "listener cannot be null"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isStreaming()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->streaming:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeListener(Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    const-string v1, "listener cannot be null"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized setStreaming(Z)V
    .locals 3
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->streaming:Z

    if-eq v2, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->streaming:Z

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->listeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;->onStreamingStatusChanged(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    monitor-exit p0

    return-void
.end method
