.class final Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DisplayRouteHolderV17"
.end annotation


# instance fields
.field private final display:Landroid/view/Display;

.field private final displayRoute:Landroid/media/MediaRouter$RouteInfo;

.field final synthetic this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1
    .param p2    # Landroid/media/MediaRouter$RouteInfo;

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "displayRoute cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->displayRoute:Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p2}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    return-void
.end method


# virtual methods
.method public initLocalPlaybackViewHolder()Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;
    invoke-static {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$3000(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$2000(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->init(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->setLoading()V

    new-instance v1, Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException;

    invoke-direct {v1, v3, v3}, Lcom/google/android/youtube/videos/player/PlaybackHelper$DisplayNotFoundException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->displayRoute:Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/media/MediaRouter$RouteInfo;->getName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->this$0:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;->access$200(Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;-><init>(Landroid/app/Activity;Landroid/view/Display;)V

    return-object v1
.end method

.method public isSecure()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public supportsProtectedBuffers()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getFlags()I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
