.class public Lcom/google/android/youtube/videos/player/DrmMediaPlayer;
.super Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;
.source "DrmMediaPlayer.java"

# interfaces
.implements Lcom/google/android/youtube/videos/drm/DrmManager$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/player/DrmMediaPlayer$1;
    }
.end annotation


# static fields
.field public static final SUPPORTED_MIME_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final appLevelDrm:Z

.field private final drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

.field private final isGoogleTv:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "video/wvm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/android/youtube/core/player/FrameworkMediaPlayer;->SUPPORTED_MIME_TYPES:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->SUPPORTED_MIME_TYPES:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;Landroid/content/Context;Lcom/google/android/youtube/videos/drm/DrmManager;Z)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p4    # Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;-><init>(Lcom/google/android/youtube/core/player/MediaPlayerInterface;)V

    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->isGoogleTv(Landroid/content/pm/PackageManager;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->isGoogleTv:Z

    iput-object p3, p0, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    iput-boolean p4, p0, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->appLevelDrm:Z

    return-void
.end method

.method private getPlayableUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 4
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->appLevelDrm:Z

    invoke-virtual {v1, p1, v2}, Lcom/google/android/youtube/videos/drm/DrmManager;->getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t obtain playable stream for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-object v0
.end method

.method private toMediaPlayerErrorCode(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_AUTHENTICATION_FAILURE:I

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACK:I

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_UNUSUAL_ACTIVITY:I

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_STREAMING_UNAVAILABLE:I

    goto :goto_0

    :pswitch_5
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    goto :goto_0

    :pswitch_6
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final onError(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z
    .locals 2
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->isGoogleTv:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, p3}, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->toMediaPlayerErrorCode(I)I

    move-result v0

    if-eqz v0, :cond_0

    move p3, v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->onError(Lcom/google/android/youtube/core/player/MediaPlayerInterface;II)Z

    move-result v1

    return v1
.end method

.method public final onHeartbeatError(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->toMediaPlayerErrorCode(I)I

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_TERMINATE_REQUESTED:I

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->notifyError(II)Z

    return-void
.end method

.method public final onPlaybackStopped(Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    const/4 v2, 0x1

    sget-object v0, Lcom/google/android/youtube/videos/player/DrmMediaPlayer$1;->$SwitchMap$com$google$android$youtube$videos$drm$DrmManager$StopReason:[I

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/16 v0, 0x1f

    invoke-virtual {p0, v2, v0}, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->notifyError(II)Z

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x20

    invoke-virtual {p0, v2, v0}, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->notifyError(II)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->getPlayableUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-super {p0, p1, v0}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method public setDataSourceV14(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->getPlayableUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-super {p0, p1, v0, p3}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->setDataSourceV14(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;

    const-string v0, "listener can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/DrmMediaPlayer;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/drm/DrmManager;->setListener(Lcom/google/android/youtube/videos/drm/DrmManager$Listener;)V

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/DelegatingMediaPlayer;->setListener(Lcom/google/android/youtube/core/player/MediaPlayerInterface$Listener;)V

    return-void
.end method
