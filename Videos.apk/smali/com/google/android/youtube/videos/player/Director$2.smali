.class Lcom/google/android/youtube/videos/player/Director$2;
.super Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
.source "Director.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/player/Director;-><init>(Lcom/google/android/youtube/videos/VideosApplication;Lcom/google/android/youtube/core/player/PlayerView;Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/Requesters;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;Lcom/google/android/youtube/videos/player/Director$Listener;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/StreamsSelector;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/videos/tagging/KnowledgeClient;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/player/Director;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$2;->this$0:Lcom/google/android/youtube/videos/player/Director;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$2;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->knowledgeOverlay:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$200(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
