.class final enum Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;
.super Ljava/lang/Enum;
.source "LocalPlaybackHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/player/LocalPlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

.field public static final enum ENDED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

.field public static final enum ERROR:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

.field public static final enum INITIALIZED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

.field public static final enum LOADED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

.field public static final enum UNINITIALIZED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->UNINITIALIZED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    new-instance v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->INITIALIZED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    new-instance v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->LOADED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    new-instance v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->ERROR:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    new-instance v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    const-string v1, "ENDED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->ENDED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->UNINITIALIZED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->INITIALIZED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->LOADED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->ERROR:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->ENDED:Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->$VALUES:[Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;
    .locals 1

    const-class v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->$VALUES:[Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    invoke-virtual {v0}, [Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/videos/player/LocalPlaybackHelper$State;

    return-object v0
.end method
