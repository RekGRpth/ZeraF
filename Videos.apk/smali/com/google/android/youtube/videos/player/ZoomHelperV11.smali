.class public Lcom/google/android/youtube/videos/player/ZoomHelperV11;
.super Ljava/lang/Object;
.source "ZoomHelperV11.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;


# instance fields
.field private final activity:Landroid/app/Activity;

.field private canZoom:Z

.field private isZoomed:Z

.field private final playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/player/PlayerSurface;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/core/player/PlayerSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

    invoke-interface {p2, p0}, Lcom/google/android/youtube/core/player/PlayerSurface;->setOnDisplayParametersChangedListener(Lcom/google/android/youtube/core/player/PlayerSurface$OnDisplayParametersChangedListener;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->shouldAllowZoom()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->canZoom:Z

    return-void
.end method

.method private onDisplayParametersChanged()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->shouldAllowZoom()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->canZoom:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->canZoom:Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method

.method private setIsZoomed(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->isZoomed:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->isZoomed:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->isZoomed:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x64

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/PlayerSurface;->setZoom(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldAllowZoom()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/PlayerSurface;->zoomSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->playerSurface:Lcom/google/android/youtube/core/player/PlayerSurface;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/PlayerSurface;->getVerticalLetterboxFraction()F

    move-result v0

    const v1, 0x3f59999a

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V
    .locals 5
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;
    .param p3    # Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p3, :cond_0

    :goto_0
    return-void

    :cond_0
    const v2, 0x7f0f0008

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v2, 0x7f0700a7

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v2, 0x7f0700a8

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->canZoom:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->isZoomed:Z

    if-nez v2, :cond_1

    if-nez p3, :cond_1

    move v2, v3

    :goto_1
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->canZoom:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->isZoomed:Z

    if-nez v2, :cond_2

    if-nez p3, :cond_2

    move v2, v3

    :goto_2
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->canZoom:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->isZoomed:Z

    if-eqz v2, :cond_3

    if-nez p3, :cond_3

    move v2, v3

    :goto_3
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->canZoom:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->isZoomed:Z

    if-eqz v2, :cond_4

    if-nez p3, :cond_4

    :goto_4
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1

    :cond_2
    move v2, v4

    goto :goto_2

    :cond_3
    move v2, v4

    goto :goto_3

    :cond_4
    move v3, v4

    goto :goto_4
.end method

.method public onLetterboxChanged(FF)V
    .locals 0
    .param p1    # F
    .param p2    # F

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->onDisplayParametersChanged()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0700a7

    if-ne v2, v3, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->setIsZoomed(Z)V

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0700a8

    if-ne v2, v3, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->setIsZoomed(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onZoomSupportedChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/player/ZoomHelperV11;->onDisplayParametersChanged()V

    return-void
.end method
