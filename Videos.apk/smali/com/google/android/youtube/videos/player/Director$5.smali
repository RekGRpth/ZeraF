.class Lcom/google/android/youtube/videos/player/Director$5;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/player/Director;->initCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Landroid/util/Pair",
        "<",
        "Lcom/google/android/youtube/videos/accounts/UserAuth;",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/player/Director;

.field final synthetic val$freshPurchaseCallback:Lcom/google/android/youtube/core/async/Callback;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/player/Director$5;->this$0:Lcom/google/android/youtube/videos/player/Director;

    iput-object p2, p0, Lcom/google/android/youtube/videos/player/Director$5;->val$freshPurchaseCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/util/Pair;Ljava/lang/Exception;)V
    .locals 1
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$5;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # invokes: Lcom/google/android/youtube/videos/player/Director;->onSyncPurchasesError(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/player/Director;->access$1100(Lcom/google/android/youtube/videos/player/Director;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Landroid/util/Pair;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$5;->onError(Landroid/util/Pair;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Landroid/util/Pair;Ljava/lang/Void;)V
    .locals 4
    .param p2    # Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Void;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/player/Director$5;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;
    invoke-static {v0}, Lcom/google/android/youtube/videos/player/Director;->access$1200(Lcom/google/android/youtube/videos/player/Director;)Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-result-object v1

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/videos/player/Director$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director$5;->this$0:Lcom/google/android/youtube/videos/player/Director;

    # getter for: Lcom/google/android/youtube/videos/player/Director;->videoId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/youtube/videos/player/Director;->access$600(Lcom/google/android/youtube/videos/player/Director;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/player/Director$5;->this$0:Lcom/google/android/youtube/videos/player/Director;

    iget-object v3, p0, Lcom/google/android/youtube/videos/player/Director$5;->val$freshPurchaseCallback:Lcom/google/android/youtube/core/async/Callback;

    # invokes: Lcom/google/android/youtube/videos/player/Director;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v2, v3}, Lcom/google/android/youtube/videos/player/Director;->access$800(Lcom/google/android/youtube/videos/player/Director;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Landroid/util/Pair;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/player/Director$5;->onResponse(Landroid/util/Pair;Ljava/lang/Void;)V

    return-void
.end method
