.class public Lcom/google/android/youtube/videos/ui/ShowsHelper;
.super Ljava/lang/Object;
.source "ShowsHelper.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Lcom/google/android/youtube/videos/NetworkMonitor$Listener;
.implements Lcom/google/android/youtube/videos/store/Database$Listener;
.implements Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;
.implements Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;
.implements Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;
.implements Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/ShowsHelper$1;,
        Lcom/google/android/youtube/videos/ui/ShowsHelper$WelcomeButtonClickListener;,
        Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;,
        Lcom/google/android/youtube/videos/ui/ShowsHelper$SuggestionClickListener;,
        Lcom/google/android/youtube/videos/ui/ShowsHelper$ShowClickListener;
    }
.end annotation


# instance fields
.field private final activity:Lvedroid/support/v4/app/FragmentActivity;

.field private final config:Lcom/google/android/youtube/videos/Config;

.field private contextMenuShowId:Ljava/lang/String;

.field private contextMenuShowTitle:Ljava/lang/String;

.field private final cursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final gdataShowSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/ui/SuggestionsHelper",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

.field private final maxSuggestions:I

.field private final networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

.field private pendingPosterUpdatedNotification:Z

.field private pendingShowsCursor:Z

.field private pendingSuggestions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation
.end field

.field private final recommendationsRequestFactory:Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

.field private final showSuggestions:Z

.field private final showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

.field private final showsOutlineHelper:Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;

.field private final statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

.field private final suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

.field private final suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/ui/SuggestionsHelper",
            "<",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private suggestionsRequested:Z

.field private final syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/Config;Lvedroid/support/v4/app/FragmentActivity;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/core/ErrorHelper;Lcom/google/android/youtube/videos/ui/SyncHelper;ZILcom/google/android/youtube/videos/Requesters;Lcom/google/android/youtube/videos/api/ApiRequesters;Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;ZLcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 20
    .param p1    # Lcom/google/android/youtube/videos/Config;
    .param p2    # Lvedroid/support/v4/app/FragmentActivity;
    .param p3    # Landroid/view/View;
    .param p4    # Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;
    .param p5    # Lcom/google/android/youtube/videos/ui/CursorHelper;
    .param p6    # Lcom/google/android/youtube/videos/store/Database;
    .param p7    # Lcom/google/android/youtube/videos/store/PosterStore;
    .param p8    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p9    # Lcom/google/android/youtube/core/ErrorHelper;
    .param p10    # Lcom/google/android/youtube/videos/ui/SyncHelper;
    .param p11    # Z
    .param p12    # I
    .param p13    # Lcom/google/android/youtube/videos/Requesters;
    .param p14    # Lcom/google/android/youtube/videos/api/ApiRequesters;
    .param p15    # Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;
    .param p16    # Z
    .param p17    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;->database:Lcom/google/android/youtube/videos/store/Database;

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;->cursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    move/from16 v0, p11

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showSuggestions:Z

    move/from16 v0, p12

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;->maxSuggestions:I

    move-object/from16 v0, p15

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;->recommendationsRequestFactory:Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/ShowsHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    new-instance v4, Lcom/google/android/youtube/videos/NetworkMonitor;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-direct {v4, v0, v1}, Lcom/google/android/youtube/videos/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/NetworkMonitor$Listener;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    const v4, 0x7f070076

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v4, v1}, Lcom/google/android/youtube/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/StatusHelper;->init()V

    const v4, 0x7f070075

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setItemsCanFocus(Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setOnTouchStateChangedListener(Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    invoke-virtual/range {p2 .. p2}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02004d

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v18

    new-instance v4, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    move-object/from16 v0, p2

    move-object/from16 v1, p7

    move-object/from16 v2, v18

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    new-instance v4, Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    invoke-interface/range {p13 .. p13}, Lcom/google/android/youtube/videos/Requesters;->getPosterArtRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v5

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v5, v1}, Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    if-eqz p16, :cond_0

    new-instance v3, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-interface/range {p13 .. p13}, Lcom/google/android/youtube/videos/Requesters;->getMostPopularShowsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    new-instance v5, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataShowPageConverter;

    invoke-direct {v5}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataShowPageConverter;-><init>()V

    invoke-static {v4, v5}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v5

    move-object/from16 v4, p2

    move-object/from16 v6, p0

    move-object/from16 v7, p17

    move-object/from16 v8, p9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->gdataShowSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    :goto_0
    invoke-interface/range {p1 .. p1}, Lcom/google/android/youtube/videos/Config;->showsWelcomeIsFree()Z

    move-result v17

    new-instance v3, Lcom/google/android/youtube/videos/ui/WelcomeOutline;

    sget-object v5, Lcom/google/android/youtube/videos/ui/ViewTypes;->SHOWS_WELCOME:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-interface/range {p13 .. p13}, Lcom/google/android/youtube/videos/Requesters;->getBitmapRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v6

    if-eqz v17, :cond_1

    const v7, 0x7f0a002e

    :goto_1
    if-eqz v17, :cond_2

    const v8, 0x7f0a002f

    :goto_2
    if-eqz v17, :cond_3

    const v9, 0x7f0a0030

    :goto_3
    const v10, 0x7f030002

    invoke-interface/range {p1 .. p1}, Lcom/google/android/youtube/videos/Config;->showsWelcomeGraphic()Landroid/net/Uri;

    move-result-object v11

    new-instance v12, Lcom/google/android/youtube/videos/ui/ShowsHelper$WelcomeButtonClickListener;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v4}, Lcom/google/android/youtube/videos/ui/ShowsHelper$WelcomeButtonClickListener;-><init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;Lcom/google/android/youtube/videos/ui/ShowsHelper$1;)V

    move-object/from16 v4, p2

    invoke-direct/range {v3 .. v12}, Lcom/google/android/youtube/videos/ui/WelcomeOutline;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Lcom/google/android/youtube/core/async/Requester;IIIILandroid/net/Uri;Landroid/view/View$OnClickListener;)V

    new-instance v11, Lcom/google/android/youtube/videos/ui/ShowsHelper$ShowClickListener;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v4}, Lcom/google/android/youtube/videos/ui/ShowsHelper$ShowClickListener;-><init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;Lcom/google/android/youtube/videos/ui/ShowsHelper$1;)V

    new-instance v4, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    new-instance v13, Lcom/google/android/youtube/videos/ui/ShowsHelper$SuggestionClickListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v5}, Lcom/google/android/youtube/videos/ui/ShowsHelper$SuggestionClickListener;-><init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;Lcom/google/android/youtube/videos/ui/ShowsHelper$1;)V

    new-instance v14, Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v5}, Lcom/google/android/youtube/videos/ui/ShowsHelper$SeeMoreOnClickListener;-><init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;Lcom/google/android/youtube/videos/ui/ShowsHelper$1;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v5, p2

    move-object/from16 v6, p9

    move-object v8, v3

    move-object v12, v11

    move/from16 v16, p11

    invoke-direct/range {v4 .. v16}, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ErrorHelper;Landroid/widget/ListView;Lcom/google/android/youtube/core/adapter/Outline;Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Landroid/view/View$OnClickListener;Landroid/view/View;Z)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsOutlineHelper:Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;

    new-instance v19, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsOutlineHelper:Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->getOutline()Lcom/google/android/youtube/core/adapter/Outline;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->setOutline(Lcom/google/android/youtube/core/adapter/Outline;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->gdataShowSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    new-instance v3, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-interface/range {p14 .. p14}, Lcom/google/android/youtube/videos/api/ApiRequesters;->getRecommendationsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v5

    move-object/from16 v4, p2

    move-object/from16 v6, p0

    move-object/from16 v7, p17

    move-object/from16 v8, p9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    goto/16 :goto_0

    :cond_1
    const v7, 0x7f0a002b

    goto/16 :goto_1

    :cond_2
    const v8, 0x7f0a002c

    goto/16 :goto_2

    :cond_3
    const v9, 0x7f0a002d

    goto/16 :goto_3
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/ui/ShowsHelper;)Lcom/google/android/youtube/videos/Config;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/ShowsHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->config:Lcom/google/android/youtube/videos/Config;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/ui/ShowsHelper;ZI)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/ShowsHelper;
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->onShowClick(ZI)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/ui/ShowsHelper;ILandroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/ShowsHelper;
    .param p1    # I
    .param p2    # Landroid/view/View;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->onShowLongClick(ILandroid/view/View;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/ui/ShowsHelper;)Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/ShowsHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/ui/ShowsHelper;)Lvedroid/support/v4/app/FragmentActivity;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/ShowsHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/ui/ShowsHelper;)Lcom/google/android/youtube/videos/ui/SyncHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/ShowsHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/ui/ShowsHelper;)Lcom/google/android/youtube/videos/logging/EventLogger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/ShowsHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    return-object v0
.end method

.method private onShowClick(ZI)V
    .locals 5
    .param p1    # Z
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v2, p2}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/google/android/youtube/videos/activity/ShowActivity;->createShowIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lvedroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onShowLongClick(ILandroid/view/View;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->contextMenuShowId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->contextMenuShowTitle:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/view/View;->showContextMenu()Z

    return-void
.end method

.method private refreshShowsCursor()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingShowsCursor:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->cursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->updateVisibilities()V

    goto :goto_0
.end method

.method private updateVisibilities()V
    .locals 8

    const/4 v4, 0x1

    const/16 v7, 0x8

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getState()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v4, v7}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v4, v7}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getInitializationErrorMessage()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v4, v6, v5}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    move v1, v4

    :goto_1
    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->gdataShowSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->gdataShowSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->isLoading()Z

    move-result v2

    :goto_2
    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_3

    if-nez v2, :cond_3

    move v0, v4

    :goto_3
    if-nez v1, :cond_0

    if-eqz v0, :cond_4

    :cond_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsOutlineHelper:Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->updateVisibilities()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v1, v5

    goto :goto_1

    :cond_2
    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->isLoading()Z

    move-result v2

    goto :goto_2

    :cond_3
    move v0, v5

    goto :goto_3

    :cond_4
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v4, v7}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setLoading()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0700a4

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->contextMenuShowId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->contextMenuShowTitle:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->contextMenuShowId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->contextMenuShowTitle:Ljava/lang/String;

    invoke-static {v2, v0, v3, v4, v1}, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->showInstance(Lvedroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0006

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return-void
.end method

.method public onCursorChanged(Lcom/google/android/youtube/videos/ui/CursorHelper;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->refreshShowsCursor()V

    return-void
.end method

.method public onNetworkConnectivityChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->setNetworkConnected(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;->setNetworkConnected(Z)V

    return-void
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onPostersUpdated()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingPosterUpdatedNotification:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public onPurchasesUpdated()V
    .locals 0

    return-void
.end method

.method public onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->onPostersUpdated()V

    return-void
.end method

.method public final onStart()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->addListener(Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/videos/store/Database;->addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->cursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->addListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/NetworkMonitor;->register()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/NetworkMonitor;->isConnected()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->setNetworkConnected(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;->setNetworkConnected(Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->refreshShowsCursor()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getState()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->onSyncStateChanged(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsOutlineHelper:Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->gdataShowSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->gdataShowSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->reset()V

    :goto_0
    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->contextMenuShowId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->contextMenuShowTitle:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsAdapter:Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MyShowsAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;->clear()V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingPosterUpdatedNotification:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingShowsCursor:Z

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingSuggestions:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->cursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->removeListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->removeListener(Lcom/google/android/youtube/videos/store/Database$Listener;)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->removeListener(Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/NetworkMonitor;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showsOutlineHelper:Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/ShowsOutlineHelper;->onStop()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->reset()V

    goto :goto_0
.end method

.method public onSuggestionsAvailable(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingSuggestions:Ljava/util/List;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->updateVisibilities()V

    goto :goto_0
.end method

.method public onSuggestionsError(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->updateVisibilities()V

    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 4
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->showSuggestions:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsRequested:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsRequested:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->gdataShowSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->gdataShowSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->maxSuggestions:I

    invoke-static {v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->getMostPopularShowsRequest(I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->init(Ljava/lang/Object;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->updateVisibilities()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->recommendationsRequestFactory:Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->maxSuggestions:I

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;->createForShows(Ljava/lang/String;I)Lcom/google/android/youtube/videos/api/RecommendationsRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->init(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onTouchStateChanged(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingShowsCursor:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->refreshShowsCursor()V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingShowsCursor:Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingSuggestions:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/ShowSuggestionsAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingSuggestions:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->updateVisibilities()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingSuggestions:Ljava/util/List;

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingPosterUpdatedNotification:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->onPostersUpdated()V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper;->pendingPosterUpdatedNotification:Z

    goto :goto_0
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    return-void
.end method
