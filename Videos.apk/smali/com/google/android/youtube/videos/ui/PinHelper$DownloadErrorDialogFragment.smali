.class public Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;
.super Lvedroid/support/v4/app/DialogFragment;
.source "PinHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/PinHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadErrorDialogFragment"
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Lvedroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;)V
    .locals 4
    .param p0    # Lvedroid/support/v4/app/FragmentActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/Long;
    .param p5    # Ljava/lang/Integer;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "video_id"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "message"

    invoke-static {p0, p3, p4, p5}, Lcom/google/android/youtube/videos/pinning/PinningStatusHelper;->humanizeFailedReason(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;

    invoke-direct {v1}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lvedroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "DownloadErrorDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    # invokes: Lcom/google/android/youtube/videos/ui/PinHelper;->getEventLogger(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {p0}, Lcom/google/android/youtube/videos/ui/PinHelper;->access$000(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v2

    invoke-interface {v2, p3, p4, p5}, Lcom/google/android/youtube/videos/logging/EventLogger;->onShowDownloadErrorDialog(ILjava/lang/Long;Ljava/lang/Integer;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    # invokes: Lcom/google/android/youtube/videos/ui/PinHelper;->getEventLogger(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/PinHelper;->access$000(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onDismissDownloadErrorDialog()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->videoId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/videos/pinning/PinService;->requestClearError(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->account:Ljava/lang/String;

    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->videoId:Ljava/lang/String;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadErrorDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a004e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a003e

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
