.class public Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;
.super Lvedroid/support/v4/app/DialogFragment;
.source "PinHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/PinHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadDialogFragment"
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Lvedroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4
    .param p0    # Lvedroid/support/v4/app/FragmentActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "video_id"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "video_title"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "pinning_status"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "pinning_status_reason"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;

    invoke-direct {v1}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lvedroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "DownloadDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    # invokes: Lcom/google/android/youtube/videos/ui/PinHelper;->getEventLogger(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {p0}, Lcom/google/android/youtube/videos/ui/PinHelper;->access$000(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v3

    const/4 v2, 0x3

    if-ne p4, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-interface {v3, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onShowDownloadDialog(Z)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v3, -0x1

    if-ne p2, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    # invokes: Lcom/google/android/youtube/videos/ui/PinHelper;->getEventLogger(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/PinHelper;->access$000(Lvedroid/support/v4/app/FragmentActivity;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onDismissDownloadDialog(Z)V

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->videoId:Ljava/lang/String;

    invoke-static {v0, v3, v4, v2}, Lcom/google/android/youtube/videos/pinning/PinService;->requestSetPinned(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "authAccount"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->account:Ljava/lang/String;

    const-string v5, "video_id"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->videoId:Ljava/lang/String;

    const-string v5, "video_title"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "pinning_status"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v5, 0x3

    if-ne v2, v5, :cond_0

    const v1, 0x7f0a005c

    :goto_0
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0a005a

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x104000a

    invoke-virtual {v5, v6, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0a005d

    invoke-virtual {v5, v6, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {v6, v1, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5

    :cond_0
    if-ne v2, v7, :cond_1

    const-string v5, "pinning_status_reason"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lcom/google/android/youtube/videos/pinning/PinningStatusHelper;->getPendingReasonTextId(I)I

    move-result v1

    goto :goto_0

    :cond_1
    const v1, 0x7f0a005b

    goto :goto_0
.end method
