.class Lcom/google/android/youtube/videos/ui/SyncHelper$1;
.super Ljava/lang/Object;
.source "SyncHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/ui/SyncHelper;->createSyncPurchasesCallback()Lcom/google/android/youtube/core/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/accounts/UserAuth;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/SyncHelper;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/ui/SyncHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/SyncHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/SyncHelper;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->access$000(Lcom/google/android/youtube/videos/ui/SyncHelper;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v0

    if-ne p0, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/SyncHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/youtube/videos/ui/SyncHelper;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->access$002(Lcom/google/android/youtube/videos/ui/SyncHelper;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/SyncHelper;

    # setter for: Lcom/google/android/youtube/videos/ui/SyncHelper;->purchasesError:Ljava/lang/Exception;
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->access$202(Lcom/google/android/youtube/videos/ui/SyncHelper;Ljava/lang/Exception;)Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/SyncHelper;

    const/16 v1, 0x8

    # invokes: Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->access$100(Lcom/google/android/youtube/videos/ui/SyncHelper;I)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->onError(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Void;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Ljava/lang/Void;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/SyncHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/SyncHelper;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->access$000(Lcom/google/android/youtube/videos/ui/SyncHelper;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v0

    if-ne p0, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/SyncHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/youtube/videos/ui/SyncHelper;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->access$002(Lcom/google/android/youtube/videos/ui/SyncHelper;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/youtube/videos/ui/SyncHelper;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->access$100(Lcom/google/android/youtube/videos/ui/SyncHelper;I)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/accounts/UserAuth;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/SyncHelper$1;->onResponse(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/lang/Void;)V

    return-void
.end method
