.class public abstract Lcom/google/android/youtube/videos/ui/CursorHelper;
.super Ljava/lang/Object;
.source "CursorHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/store/Database$Listener;
.implements Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/CursorHelper$1;,
        Lcom/google/android/youtube/videos/ui/CursorHelper$ShowsCursorHelper;,
        Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;,
        Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesEstCursorHelper;,
        Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;,
        Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;,
        Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

.field private cursorWrapper:Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

.field private haveError:Z

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private pendingCursorUpdate:Z

.field private final purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private final syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/ui/SyncHelper;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/store/PurchaseStore;
    .param p3    # Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "syncHelper cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/SyncHelper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    const-string v0, "purchaseStore cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/PurchaseStore;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->listeners:Ljava/util/Set;

    new-instance v0, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/videos/ui/CursorHelper$CursorCallback;-><init>(Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/ui/CursorHelper$1;)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->callback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/ui/CursorHelper;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/CursorHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/CursorHelper;
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/ui/CursorHelper;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/CursorHelper;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->haveError:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/youtube/videos/ui/CursorHelper;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/CursorHelper;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->haveError:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/ui/CursorHelper;Landroid/database/Cursor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/CursorHelper;
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/CursorHelper;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/ui/CursorHelper;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/CursorHelper;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->pendingCursorUpdate:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/youtube/videos/ui/CursorHelper;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/CursorHelper;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->pendingCursorUpdate:Z

    return p1
.end method

.method private changeCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/database/Cursor;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->close()V

    :cond_0
    if-eqz p1, :cond_1

    new-instance v2, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    invoke-direct {v2, p0, p1}, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;-><init>(Lcom/google/android/youtube/videos/ui/CursorHelper;Landroid/database/Cursor;)V

    :goto_0
    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;->onCursorChanged(Lcom/google/android/youtube/videos/ui/CursorHelper;)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected abstract createCursorRequest(Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
.end method

.method protected getAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;->increment()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->cursorWrapper:Lcom/google/android/youtube/videos/ui/CursorHelper$ReferenceCountingCursorWrapper;

    return-object v0
.end method

.method public hasRequestInFlight()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onPurchasesUpdated()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->updateCursors()V

    return-void
.end method

.method public onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onStart()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->addListener(Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->updateCursors()V

    return-void
.end method

.method public onStop()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->haveError:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->pendingCursorUpdate:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->removeListener(Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->removeListener(Lcom/google/android/youtube/videos/store/Database$Listener;)Z

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/ui/CursorHelper;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/ui/CursorHelper;->changeCursor(Landroid/database/Cursor;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->updateCursors()V

    return-void
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    return-void
.end method

.method public removeListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method protected updateCursors()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->hasRequestInFlight()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->pendingCursorUpdate:Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->createCursorRequest(Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->currentRequest:Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/CursorHelper;->callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method
