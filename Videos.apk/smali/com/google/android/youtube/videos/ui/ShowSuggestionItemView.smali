.class public Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;
.super Lcom/google/android/youtube/videos/ui/NonResizingFrameLayout;
.source "ShowSuggestionItemView.java"

# interfaces
.implements Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;


# instance fields
.field private final defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private final dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private isNoThumbnailIcon:Z

.field private final noThumbnailBackgroundColor:I

.field private thumbnail:Landroid/graphics/Bitmap;

.field private thumbnailTag:Ljava/lang/Object;

.field private thumbnailView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1    # Landroid/content/Context;

    const/4 v7, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/NonResizingFrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->noThumbnailBackgroundColor:I

    new-instance v4, Landroid/widget/ImageView;

    invoke-direct {v4, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    iget v5, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->noThumbnailBackgroundColor:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const v5, 0x7f01000c

    invoke-virtual {v4, v5, v2, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    const/4 v4, 0x2

    new-array v1, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    const v6, 0x7f08000e

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v5, v1, v4

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    aput-object v4, v1, v7

    new-instance v4, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v4, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public getThumbnailTag()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailTag:Ljava/lang/Object;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    sub-int v1, p4, p2

    sub-int v0, p5, p3

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2, v3, v3, v1, v0}, Landroid/widget/ImageView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/high16 v3, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0, v0, v0}, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->setMeasuredDimension(II)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/ImageView;->measure(II)V

    return-void
.end method

.method public setDimmedStyle(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;Z)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnail:Landroid/graphics/Bitmap;

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->isNoThumbnailIcon:Z

    if-eq v0, p2, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnail:Landroid/graphics/Bitmap;

    iput-boolean p2, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->isNoThumbnailIcon:Z

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->noThumbnailBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public setThumbnailTag(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/ShowSuggestionItemView;->thumbnailTag:Ljava/lang/Object;

    return-void
.end method
