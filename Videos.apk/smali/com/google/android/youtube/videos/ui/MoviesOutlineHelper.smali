.class public Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;
.super Ljava/lang/Object;
.source "MoviesOutlineHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/NetworkMonitor$Listener;


# instance fields
.field private final estAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/adapter/ListAdapterOutline",
            "<",
            "Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final estHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final featureWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

.field private final outline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final seeMoreOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

.field private final showSuggestions:Z

.field private final suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/adapter/ListAdapterOutline",
            "<",
            "Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final suggestionsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final vodAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/adapter/ListAdapterOutline",
            "<",
            "Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final vodHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final warmWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ErrorHelper;Landroid/widget/ListView;Lcom/google/android/youtube/core/adapter/Outline;Lcom/google/android/youtube/core/adapter/Outline;Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Landroid/view/View$OnClickListener;Landroid/view/View;Z)V
    .locals 22
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/core/ErrorHelper;
    .param p3    # Landroid/widget/ListView;
    .param p4    # Lcom/google/android/youtube/core/adapter/Outline;
    .param p5    # Lcom/google/android/youtube/core/adapter/Outline;
    .param p6    # Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;
    .param p7    # Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;
    .param p8    # Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;
    .param p9    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    .param p10    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    .param p11    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;
    .param p12    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;
    .param p13    # Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
    .param p14    # Landroid/view/View$OnClickListener;
    .param p15    # Landroid/view/View;
    .param p16    # Z

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move/from16 v0, p16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->showSuggestions:Z

    const-string v3, "featureWelcomeOutline cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/adapter/Outline;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->featureWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

    const-string v3, "warmWelcomeOutline cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/adapter/Outline;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->warmWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

    const-string v3, "vodClickListener cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "estClickListener cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v17

    new-instance v3, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;

    sget-object v4, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const v5, 0x7f0a0010

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->vodHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    new-instance v3, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;

    sget-object v4, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const v5, 0x7f0a0011

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->estHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    new-instance v3, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;

    sget-object v4, Lcom/google/android/youtube/videos/ui/ViewTypes;->HEADING:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const v5, 0x7f0a0069

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/videos/adapter/PanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->suggestionsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    new-instance v3, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, p6

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->vodAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    new-instance v3, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, p7

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->estAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    new-instance v3, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, p8

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    const v3, 0x7f040036

    const/4 v4, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p14

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    check-cast v21, Landroid/widget/FrameLayout;

    move-object/from16 v0, v21

    invoke-direct {v3, v0}, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;-><init>(Landroid/widget/FrameLayout;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->seeMoreOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    new-instance v3, Lcom/google/android/youtube/videos/NetworkMonitor;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lcom/google/android/youtube/videos/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/NetworkMonitor$Listener;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v19

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090028

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    new-instance v2, Lcom/google/android/youtube/videos/ui/VideoListOutline;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->vodAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    sget-object v5, Lcom/google/android/youtube/videos/ui/ViewTypes;->PURCHASE_VOD:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v8, p9

    move-object/from16 v9, p11

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;)V

    new-instance v8, Lcom/google/android/youtube/videos/ui/VideoListOutline;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->estAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v10

    sget-object v11, Lcom/google/android/youtube/videos/ui/ViewTypes;->PURCHASE_EST:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move v12, v6

    move v13, v7

    move-object/from16 v14, p10

    move-object/from16 v15, p12

    invoke-direct/range {v8 .. v15}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;)V

    new-instance v9, Lcom/google/android/youtube/videos/ui/VideoListOutline;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v11

    sget-object v12, Lcom/google/android/youtube/videos/ui/ViewTypes;->SUGGESTION:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const/16 v16, 0x0

    move/from16 v13, v19

    move v14, v7

    move-object/from16 v15, p13

    invoke-direct/range {v9 .. v16}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;)V

    new-instance v20, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;

    const v3, 0x7f040029

    sget-object v4, Lcom/google/android/youtube/videos/ui/ViewTypes;->PANEL_SPACING_FOOTER:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/youtube/videos/adapter/ResourceLayoutOutline;-><init>(Landroid/view/LayoutInflater;ILcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->updateVisibilities()V

    new-instance v3, Lcom/google/android/youtube/core/adapter/SequentialOutline;

    const/16 v4, 0xa

    new-array v4, v4, [Lcom/google/android/youtube/core/adapter/Outline;

    const/4 v5, 0x0

    aput-object p4, v4, v5

    const/4 v5, 0x1

    aput-object p5, v4, v5

    const/4 v5, 0x2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->vodHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    aput-object v10, v4, v5

    const/4 v5, 0x3

    aput-object v2, v4, v5

    const/4 v5, 0x4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->estHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    aput-object v10, v4, v5

    const/4 v5, 0x5

    aput-object v8, v4, v5

    const/4 v5, 0x6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->suggestionsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    aput-object v10, v4, v5

    const/4 v5, 0x7

    aput-object v9, v4, v5

    const/16 v5, 0x8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->seeMoreOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    aput-object v10, v4, v5

    const/16 v5, 0x9

    aput-object v20, v4, v5

    invoke-direct {v3, v4}, Lcom/google/android/youtube/core/adapter/SequentialOutline;-><init>([Lcom/google/android/youtube/core/adapter/Outline;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    return-void
.end method


# virtual methods
.method public getOutline()Lcom/google/android/youtube/core/adapter/Outline;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->outline:Lcom/google/android/youtube/core/adapter/Outline;

    return-object v0
.end method

.method public onNetworkConnectivityChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->seeMoreOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->setDimmedStyle(Z)V

    return-void
.end method

.method public onStart()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/NetworkMonitor;->register()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->seeMoreOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/NetworkMonitor;->isConnected()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->setDimmedStyle(Z)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/NetworkMonitor;->unregister()V

    return-void
.end method

.method public updateVisibilities()V
    .locals 7

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    move v1, v4

    :goto_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->vodAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    move v2, v4

    :goto_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->estAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    move v0, v4

    :goto_2
    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->warmWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->featureWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/adapter/Outline;->isVisible()Z

    move-result v3

    if-nez v3, :cond_4

    if-nez v2, :cond_4

    if-nez v0, :cond_4

    move v3, v4

    :goto_3
    invoke-virtual {v6, v3}, Lcom/google/android/youtube/core/adapter/Outline;->setVisible(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->vodHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/core/adapter/Outline;->setVisible(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->vodAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->setVisible(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->estHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/adapter/Outline;->setVisible(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->estAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->setVisible(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->suggestionsHeadingOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/core/adapter/Outline;->setVisible(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->suggestionsAdapterOutline:Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;->setVisible(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->seeMoreOutline:Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;

    iget-boolean v6, p0, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->showSuggestions:Z

    if-eqz v6, :cond_5

    if-nez v1, :cond_0

    if-nez v2, :cond_0

    if-eqz v0, :cond_5

    :cond_0
    :goto_4
    invoke-virtual {v3, v4}, Lcom/google/android/youtube/videos/adapter/DimmableViewOutline;->setVisible(Z)V

    return-void

    :cond_1
    move v1, v5

    goto :goto_0

    :cond_2
    move v2, v5

    goto :goto_1

    :cond_3
    move v0, v5

    goto :goto_2

    :cond_4
    move v3, v5

    goto :goto_3

    :cond_5
    move v4, v5

    goto :goto_4
.end method
