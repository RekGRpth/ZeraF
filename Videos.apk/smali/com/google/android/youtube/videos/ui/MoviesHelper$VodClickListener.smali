.class final Lcom/google/android/youtube/videos/ui/MoviesHelper$VodClickListener;
.super Ljava/lang/Object;
.source "MoviesHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
.implements Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/MoviesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "VodClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$VodClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;Lcom/google/android/youtube/videos/ui/MoviesHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/MoviesHelper;
    .param p2    # Lcom/google/android/youtube/videos/ui/MoviesHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/MoviesHelper$VodClickListener;-><init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/youtube/videos/ui/VideoListOutline;ZI)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/ui/VideoListOutline;
    .param p2    # Z
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$VodClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # invokes: Lcom/google/android/youtube/videos/ui/MoviesHelper;->onVodClick(ZI)V
    invoke-static {v0, p2, p3}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$600(Lcom/google/android/youtube/videos/ui/MoviesHelper;ZI)V

    return-void
.end method

.method public onItemLongClick(Lcom/google/android/youtube/videos/ui/VideoListOutline;ILandroid/view/View;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/ui/VideoListOutline;
    .param p2    # I
    .param p3    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper$VodClickListener;->this$0:Lcom/google/android/youtube/videos/ui/MoviesHelper;

    # invokes: Lcom/google/android/youtube/videos/ui/MoviesHelper;->onVodLongClick(ILandroid/view/View;)V
    invoke-static {v0, p2, p3}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->access$700(Lcom/google/android/youtube/videos/ui/MoviesHelper;ILandroid/view/View;)V

    return-void
.end method
