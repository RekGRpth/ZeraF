.class public Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;
.super Lvedroid/support/v4/app/DialogFragment;
.source "PinHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/PinHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadPolicyDialogFragment"
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private downloadOnAnyNetwork:Landroid/widget/RadioButton;

.field private downloadWhenOnWifi:Landroid/widget/RadioButton;

.field private mobileDownloadsEnabled:Z

.field private preferences:Landroid/content/SharedPreferences;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Lvedroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0    # Lvedroid/support/v4/app/FragmentActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "video_id"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;

    invoke-direct {v1}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lvedroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "DownloadPolicyDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->mobileDownloadsEnabled:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->downloadWhenOnWifi:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const v2, 0x7f0a0060

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "download_policy"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "download_policy_dialog_shown"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->videoId:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/youtube/videos/pinning/PinService;->requestSetPinned(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_1
    const v2, 0x7f0a0061

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const v10, 0x7f0a0060

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v7, "authAccount"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->account:Ljava/lang/String;

    const-string v7, "video_id"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->videoId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->preferences:Landroid/content/SharedPreferences;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/youtube/videos/Config;->mobileDownloadsEnabled()Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->mobileDownloadsEnabled:Z

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f0a005e

    invoke-virtual {v3, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v7, 0x104000a

    invoke-virtual {v3, v7, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-boolean v7, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->mobileDownloadsEnabled:Z

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "download_policy"

    invoke-virtual {v0, v10}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v10}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f04000c

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v7, 0x7f070029

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->downloadWhenOnWifi:Landroid/widget/RadioButton;

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->downloadWhenOnWifi:Landroid/widget/RadioButton;

    invoke-virtual {v7, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    const v7, 0x7f07002a

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->downloadOnAnyNetwork:Landroid/widget/RadioButton;

    iget-object v8, p0, Lcom/google/android/youtube/videos/ui/PinHelper$DownloadPolicyDialogFragment;->downloadOnAnyNetwork:Landroid/widget/RadioButton;

    if-nez v6, :cond_0

    const/4 v7, 0x1

    :goto_0
    invoke-virtual {v8, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    :goto_1
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    return-object v7

    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    :cond_1
    const v7, 0x7f0a00c0

    invoke-virtual {v3, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method
