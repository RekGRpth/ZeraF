.class Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;
.super Landroid/os/AsyncTask;
.source "LocalVideosHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/LocalVideosHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private cursor:Landroid/database/Cursor;

.field private ends:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private names:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;Lcom/google/android/youtube/videos/ui/LocalVideosHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper;
    .param p2    # Lcom/google/android/youtube/videos/ui/LocalVideosHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;-><init>(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)V

    return-void
.end method

.method private findRanges(Landroid/database/Cursor;)V
    .locals 8
    .param p1    # Landroid/database/Cursor;

    const/16 v7, 0x8

    const/4 v6, 0x7

    const/4 v2, 0x0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v2, v2, 0x1

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->ends:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->names:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->ids:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v3

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->ends:Ljava/util/List;

    add-int/lit8 v5, v2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->names:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->ids:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8
    .param p1    # [Ljava/lang/Void;

    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$900(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter$Query;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "bucket_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    :goto_1
    return-object v7

    :catch_0
    move-exception v6

    const-string v0, "Error querying the media store"

    invoke-static {v0, v6}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->findRanges(Landroid/database/Cursor;)V

    goto :goto_1
.end method

.method protected onCancelled()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1    # Ljava/lang/Void;

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$700(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-result-object v2

    const v3, 0x7f0a0036

    invoke-virtual {v2, v3, v5}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setMessage(IZ)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->queryTask:Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;
    invoke-static {v2, v3}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$1002(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;)Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->names:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->names:Ljava/util/List;

    new-array v4, v1, [Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    # setter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketNames:[Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$402(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->ids:Ljava/util/List;

    new-array v4, v1, [Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    # setter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketIds:[Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$502(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    new-array v3, v1, [I

    # setter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I
    invoke-static {v2, v3}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$302(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;[I)[I

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$300(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)[I

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->ends:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->collapsedBuckets:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$200(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->ids:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->adapter:Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$600(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->cursor:Landroid/database/Cursor;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->itemsView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$800(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;
    invoke-static {v2}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$700(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # setter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketEnds:[I
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$302(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;[I)[I

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # setter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketNames:[Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$402(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # setter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->bucketIds:[Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$502(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->adapter:Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$600(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/LocalVideosAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$700(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->itemsView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$800(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->ends:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->names:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$QueryTask;->ids:Ljava/util/List;

    return-void
.end method
