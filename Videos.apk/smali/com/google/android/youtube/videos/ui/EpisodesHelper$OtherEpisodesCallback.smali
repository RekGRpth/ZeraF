.class final Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;
.super Ljava/lang/Object;
.source "EpisodesHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/EpisodesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OtherEpisodesCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/EpisodesHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/ui/EpisodesHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;->this$0:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/ui/EpisodesHelper;Lcom/google/android/youtube/videos/ui/EpisodesHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/EpisodesHelper;
    .param p2    # Lcom/google/android/youtube/videos/ui/EpisodesHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;-><init>(Lcom/google/android/youtube/videos/ui/EpisodesHelper;)V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;->this$0:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onOtherEpisodesNewCursor(Landroid/database/Cursor;)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->access$600(Lcom/google/android/youtube/videos/ui/EpisodesHelper;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;->onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;->this$0:Lcom/google/android/youtube/videos/ui/EpisodesHelper;

    # invokes: Lcom/google/android/youtube/videos/ui/EpisodesHelper;->onOtherEpisodesNewCursor(Landroid/database/Cursor;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/ui/EpisodesHelper;->access$600(Lcom/google/android/youtube/videos/ui/EpisodesHelper;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/EpisodesHelper$OtherEpisodesCallback;->onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
