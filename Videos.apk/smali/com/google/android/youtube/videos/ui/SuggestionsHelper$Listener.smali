.class public interface abstract Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;
.super Ljava/lang/Object;
.source "SuggestionsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/SuggestionsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onSuggestionsAvailable(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onSuggestionsError(Ljava/lang/String;)V
.end method
