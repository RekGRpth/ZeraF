.class public Lcom/google/android/youtube/videos/ui/BitmapLoader;
.super Ljava/lang/Object;
.source "BitmapLoader.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TR;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final view:Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/BitmapLoader;->view:Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;

    return-void
.end method

.method public static setBitmapAsync(Landroid/app/Activity;Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/youtube/core/async/Requester;Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p0    # Landroid/app/Activity;
    .param p1    # Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;
    .param p4    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;TR;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    const/4 v1, 0x1

    if-eqz p3, :cond_1

    invoke-interface {p1}, Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;->getThumbnailTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, p3}, Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;->setThumbnailTag(Ljava/lang/Object;)V

    invoke-interface {p1, p4, v1}, Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    new-instance v0, Lcom/google/android/youtube/videos/ui/BitmapLoader;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/videos/ui/BitmapLoader;-><init>(Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v0

    invoke-interface {p2, p3, v0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;->setThumbnailTag(Ljava/lang/Object;)V

    invoke-interface {p1, p4, v1}, Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    goto :goto_0
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/BitmapLoader;->view:Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;->getThumbnailTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/BitmapLoader;->view:Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;->setThumbnailTag(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p2    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/BitmapLoader;->view:Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;->getThumbnailTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/BitmapLoader;->view:Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/BitmapLoader;->onResponse(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    return-void
.end method
