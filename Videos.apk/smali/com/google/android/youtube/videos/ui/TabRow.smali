.class public Lcom/google/android/youtube/videos/ui/TabRow;
.super Landroid/widget/HorizontalScrollView;
.source "TabRow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/TabRow$OnTabFocusListener;,
        Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;
    }
.end annotation


# instance fields
.field private final centerTabs:Z

.field private final dividerDrawable:Landroid/graphics/drawable/Drawable;

.field private needsLayout:Z

.field private onTabClickListener:Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;

.field private onTabFocusListener:Lcom/google/android/youtube/videos/ui/TabRow$OnTabFocusListener;

.field private originalLeftPadding:I

.field private requestedTab:I

.field private final tabBackgroundId:I

.field private final tabHolder:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v5, -0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v3, p0, Lcom/google/android/youtube/videos/ui/TabRow;->requestedTab:I

    iput-boolean v4, p0, Lcom/google/android/youtube/videos/ui/TabRow;->needsLayout:Z

    sget-object v1, Lcom/google/android/videos/R$styleable;->TabRow:[I

    const v2, 0x7f0b000e

    invoke-virtual {p1, p2, v1, v3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabBackgroundId:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->centerTabs:Z

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->dividerDrawable:Landroid/graphics/drawable/Drawable;

    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/videos/ui/TabRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/ui/TabRow;->setHorizontalScrollBarEnabled(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/ui/TabRow;->setFillViewport(Z)V

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->centerTabs:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/ui/TabRow;->setFillViewport(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getPaddingLeft()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->originalLeftPadding:I

    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private childIndexToTabIndex(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/TabRow;->dividerDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    div-int/lit8 p1, p1, 0x2

    goto :goto_0
.end method

.method private tabIndexToChildIndex(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/TabRow;->dividerDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    mul-int/lit8 p1, p1, 0x2

    goto :goto_0
.end method


# virtual methods
.method public addTab(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/videos/ui/TabRow;->addTab(ILjava/lang/CharSequence;)V

    return-void
.end method

.method public addTab(IILjava/lang/CharSequence;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v0, p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    if-nez p2, :cond_1

    move-object v1, v2

    :goto_0
    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v3, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabBackgroundId:I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabBackgroundId:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/ui/TabRow;->addTab(Landroid/view/View;)V

    return-void

    :cond_1
    invoke-virtual {v2, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public addTab(ILjava/lang/CharSequence;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/youtube/videos/ui/TabRow;->addTab(IILjava/lang/CharSequence;)V

    return-void
.end method

.method public addTab(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->dividerDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->dividerDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->needsLayout:Z

    return-void
.end method

.method public focusTab(IZ)V
    .locals 8
    .param p1    # I
    .param p2    # Z

    const/4 v6, 0x0

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/ui/TabRow;->needsLayout:Z

    if-eqz v5, :cond_1

    iput p1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->requestedTab:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/TabRow;->tabIndexToChildIndex(I)I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    if-ne v1, v0, :cond_2

    const/4 v5, 0x1

    :goto_2
    invoke-virtual {v7, v5}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v5, v6

    goto :goto_2

    :cond_3
    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v7

    add-int/2addr v5, v7

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getWidth()I

    move-result v7

    sub-int/2addr v5, v7

    div-int/lit8 v3, v5, 0x2

    if-eqz p2, :cond_5

    invoke-virtual {p0, v3, v6}, Lcom/google/android/youtube/videos/ui/TabRow;->smoothScrollTo(II)V

    :cond_4
    :goto_3
    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/TabRow;->onTabFocusListener:Lcom/google/android/youtube/videos/ui/TabRow$OnTabFocusListener;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/TabRow;->onTabFocusListener:Lcom/google/android/youtube/videos/ui/TabRow$OnTabFocusListener;

    invoke-interface {v5, p1}, Lcom/google/android/youtube/videos/ui/TabRow$OnTabFocusListener;->onTabFocus(I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v3, v6}, Lcom/google/android/youtube/videos/ui/TabRow;->scrollTo(II)V

    goto :goto_3
.end method

.method public getTabAt(I)Landroid/view/View;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/TabRow;->tabIndexToChildIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getTabCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/TabRow;->dividerDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->onTabClickListener:Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->onTabClickListener:Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/TabRow;->childIndexToTabIndex(I)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;->onTabClicked(I)V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/HorizontalScrollView;->onLayout(ZIIII)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->needsLayout:Z

    iget v0, p0, Lcom/google/android/youtube/videos/ui/TabRow;->requestedTab:I

    if-ltz v0, :cond_0

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->requestedTab:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/TabRow;->focusTab(IZ)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getMeasuredWidth()I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/ui/TabRow;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-ge v0, v1, :cond_0

    sub-int v2, v1, v0

    div-int/lit8 v3, v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getPaddingRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getPaddingBottom()I

    move-result v6

    invoke-super {p0, v3, v4, v5, v6}, Landroid/widget/HorizontalScrollView;->setPadding(IIII)V

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/google/android/youtube/videos/ui/TabRow;->originalLeftPadding:I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getPaddingRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TabRow;->getPaddingBottom()I

    move-result v6

    invoke-super {p0, v3, v4, v5, v6}, Landroid/widget/HorizontalScrollView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public removeAllTabs()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/TabRow;->tabHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public setOnTabClickListener(Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->onTabClickListener:Lcom/google/android/youtube/videos/ui/TabRow$OnTabClickListener;

    return-void
.end method

.method public setOnTabFocusListener(Lcom/google/android/youtube/videos/ui/TabRow$OnTabFocusListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/TabRow$OnTabFocusListener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->onTabFocusListener:Lcom/google/android/youtube/videos/ui/TabRow$OnTabFocusListener;

    return-void
.end method

.method public setPadding(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->setPadding(IIII)V

    iput p1, p0, Lcom/google/android/youtube/videos/ui/TabRow;->originalLeftPadding:I

    return-void
.end method
