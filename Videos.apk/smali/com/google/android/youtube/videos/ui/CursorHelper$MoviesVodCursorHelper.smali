.class public final Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;
.super Lcom/google/android/youtube/videos/ui/CursorHelper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MoviesVodCursorHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/ui/SyncHelper;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/store/PurchaseStore;
    .param p3    # Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/ui/CursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/ui/SyncHelper;)V

    return-void
.end method


# virtual methods
.method protected createCursorRequest(Ljava/lang/String;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter$Query;->createVodRequest(Ljava/lang/String;J)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;->updateCursors()V

    :cond_0
    return-void
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/CursorHelper$MoviesVodCursorHelper;->updateCursors()V

    return-void
.end method
