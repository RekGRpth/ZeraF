.class public Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;
.super Ljava/lang/Object;
.source "RelatedMovieSuggestionsHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/NetworkMonitor$Listener;
.implements Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;
.implements Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;
.implements Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final emptyStatusMessage:I

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/ui/SuggestionsHelper",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            ">;"
        }
    .end annotation
.end field

.field private lastGDataRelatedUri:Landroid/net/Uri;

.field private lastVideoId:Ljava/lang/String;

.field private final maxSuggestions:I

.field private final networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

.field private final recommendationsRequestFactory:Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

.field private final statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

.field private final suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

.field private final suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/ui/SuggestionsHelper",
            "<",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final suggestionsHolder:Landroid/widget/LinearLayout;

.field private suggestionsLoaded:Z

.field private final suggestionsOutline:Lcom/google/android/youtube/core/adapter/Outline;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/widget/LinearLayout;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;Lcom/google/android/youtube/core/async/Requester;Landroid/view/View;ILcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;Ljava/lang/String;IIZLcom/google/android/youtube/core/async/Requester;)V
    .locals 9
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/widget/LinearLayout;
    .param p4    # Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;
    .param p6    # Landroid/view/View;
    .param p7    # I
    .param p8    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .param p9    # Lcom/google/android/youtube/core/ErrorHelper;
    .param p10    # Ljava/lang/String;
    .param p11    # I
    .param p12    # I
    .param p13    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/widget/LinearLayout;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/view/View;",
            "I",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            "Lcom/google/android/youtube/core/ErrorHelper;",
            "Ljava/lang/String;",
            "IIZ",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsLoaded:Z

    const-string v1, "activity cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->activity:Landroid/app/Activity;

    const-string v1, "suggestionsHolder cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsHolder:Landroid/widget/LinearLayout;

    const-string v1, "suggestionsRequester cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "posterArtRequester cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "recommendationsRequestFactory cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->recommendationsRequestFactory:Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->emptyStatusMessage:I

    const-string v1, "eventLogger cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/logging/EventLogger;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const-string v1, "account cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->account:Ljava/lang/String;

    move/from16 v0, p12

    iput v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->maxSuggestions:I

    new-instance v1, Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-direct {v1, p1, p0}, Lcom/google/android/youtube/videos/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/NetworkMonitor$Listener;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    new-instance v1, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02004d

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v1, p1, p5, v2}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    new-instance v1, Lcom/google/android/youtube/videos/ui/VideoListOutline;

    new-instance v2, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    const/4 v4, 0x1

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const/4 v6, 0x0

    new-instance v7, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v8, "suggestions"

    invoke-direct {v7, v8}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/youtube/core/adapter/ListAdapterOutline;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/adapter/Outline$ViewType;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    const-string v5, "suggestion"

    invoke-direct {v4, v5}, Lcom/google/android/youtube/core/adapter/Outline$ViewType;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090028

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const/4 v8, 0x0

    move/from16 v5, p11

    move-object v7, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/youtube/videos/ui/VideoListOutline;-><init>(Lcom/google/android/youtube/core/adapter/Outline;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;IILcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsOutline:Lcom/google/android/youtube/core/adapter/Outline;

    if-eqz p13, :cond_0

    new-instance v1, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    new-instance v2, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataVideoPageConverter;

    invoke-direct {v2}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataVideoPageConverter;-><init>()V

    move-object/from16 v0, p14

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    move-object v2, p1

    move-object v4, p0

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    :goto_0
    invoke-static {p1, p6, p0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/StatusHelper;->init()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    new-instance v2, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper$1;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper$1;-><init>(Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    new-instance v1, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    move-object v2, p1

    move-object v3, p3

    move-object v4, p0

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->updateSuggestionsHolder()V

    return-void
.end method

.method private showEmpty()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->emptyStatusMessage:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setMessage(IZ)V

    return-void
.end method

.method private updateSuggestionsHolder()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->showEmpty()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsOutline:Lcom/google/android/youtube/core/adapter/Outline;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/adapter/Outline;->snapshotInto(Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    goto :goto_0
.end method


# virtual methods
.method public final init(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->lastVideoId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->lastGDataRelatedUri:Landroid/net/Uri;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsLoaded:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setLoading()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->maxSuggestions:I

    invoke-static {p2, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->getSuggestedMoviesRequest(Landroid/net/Uri;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->init(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->recommendationsRequestFactory:Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->account:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->maxSuggestions:I

    invoke-interface {v1, v2, p1, v3}, Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;->createForRelatedMovies(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/youtube/videos/api/RecommendationsRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->init(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final initEmpty()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->showEmpty()V

    return-void
.end method

.method public onItemClick(Lcom/google/android/youtube/videos/ui/VideoListOutline;ZI)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/ui/VideoListOutline;
    .param p2    # Z
    .param p3    # I

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-virtual {v2, p3}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->account:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const-string v3, "suggestionOnMovieDetails"

    invoke-interface {v2, v3, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForMovie(Ljava/lang/String;Z)V

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->activity:Landroid/app/Activity;

    const v3, 0x7f0a0078

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_0
    return-void
.end method

.method public onNetworkConnectivityChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->setNetworkConnected(Z)V

    return-void
.end method

.method public onRetry()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->lastVideoId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->lastVideoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->lastGDataRelatedUri:Landroid/net/Uri;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->init(Ljava/lang/String;Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/NetworkMonitor;->register()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/NetworkMonitor;->isConnected()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->setNetworkConnected(Z)V

    return-void
.end method

.method public onStop()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->lastVideoId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/NetworkMonitor;->unregister()V

    return-void
.end method

.method public onSuggestionsAvailable(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsLoaded:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;)V

    return-void
.end method

.method public onSuggestionsError(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/RelatedMovieSuggestionsHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    return-void
.end method
