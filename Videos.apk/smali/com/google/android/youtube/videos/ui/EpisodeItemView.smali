.class public abstract Lcom/google/android/youtube/videos/ui/EpisodeItemView;
.super Lcom/google/android/youtube/videos/ui/NonResizingFrameLayout;
.source "EpisodeItemView.java"

# interfaces
.implements Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;


# instance fields
.field private final defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field protected final detailsView:Landroid/view/View;

.field private final dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private final episodeNumberView:Landroid/widget/TextView;

.field private isNoThumbnailIcon:Z

.field private final noThumbnailBackgroundColor:I

.field private thumbnail:Landroid/graphics/Bitmap;

.field private thumbnailTag:Ljava/lang/Object;

.field private final thumbnailView:Landroid/widget/ImageView;

.field private final titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .param p4    # I

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/ui/NonResizingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->noThumbnailBackgroundColor:I

    new-instance v4, Landroid/widget/ImageView;

    invoke-direct {v4, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    iget v5, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->noThumbnailBackgroundColor:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->addView(Landroid/view/View;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-virtual {v4, p4, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->detailsView:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->detailsView:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->detailsView:Landroid/view/View;

    const v5, 0x7f07002c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->episodeNumberView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->detailsView:Landroid/view/View;

    const v5, 0x7f070017

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->titleView:Landroid/widget/TextView;

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const v5, 0x7f01000c

    invoke-virtual {v4, v5, v2, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    const/4 v4, 0x2

    new-array v1, v4, [Landroid/graphics/drawable/Drawable;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    const v5, 0x7f08000e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v4, v1, v6

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    aput-object v4, v1, v7

    new-instance v4, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v4, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public getThumbnailTag()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailTag:Ljava/lang/Object;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/high16 v6, 0x40000000

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    :goto_0
    invoke-static {v2, p1}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->getDefaultSize(II)I

    move-result v3

    int-to-float v4, v3

    const v5, 0x3fe38e39

    div-float/2addr v4, v5

    float-to-int v0, v4

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-super {p0, v4, v5}, Lcom/google/android/youtube/videos/ui/NonResizingFrameLayout;->onMeasure(II)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDimmedStyle(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setEpisodeNumber(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->episodeNumberView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->episodeNumberView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00da

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;Z)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnail:Landroid/graphics/Bitmap;

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->isNoThumbnailIcon:Z

    if-eq v0, p2, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnail:Landroid/graphics/Bitmap;

    iput-boolean p2, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->isNoThumbnailIcon:Z

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->noThumbnailBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->detailsView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->detailsView:Landroid/view/View;

    const v1, 0x7f020021

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setThumbnailTag(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->thumbnailTag:Ljava/lang/Object;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/EpisodeItemView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
