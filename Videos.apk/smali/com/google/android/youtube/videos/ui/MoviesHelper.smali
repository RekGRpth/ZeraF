.class public Lcom/google/android/youtube/videos/ui/MoviesHelper;
.super Ljava/lang/Object;
.source "MoviesHelper.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Lcom/google/android/youtube/videos/NetworkMonitor$Listener;
.implements Lcom/google/android/youtube/videos/store/Database$Listener;
.implements Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;
.implements Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;
.implements Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;
.implements Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/MoviesHelper$1;,
        Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;,
        Lcom/google/android/youtube/videos/ui/MoviesHelper$WarmWelcomeButtonClickListener;,
        Lcom/google/android/youtube/videos/ui/MoviesHelper$SeeMoreOnClickListener;,
        Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;,
        Lcom/google/android/youtube/videos/ui/MoviesHelper$EstClickListener;,
        Lcom/google/android/youtube/videos/ui/MoviesHelper$VodClickListener;
    }
.end annotation


# instance fields
.field private final activity:Lvedroid/support/v4/app/FragmentActivity;

.field private final config:Lcom/google/android/youtube/videos/Config;

.field private contextMenuVideoId:Ljava/lang/String;

.field private contextMenuVideoTitle:Ljava/lang/String;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

.field private final estCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final featureWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

.field private final gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/ui/SuggestionsHelper",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

.field private final maxSuggestions:I

.field private final moviesOutlineHelper:Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;

.field private final networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

.field private pendingEstCursor:Z

.field private pendingPosterUpdatedNotification:Z

.field private pendingSuggestions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation
.end field

.field private pendingVodCursor:Z

.field private final pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

.field private final promoManager:Lcom/google/android/youtube/videos/PromoManager;

.field private final recommendationsRequestFactory:Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

.field private final showSuggestions:Z

.field private final statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

.field private final suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

.field private final suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/videos/ui/SuggestionsHelper",
            "<",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private suggestionsRequested:Z

.field private final syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

.field private final vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

.field private final vodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/videos/PromoManager;Lvedroid/support/v4/app/FragmentActivity;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/ui/CursorHelper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/store/PosterStore;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;Lcom/google/android/youtube/videos/ui/SyncHelper;Lcom/google/android/youtube/videos/ui/PinHelper;Lcom/google/android/youtube/videos/Requesters;Lcom/google/android/youtube/videos/api/ApiRequesters;Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;ZZZ)V
    .locals 25
    .param p1    # Lcom/google/android/youtube/videos/Config;
    .param p2    # Lcom/google/android/youtube/videos/PromoManager;
    .param p3    # Lvedroid/support/v4/app/FragmentActivity;
    .param p4    # Landroid/view/View;
    .param p5    # Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;
    .param p6    # Lcom/google/android/youtube/videos/ui/CursorHelper;
    .param p7    # Lcom/google/android/youtube/videos/ui/CursorHelper;
    .param p8    # Lcom/google/android/youtube/videos/store/Database;
    .param p9    # Lcom/google/android/youtube/videos/store/PosterStore;
    .param p10    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p11    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .param p12    # Lcom/google/android/youtube/core/ErrorHelper;
    .param p13    # Lcom/google/android/youtube/videos/ui/SyncHelper;
    .param p14    # Lcom/google/android/youtube/videos/ui/PinHelper;
    .param p15    # Lcom/google/android/youtube/videos/Requesters;
    .param p16    # Lcom/google/android/youtube/videos/api/ApiRequesters;
    .param p17    # Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;
    .param p18    # Z
    .param p19    # Z
    .param p20    # Z

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->config:Lcom/google/android/youtube/videos/Config;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->promoManager:Lcom/google/android/youtube/videos/PromoManager;

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->database:Lcom/google/android/youtube/videos/store/Database;

    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    move-object/from16 v0, p14

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    move/from16 v0, p18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->showSuggestions:Z

    invoke-virtual/range {p3 .. p3}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->maxSuggestions:I

    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->recommendationsRequestFactory:Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    new-instance v5, Lcom/google/android/youtube/videos/NetworkMonitor;

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    invoke-direct {v5, v0, v1}, Lcom/google/android/youtube/videos/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/NetworkMonitor$Listener;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    const v5, 0x7f070064

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v0, v5, v1}, Lcom/google/android/youtube/videos/ui/StatusHelper;->createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/ui/StatusHelper;->init()V

    const v5, 0x7f070063

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setItemsCanFocus(Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setOnTouchStateChangedListener(Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    invoke-virtual/range {p3 .. p3}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02004d

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    new-instance v5, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    move-object/from16 v0, p3

    move-object/from16 v1, p9

    move-object/from16 v2, v23

    move/from16 v3, p19

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/graphics/Bitmap;Z)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    new-instance v5, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    move-object/from16 v0, p3

    move-object/from16 v1, p9

    move-object/from16 v2, v23

    move/from16 v3, p19

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/store/PosterStore;Landroid/graphics/Bitmap;Z)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    new-instance v5, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-interface/range {p15 .. p15}, Lcom/google/android/youtube/videos/Requesters;->getPosterArtRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v6

    move-object/from16 v0, p3

    move-object/from16 v1, v23

    invoke-direct {v5, v0, v6, v1}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    if-eqz p20, :cond_0

    new-instance v4, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-interface/range {p15 .. p15}, Lcom/google/android/youtube/videos/Requesters;->getMostPopularMoviesRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v5

    new-instance v6, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataVideoPageConverter;

    invoke-direct {v6}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataVideoPageConverter;-><init>()V

    invoke-static {v5, v6}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v6

    move-object/from16 v5, p3

    move-object/from16 v7, p0

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    invoke-direct/range {v4 .. v9}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    :goto_0
    new-instance v4, Lcom/google/android/youtube/videos/ui/WelcomeOutline;

    sget-object v6, Lcom/google/android/youtube/videos/ui/ViewTypes;->MOVIES_WELCOME:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-interface/range {p15 .. p15}, Lcom/google/android/youtube/videos/Requesters;->getBitmapRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v7

    const v8, 0x7f0a0023

    const v9, 0x7f0a0024

    const v10, 0x7f0a0027

    const v11, 0x7f030001

    invoke-interface/range {p1 .. p1}, Lcom/google/android/youtube/videos/Config;->knowledgeWelcomeGraphic()Landroid/net/Uri;

    move-result-object v12

    new-instance v13, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v5}, Lcom/google/android/youtube/videos/ui/MoviesHelper$FeatureWelcomeButtonClickListener;-><init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;Lcom/google/android/youtube/videos/ui/MoviesHelper$1;)V

    move-object/from16 v5, p3

    invoke-direct/range {v4 .. v13}, Lcom/google/android/youtube/videos/ui/WelcomeOutline;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Lcom/google/android/youtube/core/async/Requester;IIIILandroid/net/Uri;Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->featureWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/youtube/videos/Config;->moviesWelcomeIsFree()Z

    move-result v22

    new-instance v4, Lcom/google/android/youtube/videos/ui/WelcomeOutline;

    sget-object v6, Lcom/google/android/youtube/videos/ui/ViewTypes;->MOVIES_WELCOME:Lcom/google/android/youtube/core/adapter/Outline$ViewType;

    invoke-interface/range {p15 .. p15}, Lcom/google/android/youtube/videos/Requesters;->getBitmapRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v7

    if-eqz v22, :cond_1

    const v8, 0x7f0a0028

    :goto_1
    if-eqz v22, :cond_2

    const v9, 0x7f0a0029

    :goto_2
    if-eqz v22, :cond_3

    const v10, 0x7f0a002a

    :goto_3
    const v11, 0x7f030002

    invoke-interface/range {p1 .. p1}, Lcom/google/android/youtube/videos/Config;->moviesWelcomeGraphic()Landroid/net/Uri;

    move-result-object v12

    new-instance v13, Lcom/google/android/youtube/videos/ui/MoviesHelper$WarmWelcomeButtonClickListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v5}, Lcom/google/android/youtube/videos/ui/MoviesHelper$WarmWelcomeButtonClickListener;-><init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;Lcom/google/android/youtube/videos/ui/MoviesHelper$1;)V

    move-object/from16 v5, p3

    invoke-direct/range {v4 .. v13}, Lcom/google/android/youtube/videos/ui/WelcomeOutline;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Lcom/google/android/youtube/core/async/Requester;IIIILandroid/net/Uri;Landroid/view/View$OnClickListener;)V

    new-instance v14, Lcom/google/android/youtube/videos/ui/MoviesHelper$VodClickListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v5}, Lcom/google/android/youtube/videos/ui/MoviesHelper$VodClickListener;-><init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;Lcom/google/android/youtube/videos/ui/MoviesHelper$1;)V

    new-instance v15, Lcom/google/android/youtube/videos/ui/MoviesHelper$EstClickListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v5}, Lcom/google/android/youtube/videos/ui/MoviesHelper$EstClickListener;-><init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;Lcom/google/android/youtube/videos/ui/MoviesHelper$1;)V

    new-instance v5, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->featureWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    new-instance v18, Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;

    const/4 v6, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lcom/google/android/youtube/videos/ui/MoviesHelper$SuggestionClickListener;-><init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;Lcom/google/android/youtube/videos/ui/MoviesHelper$1;)V

    new-instance v19, Lcom/google/android/youtube/videos/ui/MoviesHelper$SeeMoreOnClickListener;

    const/4 v6, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lcom/google/android/youtube/videos/ui/MoviesHelper$SeeMoreOnClickListener;-><init>(Lcom/google/android/youtube/videos/ui/MoviesHelper;Lcom/google/android/youtube/videos/ui/MoviesHelper$1;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v20, v0

    move-object/from16 v6, p3

    move-object/from16 v7, p12

    move-object v10, v4

    move-object/from16 v16, v14

    move-object/from16 v17, v15

    move/from16 v21, p18

    invoke-direct/range {v5 .. v21}, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ErrorHelper;Landroid/widget/ListView;Lcom/google/android/youtube/core/adapter/Outline;Lcom/google/android/youtube/core/adapter/Outline;Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;Landroid/view/View$OnClickListener;Landroid/view/View;Z)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->moviesOutlineHelper:Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;

    new-instance v24, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;

    invoke-direct/range {v24 .. v24}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->moviesOutlineHelper:Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->getOutline()Lcom/google/android/youtube/core/adapter/Outline;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lcom/google/android/youtube/core/adapter/OutlinerAdapter;->setOutline(Lcom/google/android/youtube/core/adapter/Outline;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    new-instance v4, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-interface/range {p16 .. p16}, Lcom/google/android/youtube/videos/api/ApiRequesters;->getRecommendationsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v6

    move-object/from16 v5, p3

    move-object/from16 v7, p0

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    invoke-direct/range {v4 .. v9}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/ui/SuggestionsHelper$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;Lcom/google/android/youtube/core/ErrorHelper;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    goto/16 :goto_0

    :cond_1
    const v8, 0x7f0a0025

    goto/16 :goto_1

    :cond_2
    const v9, 0x7f0a0026

    goto/16 :goto_2

    :cond_3
    const v10, 0x7f0a0027

    goto/16 :goto_3
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lvedroid/support/v4/app/FragmentActivity;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/ui/SyncHelper;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/logging/EventLogger;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/Config;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->config:Lcom/google/android/youtube/videos/Config;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/youtube/videos/ui/MoviesHelper;)Lcom/google/android/youtube/videos/PromoManager;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->promoManager:Lcom/google/android/youtube/videos/PromoManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/ui/MoviesHelper;ZI)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onVodClick(ZI)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/ui/MoviesHelper;ILandroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;
    .param p1    # I
    .param p2    # Landroid/view/View;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onVodLongClick(ILandroid/view/View;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/ui/MoviesHelper;ZI)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onEstClick(ZI)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/ui/MoviesHelper;ILandroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/MoviesHelper;
    .param p1    # I
    .param p2    # Landroid/view/View;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onEstLongClick(ILandroid/view/View;)V

    return-void
.end method

.method private onEstClick(ZI)V
    .locals 5
    .param p1    # Z
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v2, p2}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onPinClicked(Ljava/lang/String;Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;Landroid/database/Cursor;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lvedroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private onEstLongClick(ILandroid/view/View;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoTitle:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/view/View;->showContextMenu()Z

    return-void
.end method

.method private onPinClicked(Ljava/lang/String;Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;
    .param p3    # Landroid/database/Cursor;

    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isPinned(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v5

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/PinHelper;->showPinningDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2, p3}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p3}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->showErrorDialog(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/videos/ui/PinHelper;->pinVideo(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPinClick()V

    goto :goto_0
.end method

.method private onVodClick(ZI)V
    .locals 7
    .param p1    # Z
    .param p2    # I

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v4, p2}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v4, v1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v4, v1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isExpired(Landroid/database/Cursor;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    invoke-static {v4, v3, v0}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const-string v5, "expiredRental"

    invoke-interface {v4, v5, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForMovie(Ljava/lang/String;Z)V

    if-nez v2, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    const v5, 0x7f0a0078

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-direct {p0, v3, v4, v1}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onPinClicked(Ljava/lang/String;Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v3}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v4, v5}, Lvedroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private onVodLongClick(ILandroid/view/View;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoTitle:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/view/View;->showContextMenu()Z

    return-void
.end method

.method private refreshEstCursor()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingEstCursor:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->updateVisibilities()V

    goto :goto_0
.end method

.method private refreshVodCursor()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingVodCursor:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->updateVisibilities()V

    goto :goto_0
.end method

.method private shouldShowFeatureWelcome()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Config;->knowledgeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->knowledgeWelcomeBrowseUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->promoManager:Lcom/google/android/youtube/videos/PromoManager;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/PromoManager;->shouldShow()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showErrorDialog(Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pinHelper:Lcom/google/android/youtube/videos/ui/PinHelper;

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x8

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v4, 0xb

    invoke-static {p1, v4}, Lcom/google/android/youtube/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v4

    const/16 v5, 0x9

    invoke-static {p1, v5}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/PinHelper;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private updateVisibilities()V
    .locals 10

    const/16 v9, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->featureWelcomeOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->shouldShowFeatureWelcome()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/android/youtube/core/adapter/Outline;->setVisible(Z)V

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getState()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v5, v9}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v5, v9}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getInitializationErrorMessage()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v5, v7, v6}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setErrorMessage(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    move v2, v5

    :goto_1
    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    move v0, v5

    :goto_2
    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->isLoading()Z

    move-result v3

    :goto_3
    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_4

    if-nez v3, :cond_4

    move v1, v5

    :goto_4
    if-nez v2, :cond_0

    if-nez v0, :cond_0

    if-eqz v1, :cond_5

    :cond_0
    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->moviesOutlineHelper:Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->updateVisibilities()V

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v5, v6}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v2, v6

    goto :goto_1

    :cond_2
    move v0, v6

    goto :goto_2

    :cond_3
    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->isLoading()Z

    move-result v3

    goto :goto_3

    :cond_4
    move v1, v6

    goto :goto_4

    :cond_5
    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v5, v9}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v5}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setLoading()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0700a1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoTitle:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoTitle:Ljava/lang/String;

    invoke-static {v2, v0, v3, v4, v1}, Lcom/google/android/youtube/videos/ui/RemoveItemDialogFragment;->showInstance(Lvedroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->activity:Lvedroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return-void
.end method

.method public onCursorChanged(Lcom/google/android/youtube/videos/ui/CursorHelper;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/ui/CursorHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->refreshEstCursor()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->refreshVodCursor()V

    goto :goto_0
.end method

.method public onNetworkConnectivityChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->setNetworkConnected(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->setNetworkConnected(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->setNetworkConnected(Z)V

    return-void
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onPostersUpdated()V

    return-void
.end method

.method public onPostersUpdated()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingPosterUpdatedNotification:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public onPurchasesUpdated()V
    .locals 0

    return-void
.end method

.method public onSeasonMetadataUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onStart()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->addListener(Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/videos/store/Database;->addListener(Lcom/google/android/youtube/videos/store/Database$Listener;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->addListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->addListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->refreshEstCursor()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->refreshVodCursor()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getState()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onSyncStateChanged(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/NetworkMonitor;->register()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/NetworkMonitor;->isConnected()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->setNetworkConnected(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->setNetworkConnected(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->setNetworkConnected(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->moviesOutlineHelper:Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->reset()V

    :goto_0
    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->contextMenuVideoTitle:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estAdapter:Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/adapter/MyPurchasesAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;->clear()V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingVodCursor:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingEstCursor:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingPosterUpdatedNotification:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsRequested:Z

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingSuggestions:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->statusHelper:Lcom/google/android/youtube/videos/ui/StatusHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->hide()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->estCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->removeListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->vodCursorHelper:Lcom/google/android/youtube/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/CursorHelper;->removeListener(Lcom/google/android/youtube/videos/ui/CursorHelper$Listener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/store/Database;->removeListener(Lcom/google/android/youtube/videos/store/Database$Listener;)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->removeListener(Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->networkMonitor:Lcom/google/android/youtube/videos/NetworkMonitor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/NetworkMonitor;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->moviesOutlineHelper:Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/MoviesOutlineHelper;->onStop()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->reset()V

    goto :goto_0
.end method

.method public onSuggestionsAvailable(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->listView:Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingSuggestions:Ljava/util/List;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->updateVisibilities()V

    goto :goto_0
.end method

.method public onSuggestionsError(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->updateVisibilities()V

    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 4
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->showSuggestions:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsRequested:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsRequested:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->gdataMovieSuggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->maxSuggestions:I

    invoke-static {v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->getTrendingMoviesRequest(I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->init(Ljava/lang/Object;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->updateVisibilities()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsHelper:Lcom/google/android/youtube/videos/ui/SuggestionsHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->recommendationsRequestFactory:Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/youtube/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->maxSuggestions:I

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;->createForMovies(Ljava/lang/String;I)Lcom/google/android/youtube/videos/api/RecommendationsRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->init(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onTouchStateChanged(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingVodCursor:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->refreshVodCursor()V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingVodCursor:Z

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingEstCursor:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->refreshEstCursor()V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingEstCursor:Z

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingSuggestions:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->suggestionsAdapter:Lcom/google/android/youtube/videos/adapter/MovieSuggestionsAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingSuggestions:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->updateVisibilities()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingSuggestions:Ljava/util/List;

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingPosterUpdatedNotification:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/MoviesHelper;->onPostersUpdated()V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/MoviesHelper;->pendingPosterUpdatedNotification:Z

    goto :goto_0
.end method

.method public onVideoMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    return-void
.end method
