.class public Lcom/google/android/youtube/videos/ui/TouchAwareListView;
.super Landroid/widget/ListView;
.source "TouchAwareListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;
    }
.end annotation


# instance fields
.field private final commitRunnable:Ljava/lang/Runnable;

.field private isBeingTouched:Z

.field private listener:Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

.field private pendingIsBeingTouched:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->createCommitRunnable()Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->commitRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->createCommitRunnable()Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->commitRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->createCommitRunnable()Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->commitRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/ui/TouchAwareListView;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/TouchAwareListView;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->commitIsBeingTouched()V

    return-void
.end method

.method private commitIsBeingTouched()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched:Z

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->pendingIsBeingTouched:Z

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->pendingIsBeingTouched:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->listener:Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->listener:Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;->onTouchStateChanged(Z)V

    :cond_0
    return-void
.end method

.method private createCommitRunnable()Ljava/lang/Runnable;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/ui/TouchAwareListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView$1;-><init>(Lcom/google/android/youtube/videos/ui/TouchAwareListView;)V

    return-object v0
.end method

.method private getTopFromThis(Landroid/view/View;)I
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    if-ne p0, v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getTopFromThis(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method private isClipped(Landroid/view/View;)Z
    .locals 3
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private keyCodeToFocusDirection(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x42

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x11

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x82

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x21

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->pendingIsBeingTouched:Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->commitIsBeingTouched()V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    if-eq v0, v2, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->pendingIsBeingTouched:Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->commitRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return v2
.end method

.method public isBeingTouched()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isBeingTouched:Z

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v9, 0x82

    const/4 v7, 0x1

    const/16 v10, 0x21

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->keyCodeToFocusDirection(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->findFocus()Landroid/view/View;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v8

    if-nez v8, :cond_2

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    :cond_1
    :goto_0
    return v7

    :cond_2
    invoke-virtual {v2, v1}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    if-ltz v3, :cond_3

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->smoothScrollToPosition(I)V

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :cond_3
    const/16 v8, 0x42

    if-eq v1, v8, :cond_4

    const/16 v8, 0x11

    if-ne v1, v8, :cond_5

    :cond_4
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_6

    if-eq v1, v10, :cond_7

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne v0, v8, :cond_8

    if-ne v1, v9, :cond_8

    :cond_7
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto :goto_0

    :cond_8
    if-ne v1, v10, :cond_9

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getFirstVisiblePosition()I

    move-result v8

    if-eqz v8, :cond_a

    :cond_9
    if-ne v1, v9, :cond_c

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getLastVisiblePosition()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v8, v9, :cond_c

    :cond_a
    if-ne v1, v10, :cond_b

    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setSelection(I)V

    :goto_1
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p0, v8}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setSelection(I)V

    goto :goto_1

    :cond_c
    const v8, 0x3e19999a

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getHeight()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v8, v9

    float-to-int v5, v8

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getTopFromThis(Landroid/view/View;)I

    move-result v6

    if-le v6, v5, :cond_d

    move v5, v6

    :cond_d
    if-ne v1, v10, :cond_e

    add-int/lit8 v8, v0, -0x1

    invoke-virtual {p0, v8, v5}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setSelectionFromTop(II)V

    goto :goto_0

    :cond_e
    add-int/lit8 v8, v0, 0x1

    invoke-virtual {p0, v8, v5}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->setSelectionFromTop(II)V

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    if-gez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/ListView;->onLayout(ZIIII)V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getChildCount()I

    move-result v1

    and-int/lit8 v6, p1, 0x2

    if-eqz v6, :cond_0

    const/4 v5, 0x0

    const/4 v4, 0x1

    move v2, v1

    :goto_0
    move v3, v5

    :goto_1
    if-eq v3, v2, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->isClipped(Landroid/view/View;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    :goto_2
    return v6

    :cond_0
    add-int/lit8 v5, v1, -0x1

    const/4 v4, -0x1

    const/4 v2, -0x1

    goto :goto_0

    :cond_1
    add-int/2addr v3, v4

    goto :goto_1

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    move-result v6

    goto :goto_2
.end method

.method public setOnTouchStateChangedListener(Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/TouchAwareListView;->listener:Lcom/google/android/youtube/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

    return-void
.end method
