.class public abstract Lcom/google/android/youtube/videos/ui/StatusHelper;
.super Ljava/lang/Object;
.source "StatusHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/StatusHelper$1;,
        Lcom/google/android/youtube/videos/ui/StatusHelper$ViewStatusHelper;,
        Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private errorIcon:Landroid/view/View;

.field private messageView:Landroid/widget/TextView;

.field private progress:Landroid/widget/ProgressBar;

.field private retryButton:Landroid/widget/Button;

.field private final retryListener:Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;

.field protected statusView:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->retryListener:Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->context:Landroid/content/Context;

    return-void
.end method

.method public static createFromParent(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;)Lcom/google/android/youtube/videos/ui/StatusHelper;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;

    new-instance v0, Lcom/google/android/youtube/videos/ui/StatusHelper$ViewStatusHelper;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/youtube/videos/ui/StatusHelper$ViewStatusHelper;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;Lcom/google/android/youtube/videos/ui/StatusHelper$1;)V

    return-object v0
.end method


# virtual methods
.method protected abstract ensureHidden()V
.end method

.method protected abstract ensureVisible()V
.end method

.method public hide()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->ensureHidden()V

    return-void
.end method

.method public init()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->statusView:Landroid/view/View;

    const v1, 0x7f07007d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->statusView:Landroid/view/View;

    const v1, 0x7f07007c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->progress:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->statusView:Landroid/view/View;

    const v1, 0x7f07007e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->retryButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->statusView:Landroid/view/View;

    const v1, 0x7f07007b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->errorIcon:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->retryButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->ensureHidden()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->retryListener:Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/StatusHelper$OnRetryListener;->onRetry()V

    return-void
.end method

.method public setErrorMessage(Ljava/lang/CharSequence;Z)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->errorIcon:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->retryButton:Landroid/widget/Button;

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->ensureVisible()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public setLoading()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->errorIcon:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->retryButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->progress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->ensureVisible()V

    return-void
.end method

.method public setMessage(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/youtube/videos/ui/StatusHelper;->setMessage(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;Z)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    const/4 v0, 0x0

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->errorIcon:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->retryButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->progress:Landroid/widget/ProgressBar;

    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/StatusHelper;->messageView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/StatusHelper;->ensureVisible()V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
