.class public Lcom/google/android/youtube/videos/ui/PlayPanelHelper;
.super Ljava/lang/Object;
.source "PlayPanelHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private downloadStatus:Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

.field private final downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

.field private final itemView:Lcom/google/android/youtube/videos/ui/WatchDetailsItemView;

.field private final listener:Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;

.field private final pinMessage:Landroid/widget/TextView;

.field private final playButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;Z)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->context:Landroid/content/Context;

    const-string v0, "listener cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->listener:Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;

    const-string v0, "playPanel cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v0, 0x7f070092

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/WatchDetailsItemView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->itemView:Lcom/google/android/youtube/videos/ui/WatchDetailsItemView;

    const v0, 0x7f070093

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->playButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->playButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->playButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f07002d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/DownloadView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p4, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setVisibility(I)V

    :cond_0
    const v0, 0x7f070031

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->pinMessage:Landroid/widget/TextView;

    return-void
.end method

.method private getProgress(Lcom/google/android/youtube/videos/store/VideoDownloadStatus;)I
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

    iget-object v0, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->bytesDownloaded:Ljava/lang/Long;

    iget-object v1, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->downloadSize:Ljava/lang/Long;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    div-long/2addr v2, v4

    long-to-int v2, v2

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->downloadStatus:Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->listener:Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->downloadStatus:Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;->onPinClick(Lcom/google/android/youtube/videos/store/VideoDownloadStatus;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->playButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->listener:Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;->onPlay()V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/16 v0, 0x55

    if-eq p2, v0, :cond_0

    const/16 v0, 0x7e

    if-ne p2, v0, :cond_2

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->listener:Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper$Listener;->onPlay()V

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onThumbnail(Landroid/graphics/Bitmap;Z)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->itemView:Lcom/google/android/youtube/videos/ui/WatchDetailsItemView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/videos/ui/WatchDetailsItemView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public onVideoDownloadStatus(Lcom/google/android/youtube/videos/store/VideoDownloadStatus;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->downloadStatus:Lcom/google/android/youtube/videos/store/VideoDownloadStatus;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->pinMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->context:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->getDownloadStatusText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    iget-boolean v1, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinned:Z

    iget-object v2, p1, Lcom/google/android/youtube/videos/store/VideoDownloadStatus;->pinningStatus:Ljava/lang/Integer;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->getProgress(Lcom/google/android/youtube/videos/store/VideoDownloadStatus;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/ui/DownloadView;->setPinState(ZLjava/lang/Integer;I)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/PlayPanelHelper;->downloadView:Lcom/google/android/youtube/videos/ui/DownloadView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setTitle(Ljava/lang/String;)V

    return-void
.end method
