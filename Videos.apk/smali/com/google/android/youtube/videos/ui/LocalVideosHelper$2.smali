.class Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;
.super Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;
.source "LocalVideosHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->getCollapsibleHeadingOutline(ILcom/google/android/youtube/core/adapter/Outline;)Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

.field final synthetic val$bucketId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;Lcom/google/android/youtube/core/adapter/Outline;Ljava/lang/String;)V
    .locals 0
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Lcom/google/android/youtube/core/adapter/Outline$ViewType;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/youtube/core/adapter/Outline;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    iput-object p6, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;->val$bucketId:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/adapter/Outline$ViewType;Ljava/lang/String;Lcom/google/android/youtube/core/adapter/Outline;)V

    return-void
.end method


# virtual methods
.method protected onToggledBodyVisibility()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/youtube/videos/adapter/CollapsiblePanelHeadingOutline;->onToggledBodyVisibility()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;->bodyOutline:Lcom/google/android/youtube/core/adapter/Outline;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/adapter/Outline;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->collapsedBuckets:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$200(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;->val$bucketId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;->this$0:Lcom/google/android/youtube/videos/ui/LocalVideosHelper;

    # getter for: Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->collapsedBuckets:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/youtube/videos/ui/LocalVideosHelper;->access$200(Lcom/google/android/youtube/videos/ui/LocalVideosHelper;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/LocalVideosHelper$2;->val$bucketId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
