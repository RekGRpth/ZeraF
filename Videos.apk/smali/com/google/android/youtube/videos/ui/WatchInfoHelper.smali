.class public Lcom/google/android/youtube/videos/ui/WatchInfoHelper;
.super Ljava/lang/Object;
.source "WatchInfoHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;


# instance fields
.field private final actors:Landroid/widget/TextView;

.field private final actorsHeader:Landroid/widget/TextView;

.field private final directors:Landroid/widget/TextView;

.field private final directorsHeader:Landroid/widget/TextView;

.field private final edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final expandButton:Landroid/view/View;

.field private final expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

.field private final gmsPlusOneClient:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

.field private final movieInfoLayout:Landroid/view/View;

.field private final networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

.field private final plusOneButtonFragment:Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;

.field private final producers:Landroid/widget/TextView;

.field private final producersHeader:Landroid/widget/TextView;

.field private final ratingScheme:Ljava/lang/String;

.field private final resources:Landroid/content/res/Resources;

.field private final storyline:Landroid/widget/TextView;

.field private final subheading:Landroid/widget/TextView;

.field private final title:Landroid/widget/TextView;

.field private final writers:Landroid/widget/TextView;

.field private final writersHeader:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lvedroid/support/v4/app/FragmentActivity;Landroid/view/View;Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Lcom/google/android/youtube/core/utils/NetworkStatus;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 4
    .param p1    # Lvedroid/support/v4/app/FragmentActivity;
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;
    .param p4    # Lcom/google/android/youtube/core/utils/NetworkStatus;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # Ljava/lang/String;
    .param p8    # Lcom/google/android/youtube/videos/logging/EventLogger;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    iput-object p7, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->ratingScheme:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const v0, 0x7f07008e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->gmsPlusOneClient:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    invoke-virtual {p1}, Lvedroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p6}, Lvedroid/support/v4/app/FragmentManager;->findFragmentById(I)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->plusOneButtonFragment:Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f070017

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->title:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f07008f

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->subheading:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f07008d

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->storyline:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f070084

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->actorsHeader:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f070086

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->directorsHeader:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f070088

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->writersHeader:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f07008a

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->producersHeader:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f070085

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->actors:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f070087

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->directors:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f070089

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->writers:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    const v1, 0x7f07008b

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->producers:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    check-cast v0, Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->setVerticalFadingEdgeEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->setOnFadingEdgeVisibilityChangedListener(Lcom/google/android/youtube/videos/ui/EdgeFadingView$OnFadingEdgeVisibilityChangedListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->getMovieInfoCollapsedHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    const v1, 0x7f07005c

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    const v1, 0x7f070090

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/CheckedImageView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    :goto_0
    return-void

    :cond_0
    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandButton:Landroid/view/View;

    iput-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    goto :goto_0
.end method

.method private findTextViewById(Landroid/view/View;I)Landroid/widget/TextView;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private getMovieInfoCollapsedHeight()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private getRating(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->ratingScheme:Ljava/lang/String;

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->ratingScheme:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v1, v0, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x2

    goto :goto_1
.end method

.method private titleWithHdBadge(Ljava/lang/String;)Landroid/text/Spanned;
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    const v5, 0x7f02003b

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    invoke-direct {v1, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->title:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTextSize()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v6, v6, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    const/16 v4, 0x30

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-direct {v2, v1}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    new-instance v3, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v5

    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-object v3
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    if-eqz v0, :cond_1

    const/4 v1, -0x2

    :goto_1
    invoke-direct {v3, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMoreDetailsButtonClicked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->getMovieInfoCollapsedHeight()I

    move-result v1

    goto :goto_1
.end method

.method public onFadingEdgeVisibilityChanged(Z)V
    .locals 5
    .param p1    # Z

    const v4, 0x7f0a00d3

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->edgeFadingView:Lcom/google/android/youtube/videos/ui/EdgeFadingView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/EdgeFadingView;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->getMovieInfoCollapsedHeight()I

    move-result v1

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandIcon:Lcom/google/android/youtube/videos/ui/CheckedImageView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0a00d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/ui/CheckedImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->expandButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method

.method protected showCredits(Ljava/util/List;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 5
    .param p2    # Landroid/widget/TextView;
    .param p3    # Landroid/widget/TextView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/TextView;",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    invoke-static {v2, v3, v0}, Lcom/google/android/youtube/core/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected showOrHidePlusOneButtonFragment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->gmsPlusOneClient:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->plusOneButtonFragment:Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->gmsPlusOneClient:Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://market.android.com/details?id=movie-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->initialize(Lcom/google/android/youtube/gmsplus1/GmsPlusOneClient;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->plusOneButtonFragment:Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;

    invoke-virtual {v0}, Lcom/google/android/youtube/gmsplus1/PlusOneButtonFragment;->reset()V

    goto :goto_0
.end method

.method public showVideo(Ljava/lang/String;Lcom/google/android/youtube/videos/store/VideoMetadata;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/store/VideoMetadata;

    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-string v2, "account cannot be null"

    invoke-static {p1, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "video cannot be null"

    invoke-static {p2, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->id:Ljava/lang/String;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->showOrHidePlusOneButtonFragment(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->storyline:Landroid/widget/TextView;

    iget-object v4, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->description:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->directors:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->directorsHeader:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->directors:Landroid/widget/TextView;

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->showCredits(Ljava/util/List;Landroid/widget/TextView;Landroid/widget/TextView;)V

    iget-object v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->writers:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->writersHeader:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->writers:Landroid/widget/TextView;

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->showCredits(Ljava/util/List;Landroid/widget/TextView;Landroid/widget/TextView;)V

    iget-object v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->actors:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->actorsHeader:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->actors:Landroid/widget/TextView;

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->showCredits(Ljava/util/List;Landroid/widget/TextView;Landroid/widget/TextView;)V

    iget-object v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->producers:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->producersHeader:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->producers:Landroid/widget/TextView;

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->showCredits(Ljava/util/List;Landroid/widget/TextView;Landroid/widget/TextView;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->movieInfoLayout:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->title:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v1, ""

    :goto_0
    iget-boolean v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->purchasedHd:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->title:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/4 v2, 0x4

    new-array v0, v2, [Ljava/lang/String;

    iget-object v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->ratings:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->getRating(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    iget v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->releaseYear:I

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0a0075

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->releaseYear:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    aput-object v2, v0, v9

    const/4 v4, 0x2

    iget v2, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->durationSecs:I

    if-lez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0a0072

    new-array v6, v9, [Ljava/lang/Object;

    iget v7, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->durationSecs:I

    div-int/lit8 v7, v7, 0x3c

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    aput-object v2, v0, v4

    const/4 v2, 0x3

    iget-boolean v4, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->surroundSoundPurchasedAndAvailable:Z

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0a00af

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_0
    aput-object v3, v0, v2

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->subheading:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->resources:Landroid/content/res/Resources;

    invoke-static {v3, v9, v0}, Lcom/google/android/youtube/core/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    iget-object v1, p2, Lcom/google/android/youtube/videos/store/VideoMetadata;->title:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->title:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/ui/WatchInfoHelper;->titleWithHdBadge(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    move-object v2, v3

    goto :goto_2

    :cond_4
    move-object v2, v3

    goto :goto_3
.end method
