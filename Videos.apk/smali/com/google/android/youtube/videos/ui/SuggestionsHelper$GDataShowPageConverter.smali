.class public Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataShowPageConverter;
.super Ljava/lang/Object;
.source "SuggestionsHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/SuggestionsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GDataShowPageConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/ResponseConverter",
        "<",
        "Lcom/google/android/youtube/core/model/Page",
        "<",
        "Lcom/google/android/youtube/core/model/Show;",
        ">;",
        "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private createAssetResourceForShow(Lcom/google/android/youtube/core/model/Show;)Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 4
    .param p1    # Lcom/google/android/youtube/core/model/Show;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Show;->selfUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreUtil;->itemUriToItemId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setId(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;->SHOW:Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;

    invoke-virtual {v1, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;->setType(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Type;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->setResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId$Builder;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Show;->title:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setTitle(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Show;->description:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->setDescription(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v2

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v3

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Show;->posterUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;->setUrl(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->addPoster(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$Builder;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->setMetadata(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Show;->posterUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public convertResponse(Lcom/google/android/youtube/core/model/Page;)Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;)",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;"
        }
    .end annotation

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->newBuilder()Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/model/Show;

    invoke-direct {p0, v3}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataShowPageConverter;->createAssetResourceForShow(Lcom/google/android/youtube/core/model/Show;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;->addResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;->build()Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    move-result-object v3

    return-object v3
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/core/model/Page;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/ui/SuggestionsHelper$GDataShowPageConverter;->convertResponse(Lcom/google/android/youtube/core/model/Page;)Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    move-result-object v0

    return-object v0
.end method
