.class final Lcom/google/android/youtube/videos/ui/ShowsHelper$ShowClickListener;
.super Ljava/lang/Object;
.source "ShowsHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemClickListener;
.implements Lcom/google/android/youtube/videos/ui/VideoListOutline$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/ui/ShowsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ShowClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;Lcom/google/android/youtube/videos/ui/ShowsHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/ui/ShowsHelper;
    .param p2    # Lcom/google/android/youtube/videos/ui/ShowsHelper$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/ShowsHelper$ShowClickListener;-><init>(Lcom/google/android/youtube/videos/ui/ShowsHelper;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/youtube/videos/ui/VideoListOutline;ZI)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/ui/VideoListOutline;
    .param p2    # Z
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    # invokes: Lcom/google/android/youtube/videos/ui/ShowsHelper;->onShowClick(ZI)V
    invoke-static {v0, p2, p3}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->access$400(Lcom/google/android/youtube/videos/ui/ShowsHelper;ZI)V

    return-void
.end method

.method public onItemLongClick(Lcom/google/android/youtube/videos/ui/VideoListOutline;ILandroid/view/View;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/ui/VideoListOutline;
    .param p2    # I
    .param p3    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/ShowsHelper$ShowClickListener;->this$0:Lcom/google/android/youtube/videos/ui/ShowsHelper;

    # invokes: Lcom/google/android/youtube/videos/ui/ShowsHelper;->onShowLongClick(ILandroid/view/View;)V
    invoke-static {v0, p2, p3}, Lcom/google/android/youtube/videos/ui/ShowsHelper;->access$500(Lcom/google/android/youtube/videos/ui/ShowsHelper;ILandroid/view/View;)V

    return-void
.end method
