.class public Lcom/google/android/youtube/videos/ui/DownloadView;
.super Landroid/view/View;
.source "DownloadView.java"


# static fields
.field public static final STATE_COMPLETED:I = 0x2

.field public static final STATE_ERROR:I = 0x3

.field public static final STATE_NEW:I = 0x4

.field public static final STATE_NOT_DOWNLOADED:I = 0x0

.field public static final STATE_ONGOING:I = 0x1

.field public static final STATE_PENDING:I = 0x5


# instance fields
.field private final arcColor:I

.field private final arcPaint:Landroid/graphics/Paint;

.field private final arcRect:Landroid/graphics/RectF;

.field private final background:Landroid/graphics/Bitmap;

.field private final error:Landroid/graphics/Bitmap;

.field private final errorBackgroundColor:I

.field private final internalPaddingPx:F

.field private final pendingArcColor:I

.field private final pendingBackgroundColor:I

.field private final pinned:Landroid/graphics/Bitmap;

.field private progress:I

.field private state:I

.field private title:Ljava/lang/String;

.field private final unpinned:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/videos/ui/DownloadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/ui/DownloadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020034

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->background:Landroid/graphics/Bitmap;

    const v1, 0x7f020033

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->unpinned:Landroid/graphics/Bitmap;

    const v1, 0x7f020036

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->pinned:Landroid/graphics/Bitmap;

    const v1, 0x7f020037

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->error:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcRect:Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const v1, 0x7f080017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcColor:I

    const v1, 0x7f080018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->pendingArcColor:I

    const v1, 0x7f080019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->pendingBackgroundColor:I

    const v1, 0x7f08001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->errorBackgroundColor:I

    const v1, 0x7f090027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->internalPaddingPx:F

    return-void
.end method

.method private setProgress(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->progress:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->progress:I

    iget v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->invalidate()V

    :cond_0
    return-void
.end method

.method private setState(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->state:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->state:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->updateContentDescription()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->invalidate()V

    :cond_0
    return-void
.end method

.method private updateContentDescription()V
    .locals 5

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->state:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->title:Ljava/lang/String;

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :pswitch_0
    const v0, 0x7f0a00d5

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0a00d7

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a00d8

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a00d7

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a00d6

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0a00d9

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->title:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v9, 0x43870000

    const/high16 v3, 0x43b40000

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->getPaddingLeft()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->getPaddingTop()I

    move-result v7

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->background:Landroid/graphics/Bitmap;

    int-to-float v1, v6

    int-to-float v5, v7

    invoke-virtual {p1, v0, v1, v5, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->state:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->unpinned:Landroid/graphics/Bitmap;

    int-to-float v1, v6

    int-to-float v2, v7

    invoke-virtual {p1, v0, v1, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->pinned:Landroid/graphics/Bitmap;

    int-to-float v1, v6

    int-to-float v2, v7

    invoke-virtual {p1, v0, v1, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->pendingBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->pendingArcColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcRect:Landroid/graphics/RectF;

    iget v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->progress:I

    mul-int/lit16 v0, v0, 0x168

    div-int/lit8 v0, v0, 0x64

    int-to-float v3, v0

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->pinned:Landroid/graphics/Bitmap;

    int-to-float v1, v6

    int-to-float v2, v7

    invoke-virtual {p1, v0, v1, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcRect:Landroid/graphics/RectF;

    iget v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->progress:I

    mul-int/lit16 v0, v0, 0x168

    div-int/lit8 v0, v0, 0x64

    int-to-float v3, v0

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->pinned:Landroid/graphics/Bitmap;

    int-to-float v1, v6

    int-to-float v2, v7

    invoke-virtual {p1, v0, v1, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->pinned:Landroid/graphics/Bitmap;

    int-to-float v1, v6

    int-to-float v2, v7

    invoke-virtual {p1, v0, v1, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->errorBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->error:Landroid/graphics/Bitmap;

    int-to-float v1, v6

    int-to-float v2, v7

    invoke-virtual {p1, v0, v1, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->arcRect:Landroid/graphics/RectF;

    int-to-float v3, v0

    iget v4, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->internalPaddingPx:F

    add-float/2addr v3, v4

    int-to-float v4, v1

    iget v5, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->internalPaddingPx:F

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->background:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    add-int/2addr v5, v0

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->internalPaddingPx:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->background:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    add-int/2addr v6, v1

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->internalPaddingPx:F

    sub-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->background:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->background:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/youtube/videos/ui/DownloadView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setPinState(ZLjava/lang/Integer;I)V
    .locals 3
    .param p1    # Z
    .param p2    # Ljava/lang/Integer;
    .param p3    # I

    const/4 v2, 0x3

    const/4 v1, 0x4

    if-nez p1, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/ui/DownloadView;->setState(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/DownloadView;->setState(I)V

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setState(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p3}, Lcom/google/android/youtube/videos/ui/DownloadView;->setProgress(I)V

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/DownloadView;->setState(I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/ui/DownloadView;->setState(I)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/DownloadView;->setState(I)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p3}, Lcom/google/android/youtube/videos/ui/DownloadView;->setProgress(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/DownloadView;->setState(I)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/ui/DownloadView;->setState(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->title:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/DownloadView;->title:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/DownloadView;->updateContentDescription()V

    :cond_0
    return-void
.end method
