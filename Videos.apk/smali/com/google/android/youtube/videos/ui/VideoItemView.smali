.class public abstract Lcom/google/android/youtube/videos/ui/VideoItemView;
.super Lcom/google/android/youtube/videos/ui/NonResizingFrameLayout;
.source "VideoItemView.java"

# interfaces
.implements Lcom/google/android/youtube/videos/ui/BitmapLoader$BitmapView;


# instance fields
.field private final backgroundTint:I

.field private backgroundView:Landroid/widget/ImageView;

.field private final backgroundViewMatrix:Landroid/graphics/Matrix;

.field private backgroundViewMatrixDirty:Z

.field private final backgroundViewMatrixValues:[F

.field private final defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field protected final defaultForegroundDrawableId:I

.field protected final detailsView:Landroid/view/View;

.field private final dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private isNoThumbnailIcon:Z

.field private matrixThumbnailHeight:I

.field private matrixThumbnailWidth:I

.field private matrixViewHeight:I

.field private matrixViewLeftCrop:I

.field private matrixViewWidth:I

.field private final noThumbnailBackgroundColor:I

.field private final styleWidths:[I

.field private thumbnail:Landroid/graphics/Bitmap;

.field private thumbnailTag:Ljava/lang/Object;

.field private thumbnailView:Landroid/widget/ImageView;

.field private thumbnailWidth:I

.field private final thumbnailWidthHeightRatio:F


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IIIF)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # F

    const/16 v7, 0x33

    const v8, -0x4dcceced

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/ui/VideoItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIIFII)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IIIFII)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # F
    .param p7    # I
    .param p8    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/ui/NonResizingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f08000f

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->noThumbnailBackgroundColor:I

    invoke-virtual {v4, p5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->length()I

    move-result v5

    if-lez v5, :cond_0

    rem-int/lit8 v7, v5, 0x2

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    const/4 v7, 0x1

    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "invalid styleBoundaryLength: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    new-array v7, v5, [I

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->styleWidths:[I

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_1

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->styleWidths:[I

    const/4 v8, 0x0

    invoke-virtual {v6, v2, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    aput v8, v7, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundViewMatrix:Landroid/graphics/Matrix;

    const/16 v7, 0x9

    new-array v7, v7, [F

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundViewMatrixValues:[F

    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundTint:I

    new-instance v7, Landroid/widget/ImageView;

    invoke-direct {v7, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    move/from16 v0, p7

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->setBackgroundViewImageAlpha(I)V

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    move/from16 v0, p8

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    if-eqz p8, :cond_2

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    sget-object v8, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    move/from16 v0, p8

    invoke-virtual {v7, v0, v8}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_2
    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/ui/VideoItemView;->addView(Landroid/view/View;)V

    new-instance v7, Landroid/widget/ImageView;

    invoke-direct {v7, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    iget v8, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->noThumbnailBackgroundColor:I

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/ui/VideoItemView;->addView(Landroid/view/View;)V

    move/from16 v0, p6

    iput v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidthHeightRatio:F

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, p4, p0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->detailsView:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->detailsView:Landroid/view/View;

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/ui/VideoItemView;->addView(Landroid/view/View;)V

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    const v8, 0x7f01000c

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v3, v9}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v7, v3, Landroid/util/TypedValue;->resourceId:I

    iput v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->defaultForegroundDrawableId:I

    iget v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->defaultForegroundDrawableId:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/ui/VideoItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    const/4 v7, 0x2

    new-array v1, v7, [Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    const v9, 0x7f08000e

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-direct {v8, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v8, v1, v7

    const/4 v7, 0x1

    iget v8, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->defaultForegroundDrawableId:I

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    aput-object v8, v1, v7

    new-instance v7, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v7, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v7, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private setBackgroundViewImageAlpha(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAlpha(I)V

    return-void
.end method

.method public static setFlippedImageMatrix([FIIIII)V
    .locals 8
    .param p0    # [F
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/high16 v7, 0x3f000000

    const/4 v6, 0x0

    mul-int v3, p2, p5

    mul-int v4, p4, p3

    if-le v3, v4, :cond_0

    int-to-float v3, p5

    int-to-float v4, p3

    div-float v2, v3, v4

    int-to-float v3, p4

    int-to-float v4, p2

    mul-float/2addr v4, v2

    sub-float/2addr v3, v4

    mul-float v0, v3, v7

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x0

    neg-float v4, v2

    aput v4, p0, v3

    const/4 v3, 0x1

    aput v6, p0, v3

    const/4 v3, 0x2

    sub-int v4, p4, p1

    int-to-float v4, v4

    add-float v5, v0, v7

    sub-float/2addr v4, v5

    aput v4, p0, v3

    const/4 v3, 0x3

    aput v6, p0, v3

    const/4 v3, 0x4

    aput v2, p0, v3

    const/4 v3, 0x5

    add-float v4, v1, v7

    aput v4, p0, v3

    const/4 v3, 0x6

    aput v6, p0, v3

    const/4 v3, 0x7

    aput v6, p0, v3

    const/16 v3, 0x8

    const/high16 v4, 0x3f800000

    aput v4, p0, v3

    return-void

    :cond_0
    int-to-float v3, p4

    int-to-float v4, p2

    div-float v2, v3, v4

    const/4 v0, 0x0

    int-to-float v3, p5

    int-to-float v4, p3

    mul-float/2addr v4, v2

    sub-float/2addr v3, v4

    mul-float v1, v3, v7

    goto :goto_0
.end method

.method private updateBackgroundViewImageMatrix()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundViewMatrixValues:[F

    iget v1, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixViewLeftCrop:I

    iget v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixThumbnailWidth:I

    iget v3, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixThumbnailHeight:I

    iget v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixViewWidth:I

    iget v5, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixViewHeight:I

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/videos/ui/VideoItemView;->setFlippedImageMatrix([FIIIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundViewMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundViewMatrixValues:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundViewMatrixDirty:Z

    return-void
.end method


# virtual methods
.method public getThumbnailTag()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailTag:Ljava/lang/Object;

    return-object v0
.end method

.method protected final getYearAndDurationText(II)Ljava/lang/String;
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    if-nez p1, :cond_0

    move-object v1, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object v1, v3, v7

    aput-object v0, v3, v6

    invoke-static {v2, v6, v3}, Lcom/google/android/youtube/core/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0075

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0072

    new-array v4, v6, [Ljava/lang/Object;

    div-int/lit8 v5, p2, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v4, 0x0

    sub-int v1, p4, p2

    sub-int v0, p5, p3

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    iget v3, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidth:I

    invoke-virtual {v2, v3, v4, v1, v0}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    iget v3, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidth:I

    invoke-virtual {v2, v4, v4, v3, v0}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->detailsView:Landroid/view/View;

    iget v3, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidth:I

    invoke-virtual {v2, v3, v4, v1, v0}, Landroid/view/View;->layout(IIII)V

    iget v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixViewLeftCrop:I

    iget v3, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidth:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixViewWidth:I

    if-ne v2, v1, :cond_0

    iget v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixViewHeight:I

    if-eq v2, v0, :cond_1

    :cond_0
    iget v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidth:I

    iput v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixViewLeftCrop:I

    iput v1, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixViewWidth:I

    iput v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixViewHeight:I

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnail:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->isNoThumbnailIcon:Z

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->updateBackgroundViewImageMatrix()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundViewMatrixDirty:Z

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/high16 v7, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->styleWidths:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->styleWidths:[I

    add-int/lit8 v5, v2, 0x1

    aget v4, v4, v5

    if-lt v3, v4, :cond_0

    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->styleWidths:[I

    aget v4, v4, v2

    iput v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidth:I

    iget v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidth:I

    sub-int v1, v3, v4

    iget v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidthHeightRatio:F

    div-float/2addr v4, v5

    float-to-int v0, v4

    invoke-virtual {p0, v3, v0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->setMeasuredDimension(II)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    iget v5, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailWidth:I

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/ImageView;->measure(II)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/ImageView;->measure(II)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->detailsView:Landroid/view/View;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method public setDimmedStyle(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->dimmedForegroundDrawable:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->defaultForegroundDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;Z)V
    .locals 5
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnail:Landroid/graphics/Bitmap;

    if-ne v2, p1, :cond_0

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->isNoThumbnailIcon:Z

    if-eq v2, p2, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnail:Landroid/graphics/Bitmap;

    iput-boolean p2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->isNoThumbnailIcon:Z

    if-eqz p2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    iget v3, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->noThumbnailBackgroundColor:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    iget v3, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundTint:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p1, :cond_5

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundViewMatrixDirty:Z

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixThumbnailWidth:I

    if-ne v2, v1, :cond_3

    iget v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixThumbnailHeight:I

    if-eq v2, v0, :cond_4

    :cond_3
    iput v1, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixThumbnailWidth:I

    iput v0, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->matrixThumbnailHeight:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/VideoItemView;->updateBackgroundViewImageMatrix()V

    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundView:Landroid/widget/ImageView;

    iget v3, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->backgroundTint:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method public setThumbnailTag(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/VideoItemView;->thumbnailTag:Ljava/lang/Object;

    return-void
.end method
