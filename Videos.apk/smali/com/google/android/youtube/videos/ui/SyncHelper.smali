.class public Lcom/google/android/youtube/videos/ui/SyncHelper;
.super Ljava/lang/Object;
.source "SyncHelper.java"

# interfaces
.implements Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final activity:Landroid/app/Activity;

.field private authenticationError:Ljava/lang/Exception;

.field private final gcmRegistrationManager:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

.field private initializationErrorMessage:Ljava/lang/CharSequence;

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private purchasesError:Ljava/lang/Exception;

.field private resyncSeasonEpisodes:Z

.field private final signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

.field private state:I

.field private syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/accounts/SignInManager;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p4    # Lcom/google/android/youtube/videos/accounts/SignInManager;
    .param p5    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    const-string v0, "gcmRegistrationManager cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->gcmRegistrationManager:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v0, "signInManager cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/SignInManager;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    const-string v0, "purchaseStoreSync cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->listeners:Ljava/util/Set;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->state:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/ui/SyncHelper;)Lcom/google/android/youtube/core/async/Callback;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/ui/SyncHelper;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/youtube/videos/ui/SyncHelper;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/SyncHelper;
    .param p1    # Lcom/google/android/youtube/core/async/Callback;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/ui/SyncHelper;I)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/SyncHelper;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/youtube/videos/ui/SyncHelper;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ui/SyncHelper;
    .param p1    # Ljava/lang/Exception;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->purchasesError:Ljava/lang/Exception;

    return-object p1
.end method

.method private createSyncPurchasesCallback()Lcom/google/android/youtube/core/async/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/ui/SyncHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/ui/SyncHelper$1;-><init>(Lcom/google/android/youtube/videos/ui/SyncHelper;)V

    return-object v0
.end method

.method private resetForAccount(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->account:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->initializationErrorMessage:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->authenticationError:Ljava/lang/Exception;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->purchasesError:Ljava/lang/Exception;

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->resyncSeasonEpisodes:Z

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    return-void
.end method

.method private setState(I)V
    .locals 3
    .param p1    # I

    iget v2, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->state:I

    if-eq v2, p1, :cond_0

    iput p1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->state:I

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;->onSyncStateChanged(I)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->account:Ljava/lang/String;

    return-object v0
.end method

.method public getInitializationErrorMessage()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->initializationErrorMessage:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getState()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->state:I

    return v0
.end method

.method public init(Ljava/lang/String;ZZ)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z

    const/4 v2, 0x1

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->resyncSeasonEpisodes:Z

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->reset()V

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/accounts/SignInManager;->chooseAccount(Landroid/app/Activity;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object p1

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->chooseSingleAccount()Ljava/lang/String;

    move-result-object p1

    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->reset()V

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/accounts/SignInManager;->chooseAccount(Landroid/app/Activity;Z)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->resetForAccount(Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p1, v1, p0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getUserAuth(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    goto :goto_0
.end method

.method public initForError(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->reset()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->initializationErrorMessage:Ljava/lang/CharSequence;

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v1, v2, p1, p2, p3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v1, v2, p1, p2, p3}, Lcom/google/android/youtube/videos/accounts/SignInManager;->onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->account:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getUserAuth(Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAuthenticated(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v0, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->account:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->gcmRegistrationManager:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->ensureRegistered(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->signInManager:Lcom/google/android/youtube/videos/accounts/SignInManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->createSyncPurchasesCallback()Lcom/google/android/youtube/core/async/Callback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->resyncSeasonEpisodes:Z

    iget-object v2, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->syncPurchasesCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;ZLcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    iput-object p1, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->account:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->authenticationError:Ljava/lang/Exception;

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    return-void
.end method

.method public onNotAuthenticated()V
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->setState(I)V

    return-void
.end method

.method public removeListener(Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/ui/SyncHelper$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/ui/SyncHelper;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/ui/SyncHelper;->resetForAccount(Ljava/lang/String;)V

    return-void
.end method
