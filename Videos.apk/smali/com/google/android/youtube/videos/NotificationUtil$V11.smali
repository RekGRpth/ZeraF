.class Lcom/google/android/youtube/videos/NotificationUtil$V11;
.super Lcom/google/android/youtube/videos/NotificationUtil;
.source "NotificationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/NotificationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V11"
.end annotation


# instance fields
.field private final disableNotificationSounds:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/NotificationUtil;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0x11

    if-ne v0, v1, :cond_0

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    const-string v1, "/JOP40C/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->disableNotificationSounds:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private build(Landroid/app/Notification$Builder;)Landroid/app/Notification;
    .locals 1
    .param p1    # Landroid/app/Notification$Builder;

    invoke-virtual {p1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method private getNewEpisodesBuilder(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Landroid/app/PendingIntent;
    .param p5    # Landroid/app/PendingIntent;

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f02004e

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->scaleBitmapForNotification(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->disableNotificationSounds:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p5}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private setNotificationBigStyle(Landroid/app/Notification$Builder;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/app/Notification$Builder;
    .param p2    # Landroid/app/PendingIntent;
    .param p3    # Landroid/app/PendingIntent;
    .param p4    # Ljava/lang/String;

    new-instance v0, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v0}, Landroid/app/Notification$BigTextStyle;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v2, 0x7f0a00ce

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p4, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    invoke-virtual {p1, v0}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f020051

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v4, 0x7f0a0045

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p2}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f020050

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v4, 0x7f0a0048

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    return-void
.end method

.method private setProgressV14(Landroid/app/Notification$Builder;IIZ)V
    .locals 0
    .param p1    # Landroid/app/Notification$Builder;
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    invoke-virtual {p1, p2, p3, p4}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    return-void
.end method


# virtual methods
.method public createAtHomeTrackingNotification(Ljava/lang/String;Ljava/lang/String;IIZLandroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Z
    .param p6    # Landroid/app/PendingIntent;

    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f02004f

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    invoke-direct {p0, v0, p3, p4, p5}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->setProgressV14(Landroid/app/Notification$Builder;IIZ)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method

.method public createDownloadCompletedNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    .param p2    # Landroid/app/PendingIntent;
    .param p3    # Landroid/app/PendingIntent;

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    invoke-direct {v3, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->disableNotificationSounds:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const v3, 0x7f02004e

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v4, 0x7f0a00c8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v4, 0x7f0a00c9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public createDownloadErrorNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;
    .param p2    # Landroid/app/PendingIntent;
    .param p3    # Landroid/app/PendingIntent;

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    invoke-direct {v3, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->disableNotificationSounds:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const v3, 0x7f02004e

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v4, 0x7f0a00ca

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v4, 0x7f0a00cb

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public createDownloadsOngoingNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .param p2    # Landroid/app/PendingIntent;

    const/4 v6, 0x1

    const/4 v1, 0x0

    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f02004e

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    iget-boolean v2, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotalIsIndeterminate:Z

    if-eqz v2, :cond_1

    :goto_0
    const/16 v2, 0x64

    iget-boolean v3, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotalIsIndeterminate:Z

    invoke-direct {p0, v0, v2, v1, v3}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->setProgressV14(Landroid/app/Notification$Builder;IIZ)V

    :cond_0
    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v4, 0x7f0a00c4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v4, 0x7f0a00c5

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->getDownloadProgressText(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    iget v3, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->count:I

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setNumber(I)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v2

    return-object v2

    :cond_1
    iget-wide v2, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTransfered:J

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    iget-wide v4, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotal:J

    div-long/2addr v2, v4

    long-to-int v1, v2

    goto :goto_0
.end method

.method public createNewEpisodeNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Landroid/app/PendingIntent;
    .param p5    # Landroid/app/PendingIntent;
    .param p6    # Landroid/app/PendingIntent;
    .param p7    # Landroid/app/PendingIntent;

    const/4 v7, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v3, 0x7f0a00cc

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v3, 0x7f0a00ce

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->getNewEpisodesBuilder(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_0

    invoke-direct {p0, v6, p5, p6, p1}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->setNotificationBigStyle(Landroid/app/Notification$Builder;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public createNewEpisodesNotification(ILjava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 8
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Landroid/app/PendingIntent;
    .param p5    # Landroid/app/PendingIntent;

    const/4 v5, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v3, 0x7f0a00cd

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p2, v4, v7

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    const v3, 0x7f0a00cf

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->getNewEpisodesBuilder(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/NotificationUtil$V11;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method protected scaleBitmapForNotification(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1    # Landroid/graphics/Bitmap;

    if-nez p1, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    iget-object v7, p0, Lcom/google/android/youtube/videos/NotificationUtil$V11;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x1050006

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const v7, 0x1050005

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float v7, v2, v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    div-float v8, v1, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v3

    float-to-int v6, v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v3

    float-to-int v0, v7

    const/4 v7, 0x0

    invoke-static {p1, v6, v0, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_0
.end method
