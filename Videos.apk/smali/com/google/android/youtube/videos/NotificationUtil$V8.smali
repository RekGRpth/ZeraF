.class Lcom/google/android/youtube/videos/NotificationUtil$V8;
.super Lcom/google/android/youtube/videos/NotificationUtil;
.source "NotificationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/NotificationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V8"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/NotificationUtil;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public createAtHomeTrackingNotification(Ljava/lang/String;Ljava/lang/String;IIZLandroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Z
    .param p6    # Landroid/app/PendingIntent;

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    const/4 v1, 0x0

    iput v1, v0, Landroid/app/Notification;->defaults:I

    const v1, 0x7f02004f

    iput v1, v0, Landroid/app/Notification;->icon:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2, p6}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public createDownloadCompletedNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    .param p2    # Landroid/app/PendingIntent;
    .param p3    # Landroid/app/PendingIntent;

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Landroid/app/Notification;->defaults:I

    const v1, 0x7f02004e

    iput v1, v0, Landroid/app/Notification;->icon:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v2, 0x7f0a00c8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iput-object p3, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v4, 0x7f0a00c9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, p2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v1, 0x8

    iput v1, v0, Landroid/app/Notification;->flags:I

    return-object v0
.end method

.method public createDownloadErrorNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;
    .param p2    # Landroid/app/PendingIntent;
    .param p3    # Landroid/app/PendingIntent;

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Landroid/app/Notification;->defaults:I

    const v1, 0x7f02004e

    iput v1, v0, Landroid/app/Notification;->icon:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v2, 0x7f0a00ca

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iput-object p3, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v4, 0x7f0a00cb

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, p2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v1, 0x8

    iput v1, v0, Landroid/app/Notification;->flags:I

    return-object v0
.end method

.method public createDownloadsOngoingNotification(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .param p2    # Landroid/app/PendingIntent;

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    const/4 v1, 0x0

    iput v1, v0, Landroid/app/Notification;->defaults:I

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Landroid/app/Notification;->when:J

    const v1, 0x7f02004e

    iput v1, v0, Landroid/app/Notification;->icon:I

    const/16 v1, 0x8

    iput v1, v0, Landroid/app/Notification;->flags:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v2, 0x7f0a00c4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v3, 0x7f0a00c5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/NotificationUtil$V8;->getDownloadProgressText(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, p2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public createNewEpisodeNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Landroid/app/PendingIntent;
    .param p5    # Landroid/app/PendingIntent;
    .param p6    # Landroid/app/PendingIntent;
    .param p7    # Landroid/app/PendingIntent;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v3, 0x7f0a00cd

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v3, 0x7f0a00ce

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/NotificationUtil$V8;->getNewEpisodesNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public createNewEpisodesNotification(ILjava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Landroid/app/PendingIntent;
    .param p5    # Landroid/app/PendingIntent;

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v3, 0x7f0a00cd

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p2, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    const v3, 0x7f0a00cf

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/videos/NotificationUtil$V8;->getNewEpisodesNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public getNewEpisodesNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Landroid/app/PendingIntent;
    .param p5    # Landroid/app/PendingIntent;

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    const v1, 0x7f02004e

    iput v1, v0, Landroid/app/Notification;->icon:I

    const/4 v1, 0x1

    iput v1, v0, Landroid/app/Notification;->defaults:I

    iput-object p1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/youtube/videos/NotificationUtil$V8;->context:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2, p4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iput-object p5, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    return-object v0
.end method
