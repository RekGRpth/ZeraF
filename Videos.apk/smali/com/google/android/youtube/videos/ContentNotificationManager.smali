.class public Lcom/google/android/youtube/videos/ContentNotificationManager;
.super Ljava/lang/Object;
.source "ContentNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/ContentNotificationManager$NEW_EPISODES_QUERY;
    }
.end annotation


# instance fields
.field private final config:Lcom/google/android/youtube/videos/Config;

.field private final context:Landroid/content/Context;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

.field private final purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "context cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->context:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPosterStore()Lcom/google/android/youtube/videos/store/PosterStore;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->config:Lcom/google/android/youtube/videos/Config;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStore()Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/ContentNotificationManager;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ContentNotificationManager;
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/ContentNotificationManager;->processVideoEntries(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/ContentNotificationManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/ContentNotificationManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/graphics/Bitmap;
    .param p5    # Ljava/util/List;
    .param p6    # Ljava/lang/String;

    invoke-direct/range {p0 .. p6}, Lcom/google/android/youtube/videos/ContentNotificationManager;->onNewEpisodes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method private createNewEpisodesTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "new_episodes_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getNewEpisodesCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/android/youtube/core/async/Callback;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/ContentNotificationManager$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/ContentNotificationManager$2;-><init>(Lcom/google/android/youtube/videos/ContentNotificationManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getSqlUpdateIsNewNotification([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    array-length v2, p0

    if-lez v2, :cond_0

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, p0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    const-string v2, ",\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p0, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPDATE video_userdata SET is_new_notification_dismissed = 1 WHERE pinning_account = ? AND pinning_video_id IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private onNewEpisodes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;)V
    .locals 21
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/graphics/Bitmap;
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v11, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/String;

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v3

    new-array v9, v3, [Ljava/lang/String;

    const/16 v18, 0x0

    :goto_1
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v18

    if-ge v0, v3, :cond_1

    move-object/from16 v0, p5

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    aput-object v3, v9, v18

    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/ContentNotificationManager;->context:Landroid/content/Context;

    const-string v4, "com.google.android.videos.DETAILS"

    const/4 v5, 0x0

    aget-object v6, v9, v5

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-static/range {v3 .. v9}, Lcom/google/android/youtube/videos/ContentNotificationService;->getPendingIntentForAction(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/ContentNotificationManager;->context:Landroid/content/Context;

    const-string v4, "com.google.android.videos.CANCEL"

    const/4 v5, 0x0

    aget-object v6, v9, v5

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-static/range {v3 .. v9}, Lcom/google/android/youtube/videos/ContentNotificationService;->getPendingIntentForAction(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v17

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/ContentNotificationManager;->context:Landroid/content/Context;

    const-string v4, "com.google.android.videos.PLAY"

    move-object/from16 v5, p1

    move-object/from16 v6, v20

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-static/range {v3 .. v9}, Lcom/google/android/youtube/videos/ContentNotificationService;->getPendingIntentForAction(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/ContentNotificationManager;->context:Landroid/content/Context;

    const-string v4, "com.google.android.videos.DOWNLOAD"

    move-object/from16 v5, p1

    move-object/from16 v6, v20

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-static/range {v3 .. v9}, Lcom/google/android/youtube/videos/ContentNotificationService;->getPendingIntentForAction(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/ContentNotificationManager;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/youtube/videos/NotificationUtil;->create(Landroid/content/Context;)Lcom/google/android/youtube/videos/NotificationUtil;

    move-result-object v10

    move-object/from16 v12, p6

    move-object/from16 v13, p4

    invoke-virtual/range {v10 .. v17}, Lcom/google/android/youtube/videos/NotificationUtil;->createNewEpisodeNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v19

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/ContentNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/videos/ContentNotificationManager;->createNewEpisodesTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f070009

    move-object/from16 v0, v19

    invoke-virtual {v3, v4, v5, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/ContentNotificationManager;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/youtube/videos/NotificationUtil;->create(Landroid/content/Context;)Lcom/google/android/youtube/videos/NotificationUtil;

    move-result-object v3

    array-length v4, v9

    move-object/from16 v5, p6

    move-object/from16 v6, p4

    move-object v7, v14

    move-object/from16 v8, v17

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/youtube/videos/NotificationUtil;->createNewEpisodesNotification(ILjava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v19

    goto :goto_2
.end method

.method private processVideoEntries(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    if-nez v5, :cond_0

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    iget-object v9, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/ContentNotificationManager;->getNewEpisodesCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v0

    invoke-virtual {v9, p3, v0}, Lcom/google/android/youtube/videos/store/PosterStore;->getShowPoster(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    :cond_1
    move-object v1, v6

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    :cond_2
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_4

    iget-object v9, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->posterStore:Lcom/google/android/youtube/videos/store/PosterStore;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/ContentNotificationManager;->getNewEpisodesCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v0

    invoke-virtual {v9, p3, v0}, Lcom/google/android/youtube/videos/store/PosterStore;->getShowPoster(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    :cond_4
    return-void
.end method


# virtual methods
.method public checkForNewEpisodes(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "seasonId cannot be null"

    invoke-static {p1, v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "showId cannot be null"

    invoke-static {p2, v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/Config;->maxNewContentNotificationDelayMillis()J

    move-result-wide v4

    sub-long v10, v2, v4

    new-instance v0, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "videos JOIN video_userdata ON video_id = pinning_video_id JOIN seasons ON episode_season_id = season_id JOIN purchases ON (item_id = season_id AND item_type = 2)"

    sget-object v3, Lcom/google/android/youtube/videos/ContentNotificationManager$NEW_EPISODES_QUERY;->PROJECTION:[Ljava/lang/String;

    const-string v4, "episode_season_id = ? AND publish_timestamp IS NOT NULL AND publish_timestamp > ? AND is_new_notification_dismissed = 0"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    aput-object p1, v5, v1

    const/4 v6, 0x1

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const-string v7, "pinning_account,episode_number_text"

    const/4 v8, -0x1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    new-instance v9, Lcom/google/android/youtube/videos/ContentNotificationManager$1;

    invoke-direct {v9, p0, p1, p2}, Lcom/google/android/youtube/videos/ContentNotificationManager$1;-><init>(Lcom/google/android/youtube/videos/ContentNotificationManager;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    invoke-virtual {v1, v0, v9}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public dismissContentNotification(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/ContentNotificationManager;->createNewEpisodesTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f070009

    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p3}, Lcom/google/android/youtube/videos/ContentNotificationManager;->getSqlUpdateIsNewNotification([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v0, v5, v3}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/ContentNotificationManager;->database:Lcom/google/android/youtube/videos/store/Database;

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0, v5, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2
.end method
