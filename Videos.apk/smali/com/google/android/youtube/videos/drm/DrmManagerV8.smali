.class Lcom/google/android/youtube/videos/drm/DrmManagerV8;
.super Lcom/google/android/youtube/videos/drm/DrmManager;
.source "DrmManagerV8.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/drm/DrmManagerV8$3;,
        Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;,
        Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;
    }
.end annotation


# static fields
.field private static final INITIALIZE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

.field private static final LICENSE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

.field private static final QUERY_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

.field private static final REGISTER_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;


# instance fields
.field private final context:Landroid/content/Context;

.field private final deviceId:Ljava/lang/String;

.field private final drmListener:Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final executor:Ljava/util/concurrent/Executor;

.field private fatalInitializationError:Z

.field private frameworkDrmError:I

.field private frameworkDrmLevel:I

.field private final globalCredentials:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final gservicesId:J

.field private initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

.field private final pendingResponseQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final requestLock:Ljava/lang/Object;

.field private final requestSigner:Lcom/google/android/youtube/videos/drm/DrmRequestSigner;

.field private final rootedAppLevelDrmEnabled:Z

.field private final wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v4, [Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Initialized:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->INITIALIZE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    new-array v0, v4, [Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseReceived:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->LICENSE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    new-array v0, v3, [Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->Registered:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->REGISTER_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    new-array v0, v3, [Lcom/widevine/drmapi/android/WVEvent;

    sget-object v1, Lcom/widevine/drmapi/android/WVEvent;->QueryStatus:Lcom/widevine/drmapi/android/WVEvent;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->QUERY_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Lcom/google/android/youtube/core/async/DeviceAuthorizer;
    .param p4    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p5    # Lcom/google/android/youtube/videos/Config;
    .param p6    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct {p0, p4}, Lcom/google/android/youtube/videos/drm/DrmManager;-><init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;)V

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->executor:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestLock:Ljava/lang/Object;

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->deviceId:Ljava/lang/String;

    invoke-interface {p5}, Lcom/google/android/youtube/videos/Config;->gservicesId()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->gservicesId:J

    new-instance v1, Lcom/widevine/drmapi/android/WVPlayback;

    invoke-direct {v1}, Lcom/widevine/drmapi/android/WVPlayback;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmRequestSigner;

    invoke-direct {v1, p3}, Lcom/google/android/youtube/videos/drm/DrmRequestSigner;-><init>(Lcom/google/android/youtube/core/async/DeviceAuthorizer;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestSigner:Lcom/google/android/youtube/videos/drm/DrmRequestSigner;

    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->pendingResponseQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {p5}, Lcom/google/android/youtube/videos/Config;->rootedAppLevelDrmEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->rootedAppLevelDrmEnabled:Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVDRMServer"

    invoke-interface {p5}, Lcom/google/android/youtube/videos/Config;->wvDrmServerUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVPortalKey"

    invoke-interface {p5}, Lcom/google/android/youtube/videos/Config;->wvPortalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVDeviceIDKey"

    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->deviceId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVAssetRootKey"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    const-string v2, "WVAssetDBPathKey"

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;-><init>(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;)V

    iput-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->drmListener:Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getLicense(Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/util/HashMap;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .param p1    # Ljava/util/HashMap;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->parseLicense(Ljava/util/HashMap;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Lcom/google/android/youtube/videos/drm/DrmException;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getError(Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Lcom/google/android/youtube/videos/drm/DrmException;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .param p1    # Lcom/widevine/drmapi/android/WVEvent;
    .param p2    # Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->logEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->pendingResponseQueue:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)Lcom/widevine/drmapi/android/WVPlayback;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/widevine/drmapi/android/WVStatus;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .param p1    # Lcom/widevine/drmapi/android/WVStatus;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v0

    return v0
.end method

.method static synthetic access$500()[Lcom/widevine/drmapi/android/WVEvent;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->REGISTER_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .param p1    # Ljava/lang/String;
    .param p2    # [Lcom/widevine/drmapi/android/WVEvent;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponseForAssetUri(Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700()[Lcom/widevine/drmapi/android/WVEvent;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->LICENSE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p2    # [Lcom/widevine/drmapi/android/WVEvent;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponseForIdentifiers(Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getSuccess(Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Z

    move-result v0

    return v0
.end method

.method private getError(Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Lcom/google/android/youtube/videos/drm/DrmException;
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    iget-object v2, p1, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    const-string v3, "WVErrorKey"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->parseErrorCode(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getDrmError(I)Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/videos/drm/DrmException;

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    return-object v2
.end method

.method private getLicense(Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p3    # Z

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initialize()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->isDisabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    iget-object v10, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestLock:Ljava/lang/Object;

    monitor-enter v10

    if-eqz p2, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    iget-wide v1, p2, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->systemId:J

    iget-wide v3, p2, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->assetId:J

    iget-wide v5, p2, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-virtual/range {v0 .. v6}, Lcom/widevine/drmapi/android/WVPlayback;->queryAssetStatus(JJJ)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v9

    :goto_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v9, v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Widevine queryAssetStatus failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    monitor-exit v10

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v0, p1}, Lcom/widevine/drmapi/android/WVPlayback;->queryAssetStatus(Ljava/lang/String;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v9

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_4

    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->QUERY_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    invoke-direct {p0, p2, v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponseForIdentifiers(Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v8

    :goto_2
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, v8, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    invoke-direct {p0, v0, p3}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->parseLicense(Ljava/util/HashMap;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;

    move-result-object v7

    if-nez v7, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No license for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    :try_start_2
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->QUERY_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponseForAssetUri(Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    goto :goto_2
.end method

.method private getRemainingTime(Ljava/util/HashMap;)J
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)J"
        }
    .end annotation

    const-string v0, "WVPurchaseDurationRemainingKey"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-string v0, "WVLicenseDurationRemainingKey"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private getResponse(Ljava/util/Map;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;
    .locals 11
    .param p2    # [Lcom/widevine/drmapi/android/WVEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;[",
            "Lcom/widevine/drmapi/android/WVEvent;",
            ")",
            "Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;"
        }
    .end annotation

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-static {v3, p2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    const/4 v8, 0x0

    const/4 v7, 0x0

    :cond_0
    :goto_0
    if-nez v7, :cond_5

    :try_start_0
    iget-object v9, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->pendingResponseQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v9}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-object v8, v0

    const/4 v1, 0x1

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    iget-object v9, v8, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    invoke-virtual {v9, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, v8, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    invoke-virtual {v9, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    :cond_2
    const/4 v1, 0x0

    :cond_3
    if-eqz v1, :cond_4

    iget-object v9, v8, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->event:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v3, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v7, 0x1

    :goto_1
    if-nez v7, :cond_0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Discarding unexpected event from WV library of type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v8, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->event:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v9

    goto :goto_0

    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    :cond_5
    return-object v8
.end method

.method private getResponseForAssetUri(Ljava/lang/String;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # [Lcom/widevine/drmapi/android/WVEvent;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "WVAssetPathKey"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponse(Ljava/util/Map;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v1

    return-object v1
.end method

.method private getResponseForIdentifiers(Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p2    # [Lcom/widevine/drmapi/android/WVEvent;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "WVKeyIDKey"

    iget-wide v2, p1, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVAssetIDKey"

    iget-wide v2, p1, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->assetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVSystemIDKey"

    iget-wide v2, p1, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->systemId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponse(Ljava/util/Map;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v1

    return-object v1
.end method

.method private getSuccess(Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;)Z
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    const-string v1, "WVErrorKey"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "ok"

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    const-string v2, "WVErrorKey"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initialize()V
    .locals 11

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->fatalInitializationError:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v10, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestLock:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->drmListener:Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;

    invoke-virtual {v0, v1, v2, v4}, Lcom/widevine/drmapi/android/WVPlayback;->initialize(Landroid/content/Context;Ljava/util/HashMap;Lcom/widevine/drmapi/android/WVEventListener;)Lcom/widevine/drmapi/android/WVStatus;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    :try_start_1
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v9, v0, :cond_2

    iput-object v9, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    iget v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmError:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/videos/logging/EventLogger;->onAppDrmInitFailed(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;II)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Widevine initialization failed (sync): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v0}, Lcom/widevine/drmapi/android/WVPlayback;->terminate()Lcom/widevine/drmapi/android/WVStatus;

    monitor-exit v10

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catch_0
    move-exception v7

    :try_start_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    iget v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmError:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/videos/logging/EventLogger;->onAppDrmInitFailed(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;II)V

    const-string v0, "Can\'t load native drm library"

    invoke-static {v0, v7}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->NotInitialized:Lcom/widevine/drmapi/android/WVStatus;

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->fatalInitializationError:Z

    monitor-exit v10

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->INITIALIZE_EVENT_TYPES:[Lcom/widevine/drmapi/android/WVEvent;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getResponse(Ljava/util/Map;[Lcom/widevine/drmapi/android/WVEvent;)Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    move-result-object v8

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v6, v8, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;->attributes:Ljava/util/HashMap;

    const-string v0, "WVStatusKey"

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/widevine/drmapi/android/WVStatus;

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v0, v1, :cond_5

    const-string v0, "WVErrorKey"

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "WVErrorKey"

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    iget v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    iget v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmError:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/videos/logging/EventLogger;->onAppDrmInitFailed(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;II)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Widevine initialization failed (async): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v3, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v0}, Lcom/widevine/drmapi/android/WVPlayback;->terminate()Lcom/widevine/drmapi/android/WVStatus;

    goto/16 :goto_0

    :cond_4
    const-string v0, ""

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget v1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    iget v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmError:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onAppDrmInit(II)V

    goto/16 :goto_0
.end method

.method private logEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V
    .locals 4
    .param p1    # Lcom/widevine/drmapi/android/WVEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/widevine/drmapi/android/WVEvent;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v2, "===================================="

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onEvent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/widevine/drmapi/android/WVEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v2, "===================================="

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    return-void
.end method

.method private parseLicense(Ljava/util/HashMap;Z)Lcom/google/android/youtube/videos/drm/DrmResponse;
    .locals 17
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;"
        }
    .end annotation

    const-string v2, "WVStatusKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v2, v3, :cond_0

    const/4 v13, 0x1

    :goto_0
    if-nez v13, :cond_1

    const/4 v2, 0x0

    :goto_1
    return-object v2

    :cond_0
    const/4 v13, 0x0

    goto :goto_0

    :cond_1
    const-string v2, "WVLicenseTypeKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v14

    and-int/lit8 v2, v14, 0x1

    if-eqz v2, :cond_2

    const/4 v12, 0x1

    :goto_2
    and-int/lit8 v2, v14, 0x2

    if-eqz v2, :cond_3

    const/4 v11, 0x1

    :goto_3
    const-string v2, "WVPlaybackElapsedTimeKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    const-string v2, "WVKeyIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v4, "WVAssetIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v6, "WVSystemIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;-><init>(JJJ)V

    new-instance v2, Lcom/google/android/youtube/videos/drm/DrmResponse;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct/range {p0 .. p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->getRemainingTime(Ljava/util/HashMap;)J

    move-result-wide v5

    long-to-int v7, v5

    long-to-int v8, v15

    move v5, v12

    move v6, v11

    move-object v9, v1

    move/from16 v10, p2

    invoke-direct/range {v2 .. v10}, Lcom/google/android/youtube/videos/drm/DrmResponse;-><init>(JZZIILcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)V

    goto :goto_1

    :cond_2
    const/4 v12, 0x0

    goto :goto_2

    :cond_3
    const/4 v11, 0x0

    goto :goto_3
.end method

.method private toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I
    .locals 1
    .param p1    # Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {p1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v0

    add-int/lit16 v0, v0, 0x384

    return v0
.end method


# virtual methods
.method public getDrmLevel()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->rootedAppLevelDrmEnabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v0}, Lcom/widevine/drmapi/android/WVPlayback;->isRooted()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Z

    const-string v2, "appLevelDrm must be true"

    invoke-static {p2, v2}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v2, v0}, Lcom/widevine/drmapi/android/WVPlayback;->play(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method

.method protected requestOfflineRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;-><init>(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected requestRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 10
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initialize()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->isDisabled()Z

    move-result v3

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v7, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v6, v7, :cond_0

    if-eqz v3, :cond_2

    :cond_0
    if-eqz v3, :cond_1

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->ROOTED_DEVICE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    :goto_0
    new-instance v6, Lcom/google/android/youtube/videos/drm/DrmException;

    iget-object v7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->initializationStatus:Lcom/widevine/drmapi/android/WVStatus;

    invoke-direct {p0, v7}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->toErrorCode(Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v7

    invoke-direct {v6, v1, v7}, Lcom/google/android/youtube/videos/drm/DrmException;-><init>(Lcom/google/android/youtube/videos/drm/DrmException$DrmError;I)V

    invoke-interface {p3, p1, v6}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_1
    return-void

    :cond_1
    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/HashMap;

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->globalCredentials:Ljava/util/HashMap;

    invoke-direct {v0, v6}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    const-string v7, "WVLicenseTypeKey"

    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iget-boolean v6, v6, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v6, :cond_9

    const/4 v6, 0x3

    :goto_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    if-eqz v6, :cond_3

    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    const-string v7, "server_uri"

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v7, "WVDRMServer"

    new-instance v8, Ljava/lang/String;

    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    const-string v9, "server_uri"

    invoke-interface {v6, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    invoke-direct {v8, v6}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    if-eqz v6, :cond_4

    const-string v6, "WVStreamIDKey"

    iget-object v7, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "v=2"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&videoid="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "&aid="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->gservicesId:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, "&root="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->wvPlayback:Lcom/widevine/drmapi/android/WVPlayback;

    invoke-virtual {v7}, Lcom/widevine/drmapi/android/WVPlayback;->isRooted()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v6, v6, Lcom/google/android/youtube/videos/accounts/UserAuth;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v6, v6, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->authType:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    sget-object v7, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->CLIENTLOGIN:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    if-ne v6, v7, :cond_a

    const-string v6, "&clientauth="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v7, v7, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    :goto_3
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/drm/DrmResponse;->isActivated()Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "&time_since_started="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p2, Lcom/google/android/youtube/videos/drm/DrmResponse;->secondsSinceActivation:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_6
    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    if-eqz v6, :cond_7

    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    const-string v7, "robot_token"

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v6, "&robottoken="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->robotToken:Ljava/util/Map;

    const-string v8, "robot_token"

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    const/16 v8, 0xb

    invoke-static {v6, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    sget-object v7, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    if-ne v6, v7, :cond_8

    const-string v6, "&unpin=true"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :try_start_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "&sign="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->requestSigner:Lcom/google/android/youtube/videos/drm/DrmRequestSigner;

    iget-object v8, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v7, v8, v5}, Lcom/google/android/youtube/videos/drm/DrmRequestSigner;->sign(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    const-string v6, "WVCAUserDataKey"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->executor:Ljava/util/concurrent/Executor;

    new-instance v7, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;

    invoke-direct {v7, p0, v0, p3, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8$2;-><init>(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Ljava/util/HashMap;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/drm/DrmRequest;)V

    invoke-interface {v6, v7}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    :cond_9
    const/4 v6, 0x1

    goto/16 :goto_2

    :cond_a
    iget-object v6, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v6, v6, Lcom/google/android/youtube/videos/accounts/UserAuth;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v6, v6, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->authType:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    sget-object v7, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->OAUTH2:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    if-ne v6, v7, :cond_5

    const-string v6, "&oauth="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v7, v7, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :catch_0
    move-exception v2

    invoke-interface {p3, p1, v2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto/16 :goto_1
.end method

.method public setFrameworkDrmFallbackInfo(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmLevel:I

    iput p2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->frameworkDrmError:I

    return-void
.end method
