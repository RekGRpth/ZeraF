.class final Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;
.super Ljava/lang/Object;
.source "DrmManagerV8.java"

# interfaces
.implements Lcom/widevine/drmapi/android/WVEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/drm/DrmManagerV8;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DrmListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmManagerV8;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmManagerV8$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;-><init>(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;
    .locals 6
    .param p1    # Lcom/widevine/drmapi/android/WVEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/widevine/drmapi/android/WVEvent;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/widevine/drmapi/android/WVStatus;"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # invokes: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->logEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V
    invoke-static {v2, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$1200(Lcom/google/android/youtube/videos/drm/DrmManagerV8;Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V

    const-string v2, "WVStatusKey"

    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/widevine/drmapi/android/WVStatus;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->HeartbeatError:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v1, v2, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    const-string v2, "WVAssetPathKey"

    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    const-string v3, "WVErrorKey"

    invoke-virtual {p2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->parseErrorCode(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->notifyHeartbeatError(Ljava/lang/String;I)V

    :goto_0
    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    return-object v2

    :cond_0
    sget-object v2, Lcom/widevine/drmapi/android/WVEvent;->Stopped:Lcom/widevine/drmapi/android/WVEvent;

    if-ne p1, v2, :cond_1

    sget-object v2, Lcom/google/android/youtube/videos/drm/DrmManagerV8$3;->$SwitchMap$com$widevine$drmapi$android$WVStatus:[I

    invoke-virtual {v1}, Lcom/widevine/drmapi/android/WVStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->Unknown:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    :goto_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    const-string v2, "WVAssetPathKey"

    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2, v0}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->notifyPlaybackStopped(Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;)V

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->TamperDetected:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    goto :goto_1

    :pswitch_1
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->LostConnection:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/drm/DrmManagerV8$DrmListener;->this$0:Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManagerV8;->pendingResponseQueue:Ljava/util/concurrent/BlockingQueue;
    invoke-static {v2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;->access$1300(Lcom/google/android/youtube/videos/drm/DrmManagerV8;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;

    invoke-direct {v3, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManagerV8$WVResponse;-><init>(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
