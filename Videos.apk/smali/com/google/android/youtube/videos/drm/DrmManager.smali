.class public abstract Lcom/google/android/youtube/videos/drm/DrmManager;
.super Ljava/lang/Object;
.source "DrmManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/drm/DrmManager$DrmRequestRetryStrategy;,
        Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;,
        Lcom/google/android/youtube/videos/drm/DrmManager$Listener;,
        Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;
    }
.end annotation


# static fields
.field private static final DRM_ERROR_REGEX:Ljava/util/regex/Pattern;


# instance fields
.field private final eventDispatchHandler:Landroid/os/Handler;

.field private listener:Lcom/google/android/youtube/videos/drm/DrmManager$Listener;

.field private final retryingRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "(code = |lmResult=|wvstatus=)(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/drm/DrmManager;->DRM_ERROR_REGEX:Ljava/util/regex/Pattern;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/drm/DrmManager;->createInternalDrmRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmManager$DrmRequestRetryStrategy;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/drm/DrmManager$DrmRequestRetryStrategy;-><init>(Lcom/google/android/youtube/videos/drm/DrmManager$1;)V

    invoke-static {v0, p1, v1}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;)Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManager;->retryingRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManager;->eventDispatchHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/drm/DrmManager;)Lcom/google/android/youtube/videos/drm/DrmManager$Listener;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManager;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManager;->listener:Lcom/google/android/youtube/videos/drm/DrmManager$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManager;->requestLicense(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method private final createInternalDrmRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmManager$3;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/drm/DrmManager$3;-><init>(Lcom/google/android/youtube/videos/drm/DrmManager;)V

    return-object v0
.end method

.method private static isWidevineEnabled(Landroid/content/Context;)Z
    .locals 8
    .param p0    # Landroid/content/Context;

    new-instance v1, Landroid/drm/DrmManagerClient;

    invoke-direct {v1, p0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/drm/DrmManagerClient;->getAvailableDrmEngines()[Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    if-eqz v2, :cond_0

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "widevine"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    :goto_1
    return v6

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static newDrmManager(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/logging/EventLogger;Z)Lcom/google/android/youtube/videos/drm/DrmManager;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Lcom/google/android/youtube/core/async/DeviceAuthorizer;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p4    # Lcom/google/android/youtube/videos/Config;
    .param p5    # Lcom/google/android/youtube/core/utils/NetworkStatus;
    .param p6    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .param p7    # Z

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    if-nez p7, :cond_0

    invoke-static {p0}, Lcom/google/android/youtube/videos/drm/DrmManager;->isWidevineEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmManagerV8;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/drm/DrmManagerV8;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmManagerV12;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/videos/drm/DrmManagerV12;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    goto :goto_0
.end method

.method private requestLicense(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmRequest;->type:Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;

    iget-boolean v0, v0, Lcom/google/android/youtube/videos/drm/DrmRequest$RequestType;->isOffline:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManager;->requestOfflineRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/youtube/videos/drm/DrmManager;->requestRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method


# virtual methods
.method protected getDrmError(I)Lcom/google/android/youtube/videos/drm/DrmException$DrmError;
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNKNOWN:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    :goto_0
    return-object v0

    :sswitch_0
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->LICENSE_PINNED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->AUTHENTICATION_FAILED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_2
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->USER_GEO_RESTRICTED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_3
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->NO_LICENSE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_4
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->TOO_MANY_ACTIVE_DEVICES_FOR_ACCOUNT:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_5
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->TOO_MANY_ACCOUNTS_ON_DEVICE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_6
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->TOO_MANY_DEVICE_DEACTIVATIONS_ON_ACCOUNT:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_7
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->TOO_MANY_ACTIVATIONS_ON_DEVICE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_8
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->STREAMING_DEVICES_QUOTA_EXCEEDED:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_9
    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->UNPIN_SUCCESSFUL:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x258 -> :sswitch_3
        0x259 -> :sswitch_9
        0x321 -> :sswitch_2
        0x331 -> :sswitch_0
        0x334 -> :sswitch_1
        0x336 -> :sswitch_1
        0x339 -> :sswitch_0
        0x33a -> :sswitch_4
        0x33b -> :sswitch_5
        0x33c -> :sswitch_6
        0x33d -> :sswitch_7
        0x33e -> :sswitch_9
        0x33f -> :sswitch_8
    .end sparse-switch
.end method

.method public abstract getDrmLevel()I
.end method

.method public abstract getPlayableUri(Landroid/net/Uri;Z)Landroid/net/Uri;
.end method

.method public final isDisabled()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/drm/DrmManager;->getDrmLevel()I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected notifyHeartbeatError(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManager;->eventDispatchHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmManager$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManager$1;-><init>(Lcom/google/android/youtube/videos/drm/DrmManager;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected notifyPlaybackStopped(Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManager;->eventDispatchHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/videos/drm/DrmManager$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/drm/DrmManager$2;-><init>(Lcom/google/android/youtube/videos/drm/DrmManager;Ljava/lang/String;Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected parseErrorCode(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    const-string v1, "ok"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "LM Response code = 18"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x258

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmManager;->DRM_ERROR_REGEX:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_3
    const/4 v1, -0x2

    goto :goto_0
.end method

.method public request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManager;->retryingRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method protected abstract requestOfflineRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation
.end method

.method protected abstract requestRights(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;Lcom/google/android/youtube/core/async/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/drm/DrmRequest;",
            "Lcom/google/android/youtube/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation
.end method

.method public setListener(Lcom/google/android/youtube/videos/drm/DrmManager$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmManager$Listener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmManager;->listener:Lcom/google/android/youtube/videos/drm/DrmManager$Listener;

    return-void
.end method

.method protected final stripQueryParameters(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p1    # Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_0
    return-object p1
.end method
