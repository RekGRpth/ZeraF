.class public final enum Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;
.super Ljava/lang/Enum;
.source "DrmManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/drm/DrmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StopReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

.field public static final enum LostConnection:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

.field public static final enum TamperDetected:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

.field public static final enum Unknown:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    const-string v1, "TamperDetected"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->TamperDetected:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    const-string v1, "LostConnection"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->LostConnection:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    new-instance v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->Unknown:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->TamperDetected:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->LostConnection:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->Unknown:Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->$VALUES:[Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;
    .locals 1

    const-class v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->$VALUES:[Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    invoke-virtual {v0}, [Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/videos/drm/DrmManager$StopReason;

    return-object v0
.end method
