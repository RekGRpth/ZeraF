.class Lcom/google/android/youtube/videos/drm/DrmManager$1;
.super Ljava/lang/Object;
.source "DrmManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/drm/DrmManager;->notifyHeartbeatError(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/drm/DrmManager;

.field final synthetic val$assetPathKey:Ljava/lang/String;

.field final synthetic val$errorCode:I


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/drm/DrmManager;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/drm/DrmManager$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManager;

    iput-object p2, p0, Lcom/google/android/youtube/videos/drm/DrmManager$1;->val$assetPathKey:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/youtube/videos/drm/DrmManager$1;->val$errorCode:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManager$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManager;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManager;->listener:Lcom/google/android/youtube/videos/drm/DrmManager$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/videos/drm/DrmManager;->access$100(Lcom/google/android/youtube/videos/drm/DrmManager;)Lcom/google/android/youtube/videos/drm/DrmManager$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/drm/DrmManager$1;->this$0:Lcom/google/android/youtube/videos/drm/DrmManager;

    # getter for: Lcom/google/android/youtube/videos/drm/DrmManager;->listener:Lcom/google/android/youtube/videos/drm/DrmManager$Listener;
    invoke-static {v0}, Lcom/google/android/youtube/videos/drm/DrmManager;->access$100(Lcom/google/android/youtube/videos/drm/DrmManager;)Lcom/google/android/youtube/videos/drm/DrmManager$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/drm/DrmManager$1;->val$assetPathKey:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/youtube/videos/drm/DrmManager$1;->val$errorCode:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/videos/drm/DrmManager$Listener;->onHeartbeatError(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
