.class public Lcom/google/android/youtube/videos/drm/DrmResponse;
.super Ljava/lang/Object;
.source "DrmResponse.java"


# instance fields
.field public final allowsOffline:Z

.field public final allowsStreaming:Z

.field public final ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

.field public final refreshed:Z

.field public final remainingSeconds:I

.field public final secondsSinceActivation:I

.field public final timestamp:J


# direct methods
.method public constructor <init>(JZZIILcom/google/android/youtube/videos/drm/DrmManager$Identifiers;Z)V
    .locals 0
    .param p1    # J
    .param p3    # Z
    .param p4    # Z
    .param p5    # I
    .param p6    # I
    .param p7    # Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;
    .param p8    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->timestamp:J

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->allowsStreaming:Z

    iput-boolean p4, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->allowsOffline:Z

    iput p5, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->remainingSeconds:I

    iput p6, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->secondsSinceActivation:I

    iput-object p7, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iput-boolean p8, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->refreshed:Z

    return-void
.end method


# virtual methods
.method public isActivated()Z
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->secondsSinceActivation:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[st="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->allowsStreaming:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->allowsOffline:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->timestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "remaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->remainingSeconds:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "activated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/videos/drm/DrmResponse;->secondsSinceActivation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
