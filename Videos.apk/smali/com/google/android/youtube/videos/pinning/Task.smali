.class abstract Lcom/google/android/youtube/videos/pinning/Task;
.super Ljava/lang/Object;
.source "Task.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/pinning/Task$Listener;,
        Lcom/google/android/youtube/videos/pinning/Task$TaskException;,
        Lcom/google/android/youtube/videos/pinning/Task$Key;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/youtube/videos/pinning/Task$Key;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private volatile canceled:Z

.field protected final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field public final key:Lcom/google/android/youtube/videos/pinning/Task$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final listener:Lcom/google/android/youtube/videos/pinning/Task$Listener;

.field public final taskType:I

.field private final terminated:Landroid/os/ConditionVariable;

.field private final wakeLock:Landroid/os/PowerManager$WakeLock;

.field private final wifiLock:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method public constructor <init>(ILcom/google/android/youtube/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 1
    .param p1    # I
    .param p3    # Landroid/os/PowerManager$WakeLock;
    .param p4    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5    # Lcom/google/android/youtube/videos/pinning/Task$Listener;
    .param p6    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;",
            "Landroid/os/PowerManager$WakeLock;",
            "Landroid/net/wifi/WifiManager$WifiLock;",
            "Lcom/google/android/youtube/videos/pinning/Task$Listener;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    const-string v0, "key cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/pinning/Task$Key;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    const-string v0, "wakeLock cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager$WakeLock;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    const-string v0, "wifiLock cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager$WifiLock;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const-string v0, "listener cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/pinning/Task$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->listener:Lcom/google/android/youtube/videos/pinning/Task$Listener;

    const-string v0, "eventLogger cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->canceled:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    return-void
.end method

.method protected abstract execute()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation
.end method

.method protected final isCanceled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->canceled:Z

    return v0
.end method

.method protected final notifyProgress()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/Task;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/Task;->listener:Lcom/google/android/youtube/videos/pinning/Task$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/videos/pinning/Task$Listener;->onProgress(Lcom/google/android/youtube/videos/pinning/Task;)V

    :cond_0
    return-void
.end method

.method protected abstract onCompleted()V
.end method

.method protected abstract onError(Ljava/lang/Throwable;)V
.end method

.method public final run()V
    .locals 4

    const/16 v1, 0xa

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/Task;->execute()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/Task;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/Task;->onCompleted()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->listener:Lcom/google/android/youtube/videos/pinning/Task$Listener;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/videos/pinning/Task$Listener;->onCompleted(Lcom/google/android/youtube/videos/pinning/Task;)V
    :try_end_0
    .catch Lcom/google/android/youtube/videos/pinning/Task$TaskException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/Task;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/pinning/Task;->onError(Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->listener:Lcom/google/android/youtube/videos/pinning/Task$Listener;

    invoke-interface {v1, p0, v0}, Lcom/google/android/youtube/videos/pinning/Task$Listener;->onError(Lcom/google/android/youtube/videos/pinning/Task;Lcom/google/android/youtube/videos/pinning/Task$TaskException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/Task;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/pinning/Task;->onError(Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->listener:Lcom/google/android/youtube/videos/pinning/Task$Listener;

    new-instance v2, Lcom/google/android/youtube/videos/pinning/Task$TaskException;

    const-string v3, "unexpected exception executing task"

    invoke-direct {v2, v3, v0}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, p0, v2}, Lcom/google/android/youtube/videos/pinning/Task$Listener;->onError(Lcom/google/android/youtube/videos/pinning/Task;Lcom/google/android/youtube/videos/pinning/Task$TaskException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/Task;->terminated:Landroid/os/ConditionVariable;

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->open()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/Task;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/Task;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    throw v1
.end method
