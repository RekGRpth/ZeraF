.class Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;
.super Lcom/google/android/youtube/videos/pinning/Task;
.source "UpdateLastPlaybackTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask$LastPositionQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/pinning/Task",
        "<",
        "Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final videoUpdateRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;Landroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;
    .param p2    # Landroid/os/PowerManager$WakeLock;
    .param p3    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p4    # Lcom/google/android/youtube/videos/pinning/Task$Listener;
    .param p5    # Lcom/google/android/youtube/videos/store/Database;
    .param p6    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p8    # Landroid/net/ConnectivityManager;
    .param p9    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;",
            "Landroid/os/PowerManager$WakeLock;",
            "Landroid/net/wifi/WifiManager$WifiLock;",
            "Lcom/google/android/youtube/videos/pinning/Task$Listener;",
            "Lcom/google/android/youtube/videos/store/Database;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;",
            "Landroid/net/ConnectivityManager;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")V"
        }
    .end annotation

    const/4 v1, 0x3

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p9

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/pinning/Task;-><init>(ILcom/google/android/youtube/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    const-string v0, "database cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v0, "videoUpdateRequester cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->videoUpdateRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method


# virtual methods
.method clearDirtyFlag()V
    .locals 9

    const/4 v8, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "last_playback_is_dirty"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v4, "video_userdata"

    const-string v5, "pinning_account = ? AND pinning_video_id = ?"

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    iget-object v3, v3, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;->account:Ljava/lang/String;

    aput-object v3, v6, v7

    const/4 v7, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    iget-object v3, v3, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;->videoId:Ljava/lang/String;

    aput-object v3, v6, v7

    invoke-virtual {v1, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->database:Lcom/google/android/youtube/videos/store/Database;

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0, v8, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->database:Lcom/google/android/youtube/videos/store/Database;

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v0, v8, v5}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method

.method public execute()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/Task$TaskException;
        }
    .end annotation

    const/4 v4, 0x2

    const/4 v13, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    iget-object v1, v1, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;->account:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->clearDirtyFlag()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "video_userdata"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask$LastPositionQuery;->COLUMNS:[Ljava/lang/String;

    const-string v3, "last_playback_is_dirty AND pinning_account = ? AND pinning_video_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;->account:Ljava/lang/String;

    aput-object v6, v4, v7

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;->videoId:Ljava/lang/String;

    aput-object v6, v4, v13

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->newBuilder()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStartTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setStopTimestampMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->setPositionMsec(J)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback$Builder;->build()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v11

    new-instance v8, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v8}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->videoUpdateRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v3, Lcom/google/android/youtube/videos/api/VideoUpdateRequest;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    iget-object v4, v1, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    iget-object v1, v1, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;->videoId:Ljava/lang/String;

    invoke-direct {v3, v4, v1, v11}, Lcom/google/android/youtube/videos/api/VideoUpdateRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;)V

    invoke-interface {v2, v3, v8}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v8}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->clearDirtyFlag()V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catch_0
    move-exception v10

    :try_start_2
    new-instance v1, Lcom/google/android/youtube/videos/pinning/Task$TaskException;

    const-string v2, "request failed"

    invoke-direct {v1, v2, v10}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method protected onCompleted()V
    .locals 0

    return-void
.end method

.method protected onError(Ljava/lang/Throwable;)V
    .locals 0
    .param p1    # Ljava/lang/Throwable;

    return-void
.end method
