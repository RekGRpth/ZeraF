.class interface abstract Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoingQuery;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "DownloadsOngoingQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SUM(pinning_status = 2)"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MAX(CASE WHEN pinning_status = 2 THEN pinning_account END)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MAX(CASE WHEN pinning_status = 2 THEN pinning_video_id END)"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "MAX(CASE WHEN pinning_status = 2 THEN episode_season_id END)"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "SUM(download_bytes_downloaded)"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SUM(pinning_download_size)"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "MAX(pinning_download_size IS NULL OR pinning_download_size = 0)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoingQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
