.class Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->onDownloadsStateChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeDownloadsOngoing()Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    invoke-static {v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$000(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeAllCompleted()Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$300(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeAllErrors()Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$400(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->mainHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$200(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;

    invoke-direct {v4, p0, v2, v0, v1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2$1;-><init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$2;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
