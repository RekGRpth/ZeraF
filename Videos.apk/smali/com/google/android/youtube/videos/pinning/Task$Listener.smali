.class public interface abstract Lcom/google/android/youtube/videos/pinning/Task$Listener;
.super Ljava/lang/Object;
.source "Task.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/Task;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onCompleted(Lcom/google/android/youtube/videos/pinning/Task;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation
.end method

.method public abstract onError(Lcom/google/android/youtube/videos/pinning/Task;Lcom/google/android/youtube/videos/pinning/Task$TaskException;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;",
            "Lcom/google/android/youtube/videos/pinning/Task$TaskException;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onProgress(Lcom/google/android/youtube/videos/pinning/Task;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation
.end method
