.class public Lcom/google/android/youtube/videos/pinning/TransferService;
.super Landroid/app/Service;
.source "TransferService.java"

# interfaces
.implements Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;
.implements Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/pinning/TransferService$Receiver;,
        Lcom/google/android/youtube/videos/pinning/TransferService$AlarmReceiver;
    }
.end annotation


# instance fields
.field private downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

.field private handler:Landroid/os/Handler;

.field private mostRecentStartId:I

.field private pendingTicket:I

.field private pingIntervalMillis:J

.field private transfersExecutor:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/pinning/TransferService;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/TransferService;

    iget v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService;->pendingTicket:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/pinning/TransferService;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/TransferService;

    iget v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService;->mostRecentStartId:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/pinning/TransferService;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/pinning/TransferService;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransferService;->scheduleRestart()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/pinning/TransferService;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/pinning/TransferService;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransferService;->cancelRestart()V

    return-void
.end method

.method private cancelRestart()V
    .locals 2

    const/4 v1, 0x0

    # invokes: Lcom/google/android/youtube/videos/pinning/TransferService$Receiver;->setEnabled(Landroid/content/Context;Z)V
    invoke-static {p0, v1}, Lcom/google/android/youtube/videos/pinning/TransferService$Receiver;->access$400(Landroid/content/Context;Z)V

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/pinning/TransferService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {p0}, Lcom/google/android/youtube/videos/pinning/TransferService$AlarmReceiver;->createIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/pinning/TransferService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private scheduleRestart()V
    .locals 6

    const/4 v1, 0x1

    # invokes: Lcom/google/android/youtube/videos/pinning/TransferService$Receiver;->setEnabled(Landroid/content/Context;Z)V
    invoke-static {p0, v1}, Lcom/google/android/youtube/videos/pinning/TransferService$Receiver;->access$400(Landroid/content/Context;Z)V

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/pinning/TransferService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/youtube/videos/pinning/TransferService;->pingIntervalMillis:J

    add-long/2addr v2, v4

    invoke-static {p0}, Lcom/google/android/youtube/videos/pinning/TransferService$AlarmReceiver;->createIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 35

    invoke-super/range {p0 .. p0}, Landroid/app/Service;->onCreate()V

    const-string v1, "creating transfer service"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/pinning/TransferService;->getApplication()Landroid/app/Application;

    move-result-object v33

    check-cast v33, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->transferServicePingIntervalMillis()J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/google/android/youtube/videos/pinning/TransferService;->pingIntervalMillis:J

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransferService;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getDrmManager()Lcom/google/android/youtube/videos/drm/DrmManager;

    move-result-object v5

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStore()Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-result-object v6

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStoreSync()Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-result-object v7

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Requesters;->getPermittedStreamsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v8

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Requesters;->getSyncStreamExtraRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v9

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/api/ApiRequesters;->getVideoGetRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v10

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/api/ApiRequesters;->getVideoUpdateRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v11

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/api/ApiRequesters;->getRemoveFromWishlistSyncRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v12

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getApiRequesters()Lcom/google/android/youtube/videos/api/ApiRequesters;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/videos/api/ApiRequesters;->getAddToWishlistSyncRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v13

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getStreamsSelector()Lcom/google/android/youtube/videos/StreamsSelector;

    move-result-object v14

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getStoringSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;

    move-result-object v15

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getKnowledgeClient()Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    move-result-object v16

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v17

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v18

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getStreamingStatusNotifier()Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    move-result-object v19

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->legacyDownloadsHaveAppLevelDrm()Z

    move-result v20

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->useSslForDownloads()Z

    move-result v21

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->refreshLicensesOlderThanMillis()J

    move-result-wide v22

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->maxConcurrentLicenseTasks()I

    move-result v24

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->maxConcurrentOrBackedOffPinningTasks()I

    move-result v25

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->maxConcurrentUpdateUserdataTasks()I

    move-result v26

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->maxPinningTaskRetries()I

    move-result v27

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->maxUpdateUserdataTaskRetries()I

    move-result v28

    new-instance v29, Lcom/google/android/youtube/videos/pinning/RetryInterval;

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->minLicenseTaskRetryDelayMillis()I

    move-result v2

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->maxLicenseTaskRetryDelayMillis()I

    move-result v3

    move-object/from16 v0, v29

    invoke-direct {v0, v2, v3}, Lcom/google/android/youtube/videos/pinning/RetryInterval;-><init>(II)V

    new-instance v30, Lcom/google/android/youtube/videos/pinning/RetryInterval;

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->minPinningTaskRetryDelayMillis()I

    move-result v2

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->maxPinningTaskRetryDelayMillis()I

    move-result v3

    move-object/from16 v0, v30

    invoke-direct {v0, v2, v3}, Lcom/google/android/youtube/videos/pinning/RetryInterval;-><init>(II)V

    new-instance v31, Lcom/google/android/youtube/videos/pinning/RetryInterval;

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->minUpdateUserdataTaskRetryDelayMillis()I

    move-result v2

    invoke-interface/range {v34 .. v34}, Lcom/google/android/youtube/videos/Config;->maxUpdateUserdataTaskRetryDelayMillis()I

    move-result v3

    move-object/from16 v0, v31

    invoke-direct {v0, v2, v3}, Lcom/google/android/youtube/videos/pinning/RetryInterval;-><init>(II)V

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v32

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    invoke-direct/range {v1 .. v32}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/StreamsSelector;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/videos/tagging/KnowledgeClient;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;ZZJIIIIILcom/google/android/youtube/videos/pinning/RetryInterval;Lcom/google/android/youtube/videos/pinning/RetryInterval;Lcom/google/android/youtube/videos/pinning/RetryInterval;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransferService;->transfersExecutor:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/youtube/videos/VideosApplication;->getDownloadNotificationManager()Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransferService;->downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransferService;->downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->setHandler(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService;->transfersExecutor:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->quit()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService;->downloadNotificationManager:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->setHandler(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$OngoingNotificationHandler;)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onIdle(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/videos/pinning/TransferService$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/TransferService$1;-><init>(Lcom/google/android/youtube/videos/pinning/TransferService;IZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onOngoingNotification(ILandroid/app/Notification;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/app/Notification;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/pinning/TransferService;->startForeground(ILandroid/app/Notification;)V

    return-void
.end method

.method public onOngoingNotificationCanceled()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/pinning/TransferService;->stopForeground(Z)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransferService;->scheduleRestart()V

    iput p3, p0, Lcom/google/android/youtube/videos/pinning/TransferService;->mostRecentStartId:I

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService;->transfersExecutor:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->requestPing()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService;->pendingTicket:I

    const/4 v0, 0x1

    return v0
.end method
