.class Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->onDownloadsStateChanged(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iput-object p2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$videoId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeDownloadsOngoing()Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    invoke-static {v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$000(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$videoId:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeCompletedFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;
    invoke-static {v3, v4, v5}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$700(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->val$videoId:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->computeErrorFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;
    invoke-static {v3, v4, v5}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$800(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->mainHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;->access$200(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;

    invoke-direct {v4, p0, v2, v0, v1}, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3$1;-><init>(Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$3;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadCompleted;Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
