.class public Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadError"
.end annotation


# instance fields
.field public final account:Ljava/lang/String;

.field public final seasonId:Ljava/lang/String;

.field public final title:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->account:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->videoId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->seasonId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadError;->title:Ljava/lang/String;

    return-void
.end method
