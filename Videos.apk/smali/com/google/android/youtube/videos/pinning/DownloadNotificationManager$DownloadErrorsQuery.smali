.class interface abstract Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadErrorsQuery;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "DownloadErrorsQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "pinning_account"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "pinning_video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "episode_season_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/pinning/DownloadNotificationManager$DownloadErrorsQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
