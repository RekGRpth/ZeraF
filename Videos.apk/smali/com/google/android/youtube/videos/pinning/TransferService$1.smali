.class Lcom/google/android/youtube/videos/pinning/TransferService$1;
.super Ljava/lang/Object;
.source "TransferService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/pinning/TransferService;->onIdle(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/pinning/TransferService;

.field final synthetic val$haveLicenses:Z

.field final synthetic val$ticket:I


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/pinning/TransferService;IZ)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->this$0:Lcom/google/android/youtube/videos/pinning/TransferService;

    iput p2, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->val$ticket:I

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->val$haveLicenses:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->val$ticket:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->this$0:Lcom/google/android/youtube/videos/pinning/TransferService;

    # getter for: Lcom/google/android/youtube/videos/pinning/TransferService;->pendingTicket:I
    invoke-static {v1}, Lcom/google/android/youtube/videos/pinning/TransferService;->access$000(Lcom/google/android/youtube/videos/pinning/TransferService;)I

    move-result v1

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->this$0:Lcom/google/android/youtube/videos/pinning/TransferService;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->this$0:Lcom/google/android/youtube/videos/pinning/TransferService;

    # getter for: Lcom/google/android/youtube/videos/pinning/TransferService;->mostRecentStartId:I
    invoke-static {v1}, Lcom/google/android/youtube/videos/pinning/TransferService;->access$100(Lcom/google/android/youtube/videos/pinning/TransferService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/pinning/TransferService;->stopSelfResult(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->val$haveLicenses:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->this$0:Lcom/google/android/youtube/videos/pinning/TransferService;

    # invokes: Lcom/google/android/youtube/videos/pinning/TransferService;->scheduleRestart()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/pinning/TransferService;->access$200(Lcom/google/android/youtube/videos/pinning/TransferService;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransferService$1;->this$0:Lcom/google/android/youtube/videos/pinning/TransferService;

    # invokes: Lcom/google/android/youtube/videos/pinning/TransferService;->cancelRestart()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/pinning/TransferService;->access$300(Lcom/google/android/youtube/videos/pinning/TransferService;)V

    goto :goto_0
.end method
