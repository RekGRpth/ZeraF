.class public final Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;
.super Ljava/lang/Object;
.source "PinningDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/PinningDbHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DownloadDetails"
.end annotation


# instance fields
.field public final haveLicense:Z

.field public final haveSubtitles:Z

.field public final lastModified:Ljava/lang/String;

.field public final licenseVideoFormat:I

.field public final relativeFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZIZLjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # I
    .param p4    # Z
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->relativeFilePath:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->haveLicense:Z

    iput p3, p0, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->licenseVideoFormat:I

    iput-boolean p4, p0, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->haveSubtitles:Z

    iput-object p5, p0, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->lastModified:Ljava/lang/String;

    return-void
.end method
