.class final Lcom/google/android/youtube/videos/pinning/TransfersExecutor;
.super Ljava/lang/Object;
.source "TransfersExecutor.java"

# interfaces
.implements Lcom/google/android/youtube/videos/pinning/Task$Listener;
.implements Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;
.implements Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/pinning/TransfersExecutor$WishlistQuery;,
        Lcom/google/android/youtube/videos/pinning/TransfersExecutor$TransfersQuery;,
        Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;,
        Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;,
        Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private activeTransfers:Z

.field private final addToWishlistRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AddToWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final backedOff:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/youtube/videos/pinning/Task$Key;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final backgroundHandler:Landroid/os/Handler;

.field private final backgroundThread:Landroid/os/HandlerThread;

.field private final connectivityManager:Landroid/net/ConnectivityManager;

.field private final context:Landroid/content/Context;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

.field private final eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private final executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private final failureCounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/youtube/videos/pinning/Task$Key;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private idle:Z

.field private final knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

.field private final legacyDownloadsHaveAppLevelDrm:Z

.field private final licenseTaskRetryInterval:Lcom/google/android/youtube/videos/pinning/RetryInterval;

.field private final listener:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;

.field private final maxConcurrentLicenseTasks:I

.field private final maxConcurrentOrBackedOffPinningTasks:I

.field private final maxConcurrentUserdataUpdateTasks:I

.field private final maxPinningTaskRetries:I

.field private final maxUpdateUserdataTaskRetries:I

.field private final mediaMountedReceiver:Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;

.field private final networkStateReceiver:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;

.field private final permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pinningTaskRetryInterval:Lcom/google/android/youtube/videos/pinning/RetryInterval;

.field private postedTickets:I

.field private final preferences:Landroid/content/SharedPreferences;

.field private final preferencesListener:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;

.field private processedTickets:I

.field private final purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private final purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private final refreshLicensesOlderThanMillis:J

.field private final removeFromWishlistRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

.field private final streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

.field private final subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

.field private final syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;"
        }
    .end annotation
.end field

.field private final tasks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/youtube/videos/pinning/Task$Key;",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final tasksWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private final ticketLock:Ljava/lang/Object;

.field private final updateUserdataTaskRetryInterval:Lcom/google/android/youtube/videos/pinning/RetryInterval;

.field private final useSslForDownloads:Z

.field private final videoGetRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field

.field private final videoUpdateRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/StreamsSelector;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/videos/tagging/KnowledgeClient;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;ZZJIIIIILcom/google/android/youtube/videos/pinning/RetryInterval;Lcom/google/android/youtube/videos/pinning/RetryInterval;Lcom/google/android/youtube/videos/pinning/RetryInterval;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;
    .param p3    # Landroid/content/SharedPreferences;
    .param p4    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p5    # Lcom/google/android/youtube/videos/store/PurchaseStore;
    .param p6    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    .param p13    # Lcom/google/android/youtube/videos/StreamsSelector;
    .param p14    # Lcom/google/android/youtube/core/client/SubtitlesClient;
    .param p15    # Lcom/google/android/youtube/videos/tagging/KnowledgeClient;
    .param p16    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p17    # Lcom/google/android/youtube/videos/store/Database;
    .param p18    # Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;
    .param p19    # Z
    .param p20    # Z
    .param p21    # J
    .param p23    # I
    .param p24    # I
    .param p25    # I
    .param p26    # I
    .param p27    # I
    .param p28    # Lcom/google/android/youtube/videos/pinning/RetryInterval;
    .param p29    # Lcom/google/android/youtube/videos/pinning/RetryInterval;
    .param p30    # Lcom/google/android/youtube/videos/pinning/RetryInterval;
    .param p31    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/youtube/videos/drm/DrmManager;",
            "Lcom/google/android/youtube/videos/store/PurchaseStore;",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AddToWishlistRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/youtube/videos/StreamsSelector;",
            "Lcom/google/android/youtube/core/client/SubtitlesClient;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeClient;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/youtube/videos/store/Database;",
            "Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;",
            "ZZJIIIII",
            "Lcom/google/android/youtube/videos/pinning/RetryInterval;",
            "Lcom/google/android/youtube/videos/pinning/RetryInterval;",
            "Lcom/google/android/youtube/videos/pinning/RetryInterval;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->listener:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;

    iput-object p3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    iput-object p4, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    iput-object p5, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iput-object p6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iput-object p7, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->videoGetRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->videoUpdateRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->removeFromWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->addToWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->legacyDownloadsHaveAppLevelDrm:Z

    move/from16 v0, p20

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->useSslForDownloads:Z

    move-wide/from16 v0, p21

    iput-wide v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->refreshLicensesOlderThanMillis:J

    move/from16 v0, p23

    iput v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxConcurrentLicenseTasks:I

    move/from16 v0, p24

    iput v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxConcurrentOrBackedOffPinningTasks:I

    move/from16 v0, p25

    iput v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxConcurrentUserdataUpdateTasks:I

    move/from16 v0, p26

    iput v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxPinningTaskRetries:I

    move/from16 v0, p27

    iput v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxUpdateUserdataTaskRetries:I

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->licenseTaskRetryInterval:Lcom/google/android/youtube/videos/pinning/RetryInterval;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->pinningTaskRetryInterval:Lcom/google/android/youtube/videos/pinning/RetryInterval;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->updateUserdataTaskRetryInterval:Lcom/google/android/youtube/videos/pinning/RetryInterval;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->idle:Z

    new-instance v6, Ljava/lang/Object;

    invoke-direct {v6}, Ljava/lang/Object;-><init>()V

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    const-string v6, "connectivity"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/ConnectivityManager;

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;

    new-instance v6, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;

    invoke-direct {v6, p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;-><init>(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)V

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferencesListener:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferencesListener:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->register()V

    new-instance v6, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;

    invoke-direct {v6, p1, p0}, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;-><init>(Landroid/content/Context;Lcom/google/android/youtube/videos/utils/MediaMountedReceiver$Listener;)V

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->mediaMountedReceiver:Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->mediaMountedReceiver:Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->register()V

    new-instance v6, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;

    invoke-direct {v6, p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;-><init>(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)V

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->networkStateReceiver:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->networkStateReceiver:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->register()V

    move-object/from16 v0, p18

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->addListener(Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_tasks"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "power"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    const/4 v6, 0x1

    invoke-virtual {v3, v6, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    const-string v6, "wifi"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/WifiManager;

    sget v6, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v7, 0xc

    if-lt v6, v7, :cond_0

    invoke-direct {p0, v5, v4}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->createTasksWifiLockV12(Landroid/net/wifi/WifiManager;Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v6

    :goto_0
    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_executor"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    new-instance v6, Landroid/os/HandlerThread;

    invoke-direct {v6, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundThread:Landroid/os/HandlerThread;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v6}, Landroid/os/HandlerThread;->start()V

    new-instance v6, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$1;

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v6, p0, v7}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$1;-><init>(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;Landroid/os/Looper;)V

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    return-void

    :cond_0
    invoke-direct {p0, v5, v4}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->createTasksWifiLockV8(Landroid/net/wifi/WifiManager;Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v6

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->pingUnlessIdle()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/net/ConnectivityManager;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method private cancelBackedOffTask(Lcom/google/android/youtube/videos/pinning/Task$Key;)I
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/pinning/Task$Key;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return v0
.end method

.method private cancelRunningTask(Lcom/google/android/youtube/videos/pinning/Task$Key;)I
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/pinning/Task$Key;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/pinning/Task;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/pinning/Task;->cancel()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, v0, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    return v1
.end method

.method private cancelTaskNotOfType(Lcom/google/android/youtube/videos/pinning/Task$Key;I)I
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/pinning/Task$Key;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/pinning/Task;

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    if-eq v0, p2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelRunningTask(Lcom/google/android/youtube/videos/pinning/Task$Key;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelBackedOffTask(Lcom/google/android/youtube/videos/pinning/Task$Key;)I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private cancelTaskOfAnyType(Lcom/google/android/youtube/videos/pinning/Task$Key;)I
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/pinning/Task$Key;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelRunningTask(Lcom/google/android/youtube/videos/pinning/Task$Key;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelBackedOffTask(Lcom/google/android/youtube/videos/pinning/Task$Key;)I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private cancelTaskOfType(Lcom/google/android/youtube/videos/pinning/Task$Key;I)I
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/pinning/Task$Key;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/pinning/Task;

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    if-ne v0, p2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelRunningTask(Lcom/google/android/youtube/videos/pinning/Task$Key;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelBackedOffTask(Lcom/google/android/youtube/videos/pinning/Task$Key;)I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private createTasksWifiLockV12(Landroid/net/wifi/WifiManager;Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;
    .locals 1
    .param p1    # Landroid/net/wifi/WifiManager;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x3

    invoke-virtual {p1, v0, p2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    return-object v0
.end method

.method private createTasksWifiLockV8(Landroid/net/wifi/WifiManager;Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;
    .locals 1
    .param p1    # Landroid/net/wifi/WifiManager;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    return-object v0
.end method

.method private deleteUnusedFiles(Ljava/io/File;Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/youtube/videos/pinning/DownloadKey;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->recursivelyListFiles(Ljava/io/File;Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->getVideoIdFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v4

    const-string v5, "subtitles"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p3, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-static {v2, v3}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->clearHaveSubtitles(Lcom/google/android/youtube/videos/store/Database;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_1
    const-string v5, "knowledge"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p3, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_2
    invoke-static {v2}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->getUserFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_3
    new-instance v4, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-direct {v4, v2, v3}, Lcom/google/android/youtube/videos/pinning/DownloadKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "download_bytes_downloaded"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v3, "download_last_modified"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-static {v3, v4, v2}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_4
    return-void
.end method

.method private getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "video_userdata"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$TransfersQuery;->PROJECTION:[Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private getWishlistCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "wishlist"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$WishlistQuery;->PROJECTION:[Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private handleTaskCompleted(Lcom/google/android/youtube/videos/pinning/Task;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation

    iget-object v0, p1, Lcom/google/android/youtube/videos/pinning/Task;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, p1, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    const/4 v1, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->persistDownloadStatus(Lcom/google/android/youtube/videos/pinning/DownloadKey;IILjava/lang/Integer;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ping()V

    return-void
.end method

.method private handleTaskError(Lcom/google/android/youtube/videos/pinning/Task;Lcom/google/android/youtube/videos/pinning/Task$TaskException;)V
    .locals 16
    .param p2    # Lcom/google/android/youtube/videos/pinning/Task$TaskException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;",
            "Lcom/google/android/youtube/videos/pinning/Task$TaskException;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/youtube/videos/pinning/Task;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    if-nez v13, :cond_1

    const/4 v1, 0x0

    :goto_0
    add-int/lit8 v11, v1, 0x1

    move-object/from16 v0, p1

    iget v12, v0, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    if-nez v12, :cond_4

    move-object/from16 v0, p2

    instance-of v1, v0, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    if-nez v1, :cond_2

    const/16 v6, 0x12

    const/4 v5, 0x0

    const/4 v3, 0x1

    move-object/from16 v2, p2

    :goto_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxPinningTaskRetries:I

    if-le v11, v1, :cond_3

    const/4 v4, 0x1

    :goto_2
    move-object/from16 v1, p1

    check-cast v1, Lcom/google/android/youtube/videos/pinning/PinningTask;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logError(Ljava/lang/Throwable;ZZLjava/lang/Integer;I)V

    if-nez v3, :cond_0

    if-eqz v4, :cond_4

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "transfer fatal fail "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v15, ", "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "pinning_status"

    const/4 v15, 0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v1, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "pinning_status_reason"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v1, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "pinning_drm_error_code"

    invoke-virtual {v14, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "pinned"

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    invoke-virtual {v14, v1, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    move-object v1, v7

    check-cast v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v15, v1, v14}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ping()V

    :goto_3
    return-void

    :cond_1
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/16 :goto_0

    :cond_2
    move-object/from16 v8, p2

    check-cast v8, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    iget v6, v8, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->failedReason:I

    iget-object v5, v8, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->drmErrorCode:Ljava/lang/Integer;

    iget-boolean v3, v8, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;->fatal:Z

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    goto/16 :goto_1

    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_4
    const/4 v1, 0x3

    if-eq v12, v1, :cond_5

    const/4 v1, 0x4

    if-ne v12, v1, :cond_7

    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxUpdateUserdataTaskRetries:I

    if-le v11, v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "update userdata too many failures "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v15, ", "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    const/4 v1, 0x3

    if-ne v12, v1, :cond_6

    check-cast p1, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;->clearDirtyFlag()V

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ping()V

    goto :goto_3

    :cond_6
    check-cast p1, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;->removeItem()V

    goto :goto_4

    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "task fail "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v15, ", "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v15, ", "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/youtube/videos/pinning/Task$TaskException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v1, v7, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v1, v7, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v12, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->pinningTaskRetryInterval:Lcom/google/android/youtube/videos/pinning/RetryInterval;

    invoke-virtual {v1, v11}, Lcom/google/android/youtube/videos/pinning/RetryInterval;->getLength(I)J

    move-result-wide v9

    :goto_5
    const/4 v1, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v7, v9, v10}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->sendMessageDelayed(ILjava/lang/Object;J)I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ping()V

    goto/16 :goto_3

    :cond_8
    const/4 v1, 0x3

    if-eq v12, v1, :cond_9

    const/4 v1, 0x4

    if-ne v12, v1, :cond_a

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->updateUserdataTaskRetryInterval:Lcom/google/android/youtube/videos/pinning/RetryInterval;

    invoke-virtual {v1, v11}, Lcom/google/android/youtube/videos/pinning/RetryInterval;->getLength(I)J

    move-result-wide v9

    goto :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->licenseTaskRetryInterval:Lcom/google/android/youtube/videos/pinning/RetryInterval;

    invoke-virtual {v1, v11}, Lcom/google/android/youtube/videos/pinning/RetryInterval;->getLength(I)J

    move-result-wide v9

    goto :goto_5
.end method

.method private isCurrent(Lcom/google/android/youtube/videos/pinning/Task;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/videos/pinning/Task;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final ping()V
    .locals 39

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->networkStateReceiver:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->isConnected()Z

    move-result v34

    if-nez v34, :cond_2

    const/16 v34, 0x2

    :goto_0
    or-int v18, v18, v34

    const/16 v34, 0x19

    move/from16 v0, v34

    new-array v0, v0, [I

    move-object/from16 v22, v0

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-eqz v34, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/youtube/videos/pinning/Task;

    move-object/from16 v0, v31

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    move/from16 v32, v0

    const/16 v34, 0x2

    move/from16 v0, v32

    move/from16 v1, v34

    if-eq v0, v1, :cond_1

    const/16 v34, 0x1

    move/from16 v0, v32

    move/from16 v1, v34

    if-ne v0, v1, :cond_0

    :cond_1
    add-int/lit8 v28, v28, 0x1

    goto :goto_1

    :cond_2
    const/16 v34, 0x0

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    const-string v34, "(NOT pinned AND license_key_id IS NOT NULL AND license_asset_id IS NOT NULL AND license_system_id IS NOT NULL) OR (pinned AND license_key_id IS NOT NULL AND license_asset_id IS NOT NULL AND license_system_id IS NOT NULL AND (license_last_synced_timestamp NOT BETWEEN ? AND ? OR license_force_sync))"

    const/16 v35, 0x2

    move/from16 v0, v35

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->refreshLicensesOlderThanMillis:J

    move-wide/from16 v37, v0

    sub-long v37, v19, v37

    invoke-static/range {v37 .. v38}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v37

    aput-object v37, v35, v36

    const/16 v36, 0x1

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v37

    aput-object v37, v35, v36

    const-string v36, "pinned"

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    move-object/from16 v3, v36

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :cond_4
    :goto_2
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v34

    if-eqz v34, :cond_a

    const/16 v34, 0x2

    move/from16 v0, v34

    invoke-static {v8, v0}, Lcom/google/android/youtube/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v16

    if-eqz v16, :cond_7

    const/16 v34, 0x7

    aget v35, v22, v34

    add-int/lit8 v35, v35, 0x1

    aput v35, v22, v34

    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->readDownloadKey(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadKey;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_4

    const/4 v5, 0x1

    if-eqz v16, :cond_8

    const/16 v34, 0x2

    :goto_4
    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-direct {v0, v12, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelTaskNotOfType(Lcom/google/android/youtube/videos/pinning/Task$Key;I)I

    move-result v7

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v7, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->updateCanceledTaskCounts(I[I)V

    const/16 v34, 0x2

    move/from16 v0, v34

    if-eq v7, v0, :cond_5

    const/16 v34, 0x1

    move/from16 v0, v34

    if-ne v7, v0, :cond_6

    :cond_5
    add-int/lit8 v28, v28, -0x1

    :cond_6
    if-nez v18, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxConcurrentLicenseTasks:I

    move/from16 v34, v0

    move/from16 v0, v28

    move/from16 v1, v34

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_4

    if-eqz v16, :cond_9

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startRefreshLicenseTask(Lcom/google/android/youtube/videos/pinning/DownloadKey;)V

    :goto_5
    add-int/lit8 v28, v28, 0x1

    goto :goto_2

    :cond_7
    const/16 v34, 0x6

    aget v35, v22, v34

    add-int/lit8 v35, v35, 0x1

    aput v35, v22, v34
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v34

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v34

    :cond_8
    const/16 v34, 0x1

    goto :goto_4

    :cond_9
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startReleaseLicenseTask(Lcom/google/android/youtube/videos/pinning/DownloadKey;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :cond_a
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->mediaMountedReceiver:Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->isMediaMounted()Z

    move-result v15

    if-eqz v15, :cond_e

    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->getRootFilesDir(Landroid/content/Context;)Ljava/io/File;
    :try_end_2
    .catch Lcom/google/android/youtube/videos/utils/OfflineUtil$MediaNotMountedException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v27

    :goto_6
    move/from16 v26, v18

    if-nez v15, :cond_f

    const/16 v34, 0x4

    :goto_7
    or-int v26, v26, v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferencesListener:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;

    move-object/from16 v34, v0

    # getter for: Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->transferOnWifiOnly:Z
    invoke-static/range {v34 .. v34}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->access$000(Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;)Z

    move-result v34

    if-eqz v34, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->networkStateReceiver:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->isWifi()Z

    move-result v34

    if-nez v34, :cond_10

    const/16 v34, 0x8

    :goto_8
    or-int v26, v26, v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->isStreaming()Z

    move-result v34

    if-eqz v34, :cond_11

    const/16 v34, 0x20

    :goto_9
    or-int v26, v26, v34

    new-instance v23, Ljava/util/HashSet;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashSet;-><init>()V

    new-instance v25, Ljava/util/HashSet;

    invoke-direct/range {v25 .. v25}, Ljava/util/HashSet;-><init>()V

    const/16 v29, 0x0

    const/4 v6, 0x0

    const-string v34, "pinned"

    const/16 v35, 0x0

    const-string v36, "3!=2"

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    move-object/from16 v3, v36

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :cond_b
    :goto_a
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v34

    if-eqz v34, :cond_15

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->readDownloadKey(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadKey;

    move-result-object v12

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, v12, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v34, 0x3

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    const/16 v34, 0x4

    move/from16 v0, v24

    move/from16 v1, v34

    if-eq v0, v1, :cond_12

    const/16 v34, 0x3

    move/from16 v0, v24

    move/from16 v1, v34

    if-eq v0, v1, :cond_12

    const/16 v17, 0x1

    :goto_b
    if-eqz v17, :cond_c

    const/16 v34, 0x5

    aget v35, v22, v34

    add-int/lit8 v35, v35, 0x1

    aput v35, v22, v34

    :cond_c
    if-eqz v17, :cond_b

    invoke-virtual {v4, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_b

    const/4 v6, 0x1

    const/16 v34, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-direct {v0, v12, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelTaskNotOfType(Lcom/google/android/youtube/videos/pinning/Task$Key;I)I

    move-result v7

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v7, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->updateCanceledTaskCounts(I[I)V

    move/from16 v21, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxConcurrentOrBackedOffPinningTasks:I

    move/from16 v34, v0

    move/from16 v0, v29

    move/from16 v1, v34

    if-lt v0, v1, :cond_13

    const/16 v34, 0x10

    :goto_c
    or-int v21, v21, v34

    if-nez v21, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_d

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v12, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startPinningTask(Lcom/google/android/youtube/videos/pinning/DownloadKey;Ljava/io/File;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_d
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_a

    :catch_0
    move-exception v9

    const-string v34, "media is reported as mounted, but unable to obtain root files dir"

    invoke-static/range {v34 .. v34}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    const/16 v27, 0x0

    const/4 v15, 0x0

    goto/16 :goto_6

    :cond_e
    const/16 v27, 0x0

    goto/16 :goto_6

    :cond_f
    const/16 v34, 0x0

    goto/16 :goto_7

    :cond_10
    const/16 v34, 0x0

    goto/16 :goto_8

    :cond_11
    const/16 v34, 0x0

    goto/16 :goto_9

    :cond_12
    const/16 v17, 0x0

    goto :goto_b

    :cond_13
    const/16 v34, 0x0

    goto :goto_c

    :cond_14
    const/16 v34, 0x0

    :try_start_4
    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-direct {v0, v12, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelTaskOfType(Lcom/google/android/youtube/videos/pinning/Task$Key;I)I

    move-result v7

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v7, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->updateCanceledTaskCounts(I[I)V

    const/16 v34, 0x1

    const/16 v35, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v34

    move/from16 v2, v21

    move-object/from16 v3, v35

    invoke-virtual {v0, v12, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->persistDownloadStatus(Lcom/google/android/youtube/videos/pinning/DownloadKey;IILjava/lang/Integer;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_a

    :catchall_1
    move-exception v34

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v34

    :cond_15
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    const/16 v30, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_16
    :goto_d
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-eqz v34, :cond_18

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/youtube/videos/pinning/Task;

    move-object/from16 v0, v31

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    move/from16 v34, v0

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-eq v0, v1, :cond_17

    move-object/from16 v0, v31

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    move/from16 v34, v0

    const/16 v35, 0x4

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_16

    :cond_17
    add-int/lit8 v30, v30, 0x1

    goto :goto_d

    :cond_18
    const-string v34, "last_playback_is_dirty"

    const/16 v35, 0x0

    const/16 v36, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    move-object/from16 v3, v36

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :cond_19
    :goto_e
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v34

    if-eqz v34, :cond_1a

    const/16 v34, 0x8

    aget v35, v22, v34

    add-int/lit8 v35, v35, 0x1

    aput v35, v22, v34

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->readUpdateLastPlaybackKey(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_19

    if-nez v18, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxConcurrentUserdataUpdateTasks:I

    move/from16 v34, v0

    move/from16 v0, v30

    move/from16 v1, v34

    if-ge v0, v1, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_19

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startUpdateLastPlaybackTask(Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    add-int/lit8 v30, v30, 0x1

    goto :goto_e

    :cond_1a
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    const-string v34, "wishlist_item_state != 1"

    const/16 v35, 0x0

    const/16 v36, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    move-object/from16 v3, v36

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->getWishlistCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :cond_1b
    :goto_f
    :try_start_6
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v34

    if-eqz v34, :cond_1c

    const/16 v34, 0x9

    aget v35, v22, v34

    add-int/lit8 v35, v35, 0x1

    aput v35, v22, v34

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->readUpdateWishlistKey(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_1b

    if-nez v18, :cond_1b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->maxConcurrentUserdataUpdateTasks:I

    move/from16 v34, v0

    move/from16 v0, v30

    move/from16 v1, v34

    if-ge v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_1b

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startUpdateWishlistTask(Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    add-int/lit8 v30, v30, 0x1

    goto :goto_f

    :catchall_2
    move-exception v34

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v34

    :cond_1c
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-virtual {v13, v4}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    invoke-virtual {v13}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_10
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-eqz v34, :cond_1d

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/youtube/videos/pinning/Task$Key;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->cancelTaskOfAnyType(Lcom/google/android/youtube/videos/pinning/Task$Key;)I

    move-result v7

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v7, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->updateCanceledTaskCounts(I[I)V

    goto :goto_10

    :catchall_3
    move-exception v34

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v34

    :cond_1d
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->updateRunningTaskCounts([I)V

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->updateBackedOffTaskCounts([I)V

    const/4 v14, 0x0

    const/4 v10, 0x0

    :goto_11
    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    if-ge v10, v0, :cond_1e

    aget v34, v22, v10

    if-eqz v34, :cond_21

    const/4 v14, 0x1

    :cond_1e
    if-eqz v14, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v22

    move/from16 v2, v26

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/videos/logging/EventLogger;->onTransfersPing([II)V

    :cond_1f
    if-eqz v27, :cond_23

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, v23

    move-object/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->deleteUnusedFiles(Ljava/io/File;Ljava/util/HashSet;Ljava/util/HashSet;)V

    const-string v34, "NOT pinned AND (have_subtitles OR download_bytes_downloaded IS NOT NULL OR download_last_modified IS NOT NULL)"

    const/16 v35, 0x0

    const/16 v36, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    move-object/from16 v3, v36

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :cond_20
    :goto_12
    :try_start_7
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v34

    if-eqz v34, :cond_22

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->readDownloadKey(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadKey;

    move-result-object v12

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_20

    new-instance v33, Landroid/content/ContentValues;

    invoke-direct/range {v33 .. v33}, Landroid/content/ContentValues;-><init>()V

    const-string v34, "download_bytes_downloaded"

    invoke-virtual/range {v33 .. v34}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v34, "download_last_modified"

    invoke-virtual/range {v33 .. v34}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v34, "have_subtitles"

    const/16 v35, 0x0

    invoke-static/range {v35 .. v35}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v35

    invoke-virtual/range {v33 .. v35}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-static {v0, v12, v1}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_12

    :catchall_4
    move-exception v34

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v34

    :cond_21
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_11

    :cond_22
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_23
    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    move-result v34

    if-nez v34, :cond_26

    const/16 v34, 0x1

    :goto_13
    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->activeTransfers:Z

    if-nez v5, :cond_24

    if-eqz v6, :cond_27

    if-eqz v15, :cond_27

    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v34

    if-nez v34, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    :cond_25
    :goto_14
    return-void

    :cond_26
    const/16 v34, 0x0

    goto :goto_13

    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v34

    if-eqz v34, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_14
.end method

.method private pingUnlessIdle()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->idle:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->sendMessage(I)I

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private readDownloadKey(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/DownloadKey;
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/videos/pinning/DownloadKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private readUpdateLastPlaybackKey(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private readUpdateWishlistKey(Landroid/database/Cursor;)Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;
    .locals 4
    .param p1    # Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    invoke-direct {v3, v0, v2, v1}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-object v3
.end method

.method private sendMessage(I)I
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->idle:Z

    iget v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->postedTickets:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->postedTickets:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private sendMessage(ILjava/lang/Object;)I
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->idle:Z

    iget v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->postedTickets:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->postedTickets:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private sendMessageDelayed(ILjava/lang/Object;J)I
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # J

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->idle:Z

    iget v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->postedTickets:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->postedTickets:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private startPinningTask(Lcom/google/android/youtube/videos/pinning/DownloadKey;Ljava/io/File;)V
    .locals 26
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadKey;
    .param p2    # Ljava/io/File;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->persistDownloadStatus(Lcom/google/android/youtube/videos/pinning/DownloadKey;IILjava/lang/Integer;)V

    new-instance v2, Lcom/google/android/youtube/videos/pinning/PinningTask;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->videoGetRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->useSslForDownloads:Z

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v25, v0

    move-object/from16 v3, p1

    move-object/from16 v6, p0

    move-object/from16 v10, p2

    invoke-direct/range {v2 .. v25}, Lcom/google/android/youtube/videos/pinning/PinningTask;-><init>(Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/videos/tagging/KnowledgeClient;Landroid/content/Context;Ljava/io/File;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/StreamsSelector;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Landroid/content/SharedPreferences;ZZZLandroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/youtube/videos/pinning/Task;)V

    return-void
.end method

.method private startRefreshLicenseTask(Lcom/google/android/youtube/videos/pinning/DownloadKey;)V
    .locals 11
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadKey;

    new-instance v0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Refresh;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-boolean v8, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->legacyDownloadsHaveAppLevelDrm:Z

    iget-object v9, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;

    iget-object v10, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/youtube/videos/pinning/LicenseTask$Refresh;-><init>(Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;ZLandroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/youtube/videos/pinning/Task;)V

    return-void
.end method

.method private startReleaseLicenseTask(Lcom/google/android/youtube/videos/pinning/DownloadKey;)V
    .locals 11
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadKey;

    new-instance v0, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-boolean v8, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->legacyDownloadsHaveAppLevelDrm:Z

    iget-object v9, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;

    iget-object v10, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/youtube/videos/pinning/LicenseTask$Release;-><init>(Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;ZLandroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/youtube/videos/pinning/Task;)V

    return-void
.end method

.method private startTask(Lcom/google/android/youtube/videos/pinning/Task;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation

    iget-object v0, p1, Lcom/google/android/youtube/videos/pinning/Task;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Task:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private startUpdateLastPlaybackTask(Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;)V
    .locals 10
    .param p1    # Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;

    new-instance v0, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->videoUpdateRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;

    iget-object v9, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackTask;-><init>(Lcom/google/android/youtube/videos/pinning/UpdateLastPlaybackKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;Landroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/youtube/videos/pinning/Task;)V

    return-void
.end method

.method private startUpdateWishlistTask(Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;)V
    .locals 11
    .param p1    # Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;

    new-instance v0, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasksWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v5, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->removeFromWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->addToWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v9, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->connectivityManager:Landroid/net/ConnectivityManager;

    iget-object v10, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/youtube/videos/pinning/UpdateWishlistTask;-><init>(Lcom/google/android/youtube/videos/pinning/UpdateWishlistKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Landroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->startTask(Lcom/google/android/youtube/videos/pinning/Task;)V

    return-void
.end method

.method private updateBackedOffTaskCounts([I)V
    .locals 5
    .param p1    # [I

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/pinning/Task$Key;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v3, 0x14

    aget v4, p1, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, p1, v3

    const/16 v3, 0xf

    aget v4, p1, v3

    add-int/2addr v4, v0

    aput v4, p1, v3

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_1
    const/16 v3, 0x15

    aget v4, p1, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, p1, v3

    const/16 v3, 0x10

    aget v4, p1, v3

    add-int/2addr v4, v0

    aput v4, p1, v3

    goto :goto_0

    :pswitch_2
    const/16 v3, 0x16

    aget v4, p1, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, p1, v3

    const/16 v3, 0x11

    aget v4, p1, v3

    add-int/2addr v4, v0

    aput v4, p1, v3

    goto :goto_0

    :pswitch_3
    const/16 v3, 0x17

    aget v4, p1, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, p1, v3

    const/16 v3, 0x12

    aget v4, p1, v3

    add-int/2addr v4, v0

    aput v4, p1, v3

    goto :goto_0

    :pswitch_4
    const/16 v3, 0x18

    aget v4, p1, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, p1, v3

    const/16 v3, 0x13

    aget v4, p1, v3

    add-int/2addr v4, v0

    aput v4, p1, v3

    goto/16 :goto_0

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private updateCanceledTaskCounts(I[I)V
    .locals 2
    .param p1    # I
    .param p2    # [I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/16 v0, 0xa

    aget v1, p2, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p2, v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xb

    aget v1, p2, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p2, v0

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xc

    aget v1, p2, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p2, v0

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xd

    aget v1, p2, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p2, v0

    goto :goto_0

    :pswitch_4
    const/16 v0, 0xe

    aget v1, p2, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p2, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private updateRunningTaskCounts([I)V
    .locals 7
    .param p1    # [I

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/pinning/Task$Key;

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->tasks:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/pinning/Task;

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    iget v4, v3, Lcom/google/android/youtube/videos/pinning/Task;->taskType:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    aget v4, p1, v5

    add-int/lit8 v4, v4, 0x1

    aput v4, p1, v5

    const/16 v4, 0xf

    aget v6, p1, v4

    add-int/2addr v6, v0

    aput v6, p1, v4

    goto :goto_0

    :cond_0
    move v0, v5

    goto :goto_1

    :pswitch_1
    const/4 v4, 0x1

    aget v6, p1, v4

    add-int/lit8 v6, v6, 0x1

    aput v6, p1, v4

    const/16 v4, 0x10

    aget v6, p1, v4

    add-int/2addr v6, v0

    aput v6, p1, v4

    goto :goto_0

    :pswitch_2
    const/4 v4, 0x2

    aget v6, p1, v4

    add-int/lit8 v6, v6, 0x1

    aput v6, p1, v4

    const/16 v4, 0x11

    aget v6, p1, v4

    add-int/2addr v6, v0

    aput v6, p1, v4

    goto :goto_0

    :pswitch_3
    const/4 v4, 0x3

    aget v6, p1, v4

    add-int/lit8 v6, v6, 0x1

    aput v6, p1, v4

    const/16 v4, 0x12

    aget v6, p1, v4

    add-int/2addr v6, v0

    aput v6, p1, v4

    goto :goto_0

    :pswitch_4
    const/4 v4, 0x4

    aget v6, p1, v4

    add-int/lit8 v6, v6, 0x1

    aput v6, p1, v4

    const/16 v4, 0x13

    aget v6, p1, v4

    add-int/2addr v6, v0

    aput v6, p1, v4

    goto :goto_0

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1    # Landroid/os/Message;

    const/4 v11, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->processedTickets:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->processedTickets:I

    iget v10, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->postedTickets:I

    if-ne v6, v10, :cond_2

    iget-boolean v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->activeTransfers:Z

    if-nez v6, :cond_2

    move v6, v8

    :goto_1
    iput-boolean v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->idle:Z

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->idle:Z

    iget v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->processedTickets:I

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    const-string v6, "license_key_id IS NOT NULL AND license_asset_id IS NOT NULL AND license_system_id IS NOT NULL"

    invoke-direct {p0, v6, v11, v11}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->getTransfersCursor(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_1
    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->listener:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_3

    move v6, v8

    :goto_2
    invoke-interface {v7, v0, v6}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$Listener;->onIdle(IZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ping()V

    goto :goto_0

    :pswitch_1
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/youtube/videos/pinning/Task;

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->isCurrent(Lcom/google/android/youtube/videos/pinning/Task;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->failureCounts:Ljava/util/Map;

    iget-object v7, v5, Lcom/google/android/youtube/videos/pinning/Task;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :pswitch_2
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/youtube/videos/pinning/Task;

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->isCurrent(Lcom/google/android/youtube/videos/pinning/Task;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->handleTaskCompleted(Lcom/google/android/youtube/videos/pinning/Task;)V

    goto :goto_0

    :pswitch_3
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/util/Pair;

    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/Task;

    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->isCurrent(Lcom/google/android/youtube/videos/pinning/Task;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/Task;

    iget-object v7, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Lcom/google/android/youtube/videos/pinning/Task$TaskException;

    invoke-direct {p0, v6, v7}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->handleTaskError(Lcom/google/android/youtube/videos/pinning/Task;Lcom/google/android/youtube/videos/pinning/Task$TaskException;)V

    goto :goto_0

    :pswitch_4
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/youtube/videos/pinning/Task$Key;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backedOff:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ping()V

    goto :goto_0

    :cond_2
    move v6, v9

    goto :goto_1

    :catchall_0
    move-exception v6

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    :cond_3
    move v6, v9

    goto :goto_2

    :catchall_1
    move-exception v6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onCompleted(Lcom/google/android/youtube/videos/pinning/Task;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation

    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->sendMessage(ILjava/lang/Object;)I

    return-void
.end method

.method public onError(Lcom/google/android/youtube/videos/pinning/Task;Lcom/google/android/youtube/videos/pinning/Task$TaskException;)V
    .locals 2
    .param p2    # Lcom/google/android/youtube/videos/pinning/Task$TaskException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;",
            "Lcom/google/android/youtube/videos/pinning/Task$TaskException;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x4

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->sendMessage(ILjava/lang/Object;)I

    return-void
.end method

.method public onMediaMountedChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->pingUnlessIdle()V

    return-void
.end method

.method public onProgress(Lcom/google/android/youtube/videos/pinning/Task;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/Task",
            "<*>;)V"
        }
    .end annotation

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->sendMessage(ILjava/lang/Object;)I

    return-void
.end method

.method public onStreamingStatusChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->pingUnlessIdle()V

    return-void
.end method

.method public persistDownloadStatus(Lcom/google/android/youtube/videos/pinning/DownloadKey;IILjava/lang/Integer;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadKey;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Integer;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "pinning_status"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "pinning_status_reason"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "pinning_drm_error_code"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-static {v1, p1, v0}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    return-void
.end method

.method public quit()V
    .locals 5

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "wifiLock held in quit"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->executorWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferencesListener:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->unregister()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->mediaMountedReceiver:Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/utils/MediaMountedReceiver;->unregister()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->networkStateReceiver:Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$NetworkStateReceiver;->unregister()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->streamingStatusNotifier:Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/videos/player/StreamingStatusNotifier;->removeListener(Lcom/google/android/youtube/videos/player/StreamingStatusNotifier$Listener;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->ticketLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->postedTickets:I

    iget v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->processedTickets:I

    sub-int v0, v1, v3

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pendingMessages = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->backgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public requestPing()I
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->sendMessage(I)I

    move-result v0

    return v0
.end method
