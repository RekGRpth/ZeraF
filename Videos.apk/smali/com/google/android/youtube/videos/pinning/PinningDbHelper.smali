.class public Lcom/google/android/youtube/videos/pinning/PinningDbHelper;
.super Ljava/lang/Object;
.source "PinningDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetailsQuery;,
        Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearError(Lcom/google/android/youtube/videos/store/Database;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 13
    .param p0    # Lcom/google/android/youtube/videos/store/Database;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v12, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "pinning_status"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v8, "pinning_status_reason"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v8, "pinning_drm_error_code"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v8, "video_userdata"

    const-string v9, "pinning_account = ? AND pinning_video_id = ? AND NOT pinned AND pinning_status = 4"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object p1, v10, v11

    const/4 v11, 0x1

    aput-object p2, v10, v11

    invoke-virtual {v3, v8, v5, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    const/4 v2, 0x1

    if-lez v4, :cond_0

    move v0, v6

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0, v3, v2, v0, v1}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-lez v4, :cond_4

    :goto_2
    return v6

    :cond_0
    move v0, v7

    goto :goto_0

    :cond_1
    new-array v1, v12, [Ljava/lang/Object;

    aput-object p1, v1, v7

    aput-object p2, v1, v6

    goto :goto_1

    :catchall_0
    move-exception v8

    if-lez v4, :cond_2

    move v0, v6

    :goto_3
    if-nez v0, :cond_3

    :goto_4
    invoke-virtual {p0, v3, v2, v0, v1}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v8

    :cond_2
    move v0, v7

    goto :goto_3

    :cond_3
    new-array v1, v12, [Ljava/lang/Object;

    aput-object p1, v1, v7

    aput-object p2, v1, v6

    goto :goto_4

    :cond_4
    move v6, v7

    goto :goto_2
.end method

.method public static clearHaveSubtitles(Lcom/google/android/youtube/videos/store/Database;Ljava/lang/String;)V
    .locals 8
    .param p0    # Lcom/google/android/youtube/videos/store/Database;
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "have_subtitles"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "video_userdata"

    const-string v4, "pinning_video_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0, v7, v3}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0, v7, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method

.method public static getDownloadDetails(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;)Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;
    .locals 10
    .param p0    # Lcom/google/android/youtube/videos/store/Database;
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadKey;

    const/4 v4, 0x2

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "video_userdata"

    sget-object v2, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetailsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "pinning_account = ? AND pinning_video_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v6, p1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v4, v7

    iget-object v6, p1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v4, v9

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v5

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    invoke-static {v8, v1}, Lcom/google/android/youtube/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v3

    new-instance v1, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;

    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    :goto_1
    const/4 v5, 0x3

    invoke-static {v8, v5}, Lcom/google/android/youtube/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v5

    const/4 v6, 0x4

    invoke-interface {v8, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;-><init>(Ljava/lang/String;ZIZLjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v5, v1

    goto :goto_0

    :cond_1
    const/4 v4, -0x1

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static setPinned(Lcom/google/android/youtube/videos/store/Database;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 11
    .param p0    # Lcom/google/android/youtube/videos/store/Database;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "pinned"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v6, "pinning_notification_active"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    if-eqz p3, :cond_0

    const-string v6, "pinning_status"

    const/4 v7, 0x5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_0
    const-string v6, "pinning_status_reason"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v6, "pinning_drm_error_code"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v7, "video_userdata"

    const-string v8, "pinning_account = ? AND pinning_video_id = ? AND pinned = ?"

    const/4 v6, 0x3

    new-array v9, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v9, v6

    const/4 v6, 0x1

    aput-object p2, v9, v6

    const/4 v10, 0x2

    if-nez p3, :cond_1

    const-string v6, "1"

    :goto_1
    aput-object v6, v9, v10

    invoke-virtual {v3, v7, v5, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    const/4 v2, 0x1

    if-lez v4, :cond_2

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_3

    const/4 v1, 0x0

    :goto_3
    invoke-virtual {p0, v3, v2, v0, v1}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    if-lez v4, :cond_6

    const/4 v6, 0x1

    :goto_4
    return v6

    :cond_0
    :try_start_1
    const-string v6, "pinning_status"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    if-lez v4, :cond_4

    const/4 v0, 0x1

    :goto_5
    if-nez v0, :cond_5

    const/4 v1, 0x0

    :goto_6
    invoke-virtual {p0, v3, v2, v0, v1}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v6

    :cond_1
    :try_start_2
    const-string v6, "0"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v6, 0x2

    new-array v1, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v1, v6

    const/4 v6, 0x1

    aput-object p2, v1, v6

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    :cond_5
    const/4 v7, 0x2

    new-array v1, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v1, v7

    const/4 v7, 0x1

    aput-object p2, v1, v7

    goto :goto_6

    :cond_6
    const/4 v6, 0x0

    goto :goto_4
.end method

.method public static updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V
    .locals 10
    .param p0    # Lcom/google/android/youtube/videos/store/Database;
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadKey;
    .param p2    # Landroid/content/ContentValues;

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    :try_start_0
    const-string v2, "video_userdata"

    const-string v3, "pinning_account = ? AND pinning_video_id = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v3, v2, v8

    iget-object v3, p1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {p0, v1, v0, v7, v2}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v4, v3, v8

    iget-object v4, p1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {p0, v1, v0, v7, v3}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2
.end method
