.class Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;
.super Ljava/lang/Object;
.source "TransfersExecutor.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/pinning/TransfersExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreferencesListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

.field private volatile transferOnWifiOnly:Z

.field private final wifiRestrictOption:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # getter for: Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->context:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->access$100(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0060

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->wifiRestrictOption:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->transferOnWifiOnly:Z

    return v0
.end method

.method private final readTransferOnWifiOnlyPreference()Z
    .locals 4

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->access$200(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "download_policy"

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->wifiRestrictOption:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->wifiRestrictOption:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const-string v1, "download_policy"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->readTransferOnWifiOnlyPreference()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->transferOnWifiOnly:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->transferOnWifiOnly:Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    # invokes: Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->pingUnlessIdle()V
    invoke-static {v1}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->access$300(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)V

    :cond_0
    return-void
.end method

.method public register()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->access$200(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->readTransferOnWifiOnlyPreference()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->transferOnWifiOnly:Z

    return-void
.end method

.method public unregister()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/TransfersExecutor$PreferencesListener;->this$0:Lcom/google/android/youtube/videos/pinning/TransfersExecutor;

    # getter for: Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/youtube/videos/pinning/TransfersExecutor;->access$200(Lcom/google/android/youtube/videos/pinning/TransfersExecutor;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method
