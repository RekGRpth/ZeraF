.class Lcom/google/android/youtube/videos/pinning/PinningTask;
.super Lcom/google/android/youtube/videos/pinning/Task;
.source "PinningTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/pinning/PinningTask$1;,
        Lcom/google/android/youtube/videos/pinning/PinningTask$PurchaseFormatsQuery;,
        Lcom/google/android/youtube/videos/pinning/PinningTask$ReservedSpaceQuery;,
        Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/pinning/Task",
        "<",
        "Lcom/google/android/youtube/videos/pinning/DownloadKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final CONTENT_RANGE_HEADER:Ljava/util/regex/Pattern;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private final context:Landroid/content/Context;

.field private final database:Lcom/google/android/youtube/videos/store/Database;

.field private final debug:Z

.field private final drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

.field private gdataFormat:Ljava/lang/Integer;

.field private final knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

.field private lastReportedProgress:J

.field private final permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation
.end field

.field private final preAllocate:Z

.field private final preferences:Landroid/content/SharedPreferences;

.field private final progressGranularity:I

.field private final purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private final purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

.field private final rootFilesDir:Ljava/io/File;

.field private final streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

.field private final subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

.field private final syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;"
        }
    .end annotation
.end field

.field private final useSslForDownloads:Z

.field private final videoGetRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "^bytes (\\d+)-(\\d+)/(\\d+)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->CONTENT_RANGE_HEADER:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/videos/tagging/KnowledgeClient;Landroid/content/Context;Ljava/io/File;Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/videos/store/PurchaseStore;Lcom/google/android/youtube/videos/store/PurchaseStoreSync;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/StreamsSelector;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Landroid/content/SharedPreferences;ZZZLandroid/net/ConnectivityManager;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 8
    .param p1    # Lcom/google/android/youtube/videos/pinning/DownloadKey;
    .param p2    # Landroid/os/PowerManager$WakeLock;
    .param p3    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p4    # Lcom/google/android/youtube/videos/pinning/Task$Listener;
    .param p5    # Lcom/google/android/youtube/core/client/SubtitlesClient;
    .param p6    # Lcom/google/android/youtube/videos/tagging/KnowledgeClient;
    .param p7    # Landroid/content/Context;
    .param p8    # Ljava/io/File;
    .param p9    # Lcom/google/android/youtube/videos/store/Database;
    .param p10    # Lcom/google/android/youtube/videos/drm/DrmManager;
    .param p11    # Lcom/google/android/youtube/videos/store/PurchaseStore;
    .param p12    # Lcom/google/android/youtube/videos/store/PurchaseStoreSync;
    .param p16    # Lcom/google/android/youtube/videos/StreamsSelector;
    .param p17    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p18    # Landroid/content/SharedPreferences;
    .param p19    # Z
    .param p20    # Z
    .param p21    # Z
    .param p22    # Landroid/net/ConnectivityManager;
    .param p23    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/DownloadKey;",
            "Landroid/os/PowerManager$WakeLock;",
            "Landroid/net/wifi/WifiManager$WifiLock;",
            "Lcom/google/android/youtube/videos/pinning/Task$Listener;",
            "Lcom/google/android/youtube/core/client/SubtitlesClient;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeClient;",
            "Landroid/content/Context;",
            "Ljava/io/File;",
            "Lcom/google/android/youtube/videos/store/Database;",
            "Lcom/google/android/youtube/videos/drm/DrmManager;",
            "Lcom/google/android/youtube/videos/store/PurchaseStore;",
            "Lcom/google/android/youtube/videos/store/PurchaseStoreSync;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;",
            "Lcom/google/android/youtube/videos/StreamsSelector;",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Landroid/content/SharedPreferences;",
            "ZZZ",
            "Landroid/net/ConnectivityManager;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p23

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/videos/pinning/Task;-><init>(ILcom/google/android/youtube/videos/pinning/Task$Key;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/youtube/videos/pinning/Task$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    const-string v1, "context cannot be null"

    invoke-static {p7, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->context:Landroid/content/Context;

    const-string v1, "rootFilesDir cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    const-string v1, "drmManager cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/drm/DrmManager;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    const-string v1, "database cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/Database;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    const-string v1, "accountManagerWrapper cannot be null"

    move-object/from16 v0, p17

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v1, "purchaseStore cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/PurchaseStore;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    const-string v1, "purchaseStoreSync cannot be null"

    move-object/from16 v0, p12

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    const-string v1, "permittedStreamsRequester cannot be null"

    move-object/from16 v0, p13

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/Requester;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "syncStreamExtraRequester cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/Requester;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "videoGetRequester cannot be null"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/Requester;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->videoGetRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v1, "streamsSelector cannot be null"

    move-object/from16 v0, p16

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/StreamsSelector;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    const-string v1, "subtitlesClient cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/SubtitlesClient;

    iput-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    iput-object p6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    const/high16 v1, 0x100000

    iput v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->progressGranularity:I

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->preferences:Landroid/content/SharedPreferences;

    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->debug:Z

    move/from16 v0, p20

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->preAllocate:Z

    move/from16 v0, p21

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->useSslForDownloads:Z

    return-void
.end method

.method private acquireLicense(Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Z)V
    .locals 13
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Lcom/google/android/youtube/core/model/Stream;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/drm/DrmFallbackException;,
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    move/from16 v0, p4

    invoke-static {p2, v1, p1, v0}, Lcom/google/android/youtube/videos/drm/DrmRequest;->createPinRequest(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/videos/drm/DrmRequest;

    move-result-object v10

    new-instance v9, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v9}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v1, v10, v9}, Lcom/google/android/youtube/videos/drm/DrmManager;->request(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v9}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/youtube/videos/drm/DrmResponse;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "download_relative_filepath"

    move-object/from16 v0, p3

    invoke-virtual {v12, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "license_file_path_key"

    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v2, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "license_video_format"

    iget v2, p2, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "license_last_synced_timestamp"

    iget-wide v4, v11, Lcom/google/android/youtube/videos/drm/DrmResponse;->timestamp:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "license_key_id"

    iget-object v2, v11, Lcom/google/android/youtube/videos/drm/DrmResponse;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-wide v4, v2, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->keyId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "license_asset_id"

    iget-object v2, v11, Lcom/google/android/youtube/videos/drm/DrmResponse;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-wide v4, v2, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->assetId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "license_system_id"

    iget-object v2, v11, Lcom/google/android/youtube/videos/drm/DrmResponse;->ids:Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;

    iget-wide v4, v2, Lcom/google/android/youtube/videos/drm/DrmManager$Identifiers;->systemId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "license_force_sync"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "license_expiration_timestamp"

    invoke-virtual {v12, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v2, v1, v12}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->notifyProgress()V

    return-void

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to get offline rights "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    instance-of v1, v7, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    if-eqz v1, :cond_0

    check-cast v7, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    throw v7

    :cond_0
    instance-of v1, v7, Lcom/google/android/youtube/videos/drm/DrmException;

    if-eqz v1, :cond_2

    move-object v3, v7

    check-cast v3, Lcom/google/android/youtube/videos/drm/DrmException;

    new-instance v1, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v2, "could not acquire license"

    iget-object v4, v3, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    sget-object v5, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->NETWORK_FAILURE:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-eq v4, v5, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->parseErrorFromDrmException(Lcom/google/android/youtube/videos/drm/DrmException;)I

    move-result v5

    iget v6, v3, Lcom/google/android/youtube/videos/drm/DrmException;->errorCode:I

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZII)V

    throw v1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v2, "could not cannot acquire license"

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-direct {v1, v2, v4, v5}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v1
.end method

.method private checkSufficientFreeSpace(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;)V
    .locals 18
    .param p1    # Lcom/google/android/youtube/core/model/Stream;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/youtube/core/model/Stream;->extra:Lcom/google/android/youtube/core/model/StreamExtra;

    iget-wide v12, v2, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v11, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v2

    sub-long/2addr v12, v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "video_userdata"

    sget-object v3, Lcom/google/android/youtube/videos/pinning/PinningTask$ReservedSpaceQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "pinned AND pinning_status != 4 AND NOT (pinning_account = ? AND pinning_video_id = ?)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v5, v7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v5, v7

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    :try_start_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    new-instance v17, Landroid/os/StatFs;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual/range {v17 .. v17}, Landroid/os/StatFs;->getBlockSize()I

    move-result v4

    int-to-long v4, v4

    mul-long v9, v2, v4

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide/16 v14, 0x0

    :goto_0
    sub-long v2, v9, v14

    cmp-long v2, v2, v12

    if-gez v2, :cond_2

    new-instance v2, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v3, "insufficient free space"

    const/4 v4, 0x1

    const/4 v5, 0x7

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_1
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v14

    goto :goto_0

    :cond_2
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private close(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private close(Ljava/nio/channels/FileChannel;)V
    .locals 2
    .param p1    # Ljava/nio/channels/FileChannel;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/nio/channels/FileChannel;->force(Z)V

    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_0
    .catch Ljava/io/SyncFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v0

    :try_start_3
    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    :goto_1
    throw v0

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method private convertToSsl(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p1    # Landroid/net/Uri;

    const-string v1, "http"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/videoplayback/id/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "www.youtube.com"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "cmo"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "secure_transport=yes"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method private debug(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->debug:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_0
    invoke-static {p2}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    return-void
.end method

.method private doProgress(Ljava/lang/String;JZ)V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->lastReportedProgress:J

    sub-long v0, p2, v0

    iget v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->progressGranularity:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    if-eqz p4, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "progress "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->v(Ljava/lang/String;)V

    invoke-direct {p0, p2, p3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->persistBytesDownloaded(J)V

    iput-wide p2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->lastReportedProgress:J

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->notifyProgress()V

    :cond_1
    return-void
.end method

.method private downloadKnowledgeOperation(Lcom/google/android/youtube/core/model/Stream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/videos/async/CallbackAsFuture;

    invoke-direct {v1}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->context:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v4, v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v3, v4, v0, p1}, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->createWithCurrentLocale(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeClient;->requestPinnedKnowledgeBundle(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Unable to fetch knowledge"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Unable to fetch knowledge"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private downloadSubtitlesOperation()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    const/16 v3, 0x10

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->fetchSubtitleTracks(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "have_subtitles"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v2, v0, v1}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->notifyProgress()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "Unable to fetch subtitle tracks"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v1, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v1

    :catch_1
    move-exception v0

    const-string v1, "Unable to fetch subtitle"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v1, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v1
.end method

.method private downloadVideoOperation(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Ljava/lang/String;)V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    const-string v3, "download starting"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->debug(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->useSslForDownloads:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->convertToSsl(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    :goto_0
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v12, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x0

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "opening output "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v12}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    invoke-virtual {v12}, Ljava/io/File;->createNewFile()Z

    :cond_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v6, "rw"

    invoke-direct {v3, v12, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    invoke-virtual {v3, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "appending from "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/youtube/core/L;->v(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v6

    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "opening input "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/youtube/core/L;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_a
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    :try_start_3
    new-instance v5, Ljava/net/URL;

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v8, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask;->makeConnection(Ljava/net/URL;J)Ljava/net/HttpURLConnection;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v4

    :try_start_4
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v10

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getContentLength(Ljava/net/HttpURLConnection;)J

    move-result-wide v13

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getLastModified(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v16

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "responseCode="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " + contentType="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " contentLength="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " lastModified="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/core/L;->v(Ljava/lang/String;)V

    const/16 v17, 0x1a0

    move/from16 v0, v17

    if-ne v10, v0, :cond_4

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    const-wide/16 v17, 0x1

    sub-long v17, v8, v17

    move-object/from16 v0, p0

    move-wide/from16 v1, v17

    invoke-direct {v0, v5, v1, v2}, Lcom/google/android/youtube/videos/pinning/PinningTask;->makeConnection(Ljava/net/URL;J)Ljava/net/HttpURLConnection;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getContentLength(Ljava/net/HttpURLConnection;)J

    move-result-wide v17

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getLastModified(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "re-request due to HTTP_RANGE_NOT_SATISFIABLE: responseCode="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " + contentType="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " contentLength="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " lastModified="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/core/L;->v(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/youtube/videos/pinning/PinningTask;->is2xxStatusCode(I)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static/range {v20 .. v20}, Lcom/google/android/youtube/core/utils/Util;->toLowerInvariant(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v17, "text"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isLastModifiedValid(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v3, "download already completed"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->debug(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask;->persistDownloadSize(J)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask;->close(Ljava/io/InputStream;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/youtube/videos/pinning/PinningTask;->close(Ljava/nio/channels/FileChannel;)V

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    goto/16 :goto_0

    :catch_0
    move-exception v3

    :try_start_5
    new-instance v6, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v8, "I/O exception while seeking within video"

    const/4 v9, 0x1

    const/16 v10, 0xe

    invoke-direct {v6, v8, v3, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v6, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v6
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catch_1
    move-exception v3

    move-object v6, v4

    move-object v4, v5

    move-object v5, v7

    :goto_2
    const/4 v7, 0x1

    :try_start_6
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v3

    move-object v7, v5

    move-object v5, v6

    move-object v6, v4

    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask;->close(Ljava/io/InputStream;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/youtube/videos/pinning/PinningTask;->close(Ljava/nio/channels/FileChannel;)V

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    throw v3

    :cond_4
    move-object v5, v4

    :try_start_7
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask;->is2xxStatusCode(I)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v3, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "http status "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    const/16 v9, 0xe

    invoke-direct {v3, v4, v8, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v3
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catch_2
    move-exception v3

    move-object v4, v5

    :goto_4
    :try_start_8
    new-instance v5, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v8, "I/O exception while requesting video"

    const/4 v9, 0x0

    const/16 v10, 0xe

    invoke-direct {v5, v8, v3, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v5, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v5
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    :catch_3
    move-exception v3

    move-object v5, v7

    move-object/from16 v23, v6

    move-object v6, v4

    move-object/from16 v4, v23

    goto :goto_2

    :cond_5
    const-wide/16 v17, 0x1

    cmp-long v4, v13, v17

    if-gez v4, :cond_6

    :try_start_9
    new-instance v3, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "content length "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x1

    const/16 v9, 0xe

    invoke-direct {v3, v4, v8, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v3

    :catch_4
    move-exception v3

    move-object v4, v6

    move-object v6, v5

    move-object v5, v7

    goto/16 :goto_2

    :cond_6
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static/range {v16 .. v16}, Lcom/google/android/youtube/core/utils/Util;->toLowerInvariant(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v10, "text"

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_7
    new-instance v3, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "bad content type "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    const/16 v9, 0xe

    invoke-direct {v3, v4, v8, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v3

    :catchall_1
    move-exception v3

    goto/16 :goto_3

    :cond_8
    const-wide/16 v16, 0x0

    cmp-long v4, v8, v16

    if-lez v4, :cond_f

    const-string v4, "Content-Range"

    invoke-virtual {v5, v4}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v16, "-"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_a

    :cond_9
    new-instance v3, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Content-Range "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, ", not "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    const/16 v9, 0xe

    invoke-direct {v3, v4, v8, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v3

    :cond_a
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v15}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isLastModifiedValid(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    new-instance v3, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "lastModified "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " changed to "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x1

    const/16 v9, 0xe

    invoke-direct {v3, v4, v8, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v3

    :cond_b
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/youtube/videos/pinning/PinningTask;->persistLastModified(Ljava/lang/String;)V

    :cond_c
    :goto_5
    add-long/2addr v13, v8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "size is "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->v(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/google/android/youtube/videos/pinning/PinningTask;->persistDownloadSize(J)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->preAllocate:Z

    if-eqz v4, :cond_d

    invoke-virtual {v3, v13, v14}, Ljava/io/RandomAccessFile;->setLength(J)V

    :cond_d
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v7

    const/high16 v3, 0x20000

    :try_start_a
    new-array v15, v3, [B

    invoke-static {v15}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v16

    const/4 v3, 0x0

    move/from16 v23, v3

    move-wide v3, v8

    move/from16 v8, v23

    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v9

    if-nez v9, :cond_e

    const/4 v9, -0x1

    if-eq v8, v9, :cond_e

    const/4 v8, 0x0

    const/high16 v9, 0x20000

    :try_start_b
    invoke-virtual {v7, v15, v8, v9}, Ljava/io/InputStream;->read([BII)I
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result v8

    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_6
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move-result v9

    if-eqz v9, :cond_10

    :cond_e
    const/4 v3, 0x1

    :try_start_d
    invoke-virtual {v6, v3}, Ljava/nio/channels/FileChannel;->force(Z)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :try_start_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v3

    if-eqz v3, :cond_18

    const-string v3, "download canceled"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->debug(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask;->close(Ljava/io/InputStream;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/youtube/videos/pinning/PinningTask;->close(Ljava/nio/channels/FileChannel;)V

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_1

    :cond_f
    :try_start_f
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/youtube/videos/pinning/PinningTask;->persistLastModified(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto :goto_5

    :catch_5
    move-exception v3

    :try_start_10
    new-instance v4, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v8, "I/O exception while downloading video"

    const/4 v9, 0x0

    const/16 v10, 0xe

    invoke-direct {v4, v8, v3, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v4

    :catch_6
    move-exception v3

    move-object v4, v6

    move-object v6, v5

    move-object v5, v7

    goto/16 :goto_2

    :cond_10
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/youtube/videos/pinning/PinningTask;->preAllocate:Z
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    if-nez v9, :cond_12

    :try_start_11
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v9

    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v17

    cmp-long v19, v9, v3

    if-nez v19, :cond_11

    cmp-long v19, v17, v3

    if-eqz v19, :cond_12

    :cond_11
    new-instance v8, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Inconsistent filechannel status ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/16 v9, 0xe

    invoke-direct {v8, v3, v4, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v8, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v8
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_6
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    :catch_7
    move-exception v3

    :try_start_12
    new-instance v4, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v8, "I/O exception while determining video size"

    const/4 v9, 0x1

    const/16 v10, 0xe

    invoke-direct {v4, v8, v3, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v4
    :try_end_12
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_6
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :cond_12
    if-lez v8, :cond_15

    const/4 v9, 0x0

    :try_start_13
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    const/4 v9, 0x0

    move v10, v9

    :goto_8
    if-ge v10, v8, :cond_14

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v9

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ge v9, v0, :cond_13

    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to write any data to "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_8
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_13} :catch_6
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    :catch_8
    move-exception v3

    :try_start_14
    new-instance v4, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v8, "I/O exception while writing video"

    const/4 v9, 0x0

    const/16 v10, 0xe

    invoke-direct {v4, v8, v3, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v4
    :try_end_14
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_6
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    :cond_13
    add-int/2addr v9, v10

    move v10, v9

    goto :goto_8

    :cond_14
    if-ne v10, v8, :cond_16

    const/4 v9, 0x1

    :goto_9
    :try_start_15
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "wrote "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v17, " != "

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/youtube/core/utils/Preconditions;->checkState(ZLjava/lang/Object;)V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_8
    .catch Ljava/lang/RuntimeException; {:try_start_15 .. :try_end_15} :catch_6
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    int-to-long v9, v8

    add-long/2addr v3, v9

    :cond_15
    const/4 v9, -0x1

    if-ne v8, v9, :cond_17

    const/4 v9, 0x1

    :goto_a
    :try_start_16
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v3, v4, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask;->doProgress(Ljava/lang/String;JZ)V

    goto/16 :goto_6

    :cond_16
    const/4 v9, 0x0

    goto :goto_9

    :cond_17
    const/4 v9, 0x0

    goto :goto_a

    :catch_9
    move-exception v3

    new-instance v4, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v8, "I/O exception while writing video"

    const/4 v9, 0x0

    const/16 v10, 0xe

    invoke-direct {v4, v8, v3, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v4

    :cond_18
    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v3, v3, v13

    if-eqz v3, :cond_1a

    new-instance v4, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "download completed with unexpected size "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " expecting "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v9

    cmp-long v3, v9, v13

    if-lez v3, :cond_19

    const/4 v3, 0x1

    :goto_b
    const/16 v9, 0xe

    invoke-direct {v4, v8, v3, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->logException(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v4

    :cond_19
    const/4 v3, 0x0

    goto :goto_b

    :cond_1a
    const-string v3, "download completed"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->debug(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_16
    .catch Ljava/lang/RuntimeException; {:try_start_16 .. :try_end_16} :catch_6
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    goto/16 :goto_7

    :catchall_2
    move-exception v3

    move-object v6, v5

    move-object v5, v4

    goto/16 :goto_3

    :catchall_3
    move-exception v3

    move-object v5, v4

    goto/16 :goto_3

    :catchall_4
    move-exception v3

    move-object v5, v4

    goto/16 :goto_3

    :catch_a
    move-exception v3

    move-object v5, v7

    move-object/from16 v23, v6

    move-object v6, v4

    move-object/from16 v4, v23

    goto/16 :goto_2

    :catch_b
    move-exception v3

    goto/16 :goto_4
.end method

.method private fetchSubtitleTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/model/SubtitleTrack;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/android/youtube/core/model/Subtitles;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Requesting subtitles "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->i(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/youtube/videos/async/CallbackAsFuture;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/core/client/SubtitlesClient;->requestSubtitles(Lcom/google/android/youtube/core/model/SubtitleTrack;Lcom/google/android/youtube/core/async/Callback;)V

    return-object v0
.end method

.method private fetchSubtitleTracks(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/ExecutionException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    new-instance v2, Lcom/google/android/youtube/videos/async/CallbackAsFuture;

    invoke-direct {v2}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->subtitlesClient:Lcom/google/android/youtube/core/client/SubtitlesClient;

    invoke-interface {v3, p1, v2}, Lcom/google/android/youtube/core/client/SubtitlesClient;->requestSubtitleTracks(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/SubtitleTrack;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/pinning/PinningTask;->fetchSubtitleTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)Ljava/util/concurrent/Future;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    goto :goto_0
.end method

.method private getChecksum(Ljava/io/File;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v2}, Lcom/google/android/youtube/videos/utils/Hashing;->sha1(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/pinning/PinningTask;->close(Ljava/io/InputStream;)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    move-object v2, v0

    :goto_1
    :try_start_2
    const-string v3, "checksum failed"

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/pinning/PinningTask;->close(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/pinning/PinningTask;->close(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private getContentLength(Ljava/net/HttpURLConnection;)J
    .locals 7

    const-wide/16 v0, -0x1

    const-string v2, "Content-Length"

    invoke-virtual {p1, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :cond_0
    :goto_0
    const-string v2, "Content-Range"

    invoke-virtual {p1, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/youtube/videos/pinning/PinningTask;->CONTENT_RANGE_HEADER:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sub-long v2, v5, v2

    const-wide/16 v5, 0x1

    add-long/2addr v2, v5

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-gez v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Using contentLength parsed from Content-Range "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    move-wide v0, v2

    :cond_1
    :goto_1
    return-wide v0

    :cond_2
    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v0

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method private getDownloadStreamWithExtra(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;
    .locals 5
    .param p1    # Lcom/google/android/youtube/core/model/Stream;

    new-instance v2, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v2}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v4, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-interface {v3, v4, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/StreamExtra;

    sget-object v3, Lcom/google/android/youtube/core/model/StreamExtra;->NO_EXTRA:Lcom/google/android/youtube/core/model/StreamExtra;

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/core/model/StreamExtra;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/core/model/Stream$Builder;->extra(Lcom/google/android/youtube/core/model/StreamExtra;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private getLastModified(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 2

    const-string v0, "Last-Modified"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private getPermittedStreamsOperation(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/List;)Ljava/util/Map;
    .locals 8
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    new-instance v3, Lcom/google/android/youtube/videos/async/StreamsRequest;

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-direct {v3, v4, p2}, Lcom/google/android/youtube/videos/async/StreamsRequest;-><init>(Ljava/lang/String;Ljava/util/List;)V

    new-instance v2, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v2}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v4, v3, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to get streams for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v4, v1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    if-eqz v4, :cond_0

    new-instance v4, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v5, "video not playable"

    const/4 v6, 0x1

    check-cast v1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    iget-object v7, v1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;->state:Lcom/google/android/youtube/core/model/Video$State;

    invoke-direct {p0, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask;->parseErrorFromVideoState(Lcom/google/android/youtube/core/model/Video$State;)I

    move-result v7

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v4

    :cond_0
    new-instance v4, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v5, "could not fetch permitted streams"

    const/4 v6, 0x0

    const/4 v7, 0x6

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v4
.end method

.method private getPurchasedFormats(Lcom/google/android/youtube/videos/accounts/UserAuth;)Ljava/util/List;
    .locals 11
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v8, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    sget-object v9, Lcom/google/android/youtube/videos/pinning/PinningTask$PurchaseFormatsQuery;->PROJECTION:[Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v7, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v7, v7, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v8, v9, v7, v1, v2}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createActivePurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v5

    new-instance v3, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v3}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v7, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    invoke-virtual {v7, v5, v3}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_1

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_0

    const/4 v7, 0x0

    invoke-static {v4, v7}, Lcom/google/android/youtube/videos/utils/DbUtils;->getIntegerList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v7

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v7
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    new-instance v7, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v8, "could not obtain purchase cursor"

    const/4 v9, 0x1

    const/16 v10, 0x12

    invoke-direct {v7, v8, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v7

    :cond_0
    :try_start_3
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_0

    return-object v7

    :cond_1
    :try_start_5
    new-instance v7, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v8, "no active purchase"

    const/4 v9, 0x0

    const/4 v10, 0x5

    invoke-direct {v7, v8, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private getUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    const/16 v4, 0x11

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v2, "account does not exist"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->blockingGetUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v1, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v2, "couldn\'t get userAuth"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v1

    :cond_1
    return-object v0
.end method

.method private is2xxStatusCode(I)Z
    .locals 1
    .param p1    # I

    const/16 v0, 0xc8

    if-lt p1, v0, :cond_0

    const/16 v0, 0x12c

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLastModifiedValid(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logException(Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "download error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private makeConnection(Ljava/net/URL;J)Ljava/net/HttpURLConnection;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v1, 0x7530

    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const-string v1, "Range"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bytes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    return-object v0
.end method

.method private mergePlaybackPosition(Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    .locals 13
    .param p1    # Lcom/google/wireless/android/video/magma/proto/VideoResource;

    const/4 v7, 0x5

    const/4 v11, 0x2

    const/4 v12, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hasPlayback()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->getPlayback()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    const/4 v6, 0x3

    new-array v4, v6, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v4, v8

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v4, v12

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStopTimestampMsec()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v11

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v6}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "last_playback_start_timestamp"

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStartTimestampMsec()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "last_watched_timestamp"

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getStopTimestampMsec()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "resume_timestamp"

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->getPositionMsec()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "last_playback_is_dirty"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v6, "video_userdata"

    const-string v9, "pinning_account = ? AND pinning_video_id = ? AND ? > last_watched_timestamp"

    invoke-virtual {v3, v6, v5, v9, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    const/4 v2, 0x1

    iget-object v9, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    if-lez v1, :cond_1

    :goto_1
    new-array v10, v11, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v10, v8

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v10, v12

    invoke-virtual {v9, v3, v2, v7, v10}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_1
    move v7, v8

    goto :goto_1

    :catchall_0
    move-exception v6

    move-object v9, v6

    iget-object v10, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    if-lez v1, :cond_2

    :goto_2
    new-array v11, v11, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    aput-object v6, v11, v8

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    aput-object v6, v11, v12

    invoke-virtual {v10, v3, v2, v7, v11}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v9

    :cond_2
    move v7, v8

    goto :goto_2
.end method

.method private parseErrorFromDrmException(Lcom/google/android/youtube/videos/drm/DrmException;)I
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmException;

    iget-object v0, p1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/videos/pinning/PinningTask$1;->$SwitchMap$com$google$android$youtube$videos$drm$DrmException$DrmError:[I

    iget-object v1, p1, Lcom/google/android/youtube/videos/drm/DrmException;->drmError:Lcom/google/android/youtube/videos/drm/DrmException$DrmError;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/drm/DrmException$DrmError;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x18

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x13

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x14

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x15

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x16

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x17

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private parseErrorFromVideoState(Lcom/google/android/youtube/core/model/Video$State;)I
    .locals 3

    const/16 v0, 0x8

    if-nez p1, :cond_0

    :goto_0
    :pswitch_0
    return v0

    :cond_0
    sget-object v1, Lcom/google/android/youtube/videos/pinning/PinningTask$1;->$SwitchMap$com$google$android$youtube$core$model$Video$State:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const-string v1, "should not happen"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_4
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_5
    const/16 v0, 0xb

    goto :goto_0

    :pswitch_6
    const/4 v1, 0x0

    const-string v2, "error state for video cannot be PLAYABLE"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method private persistBytesDownloaded(J)V
    .locals 3

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "download_bytes_downloaded"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v2, v0, v1}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    return-void
.end method

.method private persistDownloadSize(J)V
    .locals 5

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "pinning_download_size"

    const-wide/16 v3, 0x0

    cmp-long v0, p1, v3

    if-lez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v2, v0, v1}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private persistLastModified(Ljava/lang/String;)V
    .locals 3

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "download_last_modified"

    invoke-virtual {v1, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v2, v0, v1}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    return-void
.end method

.method private refreshPlaybackPosition()V
    .locals 7

    new-instance v0, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v0}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->videoGetRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v5, Lcom/google/android/youtube/videos/api/VideoGetRequest;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v3, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v3, v3, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-direct {v5, v6, v3}, Lcom/google/android/youtube/videos/api/VideoGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5, v0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/pinning/PinningTask;->mergePlaybackPosition(Lcom/google/wireless/android/video/magma/proto/VideoResource;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v3, "Unable to fetch updated last playback"

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setupExistingDownload(Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/util/Map;)Landroid/util/Pair;
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/core/model/Stream;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v5, 0x1

    iget-object v0, p1, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->relativeFilePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v3, "empty relativeFilePath"

    const/16 v4, 0x12

    invoke-direct {v2, v3, v5, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v2

    :cond_0
    iget v2, p1, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->licenseVideoFormat:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->gdataFormat:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->gdataFormat:Ljava/lang/Integer;

    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "licensed format is no longer permitted: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->gdataFormat:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    invoke-direct {v2, v3, v5, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v2

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->gdataFormat:Ljava/lang/Integer;

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getDownloadStreamWithExtra(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v2, v3

    :goto_0
    return-object v2

    :cond_2
    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->checkSufficientFreeSpace(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v2, v3

    goto :goto_0

    :cond_3
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto :goto_0
.end method

.method private setupNewDownload(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;I)Landroid/util/Pair;
    .locals 11
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;I)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/core/model/Stream;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v7, 0x1

    if-ne p3, v7, :cond_1

    move v2, v7

    :goto_0
    iget-object v9, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->context:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v10, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v6, v6, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {v9, v10, v6, v2}, Lcom/google/android/youtube/videos/utils/OfflineUtil;->getRelativePathForVideoId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    invoke-direct {v1, v6, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v6, v8

    :goto_1
    return-object v6

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->preferences:Landroid/content/SharedPreferences;

    const-string v9, "enable_surround_sound"

    invoke-interface {v6, v9, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    :try_start_0
    iget-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->streamsSelector:Lcom/google/android/youtube/videos/StreamsSelector;

    invoke-virtual {v6, p2, p3, v5}, Lcom/google/android/youtube/videos/StreamsSelector;->getDownloadStream(Ljava/util/Map;IZ)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getDownloadStreamWithExtra(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v4

    iget v6, v4, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->gdataFormat:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v6, v8

    goto :goto_1

    :cond_3
    iget-object v6, v4, Lcom/google/android/youtube/core/model/Stream;->extra:Lcom/google/android/youtube/core/model/StreamExtra;

    iget-wide v9, v6, Lcom/google/android/youtube/core/model/StreamExtra;->sizeInBytes:J

    invoke-direct {p0, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask;->persistDownloadSize(J)V

    invoke-direct {p0, v4, v3}, Lcom/google/android/youtube/videos/pinning/PinningTask;->checkSufficientFreeSpace(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v6, v8

    goto :goto_1

    :cond_4
    invoke-direct {p0, p1, v4, v3, v2}, Lcom/google/android/youtube/videos/pinning/PinningTask;->acquireLicense(Lcom/google/android/youtube/videos/accounts/UserAuth;Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Z)V

    invoke-static {v4, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lcom/google/android/youtube/core/player/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/youtube/videos/drm/DrmFallbackException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v6, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to select a download stream for: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x6

    invoke-direct {v6, v8, v0, v7, v9}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v6

    :catch_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v6

    if-eqz v6, :cond_5

    move-object v6, v8

    goto :goto_1

    :cond_5
    iget v6, v0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;->fallbackDrmLevel:I

    invoke-direct {p0, p1, p2, v6}, Lcom/google/android/youtube/videos/pinning/PinningTask;->setupNewDownload(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;I)Landroid/util/Pair;

    move-result-object v6

    goto :goto_1
.end method

.method private syncPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 8
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {p1, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    new-instance v2, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v2}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->purchaseStoreSync:Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    invoke-virtual {v4, v3, v2}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Landroid/util/Pair;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to sync purchases for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v4, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v4, v4, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v4, v1, Lcom/google/android/youtube/videos/store/PurchaseStoreSync$SyncException;

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    :cond_0
    instance-of v4, v1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    if-eqz v4, :cond_1

    new-instance v4, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v5, "video not playable"

    const/4 v6, 0x1

    check-cast v1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;

    iget-object v7, v1, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;->state:Lcom/google/android/youtube/core/model/Video$State;

    invoke-direct {p0, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask;->parseErrorFromVideoState(Lcom/google/android/youtube/core/model/Video$State;)I

    move-result v7

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v4

    :cond_1
    new-instance v4, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    const-string v5, "could not fetch purchases"

    const/4 v6, 0x0

    const/16 v7, 0xd

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V

    throw v4
.end method


# virtual methods
.method public execute()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;
        }
    .end annotation

    iget-object v9, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v8, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v9, v8}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->getDownloadDetails(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;)Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v2, v0, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->haveLicense:Z

    iget-boolean v3, v0, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->haveSubtitles:Z

    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->drmManager:Lcom/google/android/youtube/videos/drm/DrmManager;

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/drm/DrmManager;->getDrmLevel()I

    move-result v4

    if-gez v4, :cond_2

    new-instance v8, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Device unlocked. DrmLevel = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x4

    invoke-direct {v8, v9, v10, v11}, Lcom/google/android/youtube/videos/pinning/PinningTask$PinningException;-><init>(Ljava/lang/String;ZI)V

    throw v8

    :cond_2
    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v8, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v8, v8, Lcom/google/android/youtube/videos/pinning/DownloadKey;->account:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getUserAuth(Ljava/lang/String;)Lcom/google/android/youtube/videos/accounts/UserAuth;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-direct {p0, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask;->syncPurchases(Lcom/google/android/youtube/videos/accounts/UserAuth;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-direct {p0, v7}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getPurchasedFormats(Lcom/google/android/youtube/videos/accounts/UserAuth;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-direct {p0, v7, v1}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getPermittedStreamsOperation(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/List;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v8

    if-nez v8, :cond_0

    if-eqz v2, :cond_5

    invoke-direct {p0, v0, v5}, Lcom/google/android/youtube/videos/pinning/PinningTask;->setupExistingDownload(Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/util/Map;)Landroid/util/Pair;

    move-result-object v6

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->refreshPlaybackPosition()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v8

    if-nez v8, :cond_0

    if-nez v3, :cond_3

    invoke-direct {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->downloadSubtitlesOperation()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v8

    if-nez v8, :cond_0

    :cond_3
    iget-object v8, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->knowledgeClient:Lcom/google/android/youtube/videos/tagging/KnowledgeClient;

    if-eqz v8, :cond_4

    iget-object v8, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v8}, Lcom/google/android/youtube/videos/pinning/PinningTask;->downloadKnowledgeOperation(Lcom/google/android/youtube/core/model/Stream;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/pinning/PinningTask;->isCanceled()Z

    move-result v8

    if-nez v8, :cond_0

    :cond_4
    iget-object v8, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Lcom/google/android/youtube/core/model/Stream;

    iget-object v9, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/lang/String;

    iget-object v10, v0, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->lastModified:Ljava/lang/String;

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/youtube/videos/pinning/PinningTask;->downloadVideoOperation(Lcom/google/android/youtube/core/model/Stream;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    invoke-direct {p0, v7, v5, v4}, Lcom/google/android/youtube/videos/pinning/PinningTask;->setupNewDownload(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;I)Landroid/util/Pair;

    move-result-object v6

    goto :goto_1
.end method

.method public logError(Ljava/lang/Throwable;ZZLjava/lang/Integer;I)V
    .locals 8
    .param p1    # Ljava/lang/Throwable;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Ljava/lang/Integer;
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->gdataFormat:Ljava/lang/Integer;

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    move v7, p5

    invoke-interface/range {v0 .. v7}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPinningError(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Throwable;ZZLjava/lang/Integer;I)V

    return-void
.end method

.method protected onCompleted()V
    .locals 9

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->database:Lcom/google/android/youtube/videos/store/Database;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    invoke-static {v1, v0}, Lcom/google/android/youtube/videos/pinning/PinningDbHelper;->getDownloadDetails(Lcom/google/android/youtube/videos/store/Database;Lcom/google/android/youtube/videos/pinning/DownloadKey;)Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;

    move-result-object v7

    if-nez v7, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v8, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->rootFilesDir:Ljava/io/File;

    iget-object v1, v7, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->relativeFilePath:Ljava/lang/String;

    invoke-direct {v8, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-direct {p0, v8}, Lcom/google/android/youtube/videos/pinning/PinningTask;->getChecksum(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->key:Lcom/google/android/youtube/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;

    iget-object v1, v1, Lcom/google/android/youtube/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/pinning/PinningTask;->gdataFormat:Ljava/lang/Integer;

    iget-object v5, v7, Lcom/google/android/youtube/videos/pinning/PinningDbHelper$DownloadDetails;->lastModified:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPinningCompleted(Ljava/lang/String;Ljava/lang/Integer;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onError(Ljava/lang/Throwable;)V
    .locals 0
    .param p1    # Ljava/lang/Throwable;

    return-void
.end method
