.class Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V14;
.super Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;
.source "SignInIntentFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V14"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V14;-><init>()V

    return-void
.end method


# virtual methods
.method public newChooseAccountIntent(Landroid/content/Context;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p3    # Landroid/accounts/Account;
    .param p4    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    new-array v2, v3, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccountType()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v0

    iget-object v0, p2, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v5, v0, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    move-object v0, p3

    move-object v6, v4

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Landroid/accounts/AccountManager;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
