.class Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;
.super Landroid/widget/BaseAdapter;
.source "SignInActivityV8.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/accounts/SignInActivityV8;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AccountAdapter"
.end annotation


# instance fields
.field private final accountNames:[Ljava/lang/String;

.field private final addAccountText:Ljava/lang/String;

.field private final inflater:Landroid/view/LayoutInflater;

.field private final selectedIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # [Ljava/lang/String;
    .param p3    # I

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->inflater:Landroid/view/LayoutInflater;

    const v0, 0x7f0a0068

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->addAccountText:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->accountNames:[Ljava/lang/String;

    iput p3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->selectedIndex:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->accountNames:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->accountNames:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->accountNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->selectedIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->accountNames:[Ljava/lang/String;

    array-length v0, v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->selectedIndex:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->accountNames:[Ljava/lang/String;

    array-length v3, v3

    if-ne p1, v3, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    move-object v1, p2

    :goto_0
    check-cast v1, Landroid/widget/TextView;

    :goto_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->accountNames:[Ljava/lang/String;

    array-length v2, v2

    if-ge p1, v2, :cond_5

    iget-object v2, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->accountNames:[Ljava/lang/String;

    aget-object v2, v2, p1

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v1

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->inflater:Landroid/view/LayoutInflater;

    const v4, 0x1090011

    invoke-virtual {v3, v4, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_4

    move-object v0, p2

    :goto_3
    check-cast v0, Landroid/widget/CheckedTextView;

    iget v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->selectedIndex:I

    if-ne p1, v3, :cond_3

    const/4 v2, 0x1

    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    move-object v1, v0

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->inflater:Landroid/view/LayoutInflater;

    const v4, 0x1090012

    invoke-virtual {v3, v4, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_3

    :cond_5
    iget-object v2, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->addAccountText:Ljava/lang/String;

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$AccountAdapter;->selectedIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
