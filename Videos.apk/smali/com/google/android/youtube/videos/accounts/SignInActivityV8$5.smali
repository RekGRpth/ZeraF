.class Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;
.super Ljava/lang/Object;
.source "SignInActivityV8.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->addAccount()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string v3, "authAccount"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    # invokes: Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;
    invoke-static {v3, v0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->access$300(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "added account "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    # invokes: Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onAccountSelected(Ljava/lang/String;)V
    invoke-static {v3, v0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->access$100(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "account with name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not created"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    # invokes: Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onAccountNotAdded()V
    invoke-static {v3}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->access$400(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "added account canceled"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    # invokes: Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onSignInCanceled()V
    invoke-static {v3}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->access$000(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "added account IOException"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    # invokes: Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onSignInError(Ljava/lang/Exception;)V
    invoke-static {v3, v1}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->access$500(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v3, "added account AuthenticatorException"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$5;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    # invokes: Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onSignInError(Ljava/lang/Exception;)V
    invoke-static {v3, v1}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->access$500(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;Ljava/lang/Exception;)V

    goto :goto_0
.end method
