.class public Lcom/google/android/youtube/videos/accounts/AccountsChangedService;
.super Landroid/app/IntentService;
.source "AccountsChangedService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/accounts/AccountsChangedService$AccountsChangedReceiver;
    }
.end annotation


# instance fields
.field private videosApplication:Lcom/google/android/youtube/videos/VideosApplication;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-class v0, Lcom/google/android/youtube/videos/accounts/AccountsChangedService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/accounts/AccountsChangedService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountsChangedService;->videosApplication:Lcom/google/android/youtube/videos/VideosApplication;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v4, 0x0

    new-instance v1, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v1}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    new-instance v2, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v2}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/AccountsChangedService;->videosApplication:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStoreSync()Lcom/google/android/youtube/videos/store/PurchaseStoreSync;

    move-result-object v3

    invoke-virtual {v3, v4, v1}, Lcom/google/android/youtube/videos/store/PurchaseStoreSync;->cleanup(ILcom/google/android/youtube/core/async/Callback;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/AccountsChangedService;->videosApplication:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getWishlistStoreSync()Lcom/google/android/youtube/videos/store/WishlistStoreSync;

    move-result-object v3

    invoke-virtual {v3, v4, v2}, Lcom/google/android/youtube/videos/store/WishlistStoreSync;->cleanup(ILcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v3, "Failed to purge stores"

    invoke-static {v3, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/AccountsChangedService;->videosApplication:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getSignInManager()Lcom/google/android/youtube/videos/accounts/SignInManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->clearSignedInAccountIfRemoved()V

    invoke-super {p0, p1, p2, p3}, Landroid/app/IntentService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method
