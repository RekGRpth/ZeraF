.class Lcom/google/android/youtube/videos/accounts/SignInActivityV8$3;
.super Ljava/lang/Object;
.source "SignInActivityV8.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$3;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$3;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    # invokes: Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->onAccountSelected(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->access$100(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;Ljava/lang/String;)V

    :goto_0
    const-string v1, "logging in"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/SignInActivityV8$3;->this$0:Lcom/google/android/youtube/videos/accounts/SignInActivityV8;

    # invokes: Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->addAccount()V
    invoke-static {v1}, Lcom/google/android/youtube/videos/accounts/SignInActivityV8;->access$200(Lcom/google/android/youtube/videos/accounts/SignInActivityV8;)V

    goto :goto_0
.end method
