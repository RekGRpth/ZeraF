.class public Lcom/google/android/youtube/videos/accounts/SignInManager;
.super Ljava/lang/Object;
.source "SignInManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private listener:Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;

.field private loaded:Z

.field private final preferences:Landroid/content/SharedPreferences;

.field private final signInIntentFactory:Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;

.field private signedInAccount:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->preferences:Landroid/content/SharedPreferences;

    const-string v0, "signInIntentFactory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signInIntentFactory:Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;

    return-void
.end method

.method private getSavedAccount()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "user_account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private reloadSavedAccountIfNeeded()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->loaded:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->getSavedAccount()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signedInAccount:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->loaded:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signedInAccount:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signedInAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signedInAccount:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->removeSavedAccount()V

    goto :goto_0
.end method

.method private removeSavedAccount()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_account"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->listener:Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->listener:Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;->onSignedInAccountChanged()V

    :cond_0
    return-void
.end method

.method private saveAccount()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_account"

    iget-object v2, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signedInAccount:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->applyChangesToSharedPreferences(Landroid/content/SharedPreferences$Editor;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->listener:Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->listener:Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;->onSignedInAccountChanged()V

    :cond_0
    return-void
.end method


# virtual methods
.method public chooseAccount(Landroid/app/Activity;Z)V
    .locals 6
    .param p1    # Landroid/app/Activity;
    .param p2    # Z

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "allowSkip"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signInIntentFactory:Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    iget-object v5, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v5, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5, v2}, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;->newChooseAccountIntent(Landroid/content/Context;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    const/16 v3, 0x387

    invoke-virtual {p1, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public chooseSingleAccount()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signedInAccount:Ljava/lang/String;

    goto :goto_0
.end method

.method public clearSignedInAccountIfRemoved()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->reloadSavedAccountIfNeeded()V

    return-void
.end method

.method public getSignedInAccount()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->reloadSavedAccountIfNeeded()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signedInAccount:Ljava/lang/String;

    return-object v0
.end method

.method public onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/content/Intent;

    const/16 v1, 0x387

    if-eq p2, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    if-ne p3, v1, :cond_1

    if-eqz p4, :cond_1

    const-string v1, "authAccount"

    invoke-virtual {p4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->listener:Lcom/google/android/youtube/videos/accounts/SignInManager$SignInManagerListener;

    return-void
.end method

.method public setSignedInAccount(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "account cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->loaded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signedInAccount:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->signedInAccount:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/accounts/SignInManager;->saveAccount()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/accounts/SignInManager;->loaded:Z

    :cond_1
    return-void
.end method
