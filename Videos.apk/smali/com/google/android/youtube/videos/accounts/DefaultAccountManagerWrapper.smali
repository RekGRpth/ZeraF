.class public Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;
.super Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
.source "DefaultAccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;)V
    .locals 1
    .param p1    # Landroid/accounts/AccountManager;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    const-string v0, "com.google"

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;-><init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/accounts/AccountManager;
    .param p2    # Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;-><init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public blockingGetUserAuthInternal(Landroid/accounts/Account;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 6
    .param p1    # Landroid/accounts/Account;

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v4, v4, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v4, v5}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/youtube/videos/accounts/UserAuth;-><init>(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;Ljava/lang/String;)V

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "blockingGetUserAuth failed with AuthenticatorException"

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v3, "blockingGetUserAuth failed with IOException"

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    const-string v3, "got null authToken for the selected account"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getUserAuthInternal(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 7
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/app/Activity;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v2, v1, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    new-instance v5, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v5, p0, v1, p3}, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;-><init>(Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    move-object v1, p1

    move-object v4, p2

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method protected peekUserAuthInternal(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 2
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->peekUserAuthInternalV14(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->peekUserAuthInternalV8(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    goto :goto_0
.end method

.method protected peekUserAuthInternalV14(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 7
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v2, v1, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v5, p0, v1, p2}, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;-><init>(Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    move-object v1, p1

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method protected peekUserAuthInternalV8(Landroid/accounts/Account;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 6
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v0, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v2, v1, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v4, p0, v1, p2}, Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;-><init>(Lcom/google/android/youtube/videos/accounts/DefaultAccountManagerWrapper;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method
