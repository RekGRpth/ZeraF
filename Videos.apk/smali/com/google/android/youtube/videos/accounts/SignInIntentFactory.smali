.class public abstract Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;
.super Ljava/lang/Object;
.source "SignInIntentFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$1;,
        Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V8;,
        Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V14;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/google/android/youtube/videos/accounts/SignInIntentFactory;
    .locals 3

    const/4 v2, 0x0

    sget v0, Lcom/google/android/youtube/core/utils/Util;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V14;

    invoke-direct {v0, v2}, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V14;-><init>(Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$1;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V8;

    invoke-direct {v0, v2}, Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$V8;-><init>(Lcom/google/android/youtube/videos/accounts/SignInIntentFactory$1;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract newChooseAccountIntent(Landroid/content/Context;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/content/Intent;
.end method
