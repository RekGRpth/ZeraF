.class public final Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/TagProtos$Tag;",
        "Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$2600()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;-><init>(Lcom/google/android/youtube/videos/proto/TagProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->build()Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasSplitId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getSplitId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->setSplitId(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasOffCameraShape()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getOffCameraShape()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->setOffCameraShape(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasCircle()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getCircle()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->setCircle(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasFaceRectShape()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->getFaceRectShape()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->setFaceRectShape(J)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->setSplitId(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->setOffCameraShape(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->setFaceRectShape(J)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->setCircle(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setCircle(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasCircle:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->access$3202(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->circle_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->access$3302(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;I)I

    return-object p0
.end method

.method public setFaceRectShape(J)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasFaceRectShape:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->access$3402(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->faceRectShape_:J
    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->access$3502(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;J)J

    return-object p0
.end method

.method public setOffCameraShape(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasOffCameraShape:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->access$3002(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->offCameraShape_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->access$3102(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;I)I

    return-object p0
.end method

.method public setSplitId(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->hasSplitId:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->access$2802(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->splitId_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;->access$2902(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;I)I

    return-object p0
.end method
