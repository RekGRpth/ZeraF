.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Film"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;


# instance fields
.field private actor_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Person;",
            ">;"
        }
    .end annotation
.end field

.field private album_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Album;",
            ">;"
        }
    .end annotation
.end field

.field private director_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Person;",
            ">;"
        }
    .end annotation
.end field

.field private endDate_:Ljava/lang/String;

.field private googlePlayUrl_:Ljava/lang/String;

.field private hasEndDate:Z

.field private hasGooglePlayUrl:Z

.field private hasId:Z

.field private hasImage:Z

.field private hasIsTv:Z

.field private hasMid:Z

.field private hasReleaseDate:Z

.field private hasRuntimeMinutes:Z

.field private hasTitle:Z

.field private id_:Ljava/lang/String;

.field private image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

.field private isTv_:Z

.field private memoizedSerializedSize:I

.field private mid_:Ljava/lang/String;

.field private referencedFilm_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Film;",
            ">;"
        }
    .end annotation
.end field

.field private releaseDate_:Ljava/lang/String;

.field private runtimeMinutes_:I

.field private song_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Song;",
            ">;"
        }
    .end annotation
.end field

.field private title_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->mid_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->releaseDate_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->runtimeMinutes_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->googlePlayUrl_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->isTv_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->endDate_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->mid_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->releaseDate_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->runtimeMinutes_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->googlePlayUrl_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->isTv_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->endDate_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasId:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasTitle:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->title_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasReleaseDate:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->releaseDate_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasRuntimeMinutes:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->runtimeMinutes_:I

    return p1
.end method

.method static synthetic access$1802(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasImage:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object p1
.end method

.method static synthetic access$2002(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasGooglePlayUrl:Z

    return p1
.end method

.method static synthetic access$2102(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->googlePlayUrl_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2202(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasIsTv:Z

    return p1
.end method

.method static synthetic access$2302(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->isTv_:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasEndDate:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->endDate_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasMid:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->mid_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->access$100()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getActorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Person;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->actor_:Ljava/util/List;

    return-object v0
.end method

.method public getAlbumList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Album;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->album_:Ljava/util/List;

    return-object v0
.end method

.method public getDirectorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Person;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->director_:Ljava/util/List;

    return-object v0
.end method

.method public getEndDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->endDate_:Ljava/lang/String;

    return-object v0
.end method

.method public getGooglePlayUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->googlePlayUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object v0
.end method

.method public getIsTv()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->isTv_:Z

    return v0
.end method

.method public getMid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->mid_:Ljava/lang/String;

    return-object v0
.end method

.method public getReferencedFilmCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getReferencedFilmList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Film;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->referencedFilm_:Ljava/util/List;

    return-object v0
.end method

.method public getReleaseDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->releaseDate_:Ljava/lang/String;

    return-object v0
.end method

.method public getRuntimeMinutes()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->runtimeMinutes_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasMid()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getMid()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasId()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasTitle()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasReleaseDate()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReleaseDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasImage()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getActorList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v4, 0x6

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getDirectorList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v4, 0x7

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getSongList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/16 v4, 0x8

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_3

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasGooglePlayUrl()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getGooglePlayUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasRuntimeMinutes()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getRuntimeMinutes()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReferencedFilmList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/16 v4, 0xc

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_4

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasIsTv()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getIsTv()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasEndDate()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getEndDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getAlbumList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    const/16 v4, 0xf

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_5

    :cond_e
    iput v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->memoizedSerializedSize:I

    move v3, v2

    goto/16 :goto_0
.end method

.method public getSongList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Song;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->song_:Ljava/util/List;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasEndDate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasEndDate:Z

    return v0
.end method

.method public hasGooglePlayUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasGooglePlayUrl:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasId:Z

    return v0
.end method

.method public hasImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasImage:Z

    return v0
.end method

.method public hasIsTv()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasIsTv:Z

    return v0
.end method

.method public hasMid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasMid:Z

    return v0
.end method

.method public hasReleaseDate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasReleaseDate:Z

    return v0
.end method

.method public hasRuntimeMinutes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasRuntimeMinutes:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasTitle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasMid()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getMid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasReleaseDate()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReleaseDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getActorList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getDirectorList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getSongList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Song;

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasGooglePlayUrl()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getGooglePlayUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasRuntimeMinutes()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getRuntimeMinutes()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getReferencedFilmList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasIsTv()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getIsTv()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->hasEndDate()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getEndDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getAlbumList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_4

    :cond_d
    return-void
.end method
