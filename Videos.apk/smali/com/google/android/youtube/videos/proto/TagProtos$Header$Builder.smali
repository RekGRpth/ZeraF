.class public final Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/TagProtos$Header;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/TagProtos$Header;",
        "Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;)Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->buildParsed()Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;-><init>(Lcom/google/android/youtube/videos/proto/TagProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    return-object v0
.end method


# virtual methods
.method public addChunk(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$300(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$302(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$300(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->build()Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Header;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$300(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$300(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$302(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getFilm()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFilm()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v0

    return-object v0
.end method

.method public hasFilm()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFilm(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$700(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v0

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$700(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->newBuilder(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$702(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$602(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$702(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFps()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFps()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->setFps(F)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getFilm()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->mergeFilm(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    :cond_3
    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$300(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$300(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$302(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$300(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->chunk_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$300(Lcom/google/android/youtube/videos/proto/TagProtos$Header;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasEncrypted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->getEncrypted()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->setEncrypted(Z)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->setFps(F)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->hasFilm()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->getFilm()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Film$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->setFilm(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->newBuilder()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->addChunk(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->setEncrypted(Z)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setEncrypted(Z)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasEncrypted:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$802(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->encrypted_:Z
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$902(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Z)Z

    return-object p0
.end method

.method public setFilm(Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFilm:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$602(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->film_:Lcom/google/android/youtube/videos/proto/FilmProtos$Film;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$702(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Lcom/google/android/youtube/videos/proto/FilmProtos$Film;)Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    return-object p0
.end method

.method public setFps(F)Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;
    .locals 2
    .param p1    # F

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->hasFps:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$402(Lcom/google/android/youtube/videos/proto/TagProtos$Header;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Header$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Header;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Header;->fps_:F
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Header;->access$502(Lcom/google/android/youtube/videos/proto/TagProtos$Header;F)F

    return-object p0
.end method
