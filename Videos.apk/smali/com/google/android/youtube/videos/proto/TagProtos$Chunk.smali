.class public final Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/TagProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Chunk"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;


# instance fields
.field private byteOffset_:I

.field private hasByteOffset:Z

.field private hasStart:Z

.field private localIdMemoizedSerializedSize:I

.field private localId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private start_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->start_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->byteOffset_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localIdMemoizedSerializedSize:I

    iput v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/TagProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->start_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->byteOffset_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localIdMemoizedSerializedSize:I

    iput v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasStart:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->start_:I

    return p1
.end method

.method static synthetic access$1602(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasByteOffset:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->byteOffset_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->access$1100()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getByteOffset()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->byteOffset_:I

    return v0
.end method

.method public getLocalIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    iget v3, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->memoizedSerializedSize:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasStart()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getStart()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasByteOffset()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getByteOffset()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getLocalIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_1

    :cond_3
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getLocalIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    add-int/lit8 v3, v3, 0x1

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v3, v5

    :cond_4
    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localIdMemoizedSerializedSize:I

    iput v3, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->memoizedSerializedSize:I

    move v4, v3

    goto :goto_0
.end method

.method public getStart()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->start_:I

    return v0
.end method

.method public hasByteOffset()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasByteOffset:Z

    return v0
.end method

.method public hasStart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasStart:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasStart()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getStart()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasByteOffset()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getByteOffset()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getLocalIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    const/16 v2, 0x1a

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    iget v2, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localIdMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getLocalIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32NoTag(I)V

    goto :goto_0

    :cond_3
    return-void
.end method
