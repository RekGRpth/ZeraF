.class public final Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;",
        "Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1100()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;-><init>(Lcom/google/android/youtube/videos/proto/TagProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    return-object v0
.end method


# virtual methods
.method public addLocalId(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1300(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1302(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1300(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->build()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1300(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1300(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1302(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->clone()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getStart()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->setStart(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasByteOffset()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->getByteOffset()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->setByteOffset(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    :cond_3
    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1300(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1300(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1302(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1300(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->localId_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1300(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->setStart(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->setByteOffset(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->addLocalId(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v1

    :goto_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->addLocalId(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setByteOffset(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasByteOffset:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1602(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->byteOffset_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1702(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;I)I

    return-object p0
.end method

.method public setStart(I)Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->hasStart:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1402(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk$Builder;->result:Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;

    # setter for: Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->start_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;->access$1502(Lcom/google/android/youtube/videos/proto/TagProtos$Chunk;I)I

    return-object p0
.end method
