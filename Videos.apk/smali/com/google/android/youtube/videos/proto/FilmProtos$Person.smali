.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Person"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;


# instance fields
.field private appearance_:Lcom/google/protobuf/ByteString;

.field private character_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Character;",
            ">;"
        }
    .end annotation
.end field

.field private dateOfBirth_:Ljava/lang/String;

.field private dateOfDeath_:Ljava/lang/String;

.field private filmographyIndexMemoizedSerializedSize:I

.field private filmographyIndex_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private filmography_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Film;",
            ">;"
        }
    .end annotation
.end field

.field private hasAppearance:Z

.field private hasDateOfBirth:Z

.field private hasDateOfDeath:Z

.field private hasId:Z

.field private hasImage:Z

.field private hasLocalId:Z

.field private hasMid:Z

.field private hasName:Z

.field private hasPlaceOfBirth:Z

.field private id_:Ljava/lang/String;

.field private image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

.field private localId_:I

.field private memoizedSerializedSize:I

.field private mid_:Ljava/lang/String;

.field private name_:Ljava/lang/String;

.field private placeOfBirth_:Ljava/lang/String;

.field private splitId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->mid_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->name_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfBirth_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfDeath_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->placeOfBirth_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->localId_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndexMemoizedSerializedSize:I

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->appearance_:Lcom/google/protobuf/ByteString;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->mid_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->name_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfBirth_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfDeath_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->placeOfBirth_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->localId_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndexMemoizedSerializedSize:I

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->appearance_:Lcom/google/protobuf/ByteString;

    iput v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$3900(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4102(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4200(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4302(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasMid:Z

    return p1
.end method

.method static synthetic access$4402(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->mid_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4502(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasId:Z

    return p1
.end method

.method static synthetic access$4602(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4702(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasName:Z

    return p1
.end method

.method static synthetic access$4802(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4902(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage:Z

    return p1
.end method

.method static synthetic access$5000(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object p1
.end method

.method static synthetic access$5102(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfBirth:Z

    return p1
.end method

.method static synthetic access$5202(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfBirth_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5302(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfDeath:Z

    return p1
.end method

.method static synthetic access$5402(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfDeath_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5502(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasPlaceOfBirth:Z

    return p1
.end method

.method static synthetic access$5602(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->placeOfBirth_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5702(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasLocalId:Z

    return p1
.end method

.method static synthetic access$5802(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->localId_:I

    return p1
.end method

.method static synthetic access$5902(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasAppearance:Z

    return p1
.end method

.method static synthetic access$6002(Lcom/google/android/youtube/videos/proto/FilmProtos$Person;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .param p1    # Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->appearance_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Person;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Person;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;->access$3700()Lcom/google/android/youtube/videos/proto/FilmProtos$Person$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAppearance()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->appearance_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getCharacter(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    return-object v0
.end method

.method public getCharacterCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCharacterList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Character;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->character_:Ljava/util/List;

    return-object v0
.end method

.method public getDateOfBirth()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfBirth_:Ljava/lang/String;

    return-object v0
.end method

.method public getDateOfDeath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->dateOfDeath_:Ljava/lang/String;

    return-object v0
.end method

.method public getFilmographyIndex(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getFilmographyIndexCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilmographyIndexList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndex_:Ljava/util/List;

    return-object v0
.end method

.method public getFilmographyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Film;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmography_:Ljava/util/List;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object v0
.end method

.method public getLocalId()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->localId_:I

    return v0
.end method

.method public getMid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->mid_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaceOfBirth()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->placeOfBirth_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    iget v3, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->memoizedSerializedSize:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasMid()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getMid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasId()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasName()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getCharacterList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    const/4 v5, 0x5

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getSplitIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_2

    :cond_6
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getSplitIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v5, 0x7

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfBirth()Z

    move-result v5

    if-eqz v5, :cond_8

    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getDateOfBirth()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfDeath()Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v5, 0x9

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getDateOfDeath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasPlaceOfBirth()Z

    move-result v5

    if-eqz v5, :cond_a

    const/16 v5, 0xa

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getPlaceOfBirth()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasLocalId()Z

    move-result v5

    if-eqz v5, :cond_b

    const/16 v5, 0xb

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getLocalId()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyIndexList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_4

    :cond_c
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyIndexList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_d

    add-int/lit8 v3, v3, 0x1

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v3, v5

    :cond_d
    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndexMemoizedSerializedSize:I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasAppearance()Z

    move-result v5

    if-eqz v5, :cond_e

    const/16 v5, 0xd

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getAppearance()Lcom/google/protobuf/ByteString;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_e
    iput v3, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->memoizedSerializedSize:I

    move v4, v3

    goto/16 :goto_0
.end method

.method public getSplitId(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSplitIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSplitIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->splitId_:Ljava/util/List;

    return-object v0
.end method

.method public hasAppearance()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasAppearance:Z

    return v0
.end method

.method public hasDateOfBirth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfBirth:Z

    return v0
.end method

.method public hasDateOfDeath()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfDeath:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasId:Z

    return v0
.end method

.method public hasImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage:Z

    return v0
.end method

.method public hasLocalId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasLocalId:Z

    return v0
.end method

.method public hasMid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasMid:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasName:Z

    return v0
.end method

.method public hasPlaceOfBirth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasPlaceOfBirth:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasMid()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getMid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasName()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getCharacterList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getSplitIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Film;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfBirth()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getDateOfBirth()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasDateOfDeath()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getDateOfDeath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasPlaceOfBirth()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getPlaceOfBirth()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasLocalId()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getLocalId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyIndexList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_b

    const/16 v2, 0x62

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    iget v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->filmographyIndexMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getFilmographyIndexList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32NoTag(I)V

    goto :goto_3

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->hasAppearance()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Person;->getAppearance()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_d
    return-void
.end method
