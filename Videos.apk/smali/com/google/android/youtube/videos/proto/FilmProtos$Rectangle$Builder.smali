.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$7100()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;-><init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->build()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasLeft()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getLeft()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->setLeft(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasRight()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getRight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->setRight(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasTop()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getTop()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->setTop(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasBottom()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getBottom()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->setBottom(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->setLeft(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->setRight(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->setTop(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->setBottom(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setBottom(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasBottom:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->access$7902(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->bottom_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->access$8002(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;I)I

    return-object p0
.end method

.method public setLeft(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasLeft:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->access$7302(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->left_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->access$7402(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;I)I

    return-object p0
.end method

.method public setRight(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasRight:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->access$7502(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->right_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->access$7602(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;I)I

    return-object p0
.end method

.method public setTop(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasTop:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->access$7702(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->top_:I
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->access$7802(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;I)I

    return-object p0
.end method
