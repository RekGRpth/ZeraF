.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Character"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;


# instance fields
.field private hasImage:Z

.field private hasMid:Z

.field private hasName:Z

.field private image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

.field private memoizedSerializedSize:I

.field private mid_:Ljava/lang/String;

.field private name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->mid_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->name_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->mid_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->name_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$6402(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasMid:Z

    return p1
.end method

.method static synthetic access$6502(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->mid_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6602(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasName:Z

    return p1
.end method

.method static synthetic access$6702(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6802(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasImage:Z

    return p1
.end method

.method static synthetic access$6900(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object v0
.end method

.method static synthetic access$6902(Lcom/google/android/youtube/videos/proto/FilmProtos$Character;Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Character;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Character;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;->access$6200()Lcom/google/android/youtube/videos/proto/FilmProtos$Character$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->image_:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object v0
.end method

.method public getMid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->mid_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasMid()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getMid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasName()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasImage:Z

    return v0
.end method

.method public hasMid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasMid:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getMid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasName()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Character;->getImage()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    return-void
.end method
