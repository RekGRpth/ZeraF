.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;


# instance fields
.field private cropRegion_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;",
            ">;"
        }
    .end annotation
.end field

.field private data_:Lcom/google/protobuf/ByteString;

.field private hasData:Z

.field private hasMimeType:Z

.field private hasReferrerUrl:Z

.field private hasUrl:Z

.field private memoizedSerializedSize:I

.field private mimeType_:Ljava/lang/String;

.field private referrerUrl_:Ljava/lang/String;

.field private url_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->mimeType_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->url_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->referrerUrl_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->data_:Lcom/google/protobuf/ByteString;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->mimeType_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->url_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->referrerUrl_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->data_:Lcom/google/protobuf/ByteString;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$8400(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$8402(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$8502(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasMimeType:Z

    return p1
.end method

.method static synthetic access$8602(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->mimeType_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$8702(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasUrl:Z

    return p1
.end method

.method static synthetic access$8802(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->url_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$8902(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasReferrerUrl:Z

    return p1
.end method

.method static synthetic access$9002(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->referrerUrl_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$9102(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasData:Z

    return p1
.end method

.method static synthetic access$9202(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .param p1    # Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->data_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Image;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->access$8200()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Image;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Image;)Lcom/google/android/youtube/videos/proto/FilmProtos$Image$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCropRegion(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    return-object v0
.end method

.method public getCropRegionCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCropRegionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->cropRegion_:Ljava/util/List;

    return-object v0
.end method

.method public getData()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->data_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->mimeType_:Ljava/lang/String;

    return-object v0
.end method

.method public getReferrerUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->referrerUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasMimeType()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getMimeType()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasUrl()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasReferrerUrl()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getReferrerUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasData()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getData()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getCropRegionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    const/4 v4, 0x6

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_5
    iput v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public hasData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasData:Z

    return v0
.end method

.method public hasMimeType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasMimeType:Z

    return v0
.end method

.method public hasReferrerUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasReferrerUrl:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasUrl:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasMimeType()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasReferrerUrl()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getReferrerUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->hasData()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getData()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Image;->getCropRegionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_4
    return-void
.end method
