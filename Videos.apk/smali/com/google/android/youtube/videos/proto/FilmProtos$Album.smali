.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Album;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Album"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;


# instance fields
.field private hasMid:Z

.field private memoizedSerializedSize:I

.field private metajamId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mid_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->mid_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->mid_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Album;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3002(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Album;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->hasMid:Z

    return p1
.end method

.method static synthetic access$3102(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Album;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->mid_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Album;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->access$2700()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getMetajamIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;

    return-object v0
.end method

.method public getMid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->mid_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    iget v3, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->memoizedSerializedSize:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->hasMid()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->getMid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->getMetajamIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_1

    :cond_2
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->getMetajamIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    iput v3, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->memoizedSerializedSize:I

    move v4, v3

    goto :goto_0
.end method

.method public hasMid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->hasMid:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->hasMid()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->getMid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->getMetajamIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method
