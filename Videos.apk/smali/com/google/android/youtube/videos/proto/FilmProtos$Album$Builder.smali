.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos$Album;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Album;",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$2700()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;-><init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    return-object v0
.end method


# virtual methods
.method public addMetajamId(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2900(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2902(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2900(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/proto/FilmProtos$Album;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->build()Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Album;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2900(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2900(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2902(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->getMid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    :cond_2
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2900(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2900(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2902(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2900(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->metajamId_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$2900(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->addMetajamId(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setMid(Ljava/lang/String;)Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->hasMid:Z
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$3002(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Album$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Album;

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->mid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Album;->access$3102(Lcom/google/android/youtube/videos/proto/FilmProtos$Album;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
