.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Rectangle"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;


# instance fields
.field private bottom_:I

.field private hasBottom:Z

.field private hasLeft:Z

.field private hasRight:Z

.field private hasTop:Z

.field private left_:I

.field private memoizedSerializedSize:I

.field private right_:I

.field private top_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->left_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->right_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->top_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->bottom_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->left_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->right_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->top_:I

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->bottom_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$7302(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasLeft:Z

    return p1
.end method

.method static synthetic access$7402(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->left_:I

    return p1
.end method

.method static synthetic access$7502(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasRight:Z

    return p1
.end method

.method static synthetic access$7602(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->right_:I

    return p1
.end method

.method static synthetic access$7702(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasTop:Z

    return p1
.end method

.method static synthetic access$7802(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->top_:I

    return p1
.end method

.method static synthetic access$7902(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasBottom:Z

    return p1
.end method

.method static synthetic access$8002(Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->bottom_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->defaultInstance:Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;->access$7100()Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBottom()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->bottom_:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->left_:I

    return v0
.end method

.method public getRight()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->right_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasLeft()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getLeft()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasRight()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getRight()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasTop()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getTop()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasBottom()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getBottom()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iput v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getTop()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->top_:I

    return v0
.end method

.method public hasBottom()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasBottom:Z

    return v0
.end method

.method public hasLeft()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasLeft:Z

    return v0
.end method

.method public hasRight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasRight:Z

    return v0
.end method

.method public hasTop()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasTop:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasLeft()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getLeft()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasRight()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getRight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasTop()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getTop()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->hasBottom()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Rectangle;->getBottom()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_3
    return-void
.end method
