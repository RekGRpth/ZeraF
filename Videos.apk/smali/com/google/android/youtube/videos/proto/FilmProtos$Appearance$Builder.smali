.class public final Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;",
        "Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->buildParsed()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3300()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;-><init>()V

    new-instance v1, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;-><init>(Lcom/google/android/youtube/videos/proto/FilmProtos$1;)V

    iput-object v1, v0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    return-object v0
.end method


# virtual methods
.method public addOffset(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3500(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3502(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3500(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->build()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3500(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    iget-object v2, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3500(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3502(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    return-object v0
.end method

.method public clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->create()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->clone()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->getDefaultInstance()Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3500(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3500(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3502(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->result:Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3500(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->offset_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;->access$3500(Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->addOffset(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v1

    :goto_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->addOffset(I)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/youtube/videos/proto/FilmProtos$Appearance$Builder;

    move-result-object v0

    return-object v0
.end method
