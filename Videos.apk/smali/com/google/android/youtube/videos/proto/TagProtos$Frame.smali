.class public final Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/proto/TagProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Frame"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;


# instance fields
.field private hasOffset:Z

.field private memoizedSerializedSize:I

.field private offset_:I

.field private tagIn_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/TagProtos$Tag;",
            ">;"
        }
    .end annotation
.end field

.field private tagOut_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/TagProtos$Tag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;-><init>(Z)V

    sput-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos;->internalForceInit()V

    sget-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->offset_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/proto/TagProtos$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->offset_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->hasOffset:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;I)I
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->offset_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->defaultInstance:Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    .locals 1

    # invokes: Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->create()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;
    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->access$1900()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .locals 2
    .param p0    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->newBuilder()Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->buildParsed()Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    invoke-static {v0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;->access$1800(Lcom/google/android/youtube/videos/proto/TagProtos$Frame$Builder;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getOffset()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->offset_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->hasOffset()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getOffset()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getTagInList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getTagOutList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_2

    :cond_3
    iput v2, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public getTagIn(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    return-object v0
.end method

.method public getTagInCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTagInList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/TagProtos$Tag;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagIn_:Ljava/util/List;

    return-object v0
.end method

.method public getTagOut(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    return-object v0
.end method

.method public getTagOutCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTagOutList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/proto/TagProtos$Tag;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->tagOut_:Ljava/util/List;

    return-object v0
.end method

.method public hasOffset()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->hasOffset:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->hasOffset()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getOffset()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getTagInList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getTagOutList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    :cond_2
    return-void
.end method
