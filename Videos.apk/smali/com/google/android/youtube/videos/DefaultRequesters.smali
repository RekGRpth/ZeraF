.class public final Lcom/google/android/youtube/videos/DefaultRequesters;
.super Ljava/lang/Object;
.source "DefaultRequesters.java"

# interfaces
.implements Lcom/google/android/youtube/videos/Requesters;


# instance fields
.field private final asyncVideoRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation
.end field

.field private final bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

.field private final bitmapRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final config:Lcom/google/android/youtube/videos/Config;

.field private final context:Landroid/content/Context;

.field private final executor:Ljava/util/concurrent/Executor;

.field private final gdataRequestGetConverter:Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

.field private final httpClient:Lorg/apache/http/client/HttpClient;

.field private final inMemoryShowPageCache:Lcom/google/android/youtube/core/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/cache/Cache",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final inMemoryVideoPageCache:Lcom/google/android/youtube/core/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/cache/Cache",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final mostPopularVideosRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation
.end field

.field private final permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation
.end field

.field private final persistentCachePath:Ljava/lang/String;

.field private final persistentShowPageCache:Lcom/google/android/youtube/core/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/cache/Cache",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final persistentVideoPageCache:Lcom/google/android/youtube/core/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/cache/Cache",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final posterArtRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final robotTokenRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final showsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;>;"
        }
    .end annotation
.end field

.field private final streamExtraRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;"
        }
    .end annotation
.end field

.field private final syncBitmapBytesCacheRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation
.end field

.field private final syncBitmapBytesRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation
.end field

.field private final syncEpisodesRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation
.end field

.field private final syncMyPurchasesRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Purchase;",
            ">;>;"
        }
    .end annotation
.end field

.field private final syncSeasonRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Season;",
            ">;"
        }
    .end annotation
.end field

.field private final syncShowRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;"
        }
    .end annotation
.end field

.field private final syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;"
        }
    .end annotation
.end field

.field private final syncVideoRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation
.end field

.field private final uriRequestGetConverter:Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

.field private final uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

.field private final useHqThumbnails:Z

.field private final videosHttpRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation
.end field

.field private final videosRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation
.end field

.field private final xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/XmlParser;Lcom/google/android/youtube/videos/Config;Lcom/google/android/youtube/videos/utils/BitmapLruCache;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/core/async/DeviceAuthorizer;
    .param p3    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p4    # Ljava/util/concurrent/Executor;
    .param p5    # Ljava/lang/String;
    .param p6    # Lorg/apache/http/client/HttpClient;
    .param p7    # Lcom/google/android/youtube/core/converter/XmlParser;
    .param p8    # Lcom/google/android/youtube/videos/Config;
    .param p9    # Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    .param p10    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v4, "context can\'t be null"

    invoke-static {p1, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->context:Landroid/content/Context;

    const-string v4, "executor can\'t be null"

    invoke-static {p4, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->executor:Ljava/util/concurrent/Executor;

    const-string v4, "httpClient can\'t be null"

    invoke-static {p6, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/http/client/HttpClient;

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->httpClient:Lorg/apache/http/client/HttpClient;

    const-string v4, "parser can\'t be null"

    invoke-static {p7, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/converter/XmlParser;

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    const-string v4, "config can\'t be null"

    move-object/from16 v0, p8

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/Config;

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->config:Lcom/google/android/youtube/videos/Config;

    const-string v4, "persistentCachePath cannot be empty"

    invoke-static {p5, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->persistentCachePath:Ljava/lang/String;

    new-instance v4, Lcom/google/android/youtube/core/utils/UriRewriter;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/youtube/core/utils/UriRewriter;-><init>(Landroid/content/ContentResolver;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    const-string v4, "bitmapCache can\'t be null"

    move-object/from16 v0, p9

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->useHqThumbnails:Z

    const-string v4, "deviceAuthorizer can\'t be null"

    invoke-static {p2, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "accountManagerWrapper can\'t be null"

    invoke-static {p3, v4}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

    sget-object v5, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v4, v5}, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->uriRequestGetConverter:Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

    new-instance v4, Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

    sget-object v5, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    iget-object v6, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->uriRewriter:Lcom/google/android/youtube/core/utils/UriRewriter;

    invoke-interface/range {p8 .. p8}, Lcom/google/android/youtube/videos/Config;->gdataVersion()Lcom/google/android/youtube/core/async/GDataRequest$Version;

    move-result-object v7

    invoke-direct {v4, v5, p2, v6, v7}, Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/utils/UriRewriter;Lcom/google/android/youtube/core/async/GDataRequest$Version;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->gdataRequestGetConverter:Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->newPersistentCache()Lcom/google/android/youtube/core/cache/PersistentCache;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/DefaultRequesters;->createSyncBitmapBytesRequester(Lcom/google/android/youtube/core/cache/PersistentCache;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncBitmapBytesRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/DefaultRequesters;->createSyncBitmapBytesCacheRequester(Lcom/google/android/youtube/core/cache/PersistentCache;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncBitmapBytesCacheRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createPosterArtRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->posterArtRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createBitmapRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->bitmapRequester:Lcom/google/android/youtube/core/async/Requester;

    const/16 v4, 0x32

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newInMemoryCache(I)Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->inMemoryVideoPageCache:Lcom/google/android/youtube/core/cache/Cache;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->newPersistentCache()Lcom/google/android/youtube/core/cache/PersistentCache;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/core/cache/AsyncCache;

    invoke-direct {v4, p4, v3}, Lcom/google/android/youtube/core/cache/AsyncCache;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/cache/Cache;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->persistentVideoPageCache:Lcom/google/android/youtube/core/cache/Cache;

    const/16 v4, 0x32

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newInMemoryCache(I)Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->inMemoryShowPageCache:Lcom/google/android/youtube/core/cache/Cache;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->newPersistentCache()Lcom/google/android/youtube/core/cache/PersistentCache;

    move-result-object v2

    new-instance v4, Lcom/google/android/youtube/core/cache/AsyncCache;

    invoke-direct {v4, p4, v2}, Lcom/google/android/youtube/core/cache/AsyncCache;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/cache/Cache;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->persistentShowPageCache:Lcom/google/android/youtube/core/cache/Cache;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createVideosHttpRequester()Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->videosHttpRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createVideosRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->videosRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createMostPopularVideosRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->mostPopularVideosRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createSyncVideoRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncVideoRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncVideoRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->asyncVideoRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createShowsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->showsRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createSyncShowRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncShowRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createSyncSeasonRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncSeasonRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createSyncEpisodesRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncEpisodesRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, p3}, Lcom/google/android/youtube/videos/DefaultRequesters;->createSyncMyPurchasesRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncMyPurchasesRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createPermittedStreamsRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/DefaultRequesters;->createSyncStreamExtraRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->streamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, p3}, Lcom/google/android/youtube/videos/DefaultRequesters;->createRobotTokenRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->robotTokenRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method private createBitmapRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;-><init>(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncBitmapBytesRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/DefaultRequesters;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    new-instance v3, Lcom/google/android/youtube/videos/DefaultRequesters$2;

    iget-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    invoke-direct {v3, p0, v0, v4}, Lcom/google/android/youtube/videos/DefaultRequesters$2;-><init>(Lcom/google/android/youtube/videos/DefaultRequesters;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V

    return-object v3
.end method

.method private createMostPopularVideosRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->videosRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v1, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->videosRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/DefaultRequesters;->newFallbackRequester(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/FallbackRequester;

    move-result-object v0

    return-object v0
.end method

.method private createPermittedStreamsRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation

    new-instance v3, Lcom/google/android/youtube/videos/converter/ChannelFormatsConverter;

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v7, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v8, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v3, v6, v7, v8}, Lcom/google/android/youtube/videos/converter/ChannelFormatsConverter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->gdataRequestGetConverter:Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

    new-instance v7, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesListConverter;

    iget-object v8, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {v7, v8}, Lcom/google/android/youtube/core/converter/http/DevicePrivilegesListConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    invoke-direct {p0, v6, v7}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v4

    invoke-static {v4, v3, v3}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    const/16 v6, 0x14

    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/DefaultRequesters;->newInMemoryCache(I)Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    move-result-object v0

    const-wide/32 v6, 0x6ddd00

    invoke-direct {p0, v0, v1, v6, v7}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    new-instance v5, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;

    iget-object v6, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncVideoRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v7, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v7}, Lcom/google/android/youtube/videos/Config;->abrFormats()Ljava/util/List;

    move-result-object v7

    invoke-direct {v5, v6, v2, v7}, Lcom/google/android/youtube/videos/async/PermittedStreamsRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Ljava/util/Collection;)V

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/DefaultRequesters;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v6

    return-object v6
.end method

.method private createPosterArtRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;

    iget-object v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/youtube/core/converter/http/BytesToBitmapResponseConverter;-><init>(IZZ)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncBitmapBytesRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/DefaultRequesters;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    new-instance v3, Lcom/google/android/youtube/videos/DefaultRequesters$1;

    iget-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->bitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    invoke-direct {v3, p0, v0, v4}, Lcom/google/android/youtube/videos/DefaultRequesters$1;-><init>(Lcom/google/android/youtube/videos/DefaultRequesters;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V

    return-object v3
.end method

.method private createRobotTokenRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;)Lcom/google/android/youtube/core/async/Requester;
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;

    sget-object v3, Lcom/google/android/youtube/core/converter/http/HttpMethod;->POST:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    new-instance v3, Lcom/google/android/youtube/videos/converter/HttpToStringResponseConverter;

    invoke-direct {v3}, Lcom/google/android/youtube/videos/converter/HttpToStringResponseConverter;-><init>()V

    invoke-direct {p0, v2, v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequestConverter;

    iget-object v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->atHomeRobotTokenRequestUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequestConverter;-><init>(Landroid/net/Uri;)V

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->RETRY_STRATEGY:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    invoke-static {v1, p1, v2}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;)Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    move-result-object v2

    return-object v2
.end method

.method private createShowsRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;>;"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->gdataRequestGetConverter:Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

    new-instance v5, Lcom/google/android/youtube/core/converter/http/ShowPageResponseConverter;

    iget-object v6, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {v5, v6}, Lcom/google/android/youtube/core/converter/http/ShowPageResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->persistentShowPageCache:Lcom/google/android/youtube/core/cache/Cache;

    const-wide/32 v5, 0xa4cb800

    invoke-direct {p0, v4, v3, v5, v6}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/DefaultRequesters;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->inMemoryShowPageCache:Lcom/google/android/youtube/core/cache/Cache;

    const-wide/32 v5, 0x6ddd00

    invoke-direct {p0, v4, v0, v5, v6}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createSyncBitmapBytesCacheRequester(Lcom/google/android/youtube/core/cache/PersistentCache;)Lcom/google/android/youtube/core/async/Requester;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/cache/PersistentCache",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<[B>;>;)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation

    const-wide/32 v3, 0x48190800

    invoke-static {p1, v3, v4}, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->create(Lcom/google/android/youtube/core/cache/Cache;J)Lcom/google/android/youtube/core/async/TimestampedCachingRequester;

    move-result-object v1

    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/DefaultRequesters;->newInMemoryCache(I)Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    move-result-object v0

    invoke-direct {p0, v0, v1, v3, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    return-object v2
.end method

.method private createSyncBitmapBytesRequester(Lcom/google/android/youtube/core/cache/PersistentCache;)Lcom/google/android/youtube/core/async/Requester;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/cache/PersistentCache",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<[B>;>;)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/converter/http/HttpToBytesResponseConverter;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/http/HttpToBytesResponseConverter;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->uriRequestGetConverter:Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

    invoke-direct {p0, v2, v0}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v1

    const-wide/32 v2, 0x48190800

    invoke-direct {p0, p1, v1, v2, v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    return-object v2
.end method

.method private createSyncEpisodesRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->inMemoryVideoPageCache:Lcom/google/android/youtube/core/cache/Cache;

    iget-object v2, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->videosHttpRequester:Lcom/google/android/youtube/core/async/Requester;

    const-wide/32 v3, 0x6ddd00

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    return-object v0
.end method

.method private createSyncMyPurchasesRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;)Lcom/google/android/youtube/core/async/Requester;
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Purchase;",
            ">;>;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/core/converter/http/PurchasePageResponseConverter;

    iget-object v2, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->useHqThumbnails:Z

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/core/converter/http/PurchasePageResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->gdataRequestGetConverter:Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

    invoke-direct {p0, v2, v1}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v0

    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequest;->RETRY_STRATEGY:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    invoke-static {v0, p1, v2}, Lcom/google/android/youtube/core/async/AuthenticatedRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;)Lcom/google/android/youtube/core/async/AuthenticatedRequester;

    move-result-object v2

    return-object v2
.end method

.method private createSyncSeasonRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Season;",
            ">;"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->gdataRequestGetConverter:Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

    new-instance v4, Lcom/google/android/youtube/core/converter/http/SeasonResponseConverter;

    iget-object v5, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {v4, v5}, Lcom/google/android/youtube/core/converter/http/SeasonResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    invoke-direct {p0, v3, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v1

    new-instance v3, Lcom/google/android/youtube/videos/DefaultRequesters$5;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/videos/DefaultRequesters$5;-><init>(Lcom/google/android/youtube/videos/DefaultRequesters;)V

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    const/16 v3, 0x32

    invoke-direct {p0, v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->newInMemoryCache(I)Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    move-result-object v0

    const-wide/32 v3, 0x6ddd00

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    return-object v3
.end method

.method private createSyncShowRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->gdataRequestGetConverter:Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

    new-instance v4, Lcom/google/android/youtube/core/converter/http/ShowResponseConverter;

    iget-object v5, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    invoke-direct {v4, v5}, Lcom/google/android/youtube/core/converter/http/ShowResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;)V

    invoke-direct {p0, v3, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v1

    new-instance v3, Lcom/google/android/youtube/videos/DefaultRequesters$4;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/videos/DefaultRequesters$4;-><init>(Lcom/google/android/youtube/videos/DefaultRequesters;)V

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    const/16 v3, 0x32

    invoke-direct {p0, v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->newInMemoryCache(I)Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    move-result-object v0

    const-wide/32 v3, 0x6ddd00

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    return-object v3
.end method

.method private createSyncStreamExtraRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/converter/http/StreamExtraGetConverter;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/http/StreamExtraGetConverter;-><init>()V

    invoke-direct {p0, v0, v0}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v2

    const/16 v3, 0x14

    invoke-direct {p0, v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->newInMemoryCache(I)Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    move-result-object v1

    const-wide/32 v3, 0x6ddd00

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    return-object v3
.end method

.method private createSyncVideoRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->gdataRequestGetConverter:Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

    new-instance v4, Lcom/google/android/youtube/core/converter/http/VideoResponseConverter;

    iget-object v5, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    iget-boolean v6, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->useHqThumbnails:Z

    invoke-direct {v4, v5, v6}, Lcom/google/android/youtube/core/converter/http/VideoResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;Z)V

    invoke-direct {p0, v3, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v1

    new-instance v3, Lcom/google/android/youtube/videos/DefaultRequesters$3;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/videos/DefaultRequesters$3;-><init>(Lcom/google/android/youtube/videos/DefaultRequesters;)V

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    const/16 v3, 0x14

    invoke-direct {p0, v3}, Lcom/google/android/youtube/videos/DefaultRequesters;->newInMemoryCache(I)Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    move-result-object v0

    const-wide/32 v3, 0x6ddd00

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    return-object v3
.end method

.method private createVideosHttpRequester()Lcom/google/android/youtube/core/async/HttpRequester;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/HttpRequester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->gdataRequestGetConverter:Lcom/google/android/youtube/core/converter/http/GDataRequestConverter;

    new-instance v1, Lcom/google/android/youtube/core/converter/http/VideoPageResponseConverter;

    iget-object v2, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->xmlParser:Lcom/google/android/youtube/core/converter/XmlParser;

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->useHqThumbnails:Z

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/core/converter/http/VideoPageResponseConverter;-><init>(Lcom/google/android/youtube/core/converter/XmlParser;Z)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/DefaultRequesters;->newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v0

    return-object v0
.end method

.method private createVideosRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->persistentVideoPageCache:Lcom/google/android/youtube/core/cache/Cache;

    iget-object v4, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->videosHttpRequester:Lcom/google/android/youtube/core/async/Requester;

    const-wide/32 v5, 0xa4cb800

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/DefaultRequesters;->newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->inMemoryVideoPageCache:Lcom/google/android/youtube/core/cache/Cache;

    const-wide/32 v4, 0x6ddd00

    invoke-direct {p0, v3, v0, v4, v5}, Lcom/google/android/youtube/videos/DefaultRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private newAsyncRequester(Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/core/async/AsyncRequester",
            "<TR;TE;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->executor:Ljava/util/concurrent/Executor;

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    return-object v0
.end method

.method private newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/cache/Cache",
            "<TR;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<TE;>;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;J)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    invoke-static {p1, p2, p3, p4}, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->create(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/TimestampedCachingRequester;

    move-result-object v0

    return-object v0
.end method

.method private newFallbackRequester(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/FallbackRequester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/core/async/FallbackRequester",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/async/FallbackRequester;

    invoke-direct {v0, p1, p2}, Lcom/google/android/youtube/core/async/FallbackRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)V

    return-object v0
.end method

.method private newHttpRequester(Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
            "<TE;>;)",
            "Lcom/google/android/youtube/core/async/HttpRequester",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/async/HttpRequester;

    iget-object v1, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/youtube/core/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)V

    return-object v0
.end method

.method private newInMemoryCache(I)Lcom/google/android/youtube/core/cache/InMemoryLruCache;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lcom/google/android/youtube/core/cache/InMemoryLruCache",
            "<TK;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/cache/InMemoryLruCache;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/cache/InMemoryLruCache;-><init>(I)V

    return-object v0
.end method

.method private newPersistentCache()Lcom/google/android/youtube/core/cache/PersistentCache;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "E::",
            "Ljava/io/Serializable;",
            ">()",
            "Lcom/google/android/youtube/core/cache/PersistentCache",
            "<TK;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;

    iget-object v1, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->persistentCachePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->executor:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/cache/SerializablesPersistentCache;->init(Ljava/util/concurrent/Executor;)Lcom/google/android/youtube/core/cache/PersistentCache;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBitmapRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->bitmapRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getMostPopularMoviesRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->mostPopularVideosRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getMostPopularShowsRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->showsRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getPermittedStreamsRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/async/StreamsRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/youtube/core/model/Stream;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->permittedStreamsRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getPosterArtRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->posterArtRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getRobotTokenRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->robotTokenRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getStreamExtraRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->streamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getSuggestedMoviesRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->videosRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getSyncBitmapBytesCacheRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncBitmapBytesCacheRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getSyncBitmapBytesRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncBitmapBytesRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getSyncEpisodesRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncEpisodesRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getSyncMyPurchasesRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/core/async/GDataRequest;",
            "Lcom/google/android/youtube/core/model/Page",
            "<",
            "Lcom/google/android/youtube/core/model/Purchase;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncMyPurchasesRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getSyncSeasonRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Season;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncSeasonRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getSyncShowRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Show;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncShowRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getSyncStreamExtraRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/youtube/core/model/StreamExtra;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncStreamExtraRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getSyncVideoRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->syncVideoRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getVideoRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/DefaultRequesters;->asyncVideoRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method
