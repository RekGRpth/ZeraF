.class public Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
.super Ljava/lang/Object;
.source "AtHomeInstance.java"

# interfaces
.implements Landroid/support/place/rpc/RpcErrorHandler;
.implements Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetPlayerState;
.implements Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetRobotAccount;
.implements Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVideoInfo;
.implements Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVolume;
.implements Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$MoviesConnectorStateListener;,
        Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$BaseAtHomeConnectorListener;,
        Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;
    }
.end annotation


# instance fields
.field private connectionError:Z

.field private connectorInfo:Landroid/support/place/connector/ConnectorInfo;

.field private final context:Landroid/content/Context;

.field private isListening:Z

.field private isTracking:Z

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaRouteManager:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;

.field private moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

.field private final moviesConnectorStateListener:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

.field private negotiatedVersion:I

.field private playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

.field private robotAccount:Ljava/lang/String;

.field public screenName:Ljava/lang/String;

.field private videoInfo:Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

.field private volume:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->isListening:Z

    const-string v0, "context can not be null"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->context:Landroid/content/Context;

    const-string v0, "mediaRouteManager can not be null"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->mediaRouteManager:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$MoviesConnectorStateListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$MoviesConnectorStateListener;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$1;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnectorStateListener:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedPlayerState(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedVideoInfo(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;I)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedVolume(I)V

    return-void
.end method

.method public static accountIdMatchesAccount(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getAccountId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    return v2
.end method

.method private clamp(III)I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private createMoviesConnector(Landroid/support/place/connector/Broker;Landroid/support/place/connector/ConnectorInfo;)Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
    .locals 2
    .param p1    # Landroid/support/place/connector/Broker;
    .param p2    # Landroid/support/place/connector/ConnectorInfo;

    new-instance v0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-virtual {p2}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V

    return-object v0
.end method

.method public static getAccountId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v3

    :cond_0
    :try_start_0
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v4

    const/16 v5, 0xb

    invoke-static {v4, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "Error while trying to get SHA-1 digest algorithm"

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getExtra(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    const/16 v2, 0x7c

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v3, v1, 0x1

    add-int/2addr v3, v0

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private getVersionInfo(Landroid/support/place/connector/ConnectorInfo;)Lcom/google/android/youtube/athome/common/VersionInfo;
    .locals 3
    .param p1    # Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getExtras()Landroid/support/place/rpc/RpcData;

    move-result-object v0

    const-string v1, "versionInfo"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "versionInfo"

    sget-object v2, Lcom/google/android/youtube/athome/common/VersionInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/athome/common/VersionInfo;

    goto :goto_0
.end method

.method private invalidateMoviesConnectorCache()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->robotAccount:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedPlayerState(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedVideoInfo(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedVolume(I)V

    return-void
.end method

.method private maybeStartTrackingService()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->isTracking:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method

.method private notifyConnectorChanged(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
    .param p2    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    invoke-interface {v1, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;->onConnectorChanged(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private registerMoviesConnectorCacheUpdater()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnectorStateListener:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->startListening(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;)V

    :cond_0
    return-void
.end method

.method private unregisterMoviesConnectorCacheUpdater()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->stopListening()V

    :cond_0
    return-void
.end method

.method private updateCachedPlayerState(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;->onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->maybeStartTrackingService()V

    return-void
.end method

.method private updateCachedRobotAccount(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->robotAccount:Ljava/lang/String;

    return-void
.end method

.method private updateCachedVideoInfo(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->videoInfo:Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;->onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->maybeStartTrackingService()V

    return-void
.end method

.method private updateCachedVolume(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->volume:I

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;->onVolumeChanged(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateListener()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->isTracking:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->isListening:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->mediaRouteManager:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->register(Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->isListening:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->isListening:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->mediaRouteManager:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->unregister()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->isListening:Z

    goto :goto_0
.end method

.method private updateMoviesConnector(Landroid/support/place/connector/Broker;Landroid/support/place/connector/ConnectorInfo;)V
    .locals 2
    .param p1    # Landroid/support/place/connector/Broker;
    .param p2    # Landroid/support/place/connector/ConnectorInfo;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->connectorInfo:Landroid/support/place/connector/ConnectorInfo;

    if-ne v1, p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->unregisterMoviesConnectorCacheUpdater()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->connectorInfo:Landroid/support/place/connector/ConnectorInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    if-nez p2, :cond_1

    const/4 v1, 0x0

    :goto_1
    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->invalidateMoviesConnectorCache()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->notifyConnectorChanged(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->registerMoviesConnectorCacheUpdater()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateMoviesConnectorCache()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->createMoviesConnector(Landroid/support/place/connector/Broker;Landroid/support/place/connector/ConnectorInfo;)Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    move-result-object v1

    goto :goto_1
.end method

.method private updateMoviesConnectorCache()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->getRobotAccount(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetRobotAccount;Landroid/support/place/rpc/RpcErrorHandler;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->getPlayerState(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetPlayerState;Landroid/support/place/rpc/RpcErrorHandler;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->getVideoInfo(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVideoInfo;Landroid/support/place/rpc/RpcErrorHandler;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->getVolume(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVolume;Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public canControlPlayback()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConnectionError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->connectionError:Z

    return v0
.end method

.method public getMoviesConnector()Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    return-object v0
.end method

.method public getMoviesConnectorName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->screenName:Ljava/lang/String;

    return-object v0
.end method

.method public getMoviesConnectorVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->negotiatedVersion:I

    return v0
.end method

.method public getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    return-object v0
.end method

.method public getRobotAccount(Lcom/google/android/youtube/core/async/Callback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/Exception;

    const-string v1, "not connected"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v2, v0}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->robotAccount:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->robotAccount:Ljava/lang/String;

    invoke-interface {p1, v2, v0}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->moviesConnector:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    new-instance v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$1;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/core/async/Callback;)V

    new-instance v2, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$2;

    invoke-direct {v2, p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$2;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->getRobotAccount(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetRobotAccount;Landroid/support/place/rpc/RpcErrorHandler;)V

    goto :goto_0
.end method

.method public getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->videoInfo:Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    return-object v0
.end method

.method public getVolume()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->volume:I

    return v0
.end method

.method public onError(Landroid/support/place/rpc/RpcError;)V
    .locals 2
    .param p1    # Landroid/support/place/rpc/RpcError;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->setConnectionError(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;->onError(Landroid/support/place/rpc/RpcError;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onGetPlayerState(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedPlayerState(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    return-void
.end method

.method public onGetRobotAccount(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedRobotAccount(Ljava/lang/String;)V

    return-void
.end method

.method public onGetVideoInfo(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedVideoInfo(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    return-void
.end method

.method public onGetVolume(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedVolume(I)V

    return-void
.end method

.method public onRouteSelected(Landroid/support/place/connector/Broker;Landroid/support/place/connector/ConnectorInfo;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/support/place/connector/Broker;
    .param p2    # Landroid/support/place/connector/ConnectorInfo;
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x2

    const/4 v4, 0x1

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getVersionInfo(Landroid/support/place/connector/ConnectorInfo;)Lcom/google/android/youtube/athome/common/VersionInfo;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v3, "Selected video receiver too old"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->setConnectionError(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;->onAppVersionTooNew()V

    goto :goto_0

    :cond_0
    iget v3, v2, Lcom/google/android/youtube/athome/common/VersionInfo;->minApiVersion:I

    if-le v3, v5, :cond_1

    const-string v3, "Selected video receiver too new"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->setConnectionError(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;->onAppVersionTooOld()V

    goto :goto_1

    :cond_1
    iget v3, v2, Lcom/google/android/youtube/athome/common/VersionInfo;->maxApiVersion:I

    if-le v4, v3, :cond_2

    const-string v3, "Selected video receiver too old"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;->onAppVersionTooNew()V

    goto :goto_2

    :cond_2
    iput-object p3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->screenName:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Selected screen: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget v3, v2, Lcom/google/android/youtube/athome/common/VersionInfo;->minApiVersion:I

    iget v4, v2, Lcom/google/android/youtube/athome/common/VersionInfo;->maxApiVersion:I

    invoke-direct {p0, v3, v5, v4}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->clamp(III)I

    move-result v3

    iput v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->negotiatedVersion:I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateMoviesConnector(Landroid/support/place/connector/Broker;Landroid/support/place/connector/ConnectorInfo;)V

    :cond_3
    return-void
.end method

.method public onRouteUnselected()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateMoviesConnector(Landroid/support/place/connector/Broker;Landroid/support/place/connector/ConnectorInfo;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->screenName:Ljava/lang/String;

    return-void
.end method

.method public setConnectionError(Z)V
    .locals 3
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->connectionError:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;->onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->maybeStartTrackingService()V

    return-void
.end method

.method public setIsTracking(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->isTracking:Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateListener()V

    return-void
.end method

.method public startListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateListener()V

    goto :goto_0
.end method

.method public stopListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateListener()V

    return-void
.end method
