.class public Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;
.super Ljava/lang/Object;
.source "RobotTokenRequest.java"


# static fields
.field public static final RETRY_STRATEGY:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy",
            "<",
            "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final robotAccountName:Ljava/lang/String;

.field public final userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

.field public final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest$1;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest$1;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->RETRY_STRATEGY:Lcom/google/android/youtube/core/async/AuthenticatedRequester$RetryStrategy;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "videoId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->videoId:Ljava/lang/String;

    const-string v0, "robotAccountName cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->robotAccountName:Ljava/lang/String;

    const-string v0, "userAuth cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/UserAuth;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    return-void
.end method
