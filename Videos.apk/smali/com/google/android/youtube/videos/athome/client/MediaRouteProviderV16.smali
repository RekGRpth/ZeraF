.class public Lcom/google/android/youtube/videos/athome/client/MediaRouteProviderV16;
.super Ljava/lang/Object;
.source "MediaRouteProviderV16.java"

# interfaces
.implements Lcom/google/android/youtube/videos/athome/client/MediaRouteProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;Z)V
    .locals 3
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;
    .param p3    # Z

    if-eqz p3, :cond_0

    :goto_0
    return-void

    :cond_0
    const/high16 v2, 0x7f0f0000

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v2, 0x7f07009a

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getActionProvider()Landroid/view/ActionProvider;

    move-result-object v0

    check-cast v0, Landroid/app/MediaRouteActionProvider;

    const v2, 0x800003

    invoke-virtual {v0, v2}, Landroid/app/MediaRouteActionProvider;->setRouteTypes(I)V

    goto :goto_0
.end method

.method public setForceHidden(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method
