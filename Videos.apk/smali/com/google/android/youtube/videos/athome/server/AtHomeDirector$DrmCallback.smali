.class Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;
.super Ljava/lang/Object;
.source "AtHomeDirector.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrmCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/drm/DrmRequest;",
        "Lcom/google/android/youtube/videos/drm/DrmResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private expectedResponses:I

.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(Z)V

    iput p2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;->expectedResponses:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/drm/DrmRequest;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Ljava/lang/Exception;

    instance-of v1, p2, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget v2, v0, Lcom/google/android/youtube/videos/drm/DrmFallbackException;->fallbackDrmLevel:I

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onDrmFallback(I)V
    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$2300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onLicensesError(Ljava/lang/Exception;)V
    invoke-static {v1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$2400(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/drm/DrmRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;->onError(Lcom/google/android/youtube/videos/drm/DrmRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/drm/DrmRequest;
    .param p2    # Lcom/google/android/youtube/videos/drm/DrmResponse;

    iget v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;->expectedResponses:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;->expectedResponses:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onLicensesResponse()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$2200(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/drm/DrmRequest;

    check-cast p2, Lcom/google/android/youtube/videos/drm/DrmResponse;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$DrmCallback;->onResponse(Lcom/google/android/youtube/videos/drm/DrmRequest;Lcom/google/android/youtube/videos/drm/DrmResponse;)V

    return-void
.end method
