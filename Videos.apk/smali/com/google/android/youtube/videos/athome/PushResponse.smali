.class public Lcom/google/android/youtube/videos/athome/PushResponse;
.super Ljava/lang/Object;
.source "PushResponse.java"

# interfaces
.implements Landroid/support/place/rpc/Flattenable;


# static fields
.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/place/rpc/Flattenable$Creator",
            "<",
            "Lcom/google/android/youtube/videos/athome/PushResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final successful:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/athome/PushResponse$1;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/athome/PushResponse$1;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/athome/PushResponse;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/athome/common/SafeRpcData;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/athome/common/SafeRpcData;

    const-string v0, "successful"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/athome/common/SafeRpcData;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/PushResponse;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/athome/PushResponse;->successful:Z

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PushResponse [successful="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/PushResponse;->successful:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2
    .param p1    # Landroid/support/place/rpc/RpcData;

    const-string v0, "successful"

    iget-boolean v1, p0, Lcom/google/android/youtube/videos/athome/PushResponse;->successful:Z

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
