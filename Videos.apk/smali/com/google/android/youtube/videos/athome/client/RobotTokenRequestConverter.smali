.class public Lcom/google/android/youtube/videos/athome/client/RobotTokenRequestConverter;
.super Ljava/lang/Object;
.source "RobotTokenRequestConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/client/RobotTokenRequestConverter$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
        "Lcom/google/android/youtube/core/async/NetworkRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "uri cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequestConverter;->uri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public convertRequest(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;)Lcom/google/android/youtube/core/async/NetworkRequest;
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "videoid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequestConverter$1;->$SwitchMap$com$google$android$youtube$videos$accounts$UserAuth$AuthType:[I

    iget-object v2, p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v2, v2, Lcom/google/android/youtube/videos/accounts/UserAuth;->authMethod:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;

    iget-object v2, v2, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthMethod;->authType:Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/accounts/UserAuth$AuthType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    const-string v1, "&robotaccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->robotAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Lcom/google/android/youtube/core/async/NetworkRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequestConverter;->uri:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/youtube/core/async/NetworkRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    return-object v1

    :pswitch_0
    const-string v1, "&clientauth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v2, v2, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    const-string v1, "&oauth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    iget-object v2, v2, Lcom/google/android/youtube/videos/accounts/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequestConverter;->convertRequest(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;)Lcom/google/android/youtube/core/async/NetworkRequest;

    move-result-object v0

    return-object v0
.end method
