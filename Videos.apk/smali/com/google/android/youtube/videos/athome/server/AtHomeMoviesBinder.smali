.class public interface abstract Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;
.super Ljava/lang/Object;
.source "AtHomeMoviesBinder.java"

# interfaces
.implements Lcom/google/android/youtube/athome/server/ActivityBinder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;
    }
.end annotation


# virtual methods
.method public abstract addListener(Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;)V
.end method

.method public abstract getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;
.end method

.method public abstract getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
.end method

.method public abstract pause(Ljava/lang/String;)V
.end method

.method public abstract play(Ljava/lang/String;)V
.end method

.method public abstract push(Lcom/google/android/youtube/videos/athome/PushRequest;)V
.end method

.method public abstract removeListener(Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;)V
.end method

.method public abstract seekTo(Ljava/lang/String;I)V
.end method

.method public abstract setSubtitles(Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;)V
.end method

.method public abstract stop()V
.end method
