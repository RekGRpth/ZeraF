.class Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$SubtitlesHelperListener;
.super Ljava/lang/Object;
.source "AtHomeDirector.java"

# interfaces
.implements Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubtitlesHelperListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$SubtitlesHelperListener;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p2    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$SubtitlesHelperListener;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    return-void
.end method


# virtual methods
.method public onSubtitleDisabled()V
    .locals 0

    return-void
.end method

.method public onSubtitleEnabled()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$SubtitlesHelperListener;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$SubtitlesHelperListener;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$SubtitlesHelperListener;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->syncSubtitles(I)V

    :cond_0
    return-void
.end method
