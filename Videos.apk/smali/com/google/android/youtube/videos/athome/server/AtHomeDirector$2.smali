.class Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$2;
.super Ljava/lang/Object;
.source "AtHomeDirector.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->initCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/youtube/core/model/Video;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$2;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$2;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error loading video "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$2;->onResponse(Ljava/lang/String;Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Lcom/google/android/youtube/core/model/Video;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$2;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->onVideo(Lcom/google/android/youtube/core/model/Video;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$400(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method
