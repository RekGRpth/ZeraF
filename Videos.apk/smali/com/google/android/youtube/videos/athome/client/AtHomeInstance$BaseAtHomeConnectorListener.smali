.class public abstract Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$BaseAtHomeConnectorListener;
.super Ljava/lang/Object;
.source "AtHomeInstance.java"

# interfaces
.implements Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BaseAtHomeConnectorListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAppVersionTooNew()V
    .locals 0

    return-void
.end method

.method public onAppVersionTooOld()V
    .locals 0

    return-void
.end method

.method public onConnectorChanged(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
    .param p2    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    return-void
.end method

.method public onError(Landroid/support/place/rpc/RpcError;)V
    .locals 0
    .param p1    # Landroid/support/place/rpc/RpcError;

    return-void
.end method

.method public onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    return-void
.end method

.method public onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    return-void
.end method

.method public onVolumeChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method
