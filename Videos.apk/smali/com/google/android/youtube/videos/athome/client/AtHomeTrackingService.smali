.class public Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;
.super Landroid/app/Service;
.source "AtHomeTrackingService.java"

# interfaces
.implements Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;


# instance fields
.field private accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

.field private atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

.field private database:Lcom/google/android/youtube/videos/store/Database;

.field private executor:Ljava/util/concurrent/Executor;

.field private notificationManager:Landroid/app/NotificationManager;

.field private notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

.field private registered:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;)Lcom/google/android/youtube/videos/store/Database;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->database:Lcom/google/android/youtube/videos/store/Database;

    return-object v0
.end method

.method public static createIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private createOpenAppPendingIntent()Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.videos.athome.OPEN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createOpenVideoPendingIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.videos.athome.RESUME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->createVideoUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createVideoUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "videos"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private hideConnectionErrorNotification()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->notificationManager:Landroid/app/NotificationManager;

    const v1, 0x7f070008

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method private registerForUpdates()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->registered:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->registered:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->startListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->setIsTracking(Z)V

    return-void
.end method

.method private showConnectionErrorNotification()V
    .locals 11

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnectorName()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->notificationManager:Landroid/app/NotificationManager;

    const v10, 0x7f070008

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    const v1, 0x7f0a00e6

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v8, v2, v3

    invoke-virtual {v7, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0113

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->createOpenAppPendingIntent()Landroid/app/PendingIntent;

    move-result-object v6

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/youtube/videos/NotificationUtil;->createAtHomeTrackingNotification(Ljava/lang/String;Ljava/lang/String;IIZLandroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v9, v10, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private shutdown()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->registered:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->registered:Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->stopListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->setIsTracking(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->stopForeground(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->notificationManager:Landroid/app/NotificationManager;

    const v1, 0x7f070007

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->stopSelf()V

    return-void
.end method

.method private updateNotification(Lcom/google/android/youtube/videos/athome/PlayerState;Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)Z
    .locals 19
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;
    .param p2    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnectorName()Ljava/lang/String;

    move-result-object v15

    const v2, 0x7f0a00e6

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v15, v5, v6

    invoke-virtual {v14, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->title:Ljava/lang/String;

    move-object/from16 v17, v0

    :goto_0
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    packed-switch v2, :pswitch_data_0

    const/4 v2, 0x0

    :goto_1
    return v2

    :cond_0
    const/16 v17, 0x0

    goto :goto_0

    :pswitch_0
    if-eqz v17, :cond_2

    const v2, 0x7f0a00ee

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v17, v5, v6

    invoke-virtual {v14, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_2
    const/4 v10, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->accountId:Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->accountId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v11

    array-length v13, v11

    const/4 v12, 0x0

    :goto_3
    if-ge v12, v13, :cond_1

    aget-object v9, v11, v12

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->accountId:Ljava/lang/String;

    iget-object v5, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->accountIdMatchesAccount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v10, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    :cond_1
    if-nez v10, :cond_5

    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const v2, 0x7f0a00ec

    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :pswitch_1
    if-eqz v17, :cond_3

    const v2, 0x7f0a00ed

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v17, v5, v6

    invoke-virtual {v14, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_4
    goto :goto_2

    :cond_3
    const v2, 0x7f0a00eb

    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->hideConnectionErrorNotification()V

    const v18, 0x7f070007

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    if-nez v7, :cond_7

    const/4 v7, 0x1

    :goto_5
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v10, v8, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->createOpenVideoPendingIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/youtube/videos/NotificationUtil;->createAtHomeTrackingNotification(Ljava/lang/String;Ljava/lang/String;IIZLandroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->startForeground(ILandroid/app/Notification;)V

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    const/4 v5, 0x1

    if-eq v2, v5, :cond_6

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v5}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->updateResumeTimestamp(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_6
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_7
    const/4 v7, 0x0

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateResumeTimestamp(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "last_playback_is_dirty"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "last_playback_start_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "last_watched_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "resume_timestamp"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->executor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private updateServiceStatus()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getConnectionError()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->showConnectionErrorNotification()V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->shutdown()V

    :goto_1
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->hideConnectionErrorNotification()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->updateNotification(Lcom/google/android/youtube/videos/athome/PlayerState;Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->registerForUpdates()V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->shutdown()V

    goto :goto_1
.end method


# virtual methods
.method public onAppVersionTooNew()V
    .locals 0

    return-void
.end method

.method public onAppVersionTooOld()V
    .locals 0

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onConnectorChanged(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
    .param p2    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->updateServiceStatus()V

    return-void
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getAtHomeInstance()Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getNotificationUtil()Lcom/google/android/youtube/videos/NotificationUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->notificationUtil:Lcom/google/android/youtube/videos/NotificationUtil;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->accountManagerWrapper:Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getDatabase()Lcom/google/android/youtube/videos/store/Database;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->database:Lcom/google/android/youtube/videos/store/Database;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getExecutor()Ljava/util/concurrent/Executor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->executor:Ljava/util/concurrent/Executor;

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->notificationManager:Landroid/app/NotificationManager;

    return-void
.end method

.method public onError(Landroid/support/place/rpc/RpcError;)V
    .locals 0
    .param p1    # Landroid/support/place/rpc/RpcError;

    return-void
.end method

.method public onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->updateServiceStatus()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/high16 v6, 0x10000000

    if-eqz p1, :cond_2

    const-string v4, "com.google.android.videos.athome.RESUME"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v4, "videos"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    const-string v4, "season_id"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p0, v0, v3, v6}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createWatchMovieDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->updateServiceStatus()V

    const/4 v4, 0x2

    return v4

    :cond_1
    invoke-static {p0, v0, v1, v3, v6}, Lcom/google/android/youtube/videos/activity/LauncherActivity;->createWatchEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_0

    const-string v4, "com.google.android.videos.athome.OPEN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->setConnectionError(Z)V

    const/4 v4, 0x0

    const-string v5, "movies"

    invoke-static {p0, v4, v5}, Lcom/google/android/youtube/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->updateServiceStatus()V

    return-void
.end method

.method public onVolumeChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method
