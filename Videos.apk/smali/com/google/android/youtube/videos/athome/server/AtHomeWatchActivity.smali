.class public Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;
.super Lcom/google/android/youtube/athome/server/BinderActivity;
.source "AtHomeWatchActivity.java"

# interfaces
.implements Lcom/google/android/youtube/core/utils/HdmiReceiver$HdmiListener;
.implements Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;
.implements Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$AudioFocusListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/athome/server/BinderActivity",
        "<",
        "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;",
        ">;",
        "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;",
        "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;",
        "Lcom/google/android/youtube/core/utils/HdmiReceiver$HdmiListener;"
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private audioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private audioManager:Landroid/media/AudioManager;

.field private director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

.field private hasBeenStopped:Z

.field private hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

.field private idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;

.field private immersionHelper:Lcom/google/android/youtube/core/utils/ImmersionHelper;

.field private listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private nfcHelper:Lcom/android/athome/utils/NfcHelper;

.field private parsedRobotToken:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field

.field private pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

.field private statsClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/athome/server/BinderActivity;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;Lcom/google/android/youtube/videos/athome/PushRequest;)Lcom/google/android/youtube/videos/athome/PushRequest;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;
    .param p1    # Lcom/google/android/youtube/videos/athome/PushRequest;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parsedRobotToken:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;Ljava/util/Map;)Ljava/util/Map;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;
    .param p1    # Ljava/util/Map;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parsedRobotToken:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parseRobotToken(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->isValidRequest()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->hasBeenStopped:Z

    return v0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/videos/athome/PushRequest;)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "authAccount"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "push_request"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private isValidRequest()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->account:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v0, v0, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v0, v0, Lcom/google/android/youtube/videos/athome/PushRequest;->robotToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parsedRobotToken:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parsedRobotToken:Ljava/util/Map;

    const-string v1, "robot_token"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v0, v0, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v0, v0, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parseRobotToken(Ljava/lang/String;)Ljava/util/Map;
    .locals 12
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    const/16 v11, 0x8

    const/4 v10, 0x2

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v9, "&"

    invoke-virtual {p1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "robot_token"

    invoke-static {p1, v11}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v10

    invoke-interface {v6, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v6

    :cond_1
    const-string v9, "&"

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    array-length v5, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v0, v2, v3

    const-string v9, "="

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v4, v7, v9

    array-length v9, v7

    if-ne v9, v10, :cond_2

    const/4 v9, 0x1

    aget-object v9, v7, v9

    invoke-static {v9, v11}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v8

    :goto_1
    invoke-interface {v6, v4, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_1
.end method


# virtual methods
.method public addListener(Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/videos/athome/PlayerState;-><init>(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    return-object v0
.end method

.method public getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onBind()Lcom/google/android/youtube/athome/server/ActivityBinder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->onBind()Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;

    move-result-object v0

    return-object v0
.end method

.method protected onBind()Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;
    .locals 0

    return-object p0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 22
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lcom/google/android/youtube/athome/server/BinderActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v21

    const/high16 v2, 0x200000

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v20

    const-string v2, "authAccount"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->account:Ljava/lang/String;

    const-string v2, "push_request"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/athome/PushRequest;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v2, v2, Lcom/google/android/youtube/videos/athome/PushRequest;->robotToken:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parseRobotToken(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parsedRobotToken:Ljava/util/Map;

    const v2, 0x7f04003a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->setContentView(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v18

    check-cast v18, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    const v2, 0x7f070080

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/youtube/core/player/PlayerView;

    new-instance v12, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v19

    new-instance v2, Lcom/google/android/youtube/athome/server/IdleTimeout;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-interface/range {v19 .. v19}, Lcom/google/android/youtube/videos/Config;->atHomeIdleTimeoutMillis()J

    move-result-wide v9

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v9, v10}, Lcom/google/android/youtube/athome/server/IdleTimeout;-><init>(Landroid/app/Activity;Landroid/os/Handler;J)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getVideoStats2ClientFactory()Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->statsClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    new-instance v2, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getErrorHelper()Lcom/google/android/youtube/core/ErrorHelper;

    move-result-object v3

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getRobotAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getSubtitlesClient()Lcom/google/android/youtube/core/client/SubtitlesClient;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->statsClientFactory:Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getDrmManager()Lcom/google/android/youtube/videos/drm/DrmManager;

    move-result-object v11

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getNetworkStatus()Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v13

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getStreamsSelector()Lcom/google/android/youtube/videos/StreamsSelector;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/youtube/videos/VideosApplication;->getEventLogger()Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v17

    move-object/from16 v6, p0

    move-object/from16 v16, p0

    invoke-direct/range {v2 .. v17}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;-><init>(Lcom/google/android/youtube/core/ErrorHelper;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/player/PlayerView;Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/videos/Requesters;Lcom/google/android/youtube/core/client/SubtitlesClient;Lcom/google/android/youtube/core/client/VideoStats2Client$Provider;Lcom/google/android/youtube/videos/drm/DrmManager;Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;Lcom/google/android/youtube/core/utils/NetworkStatus;Lcom/google/android/youtube/videos/StreamsSelector;Lcom/google/android/youtube/athome/server/IdleTimeout;Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    new-instance v2, Lcom/google/android/youtube/core/utils/HdmiReceiver;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/core/utils/HdmiReceiver;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/HdmiReceiver$HdmiListener;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->getActivityBinderHandler(Landroid/content/Context;)Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->setHandler(Lcom/google/android/youtube/athome/server/ActivityBinderHandler;)V

    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->listeners:Ljava/util/List;

    const-string v2, "audio"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->audioManager:Landroid/media/AudioManager;

    new-instance v2, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$AudioFocusListener;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$AudioFocusListener;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->audioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    new-instance v2, Lcom/google/android/youtube/core/utils/ImmersionHelper;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/core/utils/ImmersionHelper;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->immersionHelper:Lcom/google/android/youtube/core/utils/ImmersionHelper;

    new-instance v2, Lcom/android/athome/utils/NfcHelper;

    const-string v3, "https://play.google.com/store/apps/details?id=com.google.android.setupwarlock&rdid=com.google.android.setupwarlock&rdot=1"

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/athome/utils/NfcHelper;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->nfcHelper:Lcom/android/athome/utils/NfcHelper;

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->isFinishing()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->release(Z)V

    invoke-super {p0}, Lcom/google/android/youtube/athome/server/BinderActivity;->onDestroy()V

    return-void
.end method

.method public onHdmiPluggedState(Z)V
    .locals 0
    .param p1    # Z

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->finish()V

    :cond_0
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v1, 0x17

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v0

    iget v1, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->pauseVideo()V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/athome/server/BinderActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playVideo()V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/youtube/athome/server/BinderActivity;->onNewIntent(Landroid/content/Intent;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->hasBeenStopped:Z

    return-void
.end method

.method protected onPause()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->nfcHelper:Lcom/android/athome/utils/NfcHelper;

    invoke-virtual {v0}, Lcom/android/athome/utils/NfcHelper;->onPause()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/server/IdleTimeout;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->immersionHelper:Lcom/google/android/youtube/core/utils/ImmersionHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/ImmersionHelper;->setImmersive(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->audioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->unregister()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->reset()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->hasBeenStopped:Z

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    invoke-super {p0}, Lcom/google/android/youtube/athome/server/BinderActivity;->onPause()V

    return-void
.end method

.method public onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;->onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/youtube/athome/server/BinderActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->hasBeenStopped:Z

    return-void
.end method

.method protected onResume()V
    .locals 6

    const/4 v0, 0x1

    invoke-super {p0}, Lcom/google/android/youtube/athome/server/BinderActivity;->onResume()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->register()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->audioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->audioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/high16 v3, -0x80000000

    invoke-virtual {v1, v2, v3, v0}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->immersionHelper:Lcom/google/android/youtube/core/utils/ImmersionHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/utils/ImmersionHelper;->setImmersive(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;

    invoke-virtual {v1}, Lcom/google/android/youtube/athome/server/IdleTimeout;->onBusy()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->nfcHelper:Lcom/android/athome/utils/NfcHelper;

    invoke-virtual {v1}, Lcom/android/athome/utils/NfcHelper;->onResume()V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->parsedRobotToken:Ljava/util/Map;

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->hasBeenStopped:Z

    if-nez v5, :cond_0

    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->init(Ljava/lang/String;Lcom/google/android/youtube/videos/athome/PushRequest;Ljava/util/Map;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 10

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/android/youtube/athome/server/BinderActivity;->onStart()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->isValidRequest()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid request: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->pushRequest:Lcom/google/android/youtube/videos/athome/PushRequest;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/youtube/videos/athome/PlayerState;

    const/4 v3, 0x4

    const v2, 0x7f0a0090

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    move-object v2, v1

    move v5, v4

    move v6, v4

    move v7, v4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/videos/athome/PlayerState;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIZLjava/lang/String;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->finish()V

    :cond_0
    return-void
.end method

.method public onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;->onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public pause(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$3;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public play(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$2;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public push(Lcom/google/android/youtube/videos/athome/PushRequest;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/PushRequest;

    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$1;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;Lcom/google/android/youtube/videos/athome/PushRequest;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public removeListener(Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public seekTo(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$4;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$4;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setSubtitles(Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->director:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v1, p2, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v2, p2, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;->videoId:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/youtube/core/model/SubtitleTrack;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->setSubtitleTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity$5;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
