.class Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;
.super Ljava/lang/Object;
.source "AtHomeTrackingService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->updateResumeTimestamp(Ljava/lang/String;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$values:Landroid/content/ContentValues;

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;

    iput-object p2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$values:Landroid/content/ContentValues;

    iput-object p3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$account:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$videoId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    const/4 v11, 0x5

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;

    # getter for: Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->access$000(Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    const-string v2, "video_userdata"

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$values:Landroid/content/ContentValues;

    const-string v4, "pinning_account = ? AND pinning_video_id = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$account:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$videoId:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;

    # getter for: Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->access$000(Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$account:Ljava/lang/String;

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$videoId:Ljava/lang/String;

    aput-object v4, v3, v9

    invoke-virtual {v2, v1, v0, v11, v3}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;

    # getter for: Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->database:Lcom/google/android/youtube/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;->access$000(Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService;)Lcom/google/android/youtube/videos/store/Database;

    move-result-object v3

    new-array v4, v10, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$account:Ljava/lang/String;

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeTrackingService$1;->val$videoId:Ljava/lang/String;

    aput-object v5, v4, v9

    invoke-virtual {v3, v1, v0, v11, v4}, Lcom/google/android/youtube/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2
.end method
