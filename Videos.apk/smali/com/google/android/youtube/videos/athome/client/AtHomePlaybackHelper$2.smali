.class Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$2;
.super Ljava/lang/Object;
.source "AtHomePlaybackHelper.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$2;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$2;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    # invokes: Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onRobotTokenError(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->access$300(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$2;->onError(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$2;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    # invokes: Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onRobotToken(Ljava/lang/String;)V
    invoke-static {v0, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->access$200(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$2;->onResponse(Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;Ljava/lang/String;)V

    return-void
.end method
