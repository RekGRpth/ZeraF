.class final Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;
.super Ljava/lang/Object;
.source "AtHomeMoviesConnector.java"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "_ResultDispatcher"
.end annotation


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;ILjava/lang/Object;)V
    .locals 0
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->this$0:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getPlayerState([B)V
    .locals 4
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    sget-object v3, Lcom/google/android/youtube/videos/athome/PlayerState;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v2, v3}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetPlayerState;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetPlayerState;->onGetPlayerState(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    return-void
.end method

.method public getRobotAccount([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetRobotAccount;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetRobotAccount;->onGetRobotAccount(Ljava/lang/String;)V

    return-void
.end method

.method public getVideoInfo([B)V
    .locals 4
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    sget-object v3, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v2, v3}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVideoInfo;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVideoInfo;->onGetVideoInfo(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    return-void
.end method

.method public getVolume([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVolume;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVolume;->onGetVolume(I)V

    return-void
.end method

.method public onResult([B)V
    .locals 1
    .param p1    # [B

    iget v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->getRobotAccount([B)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->getPlayerState([B)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->getVolume([B)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->getVideoInfo([B)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->push2([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public push2([B)V
    .locals 4
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    sget-object v3, Lcom/google/android/youtube/videos/athome/PushResponse;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v2, v3}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/athome/PushResponse;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnPush2;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnPush2;->onPush2(Lcom/google/android/youtube/videos/athome/PushResponse;)V

    return-void
.end method
