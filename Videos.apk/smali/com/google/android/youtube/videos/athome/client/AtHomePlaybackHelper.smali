.class public Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;
.super Ljava/lang/Object;
.source "AtHomePlaybackHelper.java"

# interfaces
.implements Landroid/support/place/rpc/RpcErrorHandler;
.implements Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnPush2;
.implements Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;
.implements Lcom/google/android/youtube/videos/player/PlaybackHelper;


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

.field private cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/CancelableCallback",
            "<**>;"
        }
    .end annotation
.end field

.field private final controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

.field private final getRobotAccountCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final handler:Landroid/os/Handler;

.field private lastPlayerState:Lcom/google/android/youtube/videos/athome/PlayerState;

.field private lastStateUpdateMillis:J

.field private pushRequestBuilder:Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

.field private final pushTimeout:Lcom/google/android/youtube/athome/common/Timeout;

.field private final remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

.field private final requestRobotTokenCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final robotTokenRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final seasonId:Ljava/lang/String;

.field private subtitleTrack:Lcom/google/android/youtube/core/model/SubtitleTrack;

.field private userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

.field private final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
    .param p4    # Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    .param p5    # Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;",
            "Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->activity:Landroid/app/Activity;

    const-string v0, "atHomeInstance cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    const-string v0, "robotTokenRequester cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->robotTokenRequester:Lcom/google/android/youtube/core/async/Requester;

    const-string v0, "controllerOverlay cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    const-string v0, "remoteScreenPanelHelper cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    const-string v0, "videoId cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->seasonId:Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$1;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->getRobotAccountCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$2;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->requestRobotTokenCallback:Lcom/google/android/youtube/core/async/Callback;

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$3;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const-wide/16 v2, 0x2710

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$3;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;Landroid/os/Handler;J)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushTimeout:Lcom/google/android/youtube/athome/common/Timeout;

    new-instance v0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$4;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper$4;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onRobotAccount(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onRobotAccountError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onRobotToken(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onRobotTokenError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onAtHomeError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onHeartbeat()V

    return-void
.end method

.method private concatIntegers(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/core/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/youtube/core/async/CancelableCallback;->create(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/CancelableCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->activity:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/ActivityCallback;

    move-result-object v1

    return-object v1
.end method

.method private onAtHomeError(Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/Exception;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showPaused()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushTimeout:Lcom/google/android/youtube/athome/common/Timeout;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/common/Timeout;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->setConnectionError(Z)V

    return-void
.end method

.method private onHeartbeat()V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->lastStateUpdateMillis:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->lastPlayerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->lastPlayerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v2, v2, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->lastPlayerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v2, v2, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x7530

    if-ge v0, v2, :cond_0

    new-instance v1, Lcom/google/android/youtube/videos/athome/PlayerState;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->lastPlayerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/athome/PlayerState;-><init>(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    iget v2, v1, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    add-int/2addr v2, v0

    iput v2, v1, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->updateControls(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    :cond_0
    return-void
.end method

.method private onRobotAccount(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requesting robot token for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->robotTokenRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v1, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-direct {v1, v2, p1, v3}, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->requestRobotTokenCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method private onRobotAccountError(Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onAtHomeError(Ljava/lang/Exception;)V

    return-void
.end method

.method private onRobotToken(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushRequestBuilder:Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushRequestBuilder:Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->robotToken(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->build()Lcom/google/android/youtube/videos/athome/PushRequest;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnectorVersion()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MoviesConnector: push("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->resumeTimeMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->durationMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->robotToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    const-string v2, ","

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->concatIntegers(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "])"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnector()Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    move-result-object v0

    iget-object v1, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->videoId:Ljava/lang/String;

    iget v2, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->resumeTimeMillis:I

    iget v3, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->durationMillis:I

    iget-object v4, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->robotToken:Ljava/lang/String;

    iget-object v5, v7, Lcom/google/android/youtube/videos/athome/PushRequest;->purchasedFormats:Ljava/util/List;

    const-string v6, ","

    invoke-direct {p0, v5, v6}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->concatIntegers(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->push(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushRequestBuilder:Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->robotToken(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    const-string v0, "Starting push timeout"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushTimeout:Lcom/google/android/youtube/athome/common/Timeout;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/common/Timeout;->start()V

    goto/16 :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MoviesConnector: push2("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnector()Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    move-result-object v0

    invoke-virtual {v0, v7, p0, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->push2(Lcom/google/android/youtube/videos/athome/PushRequest;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnPush2;Landroid/support/place/rpc/RpcErrorHandler;)V

    goto :goto_1
.end method

.method private onRobotTokenError(Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onAtHomeError(Ljava/lang/Exception;)V

    return-void
.end method

.method private scheduleHeartbeat()V
    .locals 4

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "should not have pending heartbeat"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->stopHeartbeat()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private stopHeartbeat()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method private updateControls(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    iget v0, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget v1, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->time:I

    iget v2, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->duration:I

    iget v3, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->bufferedPercentage:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setTimes(III)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setLoading()V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->setKeepScreenOn(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setPlaying()V

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->scheduleHeartbeat()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showPaused()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget-object v1, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->errorMessage:Ljava/lang/String;

    iget-boolean v2, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->canRetry:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showErrorMessage(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public alreadyPlaying()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getVideoInfo()Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    move-result-object v0

    return-object v0
.end method

.method public init()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setAutoHide(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setHideOnTap(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setSupportsQualityToggle(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->startListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getPlayerState()Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnectorName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->init(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public isSecure()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public load(Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/List;Lcom/google/android/youtube/core/model/VideoStreams;IIZZJZ)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p3    # Lcom/google/android/youtube/core/model/VideoStreams;
    .param p4    # I
    .param p5    # I
    .param p6    # Z
    .param p7    # Z
    .param p8    # J
    .param p10    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/UserAuth;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/youtube/core/model/VideoStreams;",
            "IIZZJZ)V"
        }
    .end annotation

    const-string v0, "userAuth cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/accounts/UserAuth;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->userAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    new-instance v0, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;-><init>()V

    iget-object v1, p1, Lcom/google/android/youtube/videos/accounts/UserAuth;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->seasonId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getAccountId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->accountId(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->videoId(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->resumeTimeMillis(I)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->durationMillis(I)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->purchasedFormats(Ljava/util/List;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushRequestBuilder:Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->subtitleTrack:Lcom/google/android/youtube/core/model/SubtitleTrack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushRequestBuilder:Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    new-instance v1, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    invoke-direct {v1}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->subtitleTrack:Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->languageCode(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->subtitleTrack:Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->trackName(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->videoId(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->build()Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->subtitles(Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->getRobotAccountCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->decorateCallback(Lcom/google/android/youtube/core/async/Callback;)Lcom/google/android/youtube/core/async/Callback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getRobotAccount(Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public onAppVersionTooNew()V
    .locals 0

    return-void
.end method

.method public onAppVersionTooOld()V
    .locals 0

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onConnectorChanged(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
    .param p2    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showPaused()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showControls()V

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->stop(Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_0
    return-void
.end method

.method public onError(Landroid/support/place/rpc/RpcError;)V
    .locals 0
    .param p1    # Landroid/support/place/rpc/RpcError;

    return-void
.end method

.method public onHQ()V
    .locals 0

    return-void
.end method

.method public onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "Loading started on Tungsten, resetting timeout"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushTimeout:Lcom/google/android/youtube/athome/common/Timeout;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/common/Timeout;->reset()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->stopHeartbeat()V

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showPaused()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p1, Lcom/google/android/youtube/videos/athome/PlayerState;->videoId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->lastPlayerState:Lcom/google/android/youtube/videos/athome/PlayerState;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->lastStateUpdateMillis:J

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->updateControls(Lcom/google/android/youtube/videos/athome/PlayerState;)V

    goto :goto_0
.end method

.method public onPush2(Lcom/google/android/youtube/videos/athome/PushResponse;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/athome/PushResponse;

    iget-boolean v0, p1, Lcom/google/android/youtube/videos/athome/PushResponse;->successful:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->pushTimeout:Lcom/google/android/youtube/athome/common/Timeout;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/common/Timeout;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->setConnectionError(Z)V

    :cond_0
    return-void
.end method

.method public onScrubbingStart()V
    .locals 0

    return-void
.end method

.method public onSeekTo(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnector()Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->seekTo(Ljava/lang/String;ILandroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;

    return-void
.end method

.method public onVideoTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onVolumeChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnector()Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->pause(Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public play()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnector()Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->play(Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public release(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/CancelableCallback;->cancel()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->cancelableCallback:Lcom/google/android/youtube/core/async/CancelableCallback;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->stopHeartbeat()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->stopListening(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$AtHomeConnectorListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->reset()V

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->subtitleTrack:Lcom/google/android/youtube/core/model/SubtitleTrack;

    return-void
.end method

.method public selectTrack(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/core/model/SubtitleTrack;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->subtitleTrack:Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setCcEnabled(Z)V

    new-instance v1, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    invoke-direct {v1}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->videoId(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->languageCode(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->trackName(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack$Builder;->build()Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MoviesConnector: setSubtitles("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", AtHomeSubtitleTrack["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]) called"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->atHomeInstance:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getMoviesConnector()Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->setSubtitles(Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setKeepScreenOn(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/ui/RemoteScreenPanelHelper;->setKeepScreenOn(Z)V

    return-void
.end method

.method public setLoading()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setLoading()V

    return-void
.end method

.method public showErrorMessage(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showErrorMessage(Ljava/lang/String;Z)V

    return-void
.end method

.method public showPaused()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomePlaybackHelper;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->showPaused()V

    return-void
.end method
