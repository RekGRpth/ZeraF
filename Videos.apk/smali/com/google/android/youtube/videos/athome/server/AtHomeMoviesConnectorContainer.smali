.class public Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;
.super Landroid/support/place/connector/ConnectorContainer;
.source "AtHomeMoviesConnectorContainer.java"

# interfaces
.implements Lcom/google/android/youtube/core/utils/HdmiReceiver$HdmiListener;


# static fields
.field private static activityBinderHelper:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/athome/server/ActivityBinderHandler",
            "<",
            "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private broker:Landroid/support/place/connector/Broker;

.field private connector:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;

.field private connectorRegistered:Z

.field private hdmiDisconnectTimeout:Lcom/google/android/youtube/athome/common/Timeout;

.field private hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

.field private place:Landroid/support/place/connector/PlaceInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/place/connector/ConnectorContainer;-><init>()V

    return-void
.end method

.method private createHdmiDisconnectTimeout(Landroid/os/Handler;J)Lcom/google/android/youtube/athome/common/Timeout;
    .locals 1
    .param p1    # Landroid/os/Handler;
    .param p2    # J

    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer$1;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;Landroid/os/Handler;J)V

    return-object v0
.end method

.method public static getActivityBinderHandler(Landroid/content/Context;)Lcom/google/android/youtube/athome/server/ActivityBinderHandler;
    .locals 1
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/android/youtube/athome/server/ActivityBinderHandler",
            "<",
            "Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesBinder;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->activityBinderHelper:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/athome/server/SingleActivityBinderHandler;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->activityBinderHelper:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    :cond_0
    sget-object v0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->activityBinderHelper:Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    return-object v0
.end method


# virtual methods
.method public onBrokerConnected(Landroid/support/place/connector/Broker;)V
    .locals 0
    .param p1    # Landroid/support/place/connector/Broker;

    invoke-super {p0, p1}, Landroid/support/place/connector/ConnectorContainer;->onBrokerConnected(Landroid/support/place/connector/Broker;)V

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->broker:Landroid/support/place/connector/Broker;

    return-void
.end method

.method public onBrokerDisconnected()V
    .locals 1

    invoke-super {p0}, Landroid/support/place/connector/ConnectorContainer;->onBrokerDisconnected()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->broker:Landroid/support/place/connector/Broker;

    return-void
.end method

.method public onCreate()V
    .locals 5

    invoke-super {p0}, Landroid/support/place/connector/ConnectorContainer;->onCreate()V

    new-instance v4, Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-direct {v4, p0, p0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/HdmiReceiver$HdmiListener;)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v4}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->register()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->atHomeHdmiDisconnectTimeoutMillis()J

    move-result-wide v2

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v4, v2, v3}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->createHdmiDisconnectTimeout(Landroid/os/Handler;J)Lcom/google/android/youtube/athome/common/Timeout;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->hdmiDisconnectTimeout:Lcom/google/android/youtube/athome/common/Timeout;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->unregister()V

    invoke-super {p0}, Landroid/support/place/connector/ConnectorContainer;->onDestroy()V

    return-void
.end method

.method public onHdmiPluggedState(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->hdmiDisconnectTimeout:Lcom/google/android/youtube/athome/common/Timeout;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/common/Timeout;->reset()V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->updateConnectorState()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->hdmiDisconnectTimeout:Lcom/google/android/youtube/athome/common/Timeout;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/common/Timeout;->start()V

    goto :goto_0
.end method

.method public onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V
    .locals 8
    .param p1    # Landroid/support/place/connector/PlaceInfo;

    invoke-super {p0, p1}, Landroid/support/place/connector/ConnectorContainer;->onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->getApplication()Landroid/app/Application;

    move-result-object v7

    check-cast v7, Lcom/google/android/youtube/videos/VideosApplication;

    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/VideosApplication;->getApplicationVersion()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->broker:Landroid/support/place/connector/Broker;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/VideosApplication;->getRobotAccountManagerWrapper()Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->getActivityBinderHandler(Landroid/content/Context;)Lcom/google/android/youtube/athome/server/ActivityBinderHandler;

    move-result-object v6

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/athome/server/ActivityBinderHandler;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->connector:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->updateConnectorState()V

    return-void
.end method

.method public onPlaceDisconnected()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/support/place/connector/ConnectorContainer;->onPlaceDisconnected()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->updateConnectorState()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->connector:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;

    return-void
.end method

.method public updateConnectorState()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->place:Landroid/support/place/connector/PlaceInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->isHdmiPlugged()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->connectorRegistered:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->connector:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;

    iget-object v2, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->connector:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/place/connector/Broker;->registerConnector(Landroid/support/place/connector/Endpoint;Landroid/support/place/connector/ConnectorInfo;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->connectorRegistered:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->place:Landroid/support/place/connector/PlaceInfo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->hdmiReceiver:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->isHdmiPlugged()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->connectorRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->connector:Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorImpl;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/Broker;->unregisterConnector(Landroid/support/place/connector/Endpoint;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeMoviesConnectorContainer;->connectorRegistered:Z

    goto :goto_0
.end method
