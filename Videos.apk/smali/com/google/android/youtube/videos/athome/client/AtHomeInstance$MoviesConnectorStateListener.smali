.class final Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$MoviesConnectorStateListener;
.super Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;
.source "AtHomeInstance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MoviesConnectorStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$MoviesConnectorStateListener;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;
    .param p2    # Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$MoviesConnectorStateListener;-><init>(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;)V

    return-void
.end method


# virtual methods
.method public onPlayerStateChanged(Lcom/google/android/youtube/videos/athome/PlayerState;Landroid/support/place/rpc/RpcContext;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/athome/PlayerState;
    .param p2    # Landroid/support/place/rpc/RpcContext;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$MoviesConnectorStateListener;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    # invokes: Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedPlayerState(Lcom/google/android/youtube/videos/athome/PlayerState;)V
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->access$100(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/videos/athome/PlayerState;)V

    return-void
.end method

.method public onVideoInfoChanged(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;Landroid/support/place/rpc/RpcContext;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;
    .param p2    # Landroid/support/place/rpc/RpcContext;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$MoviesConnectorStateListener;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    # invokes: Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedVideoInfo(Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->access$200(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;Lcom/google/android/youtube/athome/common/AtHomeVideoInfo;)V

    return-void
.end method

.method public onVolumeChanged(ILandroid/support/place/rpc/RpcContext;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/support/place/rpc/RpcContext;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance$MoviesConnectorStateListener;->this$0:Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;

    # invokes: Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->updateCachedVolume(I)V
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->access$300(Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;I)V

    return-void
.end method
