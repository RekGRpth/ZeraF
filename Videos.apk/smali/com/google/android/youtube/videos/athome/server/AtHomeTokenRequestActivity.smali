.class public Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;
.super Landroid/app/Activity;
.source "AtHomeTokenRequestActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;
    }
.end annotation


# instance fields
.field private mApp:Lcom/google/android/youtube/videos/VideosApplication;

.field private mGoogleAccounts:[Landroid/accounts/Account;

.field private mGoogleUserAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

.field private mRobotAccounts:[Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;)Lcom/google/android/youtube/videos/VideosApplication;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mApp:Lcom/google/android/youtube/videos/VideosApplication;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/videos/accounts/UserAuth;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mGoogleUserAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->onUserAuthReceived()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/util/List;

    invoke-direct/range {p0 .. p6}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->launchActivity(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video_id"

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "purchase_format"

    const-string v2, "SD_FORMAT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private launchActivity(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;-><init>()V

    invoke-static {p1, p4}, Lcom/google/android/youtube/videos/athome/client/AtHomeInstance;->getAccountId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->accountId(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->resumeTimeMillis(I)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->videoId(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v2

    invoke-virtual {v2, p5}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->robotToken(Ljava/lang/String;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    move-result-object v2

    invoke-virtual {v2, p6}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->purchasedFormats(Ljava/util/List;)Lcom/google/android/youtube/videos/athome/PushRequest$Builder;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/athome/PushRequest$Builder;->build()Lcom/google/android/youtube/videos/athome/PushRequest;

    move-result-object v2

    invoke-static {p0, p2, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/videos/athome/PushRequest;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onUserAuthReceived()V
    .locals 9

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "season_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "purchase_format"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mGoogleAccounts:[Landroid/accounts/Account;

    aget-object v0, v0, v8

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mRobotAccounts:[Landroid/accounts/Account;

    aget-object v0, v0, v8

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v0, "AtHomeTokenRequestActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "About to request robot token for videoId:"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " robotAccountName: "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " and UserAuth "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v8, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mGoogleUserAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-virtual {v8}, Lcom/google/android/youtube/videos/accounts/UserAuth;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mGoogleUserAuth:Lcom/google/android/youtube/videos/accounts/UserAuth;

    invoke-direct {v7, v5, v4, v0}, Lcom/google/android/youtube/videos/athome/client/RobotTokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mApp:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Requesters;->getRobotTokenRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v8

    new-instance v0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v7, v0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method


# virtual methods
.method protected onResume()V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mGoogleAccounts:[Landroid/accounts/Account;

    const-string v1, "android.athome"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mRobotAccounts:[Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/VideosApplication;

    iput-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mApp:Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mGoogleAccounts:[Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mGoogleAccounts:[Landroid/accounts/Account;

    array-length v1, v1

    if-ge v1, v3, :cond_1

    :cond_0
    const-string v1, "AtHomeTokenRequestActivity"

    const-string v2, "No google accounts found"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->finish()V

    :cond_1
    new-instance v1, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$1;)V

    new-array v2, v3, [Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity;->mGoogleAccounts:[Landroid/accounts/Account;

    aget-object v3, v3, v4

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/athome/server/AtHomeTokenRequestActivity$UserAuthAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
