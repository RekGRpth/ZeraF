.class public Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;
.super Ljava/lang/Object;
.source "AtHomeMoviesConnector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;,
        Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;,
        Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;,
        Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnPush2;,
        Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVideoInfo;,
        Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVolume;,
        Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetPlayerState;,
        Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetRobotAccount;,
        Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$EndpointBase;
    }
.end annotation


# instance fields
.field private _broker:Landroid/support/place/connector/Broker;

.field private _endpoint:Landroid/support/place/rpc/EndpointInfo;

.field private _presenter:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 0
    .param p1    # Landroid/support/place/connector/Broker;
    .param p2    # Landroid/support/place/rpc/EndpointInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iput-object p2, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-void
.end method


# virtual methods
.method public getPlayerState(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetPlayerState;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetPlayerState;
    .param p2    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getPlayerState"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;

    const/4 v5, 0x7

    invoke-direct {v4, p0, v5, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;-><init>(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getRobotAccount(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetRobotAccount;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetRobotAccount;
    .param p2    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getRobotAccount"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;

    const/4 v5, 0x6

    invoke-direct {v4, p0, v5, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;-><init>(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getVideoInfo(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVideoInfo;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVideoInfo;
    .param p2    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getVideoInfo"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;

    const/16 v5, 0xc

    invoke-direct {v4, p0, v5, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;-><init>(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getVolume(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVolume;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnGetVolume;
    .param p2    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getVolume"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;

    const/16 v5, 0x9

    invoke-direct {v4, p0, v5, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;-><init>(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public pause(Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "videoId"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "pause"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public play(Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "videoId"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "play"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public push(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "videoId"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "resumeTimeMillis"

    invoke-virtual {v6, v0, p2}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "durationMillis"

    invoke-virtual {v6, v0, p3}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "token"

    invoke-virtual {v6, v0, p4}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "formats"

    invoke-virtual {v6, v0, p5}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "push"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public push2(Lcom/google/android/youtube/videos/athome/PushRequest;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnPush2;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/athome/PushRequest;
    .param p2    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$OnPush2;
    .param p3    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "request"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "push2"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;

    const/16 v5, 0xe

    invoke-direct {v4, p0, v5, p2}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$_ResultDispatcher;-><init>(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public seekTo(Ljava/lang/String;ILandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "videoId"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "timeMillis"

    invoke-virtual {v6, v0, p2}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "seekTo"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setSubtitles(Ljava/lang/String;Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;
    .param p3    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "videoId"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "subtitles"

    invoke-virtual {v6, v0, p2}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setSubtitles"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setVolume(ILandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # I
    .param p2    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "volume"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setVolume"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public startListening(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->stopListening()V

    new-instance v0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;-><init>(Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;Landroid/support/place/connector/Broker;Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Listener;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_presenter:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_presenter:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;->startListening(Landroid/support/place/rpc/EndpointInfo;)V

    return-void
.end method

.method public stop(Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "stop"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_presenter:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_presenter:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;->stopListening()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;->_presenter:Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector$Presenter;

    :cond_0
    return-void
.end method
