.class public Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;
.super Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;
.source "MediaRouteManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;
    }
.end annotation


# static fields
.field private static final CONNECTOR_TYPE:Ljava/lang/String;


# instance fields
.field private final atHomeMediaRouter:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

.field private final brokerManager:Landroid/support/place/api/broker/BrokerManager;

.field private listener:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;

.field private final mediaRouter:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/youtube/videos/athome/AtHomeMoviesConnector;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->CONNECTOR_TYPE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Landroid/support/place/api/broker/BrokerManager;->getInstance(Landroid/content/Context;)Landroid/support/place/api/broker/BrokerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->brokerManager:Landroid/support/place/api/broker/BrokerManager;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat;->forApplication(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->mediaRouter:Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->forApplication(Landroid/content/Context;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->atHomeMediaRouter:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->atHomeMediaRouter:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    sget-object v1, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->CONNECTOR_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->setConnectorType(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->atHomeMediaRouter:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->setApplicationId(Ljava/lang/String;)V

    return-void
.end method

.method private notifyCurrentRouteSelected(Z)V
    .locals 3
    .param p1    # Z

    const/high16 v2, 0x800000

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->mediaRouter:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/android/athome/picker/media/MediaRouterCompat;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->mediaRouter:Ljava/lang/Object;

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->onRouteSelected(Ljava/lang/Object;ILjava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->mediaRouter:Ljava/lang/Object;

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->onRouteUnselected(Ljava/lang/Object;ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public onRouteSelected(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "routeInfo: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->atHomeMediaRouter:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-virtual {v3, p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getReceiver(Ljava/lang/Object;)Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->getConnectors()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/connector/ConnectorInfo;

    sget-object v3, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->CONNECTOR_TYPE:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/support/place/connector/ConnectorInfo;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->listener:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->brokerManager:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v4}, Landroid/support/place/api/broker/BrokerManager;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v4

    invoke-static {p3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getName(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v1, v5}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;->onRouteSelected(Landroid/support/place/connector/Broker;Landroid/support/place/connector/ConnectorInfo;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const-string v3, "Route was selected but there was no valid connector"

    invoke-static {v3}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->listener:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;

    invoke-interface {v3}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;->onRouteUnselected()V

    goto :goto_0
.end method

.method public onRouteUnselected(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    const/high16 v0, 0x800000

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->listener:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;->onRouteUnselected()V

    :cond_0
    return-void
.end method

.method public register(Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->listener:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->mediaRouter:Ljava/lang/Object;

    const/high16 v1, 0x800000

    invoke-static {v0, v1, p0}, Lcom/android/athome/picker/media/MediaRouterCompat;->addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->notifyCurrentRouteSelected(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->atHomeMediaRouter:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-virtual {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->start()V

    return-void
.end method

.method public unregister()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->atHomeMediaRouter:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-virtual {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->stop()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->notifyCurrentRouteSelected(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->mediaRouter:Ljava/lang/Object;

    invoke-static {v0, p0}, Lcom/android/athome/picker/media/MediaRouterCompat;->removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/videos/athome/client/MediaRouteManager;->listener:Lcom/google/android/youtube/videos/athome/client/MediaRouteManager$Listener;

    return-void
.end method
