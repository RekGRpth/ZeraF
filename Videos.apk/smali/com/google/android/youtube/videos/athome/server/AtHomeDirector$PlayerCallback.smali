.class final Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;
.super Ljava/lang/Object;
.source "AtHomeDirector.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PlayerCallback"
.end annotation


# instance fields
.field private started:Z

.field final synthetic this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;
    .param p2    # Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;-><init>(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    return-void
.end method

.method private computeAndLogErrorMessage(II)Landroid/util/Pair;
    .locals 10
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x1

    const v8, 0x7f0a0110

    const/4 v3, 0x1

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->networkStatus:Lcom/google/android/youtube/core/utils/NetworkStatus;
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$2000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/utils/NetworkStatus;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    const v8, 0x7f0a00ac

    const/4 v4, 0x0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$2100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z
    invoke-static {v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Z

    move-result v5

    if-eqz v5, :cond_11

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;
    invoke-static {v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1500(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/model/VideoStreams;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->hi:Lcom/google/android/youtube/core/model/Stream;

    iget v5, v5, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    :goto_1
    move v6, p1

    move v7, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/youtube/videos/logging/EventLogger;->onPlaybackError(Ljava/lang/String;ZZIIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1400(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Landroid/app/Activity;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v0, v8, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v9, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_1
    if-ne p1, v2, :cond_0

    const/4 v4, 0x1

    const v8, 0x7f0a0111

    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_AUTHENTICATION_FAILURE:I

    if-ne p2, v0, :cond_2

    const v8, 0x7f0a0080

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_STREAMING_UNAVAILABLE:I

    if-ne p2, v0, :cond_3

    const v8, 0x7f0a0089

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CONCURRENT_PLAYBACK:I

    if-ne p2, v0, :cond_4

    const v8, 0x7f0a0087

    goto :goto_0

    :cond_4
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_TERMINATE_REQUESTED:I

    if-ne p2, v0, :cond_5

    const v8, 0x7f0a0113

    goto :goto_0

    :cond_5
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    if-ne p2, v0, :cond_6

    const v8, 0x7f0a008f

    const/4 v3, 0x0

    goto :goto_0

    :cond_6
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_UNUSUAL_ACTIVITY:I

    if-ne p2, v0, :cond_7

    const v8, 0x7f0a008e

    const/4 v3, 0x0

    goto :goto_0

    :cond_7
    sget v0, Lcom/google/android/youtube/core/player/YouTubePlayer;->ERROR_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    if-ne p2, v0, :cond_8

    const v8, 0x7f0a0086

    const/4 v3, 0x0

    goto :goto_0

    :cond_8
    const/16 v0, -0x7d2

    if-eq p2, v0, :cond_9

    const/16 v0, -0x7d1

    if-ne p2, v0, :cond_a

    :cond_9
    const v8, 0x7f0a0083

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_a
    const/16 v0, -0x3ec

    if-ne p2, v0, :cond_b

    const v8, 0x7f0a0113

    goto/16 :goto_0

    :cond_b
    const/16 v0, -0x3e81

    if-ne p2, v0, :cond_c

    const v8, 0x7f0a00ac

    goto/16 :goto_0

    :cond_c
    const/16 v0, -0x3ea

    if-eq p2, v0, :cond_d

    const/16 v0, -0x3eb

    if-ne p2, v0, :cond_e

    :cond_d
    const v8, 0x7f0a0112

    goto/16 :goto_0

    :cond_e
    const/16 v0, 0x20

    if-eq p2, v0, :cond_f

    const/16 v0, -0x3ed

    if-ne p2, v0, :cond_10

    :cond_f
    const v8, 0x7f0a0113

    goto/16 :goto_0

    :cond_10
    const/16 v0, -0x3f2

    if-ne p2, v0, :cond_0

    const v8, 0x7f0a0114

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_11
    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;
    invoke-static {v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1500(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/model/VideoStreams;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/youtube/core/model/VideoStreams;->lo:Lcom/google/android/youtube/core/model/Stream;

    iget v5, v5, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    goto/16 :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 11
    .param p1    # Landroid/os/Message;

    const/4 v6, 0x3

    const/4 v10, 0x4

    const/4 v9, 0x1

    const/4 v8, 0x2

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$500(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    move-result-object v4

    sget-object v5, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->PLAYER_LOADED:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    if-ne v4, v5, :cond_0

    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->started:Z

    if-nez v4, :cond_4

    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_1

    :cond_1
    :goto_1
    return v9

    :pswitch_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackPlay()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$700(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/athome/server/IdleTimeout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/athome/server/IdleTimeout;->onBusy()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setPlaying()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget v4, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-eq v4, v8, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iput v8, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackSuspended()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$700(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/athome/server/IdleTimeout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/athome/server/IdleTimeout;->onIdle()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showPaused()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->hideSubtitles()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget v4, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iput v7, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackPaused()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$700(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/athome/server/IdleTimeout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/athome/server/IdleTimeout;->onIdle()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showPaused()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->freezeSubtitles()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget v4, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-eq v4, v6, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iput v6, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    goto/16 :goto_0

    :pswitch_3
    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget v0, p1, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    int-to-long v5, v1

    invoke-interface {v4, v5, v6}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackProgress(J)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-result-object v4

    invoke-virtual {v4, v1, v2, v0}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setTimes(III)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->updatePlayerStateTimes(III)V
    invoke-static {v4, v1, v2, v0}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1200(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;III)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/YouTubePlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->syncSubtitles(I)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackEnded()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$700(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/athome/server/IdleTimeout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/athome/server/IdleTimeout;->onIdle()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    sget-object v5, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->PLAYER_ENDED:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    # setter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;
    invoke-static {v4, v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$502(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;)Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->hideSubtitles()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->activity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1400(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget v4, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iput v7, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->idleTimeout:Lcom/google/android/youtube/athome/server/IdleTimeout;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$700(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/athome/server/IdleTimeout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/athome/server/IdleTimeout;->onIdle()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    sget-object v5, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;->PLAYER_ERROR:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    # setter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->state:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;
    invoke-static {v4, v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$502(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;)Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$State;

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->computeAndLogErrorMessage(II)Landroid/util/Pair;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->hideSubtitles()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-result-object v6

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    iget-object v5, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v6, v4, v5}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->showErrorMessage(Ljava/lang/String;Z)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget v4, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-ne v4, v10, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget-object v5, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->errorMessage:Ljava/lang/String;

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v5, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget-boolean v5, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->canRetry:Z

    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eq v5, v4, :cond_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iput v10, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v5

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    iput-object v4, v5, Lcom/google/android/youtube/videos/athome/PlayerState;->errorMessage:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v5

    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, v5, Lcom/google/android/youtube/videos/athome/PlayerState;->canRetry:Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setLoading()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->subtitlesOverlayHelper:Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1100(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/SubtitlesOverlayHelper;->freezeSubtitles()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->videoStreams:Lcom/google/android/youtube/core/model/VideoStreams;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1500(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/model/VideoStreams;

    move-result-object v4

    iget-boolean v4, v4, Lcom/google/android/youtube/core/model/VideoStreams;->supportsQualityToggle:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # ++operator for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->bufferingCount:I
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1704(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)I

    move-result v4

    if-ne v4, v10, :cond_3

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/logging/EventLogger;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/videos/logging/EventLogger;->onQualityDropped()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # setter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z
    invoke-static {v4, v7}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1602(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;Z)Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->hq:Z
    invoke-static {v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setHQ(Z)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    iget-object v5, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->player:Lcom/google/android/youtube/core/player/YouTubePlayer;
    invoke-static {v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1300(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/YouTubePlayer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/youtube/core/player/YouTubePlayer;->getCurrentPositionMillis()I

    move-result v5

    # setter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->resumeTimeMillis:I
    invoke-static {v4, v5}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1902(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;I)I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    invoke-virtual {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playVideo()V

    :cond_3
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget v4, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-eq v4, v9, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iput v9, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->controllerOverlay:Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$800(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/overlay/RemoteControllerOverlay;->setPlaying()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget v4, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-eq v4, v8, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iput v8, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->statsClient:Lcom/google/android/youtube/core/client/VideoStats2Client;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$600(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->arg1:I

    int-to-long v5, v5

    invoke-interface {v4, v5, v6}, Lcom/google/android/youtube/core/client/VideoStats2Client;->onPlaybackSeek(J)V

    goto/16 :goto_0

    :pswitch_9
    iput-boolean v9, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->started:Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget v4, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-eq v4, v8, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iput v8, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    goto/16 :goto_1

    :cond_4
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_2

    :pswitch_a
    goto/16 :goto_1

    :pswitch_b
    iput-boolean v7, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->started:Z

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iget v4, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # getter for: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->playerState:Lcom/google/android/youtube/videos/athome/PlayerState;
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$900(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)Lcom/google/android/youtube/videos/athome/PlayerState;

    move-result-object v4

    iput v7, v4, Lcom/google/android/youtube/videos/athome/PlayerState;->state:I

    iget-object v4, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->this$0:Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;

    # invokes: Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->notifyPlayerStateChanged()V
    invoke-static {v4}, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;->access$1000(Lcom/google/android/youtube/videos/athome/server/AtHomeDirector;)V

    goto/16 :goto_1

    :pswitch_c
    iput-boolean v7, p0, Lcom/google/android/youtube/videos/athome/server/AtHomeDirector$PlayerCallback;->started:Z

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_9
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x4
        :pswitch_b
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
