.class public final Lcom/google/android/youtube/videos/async/GDataRequests;
.super Ljava/lang/Object;
.source "GDataRequests.java"


# static fields
.field private static volatile config:Lcom/google/android/youtube/videos/Config;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static appendCountryRestrictionParameter(Landroid/net/Uri$Builder;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/net/Uri$Builder;
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "restriction"

    invoke-virtual {p0, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    return-void
.end method

.method private static appendPageParameters(Landroid/net/Uri$Builder;II)V
    .locals 2
    .param p0    # Landroid/net/Uri$Builder;
    .param p1    # I
    .param p2    # I

    const-string v0, "start-index"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v0, "max-results"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    return-void
.end method

.method private static appendPaidContentParameters(Landroid/net/Uri$Builder;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/net/Uri$Builder;
    .param p1    # Ljava/lang/String;

    const-string v0, "paid-content"

    const-string v1, "true"

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    if-eqz p1, :cond_0

    const-string v0, "region"

    invoke-virtual {p0, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    return-void
.end method

.method public static getDevicePrivilegesForChannelRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v1, "channel cannot be empty"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequest;->HTTPS_BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "devices"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "privilege"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "model"

    invoke-virtual {v1, v2, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "manufacturer"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "design"

    invoke-virtual {v1, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "uploader"

    invoke-virtual {v1, v2, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    return-object v1
.end method

.method public static getMostPopularShowsRequest(I)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 5
    .param p0    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequest;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "charts"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "shows"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "most_popular"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "show-type"

    const-string v4, "tv"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2, p0}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendPageParameters(Landroid/net/Uri$Builder;II)V

    sget-object v2, Lcom/google/android/youtube/videos/async/GDataRequests;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendCountryRestrictionParameter(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendPaidContentParameters(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    return-object v2
.end method

.method public static getMyPurchasesRequest(Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p1    # Z

    invoke-static {p0, p1}, Lcom/google/android/youtube/videos/async/GDataRequests;->getMyPurchasesUriBuilder(Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x19

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendPageParameters(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    return-object v1
.end method

.method private static getMyPurchasesUriBuilder(Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Landroid/net/Uri$Builder;
    .locals 3
    .param p0    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p1    # Z

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequest;->HTTPS_BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "users"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "default"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "purchases"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    sget-object v1, Lcom/google/android/youtube/videos/async/GDataRequests;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendCountryRestrictionParameter(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getMySeasonPurchasesRequest(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Z

    invoke-static {p1, p2}, Lcom/google/android/youtube/videos/async/GDataRequests;->getMyPurchasesUriBuilder(Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "season"

    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const/4 v1, 0x1

    const/16 v2, 0x19

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendPageParameters(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    return-object v1
.end method

.method public static getMyVideoPurchasesRequest(Ljava/lang/String;Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/android/youtube/videos/accounts/UserAuth;
    .param p2    # Z

    invoke-static {p1, p2}, Lcom/google/android/youtube/videos/async/GDataRequests;->getMyPurchasesUriBuilder(Lcom/google/android/youtube/videos/accounts/UserAuth;Z)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const/4 v1, 0x1

    const/16 v2, 0x19

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendPageParameters(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    return-object v1
.end method

.method public static getSeasonEpisodesRequest(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v1, "seasonId cannot be empty"

    invoke-static {p0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequest;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "seasons"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "episodes"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    return-object v1
.end method

.method public static getSeasonRequest(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v1, "seasonId cannot be empty"

    invoke-static {p0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequest;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "seasons"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    return-object v1
.end method

.method public static getShowRequest(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v1, "showId cannot be empty"

    invoke-static {p0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequest;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "shows"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/videos/async/GDataRequests;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendCountryRestrictionParameter(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    return-object v1
.end method

.method public static getSuggestedMoviesRequest(Landroid/net/Uri;I)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Landroid/net/Uri;
    .param p1    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v2, "uri cannot be null"

    invoke-static {p0, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2, p1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendPageParameters(Landroid/net/Uri$Builder;II)V

    sget-object v2, Lcom/google/android/youtube/videos/async/GDataRequests;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendCountryRestrictionParameter(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendPaidContentParameters(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    return-object v2
.end method

.method public static getTrendingMoviesRequest(I)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 4
    .param p0    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequest;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "charts"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "movies"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "trending"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2, p0}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendPageParameters(Landroid/net/Uri$Builder;II)V

    sget-object v2, Lcom/google/android/youtube/videos/async/GDataRequests;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendCountryRestrictionParameter(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendPaidContentParameters(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    return-object v2
.end method

.method public static getVideoRequest(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v1, "videoId cannot be empty"

    invoke-static {p0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequest;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "videos"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/videos/async/GDataRequests;->config:Lcom/google/android/youtube/videos/Config;

    invoke-interface {v1}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/async/GDataRequests;->appendCountryRestrictionParameter(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/async/GDataRequest;->create(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    return-object v1
.end method

.method public static setConfig(Lcom/google/android/youtube/videos/Config;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/Config;

    sput-object p0, Lcom/google/android/youtube/videos/async/GDataRequests;->config:Lcom/google/android/youtube/videos/Config;

    return-void
.end method
