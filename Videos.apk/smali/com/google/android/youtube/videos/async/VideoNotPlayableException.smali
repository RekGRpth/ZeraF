.class public Lcom/google/android/youtube/videos/async/VideoNotPlayableException;
.super Ljava/lang/Exception;
.source "VideoNotPlayableException.java"


# instance fields
.field public final state:Lcom/google/android/youtube/core/model/Video$State;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/model/Video$State;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/core/model/Video$State;

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const-string v0, "state cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video$State;

    iput-object v0, p0, Lcom/google/android/youtube/videos/async/VideoNotPlayableException;->state:Lcom/google/android/youtube/core/model/Video$State;

    return-void
.end method
