.class Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;
.super Ljava/lang/Object;
.source "BitmapCachingRequester.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/async/BitmapCachingRequester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<TR;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/async/BitmapCachingRequester;

.field final synthetic val$cacheKey:Ljava/lang/String;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$request:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/async/BitmapCachingRequester;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->this$0:Lcom/google/android/youtube/videos/async/BitmapCachingRequester;

    iput-object p2, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->val$cacheKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p4, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->val$request:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->val$request:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p2    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->this$0:Lcom/google/android/youtube/videos/async/BitmapCachingRequester;

    # getter for: Lcom/google/android/youtube/videos/async/BitmapCachingRequester;->cache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    invoke-static {v0}, Lcom/google/android/youtube/videos/async/BitmapCachingRequester;->access$000(Lcom/google/android/youtube/videos/async/BitmapCachingRequester;)Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->val$cacheKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->val$request:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/async/BitmapCachingRequester$1;->onResponse(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    return-void
.end method
