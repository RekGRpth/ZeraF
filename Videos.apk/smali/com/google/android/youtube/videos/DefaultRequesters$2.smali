.class Lcom/google/android/youtube/videos/DefaultRequesters$2;
.super Lcom/google/android/youtube/videos/async/BitmapCachingRequester;
.source "DefaultRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/DefaultRequesters;->createBitmapRequester()Lcom/google/android/youtube/core/async/Requester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/async/BitmapCachingRequester",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/DefaultRequesters;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/DefaultRequesters;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V
    .locals 0
    .param p3    # Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    iput-object p1, p0, Lcom/google/android/youtube/videos/DefaultRequesters$2;->this$0:Lcom/google/android/youtube/videos/DefaultRequesters;

    invoke-direct {p0, p2, p3}, Lcom/google/android/youtube/videos/async/BitmapCachingRequester;-><init>(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/utils/BitmapLruCache;)V

    return-void
.end method


# virtual methods
.method public toCacheKey(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".gb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toCacheKey(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/DefaultRequesters$2;->toCacheKey(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
