.class Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$1;
.super Ljava/lang/Object;
.source "GcmRegistrationManager.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;-><init>(Landroid/content/Context;ZLcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/api/GcmRegisterRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$1;->this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/api/GcmRegisterRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/api/GcmRegisterRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$1;->this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    iget-object v1, p1, Lcom/google/android/youtube/videos/api/GcmRegisterRequest;->account:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->clearRegistrationId(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->access$100(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/api/GcmRegisterRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$1;->onError(Lcom/google/android/youtube/videos/api/GcmRegisterRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/api/GcmRegisterRequest;Ljava/lang/Void;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/api/GcmRegisterRequest;
    .param p2    # Ljava/lang/Void;

    iget-object v0, p0, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$1;->this$0:Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;

    iget-object v1, p1, Lcom/google/android/youtube/videos/api/GcmRegisterRequest;->account:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/videos/api/GcmRegisterRequest;->registrationId:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->setRegistrationId(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;->access$000(Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/api/GcmRegisterRequest;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/gcm/GcmRegistrationManager$1;->onResponse(Lcom/google/android/youtube/videos/api/GcmRegisterRequest;Ljava/lang/Void;)V

    return-void
.end method
