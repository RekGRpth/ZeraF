.class Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SimpleGridItem"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

.field view:Landroid/view/View;

.field views:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)V
    .locals 1
    .param p2    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public getGridLayoutParams()Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    return-object v0
.end method

.method public getMeasuredHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getMeasuredWidth()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public getViews()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    return-object v0
.end method

.method public gridLayout(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public gridMeasure(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method public isGone()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
