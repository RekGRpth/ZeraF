.class final Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;
.super Ljava/lang/Object;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SendFeedbackRequester"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<",
        "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountManager:Landroid/accounts/AccountManager;

.field private final cookieStore:Lorg/apache/http/client/CookieStore;

.field private final feedbackSubmitter:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final tokenRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;Lorg/apache/http/client/CookieStore;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 0
    .param p1    # Landroid/accounts/AccountManager;
    .param p2    # Lorg/apache/http/client/CookieStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManager;",
            "Lorg/apache/http/client/CookieStore;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;",
            ">;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->cookieStore:Lorg/apache/http/client/CookieStore;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->tokenRequester:Lcom/google/android/youtube/core/async/Requester;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->feedbackSubmitter:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;ZLcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;
    .param p1    # Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;
    .param p2    # Z
    .param p3    # Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;
    .param p4    # Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->onGotAutoLoginHeaderOrToken(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;ZLcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method private onGotAutoLoginHeaderOrToken(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;ZLcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;
    .param p2    # Z
    .param p3    # Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Z",
            "Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p3}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;->hasToken()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->feedbackSubmitter:Lcom/google/android/youtube/core/async/Requester;

    new-instance v1, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;

    iget-object v2, p3, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;->token:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;-><init>(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;

    invoke-direct {v2, p0, p4, p1}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p3, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;->autoLoginHeader:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p4}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->performAutoLogin(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Could not get GF token; asked to log in twice"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p4, p1, v0}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private performAutoLogin(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 17
    .param p1    # Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    const-string v3, "&"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    const/16 v16, 0x0

    const/4 v10, 0x0

    const/4 v14, 0x0

    :goto_0
    array-length v3, v12

    if-ge v14, v3, :cond_2

    aget-object v15, v12, v14

    const-string v3, "realm="

    invoke-virtual {v15, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x6

    invoke-virtual {v15, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    :cond_0
    :goto_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_1
    const-string v3, "args="

    invoke-virtual {v15, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x5

    invoke-virtual {v15, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    :cond_2
    const-string v3, "com.google"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    new-instance v3, Ljava/lang/Exception;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error reading auto-login header "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v3}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_2
    return-void

    :cond_4
    new-instance v4, Landroid/accounts/Account;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;->getAccount()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.google"

    invoke-direct {v4, v3, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->accountManager:Landroid/accounts/AccountManager;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "weblogin:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v9

    :try_start_0
    invoke-interface {v9}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    const-string v5, "authtoken"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Ljava/lang/Exception;

    const-string v5, "Cannot authenticate user"

    invoke-direct {v3, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v3}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_2

    :catch_0
    move-exception v13

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v13}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_2

    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->requestToken(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->accountManager:Landroid/accounts/AccountManager;

    const-string v5, "com.google"

    invoke-virtual {v3, v5, v11}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    :catch_1
    move-exception v13

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v13}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_2

    :catch_2
    move-exception v13

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v13}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method private requestToken(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->tokenRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v1, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;

    invoke-direct {v1, p0, p1, p3}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v0, p2, v1}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method


# virtual methods
.method public request(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->cookieStore:Lorg/apache/http/client/CookieStore;

    invoke-interface {v0}, Lorg/apache/http/client/CookieStore;->clear()V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->requestToken(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->request(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
