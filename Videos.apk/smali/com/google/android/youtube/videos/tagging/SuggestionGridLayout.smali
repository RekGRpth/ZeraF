.class public Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
.super Landroid/view/ViewGroup;
.source "SuggestionGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$1;,
        Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;,
        Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;,
        Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;,
        Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;,
        Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;
    }
.end annotation


# instance fields
.field private final mAnimatingViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mChangeAppearingAnimator:Landroid/animation/Animator;

.field private mColCount:I

.field private mColWidth:I

.field private mContentWidth:I

.field private final mGridItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;",
            ">;"
        }
    .end annotation
.end field

.field private mHitRect:Landroid/graphics/Rect;

.field private mHorizontalItemMargin:I

.field private mIsDragging:Z

.field private mItemBottoms:[I

.field private mLayoutTransition:Landroid/animation/LayoutTransition;

.field private mMaxColumnWidth:I

.field private mOnDismissListener:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;

.field mReinitStackBitmap:Z

.field private mSwiper:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

.field private mVerticalItemMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v7, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mIsDragging:Z

    iput-boolean v7, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mReinitStackBitmap:Z

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    sget-object v4, Lcom/google/android/videos/R$styleable;->SuggestionGridLayout:[I

    invoke-virtual {p1, p2, v4, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const v4, 0x7fffffff

    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    invoke-virtual {v0, v7, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    const/4 v4, 0x3

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mHorizontalItemMargin:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setClipToPadding(Z)V

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setClipChildren(Z)V

    invoke-virtual {p0, v5}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setChildrenDrawingOrderEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->configureTransition(Landroid/animation/LayoutTransition;)V

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    invoke-virtual {v4, v7}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mChangeAppearingAnimator:Landroid/animation/Animator;

    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->density:F

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    new-instance v4, Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    new-instance v5, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;

    invoke-direct {v5, p0, v6}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;-><init>(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$1;)V

    int-to-float v6, v2

    invoke-direct {v4, v7, v5, v1, v6}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;-><init>(ILcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;FF)V

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    return-void

    :cond_0
    iput-object v6, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mChangeAppearingAnimator:Landroid/animation/Animator;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Z
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mIsDragging:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mIsDragging:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Landroid/graphics/Rect;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$501(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method private addNewCardsToDeal(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    invoke-interface {p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private configureTransition(Landroid/animation/LayoutTransition;)V
    .locals 7
    .param p1    # Landroid/animation/LayoutTransition;

    const/4 v6, 0x0

    const/4 v5, 0x2

    const-string v1, "translationY"

    new-array v2, v5, [F

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v6, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {p1, v5, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v5, v1, v2}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    return-void
.end method

.method private getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private setupLayoutParams(Landroid/view/View;I)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    if-lt p2, v2, :cond_0

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Column exceeds column count."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v1, v0

    :goto_0
    check-cast v1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    const/4 v2, -0x2

    if-eq p2, v2, :cond_2

    iput p2, v1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    :cond_2
    iget-boolean v2, v1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    if-eqz v2, :cond_3

    iget v2, v1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    const-string v2, "SGL: only spanAllColumns views can have no padding"

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v1

    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v2, -0x2

    invoke-direct {p0, p1, v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;I)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    new-instance v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;-><init>(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)V

    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->addNewCardsToDeal(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;)V

    invoke-super {p0, p1, p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public addViewToColumn(Landroid/view/View;I)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;I)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    new-instance v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;-><init>(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->addNewCardsToDeal(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;)V

    invoke-super {p0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dismissView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "View is not a child of this SuggestionGridLayout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->cancelOngoingDrag()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->dismissChild(Landroid/view/View;F)V

    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/view/View;
    .param p3    # J

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v2

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_0
    return v2
.end method

.method public generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 7
    .param p1    # I
    .param p2    # I

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return p2

    :cond_0
    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v1, p1, v0

    if-lt p2, v1, :cond_1

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    sub-int v6, p2, v1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->indexOfChild(Landroid/view/View;)I

    move-result p2

    goto :goto_0

    :cond_1
    move v3, p2

    const/4 v2, 0x0

    :goto_1
    if-gt v2, v3, :cond_3

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v3, v3, 0x1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move p2, v3

    goto :goto_0
.end method

.method public getColumnCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    return v0
.end method

.method public getMaxColumnWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v10, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v11

    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([II)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getMeasuredWidth()I

    move-result v10

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mContentWidth:I

    sub-int/2addr v10, v11

    div-int/lit8 v0, v10, 0x2

    iget-object v10, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_4

    iget-object v10, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v7

    iget v10, v7, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_0

    const/4 v4, 0x0

    :goto_1
    iget-boolean v10, v7, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    if-eqz v10, :cond_1

    const/4 v6, 0x0

    :goto_2
    iget-object v10, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    aget v10, v10, v4

    iget v11, v7, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->topMargin:I

    add-int v9, v10, v11

    invoke-interface {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->isGone()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v2, v6, v9, v6, v9}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->gridLayout(IIII)V

    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    iget v4, v7, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    goto :goto_1

    :cond_1
    iget v10, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColWidth:I

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mHorizontalItemMargin:I

    add-int/2addr v10, v11

    mul-int/2addr v10, v4

    add-int v6, v0, v10

    goto :goto_2

    :cond_2
    invoke-interface {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getMeasuredWidth()I

    move-result v10

    add-int v8, v6, v10

    invoke-interface {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v10

    add-int v1, v9, v10

    invoke-interface {v2, v6, v9, v8, v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->gridLayout(IIII)V

    iget v10, v7, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_3

    iget-object v10, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    iget v11, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    add-int/2addr v11, v1

    iget v12, v7, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v11, v12

    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([II)V

    goto :goto_3

    :cond_3
    iget-object v10, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    iget v11, v7, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    iget v12, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    add-int/2addr v12, v1

    iget v13, v7, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v12, v13

    aput v12, v10, v11

    goto :goto_3

    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 28
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v22

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v12

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v23

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v13

    const/16 v21, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mHorizontalItemMargin:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, -0x1

    mul-int v16, v24, v25

    sparse-switch v22, :sswitch_data_0

    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v24, v0

    mul-int v24, v24, v7

    add-int v24, v24, v16

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mContentWidth:I

    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColWidth:I

    const/high16 v24, 0x40000000

    move/from16 v0, v24

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mContentWidth:I

    move/from16 v24, v0

    const/high16 v25, 0x40000000

    invoke-static/range {v24 .. v25}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v24, 0x40000000

    move/from16 v0, v21

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Ljava/util/Arrays;->fill([II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v14, 0x0

    :goto_1
    if-ge v14, v5, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    invoke-interface {v4}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->isGone()Z

    move-result v24

    if-eqz v24, :cond_1

    :cond_0
    :goto_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :sswitch_0
    move/from16 v21, v23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingLeft()I

    move-result v24

    sub-int v24, v21, v24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingRight()I

    move-result v25

    sub-int v18, v24, v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    move/from16 v24, v0

    sub-int v25, v18, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v26, v0

    div-int v25, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(II)I

    move-result v7

    goto/16 :goto_0

    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingLeft()I

    move-result v24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingRight()I

    move-result v25

    add-int v19, v24, v25

    sub-int v18, v23, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    move/from16 v24, v0

    sub-int v25, v18, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v26, v0

    div-int v25, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(II)I

    move-result v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v24, v0

    mul-int v24, v24, v7

    add-int v24, v24, v16

    add-int v21, v24, v19

    goto/16 :goto_0

    :sswitch_2
    new-instance v24, Ljava/lang/IllegalArgumentException;

    const-string v25, "Cannot measure SuggestionGridLayout with mode UNSPECIFIED"

    invoke-direct/range {v24 .. v25}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v24

    :cond_1
    invoke-interface {v4}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v15

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    iget v10, v15, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    const/16 v24, -0x1

    move/from16 v0, v24

    if-ne v10, v0, :cond_3

    iget-boolean v0, v15, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    move/from16 v24, v0

    if-eqz v24, :cond_2

    move v8, v2

    :goto_3
    const/4 v10, 0x0

    :goto_4
    invoke-interface {v4, v8, v6}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->gridMeasure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v24, v0

    aget v25, v24, v10

    iget v0, v15, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->topMargin:I

    move/from16 v26, v0

    invoke-interface {v4}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v27

    add-int v26, v26, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    iget v0, v15, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->bottomMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    add-int v25, v25, v26

    aput v25, v24, v10

    iget v0, v15, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v25, v0

    aget v25, v25, v10

    invoke-static/range {v24 .. v25}, Ljava/util/Arrays;->fill([II)V

    goto/16 :goto_2

    :cond_2
    move v8, v3

    goto :goto_3

    :cond_3
    move/from16 v8, v20

    goto :goto_4

    :cond_4
    move v11, v13

    const/high16 v24, 0x40000000

    move/from16 v0, v24

    if-eq v12, v0, :cond_7

    const/16 v17, 0x0

    const/4 v14, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v14, v0, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v24, v0

    aget v24, v24, v14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mVerticalItemMargin:I

    move/from16 v25, v0

    sub-int v9, v24, v25

    move/from16 v0, v17

    if-le v9, v0, :cond_5

    move/from16 v17, v9

    :cond_5
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingTop()I

    move-result v24

    add-int v24, v24, v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getPaddingBottom()I

    move-result v25

    add-int v11, v24, v25

    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1, v11}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setMeasuredDimension(II)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeAllViews()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public removeGridItem(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mSwiper:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->cancelOngoingDrag()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    invoke-interface {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SGL: removeGridItem with non-grid item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    return-void
.end method

.method public setChangeAppearingAnimatorEnabled(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mChangeAppearingAnimator:Landroid/animation/Animator;

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setColumnCount(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mColCount:I

    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mItemBottoms:[I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setLayoutTransitionsEnabled(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mLayoutTransition:Landroid/animation/LayoutTransition;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMaxColumnWidth(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mMaxColumnWidth:I

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setOnDismissListener(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;

    return-void
.end method
