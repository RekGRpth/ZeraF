.class Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;
.super Landroid/os/AsyncTask;
.source "KnowledgeOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadChunkData(I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/youtube/videos/tagging/ChunkData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

.field final synthetic val$chunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->this$0:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->val$chunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/youtube/videos/tagging/ChunkData;
    .locals 3
    .param p1    # [Ljava/lang/Void;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->this$0:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    # getter for: Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->access$000(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;)Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->val$chunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/tagging/TagStream;->loadChunkData(Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/youtube/videos/tagging/ChunkData;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load chunk for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->val$chunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iget v2, v2, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->val$chunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iget v2, v2, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->endMillis:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->doInBackground([Ljava/lang/Void;)Lcom/google/android/youtube/videos/tagging/ChunkData;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/youtube/videos/tagging/ChunkData;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/ChunkData;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->this$0:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    # getter for: Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->access$100(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;)Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->val$chunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->this$0:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->val$chunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    # setter for: Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->access$202(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->this$0:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    # setter for: Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkData:Lcom/google/android/youtube/videos/tagging/ChunkData;
    invoke-static {v0, p1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->access$302(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/ChunkData;)Lcom/google/android/youtube/videos/tagging/ChunkData;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->this$0:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->access$102(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->this$0:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    # getter for: Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledgeAtMillis:I
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->access$400(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->this$0:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    # invokes: Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledge()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->access$500(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/ChunkData;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->onPostExecute(Lcom/google/android/youtube/videos/tagging/ChunkData;)V

    return-void
.end method
