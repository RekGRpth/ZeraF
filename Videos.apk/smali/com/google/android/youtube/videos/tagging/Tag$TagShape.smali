.class public interface abstract Lcom/google/android/youtube/videos/tagging/Tag$TagShape;
.super Ljava/lang/Object;
.source "Tag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/Tag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TagShape"
.end annotation


# virtual methods
.method public abstract draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
.end method

.method public abstract getBoundingBox(Landroid/graphics/RectF;)V
.end method

.method public abstract hitTest(FFF)Z
.end method
