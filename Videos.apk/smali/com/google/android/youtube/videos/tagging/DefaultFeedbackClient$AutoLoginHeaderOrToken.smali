.class final Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;
.super Ljava/lang/Object;
.source "DefaultFeedbackClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AutoLoginHeaderOrToken"
.end annotation


# instance fields
.field public final autoLoginHeader:Ljava/lang/String;

.field public final token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;->autoLoginHeader:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;->token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public hasToken()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;->token:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
