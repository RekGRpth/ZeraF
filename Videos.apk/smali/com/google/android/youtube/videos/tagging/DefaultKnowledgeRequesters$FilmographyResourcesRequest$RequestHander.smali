.class public final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;
.super Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestHander"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
        ">;",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createComponent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;->createComponent(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;[B)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public createComponent(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;[B)Ljava/util/Map;
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "[B)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p2}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->parseFrom([B)Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->getResourceCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    invoke-virtual {v3, v0}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->getResource(I)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasMid()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    const-string v5, "Stored AssetResource has no MID"

    invoke-direct {v4, v5}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->getMid()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public createUnderlyingRequest(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;[B)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2    # [B

    new-instance v0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    iget-object v1, v1, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->account:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;->userCountry:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;->filmographyIds:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic createUnderlyingRequest(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest$RequestHander;->createUnderlyingRequest(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;[B)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    move-result-object v0

    return-object v0
.end method
