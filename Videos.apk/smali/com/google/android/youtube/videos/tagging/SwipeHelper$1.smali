.class Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SwipeHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/tagging/SwipeHelper;->dismissChild(Landroid/view/View;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

.field final synthetic val$canAnimViewBeDismissed:Z

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/tagging/SwipeHelper;Landroid/view/View;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;->this$0:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;->val$view:Landroid/view/View;

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;->val$canAnimViewBeDismissed:Z

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1    # Landroid/animation/Animator;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;->this$0:Lcom/google/android/youtube/videos/tagging/SwipeHelper;

    # getter for: Lcom/google/android/youtube/videos/tagging/SwipeHelper;->mCallback:Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/SwipeHelper;->access$000(Lcom/google/android/youtube/videos/tagging/SwipeHelper;)Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;->val$view:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;->onChildDismissed(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;->val$canAnimViewBeDismissed:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;->val$view:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;->val$view:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/SwipeHelper$1;->val$view:Landroid/view/View;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    return-void
.end method
