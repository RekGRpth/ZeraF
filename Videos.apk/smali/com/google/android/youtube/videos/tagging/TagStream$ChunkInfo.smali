.class public Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
.super Ljava/lang/Object;
.source "TagStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/TagStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChunkInfo"
.end annotation


# instance fields
.field public final byteCount:I

.field public final byteOffset:I

.field public final endMillis:I

.field public final startMillis:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    iput p2, p0, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->endMillis:I

    iput p3, p0, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->byteOffset:I

    iput p4, p0, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->byteCount:I

    return-void
.end method
