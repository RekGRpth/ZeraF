.class Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$1;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
        "Lcom/google/android/youtube/videos/tagging/TagStreamParser;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;

.field final synthetic val$this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$1;->this$1:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$1;->val$this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$1;->this$1:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;

    # invokes: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onTagStreamRequestError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->access$100(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$1;->onError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/TagStreamParser;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2    # Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$1;->this$1:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;

    # invokes: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->onTagStreamResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/TagStreamParser;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;->access$000(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/TagStreamParser;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    check-cast p2, Lcom/google/android/youtube/videos/tagging/TagStreamParser;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$Worker$1;->onResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/youtube/videos/tagging/TagStreamParser;)V

    return-void
.end method
