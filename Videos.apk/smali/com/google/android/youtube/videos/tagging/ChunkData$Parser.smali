.class Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;
.super Ljava/lang/Object;
.source "ChunkData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/ChunkData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Parser"
.end annotation


# instance fields
.field private final chunkStartMillis:I

.field private currentKeyframeIndex:I

.field private currentTagCount:I

.field private final keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/util/SparseArrayCompat",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/youtube/videos/tagging/Tag;",
            ">;>;"
        }
    .end annotation
.end field

.field private final lastTagBySplitId:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/youtube/videos/tagging/Tag;",
            ">;"
        }
    .end annotation
.end field

.field private nextKeyframe:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/youtube/videos/tagging/Tag;",
            ">;"
        }
    .end annotation
.end field

.field private offset:I

.field private final tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/TagStream;I)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagStream;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iput p2, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->chunkStartMillis:I

    new-instance v0, Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v0}, Lvedroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    return-void
.end method

.method private parseFrame(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Z)V
    .locals 9
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$Frame;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->chunkStartMillis:I

    if-nez p2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->hasOffset()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getOffset()I

    move-result v1

    :goto_0
    if-gtz v1, :cond_1

    new-instance v5, Lcom/google/protobuf/InvalidProtocolBufferException;

    const-string v6, "Frame invalid - no or non-positive offset"

    invoke-direct {v5, v6}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    move v1, v6

    goto :goto_0

    :cond_1
    iget v7, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->offset:I

    add-int/2addr v7, v1

    iput v7, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->offset:I

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget v8, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->offset:I

    invoke-virtual {v7, v8}, Lcom/google/android/youtube/videos/tagging/TagStream;->toMillis(I)I

    move-result v7

    add-int/2addr v3, v7

    :cond_2
    iget v7, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v8

    if-lt v7, v8, :cond_4

    move v0, v5

    :goto_1
    if-eqz v0, :cond_3

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;

    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    invoke-virtual {v7, v3, v8}, Lvedroid/support/v4/util/SparseArrayCompat;->append(ILjava/lang/Object;)V

    :cond_3
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getTagInCount()I

    move-result v7

    if-ge v2, v7, :cond_5

    invoke-virtual {p1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getTagIn(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    move-result-object v7

    invoke-direct {p0, v7, v3, v0, v5}, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->parseTag(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;IZZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    move v0, v6

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    :goto_3
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getTagOutCount()I

    move-result v5

    if-ge v2, v5, :cond_6

    invoke-virtual {p1, v2}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->getTagOut(I)Lcom/google/android/youtube/videos/proto/TagProtos$Tag;

    move-result-object v5

    invoke-direct {p0, v5, v3, v0, v6}, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->parseTag(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;IZZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    iget v5, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentTagCount:I

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;

    iget v6, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    invoke-virtual {v5, v6}, Lvedroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    :cond_7
    :goto_4
    return-void

    :cond_8
    iget v5, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentTagCount:I

    const/16 v7, 0x32

    if-lt v5, v7, :cond_7

    new-instance v5, Landroid/util/SparseArray;

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v7

    invoke-direct {v5, v7}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v5, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    const/4 v2, 0x0

    :goto_5
    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-ge v2, v5, :cond_a

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/videos/tagging/Tag;

    iget-boolean v5, v4, Lcom/google/android/youtube/videos/tagging/Tag;->interpolates:Z

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    iget v7, v4, Lcom/google/android/youtube/videos/tagging/Tag;->splitId:I

    invoke-virtual {v5, v7, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_a
    iget v5, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    iput v6, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentTagCount:I

    goto :goto_4
.end method

.method private parseTag(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;IZZ)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/proto/TagProtos$Tag;
    .param p2    # I
    .param p3    # Z
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {p1, p2, p4}, Lcom/google/android/youtube/videos/tagging/Tag;->parseFrom(Lcom/google/android/youtube/videos/proto/TagProtos$Tag;IZ)Lcom/google/android/youtube/videos/tagging/Tag;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget v4, v2, Lcom/google/android/youtube/videos/tagging/Tag;->splitId:I

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/videos/tagging/TagStream;->getKnowledgeEntityBySplitId(I)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentTagCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentTagCount:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    iget v4, v2, Lcom/google/android/youtube/videos/tagging/Tag;->splitId:I

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/tagging/Tag;

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    iget v4, v2, Lcom/google/android/youtube/videos/tagging/Tag;->splitId:I

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    if-eqz v1, :cond_2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/tagging/Tag;->setNextTag(Lcom/google/android/youtube/videos/tagging/Tag;)V

    iget-boolean v3, v1, Lcom/google/android/youtube/videos/tagging/Tag;->interpolates:Z

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/tagging/Tag;->setInterpolatesIn(Z)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->currentKeyframeIndex:I

    invoke-virtual {v3, v4}, Lvedroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    if-nez p3, :cond_3

    iget v3, v2, Lcom/google/android/youtube/videos/tagging/Tag;->splitId:I

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v3

    if-gez v3, :cond_0

    :cond_3
    iget v3, v2, Lcom/google/android/youtube/videos/tagging/Tag;->splitId:I

    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/videos/tagging/ChunkData;
    .locals 10
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    iput-object v9, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    const/4 v2, 0x1

    :goto_0
    invoke-static {p1}, Lcom/google/android/youtube/videos/proto/TagProtos$Frame;->parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/videos/proto/TagProtos$Frame;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, v2}, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->parseFrame(Lcom/google/android/youtube/videos/proto/TagProtos$Frame;Z)V

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v9}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v5

    new-array v6, v5, [I

    new-array v7, v5, [[Lcom/google/android/youtube/videos/tagging/Tag;

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v5, :cond_2

    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v9, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v9

    aput v9, v6, v1

    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v9, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v9

    new-array v8, v9, [Lcom/google/android/youtube/videos/tagging/Tag;

    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v9

    if-ge v3, v9, :cond_1

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/youtube/videos/tagging/Tag;

    aput-object v9, v8, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    aput-object v8, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->keyframesByTime:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v9}, Lvedroid/support/v4/util/SparseArrayCompat;->clear()V

    iget-object v9, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->lastTagBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v9}, Landroid/util/SparseArray;->clear()V

    const/4 v9, 0x0

    iput-object v9, p0, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->nextKeyframe:Landroid/util/SparseArray;

    new-instance v9, Lcom/google/android/youtube/videos/tagging/ChunkData;

    invoke-direct {v9, v6, v7}, Lcom/google/android/youtube/videos/tagging/ChunkData;-><init>([I[[Lcom/google/android/youtube/videos/tagging/Tag;)V

    return-object v9
.end method
