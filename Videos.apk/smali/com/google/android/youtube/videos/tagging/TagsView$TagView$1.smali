.class final Lcom/google/android/youtube/videos/tagging/TagsView$TagView$1;
.super Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;
.source "TagsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/TagsView$TagView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;-><init>(Lcom/google/android/youtube/videos/tagging/TagsView$1;)V

    return-void
.end method


# virtual methods
.method protected computeDifference(Lcom/google/android/youtube/videos/tagging/TagsView$TagView;Lcom/google/android/youtube/videos/tagging/TagsView$TagView;)F
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagsView$TagView;
    .param p2    # Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    iget-object v0, p1, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    mul-float/2addr v0, v1

    iget-object v1, p2, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p2, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method
