.class Lcom/google/android/youtube/videos/tagging/CardsView$1;
.super Ljava/lang/Object;
.source "CardsView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/tagging/CardsView;->scrollToInternal(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/CardsView;

.field final synthetic val$finalToScrollTo:I

.field final synthetic val$skipAnimations:Z


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/tagging/CardsView;IZ)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/CardsView$1;->this$0:Lcom/google/android/youtube/videos/tagging/CardsView;

    iput p2, p0, Lcom/google/android/youtube/videos/tagging/CardsView$1;->val$finalToScrollTo:I

    iput-boolean p3, p0, Lcom/google/android/youtube/videos/tagging/CardsView$1;->val$skipAnimations:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView$1;->this$0:Lcom/google/android/youtube/videos/tagging/CardsView;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/CardsView$1;->val$finalToScrollTo:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/tagging/CardsView;->smoothScrollTo(II)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView$1;->this$0:Lcom/google/android/youtube/videos/tagging/CardsView;

    # invokes: Lcom/google/android/youtube/videos/tagging/CardsView;->maybePulseCard()V
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->access$000(Lcom/google/android/youtube/videos/tagging/CardsView;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView$1;->val$skipAnimations:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/CardsView$1;->this$0:Lcom/google/android/youtube/videos/tagging/CardsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/CardsView;->cardHolder:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->access$100(Lcom/google/android/youtube/videos/tagging/CardsView;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->setLayoutTransitionsEnabled(Z)V

    :cond_0
    return-void
.end method
