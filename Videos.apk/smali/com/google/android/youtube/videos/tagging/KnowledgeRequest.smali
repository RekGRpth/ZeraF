.class public final Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
.super Ljava/lang/Object;
.source "KnowledgeRequest.java"


# instance fields
.field public final account:Ljava/lang/String;

.field public final locale:Ljava/util/Locale;

.field public final videoId:Ljava/lang/String;

.field public final videoItag:Ljava/lang/String;

.field public final videoTimestamp:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/util/Locale;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "account cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->account:Ljava/lang/String;

    const-string v0, "videoId cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoItag:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    return-void
.end method

.method public static final createWithCurrentLocale(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/core/model/Stream;

    iget-boolean v0, p3, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v0, :cond_0

    iget v0, p3, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->gdataFormatToItag(I)Ljava/lang/String;

    move-result-object v4

    :goto_0
    new-instance v0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v3, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v1, p3, Lcom/google/android/youtube/core/model/Stream;->extra:Lcom/google/android/youtube/core/model/StreamExtra;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/StreamExtra;->lastModified:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->getVideoTimestamp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    iget-object v0, p3, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->getVideoItag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private static final gdataFormatToItag(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "61"

    goto :goto_0

    :pswitch_2
    const-string v0, "62"

    goto :goto_0

    :pswitch_3
    const-string v0, "63"

    goto :goto_0

    :pswitch_4
    const-string v0, "64"

    goto :goto_0

    :pswitch_5
    const-string v0, "81"

    goto :goto_0

    :pswitch_6
    const-string v0, "88"

    goto :goto_0

    :pswitch_7
    const-string v0, "113"

    goto :goto_0

    :pswitch_8
    const-string v0, "114"

    goto :goto_0

    :pswitch_9
    const-string v0, "119"

    goto :goto_0

    :pswitch_a
    const-string v0, "159"

    goto :goto_0

    :pswitch_b
    const-string v0, "180"

    goto :goto_0

    :pswitch_c
    const-string v0, "186"

    goto :goto_0

    :pswitch_d
    const-string v0, "188"

    goto :goto_0

    :pswitch_e
    const-string v0, "190"

    goto :goto_0

    :pswitch_f
    const-string v0, "192"

    goto :goto_0

    :pswitch_10
    const-string v0, "193"

    goto :goto_0

    :pswitch_11
    const-string v0, "80"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_a
        :pswitch_0
        :pswitch_c
        :pswitch_b
        :pswitch_e
        :pswitch_0
        :pswitch_10
        :pswitch_f
        :pswitch_d
    .end packed-switch
.end method

.method private static final getVideoItag(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v5, -0x1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    const-string v4, "/itag/"

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_0

    const-string v3, "/itag/"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int v2, v1, v3

    const/16 v3, 0x2f

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    if-ne v0, v5, :cond_2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private static final getVideoTimestamp(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-object v3

    :cond_0
    :try_start_0
    invoke-static {p0}, Lorg/apache/http/impl/cookie/DateUtils;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v1, v4, v6

    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/http/impl/cookie/DateParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not turn this Last-Modified value to timestamp: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KnowledgeRequest[videoId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",itag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoItag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",ts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
