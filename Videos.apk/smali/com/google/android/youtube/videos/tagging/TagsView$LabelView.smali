.class final Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;
.super Lcom/google/android/youtube/videos/tagging/TagsView$TagView;
.source "TagsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/TagsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LabelView"
.end annotation


# instance fields
.field private bottomY:F

.field private label:Ljava/lang/String;

.field private final overlaps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;",
            ">;"
        }
    .end annotation
.end field

.field private tagShapeView:Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;

.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/TagsView;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/TagsView;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->overlaps:Ljava/util/List;

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public addOverlappingLabelView(Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->overlaps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected animationScaleFactor()F
    .locals 1

    const/high16 v0, 0x3f800000

    return v0
.end method

.method public dispatchSetActivated(Z)V
    .locals 3
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->dispatchSetActivated(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->tagShapeView:Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->tagShapeView:Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->isActivated()Z

    move-result v1

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->tagShapeView:Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->setActivated(Z)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->setVisibility(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->overlaps:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->overlaps:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected getTaggedKnowledgeEntity()Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->tagShapeView:Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->getTaggedKnowledgeEntity()Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    move-result-object v0

    return-object v0
.end method

.method public init(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;Landroid/graphics/RectF;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;
    .param p2    # Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;
    .param p3    # Landroid/graphics/RectF;

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/youtube/videos/tagging/Tag$TagShape;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/videos/tagging/Tag$TagShape;->getBoundingBox(Landroid/graphics/RectF;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$1200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    iget-object v2, v2, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->textYOffset:F
    invoke-static {v3}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$1300(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->textHeight:F
    invoke-static {v3}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$1400(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    const/high16 v3, 0x40000000

    div-float v3, v0, v3

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v0

    iput v2, v1, Landroid/graphics/RectF;->right:F

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    iget-object v1, v1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->label:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iput v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->bottomY:F

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->tagShapeView:Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->shadowOverflow:F
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$300(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v2

    neg-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->shadowOverflow:F
    invoke-static {v3}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$300(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v3

    neg-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->init(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    return-void
.end method

.method public intersects(Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;)Z
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->rect:Landroid/graphics/RectF;

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->rect:Landroid/graphics/RectF;

    invoke-static {v0, v1}, Landroid/graphics/RectF;->intersects(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$1200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->activatedTagColor:I
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$900(Lcom/google/android/youtube/videos/tagging/TagsView;)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->label:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->centerX:F

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->rect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->bottomY:F

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->rect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->textPaint:Landroid/graphics/Paint;
    invoke-static {v3}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$1200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagColor:I
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$1000(Lcom/google/android/youtube/videos/tagging/TagsView;)I

    move-result v0

    goto :goto_0
.end method

.method public setVisibleIfNoVisibleOverlaps()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->overlaps:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->overlaps:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->setVisibility(I)V

    goto :goto_1
.end method
