.class final Lcom/google/android/youtube/videos/tagging/ActorCards;
.super Lcom/google/android/youtube/videos/tagging/Cards;
.source "ActorCards.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/ActorCards$RequestFeedbackOnClickListener;,
        Lcom/google/android/youtube/videos/tagging/ActorCards$ImageSearchOnClickListener;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/Cards;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;
    .locals 29
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;
    .param p2    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;
    .param p4    # Landroid/app/Activity;
    .param p5    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/videos/store/StoreStatusMonitor;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const v2, 0x7f040009

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;->inflate(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v2, 0x7f07001f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/google/android/youtube/videos/tagging/ActorCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/youtube/videos/tagging/ActorCards;->buildCharacterNamesString(Landroid/content/res/Resources;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const v2, 0x7f070020

    move-object/from16 v0, v16

    invoke-static {v9, v2, v0}, Lcom/google/android/youtube/videos/tagging/ActorCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    if-eqz v2, :cond_1

    const v2, 0x7f07001d

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ImageView;

    const/4 v2, 0x4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const v2, 0x7f0a00ef

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    const v3, 0x7f090037

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-static {v2, v0, v3}, Lcom/google/android/youtube/videos/tagging/ActorCards;->setImageMatrixIfSquareCropExists(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/widget/ImageView;I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-static {v0, v2, v1}, Lcom/google/android/youtube/videos/tagging/ActorCards;->requestImage(Landroid/widget/ImageView;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/youtube/core/async/Requester;)V

    const v2, 0x7f07001e

    const v3, 0x7f0a00f0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    iget-object v6, v6, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->source:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/google/android/youtube/videos/tagging/ActorCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/youtube/videos/tagging/ActorCards$ImageSearchOnClickListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/videos/tagging/ActorCards$ImageSearchOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const/4 v12, 0x0

    const/4 v11, 0x0

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v15

    const/4 v2, 0x1

    invoke-virtual {v15, v2}, Ljava/util/Calendar;->get(I)I

    move-result v24

    const-string v2, "MM-dd"

    invoke-static {v2, v15}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    sub-int v10, v24, v18

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_2

    add-int/lit8 v10, v10, -0x1

    :cond_2
    const v2, 0x7f070021

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f070022

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/google/android/youtube/videos/tagging/ActorCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v12

    :cond_3
    :goto_0
    const/4 v14, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->placeOfBirth:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const v2, 0x7f070024

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f070025

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->placeOfBirth:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/google/android/youtube/videos/tagging/ActorCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v14

    :cond_4
    const v2, 0x7f070026

    const v3, 0x7f0a00f5

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/google/android/youtube/videos/tagging/ActorCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v25

    new-instance v2, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "actorCard"

    move-object/from16 v3, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f070027

    const v3, 0x7f0a00f6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/google/android/youtube/videos/tagging/ActorCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    move-result-object v27

    new-instance v2, Lcom/google/android/youtube/videos/tagging/Cards$WebSearchOnClickListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    const-string v4, "actorCard"

    move-object/from16 v0, p4

    move-object/from16 v1, p7

    invoke-direct {v2, v0, v3, v1, v4}, Lcom/google/android/youtube/videos/tagging/Cards$WebSearchOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {v9}, Lcom/google/android/youtube/videos/tagging/ActorCards;->layout(Landroid/view/View;)V

    const v2, 0x7f090037

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    if-eqz v12, :cond_5

    invoke-static {v12}, Lcom/google/android/youtube/videos/tagging/ActorCards;->getBottom(Landroid/view/View;)I

    move-result v2

    move/from16 v0, v21

    if-le v2, v0, :cond_5

    const/16 v2, 0x8

    invoke-virtual {v12, v2}, Landroid/view/View;->setVisibility(I)V

    const/16 v2, 0x8

    invoke-virtual {v11, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    if-eqz v28, :cond_6

    invoke-static/range {v28 .. v28}, Lcom/google/android/youtube/videos/tagging/ActorCards;->getBottom(Landroid/view/View;)I

    move-result v2

    move/from16 v0, v21

    if-le v2, v0, :cond_6

    const/16 v2, 0x8

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    if-eqz v14, :cond_7

    invoke-static {v14}, Lcom/google/android/youtube/videos/tagging/ActorCards;->getBottom(Landroid/view/View;)I

    move-result v2

    move/from16 v0, v21

    if-le v2, v0, :cond_7

    const/16 v2, 0x8

    invoke-virtual {v14, v2}, Landroid/view/View;->setVisibility(I)V

    const/16 v2, 0x8

    invoke-virtual {v13, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    const v2, 0x7f070028

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    if-nez p2, :cond_9

    const/16 v2, 0x8

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/CardTag;->forActor(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;)Lcom/google/android/youtube/videos/tagging/CardTag;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v9

    :cond_8
    const v2, 0x7f070023

    const v3, 0x7f0a00f3

    const/4 v4, 0x2

    :try_start_1
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/google/android/youtube/videos/tagging/ActorCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v28

    goto/16 :goto_0

    :catch_0
    move-exception v19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reading actor\'s date of birth ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") or death ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v19 .. v19}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reading actor\'s date of birth ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") or death ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v19 .. v19}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    new-instance v2, Lcom/google/android/youtube/videos/tagging/ActorCards$RequestFeedbackOnClickListener;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/videos/tagging/ActorCards$RequestFeedbackOnClickListener;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method
