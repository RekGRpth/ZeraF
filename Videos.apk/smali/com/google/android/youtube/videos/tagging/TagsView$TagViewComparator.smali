.class abstract Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;
.super Ljava/lang/Object;
.source "TagsView.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/TagsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "TagViewComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/youtube/videos/tagging/TagsView$TagView;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/tagging/TagsView$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagsView$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/youtube/videos/tagging/TagsView$TagView;Lcom/google/android/youtube/videos/tagging/TagsView$TagView;)I
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagsView$TagView;
    .param p2    # Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;->computeDifference(Lcom/google/android/youtube/videos/tagging/TagsView$TagView;Lcom/google/android/youtube/videos/tagging/TagsView$TagView;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    check-cast p2, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;->compare(Lcom/google/android/youtube/videos/tagging/TagsView$TagView;Lcom/google/android/youtube/videos/tagging/TagsView$TagView;)I

    move-result v0

    return v0
.end method

.method protected abstract computeDifference(Lcom/google/android/youtube/videos/tagging/TagsView$TagView;Lcom/google/android/youtube/videos/tagging/TagsView$TagView;)F
.end method
