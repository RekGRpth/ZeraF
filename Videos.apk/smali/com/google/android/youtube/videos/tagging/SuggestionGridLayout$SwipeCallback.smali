.class Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/youtube/videos/tagging/SwipeHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SwipeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
    .param p2    # Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$1;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;-><init>(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)V

    return-void
.end method


# virtual methods
.method public canChildBeDismissed(Landroid/view/View;)Z
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    goto :goto_0
.end method

.method dragEnd(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$400(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # setter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v1, v3}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$102(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Z)Z

    const/4 v1, 0x0

    invoke-virtual {p1, v3, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->invalidate()V

    return-void
.end method

.method public getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/view/MotionEvent;

    const/4 v6, 0x0

    const/high16 v8, 0x3f000000

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v7}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$100(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Z

    move-result v7

    if-eqz v7, :cond_0

    move-object v3, v6

    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    add-float/2addr v7, v8

    float-to-int v4, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    add-float/2addr v7, v8

    float-to-int v5, v7

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v7}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_1
    if-ltz v1, :cond_3

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v7, v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_2

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v7}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$200(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v7}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$200(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v7, v3}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v7

    iget-boolean v7, v7, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    if-eqz v7, :cond_1

    instance-of v7, v2, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SimpleGridItem;

    if-eqz v7, :cond_1

    goto :goto_0

    :cond_3
    move-object v3, v6

    goto :goto_0
.end method

.method public onBeginDrag(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$400(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # setter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v1, v3}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$102(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Z)Z

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->invalidate()V

    return-void
.end method

.method public onChildDismissed(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # invokes: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;
    invoke-static {v2, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$300(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$400(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    if-eqz v2, :cond_0

    invoke-interface {v1, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # invokes: Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    invoke-static {v2, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$501(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Landroid/view/View;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$600(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$600(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$OnDismissListener;->onViewDismissed(Landroid/view/View;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v2, v3}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$102(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;Z)Z

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$GridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    # getter for: Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->access$700(Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;->invalidate()V

    return-void
.end method

.method public onDragCancelled(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public onSnapBackCompleted(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$SwipeCallback;->dragEnd(Landroid/view/View;)V

    return-void
.end method
