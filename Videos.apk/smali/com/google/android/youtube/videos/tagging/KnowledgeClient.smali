.class public interface abstract Lcom/google/android/youtube/videos/tagging/KnowledgeClient;
.super Ljava/lang/Object;
.source "KnowledgeClient.java"


# virtual methods
.method public abstract getFeedbackClient()Lcom/google/android/youtube/videos/tagging/FeedbackClient;
.end method

.method public abstract requestKnowledgeBundle(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract requestPinnedKnowledgeBundle(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;",
            ">;)V"
        }
    .end annotation
.end method
