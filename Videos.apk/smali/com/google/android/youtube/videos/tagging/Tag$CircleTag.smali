.class Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;
.super Lcom/google/android/youtube/videos/tagging/Tag;
.source "Tag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/Tag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CircleTag"
.end annotation


# instance fields
.field private final radius:F

.field private final x:F

.field private final y:F


# direct methods
.method private constructor <init>(IIIZ)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    const v1, 0x43ff8000

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p4, v0}, Lcom/google/android/youtube/videos/tagging/Tag;-><init>(IIZLcom/google/android/youtube/videos/tagging/Tag$1;)V

    and-int/lit16 v0, p3, 0x1ff

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->x:F

    shr-int/lit8 v0, p3, 0x9

    and-int/lit16 v0, v0, 0x1ff

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->y:F

    shr-int/lit8 v0, p3, 0x12

    and-int/lit16 v0, v0, 0x1ff

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->radius:F

    return-void
.end method

.method synthetic constructor <init>(IIIZLcom/google/android/youtube/videos/tagging/Tag$1;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .param p5    # Lcom/google/android/youtube/videos/tagging/Tag$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;-><init>(IIIZ)V

    return-void
.end method


# virtual methods
.method public getTagShape(IFFF)Lcom/google/android/youtube/videos/tagging/Tag$TagShape;
    .locals 7
    .param p1    # I
    .param p2    # F
    .param p3    # F
    .param p4    # F

    new-instance v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;-><init>(Lcom/google/android/youtube/videos/tagging/Tag$1;)V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->shouldInterpolate(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->getNextTag()Lcom/google/android/youtube/videos/tagging/Tag;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->getNextTag()Lcom/google/android/youtube/videos/tagging/Tag;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->x:F

    iget v3, v0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->timeMillis:I

    iget v4, v0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->x:F

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->interpolate(IFIF)F

    move-result v2

    mul-float/2addr v2, p2

    iput v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->y:F

    iget v3, v0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->timeMillis:I

    iget v4, v0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->y:F

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->interpolate(IFIF)F

    move-result v2

    mul-float/2addr v2, p3

    iput v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->radius:F

    iget v3, v0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->timeMillis:I

    iget v4, v0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->radius:F

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->interpolate(IFIF)F

    move-result v2

    mul-float/2addr v2, p2

    iput v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    :goto_0
    iget v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    float-to-double v3, p4

    const-wide v5, 0x400921fb54442d18L

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    iget v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    iput v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalB:F

    return-object v1

    :cond_0
    iget v2, p0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->x:F

    mul-float/2addr v2, p2

    iput v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerX:F

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->y:F

    mul-float/2addr v2, p3

    iput v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->centerY:F

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/Tag$CircleTag;->radius:F

    mul-float/2addr v2, p2

    iput v2, v1, Lcom/google/android/youtube/videos/tagging/Tag$RotatedOvalTagShape;->ovalA:F

    goto :goto_0
.end method
