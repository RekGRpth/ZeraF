.class abstract Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;
.super Lcom/google/android/youtube/videos/tagging/Cards$SecondaryActionButtonHelper;
.source "MoviesCards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/MoviesCards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "FilmSecondaryActionButtonHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/widget/Button;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/youtube/videos/tagging/Cards$SecondaryActionButtonHelper;-><init>(Landroid/app/Activity;Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    return-void
.end method


# virtual methods
.method protected abstract createPlayIntent()Landroid/content/Intent;
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;->button:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    invoke-super {p0, p1}, Lcom/google/android/youtube/videos/tagging/Cards$SecondaryActionButtonHelper;->onClick(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;->onClickToPlay()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const-string v1, "openStore"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMoviesCardItem(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;->startShop()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0078

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    goto :goto_0
.end method

.method protected onClickToPlay()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    const-string v1, "play"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/logging/EventLogger;->onMoviesCardItem(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;->activity:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;->createPlayIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected final playStoreEventSource()Ljava/lang/String;
    .locals 1

    const-string v0, "moviesCard"

    return-object v0
.end method

.method protected abstract startShop()Z
.end method
