.class final Lcom/google/android/youtube/videos/tagging/MoviesCards;
.super Lcom/google/android/youtube/videos/tagging/Cards;
.source "MoviesCards.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/MoviesCards$ShowSecondaryActionButtonHelper;,
        Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;,
        Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/Cards;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;
    .locals 19
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;
    .param p3    # Landroid/app/Activity;
    .param p4    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/videos/store/StoreStatusMonitor;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const v1, 0x7f040024

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;->inflate(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    const v1, 0x7f07005f

    invoke-virtual {v13}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00f7

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v13, v1, v3}, Lcom/google/android/youtube/videos/tagging/MoviesCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    const v1, 0x7f070026

    invoke-virtual {v13, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    const-string v4, "movies"

    const-string v7, "moviesCard"

    move-object/from16 v2, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v13, v15}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v16

    invoke-virtual {v13}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    const/4 v1, 0x3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v12

    new-array v14, v12, [Landroid/view/View;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move/from16 v17, v16

    :goto_0
    if-ge v10, v12, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;

    const v1, 0x7f040025

    const/4 v3, 0x0

    invoke-virtual {v11, v1, v13, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    aput-object v1, v14, v10

    aget-object v1, v14, v10

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcom/google/android/youtube/videos/tagging/MoviesCards;->initializeItemView(Landroid/view/View;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v9, 0x1

    :cond_0
    aget-object v1, v14, v10

    add-int/lit8 v16, v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    add-int/lit8 v10, v10, 0x1

    move/from16 v17, v16

    goto :goto_0

    :cond_1
    if-eqz v9, :cond_3

    invoke-static {v13}, Lcom/google/android/youtube/videos/tagging/MoviesCards;->layout(Landroid/view/View;)V

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v12, :cond_3

    aget-object v1, v14, v10

    const v3, 0x7f07002e

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aget-object v1, v14, v10

    const v3, 0x7f070062

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    invoke-static/range {v18 .. v18}, Lcom/google/android/youtube/videos/tagging/MoviesCards;->getBottom(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v3

    if-le v1, v3, :cond_2

    const/16 v1, 0x8

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_3
    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/CardTag;->forMovies(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;)Lcom/google/android/youtube/videos/tagging/CardTag;

    move-result-object v1

    invoke-virtual {v13, v1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    return-object v13
.end method

.method private static initializeItemView(Landroid/view/View;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Z
    .locals 29
    .param p0    # Landroid/view/View;
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;
    .param p3    # Landroid/app/Activity;
    .param p4    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/videos/store/StoreStatusMonitor;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")Z"
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v2, 0x7f070060

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    const v2, 0x7f0a00f9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    aput-object v7, v3, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    if-eqz v2, :cond_5

    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Movie;

    if-eqz v2, :cond_0

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    :cond_0
    const/4 v2, 0x4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->image:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-static {v0, v2, v1}, Lcom/google/android/youtube/videos/tagging/MoviesCards;->requestImage(Landroid/widget/ImageView;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/youtube/core/async/Requester;)V

    :goto_0
    const v2, 0x7f070061

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/google/android/youtube/videos/tagging/MoviesCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    const/16 v22, 0x0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->releaseDate:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :try_start_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->releaseDate:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v5, 0x4

    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    :cond_1
    :goto_1
    const/16 v26, 0x0

    const/16 v28, 0x0

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Movie;

    if-eqz v2, :cond_8

    move-object/from16 v20, p1

    check-cast v20, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Movie;

    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Movie;->videoId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v28, 0x1

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Movie;->videoId:Ljava/lang/String;

    :cond_2
    move-object/from16 v0, v20

    iget v0, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Movie;->runningTime:I

    move/from16 v24, v0

    if-nez v24, :cond_6

    const/16 v16, 0x0

    :goto_2
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v26, v16

    :cond_3
    :goto_3
    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const v2, 0x7f07002e

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-static {v0, v2, v1}, Lcom/google/android/youtube/videos/tagging/MoviesCards;->showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    :cond_4
    if-eqz v28, :cond_c

    const v2, 0x7f070062

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setVisibility(I)V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    new-instance v2, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    move-object/from16 v3, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;-><init>(Landroid/app/Activity;Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object v15, v2

    :goto_4
    invoke-virtual {v15}, Lcom/google/android/youtube/videos/tagging/Cards$SecondaryActionButtonHelper;->setup()V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_5
    return v28

    :cond_5
    const/4 v2, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :catch_0
    move-exception v17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reading the release date \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->releaseDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' of film \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\': "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v17 .. v17}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    const v2, 0x7f0a0072

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    :cond_7
    const v2, 0x7f0a0075

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v22, v3, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    const/4 v2, 0x1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v27, v3, v5

    const/4 v5, 0x1

    aput-object v16, v3, v5

    move-object/from16 v0, v23

    invoke-static {v0, v2, v3}, Lcom/google/android/youtube/core/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_3

    :cond_8
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;

    if-eqz v2, :cond_3

    move-object/from16 v25, p1

    check-cast v25, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;

    move-object/from16 v0, v25

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;->showId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    const/16 v28, 0x1

    move-object/from16 v0, v25

    iget-object v11, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;->showId:Ljava/lang/String;

    :cond_9
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;->endDate:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, ""

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    const/4 v2, 0x0

    const/4 v3, 0x4

    :try_start_1
    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v19

    :cond_a
    :goto_6
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const v2, 0x7f0a00f3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v22, v3, v5

    const/4 v5, 0x1

    aput-object v19, v3, v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_3

    :catch_1
    move-exception v17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reading the end date \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' of TV show \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\': "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v17 .. v17}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;)V

    goto :goto_6

    :cond_b
    new-instance v7, Lcom/google/android/youtube/videos/tagging/MoviesCards$ShowSecondaryActionButtonHelper;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    move-object/from16 v8, p3

    move-object v9, v4

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    invoke-direct/range {v7 .. v14}, Lcom/google/android/youtube/videos/tagging/MoviesCards$ShowSecondaryActionButtonHelper;-><init>(Landroid/app/Activity;Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    move-object v15, v7

    goto/16 :goto_4

    :cond_c
    new-instance v2, Lcom/google/android/youtube/videos/tagging/Cards$WebSearchOnClickListener;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;->title:Ljava/lang/String;

    const-string v5, "moviesCard"

    move-object/from16 v0, p3

    move-object/from16 v1, p6

    invoke-direct {v2, v0, v3, v1, v5}, Lcom/google/android/youtube/videos/tagging/Cards$WebSearchOnClickListener;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_5
.end method
