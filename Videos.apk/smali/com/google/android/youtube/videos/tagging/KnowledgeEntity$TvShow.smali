.class public final Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;
.super Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;
.source "KnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TvShow"
.end annotation


# instance fields
.field public final endDate:Ljava/lang/String;

.field public final showId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p6    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Film;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;)V

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;->endDate:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$TvShow;->showId:Ljava/lang/String;

    return-void
.end method
