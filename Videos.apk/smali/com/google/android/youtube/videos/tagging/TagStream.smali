.class public Lcom/google/android/youtube/videos/tagging/TagStream;
.super Ljava/lang/Object;
.source "TagStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
    }
.end annotation


# instance fields
.field private final allKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final chunkStarts:[I

.field private final chunks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final contentLanguage:Ljava/lang/String;

.field private final framesPerSecond:F

.field private final images:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            ">;"
        }
    .end annotation
.end field

.field private final knowledgeEntities:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final knowledgeEntitiesBySplitId:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field public final lastModified:Ljava/lang/String;

.field private final source:[B

.field private valid:Z

.field public final videoId:Ljava/lang/String;

.field public final videoItag:Ljava/lang/String;

.field public final videoTimestamp:Ljava/lang/String;

.field public final videoTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BFLjava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # [B
    .param p8    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[BF",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->videoId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->videoTitle:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->contentLanguage:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->lastModified:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->videoItag:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->videoTimestamp:Ljava/lang/String;

    const-string v7, "source cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v7}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    iput-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->source:[B

    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->framesPerSecond:F

    const-string v7, "knowledgeEntities cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v7}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p9 .. p9}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->allKnowledgeEntities:Ljava/util/List;

    invoke-interface/range {p9 .. p9}, Ljava/util/Collection;->size()I

    move-result v1

    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->knowledgeEntities:Landroid/util/SparseArray;

    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->knowledgeEntitiesBySplitId:Landroid/util/SparseArray;

    invoke-interface/range {p9 .. p9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->knowledgeEntities:Landroid/util/SparseArray;

    iget v8, v5, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->localId:I

    invoke-virtual {v7, v8, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v7, v5, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->splitIds:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->knowledgeEntitiesBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v7, v6, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    const-string v7, "images cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v7}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p10 .. p10}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->images:Ljava/util/List;

    const-string v7, "chunks cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v7}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p11 .. p11}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->chunks:Ljava/util/List;

    invoke-interface/range {p11 .. p11}, Ljava/util/Collection;->size()I

    move-result v7

    new-array v7, v7, [I

    iput-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->chunkStarts:[I

    const/4 v2, 0x0

    :goto_1
    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->chunkStarts:[I

    array-length v7, v7

    if-ge v2, v7, :cond_2

    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->chunkStarts:[I

    iget-object v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->chunks:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iget v7, v7, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    aput v7, v8, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->valid:Z

    return-void
.end method


# virtual methods
.method public getAllKnowledgeEntities()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->allKnowledgeEntities:Ljava/util/List;

    return-object v0
.end method

.method public getChunkAt(I)Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->valid:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->chunkStarts:[I

    invoke-static {v2, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-gez v0, :cond_2

    xor-int/lit8 v2, v0, -0x1

    add-int/lit8 v0, v2, -0x1

    :cond_2
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->chunks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    goto :goto_0
.end method

.method public getKnowledgeEntityBySplitId(I)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->knowledgeEntitiesBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    return-object v0
.end method

.method public getUniqueImages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->images:Ljava/util/List;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->valid:Z

    return v0
.end method

.method public loadChunkData(Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/youtube/videos/tagging/ChunkData;
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v2, "chunkInfo cannot be null"

    invoke-static {p1, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->chunks:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "chunkInfo must be from this tag stream"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->source:[B

    iget v3, p1, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->byteOffset:I

    iget v4, p1, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->byteCount:I

    invoke-direct {v1, v2, v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    :try_start_0
    new-instance v2, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;

    iget v3, p1, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;-><init>(Lcom/google/android/youtube/videos/tagging/TagStream;I)V

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/videos/tagging/ChunkData$Parser;->parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/videos/tagging/ChunkData;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->valid:Z

    throw v0
.end method

.method toMillis(I)I
    .locals 2
    .param p1    # I

    int-to-float v0, p1

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/TagStream;->framesPerSecond:F

    div-float/2addr v0, v1

    const/high16 v1, 0x447a0000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method
