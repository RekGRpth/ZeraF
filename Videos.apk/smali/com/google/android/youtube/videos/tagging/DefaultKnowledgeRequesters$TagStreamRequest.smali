.class public final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
.super Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TagStreamRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest$RequestHandler;
    }
.end annotation


# instance fields
.field private final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/net/Uri;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->uri:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method public static from(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Lcom/google/android/youtube/videos/Config;)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .locals 5
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p1    # Lcom/google/android/youtube/videos/Config;

    invoke-interface {p1}, Lcom/google/android/youtube/videos/Config;->baseKnowledgeUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    const-string v2, "lr"

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoItag:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "fmt"

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoItag:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ts"

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    new-instance v2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    invoke-interface {p1}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;Landroid/net/Uri;)V

    return-object v2
.end method


# virtual methods
.method public toFileName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->baseFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
