.class public Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
.super Landroid/widget/FrameLayout;
.source "KnowledgeOverlay.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;
.implements Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;
.implements Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;
.implements Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;
.implements Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;
.implements Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;
.implements Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$PlayerOverlay;",
        "Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;",
        "Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;",
        "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;",
        "Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;",
        "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private activity:Landroid/app/Activity;

.field private cardsToShowOnRecentActorsCardDismissed:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

.field private final cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

.field private currentChunkData:Lcom/google/android/youtube/videos/tagging/ChunkData;

.field private currentChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

.field private eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

.field private feedbackClient:Lcom/google/android/youtube/videos/tagging/FeedbackClient;

.field private feedbackProductId:I

.field private final feedbackViewHelper:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

.field private knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

.field private knowledgeFeedbackTypeId:I

.field private listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

.field private loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

.field private final minTagArea:F

.field private showAllKnowledgeCards:Z

.field private showKnowledgeAtMillis:I

.field private showRecentActorsWithinMillis:I

.field private storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

.field private final taggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

.field private videoDisplayHeight:I

.field private videoDisplayWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v7, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040012

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f070032

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/TagsView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/tagging/TagsView;->setOnTagClickListener(Lcom/google/android/youtube/videos/tagging/TagsView$OnTagClickListener;)V

    const v0, 0x7f070033

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/CardsView;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/videos/tagging/CardsView;->setCardDismissListener(Lcom/google/android/youtube/videos/tagging/CardsView$CardDismissListener;)V

    new-instance v0, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/CardsView;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    mul-float v0, v6, v6

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->minTagArea:F

    new-instance v0, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

    const v1, 0x7f070034

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/videos/tagging/TagsView;Lcom/google/android/youtube/videos/tagging/CardsView;Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper$Listener;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->setClipChildren(Z)V

    invoke-virtual {p0, v7}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->setClipToPadding(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->setMeasureAllChildren(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->hide()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;)Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;)Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/ChunkData;)Lcom/google/android/youtube/videos/tagging/ChunkData;
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;
    .param p1    # Lcom/google/android/youtube/videos/tagging/ChunkData;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkData:Lcom/google/android/youtube/videos/tagging/ChunkData;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;)I
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    iget v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledgeAtMillis:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;)V
    .locals 0
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledge()V

    return-void
.end method

.method private hide(Z)V
    .locals 2
    .param p1    # Z

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapse(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->reset()V

    if-eqz p1, :cond_0

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledgeAtMillis:I

    :cond_0
    return-void
.end method

.method private loadChunkData(I)Z
    .locals 5
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iget v3, v3, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    if-gt v3, p1, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iget v3, v3, Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;->endMillis:I

    if-ge p1, v3, :cond_0

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    :goto_0
    return v1

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    iget-object v3, v3, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/videos/tagging/TagStream;->getChunkAt(I)Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    move-result-object v0

    if-nez v0, :cond_1

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkData:Lcom/google/android/youtube/videos/tagging/ChunkData;

    iput-object v4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    if-ne v0, v1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    new-instance v1, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;)V

    new-array v3, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v1, v2

    goto :goto_0
.end method

.method private showKnowledge()V
    .locals 29

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkData:Lcom/google/android/youtube/videos/tagging/ChunkData;

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    iget-object v2, v2, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/youtube/videos/tagging/TagStream;->toMillis(I)I

    move-result v26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkData:Lcom/google/android/youtube/videos/tagging/ChunkData;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledgeAtMillis:I

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v2, v4, v0, v1}, Lcom/google/android/youtube/videos/tagging/ChunkData;->getActiveTags(ILjava/util/List;I)V

    const/16 v24, 0x0

    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v24

    if-ge v0, v2, :cond_0

    move-object/from16 v0, v21

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/google/android/youtube/videos/tagging/Tag;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledgeAtMillis:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->videoDisplayWidth:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->videoDisplayHeight:I

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->minTagArea:F

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v4, v5, v6}, Lcom/google/android/youtube/videos/tagging/Tag;->getTagShape(IFFF)Lcom/google/android/youtube/videos/tagging/Tag$TagShape;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    iget-object v2, v2, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    move-object/from16 v0, v27

    iget v4, v0, Lcom/google/android/youtube/videos/tagging/Tag;->splitId:I

    invoke-virtual {v2, v4}, Lcom/google/android/youtube/videos/tagging/TagStream;->getKnowledgeEntityBySplitId(I)Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    new-instance v4, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    move-object/from16 v0, v27

    iget v5, v0, Lcom/google/android/youtube/videos/tagging/Tag;->splitId:I

    move-object/from16 v0, v28

    invoke-direct {v4, v3, v5, v0}, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;ILcom/google/android/youtube/videos/tagging/Tag$TagShape;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v24, v24, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->videoDisplayWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->videoDisplayHeight:I

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/android/youtube/videos/tagging/TagsView;->show(Ljava/util/List;II)V

    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->sortByTypeAndHorizontalPosition(Ljava/util/List;)V

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v14

    const/16 v24, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v24

    if-ge v0, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    move/from16 v0, v24

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    iget-object v3, v2, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    iget v2, v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->localId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v14, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->activity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v5, p0

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->inflateCards(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/util/Collection;)V

    :cond_2
    add-int/lit8 v24, v24, 0x1

    goto :goto_1

    :cond_3
    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showAllKnowledgeCards:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    iget-object v2, v2, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/TagStream;->getAllKnowledgeEntities()Ljava/util/List;

    move-result-object v22

    const/16 v24, 0x0

    :goto_2
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v24

    if-ge v0, v2, :cond_6

    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    iget v2, v3, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->localId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v14, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->activity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->inflateCards(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/util/Collection;)V

    :cond_4
    add-int/lit8 v24, v24, 0x1

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showRecentActorsWithinMillis:I

    if-lez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledgeAtMillis:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showRecentActorsWithinMillis:I

    sub-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledgeAtMillis:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->activity:Landroid/app/Activity;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->account:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    move-object/from16 v20, v0

    move-object/from16 v16, p0

    invoke-virtual/range {v11 .. v20}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->inflateRecentActorsCard(IILjava/util/Set;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;

    move-result-object v25

    if-eqz v25, :cond_6

    const/16 v23, 0x1

    move-object/from16 v0, v25

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->hide()V

    :cond_7
    :goto_3
    return-void

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v2, v10}, Lcom/google/android/youtube/videos/tagging/CardsView;->show(Ljava/util/List;)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    move/from16 v0, v23

    invoke-interface {v2, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;->onCardsShown(Z)V

    goto :goto_3
.end method


# virtual methods
.method public generateLayoutParams()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public hide()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->hide(Z)V

    return-void
.end method

.method public init(Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Lcom/google/android/youtube/videos/tagging/FeedbackClient;IILcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;
    .param p2    # Landroid/app/Activity;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p5    # Lcom/google/android/youtube/videos/tagging/FeedbackClient;
    .param p6    # I
    .param p7    # I
    .param p8    # Lcom/google/android/youtube/videos/logging/EventLogger;

    const-string v0, "knowledgeBundle cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    const-string v0, "activity cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->activity:Landroid/app/Activity;

    const-string v0, "account cannot be empty"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->account:Ljava/lang/String;

    const-string v0, "storeStatusMonitor cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    const-string v0, "feedbackClient cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/FeedbackClient;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackClient:Lcom/google/android/youtube/videos/tagging/FeedbackClient;

    iput p6, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackProductId:I

    iput p7, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeFeedbackTypeId:I

    const-string v0, "eventLogger cannot be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->hide()V

    return-void
.end method

.method public isCardListExpanded()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->isExpanded()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAllCardsDismissed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    const-string v1, "dismissAllCards"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapse(Ljava/lang/String;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->isCollectingFeedback()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->reset()V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->isExpanded()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    const-string v2, "backButton"

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapse(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCardDismissed(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/youtube/videos/tagging/CardTag;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, v1

    check-cast v0, Lcom/google/android/youtube/videos/tagging/CardTag;

    sget-object v2, Lcom/google/android/youtube/videos/tagging/CardTag;->RECENT_ACTORS:Lcom/google/android/youtube/videos/tagging/CardTag;

    if-ne v0, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsToShowOnRecentActorsCardDismissed:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsToShowOnRecentActorsCardDismissed:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v4, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/tagging/CardsView;->setChangeAppearingAnimatorEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsToShowOnRecentActorsCardDismissed:Ljava/util/List;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/youtube/videos/tagging/CardsView;->insertAfter(Ljava/util/List;Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v2, v4}, Lcom/google/android/youtube/videos/tagging/CardsView;->setChangeAppearingAnimatorEnabled(Z)V

    :goto_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsToShowOnRecentActorsCardDismissed:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    invoke-interface {v2}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;->onExpandRecentActors()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsToShowOnRecentActorsCardDismissed:Ljava/util/List;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/youtube/videos/tagging/CardsView;->insertAfter(Ljava/util/List;Landroid/view/View;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    invoke-interface {v2, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;->onCardDismissed(Lcom/google/android/youtube/videos/tagging/CardTag;)V

    goto :goto_0
.end method

.method public onError(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;
    .param p2    # Ljava/lang/Exception;

    const-string v0, "Error sending knowledge feedback"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->onError(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/Exception;)V

    return-void
.end method

.method public onFeedbackButtonClick(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    iget-object v3, v2, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->collectFeedback(Ljava/util/List;)V

    :cond_2
    return-void
.end method

.method public onFeedbackCollected(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;

    iget v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackProductId:I

    iget v2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeFeedbackTypeId:I

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    iget-object v4, v4, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    iget v5, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledgeAtMillis:I

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/tagging/KnowledgeFeedbackReport;-><init>(IILjava/lang/String;Lcom/google/android/youtube/videos/tagging/TagStream;ILjava/util/List;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackClient:Lcom/google/android/youtube/videos/tagging/FeedbackClient;

    invoke-interface {v1, v0, p0}, Lcom/google/android/youtube/videos/tagging/FeedbackClient;->sendFeedback(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Lcom/google/android/youtube/core/async/Callback;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->activity:Landroid/app/Activity;

    const v2, 0x7f0a0104

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/youtube/core/utils/Util;->showToast(Landroid/content/Context;II)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRecentActorsCardClick(Landroid/view/View;Ljava/util/List;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsToShowOnRecentActorsCardDismissed:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/videos/tagging/CardsView;->dismiss(Landroid/view/View;Z)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/Void;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;
    .param p2    # Ljava/lang/Void;

    const-string v0, "Knowledge feedback sent."

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->onResponse(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/Void;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->updateMaxTranslationY()V

    return-void
.end method

.method public onTagClick(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;->onClickOutsideTags()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsView;->scrollTo(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    const-string v1, "clickTag"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->expand(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->isCollectingFeedback()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->reset()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    const-string v1, "clickOutsideCards"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->collapse(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;->onClickOutsideTags()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepareForPosition(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadChunkData(I)Z

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->hide()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->activity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->account:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->storeStatusMonitor:Lcom/google/android/youtube/videos/store/StoreStatusMonitor;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackClient:Lcom/google/android/youtube/videos/tagging/FeedbackClient;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->currentChunkData:Lcom/google/android/youtube/videos/tagging/ChunkData;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadingChunkInfo:Lcom/google/android/youtube/videos/tagging/TagStream$ChunkInfo;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;->onReset()V

    :cond_0
    return-void
.end method

.method public setContentVisible(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->tagsView:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/videos/tagging/CardsView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->listener:Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->setListener(Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay$Listener;)V

    return-void
.end method

.method public setPadding(IIII)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0, v1, v1, p3, p4}, Lcom/google/android/youtube/videos/tagging/CardsView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsView:Lcom/google/android/youtube/videos/tagging/CardsView;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/videos/tagging/CardsView;->setTopInset(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/CardsViewAnimationHelper;->updateMaxTranslationY()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v0, v1, p2, p3, p4}, Lcom/google/android/youtube/videos/tagging/FeedbackViewHelper;->setPadding(IIII)V

    goto :goto_0
.end method

.method public setShowAllKnowledgeCards(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showAllKnowledgeCards:Z

    return-void
.end method

.method public show(III)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->show(IIII)V

    return-void
.end method

.method public show(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->knowledgeBundle:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledgeAtMillis:I

    iput p2, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showRecentActorsWithinMillis:I

    iput p3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->videoDisplayWidth:I

    iput p4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->videoDisplayHeight:I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->loadChunkData(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->showKnowledge()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeOverlay;->hide(Z)V

    goto :goto_0
.end method
