.class final Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;
.super Ljava/lang/Object;
.source "KnowledgeBundle.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RecentActorComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/util/Pair",
        "<",
        "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;->INSTANCE:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/util/Pair;Landroid/util/Pair;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    if-ne p1, p2, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    if-nez p2, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int v0, v2, v1

    if-nez v0, :cond_0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    iget v2, v1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->localId:I

    iget-object v1, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    iget v1, v1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->localId:I

    sub-int v0, v2, v1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Landroid/util/Pair;

    check-cast p2, Landroid/util/Pair;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;->compare(Landroid/util/Pair;Landroid/util/Pair;)I

    move-result v0

    return v0
.end method
