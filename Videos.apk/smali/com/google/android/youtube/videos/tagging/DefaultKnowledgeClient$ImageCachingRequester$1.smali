.class Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->request(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

.field final synthetic val$cacheKey:Ljava/lang/String;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$request:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$cacheKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$request:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$request:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->onError(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
    .param p2    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    # getter for: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->access$1000(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;)Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$cacheKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$request:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->onResponse(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Landroid/graphics/Bitmap;)V

    return-void
.end method
