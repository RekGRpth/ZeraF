.class final Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;
.super Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;
.source "MoviesCards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/MoviesCards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MovieSecondaryActionButtonHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/widget/Button;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/youtube/videos/tagging/MoviesCards$FilmSecondaryActionButtonHelper;-><init>(Landroid/app/Activity;Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)V

    return-void
.end method


# virtual methods
.method protected createPlayIntent()Landroid/content/Intent;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;->itemId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/videos/activity/WatchDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected itemType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected startShop()Z
    .locals 4

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;->itemId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;->account:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/youtube/core/utils/ExternalIntents;->startShopForMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;->eventLogger:Lcom/google/android/youtube/videos/logging/EventLogger;

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/MoviesCards$MovieSecondaryActionButtonHelper;->playStoreEventSource()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/videos/logging/EventLogger;->onOpenedPlayStoreForMovie(Ljava/lang/String;Z)V

    return v0
.end method
