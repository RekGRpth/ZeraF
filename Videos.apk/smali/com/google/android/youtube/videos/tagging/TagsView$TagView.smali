.class abstract Lcom/google/android/youtube/videos/tagging/TagsView$TagView;
.super Landroid/view/View;
.source "TagsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/TagsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "TagView"
.end annotation


# static fields
.field public static final ORDER_BY_AREA:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;

.field public static final ORDER_BY_CENTER_X:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;

.field public static final ORDER_BY_CENTER_Y:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;


# instance fields
.field protected centerX:F

.field protected centerY:F

.field protected final rect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView$1;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView$1;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->ORDER_BY_AREA:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView$2;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView$2;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->ORDER_BY_CENTER_X:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;

    new-instance v0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView$3;

    invoke-direct {v0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView$3;-><init>()V

    sput-object v0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->ORDER_BY_CENTER_Y:Lcom/google/android/youtube/videos/tagging/TagsView$TagViewComparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method protected abstract animationScaleFactor()F
.end method

.method protected animationTargetAlpha()F
    .locals 1

    const/high16 v0, 0x3f800000

    return v0
.end method

.method protected abstract getTaggedKnowledgeEntity()Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;
.end method

.method public hitTest(FFF)Z
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, p3

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, p3

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, p3

    cmpg-float v0, v0, p2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, p3

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected init(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 7
    .param p1    # Landroid/graphics/RectF;
    .param p2    # Landroid/graphics/RectF;

    const/high16 v6, 0x3f000000

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v5, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    iput v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->centerX:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    iput v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->centerY:F

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v5, p2}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    add-float/2addr v5, v6

    float-to-int v4, v5

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    add-float/2addr v5, v6

    float-to-int v0, v5

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget v6, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v5, v6

    float-to-int v2, v5

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget v6, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v6

    float-to-int v3, v5

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    add-int v5, v2, v4

    add-int v6, v3, v0

    invoke-virtual {p0, v2, v3, v5, v6}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->layout(IIII)V

    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # Landroid/graphics/Rect;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setActivated(Z)V

    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    return-void
.end method

.method public startAppearAnimation(I)V
    .locals 10
    .param p1    # I

    const/high16 v9, 0x3f800000

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setAlpha(F)V

    const-string v4, "alpha"

    new-array v5, v7, [F

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->animationTargetAlpha()F

    move-result v6

    aput v6, v5, v8

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->animationScaleFactor()F

    move-result v3

    cmpl-float v4, v3, v9

    if-nez v4, :cond_0

    move-object v1, v0

    :goto_0
    int-to-long v4, p1

    const-wide/16 v6, 0xc8

    mul-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Landroid/animation/Animator;->setStartDelay(J)V

    const-wide/16 v4, 0x96

    invoke-virtual {v1, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    return-void

    :cond_0
    iget v4, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->centerX:F

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v5

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setPivotX(F)V

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->centerY:F

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v5

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setPivotY(F)V

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setScaleX(F)V

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->setScaleY(F)V

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v0, v4, v8

    const-string v5, "scaleX"

    new-array v6, v7, [F

    aput v9, v6, v8

    invoke-static {p0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    const-string v6, "scaleY"

    new-array v7, v7, [F

    aput v9, v7, v8

    invoke-static {p0, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    move-object v1, v2

    goto :goto_0
.end method
