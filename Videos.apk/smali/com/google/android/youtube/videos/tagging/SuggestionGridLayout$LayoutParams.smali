.class public Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SuggestionGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public canDismiss:Z

.field public canDrag:Z

.field public column:I

.field public noPadding:Z

.field public removeOnDismiss:Z


# direct methods
.method public constructor <init>(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    iput p3, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    sget-object v1, Lcom/google/android/videos/R$styleable;->SuggestionGridLayout_Layout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, 0x1

    const/4 v1, -0x1

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {p0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    instance-of v1, p1, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;

    iget v1, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    iput v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    iget-boolean v1, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iget-boolean v1, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iget-boolean v1, v0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    :cond_0
    return-void
.end method
