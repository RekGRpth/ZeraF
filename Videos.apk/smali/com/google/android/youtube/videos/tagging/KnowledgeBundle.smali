.class public Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;
.super Ljava/lang/Object;
.source "KnowledgeBundle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;,
        Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;,
        Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;,
        Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;
    }
.end annotation


# instance fields
.field private final imageRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public final tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/TagStream;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/TagStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/TagStream;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "tagStream cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/tagging/TagStream;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    const-string v0, "imageRequester cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/Requester;

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method


# virtual methods
.method public inflateCards(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;Ljava/util/Collection;)V
    .locals 9
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;
    .param p2    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;
    .param p3    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;
    .param p4    # Landroid/app/Activity;
    .param p5    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/videos/store/StoreStatusMonitor;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v3, "knowledgeEntity cannot be null"

    invoke-static {p1, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "cardCreationHelper cannot be null"

    invoke-static {p2, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "storeStatusMonitor cannot be null"

    invoke-static {p5, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "account cannot be empty"

    invoke-static {p6, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v3, "eventLogger cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "knowledgeCards cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v3, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    if-eqz v3, :cond_1

    move-object v1, p1

    check-cast v1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-static/range {v1 .. v8}, Lcom/google/android/youtube/videos/tagging/ActorCards;->create(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnFeedbackButtonClickListener;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v3, v1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v1 .. v7}, Lcom/google/android/youtube/videos/tagging/MoviesCards;->create(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v3, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;

    if-eqz v3, :cond_0

    move-object v2, p1

    check-cast v2, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object v3, p2

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-static/range {v2 .. v8}, Lcom/google/android/youtube/videos/tagging/SongCards;->create(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public inflateRecentActorsCard(IILjava/util/Set;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;
    .locals 15
    .param p1    # I
    .param p2    # I
    .param p4    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;
    .param p5    # Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;
    .param p6    # Landroid/app/Activity;
    .param p7    # Lcom/google/android/youtube/videos/store/StoreStatusMonitor;
    .param p8    # Ljava/lang/String;
    .param p9    # Lcom/google/android/youtube/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;",
            "Landroid/app/Activity;",
            "Lcom/google/android/youtube/videos/store/StoreStatusMonitor;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/logging/EventLogger;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v1, "cardInflater cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "onRecentActorsCardClickListener cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "activity cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "storeStatusMonitor cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "account cannot be empty"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "eventLogger cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/TagStream;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/youtube/videos/tagging/TagStream;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/tagging/TagStream;->getAllKnowledgeEntities()Ljava/util/List;

    move-result-object v10

    sget-object v1, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;->INSTANCE:Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$RecentActorComparator;

    invoke-static {v1}, Lcom/google/common/collect/Maps;->newTreeMap(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v14

    const/4 v11, 0x0

    :goto_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v1

    if-ge v11, v1, :cond_3

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    instance-of v1, v12, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    if-eqz v1, :cond_2

    if-eqz p3, :cond_1

    iget v1, v12, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;->localId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    move-object v9, v12

    check-cast v9, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->lastAppearance(I)I

    move-result v13

    move/from16 v0, p1

    if-gt v0, v13, :cond_2

    move/from16 v0, p2

    if-ge v13, v0, :cond_2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v9, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v14, v1, v9}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v14}, Ljava/util/TreeMap;->size()I

    move-result v1

    const/4 v2, 0x5

    if-le v1, v2, :cond_2

    invoke-virtual {v14}, Ljava/util/TreeMap;->pollLastEntry()Ljava/util/Map$Entry;

    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v14}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    invoke-virtual {v14}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-static/range {v1 .. v8}, Lcom/google/android/youtube/videos/tagging/RecentActorsCards;->create(Ljava/util/List;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/youtube/videos/tagging/KnowledgeBundle$OnRecentActorsCardClickListener;Lcom/google/android/youtube/core/async/Requester;Landroid/app/Activity;Lcom/google/android/youtube/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/youtube/videos/logging/EventLogger;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method
