.class public abstract Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;
.super Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BytesRequestHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler",
        "<TR;TE;TT;[B>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic shouldUpdate(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, [B

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;->shouldUpdate([B[B)Z

    move-result v0

    return v0
.end method

.method public shouldUpdate([B[B)Z
    .locals 1
    .param p1    # [B
    .param p2    # [B

    invoke-static {p1, p2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
