.class final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ImageCachingRequester"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<",
        "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private alwaysRequest:Z

.field private final globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

.field private final knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

.field private final target:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final userCountry:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;Lcom/google/android/youtube/videos/utils/BitmapLruCache;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/videos/utils/BitmapLruCache;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->userCountry:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;)Lcom/google/android/youtube/videos/utils/BitmapLruCache;
    .locals 1
    .param p0    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    return-object v0
.end method


# virtual methods
.method public request(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    new-instance v2, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->knowledgeRequest:Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->userCountry:Ljava/lang/String;

    invoke-direct {v2, v3, v4, p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;-><init>(Lcom/google/android/youtube/videos/tagging/KnowledgeRequest;Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->toFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".k"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->alwaysRequest:Z

    if-eqz v3, :cond_1

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {p2, p1, v1}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->target:Lcom/google/android/youtube/core/async/Requester;

    new-instance v4, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;

    invoke-direct {v4, p0, v0, p2, p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;-><init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;)V

    invoke-interface {v3, v2, v4}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->globalBitmapCache:Lcom/google/android/youtube/videos/utils/BitmapLruCache;

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/videos/utils/BitmapLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    move-object v1, v3

    goto :goto_0
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->request(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public setAlwaysRequest(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->alwaysRequest:Z

    return-void
.end method
