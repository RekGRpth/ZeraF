.class final Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;
.super Ljava/lang/Object;
.source "TaggedKnowledgeEntity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TypeAndHorizontalPositionComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;",
        ">;"
    }
.end annotation


# static fields
.field private static final TYPE_ORDER:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final boundingBox:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Song;

    const-class v1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->TYPE_ORDER:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->boundingBox:Landroid/graphics/RectF;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;-><init>()V

    return-void
.end method

.method private getCenterX(Lcom/google/android/youtube/videos/tagging/Tag$TagShape;)F
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/tagging/Tag$TagShape;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->boundingBox:Landroid/graphics/RectF;

    invoke-interface {p1, v0}, Lcom/google/android/youtube/videos/tagging/Tag$TagShape;->getBoundingBox(Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->boundingBox:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public compare(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;)I
    .locals 7
    .param p1    # Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;
    .param p2    # Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    if-nez p1, :cond_1

    if-nez p2, :cond_0

    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_0
    const/4 v5, -0x1

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    sget-object v5, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->TYPE_ORDER:Ljava/util/List;

    iget-object v6, p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    sget-object v5, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->TYPE_ORDER:Ljava/util/List;

    iget-object v6, p2, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/youtube/videos/tagging/KnowledgeEntity;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    sub-int v2, v0, v3

    if-eqz v2, :cond_3

    move v5, v2

    goto :goto_0

    :cond_3
    iget-object v5, p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/youtube/videos/tagging/Tag$TagShape;

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->getCenterX(Lcom/google/android/youtube/videos/tagging/Tag$TagShape;)F

    move-result v1

    iget-object v5, p2, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/youtube/videos/tagging/Tag$TagShape;

    invoke-direct {p0, v5}, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->getCenterX(Lcom/google/android/youtube/videos/tagging/Tag$TagShape;)F

    move-result v4

    sub-float v5, v1, v4

    invoke-static {v5}, Ljava/lang/Math;->signum(F)F

    move-result v5

    float-to-int v5, v5

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    check-cast p2, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;->compare(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;)I

    move-result v0

    return v0
.end method
