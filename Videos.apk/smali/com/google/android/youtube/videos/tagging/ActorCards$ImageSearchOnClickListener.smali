.class final Lcom/google/android/youtube/videos/tagging/ActorCards$ImageSearchOnClickListener;
.super Ljava/lang/Object;
.source "ActorCards.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/ActorCards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ImageSearchOnClickListener"
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final query:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/ActorCards$ImageSearchOnClickListener;->activity:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/youtube/videos/tagging/Cards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/ActorCards$ImageSearchOnClickListener;->query:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const-string v1, "http://www.google.com/search?q=%1$s&tbm=isch"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/ActorCards$ImageSearchOnClickListener;->query:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/ActorCards$ImageSearchOnClickListener;->activity:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/ExternalIntents;->viewUri(Landroid/app/Activity;Landroid/net/Uri;)V

    return-void
.end method
