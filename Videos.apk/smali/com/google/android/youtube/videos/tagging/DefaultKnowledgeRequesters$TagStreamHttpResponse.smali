.class public final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TagStreamHttpResponse"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final content:[B

.field public final contentLanguage:Ljava/lang/String;

.field public final isNotFound:Z

.field public final lastModified:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;[B)V
    .locals 0
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->isNotFound:Z

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->lastModified:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->contentLanguage:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->content:[B

    return-void
.end method
