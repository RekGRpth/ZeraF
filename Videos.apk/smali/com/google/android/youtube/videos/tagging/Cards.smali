.class Lcom/google/android/youtube/videos/tagging/Cards;
.super Ljava/lang/Object;
.source "Cards.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;,
        Lcom/google/android/youtube/videos/tagging/Cards$StoreSearchOnClickListener;,
        Lcom/google/android/youtube/videos/tagging/Cards$WebSearchOnClickListener;,
        Lcom/google/android/youtube/videos/tagging/Cards$SecondaryActionButtonHelper;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static buildCharacterNamesString(Landroid/content/res/Resources;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;)Ljava/lang/String;
    .locals 8
    .param p0    # Landroid/content/res/Resources;
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;

    const/4 v7, 0x0

    const/4 v1, 0x0

    iget-object v4, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->characterNames:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    const v4, 0x7f0a00f1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p1, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Person;->characterNames:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p0, v7, v3}, Lcom/google/android/youtube/core/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method protected static getBottom(Landroid/view/View;)I
    .locals 2
    .param p0    # Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v0

    goto :goto_0
.end method

.method protected static layout(Landroid/view/View;)V
    .locals 4
    .param p0    # Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/high16 v1, 0x40000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method protected static requestImage(Landroid/widget/ImageView;Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 1
    .param p0    # Landroid/widget/ImageView;
    .param p1    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/videos/tagging/Cards$ImageCallback;-><init>(Landroid/widget/ImageView;)V

    invoke-interface {p2, p1, v0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method protected static setImageMatrixIfSquareCropExists(Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;Landroid/widget/ImageView;I)V
    .locals 7
    .param p0    # Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;
    .param p1    # Landroid/widget/ImageView;
    .param p2    # I

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->getSquareCropRegionIndex()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {p0, v2, v3}, Lcom/google/android/youtube/videos/tagging/KnowledgeEntity$Image;->getCropRegion(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    new-instance v3, Landroid/graphics/RectF;

    int-to-float v4, p2

    int-to-float v5, p2

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v4, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v0, v3, v4}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    sget-object v3, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_0
    return-void
.end method

.method protected static showViewAndSetText(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;
    .locals 2
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method protected static toQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "\""

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "\""

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
