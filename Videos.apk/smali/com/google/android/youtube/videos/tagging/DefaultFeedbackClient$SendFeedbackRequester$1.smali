.class Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;
.super Ljava/lang/Object;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->requestToken(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;

.field final synthetic val$callback:Lcom/google/android/youtube/core/async/Callback;

.field final synthetic val$report:Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->val$report:Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->val$report:Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->onResponse(Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->val$report:Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$1;->val$callback:Lcom/google/android/youtube/core/async/Callback;

    # invokes: Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->onGotAutoLoginHeaderOrToken(Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;ZLcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;Lcom/google/android/youtube/core/async/Callback;)V
    invoke-static {v0, v1, v2, p2, v3}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->access$000(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;ZLcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
