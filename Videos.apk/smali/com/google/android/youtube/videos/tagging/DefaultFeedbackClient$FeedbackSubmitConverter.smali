.class final Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;
.super Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FeedbackSubmitConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final submitUrlTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;->submitUrlTemplate:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;->convertRequest(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 8
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;->submitUrlTemplate:Ljava/lang/String;

    const-string v6, "{$GF_TOKEN}"

    iget-object v7, p1, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;->token:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v3, v4}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    iget-object v5, p1, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;->report:Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-interface {v5}, Lcom/google/android/youtube/videos/tagging/FeedbackClient$FeedbackReport;->buildMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v5

    invoke-interface {v5, v0}, Lcom/google/protobuf/MessageLite;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v2, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {v2, v5}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    const-string v5, "application/x-protobuf"

    invoke-virtual {v2, v5}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    return-object v3

    :catch_0
    move-exception v1

    new-instance v5, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v6, "Error constructing feedback report blob"

    invoke-direct {v5, v6, v1}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method protected bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Void;
    .locals 3
    .param p1    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\"success\":true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v2, "Couldn\'t find success signal in feedback submission response"

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
