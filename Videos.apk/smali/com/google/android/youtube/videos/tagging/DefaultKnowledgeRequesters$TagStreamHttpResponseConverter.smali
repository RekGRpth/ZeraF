.class final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;
.super Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TagStreamHttpResponseConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
        "<",
        "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;
    .locals 12
    .param p1    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v11, 0x0

    const/4 v8, 0x0

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    const/16 v10, 0x194

    if-ne v9, v10, :cond_0

    new-instance v9, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    const/4 v10, 0x1

    invoke-direct {v9, v10, v8, v8, v8}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;[B)V

    move-object v8, v9

    :goto_0
    return-object v8

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;->checkHttpError(Lorg/apache/http/HttpResponse;)V

    const-string v9, "Last-Modified"

    invoke-interface {p1, v9}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v5

    if-nez v5, :cond_1

    move-object v6, v8

    :goto_1
    const-string v9, "Content-Language"

    invoke-interface {p1, v9}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v5

    if-nez v5, :cond_2

    move-object v2, v8

    :goto_2
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    if-nez v7, :cond_3

    new-array v1, v11, [B

    :goto_3
    new-instance v8, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    invoke-direct {v8, v11, v6, v2, v1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;[B)V

    goto :goto_0

    :cond_1
    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_2
    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_3
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v3

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const-wide/16 v8, 0x0

    cmp-long v8, v8, v3

    if-gtz v8, :cond_4

    const-wide/32 v8, 0x7fffffff

    cmp-long v8, v3, v8

    if-gez v8, :cond_4

    long-to-int v8, v3

    :goto_4
    invoke-direct {v0, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-interface {v7, v0}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    goto :goto_3

    :cond_4
    const/16 v8, 0x4000

    goto :goto_4
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponseConverter;->convertResponse(Lorg/apache/http/HttpResponse;)Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;

    move-result-object v0

    return-object v0
.end method
