.class final Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Worker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/api/AssetsRequest;",
        "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private firstException:Ljava/lang/Exception;

.field private hadSuccessfulResponse:Z

.field private nextIndex:I

.field private final originalCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "[B>;"
        }
    .end annotation
.end field

.field private final originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

.field private final responseBuilder:Lcom/google/wireless/android/video/magma/proto/AssetListResponse$Builder;

.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 1
    .param p2    # Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "[B>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iput-object p3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetListResponse$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->responseBuilder:Lcom/google/wireless/android/video/magma/proto/AssetListResponse$Builder;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    return-void
.end method

.method private maybeAddAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 10
    .param p1    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasResourceId()Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getResourceId()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasMid()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v7}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hasId()Z

    move-result v1

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hasMetadata()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getImagesCount()I

    move-result v4

    :goto_1
    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->getMetadata()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->getImages(I)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    move-result-object v8

    sget-object v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;->TYPE_POSTER:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    if-eq v8, v9, :cond_2

    invoke-virtual {v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->getType()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    move-result-object v8

    sget-object v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;->TYPE_LOGO:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image$ImageType;

    if-ne v8, v9, :cond_3

    :cond_2
    invoke-virtual {v5, v3}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;->addImages(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;

    const/4 v0, 0x1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    :cond_5
    if-nez v1, :cond_6

    if-eqz v0, :cond_0

    :cond_6
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->newBuilder()Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->setResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    move-result-object v6

    if-eqz v0, :cond_7

    invoke-virtual {v6, v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;->setMetadata(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Builder;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;

    :cond_7
    iget-object v8, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->responseBuilder:Lcom/google/wireless/android/video/magma/proto/AssetListResponse$Builder;

    invoke-virtual {v8, v6}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse$Builder;->addResource(Lcom/google/wireless/android/video/magma/proto/AssetResource$Builder;)Lcom/google/wireless/android/video/magma/proto/AssetListResponse$Builder;

    goto :goto_0
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/api/AssetsRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/api/AssetsRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->firstException:Ljava/lang/Exception;

    if-nez v0, :cond_0

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->firstException:Ljava/lang/Exception;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->requestNextBatch()V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/api/AssetsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->onError(Lcom/google/android/youtube/videos/api/AssetsRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/api/AssetsRequest;
    .param p2    # Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->hadSuccessfulResponse:Z

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->getResourceCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p2, v0}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->getResource(I)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->maybeAddAssetResource(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->requestNextBatch()V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/api/AssetsRequest;

    check-cast p2, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->onResponse(Lcom/google/android/youtube/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;)V

    return-void
.end method

.method public requestNextBatch()V
    .locals 7

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v3, v3, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->assetResourceIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    if-lt v3, v0, :cond_2

    if-eqz v0, :cond_0

    iget-boolean v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->hadSuccessfulResponse:Z

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->responseBuilder:Lcom/google/wireless/android/video/magma/proto/AssetListResponse$Builder;

    invoke-virtual {v5}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse$Builder;->build()Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->toByteArray()[B

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v5, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->firstException:Ljava/lang/Exception;

    invoke-interface {v3, v4, v5}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_2
    new-instance v3, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;

    invoke-direct {v3}, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;-><init>()V

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v4, v4, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->account:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->includeImages(Z)Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v3, v3, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->userCountry:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v3, v3, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;

    :cond_3
    :goto_1
    iget v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    if-ge v3, v0, :cond_4

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->getIdCount()I

    move-result v3

    const/16 v4, 0x64

    if-ge v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    iget-object v3, v3, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;->assetResourceIds:Ljava/util/List;

    iget v4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->getIdsLengthIncluding(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x708

    if-gt v3, v4, :cond_4

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->addId(Ljava/lang/String;)Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;

    iget v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->nextIndex:I

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->getIdCount()I

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalCallback:Lcom/google/android/youtube/core/async/Callback;

    iget-object v4, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->originalRequest:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    new-instance v5, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v6, "Request contains an invalid ID"

    invoke-direct {v5, v6}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4, v5}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->this$0:Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;

    # getter for: Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->apiAssetsRequester:Lcom/google/android/youtube/core/async/Requester;
    invoke-static {v3}, Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->access$200(Lcom/google/android/youtube/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/youtube/videos/api/AssetsRequest;

    move-result-object v4

    invoke-interface {v3, v4, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0
.end method
