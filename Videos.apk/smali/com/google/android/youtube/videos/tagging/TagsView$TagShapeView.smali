.class final Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;
.super Lcom/google/android/youtube/videos/tagging/TagsView$TagView;
.source "TagsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/tagging/TagsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TagShapeView"
.end annotation


# instance fields
.field private labelView:Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

.field private taggedKnowledgeEntity:Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

.field final synthetic this$0:Lcom/google/android/youtube/videos/tagging/TagsView;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/tagging/TagsView;Landroid/content/Context;)V
    .locals 2
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method


# virtual methods
.method protected animationScaleFactor()F
    .locals 1

    const v0, 0x3f8ccccd

    return v0
.end method

.method protected animationTargetAlpha()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagShapeAlpha:F
    invoke-static {v0}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$1100(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v0

    return v0
.end method

.method protected dispatchSetActivated(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->dispatchSetActivated(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->labelView:Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->labelView:Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->setActivated(Z)V

    :cond_0
    return-void
.end method

.method public getLabelView()Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->labelView:Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    return-object v0
.end method

.method protected getTaggedKnowledgeEntity()Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->taggedKnowledgeEntity:Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    return-object v0
.end method

.method public hitTest(FFF)Z
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->taggedKnowledgeEntity:Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    iget-object v0, v0, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/youtube/videos/tagging/Tag$TagShape;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/videos/tagging/Tag$TagShape;->hitTest(FFF)Z

    move-result v0

    return v0
.end method

.method public init(Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;Landroid/graphics/RectF;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;
    .param p2    # Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;
    .param p3    # Landroid/graphics/RectF;

    iput-object p1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->taggedKnowledgeEntity:Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    iput-object p2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->labelView:Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    iget-object v1, p1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/youtube/videos/tagging/Tag$TagShape;

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/videos/tagging/Tag$TagShape;->getBoundingBox(Landroid/graphics/RectF;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->shadowOverflow:F
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$300(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagStrokeWidth:F
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$400(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v2

    add-float v0, v1, v2

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v1

    neg-float v2, v0

    neg-float v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->boundingBox:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$200(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->init(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->taggedKnowledgeEntity:Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;

    iget-object v0, v1, Lcom/google/android/youtube/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/youtube/videos/tagging/Tag$TagShape;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->save(I)I

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->rect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->rect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    neg-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->shadowYOffset:F
    invoke-static {v3}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$500(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$700(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->shadowColor:I
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$600(Lcom/google/android/youtube/videos/tagging/TagsView;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$700(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagStrokeWidth:F
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$400(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->shadowRadius:F
    invoke-static {v3}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$800(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$700(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/videos/tagging/Tag$TagShape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$700(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagStrokeWidth:F
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$400(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$700(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/videos/tagging/Tag$TagShape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->shadowYOffset:F
    invoke-static {v2}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$500(Lcom/google/android/youtube/videos/tagging/TagsView;)F

    move-result v2

    neg-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$700(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->isActivated()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->activatedTagColor:I
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$900(Lcom/google/android/youtube/videos/tagging/TagsView;)I

    move-result v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$700(Lcom/google/android/youtube/videos/tagging/TagsView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/videos/tagging/Tag$TagShape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->this$0:Lcom/google/android/youtube/videos/tagging/TagsView;

    # getter for: Lcom/google/android/youtube/videos/tagging/TagsView;->tagColor:I
    invoke-static {v1}, Lcom/google/android/youtube/videos/tagging/TagsView;->access$1000(Lcom/google/android/youtube/videos/tagging/TagsView;)I

    move-result v1

    goto :goto_0
.end method

.method public startAppearAnimation(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/youtube/videos/tagging/TagsView$TagView;->startAppearAnimation(I)V

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->labelView:Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->labelView:Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/tagging/TagsView$TagShapeView;->labelView:Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/videos/tagging/TagsView$LabelView;->startAppearAnimation(I)V

    :cond_0
    return-void
.end method
