.class public Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;
.super Ljava/lang/Object;
.source "AssetsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/api/AssetsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field private static final ENCODED_COMMA:Ljava/lang/String;


# instance fields
.field private account:Ljava/lang/String;

.field private idCount:I

.field private final ids:Ljava/lang/StringBuilder;

.field private includeImages:Z

.field private includeMetadata:Z

.field private userCountry:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ","

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->ENCODED_COMMA:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    return-void
.end method


# virtual methods
.method public account(Ljava/lang/String;)Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "account cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->account:Ljava/lang/String;

    return-object p0
.end method

.method public addId(Ljava/lang/String;)Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "id cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->ENCODED_COMMA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->idCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->idCount:I

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/videos/api/AssetsRequest;
    .locals 6

    new-instance v0, Lcom/google/android/youtube/videos/api/AssetsRequest;

    iget-object v1, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->userCountry:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->includeMetadata:Z

    iget-boolean v5, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->includeImages:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/api/AssetsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public getIdCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->idCount:I

    return v0
.end method

.method public getIdsLengthIncluding(Ljava/lang/String;)I
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v2, "id cannot be empty"

    invoke-static {p1, v2}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->ids:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    sget-object v2, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->ENCODED_COMMA:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method public includeImages(Z)Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->includeImages:Z

    return-object p0
.end method

.method public userCountry(Ljava/lang/String;)Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "userCountry cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;->userCountry:Ljava/lang/String;

    return-object p0
.end method
