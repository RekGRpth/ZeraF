.class public interface abstract Lcom/google/android/youtube/videos/api/RecommendationsRequest$Factory;
.super Ljava/lang/Object;
.source "RecommendationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/api/RecommendationsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Factory"
.end annotation


# virtual methods
.method public abstract createForMovies(Ljava/lang/String;I)Lcom/google/android/youtube/videos/api/RecommendationsRequest;
.end method

.method public abstract createForRelatedMovies(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/youtube/videos/api/RecommendationsRequest;
.end method

.method public abstract createForShows(Ljava/lang/String;I)Lcom/google/android/youtube/videos/api/RecommendationsRequest;
.end method
