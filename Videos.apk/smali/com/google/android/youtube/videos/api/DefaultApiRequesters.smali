.class public Lcom/google/android/youtube/videos/api/DefaultApiRequesters;
.super Ljava/lang/Object;
.source "DefaultApiRequesters.java"

# interfaces
.implements Lcom/google/android/youtube/videos/api/ApiRequesters;


# instance fields
.field private final addToWishlistRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AddToWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final assetsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final gcmRegisterSyncRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final gcmUnregisterSyncRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmUnregisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final recommendationsRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final removeFromWishlistRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final videoGetRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field

.field private final videoUpdateRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/WishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;JLjava/util/concurrent/Executor;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)V
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # J
    .param p4    # Ljava/util/concurrent/Executor;
    .param p5    # Ljava/lang/String;
    .param p6    # Lorg/apache/http/client/HttpClient;
    .param p7    # Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "accountManagerWrapper can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "executor can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "persistentCachePath can\'t be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "baseApiUri can\'t be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p6, p7}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createVideoGetRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    invoke-direct {p0, p4, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->newAsyncRequester(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->videoGetRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, p1, p6, p7}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createVideoUpdateRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    invoke-direct {p0, p4, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->newAsyncRequester(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->videoUpdateRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createGcmRegisterRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;JLorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->gcmRegisterSyncRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createGcmUnregisterRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;JLorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->gcmUnregisterSyncRequester:Lcom/google/android/youtube/core/async/Requester;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createCachingRecommendationsRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->recommendationsRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, p1, p6, p7}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createWishlistRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->wishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, p1, p6, p7}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createAddToWishlistRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->addToWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, p1, p6, p7}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createRemoveFromWishlistRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->removeFromWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {p0, p1, p6, p7}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createAssetsRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    invoke-direct {p0, p4, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->newAsyncRequester(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->assetsRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method private createAddToWishlistRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .param p3    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AddToWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/AddToWishlistConverter;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/videos/api/AddToWishlistConverter;-><init>(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createVoidResponseApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/youtube/videos/api/Request;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;",
            "Lcom/google/android/youtube/core/converter/ResponseConverter",
            "<[BTE;>;)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/core/converter/http/HttpToBytesResponseConverter;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/HttpToBytesResponseConverter;-><init>()V

    invoke-direct {p0, p2, v1}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->newHttpRequester(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v0

    invoke-static {p1, p3, v0}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->create(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    move-result-object v1

    invoke-static {v1, p4}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createAssetsRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .param p3    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/AssetListConverter;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/videos/api/AssetListConverter;-><init>(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createCachingRecommendationsRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/apache/http/client/HttpClient;
    .param p5    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/lang/String;",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation

    new-instance v3, Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;

    const-string v4, ".rec"

    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->newBuilder()Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse$Builder;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/youtube/videos/ProtoParser;->create(Lcom/google/protobuf/GeneratedMessageLite$Builder;)Lcom/google/android/youtube/videos/ProtoParser;

    move-result-object v5

    invoke-direct {v3, p3, v4, v5}, Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/ProtoParser;)V

    invoke-virtual {v3, p2}, Lcom/google/android/youtube/core/cache/TimestampedProtosPersistentCache;->init(Ljava/util/concurrent/Executor;)Lcom/google/android/youtube/core/cache/PersistentCache;

    move-result-object v0

    invoke-direct {p0, p1, p4, p5}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createSyncRecommendationsRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v2

    const-wide/32 v3, 0xa4cb800

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->newAsyncRequester(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v3

    return-object v3
.end method

.method private createGcmRegisterRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;JLorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # J
    .param p4    # Lorg/apache/http/client/HttpClient;
    .param p5    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "J",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;

    invoke-direct {v0, p5, p2, p3}, Lcom/google/android/youtube/videos/api/GcmRegisterConverter;-><init>(Landroid/net/Uri;J)V

    invoke-direct {p0, p1, p4, v0, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createGcmUnregisterRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;JLorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # J
    .param p4    # Lorg/apache/http/client/HttpClient;
    .param p5    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "J",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmUnregisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/GcmUnregisterConverter;

    invoke-direct {v0, p5, p2, p3}, Lcom/google/android/youtube/videos/api/GcmUnregisterConverter;-><init>(Landroid/net/Uri;J)V

    invoke-direct {p0, p1, p4, v0, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createRemoveFromWishlistRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .param p3    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/RemoveFromWishlistConverter;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/videos/api/RemoveFromWishlistConverter;-><init>(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createVoidResponseApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createSyncRecommendationsRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .param p3    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/RecommendationsConverter;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/videos/api/RecommendationsConverter;-><init>(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createVideoGetRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .param p3    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/VideoGetConverter;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/videos/api/VideoGetConverter;-><init>(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createVideoUpdateRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .param p3    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/VideoUpdateConverter;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/videos/api/VideoUpdateConverter;-><init>(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private createVoidResponseApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/google/android/youtube/videos/api/Request;",
            ">(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;->VOID:Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;

    invoke-direct {p0, p2, v1}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->newHttpRequester(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;

    move-result-object v0

    invoke-static {p1, p3, v0}, Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;->create(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/videos/api/SimpleAuthenticatedHttpRequester;

    move-result-object v1

    return-object v1
.end method

.method private createWishlistRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Landroid/net/Uri;)Lcom/google/android/youtube/core/async/Requester;
    .locals 2
    .param p1    # Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .param p3    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;",
            "Lorg/apache/http/client/HttpClient;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/WishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/videos/api/WishlistConverter;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/videos/api/WishlistConverter;-><init>(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->createApiRequester(Lcom/google/android/youtube/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/ResponseConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    return-object v1
.end method

.method private newAsyncRequester(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;)",
            "Lcom/google/android/youtube/core/async/AsyncRequester",
            "<TR;TE;>;"
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/google/android/youtube/core/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/Requester;)Lcom/google/android/youtube/core/async/AsyncRequester;

    move-result-object v0

    return-object v0
.end method

.method private newCachingRequester(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/youtube/core/cache/Cache",
            "<TR;",
            "Lcom/google/android/youtube/core/async/Timestamped",
            "<TE;>;>;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;J)",
            "Lcom/google/android/youtube/core/async/Requester",
            "<TR;TE;>;"
        }
    .end annotation

    invoke-static {p1, p2, p3, p4}, Lcom/google/android/youtube/core/async/TimestampedCachingRequester;->create(Lcom/google/android/youtube/core/cache/Cache;Lcom/google/android/youtube/core/async/Requester;J)Lcom/google/android/youtube/core/async/TimestampedCachingRequester;

    move-result-object v0

    return-object v0
.end method

.method private newHttpRequester(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)Lcom/google/android/youtube/core/async/HttpRequester;
    .locals 2
    .param p1    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/youtube/core/converter/http/HttpResponseConverter",
            "<TE;>;)",
            "Lcom/google/android/youtube/core/async/HttpRequester",
            "<TR;TE;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/youtube/core/async/HttpRequester;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/youtube/core/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)V

    return-object v0
.end method


# virtual methods
.method public getAddToWishlistSyncRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AddToWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->addToWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getAssetsRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->assetsRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getGcmRegisterSyncRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmRegisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->gcmRegisterSyncRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getGcmUnregisterSyncRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/GcmUnregisterRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->gcmUnregisterSyncRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getRecommendationsRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->recommendationsRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getRemoveFromWishlistSyncRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->removeFromWishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getVideoGetRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->videoGetRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getVideoUpdateRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->videoUpdateRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method

.method public getWishlistSyncRequester()Lcom/google/android/youtube/core/async/Requester;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/WishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/DefaultApiRequesters;->wishlistRequester:Lcom/google/android/youtube/core/async/Requester;

    return-object v0
.end method
