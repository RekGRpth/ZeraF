.class public Lcom/google/android/youtube/videos/api/AssetsRequest;
.super Lcom/google/android/youtube/videos/api/Request;
.source "AssetsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/api/AssetsRequest$Builder;
    }
.end annotation


# instance fields
.field public final encodedIds:Ljava/lang/String;

.field public final includeImages:Z

.field public final includeMetadata:Z

.field public final userCountry:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/api/Request;-><init>(Ljava/lang/String;)V

    const-string v0, "encodedIds cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest;->encodedIds:Ljava/lang/String;

    const-string v0, "userCountry cannot be empty"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/AssetsRequest;->userCountry:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/youtube/videos/api/AssetsRequest;->includeMetadata:Z

    iput-boolean p5, p0, Lcom/google/android/youtube/videos/api/AssetsRequest;->includeImages:Z

    return-void
.end method
