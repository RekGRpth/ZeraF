.class public interface abstract Lcom/google/android/youtube/videos/api/ApiRequesters;
.super Ljava/lang/Object;
.source "ApiRequesters.java"


# virtual methods
.method public abstract getAddToWishlistSyncRequester()Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/AddToWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRecommendationsRequester()Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRemoveFromWishlistSyncRequester()Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/RemoveFromWishlistRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVideoGetRequester()Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVideoUpdateRequester()Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/VideoUpdateRequest;",
            "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getWishlistSyncRequester()Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/api/WishlistRequest;",
            "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
            ">;"
        }
    .end annotation
.end method
