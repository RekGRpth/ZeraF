.class public Lcom/google/android/youtube/videos/api/VideoGetConverter;
.super Ljava/lang/Object;
.source "VideoGetConverter.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;
.implements Lcom/google/android/youtube/core/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Lcom/google/android/youtube/videos/api/VideoGetRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;",
        "Lcom/google/android/youtube/core/converter/ResponseConverter",
        "<[B",
        "Lcom/google/wireless/android/video/magma/proto/VideoResource;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Landroid/net/Uri;

.field private final converter:Lcom/google/android/youtube/core/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/converter/RequestConverter",
            "<",
            "Lcom/google/android/youtube/core/async/NetworkRequest;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/api/VideoGetConverter;->baseUri:Landroid/net/Uri;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/NetworkRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/api/VideoGetConverter;->converter:Lcom/google/android/youtube/core/converter/RequestConverter;

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/api/VideoGetRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/api/VideoGetConverter;->convertRequest(Lcom/google/android/youtube/videos/api/VideoGetRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/youtube/videos/api/VideoGetRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/api/VideoGetRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/videos/api/VideoGetConverter;->converter:Lcom/google/android/youtube/core/converter/RequestConverter;

    new-instance v1, Lcom/google/android/youtube/core/async/NetworkRequest;

    iget-object v2, p0, Lcom/google/android/youtube/videos/api/VideoGetConverter;->baseUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "video"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/youtube/videos/api/VideoGetRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2, v4, v4, v4}, Lcom/google/android/youtube/core/async/NetworkRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/videos/accounts/UserAuth;Ljava/util/Map;[B)V

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    return-object v0
.end method

.method public convertResponse([B)Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->parseFrom([B)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/api/VideoGetConverter;->convertResponse([B)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method
