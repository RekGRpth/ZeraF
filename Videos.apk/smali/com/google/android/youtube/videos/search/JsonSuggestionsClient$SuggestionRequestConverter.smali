.class Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequestConverter;
.super Ljava/lang/Object;
.source "JsonSuggestionsClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SuggestionRequestConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/converter/RequestConverter",
        "<",
        "Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# static fields
.field private static final BASE_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://market.android.com/suggest/SuggRequest"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequestConverter;->BASE_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$1;)V
    .locals 0
    .param p1    # Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$1;

    invoke-direct {p0}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequestConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public convertRequest(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;)Landroid/net/Uri;
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;

    sget-object v1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequestConverter;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "query"

    iget-object v3, p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->query:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "json"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "hl"

    iget-object v3, p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "gl"

    iget-object v3, p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ct"

    iget-object v3, p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->country:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "c"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "type"

    const-string v3, "aq"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "max_results"

    iget v3, p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->limit:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/youtube/core/converter/ConverterException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequestConverter;->convertRequest(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
