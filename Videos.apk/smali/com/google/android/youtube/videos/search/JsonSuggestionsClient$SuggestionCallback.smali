.class Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;
.super Ljava/lang/Object;
.source "JsonSuggestionsClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SuggestionCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;",
        "Lorg/json/JSONArray;",
        ">;"
    }
.end annotation


# static fields
.field private static final BASE_THUMBNAIL_URI:Landroid/net/Uri;


# instance fields
.field private final context:Landroid/content/Context;

.field private final targetCallback:Lcom/google/android/youtube/core/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final videoRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.videos"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "thumbnail/suggest"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->BASE_THUMBNAIL_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->videoRequester:Lcom/google/android/youtube/core/async/Requester;

    iput-object p3, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->targetCallback:Lcom/google/android/youtube/core/async/Callback;

    return-void
.end method

.method private convertSuggestion(Lorg/json/JSONObject;Ljava/util/Locale;)[Ljava/lang/Object;
    .locals 4
    .param p1    # Lorg/json/JSONObject;
    .param p2    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v3, "s"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "p"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v3, "u"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v3, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->BASE_THUMBNAIL_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    const-string v3, "movie-"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v2, v1, p2}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->newMovieSuggestion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)[Ljava/lang/Object;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_1
    const-string v3, "tvshow-"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v2, v1}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->newShowSuggestion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->newSearchSuggestion(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v3

    goto :goto_0
.end method

.method private findCheapestPrice(Ljava/util/List;)Lcom/google/android/youtube/core/model/Money;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/youtube/core/model/PricingStructure;",
            ">;)",
            "Lcom/google/android/youtube/core/model/Money;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/model/PricingStructure;

    if-eqz v0, :cond_1

    iget-object v3, v2, Lcom/google/android/youtube/core/model/PricingStructure;->price:Lcom/google/android/youtube/core/model/Money;

    iget v3, v3, Lcom/google/android/youtube/core/model/Money;->value:I

    iget v4, v0, Lcom/google/android/youtube/core/model/Money;->value:I

    if-ge v3, v4, :cond_0

    :cond_1
    iget-object v0, v2, Lcom/google/android/youtube/core/model/PricingStructure;->price:Lcom/google/android/youtube/core/model/Money;

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private format(Lcom/google/android/youtube/core/model/Money;Ljava/util/Locale;)Ljava/lang/String;
    .locals 5
    .param p1    # Lcom/google/android/youtube/core/model/Money;
    .param p2    # Ljava/util/Locale;

    invoke-static {p2}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Money;->currency:Ljava/util/Currency;

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Money;->currency:Ljava/util/Currency;

    invoke-virtual {v1}, Ljava/util/Currency;->getDefaultFractionDigits()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Money;->currency:Ljava/util/Currency;

    invoke-virtual {v1}, Ljava/util/Currency;->getDefaultFractionDigits()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    iget v1, p1, Lcom/google/android/youtube/core/model/Money;->value:I

    int-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getPricingInfo(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Locale;

    invoke-static {}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->create()Lcom/google/android/youtube/videos/async/CallbackAsFuture;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->videoRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v4, p1, v0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/model/Video;

    iget-object v4, v3, Lcom/google/android/youtube/core/model/Video;->pricing:Ljava/util/List;

    invoke-direct {p0, v4}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->findCheapestPrice(Ljava/util/List;)Lcom/google/android/youtube/core/model/Money;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->context:Landroid/content/Context;

    const v5, 0x7f0a00bd

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-direct {p0, v2, p2}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->format(Lcom/google/android/youtube/core/model/Money;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :goto_0
    return-object v4

    :catch_0
    move-exception v1

    const-string v4, "can\'t fetch price"

    invoke-static {v4, v1}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->context:Landroid/content/Context;

    const v5, 0x7f0a00be

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private newMovieSuggestion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)[Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/util/Locale;

    # getter for: Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->PLAY_STORE_BASE_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->access$300()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "movies"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "details"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-direct {p0, p1, p4}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->getPricingInfo(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p3

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->newRow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private newRow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;

    # getter for: Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->SUGGEST_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->access$200()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x2

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x5

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p5, v0, v1

    const/4 v1, 0x4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method private newSearchSuggestion(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/String;

    # getter for: Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->PLAY_STORE_BASE_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->access$300()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "search"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "q"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "c"

    const-string v2, "movies"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    const-string v1, "android.resource://android/17170445"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->newRow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private newShowSuggestion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    # getter for: Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->PLAY_STORE_BASE_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->access$300()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "tv"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "show"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->context:Landroid/content/Context;

    const v1, 0x7f0a00be

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p3

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->newRow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->targetCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->onError(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;Lorg/json/JSONArray;)V
    .locals 6
    .param p1    # Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;
    .param p2    # Lorg/json/JSONArray;

    :try_start_0
    new-instance v2, Landroid/database/MatrixCursor;

    # getter for: Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->SUGGEST_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->access$200()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    invoke-virtual {p2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;->locale:Ljava/util/Locale;

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->convertSuggestion(Lorg/json/JSONObject;Ljava/util/Locale;)[Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->targetCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v4, p1, v2}, Lcom/google/android/youtube/core/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    iget-object v4, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->targetCallback:Lcom/google/android/youtube/core/async/Callback;

    invoke-interface {v4, p1, v0}, Lcom/google/android/youtube/core/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;

    check-cast p2, Lorg/json/JSONArray;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;->onResponse(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;Lorg/json/JSONArray;)V

    return-void
.end method
