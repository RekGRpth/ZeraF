.class public Lcom/google/android/youtube/videos/search/SuggestionsProvider;
.super Landroid/content/ContentProvider;
.source "SuggestionsProvider.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/ContentProvider;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Landroid/net/Uri;",
        "[B>;"
    }
.end annotation


# static fields
.field private static final SUGGEST_COLUMNS:[Ljava/lang/String;


# instance fields
.field private application:Lcom/google/android/youtube/videos/VideosApplication;

.field private purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private suggestionsClient:Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;

.field private syncBitmapCacheRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation
.end field

.field private syncBitmapRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_icon_2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "suggest_shortcut_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "suggest_last_access_hint"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "suggest_flags"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->SUGGEST_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private static createShowSearchProjection()[Ljava/lang/String;
    .locals 6

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "com.google.android.videos"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "thumbnail/show"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http://www.youtube.com/show/?p="

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "show_title AS suggest_text_1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/\' || "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "show_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "suggest_icon_1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "\'android.intent.action.VIEW\' AS suggest_intent_action"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' || "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "show_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "suggest_intent_data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "account AS suggest_intent_extra_data"

    aput-object v4, v2, v3

    return-object v2
.end method

.method private fetchSuggestionBitmaps(Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "thumbnail/suggest"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->syncBitmapRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v2, v1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    iget-object v2, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->syncBitmapCacheRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v2, v1, p0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    invoke-virtual {p1, v2}, Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;->moveToPosition(I)Z

    return-void
.end method

.method private requestMoviePurchasesCursor(Ljava/lang/String;I)Ljava/util/concurrent/Future;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/concurrent/Future",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getSignInManager()Lcom/google/android/youtube/videos/accounts/SignInManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v2, p1

    move v3, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createMoviesSearchRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/youtube/videos/store/CancellationSignalHolder;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v8

    invoke-static {}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->create()Lcom/google/android/youtube/videos/async/CallbackAsFuture;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    invoke-virtual {v1, v8, v7}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-object v7
.end method

.method private requestShowPurchasesCursor(Ljava/lang/String;I)Ljava/util/concurrent/Future;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/concurrent/Future",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getSignInManager()Lcom/google/android/youtube/videos/accounts/SignInManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->createShowSearchProjection()[Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v2, p1

    move v3, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createShowsSearchRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/youtube/videos/store/CancellationSignalHolder;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v8

    invoke-static {}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->create()Lcom/google/android/youtube/videos/async/CallbackAsFuture;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    invoke-virtual {v1, v8, v7}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-object v7
.end method

.method private requestSuggestionsCursor(Ljava/lang/String;I)Ljava/util/concurrent/Future;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/concurrent/Future",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    new-instance v1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getConfig()Lcom/google/android/youtube/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p1, p2, v2, v3}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;-><init>(Ljava/lang/String;ILjava/util/Locale;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/youtube/videos/async/CallbackAsFuture;->create()Lcom/google/android/youtube/videos/async/CallbackAsFuture;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->suggestionsClient:Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->request(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    iput-object v0, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->init()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStore()Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    new-instance v0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;

    iget-object v1, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v2, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/videos/VideosApplication;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/videos/Requesters;->getSyncVideoRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;-><init>(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/Requester;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->suggestionsClient:Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Requesters;->getSyncBitmapBytesRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->syncBitmapRequester:Lcom/google/android/youtube/core/async/Requester;

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Requesters;->getSyncBitmapBytesCacheRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->syncBitmapCacheRequester:Lcom/google/android/youtube/core/async/Requester;

    const/4 v0, 0x1

    return v0
.end method

.method public onError(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/Exception;

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->onError(Landroid/net/Uri;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Landroid/net/Uri;[B)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # [B

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Landroid/net/Uri;

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->onResponse(Landroid/net/Uri;[B)V

    return-void
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v7, "limit"

    invoke-virtual {p1, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0xa

    invoke-static {v7, v8}, Lcom/google/android/youtube/core/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v3

    new-instance v6, Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;

    sget-object v7, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->SUGGEST_COLUMNS:[Ljava/lang/String;

    invoke-direct {v6, v7}, Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;-><init>([Ljava/lang/String;)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v4, v3}, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->requestMoviePurchasesCursor(Ljava/lang/String;I)Ljava/util/concurrent/Future;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v4, v3}, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->requestShowPurchasesCursor(Ljava/lang/String;I)Ljava/util/concurrent/Future;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v4, v3}, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->requestSuggestionsCursor(Ljava/lang/String;I)Ljava/util/concurrent/Future;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Future;

    const-wide/16 v7, 0x1388

    :try_start_0
    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v7, v8, v9}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/database/Cursor;

    invoke-virtual {v6, v7}, Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;->add(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v7, "suggestion query failed"

    invoke-static {v7, v0}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/search/SuggestionsProvider;->fetchSuggestionBitmaps(Lcom/google/android/youtube/videos/search/SuggestionsProvider$SuggestionsCursor;)V

    return-object v6
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
