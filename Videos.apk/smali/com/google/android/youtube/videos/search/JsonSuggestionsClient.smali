.class Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;
.super Ljava/lang/Object;
.source "JsonSuggestionsClient.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$1;,
        Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;,
        Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$JSONArrayResponseConverter;,
        Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequestConverter;,
        Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Requester",
        "<",
        "Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final PLAY_STORE_BASE_URI:Landroid/net/Uri;

.field private static final SUGGEST_COLUMNS:[Ljava/lang/String;


# instance fields
.field private final context:Landroid/content/Context;

.field private final suggestionRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;",
            "Lorg/json/JSONArray;",
            ">;"
        }
    .end annotation
.end field

.field private final videoRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "http://play.google.com/store/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->PLAY_STORE_BASE_URI:Landroid/net/Uri;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_flags"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->SUGGEST_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/Requester;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lorg/apache/http/client/HttpClient;",
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/youtube/core/model/Video;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->videoRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v0, Lcom/google/android/youtube/core/async/HttpRequester;

    new-instance v1, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;

    sget-object v2, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/converter/http/UriRequestConverter;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    new-instance v2, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$JSONArrayResponseConverter;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$JSONArrayResponseConverter;-><init>(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$1;)V

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/youtube/core/async/HttpRequester;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/RequestConverter;Lcom/google/android/youtube/core/converter/http/HttpResponseConverter;)V

    new-instance v1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequestConverter;

    invoke-direct {v1, v3}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequestConverter;-><init>(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$1;)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->suggestionRequester:Lcom/google/android/youtube/core/async/Requester;

    return-void
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->SUGGEST_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->PLAY_STORE_BASE_URI:Landroid/net/Uri;

    return-object v0
.end method


# virtual methods
.method public request(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 4
    .param p1    # Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;",
            "Lcom/google/android/youtube/core/async/Callback",
            "<",
            "Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->suggestionRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;

    iget-object v2, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->videoRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-direct {v1, v2, v3, p2}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionCallback;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/youtube/core/async/Callback;

    check-cast p1, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/search/JsonSuggestionsClient;->request(Lcom/google/android/youtube/videos/search/JsonSuggestionsClient$SuggestionRequest;Lcom/google/android/youtube/core/async/Callback;)V

    return-void
.end method
