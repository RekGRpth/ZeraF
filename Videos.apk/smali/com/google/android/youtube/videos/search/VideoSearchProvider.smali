.class public Lcom/google/android/youtube/videos/search/VideoSearchProvider;
.super Landroid/content/ContentProvider;
.source "VideoSearchProvider.java"


# static fields
.field protected static final PROJECTION:[Ljava/lang/String;

.field private static final URI_MATCHER:Landroid/content/UriMatcher;


# instance fields
.field private application:Lcom/google/android/youtube/videos/VideosApplication;

.field private bitmapConverter:Lcom/google/android/youtube/core/v9/BytesToPipeConverter;

.field private purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

.field private syncBitmapBytesCacheRequester:Lcom/google/android/youtube/core/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/youtube/core/async/Requester",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->createUriMatcher()Landroid/content/UriMatcher;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-static {}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->createSearchProjection()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private static createSearchProjection()[Ljava/lang/String;
    .locals 9

    const-string v3, "suggest_last_access_hint"

    new-instance v5, Landroid/net/Uri$Builder;

    invoke-direct {v5}, Landroid/net/Uri$Builder;-><init>()V

    const-string v6, "content"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "com.google.android.videos"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "thumbnail"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http://www.youtube.com/watch/?v="

    const-string v2, "&season_id=tvseason-"

    const-string v4, "max(purchase_timestamp,last_watched_timestamp)"

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "video_id AS _id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "title AS suggest_text_1"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/\' || "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "video_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "suggest_icon_1"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "\'android.intent.action.VIEW\' AS suggest_intent_action"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' || "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "video_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " || ifnull(\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' || "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "episode_season_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", \'\')"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "suggest_intent_data"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "account AS suggest_intent_extra_data"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "video_id AS suggest_shortcut_id"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    return-object v5
.end method

.method private static final createUriMatcher()Landroid/content/UriMatcher;
    .locals 5

    const/4 v4, 0x3

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    const-string v1, "com.google.android.videos"

    const-string v2, "search_suggest_query"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v1, "com.google.android.videos"

    const-string v2, "search_suggest_query/*"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v1, "com.google.android.videos"

    const-string v2, "search_suggest_shortcut/*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v1, "com.google.android.videos"

    const-string v2, "thumbnail/show/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v1, "com.google.android.videos"

    const-string v2, "thumbnail/suggest/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v1, "com.google.android.videos"

    const-string v2, "thumbnail/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method private enforceGlobalSearchPermssion()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    const-string v1, "android.permission.GLOBAL_SEARCH"

    const-string v2, "insufficient permission"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/videos/VideosApplication;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private getLimit(Landroid/net/Uri;)I
    .locals 2
    .param p1    # Landroid/net/Uri;

    const-string v0, "limit"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getPurchaseCursor(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;
    .locals 5
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    :try_start_0
    new-instance v0, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v0}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    invoke-virtual {v3, p1, v0}, Lcom/google/android/youtube/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/youtube/core/async/Callback;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v3

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    instance-of v3, v3, Landroid/os/OperationCanceledException;

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    check-cast v3, Landroid/os/OperationCanceledException;

    throw v3

    :cond_0
    const-string v3, "Exception fetching purchases."

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "Exception fetching purchases."

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private makeThumbnailRequest(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .locals 5
    .param p1    # Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/youtube/core/async/SyncCallback;

    invoke-direct {v0}, Lcom/google/android/youtube/core/async/SyncCallback;-><init>()V

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "thumbnail/suggest"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->syncBitmapBytesCacheRequester:Lcom/google/android/youtube/core/async/Requester;

    invoke-interface {v3, v2, v0}, Lcom/google/android/youtube/core/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/youtube/core/async/Callback;)V

    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->bitmapConverter:Lcom/google/android/youtube/core/v9/BytesToPipeConverter;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    invoke-virtual {v4, v3}, Lcom/google/android/youtube/core/v9/BytesToPipeConverter;->convertResponse([B)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "thumbnail/show"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getPosterStore()Lcom/google/android/youtube/videos/store/PosterStore;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/google/android/youtube/videos/store/PosterStore;->getShowPosterBytes(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/videos/VideosApplication;->getPosterStore()Lcom/google/android/youtube/videos/store/PosterStore;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/google/android/youtube/videos/store/PosterStore;->getVideoPosterBytes(Ljava/lang/String;Lcom/google/android/youtube/core/async/Callback;)V

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception fetching icon bytes for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    const/4 v3, 0x0

    goto :goto_1

    :catch_1
    move-exception v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception converting icon bytes for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/youtube/core/converter/ConverterException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->enforceGlobalSearchPermssion()V

    sget-object v2, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->getLimit(Landroid/net/Uri;)I

    move-result v2

    invoke-direct {p0, v1, v2, p6}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->querySearch(Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->getLimit(Landroid/net/Uri;)I

    move-result v1

    invoke-direct {p0, v1, p6}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->zeroQuerySearch(ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->shortcutCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private querySearch(Ljava/lang/String;ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    sget-object v0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createActivePurchasesRequestForGlobalSearch([Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/youtube/videos/store/CancellationSignalHolder;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->getPurchaseCursor(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private shortcutCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, p1, v2, v3}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createActivePurchaseRequestForGlobalSearch([Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->getPurchaseCursor(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private zeroQuerySearch(ILcom/google/android/youtube/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    sget-object v1, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, p1, v2, v3, p2}, Lcom/google/android/youtube/videos/store/PurchaseRequests;->createNowPlayingRequestForGlobalSearch([Ljava/lang/String;IJLcom/google/android/youtube/videos/store/CancellationSignalHolder;)Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->getPurchaseCursor(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/videos/VideosApplication;

    iput-object v0, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->init()V

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getPurchaseStore()Lcom/google/android/youtube/videos/store/PurchaseStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->purchaseStore:Lcom/google/android/youtube/videos/store/PurchaseStore;

    iget-object v0, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/videos/VideosApplication;->getRequesters()Lcom/google/android/youtube/videos/Requesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/videos/Requesters;->getSyncBitmapBytesCacheRequester()Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/videos/search/VideoSearchProvider$1;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/videos/search/VideoSearchProvider$1;-><init>(Lcom/google/android/youtube/videos/search/VideoSearchProvider;)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/ConvertingRequester;->create(Lcom/google/android/youtube/core/async/Requester;Lcom/google/android/youtube/core/converter/RequestConverter;)Lcom/google/android/youtube/core/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->syncBitmapBytesCacheRequester:Lcom/google/android/youtube/core/async/Requester;

    new-instance v0, Lcom/google/android/youtube/core/v9/BytesToPipeConverter;

    iget-object v1, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->application:Lcom/google/android/youtube/videos/VideosApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/videos/VideosApplication;->getExecutor()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/v9/BytesToPipeConverter;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->bitmapConverter:Lcom/google/android/youtube/core/v9/BytesToPipeConverter;

    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->makeThumbnailRequest(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/os/CancellationSignal;

    if-nez p6, :cond_0

    const/4 v6, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/videos/search/VideoSearchProvider;->queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v6, Lcom/google/android/youtube/videos/store/CancellationSignalHolder;

    invoke-direct {v6, p6}, Lcom/google/android/youtube/videos/store/CancellationSignalHolder;-><init>(Landroid/os/CancellationSignal;)V

    goto :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
