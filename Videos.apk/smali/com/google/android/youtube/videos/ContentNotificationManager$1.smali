.class Lcom/google/android/youtube/videos/ContentNotificationManager$1;
.super Ljava/lang/Object;
.source "ContentNotificationManager.java"

# interfaces
.implements Lcom/google/android/youtube/core/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/youtube/videos/ContentNotificationManager;->checkForNewEpisodes(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/youtube/core/async/Callback",
        "<",
        "Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/youtube/videos/ContentNotificationManager;

.field final synthetic val$seasonId:Ljava/lang/String;

.field final synthetic val$showId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/videos/ContentNotificationManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$1;->this$0:Lcom/google/android/youtube/videos/ContentNotificationManager;

    iput-object p2, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$1;->val$seasonId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$1;->val$showId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Ljava/lang/Exception;

    const-string v0, "Error when fetching new episode entries."

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Exception;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ContentNotificationManager$1;->onError(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;
    .param p2    # Landroid/database/Cursor;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$1;->this$0:Lcom/google/android/youtube/videos/ContentNotificationManager;

    iget-object v1, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$1;->val$seasonId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/videos/ContentNotificationManager$1;->val$showId:Ljava/lang/String;

    # invokes: Lcom/google/android/youtube/videos/ContentNotificationManager;->processVideoEntries(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, p2, v1, v2}, Lcom/google/android/youtube/videos/ContentNotificationManager;->access$000(Lcom/google/android/youtube/videos/ContentNotificationManager;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/videos/ContentNotificationManager$1;->onResponse(Lcom/google/android/youtube/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
