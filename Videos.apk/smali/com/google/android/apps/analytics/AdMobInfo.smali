.class public Lcom/google/android/apps/analytics/AdMobInfo;
.super Ljava/lang/Object;


# static fields
.field private static final INSTANCE:Lcom/google/android/apps/analytics/AdMobInfo;


# instance fields
.field private adHitId:I

.field private random:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/analytics/AdMobInfo;

    invoke-direct {v0}, Lcom/google/android/apps/analytics/AdMobInfo;-><init>()V

    sput-object v0, Lcom/google/android/apps/analytics/AdMobInfo;->INSTANCE:Lcom/google/android/apps/analytics/AdMobInfo;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/analytics/AdMobInfo;->random:Ljava/util/Random;

    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/analytics/AdMobInfo;
    .locals 1

    sget-object v0, Lcom/google/android/apps/analytics/AdMobInfo;->INSTANCE:Lcom/google/android/apps/analytics/AdMobInfo;

    return-object v0
.end method


# virtual methods
.method public generateAdHitId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/AdMobInfo;->random:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/analytics/AdMobInfo;->adHitId:I

    iget v0, p0, Lcom/google/android/apps/analytics/AdMobInfo;->adHitId:I

    return v0
.end method
