.class interface abstract Lcom/google/android/apps/analytics/HitStore;
.super Ljava/lang/Object;


# virtual methods
.method public abstract deleteHit(J)V
.end method

.method public abstract getNumStoredHits()I
.end method

.method public abstract peekHits()[Lcom/google/android/apps/analytics/Hit;
.end method

.method public abstract putEvent(Lcom/google/android/apps/analytics/Event;)V
.end method

.method public abstract setAnonymizeIp(Z)V
.end method

.method public abstract setReferrer(Ljava/lang/String;)Z
.end method

.method public abstract setSampleRate(I)V
.end method

.method public abstract startNewVisit()V
.end method
