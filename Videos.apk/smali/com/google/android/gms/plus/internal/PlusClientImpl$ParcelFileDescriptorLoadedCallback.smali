.class final Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;
.super Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ParcelFileDescriptorLoadedCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/gms/plus/internal/IPlusService;",
        ">.CallbackProxy<",
        "Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final mPfd:Landroid/os/ParcelFileDescriptor;

.field private final mStatus:Lcom/google/android/gms/common/ConnectionResult;

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;)V
    .locals 0
    .param p2    # Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;
    .param p3    # Lcom/google/android/gms/common/ConnectionResult;
    .param p4    # Landroid/os/ParcelFileDescriptor;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;->this$0:Lcom/google/android/gms/plus/internal/PlusClientImpl;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;-><init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    iput-object p4, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;->mPfd:Landroid/os/ParcelFileDescriptor;

    return-void
.end method


# virtual methods
.method public deliverCallback(Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;->mStatus:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;->mPfd:Landroid/os/ParcelFileDescriptor;

    invoke-interface {p1, v1, v2}, Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;->onParcelFileDescriptorLoaded(Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;->mPfd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PlusClientImpl"

    const-string v2, "failed close"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public bridge synthetic deliverCallback(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;->deliverCallback(Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;)V

    return-void
.end method

.method public removeListener()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;->removeListener()V

    return-void
.end method
