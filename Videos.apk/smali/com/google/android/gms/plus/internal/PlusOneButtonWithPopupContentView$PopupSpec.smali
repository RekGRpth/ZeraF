.class final Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;
.super Ljava/lang/Object;
.source "PlusOneButtonWithPopupContentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PopupSpec"
.end annotation


# instance fields
.field public final beakGravity:I

.field public final beakMarginBottom:I

.field public final beakMarginLeft:I

.field public final beakMarginRight:I

.field public final beakMarginTop:I

.field public final popupX:I

.field public final popupY:I

.field public final showPopupAboveButton:Z

.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Landroid/widget/FrameLayout;)V
    .locals 11
    .param p2    # Landroid/widget/FrameLayout;

    const/4 v8, 0x1

    const/4 v9, 0x0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v9, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginTop:I

    iput v9, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginBottom:I

    invoke-virtual {p2, v9, v9}, Landroid/widget/FrameLayout;->measure(II)V

    const/4 v7, 0x2

    new-array v4, v7, [I

    iget-object v7, p1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v4}, Landroid/widget/FrameLayout;->getLocationOnScreen([I)V

    aget v5, v4, v9

    aget v6, v4, v8

    iget-object v7, p1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v6

    iget-object v10, p1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v10}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v10

    sub-int v3, v7, v10

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v3

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->getActionBarHeight()I

    move-result v10

    add-int/2addr v7, v10

    # getter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mDisplay:Landroid/view/Display;
    invoke-static {p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$500(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getHeight()I

    move-result v10

    if-le v7, v10, :cond_1

    move v7, v8

    :goto_0
    iput-boolean v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->showPopupAboveButton:Z

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v5

    # getter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mDisplay:Landroid/view/Display;
    invoke-static {p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$500(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getWidth()I

    move-result v10

    if-ge v7, v10, :cond_2

    move v2, v8

    :goto_1
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v5

    iget-object v10, p1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v10}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v7, v10

    # getter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mDisplay:Landroid/view/Display;
    invoke-static {p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$500(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getWidth()I

    move-result v10

    if-ge v7, v10, :cond_3

    move v0, v8

    :goto_2
    iget-object v7, p1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    # getter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPopupBeak:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$600(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int v1, v7, v8

    if-eqz v2, :cond_5

    iget-boolean v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->showPopupAboveButton:Z

    if-eqz v7, :cond_4

    const/16 v7, 0x50

    iput v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakGravity:I

    :goto_3
    iput v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginLeft:I

    iput v9, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginRight:I

    :goto_4
    if-eqz v2, :cond_9

    iput v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->popupX:I

    :goto_5
    iget-boolean v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->showPopupAboveButton:Z

    if-eqz v7, :cond_0

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v6, v7

    iget-object v8, p1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getPaddingTop()I

    move-result v8

    sub-int v3, v7, v8

    :cond_0
    iput v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->popupY:I

    return-void

    :cond_1
    move v7, v9

    goto :goto_0

    :cond_2
    move v2, v9

    goto :goto_1

    :cond_3
    move v0, v9

    goto :goto_2

    :cond_4
    const/16 v7, 0x30

    iput v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakGravity:I

    goto :goto_3

    :cond_5
    if-eqz v0, :cond_7

    iget-boolean v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->showPopupAboveButton:Z

    if-eqz v7, :cond_6

    const/16 v7, 0x51

    iput v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakGravity:I

    :goto_6
    iput v9, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginLeft:I

    iput v9, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginRight:I

    goto :goto_4

    :cond_6
    const/16 v7, 0x31

    iput v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakGravity:I

    goto :goto_6

    :cond_7
    iget-boolean v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->showPopupAboveButton:Z

    if-eqz v7, :cond_8

    const/16 v7, 0x55

    iput v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakGravity:I

    :goto_7
    iput v9, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginLeft:I

    iput v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakMarginRight:I

    goto :goto_4

    :cond_8
    const/16 v7, 0x35

    iput v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->beakGravity:I

    goto :goto_7

    :cond_9
    if-eqz v0, :cond_a

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int v7, v5, v7

    iget-object v8, p1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iput v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->popupX:I

    goto :goto_5

    :cond_a
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v7

    sub-int v7, v5, v7

    iget-object v8, p1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$PopupSpec;->popupX:I

    goto :goto_5
.end method

.method private getActionBarHeight()I
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/16 v0, 0x30

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
