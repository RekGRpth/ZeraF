.class public Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;
.super Landroid/widget/LinearLayout;
.source "PlusOneButtonContentView.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneCompoundButton;,
        Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;
    }
.end annotation


# static fields
.field private static final TEXT_COLOR:I

.field protected static final TYPE_DISABLED:I = 0x3

.field protected static final TYPE_EMPTY:I = 0x2

.field protected static final TYPE_OFF:I = 0x1

.field protected static final TYPE_ON:I


# instance fields
.field protected mActivityRequestCode:I

.field private mAnnotation:I

.field private mAnnotationTextList:[Ljava/lang/String;

.field private mBubbleTextList:[Ljava/lang/String;

.field protected final mGmsLayoutInflater:Landroid/view/LayoutInflater;

.field protected final mGmsResources:Landroid/content/res/Resources;

.field protected final mIconContainer:Landroid/widget/FrameLayout;

.field protected final mLayout:Landroid/widget/LinearLayout;

.field protected mPlusClient:Lcom/google/android/gms/plus/PlusClient;

.field protected mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

.field protected final mPlusOneButton:Landroid/widget/CompoundButton;

.field private final mPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

.field private mProfilePhotoUriList:[Landroid/net/Uri;

.field private final mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

.field private final mProgressBar:Landroid/widget/ProgressBar;

.field private mSize:I

.field protected final mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

.field protected mTogglePending:Z

.field protected mType:I

.field protected mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "#666666"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->TEXT_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v11, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v10, 0x11

    const/4 v8, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v6, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mActivityRequestCode:I

    new-array v5, v11, [Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iput-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iput v7, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mType:I

    const/4 v5, 0x2

    iput v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    const/4 v5, 0x3

    iput v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    new-instance v5, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$1;

    invoke-direct {v5, p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$1;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;)V

    iput-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    const-string v5, "Context must not be null."

    invoke-static {p1, v5}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v5

    if-eqz v5, :cond_0

    iput-object v8, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mGmsResources:Landroid/content/res/Resources;

    iput-object v8, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mGmsLayoutInflater:Landroid/view/LayoutInflater;

    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getSize(Landroid/content/Context;Landroid/util/AttributeSet;)I

    move-result v5

    iput v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getAnnotation(Landroid/content/Context;Landroid/util/AttributeSet;)I

    move-result v5

    iput v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getButtonSize(Landroid/graphics/Point;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->isInEditMode()Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setGravity(I)V

    const-string v5, "[ +1 ]"

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget v6, v0, Landroid/graphics/Point;->x:I

    iget v7, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v5}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v8, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    iput-object v8, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProgressBar:Landroid/widget/ProgressBar;

    iput-object v8, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    iput-object v8, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mIconContainer:Landroid/widget/FrameLayout;

    iput-object v8, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    :goto_1
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getGmsContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mGmsResources:Landroid/content/res/Resources;

    const-string v5, "layout_inflater"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    iput-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mGmsLayoutInflater:Landroid/view/LayoutInflater;

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v7}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setFocusable(Z)V

    new-instance v5, Landroid/widget/LinearLayout;

    invoke-direct {v5, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->addView(Landroid/view/View;)V

    new-instance v5, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneCompoundButton;

    invoke-direct {v5, p0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneCompoundButton;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v5, v8}, Landroid/widget/CompoundButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->createTextView(Landroid/content/Context;)Lcom/google/android/gms/plus/internal/ResizingTextView;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->createIconContainer(Landroid/content/Context;)Landroid/widget/FrameLayout;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mIconContainer:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mIconContainer:Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    iget v8, v0, Landroid/graphics/Point;->x:I

    iget v9, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v7, v8, v9, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v5, v6, v7}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->padForProgressSpinner(Landroid/graphics/Point;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->createProgressBar(Landroid/content/Context;)Landroid/widget/ProgressBar;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mIconContainer:Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProgressBar:Landroid/widget/ProgressBar;

    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    iget v8, v0, Landroid/graphics/Point;->x:I

    iget v9, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v7, v8, v9, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v5, v6, v7}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v4, v5

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->createProfilePhotoView(Landroid/content/Context;)Lcom/google/android/gms/plus/data/internal/PlusImageView;

    move-result-object v6

    aput-object v6, v5, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->showNotPlusOned()V

    goto/16 :goto_1
.end method

.method private createIconContainer(Landroid/content/Context;)Landroid/widget/FrameLayout;
    .locals 2
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    return-object v0
.end method

.method private createProfilePhotoView(Landroid/content/Context;)Lcom/google/android/gms/plus/data/internal/PlusImageView;
    .locals 2
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    return-object v0
.end method

.method private createProgressBar(Landroid/content/Context;)Landroid/widget/ProgressBar;
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    new-instance v0, Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    const v3, 0x1010288

    invoke-direct {v0, p1, v2, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setFocusable(Z)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    return-object v0
.end method

.method private createTextView(Landroid/content/Context;)Lcom/google/android/gms/plus/internal/ResizingTextView;
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x0

    new-instance v1, Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-direct {v1, p1}, Lcom/google/android/gms/plus/internal/ResizingTextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v5}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setFocusable(Z)V

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setGravity(I)V

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setSingleLine()V

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    iget v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    invoke-static {v3, v4}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getTextSize(II)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-virtual {v1, v5, v0}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setTextSize(IF)V

    sget v2, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->TEXT_COLOR:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setTextColor(I)V

    invoke-virtual {v1, v5}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setVisibility(I)V

    return-object v1
.end method

.method private getAnnotation(Landroid/content/Context;Landroid/util/AttributeSet;)I
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v0, "http://schemas.android.com/apk/lib/com.google.android.gms.plus"

    const-string v1, "annotation"

    const-string v6, "PlusOneButton"

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/common/internal/ViewUtils;->getXmlAttributeString(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;ZZLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "INLINE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v4, 0x2

    :cond_0
    :goto_0
    return v4

    :cond_1
    const-string v0, "NONE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v4, v5

    goto :goto_0

    :cond_2
    const-string v0, "BUBBLE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method private getBubbleResourceName()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "global_count_bubble_standard"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "global_count_bubble_medium"

    goto :goto_0

    :pswitch_1
    const-string v0, "global_count_bubble_small"

    goto :goto_0

    :pswitch_2
    const-string v0, "global_count_bubble_tall"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getBubbleResourceUri()Landroid/net/Uri;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getBubbleResourceName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusResources;->getDrawableUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private getButtonDrawable()Landroid/graphics/drawable/Drawable;
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mGmsResources:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mGmsResources:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mGmsResources:Landroid/content/res/Resources;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getButtonResourceName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    const-string v4, "com.google.android.gms"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private getButtonResourceName()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "ic_plusone_standard"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "ic_plusone_small"

    goto :goto_0

    :pswitch_1
    const-string v0, "ic_plusone_medium"

    goto :goto_0

    :pswitch_2
    const-string v0, "ic_plusone_tall"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getButtonSize(Landroid/graphics/Point;)V
    .locals 9
    .param p1    # Landroid/graphics/Point;

    const/4 v6, 0x1

    const-wide/high16 v7, 0x3fe0000000000000L

    iget v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    packed-switch v5, :pswitch_data_0

    const/16 v4, 0x26

    const/16 v2, 0x18

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    int-to-float v5, v4

    invoke-static {v6, v5, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    int-to-float v5, v2

    invoke-static {v6, v5, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-double v5, v3

    add-double/2addr v5, v7

    double-to-int v5, v5

    iput v5, p1, Landroid/graphics/Point;->x:I

    float-to-double v5, v1

    add-double/2addr v5, v7

    double-to-int v5, v5

    iput v5, p1, Landroid/graphics/Point;->y:I

    return-void

    :pswitch_0
    const/16 v4, 0x20

    const/16 v2, 0x14

    goto :goto_0

    :pswitch_1
    const/16 v4, 0x18

    const/16 v2, 0xe

    goto :goto_0

    :pswitch_2
    const/16 v4, 0x32

    const/16 v2, 0x14

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getGmsContext(Landroid/content/Context;)Landroid/content/Context;
    .locals 4
    .param p1    # Landroid/content/Context;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.google.android.gms"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "PlusOneButton"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PlusOneButton"

    const-string v2, "Google Play services is not installed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getSize(Landroid/content/Context;Landroid/util/AttributeSet;)I
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v8, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v0, "http://schemas.android.com/apk/lib/com.google.android.gms.plus"

    const-string v1, "size"

    const-string v6, "PlusOneButton"

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/common/internal/ViewUtils;->getXmlAttributeString(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;ZZLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "SMALL"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v5

    :cond_0
    const-string v0, "MEDIUM"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v5, v4

    goto :goto_0

    :cond_1
    const-string v0, "TALL"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x2

    goto :goto_0

    :cond_2
    const-string v0, "STANDARD"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v5, v8

    goto :goto_0

    :cond_3
    move v5, v8

    goto :goto_0
.end method

.method private static getTextSize(II)I
    .locals 2
    .param p0    # I
    .param p1    # I

    const/16 v0, 0xd

    packed-switch p0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/16 v0, 0xf

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xb

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getTextViewLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 6

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x2

    iget v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    packed-switch v1, :pswitch_data_0

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    :goto_0
    iget v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    if-ne v1, v5, :cond_0

    move v1, v2

    :goto_1
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iget v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    if-ne v1, v5, :cond_1

    :goto_2
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    return-object v0

    :pswitch_0
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_1

    :cond_1
    move v3, v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private padForProgressSpinner(Landroid/graphics/Point;)V
    .locals 4
    .param p1    # Landroid/graphics/Point;

    iget v0, p1, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    const/4 v1, 0x1

    const/high16 v2, 0x40c00000

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Point;->y:I

    iget v0, p1, Landroid/graphics/Point;->y:I

    iput v0, p1, Landroid/graphics/Point;->x:I

    return-void
.end method

.method private setAnnotationText([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotationTextList:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateTextView()V

    return-void
.end method

.method private setAvatarImageMargins()V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v6, 0x40a00000

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    invoke-static {v9, v6, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    float-to-int v4, v6

    const/high16 v6, 0x3f800000

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    invoke-static {v9, v6, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    float-to-int v1, v6

    const/4 v3, 0x1

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v0, v6

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->getVisibility()I

    move-result v6

    if-eqz v6, :cond_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz v3, :cond_1

    invoke-virtual {v5, v4, v8, v1, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    const/4 v3, 0x0

    :goto_2
    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v6, v6, v2

    invoke-virtual {v6, v5}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v5, v1, v8, v1, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_2

    :cond_2
    return-void
.end method

.method private setBubbleText([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mBubbleTextList:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateTextView()V

    return-void
.end method

.method private setProfilePhotos([Landroid/net/Uri;)V
    .locals 0
    .param p1    # [Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoUriList:[Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateProfilePhotos()V

    return-void
.end method

.method private setState(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mType:I

    iput p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->refreshButtonLayout()V

    return-void
.end method

.method private setTextViewPadding(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x40400000

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v5, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v1, v3

    const/high16 v3, 0x40a00000

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v5, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v0, v3

    iget v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    if-ne v3, v6, :cond_0

    :goto_0
    iget v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    if-ne v3, v6, :cond_1

    iget v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    if-ne v3, v5, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/view/View;->setPadding(IIII)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private updateProfilePhotos()V
    .locals 10

    const/16 v9, 0x8

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoUriList:[Landroid/net/Uri;

    if-eqz v5, :cond_2

    iget v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getButtonSize(Landroid/graphics/Point;)V

    iget v5, v4, Landroid/graphics/Point;->y:I

    iput v5, v4, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v3, v5

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoUriList:[Landroid/net/Uri;

    array-length v2, v5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_3

    if-ge v0, v2, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoUriList:[Landroid/net/Uri;

    aget-object v1, v5, v0

    :goto_1
    if-nez v1, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5, v9}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v5, v5, v0

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    iget v7, v4, Landroid/graphics/Point;->x:I

    iget v8, v4, Landroid/graphics/Point;->y:I

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v5, v5, v0

    iget v6, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v1, v6}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->loadFromUri(Landroid/net/Uri;I)V

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v5, v5, v0

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v3, v5

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v3, :cond_3

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5, v9}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setAvatarImageMargins()V

    return-void
.end method

.method private updateTextView()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setText([Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setVisibility(I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotationTextList:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setText([Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mBubbleTextList:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setText([Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public initialize(Lcom/google/android/gms/plus/PlusClient;Ljava/lang/String;I)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const-string v2, "Plus client must not be null."

    invoke-static {p1, v2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "URL must not be null."

    invoke-static {p2, v2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-ltz p3, :cond_1

    const v2, 0xffff

    if-gt p3, v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    const-string v3, "activityRequestCode must be an unsigned 16 bit integer."

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    iput p3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mActivityRequestCode:I

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eq p1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v2, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v2, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v2, p0}, Lcom/google/android/gms/plus/PlusClient;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v2, p0}, Lcom/google/android/gms/plus/PlusClient;->registerConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v1, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->initialize(Lcom/google/android/gms/plus/PlusClient;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->onConnected()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->refreshButtonLayout()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->isConnectionCallbacksRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->isConnectionFailedListenerRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->registerConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    :cond_1
    return-void
.end method

.method public onConnected()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/PlusClient;->loadPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->showDisabled()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->isConnectionCallbacksRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->isConnectionFailedListenerRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    :cond_1
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public performClick()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->performClick()Z

    move-result v0

    return v0
.end method

.method protected refreshButtonLayout()V
    .locals 12

    const/16 v11, 0x11

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->isInEditMode()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getButtonSize(Landroid/graphics/Point;)V

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    iget v6, v2, Landroid/graphics/Point;->x:I

    iget v7, v2, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v6, v7, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v4, v5}, Landroid/widget/CompoundButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->padForProgressSpinner(Landroid/graphics/Point;)V

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProgressBar:Landroid/widget/ProgressBar;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    iget v6, v2, Landroid/graphics/Point;->x:I

    iget v7, v2, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v6, v7, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    if-ne v4, v8, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getBubbleResourceUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setImage(Landroid/net/Uri;)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateProfilePhotos()V

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getTextViewLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    iget v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    invoke-static {v4, v5}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getTextSize(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v10, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v4, v9, v3}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setTextSize(IF)V

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setTextViewPadding(Landroid/view/View;)V

    iget v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    if-ne v4, v10, :cond_2

    iget v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    if-ne v4, v8, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->requestLayout()V

    goto/16 :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setImage(Landroid/net/Uri;)V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    array-length v1, v4

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_3

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProfilePhotoViews:[Lcom/google/android/gms/plus/data/internal/PlusImageView;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public setAnnotation(I)V
    .locals 2
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "Annotation must not be null."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mAnnotation:I

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateTextView()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->refreshButtonLayout()V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTextView:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnPlusOneClickListener(Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$DefaultOnPlusOneClickListener;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;Lcom/google/android/gms/plus/PlusOneButton$OnPlusOneClickListener;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setSize(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mType:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setState(II)V

    return-void
.end method

.method public setType(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mSize:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setState(II)V

    return-void
.end method

.method public showDisabled()V
    .locals 2

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setType(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateButtonImage()V

    return-void
.end method

.method protected showNotPlusOned()V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setType(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateButtonImage()V

    return-void
.end method

.method protected showPlusOned()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setType(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateButtonImage()V

    return-void
.end method

.method public showProgress()V
    .locals 2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setType(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->updateButtonImage()V

    return-void
.end method

.method protected updateButtonImage()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getButtonDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    iget v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mType:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOneButton:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected updateView()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->getInlineAnnotationText()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setAnnotationText([Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->getBubbleText()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setBubbleText([Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->getProfilePhotoUris()[Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setProfilePhotos([Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mPlusOne:Lcom/google/android/gms/plus/data/plusone/PlusOne;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/plusone/PlusOne;->hasPlusOne()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->showPlusOned()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->showNotPlusOned()V

    goto :goto_0
.end method
