.class public Lcom/google/android/gms/plus/data/plusone/PlusOne;
.super Ljava/lang/Object;
.source "PlusOne.java"


# instance fields
.field mBundle:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public getBubbleText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "bubble_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConfirmationMessage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "confirmation_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInlineAnnotationText()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "inline_annotations"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method public getProfilePhotoUris()[Landroid/net/Uri;
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v3, "profile_photo_uris"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    array-length v2, v0

    new-array v1, v2, [Landroid/net/Uri;

    array-length v2, v0

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public getToken()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVisibility()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "visibility"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasPlusOne()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/data/plusone/PlusOne;->mBundle:Landroid/os/Bundle;

    const-string v1, "has_plus_one"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
