.class public Lcom/google/android/gms/plus/data/internal/PlusImageView;
.super Landroid/widget/ImageView;
.source "PlusImageView.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBoundingBox:I

.field private mIsAttached:Z

.field private mPlusClient:Lcom/google/android/gms/plus/PlusClient;

.field private mShouldLoad:Z

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic access$000(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # I

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->resizeBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/plus/data/internal/PlusImageView;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/gms/plus/data/internal/PlusImageView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/data/internal/PlusImageView;
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/gms/plus/data/internal/PlusImageView;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mIsAttached:Z

    return v0
.end method

.method private static resizeBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 12
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # I

    const-wide/high16 v10, 0x3fe0000000000000L

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-double v6, v8

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-double v4, v8

    cmpl-double v8, v6, v4

    if-lez v8, :cond_0

    int-to-double v8, p1

    div-double v2, v8, v6

    :goto_0
    mul-double v8, v6, v2

    add-double/2addr v8, v10

    double-to-int v1, v8

    mul-double v8, v4, v2

    add-double/2addr v8, v10

    double-to-int v0, v8

    const/4 v8, 0x1

    invoke-static {p0, v1, v0, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    return-object v8

    :cond_0
    int-to-double v8, p1

    div-double v2, v8, v4

    goto :goto_0
.end method

.method private updateView()V
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_1

    const-string v2, "android.resource"

    iget-object v3, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mShouldLoad:Z

    if-nez v2, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mUri:Landroid/net/Uri;

    if-nez v2, :cond_3

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_3
    if-nez v0, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/PlusClient;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setImageURI(Landroid/net/Uri;)V

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mShouldLoad:Z

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v3, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mUri:Landroid/net/Uri;

    iget v4, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBoundingBox:I

    invoke-virtual {v2, p0, v3, v4}, Lcom/google/android/gms/plus/PlusClient;->loadImage(Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;Landroid/net/Uri;I)V

    goto :goto_2
.end method


# virtual methods
.method public initialize(Lcom/google/android/gms/plus/PlusClient;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->isConnectionCallbacksRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    :cond_1
    return-void
.end method

.method public loadFromUri(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->loadFromUri(Landroid/net/Uri;I)V

    return-void
.end method

.method public loadFromUri(Landroid/net/Uri;I)V
    .locals 5
    .param p1    # Landroid/net/Uri;
    .param p2    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mUri:Landroid/net/Uri;

    if-nez v4, :cond_1

    if-nez p1, :cond_0

    move v1, v2

    :goto_0
    iget v4, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBoundingBox:I

    if-ne v4, p2, :cond_2

    move v0, v2

    :goto_1
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    :goto_2
    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    iput-object p1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mUri:Landroid/net/Uri;

    iput p2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBoundingBox:I

    iput-boolean v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mShouldLoad:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->updateView()V

    goto :goto_2
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mIsAttached:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->isConnectionCallbacksRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    return-void
.end method

.method public onConnected()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->updateView()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mIsAttached:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->isConnectionCallbacksRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/PlusClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    :cond_0
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onParcelFileDescriptorLoaded(Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Landroid/os/ParcelFileDescriptor;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mShouldLoad:Z

    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;

    iget v1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBoundingBox:I

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;-><init>(Lcom/google/android/gms/plus/data/internal/PlusImageView;I)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/os/ParcelFileDescriptor;

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
