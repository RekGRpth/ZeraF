.class Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;
.super Landroid/os/AsyncTask;
.source "PlusImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/data/internal/PlusImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadBitmapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/ParcelFileDescriptor;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mBoundingBox:I

.field final synthetic this$0:Lcom/google/android/gms/plus/data/internal/PlusImageView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/data/internal/PlusImageView;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->this$0:Lcom/google/android/gms/plus/data/internal/PlusImageView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput p2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->mBoundingBox:I

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1    # [Landroid/os/ParcelFileDescriptor;

    const/4 v3, 0x0

    aget-object v2, p1, v3

    :try_start_0
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget v3, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->mBoundingBox:I

    if-lez v3, :cond_0

    iget v3, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->mBoundingBox:I

    # invokes: Lcom/google/android/gms/plus/data/internal/PlusImageView;->resizeBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    invoke-static {v0, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->access$000(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    # getter for: Lcom/google/android/gms/plus/data/internal/PlusImageView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->access$100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "closed failed"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    :try_start_2
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    # getter for: Lcom/google/android/gms/plus/data/internal/PlusImageView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->access$100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "closed failed"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_1
    throw v3

    :catch_2
    move-exception v1

    # getter for: Lcom/google/android/gms/plus/data/internal/PlusImageView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "closed failed"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/os/ParcelFileDescriptor;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->doInBackground([Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->this$0:Lcom/google/android/gms/plus/data/internal/PlusImageView;

    # setter for: Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, p1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->access$202(Lcom/google/android/gms/plus/data/internal/PlusImageView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->this$0:Lcom/google/android/gms/plus/data/internal/PlusImageView;

    # getter for: Lcom/google/android/gms/plus/data/internal/PlusImageView;->mIsAttached:Z
    invoke-static {v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->access$300(Lcom/google/android/gms/plus/data/internal/PlusImageView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->this$0:Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->this$0:Lcom/google/android/gms/plus/data/internal/PlusImageView;

    # getter for: Lcom/google/android/gms/plus/data/internal/PlusImageView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->access$200(Lcom/google/android/gms/plus/data/internal/PlusImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/data/internal/PlusImageView$LoadBitmapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
