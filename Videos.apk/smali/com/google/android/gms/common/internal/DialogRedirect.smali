.class public Lcom/google/android/gms/common/internal/DialogRedirect;
.super Ljava/lang/Object;
.source "DialogRedirect.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mIntent:Landroid/content/Intent;

.field private final mRequestCode:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/Intent;I)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/content/Intent;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/internal/DialogRedirect;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/gms/common/internal/DialogRedirect;->mIntent:Landroid/content/Intent;

    iput p3, p0, Lcom/google/android/gms/common/internal/DialogRedirect;->mRequestCode:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/common/internal/DialogRedirect;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/DialogRedirect;->mIntent:Landroid/content/Intent;

    iget v3, p0, Lcom/google/android/gms/common/internal/DialogRedirect;->mRequestCode:I

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SettingsRedirect"

    const-string v2, "Can\'t redirect to app settings for Google Play Services"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
