.class public final Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientInfo"
.end annotation


# instance fields
.field private androidClientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

.field private cachedSize:I

.field private clientType_:I

.field private desktopClientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

.field private hasAndroidClientInfo:Z

.field private hasClientType:Z

.field private hasDesktopClientInfo:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType_:I

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAndroidClientInfo()Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->cachedSize:I

    return v0
.end method

.method public getClientType()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType_:I

    return v0
.end method

.method public getDesktopClientInfo()Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasClientType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->getClientType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasAndroidClientInfo()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->getAndroidClientInfo()Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasDesktopClientInfo()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->getDesktopClientInfo()Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->cachedSize:I

    return v0
.end method

.method public hasAndroidClientInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasAndroidClientInfo:Z

    return v0
.end method

.method public hasClientType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasClientType:Z

    return v0
.end method

.method public hasDesktopClientInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasDesktopClientInfo:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->setClientType(I)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    invoke-direct {v1}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->setAndroidClientInfo(Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    invoke-direct {v1}, Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->setDesktopClientInfo(Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    move-result-object v0

    return-object v0
.end method

.method public setAndroidClientInfo(Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasAndroidClientInfo:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    return-object p0
.end method

.method public setClientType(I)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasClientType:Z

    iput p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType_:I

    return-object p0
.end method

.method public setDesktopClientInfo(Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasDesktopClientInfo:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasClientType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->getClientType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasAndroidClientInfo()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->getAndroidClientInfo()Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->hasDesktopClientInfo()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->getDesktopClientInfo()Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    return-void
.end method
