.class Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImplFallback;
.super Ljava/lang/Object;
.source "MediaRouterCompat.java"

# interfaces
.implements Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/MediaRouterCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MediaRouterCompatImplFallback"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public RouteCategory_getName(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->getName()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public RouteCategory_getRoutes(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->getRoutes(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public RouteCategory_isGroupable(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->isGroupable()Z

    move-result v0

    return v0
.end method

.method public RouteGroup_addRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    check-cast p2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->addRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    return-void
.end method

.method public RouteGroup_getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public RouteGroup_getRouteCount(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getRouteCount()I

    move-result v0

    return v0
.end method

.method public RouteGroup_removeRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    check-cast p2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->removeRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    return-void
.end method

.method public RouteGroup_removeRouteAt(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->removeRoute(I)V

    return-void
.end method

.method public RouteInfo_getCategory(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getCategory()Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getGroup(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getGroup()Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getIconDrawable(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getName(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getName()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getPlaybackType(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getPlaybackType()I

    move-result v0

    return v0
.end method

.method public RouteInfo_getStatus(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getStatus()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getSupportedTypes(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getSupportedTypes()I

    move-result v0

    return v0
.end method

.method public RouteInfo_getTag(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public RouteInfo_getVolume(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolume()I

    move-result v0

    return v0
.end method

.method public RouteInfo_getVolumeHandling(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolumeHandling()I

    move-result v0

    return v0
.end method

.method public RouteInfo_getVolumeMax(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolumeMax()I

    move-result v0

    return v0
.end method

.method public RouteInfo_requestSetVolume(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->requestSetVolume(I)V

    return-void
.end method

.method public RouteInfo_requestUpdateVolume(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->requestUpdateVolume(I)V

    return-void
.end method

.method public RouteInfo_setTag(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public UserRouteInfo_setName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/CharSequence;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->setName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public UserRouteInfo_setPlaybackType(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->setPlaybackType(I)V

    return-void
.end method

.method public UserRouteInfo_setRemoteControlClient(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    return-void
.end method

.method public UserRouteInfo_setStatus(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/CharSequence;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->setStatus(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public UserRouteInfo_setVolume(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->setVolume(I)V

    return-void
.end method

.method public UserRouteInfo_setVolumeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->setVolumeCallback(Lcom/android/athome/picker/media/AbsMediaRouterCompat$VolumeCallback;)V

    return-void
.end method

.method public UserRouteInfo_setVolumeHandling(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->setVolumeHandling(I)V

    return-void
.end method

.method public UserRouteInfo_setVolumeMax(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;->setVolumeMax(I)V

    return-void
.end method

.method public addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {p1, p2, p3}, Lcom/android/athome/picker/media/MediaRouterFallback;->addCallback(ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    return-void
.end method

.method public addUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    check-cast p2, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->addUserRoute(Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;)V

    return-void
.end method

.method public createRouteCategory(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Z

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {p1, p2, p3}, Lcom/android/athome/picker/media/MediaRouterFallback;->createRouteCategory(Ljava/lang/CharSequence;Z)Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    move-result-object v0

    return-object v0
.end method

.method public createUserRoute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    check-cast p2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->createUserRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public forApplication(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->forApplication(Landroid/content/Context;)Lcom/android/athome/picker/media/MediaRouterFallback;

    move-result-object v0

    return-object v0
.end method

.method public getCategoryAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->getCategoryAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    move-result-object v0

    return-object v0
.end method

.method public getCategoryCount(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->getCategoryCount()I

    move-result v0

    return v0
.end method

.method public getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public getRouteCount(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback;->getRouteCount()I

    move-result v0

    return v0
.end method

.method public getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->getSelectedRoute(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public isRouteCategory(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    return v0
.end method

.method public isRouteGroup(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    return v0
.end method

.method public isRouteInfo(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    return v0
.end method

.method public removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->removeCallback(Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    return-void
.end method

.method public removeUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    check-cast p2, Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback;->removeUserRoute(Lcom/android/athome/picker/media/MediaRouterFallback$UserRouteInfo;)V

    return-void
.end method

.method public selectRoute(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    check-cast p1, Lcom/android/athome/picker/media/MediaRouterFallback;

    check-cast p3, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1, p2, p3}, Lcom/android/athome/picker/media/MediaRouterFallback;->selectRoute(ILcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    return-void
.end method
