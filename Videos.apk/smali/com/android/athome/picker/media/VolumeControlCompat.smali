.class public Lcom/android/athome/picker/media/VolumeControlCompat;
.super Ljava/lang/Object;
.source "VolumeControlCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatFallbackImpl;,
        Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;
    }
.end annotation


# static fields
.field private static final IMPL:Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;

.field public static PLAYSTATE_BUFFERING:I

.field public static PLAYSTATE_ERROR:I

.field public static PLAYSTATE_FAST_FORWARDING:I

.field public static PLAYSTATE_PAUSED:I

.field public static PLAYSTATE_PLAYING:I

.field public static PLAYSTATE_REWINDING:I

.field public static PLAYSTATE_SKIPPING_BACKWARDS:I

.field public static PLAYSTATE_SKIPPING_FORWARDS:I

.field public static PLAYSTATE_STOPPED:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lcom/android/athome/picker/media/VolumeControlCompat;->PLAYSTATE_STOPPED:I

    const/4 v0, 0x2

    sput v0, Lcom/android/athome/picker/media/VolumeControlCompat;->PLAYSTATE_PAUSED:I

    const/4 v0, 0x3

    sput v0, Lcom/android/athome/picker/media/VolumeControlCompat;->PLAYSTATE_PLAYING:I

    const/4 v0, 0x4

    sput v0, Lcom/android/athome/picker/media/VolumeControlCompat;->PLAYSTATE_FAST_FORWARDING:I

    const/4 v0, 0x5

    sput v0, Lcom/android/athome/picker/media/VolumeControlCompat;->PLAYSTATE_REWINDING:I

    const/4 v0, 0x6

    sput v0, Lcom/android/athome/picker/media/VolumeControlCompat;->PLAYSTATE_SKIPPING_FORWARDS:I

    const/4 v0, 0x7

    sput v0, Lcom/android/athome/picker/media/VolumeControlCompat;->PLAYSTATE_SKIPPING_BACKWARDS:I

    const/16 v0, 0x8

    sput v0, Lcom/android/athome/picker/media/VolumeControlCompat;->PLAYSTATE_BUFFERING:I

    const/16 v0, 0x9

    sput v0, Lcom/android/athome/picker/media/VolumeControlCompat;->PLAYSTATE_ERROR:I

    new-instance v0, Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatFallbackImpl;

    invoke-direct {v0}, Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatFallbackImpl;-><init>()V

    sput-object v0, Lcom/android/athome/picker/media/VolumeControlCompat;->IMPL:Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;

    return-void
.end method

.method public static newRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/content/ComponentName;

    sget-object v0, Lcom/android/athome/picker/media/VolumeControlCompat;->IMPL:Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;->newRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static newVolumeControl(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/android/athome/picker/media/VolumeControlCompat;->IMPL:Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;->newVolumeControl(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static registerRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/VolumeControlCompat;->IMPL:Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;->registerRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;Ljava/lang/Object;)V

    return-void
.end method

.method public static unregisterRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/VolumeControlCompat;->IMPL:Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;->unregisterRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;Ljava/lang/Object;)V

    return-void
.end method
