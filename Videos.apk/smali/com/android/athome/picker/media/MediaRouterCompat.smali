.class public Lcom/android/athome/picker/media/MediaRouterCompat;
.super Lcom/android/athome/picker/media/AbsMediaRouterCompat;
.source "MediaRouterCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/media/MediaRouterCompat$RouteCategory;,
        Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;,
        Lcom/android/athome/picker/media/MediaRouterCompat$UserRouteInfo;,
        Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;,
        Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImplJellybean;,
        Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImplFallback;,
        Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;
    }
.end annotation


# static fields
.field static final IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImplJellybean;

    invoke-direct {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImplJellybean;-><init>()V

    sput-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImplFallback;

    invoke-direct {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImplFallback;-><init>()V

    sput-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/athome/picker/media/AbsMediaRouterCompat;-><init>()V

    return-void
.end method

.method public static addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    return-void
.end method

.method public static addUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->addUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public static createRouteCategory(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->createRouteCategory(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static createUserRoute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->createUserRoute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static forApplication(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->forApplication(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static getCategoryAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->getCategoryAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static getCategoryCount(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->getCategoryCount(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static getRouteCount(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->getRouteCount(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static isRouteCategory(Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->isRouteCategory(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isRouteGroup(Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->isRouteGroup(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isRouteInfo(Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->isRouteInfo(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    return-void
.end method

.method public static removeUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->removeUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public static selectRoute(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    sget-object v0, Lcom/android/athome/picker/media/MediaRouterCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompat$MediaRouterCompatImpl;->selectRoute(Ljava/lang/Object;ILjava/lang/Object;)V

    return-void
.end method
