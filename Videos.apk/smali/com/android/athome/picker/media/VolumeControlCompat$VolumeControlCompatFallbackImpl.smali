.class Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatFallbackImpl;
.super Ljava/lang/Object;
.source "VolumeControlCompat.java"

# interfaces
.implements Lcom/android/athome/picker/media/VolumeControlCompat$VolumeControlCompatImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/VolumeControlCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "VolumeControlCompatFallbackImpl"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public newRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/content/ComponentName;

    const/4 v0, 0x0

    return-object v0
.end method

.method public newVolumeControl(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/android/athome/picker/media/VolumeControlFallback;

    invoke-direct {v0, p1}, Lcom/android/athome/picker/media/VolumeControlFallback;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public registerRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/content/ComponentName;
    .param p3    # Ljava/lang/Object;

    return-void
.end method

.method public unregisterRemoteControlClient(Ljava/lang/Object;Landroid/content/ComponentName;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/content/ComponentName;
    .param p3    # Ljava/lang/Object;

    return-void
.end method
