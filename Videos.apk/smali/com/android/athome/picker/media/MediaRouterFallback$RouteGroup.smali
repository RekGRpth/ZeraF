.class public Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;
.super Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
.source "MediaRouterFallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/MediaRouterFallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RouteGroup"
.end annotation


# instance fields
.field final mRoutes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateName:Z

.field final synthetic this$0:Lcom/android/athome/picker/media/MediaRouterFallback;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/media/MediaRouterFallback;Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)V
    .locals 1
    .param p2    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;-><init>(Lcom/android/athome/picker/media/MediaRouterFallback;Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    iput-object p0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    return-void
.end method


# virtual methods
.method public addRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getGroup()Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Route "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is already part of a group."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getCategory()Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    if-eq v1, v2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Route cannot be added to a group with a different category. (Route category="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getCategory()Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " group category="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p0, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mUpdateName:Z

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->updateVolume()V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->routeUpdated()V

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v1, p1, p0, v0}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteGrouped(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;I)V

    return-void
.end method

.method public getName()Ljava/lang/CharSequence;
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mUpdateName:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->updateName()V

    :cond_0
    invoke-super {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getName()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    return-object v0
.end method

.method public getRouteCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getStatus()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getStatus()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getStatus()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method memberStatusChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
    .param p2    # Ljava/lang/CharSequence;

    invoke-virtual {p0, p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->setStatusInt(Ljava/lang/CharSequence;)V

    return-void
.end method

.method memberVolumeChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->updateVolume()V

    return-void
.end method

.method public removeRoute(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mUpdateName:Z

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v1, v0, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteUngrouped(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;)V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->routeUpdated()V

    return-void
.end method

.method public removeRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V
    .locals 3
    .param p1    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getGroup()Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Route "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a member of this group."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mUpdateName:Z

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v0, p1, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteUngrouped(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;)V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->routeUpdated()V

    return-void
.end method

.method public requestSetVolume(I)V
    .locals 8
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getVolumeMax()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    int-to-float v6, p1

    int-to-float v7, v1

    div-float v5, v6, v7

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getRouteCount()I

    move-result v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolumeMax()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    float-to-int v4, v6

    invoke-virtual {v2, v4}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->requestSetVolume(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget v6, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mVolume:I

    if-eq p1, v6, :cond_0

    iput p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mVolume:I

    iget-object v6, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v6, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteVolumeChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    goto :goto_0
.end method

.method public requestUpdateVolume(I)V
    .locals 7
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getVolumeMax()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getRouteCount()I

    move-result v3

    const/4 v5, 0x0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->requestUpdateVolume(I)V

    invoke-virtual {v2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolume()I

    move-result v4

    if-le v4, v5, :cond_2

    move v5, v4

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget v6, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mVolume:I

    if-eq v5, v6, :cond_0

    iput v5, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mVolume:I

    iget-object v6, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v6, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteVolumeChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    goto :goto_0
.end method

.method routeUpdated()V
    .locals 11

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v8, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->removeRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_4

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    iget v8, v5, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mSupportedTypes:I

    or-int/2addr v7, v8

    invoke-virtual {v5}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolumeMax()I

    move-result v6

    if-le v6, v4, :cond_1

    move v4, v6

    :cond_1
    invoke-virtual {v5}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getPlaybackType()I

    move-result v8

    if-nez v8, :cond_2

    move v8, v9

    :goto_2
    and-int/2addr v3, v8

    invoke-virtual {v5}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolumeHandling()I

    move-result v8

    if-nez v8, :cond_3

    move v8, v9

    :goto_3
    and-int/2addr v2, v8

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v8, v10

    goto :goto_2

    :cond_3
    move v8, v10

    goto :goto_3

    :cond_4
    if-eqz v3, :cond_5

    move v8, v10

    :goto_4
    iput v8, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mPlaybackType:I

    if-eqz v2, :cond_6

    move v8, v10

    :goto_5
    iput v8, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mVolumeHandling:I

    iput v7, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mSupportedTypes:I

    iput v4, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mVolumeMax:I

    if-ne v0, v9, :cond_7

    iget-object v8, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    invoke-virtual {v8}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    :goto_6
    iput-object v8, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-super {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->routeUpdated()V

    goto :goto_0

    :cond_5
    move v8, v9

    goto :goto_4

    :cond_6
    move v8, v9

    goto :goto_5

    :cond_7
    const/4 v8, 0x0

    goto :goto_6
.end method

.method updateName()V
    .locals 5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    if-lez v1, :cond_0

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v4, v2, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mName:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mName:Ljava/lang/CharSequence;

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mUpdateName:Z

    return-void
.end method

.method updateVolume()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getRouteCount()I

    move-result v1

    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->getRouteAt(I)Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolume()I

    move-result v2

    if-le v2, v3, :cond_0

    move v3, v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v4, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mVolume:I

    if-eq v3, v4, :cond_2

    iput v3, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->mVolume:I

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v4, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteVolumeChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :cond_2
    return-void
.end method
