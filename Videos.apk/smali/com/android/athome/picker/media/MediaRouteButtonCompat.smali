.class public Lcom/android/athome/picker/media/MediaRouteButtonCompat;
.super Ljava/lang/Object;
.source "MediaRouteButtonCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatJellybeanImpl;,
        Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatFallbackImpl;,
        Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatImpl;
    }
.end annotation


# static fields
.field private static final IMPL:Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatJellybeanImpl;

    invoke-direct {v0}, Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatJellybeanImpl;-><init>()V

    sput-object v0, Lcom/android/athome/picker/media/MediaRouteButtonCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatImpl;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatFallbackImpl;

    invoke-direct {v0}, Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatFallbackImpl;-><init>()V

    sput-object v0, Lcom/android/athome/picker/media/MediaRouteButtonCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatImpl;

    goto :goto_0
.end method

.method public static setRouteTypes(Landroid/view/View;I)V
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I

    sget-object v0, Lcom/android/athome/picker/media/MediaRouteButtonCompat;->IMPL:Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatImpl;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouteButtonCompat$MediaRouteButtonCompatImpl;->setRouteTypes(Landroid/view/View;I)V

    return-void
.end method
