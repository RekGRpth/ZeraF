.class public Lcom/android/athome/picker/media/VolumeControlFallback;
.super Ljava/lang/Object;
.source "VolumeControlFallback.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mRouteTypes:I

.field private mRouter:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/athome/picker/media/VolumeControlFallback;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat;->forApplication(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/media/VolumeControlFallback;->mRouter:Ljava/lang/Object;

    const v0, 0x800001

    iput v0, p0, Lcom/android/athome/picker/media/VolumeControlFallback;->mRouteTypes:I

    return-void
.end method
