.class public Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;
.super Ljava/lang/Object;
.source "MediaRouterFallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/media/MediaRouterFallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RouteInfo"
.end annotation


# instance fields
.field final mCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

.field mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

.field mIcon:Landroid/graphics/drawable/Drawable;

.field mName:Ljava/lang/CharSequence;

.field mPlaybackStream:I

.field mPlaybackType:I

.field private mStatus:Ljava/lang/CharSequence;

.field mSupportedTypes:I

.field private mTag:Ljava/lang/Object;

.field mVcb:Lcom/android/athome/picker/media/MediaRouterFallback$VolumeCallbackInfo;

.field mVolume:I

.field mVolumeHandling:I

.field mVolumeMax:I

.field final synthetic this$0:Lcom/android/athome/picker/media/MediaRouterFallback;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/media/MediaRouterFallback;Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;)V
    .locals 2
    .param p2    # Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    const/16 v0, 0xf

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackType:I

    iput v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mVolumeMax:I

    iput v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mVolume:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackStream:I

    iput v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mVolumeHandling:I

    iput-object p2, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    invoke-virtual {p2}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;->getSupportedTypes()I

    move-result v0

    iput v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mSupportedTypes:I

    return-void
.end method


# virtual methods
.method public getCategory()Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    return-object v0
.end method

.method public getGroup()Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    return-object v0
.end method

.method public getIconDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getName()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getPlaybackType()I
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackType:I

    return v0
.end method

.method public getStatus()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mStatus:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getSupportedTypes()I
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mSupportedTypes:I

    return v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mTag:Ljava/lang/Object;

    return-object v0
.end method

.method public getVolume()I
    .locals 2

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackType:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    # getter for: Lcom/android/athome/picker/media/MediaRouterFallback;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterFallback;->access$000(Lcom/android/athome/picker/media/MediaRouterFallback;)Landroid/media/AudioManager;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackStream:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mVolume:I

    goto :goto_0
.end method

.method public getVolumeHandling()I
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mVolumeHandling:I

    return v0
.end method

.method public getVolumeMax()I
    .locals 2

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackType:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    # getter for: Lcom/android/athome/picker/media/MediaRouterFallback;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterFallback;->access$000(Lcom/android/athome/picker/media/MediaRouterFallback;)Landroid/media/AudioManager;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackStream:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mVolumeMax:I

    goto :goto_0
.end method

.method public requestSetVolume(I)V
    .locals 3
    .param p1    # I

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackType:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    # getter for: Lcom/android/athome/picker/media/MediaRouterFallback;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterFallback;->access$000(Lcom/android/athome/picker/media/MediaRouterFallback;)Landroid/media/AudioManager;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackStream:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v0, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteVolumeChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "MediaRouterFallback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".requestSetVolume(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Non-local volume playback on system route? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Could not request volume change."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public requestUpdateVolume(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    iget v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackType:I

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolume()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->getVolumeMax()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    # getter for: Lcom/android/athome/picker/media/MediaRouterFallback;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->access$000(Lcom/android/athome/picker/media/MediaRouterFallback;)Landroid/media/AudioManager;

    move-result-object v1

    iget v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mPlaybackStream:I

    invoke-virtual {v1, v2, v0, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v1, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->dispatchRouteVolumeChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "MediaRouterFallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".requestUpdateVolume(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Non-local volume playback on system route? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Could not request volume change."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method routeUpdated()V
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->this$0:Lcom/android/athome/picker/media/MediaRouterFallback;

    invoke-virtual {v0, p0}, Lcom/android/athome/picker/media/MediaRouterFallback;->updateRoute(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;)V

    return-void
.end method

.method setStatusInt(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mStatus:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mStatus:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mGroup:Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;

    invoke-virtual {v0, p0, p1}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteGroup;->memberStatusChanged(Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->routeUpdated()V

    :cond_1
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mTag:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->routeUpdated()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    iget v1, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mSupportedTypes:I

    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterFallback;->typesToString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RouteInfo{ name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mName:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mStatus:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " category="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouterFallback$RouteInfo;->mCategory:Lcom/android/athome/picker/media/MediaRouterFallback$RouteCategory;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " supportedTypes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
