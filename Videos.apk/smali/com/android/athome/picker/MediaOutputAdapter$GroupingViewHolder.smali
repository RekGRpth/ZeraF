.class Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;
.super Ljava/lang/Object;
.source "MediaOutputAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/MediaOutputAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GroupingViewHolder"
.end annotation


# instance fields
.field public checkBox:Landroid/widget/CheckBox;

.field public name:Landroid/widget/TextView;

.field public nowPlaying:Landroid/widget/ImageView;

.field public status:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/CheckBox;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;
    .param p2    # Landroid/widget/ImageView;
    .param p3    # Landroid/widget/TextView;
    .param p4    # Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->name:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->nowPlaying:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->status:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/android/athome/picker/MediaOutputAdapter$GroupingViewHolder;->checkBox:Landroid/widget/CheckBox;

    return-void
.end method
