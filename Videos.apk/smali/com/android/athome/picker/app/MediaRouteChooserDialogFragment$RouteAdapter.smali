.class Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
.super Landroid/widget/BaseAdapter;
.source "MediaRouteChooserDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RouteAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;
    }
.end annotation


# instance fields
.field private final mCatRouteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryEditingGroups:Ljava/lang/Object;

.field private mEditingGroup:Ljava/lang/Object;

.field private mIgnoreUpdates:Z

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedItemPosition:I

.field private final mSortRouteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCatRouteList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$902(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Ljava/lang/Object;

    return-object p1
.end method


# virtual methods
.method addGroupEditingCategoryRoutes(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v6, :cond_2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v5, :cond_0

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v1

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    invoke-static {v0, v4}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    iget-object v7, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    iget-object v7, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v8, v8, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mComparator:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteComparator;

    invoke-static {v7, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v7, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v7, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSortRouteList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    iget-object v7, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method addSelectableRoutes(Ljava/lang/Object;Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    :cond_0
    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method bindHeaderView(ILcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteCategory;->getName(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method bindItemView(ILcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;)V
    .locals 12
    .param p1    # I
    .param p2    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;

    const/4 v11, 0x2

    const/16 v8, 0x8

    const/4 v9, 0x1

    const/4 v7, 0x0

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    iget-object v6, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getName(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v4}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getStatus(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-static {v4}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getIconDrawable(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v6

    iget-object v10, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-virtual {v10}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    :cond_0
    iget-object v6, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v10, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->icon:Landroid/widget/ImageView;

    if-eqz v3, :cond_4

    move v6, v7

    :goto_1
    invoke-virtual {v10, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {v4}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getCategory(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Ljava/lang/Object;

    if-ne v1, v6, :cond_7

    invoke-static {v4}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iget-object v10, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->check:Landroid/widget/CheckBox;

    invoke-static {v2}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v6

    if-le v6, v9, :cond_5

    move v6, v9

    :goto_2
    invoke-virtual {v10, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v6, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->check:Landroid/widget/CheckBox;

    iget-object v10, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    if-ne v2, v10, :cond_6

    :goto_3
    invoke-virtual {v6, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_1
    :goto_4
    iget-object v6, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    if-eqz v6, :cond_2

    iget-object v6, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_a

    :goto_5
    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v6, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;

    iput p1, v6, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;->position:I

    :cond_2
    return-void

    :cond_3
    iget-object v6, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v6, p2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    move v6, v8

    goto :goto_1

    :cond_5
    move v6, v7

    goto :goto_2

    :cond_6
    move v9, v7

    goto :goto_3

    :cond_7
    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteCategory;->isGroupable(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v4}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v6

    if-gt v6, v9, :cond_8

    add-int/lit8 v6, p1, -0x1

    invoke-virtual {p0, v6}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    move-result v6

    if-eq v6, v11, :cond_8

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge p1, v6, :cond_9

    add-int/lit8 v6, p1, 0x1

    invoke-virtual {p0, v6}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    move-result v6

    if-ne v6, v11, :cond_9

    :cond_8
    move v0, v9

    :goto_6
    goto :goto_4

    :cond_9
    move v0, v7

    goto :goto_6

    :cond_a
    move v7, v8

    goto :goto_5
.end method

.method finishGrouping()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->scrollToSelectedItem()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat;->isRouteCategory(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    const/4 v1, 0x4

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getCategory(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Ljava/lang/Object;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x3

    goto :goto_0

    :cond_3
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    move-result v7

    if-nez p2, :cond_2

    iget-object v8, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v8}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$600(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/view/LayoutInflater;

    move-result-object v8

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->ITEM_LAYOUTS:[I
    invoke-static {}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$500()[I

    move-result-object v11

    aget v11, v11, v7

    invoke-virtual {v8, v11, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;

    const/4 v8, 0x0

    invoke-direct {v3, v8}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$1;)V

    iput p1, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->position:I

    const v8, 0x1020014

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->text1:Landroid/widget/TextView;

    const v8, 0x1020015

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->text2:Landroid/widget/TextView;

    sget v8, Lcom/android/athome/picker/R$id;->icon:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->icon:Landroid/widget/ImageView;

    sget v8, Lcom/android/athome/picker/R$id;->check:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    iput-object v8, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->check:Landroid/widget/CheckBox;

    sget v8, Lcom/android/athome/picker/R$id;->expand_button:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    iput-object v8, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    iget-object v8, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    if-eqz v8, :cond_0

    new-instance v8, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;

    invoke-direct {v8, p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;)V

    iput-object v8, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;

    iget-object v8, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupButton:Landroid/widget/ImageButton;

    iget-object v11, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->expandGroupListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter$ExpandGroupListener;

    invoke-virtual {v8, v11}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    move-object v2, p2

    move-object v4, p3

    check-cast v4, Landroid/widget/ListView;

    move-object v1, v3

    new-instance v8, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter$1;

    invoke-direct {v8, p0, v4, v2, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter$1;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;Landroid/widget/ListView;Landroid/view/View;Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;)V

    invoke-virtual {p2, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    packed-switch v7, :pswitch_data_0

    :goto_1
    instance-of v8, p2, Landroid/widget/Checkable;

    if-eqz v8, :cond_1

    move-object v0, p2

    check-cast v0, Landroid/widget/Checkable;

    const/4 v8, 0x3

    if-ne v7, v8, :cond_4

    iget-object v8, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    iget-object v8, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    if-ne v5, v8, :cond_3

    move v8, v9

    :goto_2
    invoke-interface {v0, v8}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_1
    :goto_3
    return-object p2

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;

    iput p1, v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;->position:I

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, p1, v3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->bindItemView(ILcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0, p1, v3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->bindHeaderView(ILcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;)V

    goto :goto_1

    :cond_3
    move v8, v10

    goto :goto_2

    :cond_4
    iget v8, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    if-ne p1, v8, :cond_5

    :goto_4
    invoke-interface {v0, v9}, Landroid/widget/Checkable;->setChecked(Z)V

    goto :goto_3

    :cond_5
    move v9, v10

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method isGrouping()Z
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual {p0, p3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItemViewType(I)I

    move-result v4

    const/4 v6, 0x1

    if-eq v4, v6, :cond_0

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v6, 0x4

    if-ne v4, v6, :cond_2

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->finishGrouping()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterCompat;->isRouteInfo(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v3, v1

    const/4 v6, 0x2

    if-ne v4, v6, :cond_3

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v6, v6, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I
    invoke-static {v7}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)I

    move-result v7

    invoke-static {v6, v7, v3}, Lcom/android/athome/picker/media/MediaRouterCompat;->selectRoute(Ljava/lang/Object;ILjava/lang/Object;)V

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-virtual {v6}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->dismiss()V

    goto :goto_0

    :cond_3
    const/4 v6, 0x3

    if-ne v4, v6, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/widget/Checkable;

    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v5

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mIgnoreUpdates:Z

    invoke-static {v3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getGroup(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v5, :cond_6

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    if-eq v2, v6, :cond_6

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v6, v6, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I
    invoke-static {v7}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)I

    move-result v7

    invoke-static {v6, v7}, Lcom/android/athome/picker/media/MediaRouterCompat;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v6

    if-ne v6, v2, :cond_4

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v6, v6, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I
    invoke-static {v7}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)I

    move-result v7

    iget-object v8, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/android/athome/picker/media/MediaRouterCompat;->selectRoute(Ljava/lang/Object;ILjava/lang/Object;)V

    :cond_4
    invoke-static {v2, v3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->removeRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    invoke-static {v6, v3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->addRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_5
    :goto_1
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mIgnoreUpdates:Z

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    goto :goto_0

    :cond_6
    if-eqz v5, :cond_5

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    invoke-static {v6}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_5

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;

    invoke-static {v6, v3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->removeRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v6, v6, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    invoke-static {v6, v3}, Lcom/android/athome/picker/media/MediaRouterCompat;->addUserRoute(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/widget/Checkable;->setChecked(Z)V

    goto :goto_1
.end method

.method scrollToEditingGroup()V
    .locals 6

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Ljava/lang/Object;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_3

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Ljava/lang/Object;

    if-ne v2, v5, :cond_2

    move v0, v1

    :cond_2
    if-nez v2, :cond_4

    move v4, v1

    :cond_3
    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5, v4, v0}, Landroid/widget/ListView;->smoothScrollToPosition(II)V

    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method scrollToSelectedItem()V
    .locals 2

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    goto :goto_0
.end method

.method update()V
    .locals 8

    iget-boolean v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mIgnoreUpdates:Z

    if-eqz v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v5, v5, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget-object v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I
    invoke-static {v6}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)I

    move-result v6

    invoke-static {v5, v6}, Lcom/android/athome/picker/media/MediaRouterCompat;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v4

    const/4 v5, -0x1

    iput v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v5, v5, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    invoke-static {v5}, Lcom/android/athome/picker/media/MediaRouterCompat;->getCategoryCount(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v5, v5, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    invoke-static {v5, v2}, Lcom/android/athome/picker/media/MediaRouterCompat;->getCategoryAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCatRouteList:Ljava/util/ArrayList;

    invoke-static {v0, v5}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteCategory;->getRoutes(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mCategoryEditingGroups:Ljava/lang/Object;

    if-ne v0, v5, :cond_1

    invoke-virtual {p0, v3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->addGroupEditingCategoryRoutes(Ljava/util/List;)V

    :goto_2
    invoke-interface {v3}, Ljava/util/List;->clear()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v4, v3}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->addSelectableRoutes(Ljava/lang/Object;Ljava/util/List;)V

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->notifyDataSetChanged()V

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v5

    if-eqz v5, :cond_3

    iget v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    if-ltz v5, :cond_3

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v5

    iget v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mSelectedItemPosition:I

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_3
    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/view/View$OnClickListener;

    move-result-object v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->showPerRouteVolumeSettings()Z
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsButton:Landroid/view/View;
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$400(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsButton:Landroid/view/View;
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$400(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/view/View;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method
