.class Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;
.super Ljava/lang/Object;
.source "MediaRouteChooserDialogFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "VolumeSliderChangeListener"
.end annotation


# instance fields
.field private mBaseProgress:F

.field private mBaseRxVolumes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseRxVolumes:Ljava/util/HashMap;

    return-void
.end method

.method private changeVolumeOnGroup(Ljava/lang/Object;I)V
    .locals 9
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeMax(Ljava/lang/Object;)I

    move-result v1

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseRxVolumes:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeMax(Ljava/lang/Object;)I

    move-result v4

    iget v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseProgress:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_1

    int-to-float v5, p2

    iget v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseProgress:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v6, v5

    int-to-float v7, v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    sub-float v5, v7, v5

    int-to-float v7, p2

    iget v8, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseProgress:F

    sub-float/2addr v7, v8

    mul-float/2addr v5, v7

    int-to-float v7, v1

    iget v8, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseProgress:F

    sub-float/2addr v7, v8

    div-float/2addr v5, v7

    add-float v3, v6, v5

    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-direct {p0, v5, v6}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->changeVolumeOnRoute(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    mul-int/2addr v5, p2

    int-to-float v5, v5

    iget v6, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseProgress:F

    div-float v3, v5, v6

    goto :goto_1

    :cond_1
    int-to-float v5, p2

    int-to-float v6, v1

    div-float v3, v5, v6

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->notifyDataSetChanged()V

    :cond_3
    return-void
.end method

.method private changeVolumeOnRoute(Ljava/lang/Object;I)V
    .locals 3
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeMax(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-static {p1, p2}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->requestSetVolume(Ljava/lang/Object;I)V

    return-void
.end method


# virtual methods
.method changeVolume(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChanges:Z
    invoke-static {v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$2300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v1, v1, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I
    invoke-static {v2}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/athome/picker/media/MediaRouterCompat;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeHandling(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat;->isRouteGroup(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->changeVolumeOnGroup(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0, p1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->changeVolumeOnRoute(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p0, p2}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->changeVolume(I)V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 6
    .param p1    # Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    const/4 v5, 0x1

    # setter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreCallbackVolumeChanges:Z
    invoke-static {v4, v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$2102(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Z)Z

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    int-to-float v4, v4

    iput v4, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseProgress:F

    iget-object v4, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v4, v4, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I
    invoke-static {v5}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)I

    move-result v5

    invoke-static {v4, v5}, Lcom/android/athome/picker/media/MediaRouterCompat;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/athome/picker/media/MediaRouterCompat;->isRouteGroup(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseRxVolumes:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    invoke-static {v3}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-static {v3, v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteAt(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    iget-object v4, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->mBaseRxVolumes:Ljava/util/HashMap;

    invoke-static {v1}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolume(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1    # Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    const/4 v1, 0x0

    # setter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreCallbackVolumeChanges:Z
    invoke-static {v0, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$2102(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Z)Z

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->updateVolume()V

    return-void
.end method
