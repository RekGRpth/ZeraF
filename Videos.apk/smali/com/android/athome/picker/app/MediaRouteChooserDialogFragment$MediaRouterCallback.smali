.class Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;
.super Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;
.source "MediaRouteChooserDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MediaRouterCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-direct {p0}, Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onRouteAdded(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v0

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->update()V
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->access$2000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)V

    :cond_0
    return-void
.end method

.method public onRouteChanged(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onRouteGrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;
    .param p4    # I

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v0

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->update()V
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->access$2000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)V

    :cond_0
    return-void
.end method

.method public onRouteRemoved(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v0

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->mEditingGroup:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->access$800(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;)Ljava/lang/Object;

    move-result-object v0

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->finishGrouping()V

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v0

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->update()V
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->access$2000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)V

    :cond_1
    return-void
.end method

.method public onRouteSelected(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->updateVolume()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v0

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->update()V
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->access$2000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)V

    :cond_0
    return-void
.end method

.method public onRouteUngrouped(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v0

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->update()V
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->access$2000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)V

    :cond_0
    return-void
.end method

.method public onRouteUnselected(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->update()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v0

    # invokes: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->update()V
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->access$2000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)V

    :cond_0
    return-void
.end method

.method public onRouteVolumeChanged(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreCallbackVolumeChanges:Z
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$2100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->updateVolume()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/android/athome/picker/media/MediaRouterCompat;->isRouteGroup(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreVolumeCallbackOnRoute:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1400(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Ljava/lang/Object;

    move-result-object v0

    if-eq p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
