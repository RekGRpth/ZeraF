.class public Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
.super Lvedroid/support/v4/app/DialogFragment;
.source "MediaRouteChooserDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$1;,
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;,
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;,
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$LauncherListener;,
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteChooserDialog;,
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteComparator;,
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;,
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;,
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;,
        Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_LAYOUTS:[I

.field private static final VOLUME_ITEM_LAYOUTS:[I


# instance fields
.field private mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

.field final mCallback:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

.field final mComparator:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteComparator;

.field private mExtendedSettingsButton:Landroid/view/View;

.field private mExtendedSettingsListener:Landroid/view/View$OnClickListener;

.field private mIgnoreCallbackVolumeChanges:Z

.field private mIgnoreSliderVolumeChangeOnRoute:Ljava/lang/Object;

.field private mIgnoreSliderVolumeChanges:Z

.field private mIgnoreVolumeCallbackOnRoute:Ljava/lang/Object;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLauncherListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$LauncherListener;

.field private mListView:Landroid/widget/ListView;

.field private mPerRouteVolumeMode:Z

.field private mRouteTypes:I

.field mRouter:Ljava/lang/Object;

.field private mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

.field private mVolumeIcon:Landroid/widget/ImageView;

.field private mVolumeSlider:Landroid/widget/SeekBar;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/android/athome/picker/R$layout;->media_route_list_item_top_header:I

    aput v1, v0, v3

    sget v1, Lcom/android/athome/picker/R$layout;->media_route_list_item_section_header:I

    aput v1, v0, v4

    sget v1, Lcom/android/athome/picker/R$layout;->media_route_list_item:I

    aput v1, v0, v5

    const/4 v1, 0x3

    sget v2, Lcom/android/athome/picker/R$layout;->media_route_list_item_checkable:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/android/athome/picker/R$layout;->media_route_list_item_collapse_group:I

    aput v2, v0, v1

    sput-object v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->ITEM_LAYOUTS:[I

    new-array v0, v5, [I

    sget v1, Lcom/android/athome/picker/R$layout;->media_route_volume_list_item:I

    aput v1, v0, v3

    sget v1, Lcom/android/athome/picker/R$layout;->media_route_volume_list_item_done:I

    aput v1, v0, v4

    sput-object v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->VOLUME_ITEM_LAYOUTS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    new-instance v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteComparator;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteComparator;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mComparator:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteComparator;

    new-instance v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mCallback:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

    const/4 v0, 0x1

    sget v1, Lcom/android/athome/picker/R$style;->Theme_Dialog:I

    invoke-virtual {p0, v0, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->setStyle(II)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)I
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    return v0
.end method

.method static synthetic access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1000()[I
    .locals 1

    sget-object v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->VOLUME_ITEM_LAYOUTS:[I

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-boolean v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreVolumeCallbackOnRoute:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreVolumeCallbackOnRoute:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChangeOnRoute:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChangeOnRoute:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-direct {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->enablePerRouteVolumeControl()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
    .param p1    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-boolean v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreCallbackVolumeChanges:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreCallbackVolumeChanges:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-boolean v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChanges:Z

    return v0
.end method

.method static synthetic access$300(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-direct {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->showPerRouteVolumeSettings()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500()[I
    .locals 1

    sget-object v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->ITEM_LAYOUTS:[I

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private enablePerRouteVolumeControl()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget v4, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    invoke-static {v3, v4}, Lcom/android/athome/picker/media/MediaRouterCompat;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat;->isRouteGroup(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteGroup;->getRouteCount(Ljava/lang/Object;)I

    move-result v3

    if-le v3, v1, :cond_1

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private showPerRouteVolumeSettings()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->enablePerRouteVolumeControl()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat;->forApplication(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # Landroid/os/Bundle;

    new-instance v0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteChooserDialog;

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->getTheme()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteChooserDialog;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Landroid/content/Context;I)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/android/athome/picker/R$layout;->media_route_chooser_layout:I

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/android/athome/picker/R$id;->volume_icon:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeIcon:Landroid/widget/ImageView;

    sget v2, Lcom/android/athome/picker/R$id;->volume_slider:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->updateVolume()V

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    new-instance v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;

    invoke-direct {v3, p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$VolumeSliderChangeListener;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    sget v2, Lcom/android/athome/picker/R$id;->extended_settings:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsButton:Landroid/view/View;

    iget-object v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    sget v2, Lcom/android/athome/picker/R$id;->list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    new-instance v2, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V

    iput-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    iget-object v4, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mCallback:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

    invoke-static {v2, v3, v4}, Lcom/android/athome/picker/media/MediaRouterCompat;->addCallback(Ljava/lang/Object;ILcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    invoke-virtual {v2}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;->scrollToSelectedItem()V

    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsButton:Landroid/view/View;

    new-instance v3, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;

    invoke-direct {v3, p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lvedroid/support/v4/app/DialogFragment;->onDetach()V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mLauncherListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$LauncherListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mLauncherListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$LauncherListener;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$LauncherListener;->onDetached(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    if-eqz v0, :cond_1

    iput-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteAdapter;

    :cond_1
    iput-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mInflater:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mCallback:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$MediaRouterCallback;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/MediaRouterCompat;->removeCallback(Ljava/lang/Object;Lcom/android/athome/picker/media/AbsMediaRouterCompat$Callback;)V

    iput-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lvedroid/support/v4/app/DialogFragment;->onResume()V

    return-void
.end method

.method public setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setLauncherListener(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$LauncherListener;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$LauncherListener;

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mLauncherListener:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$LauncherListener;

    return-void
.end method

.method public setRouteTypes(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    return-void
.end method

.method updateVolume()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouter:Ljava/lang/Object;

    iget v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mRouteTypes:I

    invoke-static {v1, v2}, Lcom/android/athome/picker/media/MediaRouterCompat;->getSelectedRoute(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeIcon:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getPlaybackType(Ljava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_1

    sget v1, Lcom/android/athome/picker/R$drawable;->ic_audio_vol_multi_holo_dark:I

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iput-boolean v3, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChanges:Z

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeHandling(Ljava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_2
    iput-boolean v4, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mIgnoreSliderVolumeChanges:Z

    goto :goto_0

    :cond_1
    sget v1, Lcom/android/athome/picker/R$drawable;->ic_media_route_on_holo_dark:I

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolumeMax(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeSlider:Landroid/widget/SeekBar;

    invoke-static {v0}, Lcom/android/athome/picker/media/MediaRouterCompat$RouteInfo;->getVolume(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2
.end method
