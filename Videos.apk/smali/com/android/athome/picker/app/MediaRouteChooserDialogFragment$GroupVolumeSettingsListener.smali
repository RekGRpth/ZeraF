.class Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;
.super Ljava/lang/Object;
.source "MediaRouteChooserDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GroupVolumeSettingsListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    const/4 v1, 0x1

    # setter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mPerRouteVolumeMode:Z
    invoke-static {v0, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1202(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Z)Z

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    new-instance v1, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    iget-object v2, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    invoke-direct {v1, v2}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;-><init>(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)V

    # setter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v0, v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1902(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$100(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mVolumeAdapter:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;
    invoke-static {v1}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$1900(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$RouteVolumeAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment$GroupVolumeSettingsListener;->this$0:Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;

    # getter for: Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->mExtendedSettingsButton:Landroid/view/View;
    invoke-static {v0}, Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;->access$400(Lcom/android/athome/picker/app/MediaRouteChooserDialogFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
