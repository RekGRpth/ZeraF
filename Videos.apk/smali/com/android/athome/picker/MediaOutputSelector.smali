.class public Lcom/android/athome/picker/MediaOutputSelector;
.super Lvedroid/support/v4/app/DialogFragment;
.source "MediaOutputSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/athome/picker/MediaOutputSelector$PrimaryVolumeListener;,
        Lcom/android/athome/picker/MediaOutputSelector$Listener;
    }
.end annotation


# static fields
.field private static mOutputNameComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-",
            "Lcom/android/athome/picker/MediaOutput;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

.field private mActiveOutputsList:Landroid/widget/ListView;

.field private mAtHomeGroupEditingDone:Landroid/widget/Button;

.field private mAtHomeGroupList:Landroid/widget/ListView;

.field private mAtHomeReceiverList:Landroid/widget/ListView;

.field private mAtHomeSection:Landroid/view/ViewGroup;

.field private mContentView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mDetailsIcon:Landroid/widget/ImageView;

.field private mDialog:Landroid/app/AlertDialog;

.field private mGroupAdapterListener:Lcom/android/athome/picker/MediaGroupAdapter$Listener;

.field private mGroupAdapters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/athome/picker/MediaGroupAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

.field private mMasterVolumeMode:I

.field private mMasterVolumeSlider:Landroid/widget/SeekBar;

.field private mMediaRouterCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;

.field private mOutputAdapters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/athome/picker/MediaOutputAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mPrimarySpeakerIcon:Landroid/widget/ImageView;

.field private mPrimarySpeakerIconLevel:Landroid/graphics/drawable/LevelListDrawable;

.field private mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

.field private mSpeakerSection:Landroid/view/ViewGroup;

.field private mSpeakersList:Landroid/widget/ListView;

.field private mUIMode:I

.field private mUIModeBySection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mUserRouteGroupLayout:Landroid/view/ViewGroup;

.field private mUserRouteGroupList:Landroid/widget/ListView;

.field private mUserRouteList:Landroid/widget/ListView;

.field private mUserRouteReceiverLayout:Landroid/view/ViewGroup;

.field private mUserRouteSection:Landroid/view/ViewGroup;

.field private mVolumeDetailSection:Landroid/view/ViewGroup;

.field private mVolumeSliderListener:Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/athome/picker/MediaOutputSelector$8;

    invoke-direct {v0}, Lcom/android/athome/picker/MediaOutputSelector$8;-><init>()V

    sput-object v0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputNameComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    new-instance v0, Lcom/android/athome/picker/MediaOutputSelector$1;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/MediaOutputSelector$1;-><init>(Lcom/android/athome/picker/MediaOutputSelector;)V

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMediaRouterCallback:Lcom/android/athome/picker/media/AbsMediaRouterCompat$SimpleCallback;

    new-instance v0, Lcom/android/athome/picker/MediaOutputSelector$9;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/MediaOutputSelector$9;-><init>(Lcom/android/athome/picker/MediaOutputSelector;)V

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapterListener:Lcom/android/athome/picker/MediaGroupAdapter$Listener;

    new-instance v0, Lcom/android/athome/picker/MediaOutputSelector$10;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/MediaOutputSelector$10;-><init>(Lcom/android/athome/picker/MediaOutputSelector;)V

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mVolumeSliderListener:Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/athome/picker/MediaOutputSelector;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->toggleMasterVolumeMute()V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/athome/picker/MediaOutputSelector;)Lcom/android/athome/picker/MediaOutputGroup;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->getSelectedGroup()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/athome/picker/MediaOutputSelector;)Landroid/graphics/drawable/LevelListDrawable;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIconLevel:Landroid/graphics/drawable/LevelListDrawable;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/athome/picker/MediaOutputSelector;)Lcom/android/athome/picker/MediaOutput;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/athome/picker/MediaOutputSelector;)Lcom/android/athome/picker/MediaOutputSelector$Listener;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/athome/picker/MediaOutputSelector;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;
    .param p1    # Lcom/android/athome/picker/MediaOutputGroup;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/MediaOutputSelector;->updateVolumeDetailsView(Lcom/android/athome/picker/MediaOutputGroup;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/android/athome/picker/MediaOutputSelector;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateMasterVolumeViews()V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/athome/picker/MediaOutputSelector;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/athome/picker/MediaOutputSelector;Lcom/android/athome/picker/MediaGroupAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;
    .param p1    # Lcom/android/athome/picker/MediaGroupAdapter;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/MediaOutputSelector;->getIndividualOutputs(Lcom/android/athome/picker/MediaGroupAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/athome/picker/MediaOutputSelector;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/athome/picker/MediaOutputSelector;Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;
    .param p1    # Landroid/widget/ArrayAdapter;
    .param p2    # Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/MediaOutputSelector;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/athome/picker/MediaOutputSelector;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->onStartVolumeDetailsChange()V

    return-void
.end method

.method static synthetic access$2000(Lcom/android/athome/picker/MediaOutputSelector;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeReceiverList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/athome/picker/MediaOutputSelector;Lcom/android/athome/picker/MediaOutput;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/MediaOutputSelector;->editGroup(Lcom/android/athome/picker/MediaOutput;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/athome/picker/MediaOutputSelector;Lcom/android/athome/picker/MediaOutputAdapter;Lcom/android/athome/picker/MediaOutput;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;
    .param p1    # Lcom/android/athome/picker/MediaOutputAdapter;
    .param p2    # Lcom/android/athome/picker/MediaOutput;

    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/MediaOutputSelector;->selectOutput(Lcom/android/athome/picker/MediaOutputAdapter;Lcom/android/athome/picker/MediaOutput;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/athome/picker/MediaOutputSelector;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/athome/picker/MediaOutputSelector;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/athome/picker/MediaOutputSelector;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateViewVisibility()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/athome/picker/MediaOutputSelector;Lcom/android/athome/picker/MediaGroupAdapter;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 0
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;
    .param p1    # Lcom/android/athome/picker/MediaGroupAdapter;
    .param p2    # Lcom/android/athome/picker/MediaOutputGroup;

    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/MediaOutputSelector;->selectOutputGroup(Lcom/android/athome/picker/MediaGroupAdapter;Lcom/android/athome/picker/MediaOutputGroup;)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutput;

    invoke-static {p0}, Lcom/android/athome/picker/MediaOutputSelector;->isOnSpeaker(Lcom/android/athome/picker/MediaOutput;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/android/athome/picker/MediaOutputSelector;)I
    .locals 1
    .param p0    # Lcom/android/athome/picker/MediaOutputSelector;

    iget v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeMode:I

    return v0
.end method

.method private clearSelected()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->isGroupSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaGroupAdapter;

    invoke-virtual {v0, v2}, Lcom/android/athome/picker/MediaGroupAdapter;->setSelectedGroup(Lcom/android/athome/picker/MediaOutputGroup;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutputAdapter;

    invoke-virtual {v0, v2}, Lcom/android/athome/picker/MediaOutputAdapter;->setSelectedOutput(Lcom/android/athome/picker/MediaOutput;)V

    goto :goto_0
.end method

.method private configureGroupList(Landroid/widget/ListView;Ljava/util/ArrayList;I)V
    .locals 4
    .param p1    # Landroid/widget/ListView;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/athome/picker/MediaOutputGroup;",
            ">;I)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/high16 v1, 0x1000000

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    new-instance v0, Lcom/android/athome/picker/MediaGroupAdapter;

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContext:Landroid/content/Context;

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/android/athome/picker/MediaGroupAdapter;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapterListener:Lcom/android/athome/picker/MediaGroupAdapter$Listener;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/MediaGroupAdapter;->setListener(Lcom/android/athome/picker/MediaGroupAdapter$Listener;)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2, v0}, Lcom/android/athome/picker/MediaOutputSelector;->updateGroupAdapter(Ljava/util/ArrayList;Lcom/android/athome/picker/MediaGroupAdapter;)V

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaGroupAdapter;->setView(Landroid/widget/AdapterView;)V

    new-instance v1, Lcom/android/athome/picker/MediaOutputSelector$7;

    invoke-direct {v1, p0, v0}, Lcom/android/athome/picker/MediaOutputSelector$7;-><init>(Lcom/android/athome/picker/MediaOutputSelector;Lcom/android/athome/picker/MediaGroupAdapter;)V

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method private configureReceiverList(Landroid/widget/ListView;Ljava/util/ArrayList;I)V
    .locals 4
    .param p1    # Landroid/widget/ListView;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/athome/picker/MediaOutput;",
            ">;I)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/high16 v1, 0x1000000

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    new-instance v0, Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContext:Landroid/content/Context;

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2, p3}, Lcom/android/athome/picker/MediaOutputAdapter;-><init>(Landroid/content/Context;II)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, p2}, Lcom/android/athome/picker/MediaOutputSelector;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutputAdapter;->setView(Landroid/widget/AdapterView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/android/athome/picker/MediaOutputSelector$6;

    invoke-direct {v1, p0, v0}, Lcom/android/athome/picker/MediaOutputSelector$6;-><init>(Lcom/android/athome/picker/MediaOutputSelector;Lcom/android/athome/picker/MediaOutputAdapter;)V

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private configureUi(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputSelector;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v8, "selected_output"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/android/athome/picker/MediaOutput;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    const-string v8, "MediaOutputSelector"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SelectedOutput: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    const-string v8, "ui_mode"

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIMode:I

    :goto_0
    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    if-eqz v8, :cond_1

    const/4 v4, 0x1

    :goto_1
    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->icon_volume:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIcon:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIcon:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    check-cast v8, Landroid/graphics/drawable/LevelListDrawable;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIconLevel:Landroid/graphics/drawable/LevelListDrawable;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIcon:Landroid/widget/ImageView;

    new-instance v9, Lcom/android/athome/picker/MediaOutputSelector$2;

    invoke-direct {v9, p0}, Lcom/android/athome/picker/MediaOutputSelector$2;-><init>(Lcom/android/athome/picker/MediaOutputSelector;)V

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIcon:Landroid/widget/ImageView;

    invoke-virtual {v8, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->volume_slider:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/SeekBar;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    new-instance v9, Lcom/android/athome/picker/MediaOutputSelector$PrimaryVolumeListener;

    const/4 v10, 0x0

    invoke-direct {v9, p0, v10}, Lcom/android/athome/picker/MediaOutputSelector$PrimaryVolumeListener;-><init>(Lcom/android/athome/picker/MediaOutputSelector;Lcom/android/athome/picker/MediaOutputSelector$1;)V

    invoke-virtual {v8, v9}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v8, v4}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->icon_detailed:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mDetailsIcon:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mDetailsIcon:Landroid/widget/ImageView;

    new-instance v9, Lcom/android/athome/picker/MediaOutputSelector$3;

    invoke-direct {v9, p0}, Lcom/android/athome/picker/MediaOutputSelector$3;-><init>(Lcom/android/athome/picker/MediaOutputSelector;)V

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateDetailsIconVisibility()V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->volume_details_section:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mVolumeDetailSection:Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->active_output_list:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputsList:Landroid/widget/ListView;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputsList:Landroid/widget/ListView;

    const/high16 v9, 0x1000000

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->speakers:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakerSection:Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->speaker_list:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakersList:Landroid/widget/ListView;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakersList:Landroid/widget/ListView;

    const/high16 v9, 0x1000000

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    const-string v8, "speaker"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v10, 0x2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContext:Landroid/content/Context;

    const/4 v9, -0x1

    const/4 v10, 0x1

    invoke-direct {v0, v8, v9, v10}, Lcom/android/athome/picker/MediaOutputAdapter;-><init>(Landroid/content/Context;II)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, v5}, Lcom/android/athome/picker/MediaOutputSelector;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakersList:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Lcom/android/athome/picker/MediaOutputAdapter;->setView(Landroid/widget/AdapterView;)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakersList:Landroid/widget/ListView;

    new-instance v9, Lcom/android/athome/picker/MediaOutputSelector$4;

    invoke-direct {v9, p0, v0}, Lcom/android/athome/picker/MediaOutputSelector$4;-><init>(Lcom/android/athome/picker/MediaOutputSelector;Lcom/android/athome/picker/MediaOutputAdapter;)V

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakersList:Landroid/widget/ListView;

    invoke-virtual {v8, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_2
    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->at_home_section:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeSection:Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->icon_group_editing_done:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupEditingDone:Landroid/widget/Button;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupEditingDone:Landroid/widget/Button;

    new-instance v9, Lcom/android/athome/picker/MediaOutputSelector$5;

    invoke-direct {v9, p0}, Lcom/android/athome/picker/MediaOutputSelector$5;-><init>(Lcom/android/athome/picker/MediaOutputSelector;)V

    invoke-virtual {v8, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->at_home_group_list:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupList:Landroid/widget/ListView;

    const-string v8, "athome_group"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupList:Landroid/widget/ListView;

    const/4 v9, 0x2

    invoke-direct {p0, v8, v2, v9}, Lcom/android/athome/picker/MediaOutputSelector;->configureGroupList(Landroid/widget/ListView;Ljava/util/ArrayList;I)V

    :goto_3
    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->at_home_receiver_list:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeReceiverList:Landroid/widget/ListView;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeReceiverList:Landroid/widget/ListView;

    const/high16 v9, 0x1000000

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    const-string v8, "Nexus Q"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeReceiverList:Landroid/widget/ListView;

    const/4 v9, 0x2

    invoke-direct {p0, v8, v3, v9}, Lcom/android/athome/picker/MediaOutputSelector;->configureReceiverList(Landroid/widget/ListView;Ljava/util/ArrayList;I)V

    :goto_4
    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->user_route_section:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteSection:Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->user_route_groups:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteGroupLayout:Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->user_route_group_list:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteGroupList:Landroid/widget/ListView;

    const-string v8, "user_route_group"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteGroupList:Landroid/widget/ListView;

    const/16 v9, 0x3e8

    invoke-direct {p0, v8, v6, v9}, Lcom/android/athome/picker/MediaOutputSelector;->configureGroupList(Landroid/widget/ListView;Ljava/util/ArrayList;I)V

    :goto_5
    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->user_routes:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteReceiverLayout:Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    sget v9, Lcom/android/athome/picker/R$id;->user_route_list:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    iput-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteList:Landroid/widget/ListView;

    const-string v8, "user_route"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_6

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteList:Landroid/widget/ListView;

    const/16 v9, 0x3e8

    invoke-direct {p0, v8, v7, v9}, Lcom/android/athome/picker/MediaOutputSelector;->configureReceiverList(Landroid/widget/ListView;Ljava/util/ArrayList;I)V

    :goto_6
    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->setSelectedOutput()V

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateViewVisibility()V

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateLayoutHeight()V

    return-void

    :cond_0
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIMode:I

    goto/16 :goto_0

    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_2
    const-string v8, "MediaOutputSelector"

    const-string v9, "No speaker outputs are available."

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_3
    const-string v8, "MediaOutputSelector"

    const-string v9, "No athome groups are available."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_4
    const-string v8, "MediaOutputSelector"

    const-string v9, "No individual athome receivers are passed in."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_5
    const-string v8, "MediaOutputSelector"

    const-string v9, "No user defined groups are available."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_6
    const-string v8, "MediaOutputSelector"

    const-string v9, "No individual user defined receivers are passed in."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method

.method private editGroup(Lcom/android/athome/picker/MediaOutput;)V
    .locals 4
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->isGroupSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    check-cast v0, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->contains(Lcom/android/athome/picker/MediaOutput;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "MediaOutputSelector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Remove "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from group:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->remove(Lcom/android/athome/picker/MediaOutput;)Z

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    invoke-interface {v1, p1, v0}, Lcom/android/athome/picker/MediaOutputSelector$Listener;->onOutputRemoved(Lcom/android/athome/picker/MediaOutput;Lcom/android/athome/picker/MediaOutputGroup;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "MediaOutputSelector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Add "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to group:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->add(Lcom/android/athome/picker/MediaOutput;)Z

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    invoke-interface {v1, p1, v0}, Lcom/android/athome/picker/MediaOutputSelector$Listener;->onOutputAdded(Lcom/android/athome/picker/MediaOutput;Lcom/android/athome/picker/MediaOutputGroup;)V

    goto :goto_0
.end method

.method private getIndividualOutputs(Lcom/android/athome/picker/MediaGroupAdapter;)Ljava/util/ArrayList;
    .locals 6
    .param p1    # Lcom/android/athome/picker/MediaGroupAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/athome/picker/MediaGroupAdapter;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/athome/picker/MediaOutput;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_1

    const/4 v4, 0x0

    :cond_0
    return-object v4

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/android/athome/picker/MediaGroupAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_0

    invoke-virtual {p1, v1}, Lcom/android/athome/picker/MediaGroupAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getSelectedGroup()Lcom/android/athome/picker/MediaOutputGroup;
    .locals 1

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->isGroupSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    check-cast v0, Lcom/android/athome/picker/MediaOutputGroup;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isGroupSelected()Z
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    instance-of v0, v0, Lcom/android/athome/picker/MediaOutputGroup;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isOnSpeaker(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 2
    .param p0    # Lcom/android/athome/picker/MediaOutput;

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onStartVolumeDetailsChange()V
    .locals 5

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->getSelectedGroup()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIMode:I

    new-instance v1, Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContext:Landroid/content/Context;

    const/4 v3, -0x1

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getType()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/android/athome/picker/MediaOutputAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputsList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/android/athome/picker/MediaOutputAdapter;->setMode(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v1, v2}, Lcom/android/athome/picker/MediaOutputSelector;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputsList:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Lcom/android/athome/picker/MediaOutputAdapter;->setView(Landroid/widget/AdapterView;)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mVolumeSliderListener:Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;

    invoke-virtual {v1, v2}, Lcom/android/athome/picker/MediaOutputAdapter;->setVolumeSliderListener(Lcom/android/athome/picker/MediaOutputAdapter$VolumeSliderListener;)V

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateViewVisibility()V

    goto :goto_0
.end method

.method private declared-synchronized selectOutput(Lcom/android/athome/picker/MediaOutputAdapter;Lcom/android/athome/picker/MediaOutput;)V
    .locals 3
    .param p1    # Lcom/android/athome/picker/MediaOutputAdapter;
    .param p2    # Lcom/android/athome/picker/MediaOutput;

    monitor-enter p0

    :try_start_0
    const-string v0, "MediaOutputSelector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SelectedOutput: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->clearSelected()V

    iput-object p2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/MediaOutputAdapter;->setSelectedOutput(Lcom/android/athome/picker/MediaOutput;)V

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    invoke-interface {v0, p2}, Lcom/android/athome/picker/MediaOutputSelector$Listener;->onOutputSelected(Lcom/android/athome/picker/MediaOutput;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateMasterVolumeViews()V

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputSelector;->dismiss()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized selectOutputGroup(Lcom/android/athome/picker/MediaGroupAdapter;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 3
    .param p1    # Lcom/android/athome/picker/MediaGroupAdapter;
    .param p2    # Lcom/android/athome/picker/MediaOutputGroup;

    monitor-enter p0

    :try_start_0
    const-string v0, "MediaOutputSelector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SelectedOutputGroup: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->clearSelected()V

    iput-object p2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/MediaGroupAdapter;->setSelectedGroup(Lcom/android/athome/picker/MediaOutputGroup;)V

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    invoke-interface {v0, p2}, Lcom/android/athome/picker/MediaOutputSelector$Listener;->onOutputSelected(Lcom/android/athome/picker/MediaOutput;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateMasterVolumeViews()V

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputSelector;->dismiss()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setSelectedOutput()V
    .locals 3

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->getSelectedGroup()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaGroupAdapter;

    invoke-virtual {v1, v0}, Lcom/android/athome/picker/MediaGroupAdapter;->setSelectedGroup(Lcom/android/athome/picker/MediaOutputGroup;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaOutputAdapter;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1, v2}, Lcom/android/athome/picker/MediaOutputAdapter;->setSelectedOutput(Lcom/android/athome/picker/MediaOutput;)V

    goto :goto_0
.end method

.method private toggleMasterVolumeMute()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeMode:I

    if-ne v3, v1, :cond_3

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    iget-object v4, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v4}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v4

    if-nez v4, :cond_2

    :goto_1
    invoke-virtual {v3, v1}, Lcom/android/athome/picker/MediaOutput;->setIsMuted(Z)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-interface {v1, v2}, Lcom/android/athome/picker/MediaOutputSelector$Listener;->onVolumeChanged(Lcom/android/athome/picker/MediaOutput;)V

    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateMasterVolumeViews()V

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->getSelectedGroup()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getIsMuted()Z

    move-result v3

    if-nez v3, :cond_4

    :goto_3
    invoke-virtual {v0, v1}, Lcom/android/athome/picker/MediaOutputGroup;->setIsMuted(Z)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    invoke-interface {v1, v0}, Lcom/android/athome/picker/MediaOutputSelector$Listener;->onVolumeChanged(Lcom/android/athome/picker/MediaOutput;)V

    invoke-direct {p0, v0}, Lcom/android/athome/picker/MediaOutputSelector;->updateVolumeDetailsView(Lcom/android/athome/picker/MediaOutputGroup;)V

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method private updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/athome/picker/MediaOutput;",
            ">;",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/android/athome/picker/MediaOutput;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {p1, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/android/athome/picker/MediaOutputSelector;->mOutputNameComparator:Ljava/util/Comparator;

    invoke-virtual {p1, v2}, Landroid/widget/ArrayAdapter;->sort(Ljava/util/Comparator;)V

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method private updateDetailsIconVisibility()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIMode:I

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->getSelectedGroup()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->size()I

    move-result v3

    if-le v3, v1, :cond_1

    :goto_0
    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mDetailsIcon:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    const/16 v2, 0x8

    goto :goto_1
.end method

.method private updateGroupAdapter(Ljava/util/ArrayList;Lcom/android/athome/picker/MediaGroupAdapter;)V
    .locals 3
    .param p2    # Lcom/android/athome/picker/MediaGroupAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/athome/picker/MediaOutputGroup;",
            ">;",
            "Lcom/android/athome/picker/MediaGroupAdapter;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Lcom/android/athome/picker/MediaGroupAdapter;->setNotifyOnChange(Z)V

    invoke-virtual {p2}, Lcom/android/athome/picker/MediaGroupAdapter;->clear()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {p2, v1}, Lcom/android/athome/picker/MediaGroupAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/android/athome/picker/MediaOutputSelector;->mOutputNameComparator:Ljava/util/Comparator;

    invoke-virtual {p2, v2}, Lcom/android/athome/picker/MediaGroupAdapter;->sort(Ljava/util/Comparator;)V

    invoke-virtual {p2}, Lcom/android/athome/picker/MediaGroupAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method private updateLayoutHeight()V
    .locals 0

    return-void
.end method

.method private updateMasterVolumeViews()V
    .locals 9

    const/16 v5, 0x65

    const/4 v6, 0x1

    const/4 v4, 0x0

    iget-object v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    if-nez v7, :cond_0

    iput v4, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeMode:I

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIconLevel:Landroid/graphics/drawable/LevelListDrawable;

    invoke-virtual {v5, v4}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v5, v4}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_0
    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateDetailsIconVisibility()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->getSelectedGroup()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v6, :cond_4

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getIsMuted()Z

    move-result v1

    const/4 v7, 0x2

    iput v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeMode:I

    if-eqz v1, :cond_2

    move v2, v4

    :goto_1
    iget-object v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v7, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIconLevel:Landroid/graphics/drawable/LevelListDrawable;

    if-eqz v1, :cond_3

    :goto_2
    invoke-virtual {v7, v5}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    if-nez v1, :cond_1

    move v4, v6

    :cond_1
    invoke-virtual {v5, v4}, Landroid/widget/SeekBar;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getVolume()F

    move-result v7

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getMax()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    float-to-int v2, v7

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getProgress()I

    move-result v5

    goto :goto_2

    :cond_4
    iput v6, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeMode:I

    iget-object v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v7}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v3

    iget-object v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getMax()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v3

    float-to-int v8, v8

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mPrimarySpeakerIconLevel:Landroid/graphics/drawable/LevelListDrawable;

    iget-object v8, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v8}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v8

    if-eqz v8, :cond_5

    :goto_3
    invoke-virtual {v7, v5}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    iget-object v7, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSelectedOutput:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v7}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v7

    if-nez v7, :cond_6

    :goto_4
    invoke-virtual {v5, v6}, Landroid/widget/SeekBar;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_5
    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mMasterVolumeSlider:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getProgress()I

    move-result v5

    goto :goto_3

    :cond_6
    move v6, v4

    goto :goto_4
.end method

.method private updateViewVisibility()V
    .locals 7

    const/16 v6, 0x3e8

    const/4 v3, 0x2

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIMode:I

    packed-switch v1, :pswitch_data_0

    const-string v1, "MediaOutputSelector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported UI mode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateMasterVolumeViews()V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mVolumeDetailSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakerSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakersList:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    :goto_1
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    const-string v1, "MediaOutputSelector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknow section UI mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    const-string v1, "MediaOutputSelector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknow section UI mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakerSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakersList:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupList:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeReceiverList:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupEditingDone:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeReceiverList:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupList:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupEditingDone:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    :pswitch_3
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeReceiverList:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupList:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeGroupEditingDone:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_2

    :cond_1
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_2

    :pswitch_4
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteGroupLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteGroupList:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteReceiverLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteGroupLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteReceiverLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteList:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_6
    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateMasterVolumeViews()V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mDetailsIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mVolumeDetailSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutputAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputsList:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    :goto_3
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mSpeakerSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mAtHomeSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUserRouteSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputsList:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private updateVolumeDetailsView(Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 3
    .param p1    # Lcom/android/athome/picker/MediaOutputGroup;

    iget v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mActiveOutputAdapter:Lcom/android/athome/picker/MediaOutputAdapter;

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0, v1}, Lcom/android/athome/picker/MediaOutputSelector;->updateAdapter(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/athome/picker/media/MediaRouterCompat;->forApplication(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutputSelector$Listener;

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/MediaOutputSelector;->setListener(Lcom/android/athome/picker/MediaOutputSelector$Listener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputSelector;->mUIModeBySection:Ljava/util/HashMap;

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x1

    new-instance v4, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputSelector;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    sget v6, Lcom/android/athome/picker/R$style;->Theme_Dialog:I

    invoke-direct {v4, v5, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/view/ContextThemeWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    sget v5, Lcom/android/athome/picker/R$layout;->media_output_selector:I

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/athome/picker/MediaOutputSelector;->configureUi(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mContentView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mDialog:Landroid/app/AlertDialog;

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5, v7}, Landroid/app/AlertDialog;->requestWindowFeature(I)Z

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v5, 0x30

    invoke-virtual {v3, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/android/athome/picker/R$dimen;->volume_panel_top:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v5, p0, Lcom/android/athome/picker/MediaOutputSelector;->mDialog:Landroid/app/AlertDialog;

    return-object v5
.end method

.method public onOutputRemoved(Lcom/android/athome/picker/MediaOutput;)V
    .locals 5
    .param p1    # Lcom/android/athome/picker/MediaOutput;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v2

    instance-of v3, p1, Lcom/android/athome/picker/MediaOutputGroup;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mGroupAdapters:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/MediaGroupAdapter;

    check-cast p1, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {v1, p1}, Lcom/android/athome/picker/MediaGroupAdapter;->remove(Ljava/lang/Object;)V

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaGroupAdapter;->notifyDataSetChanged()V

    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/android/athome/picker/MediaOutputSelector;->updateMasterVolumeViews()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/MediaOutputSelector;->mOutputAdapters:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutputAdapter;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutputAdapter;->remove(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputAdapter;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method public setListener(Lcom/android/athome/picker/MediaOutputSelector$Listener;)V
    .locals 0
    .param p1    # Lcom/android/athome/picker/MediaOutputSelector$Listener;

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutputSelector;->mListener:Lcom/android/athome/picker/MediaOutputSelector$Listener;

    return-void
.end method
