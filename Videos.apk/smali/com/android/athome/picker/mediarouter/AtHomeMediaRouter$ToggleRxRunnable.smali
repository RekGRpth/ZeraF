.class Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;
.super Ljava/lang/Object;
.source "AtHomeMediaRouter.java"

# interfaces
.implements Landroid/support/place/music/TungstenGroupingService$OnAssignRxToGroup;
.implements Landroid/support/place/music/TungstenGroupingService$OnRemoveRxFromGroup;
.implements Landroid/support/place/rpc/RpcErrorHandler;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ToggleRxRunnable"
.end annotation


# instance fields
.field private mGroupId:Ljava/lang/String;

.field private mReceiverId:Ljava/lang/String;

.field private mRemove:Z

.field final synthetic this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mReceiverId:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mGroupId:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mRemove:Z

    return-void
.end method


# virtual methods
.method public onAssignRxToGroup(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "assigned: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mReceiverId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to group: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mGroupId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    const/4 v1, 0x0

    # setter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAssigningRxToGroup:Z
    invoke-static {v0, v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$4402(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getLatestTgsState()V
    invoke-static {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$4500(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    return-void
.end method

.method public onError(Landroid/support/place/rpc/RpcError;)V
    .locals 4
    .param p1    # Landroid/support/place/rpc/RpcError;

    const/4 v3, 0x0

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AssignRxRunnable error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # setter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAssigningRxToGroup:Z
    invoke-static {v0, v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$4402(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # setter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemovingFromGroup:Z
    invoke-static {v0, v3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$4302(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getLatestTgsState()V
    invoke-static {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$4500(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    return-void
.end method

.method public onRemoveRxFromGroup(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Removed Rx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mReceiverId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    const/4 v1, 0x0

    # setter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemovingFromGroup:Z
    invoke-static {v0, v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$4302(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # invokes: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->getLatestTgsState()V
    invoke-static {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$4500(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)V

    return-void
.end method

.method public run()V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;
    invoke-static {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$2000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/music/TungstenGroupingService;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "TGS not ready."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mRemove:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # setter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mRemovingFromGroup:Z
    invoke-static {v0, v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$4302(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Removing Rx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mReceiverId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;
    invoke-static {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$2000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/music/TungstenGroupingService;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mReceiverId:Ljava/lang/String;

    invoke-virtual {v0, v1, p0, p0}, Landroid/support/place/music/TungstenGroupingService;->removeRxFromGroup(Ljava/lang/String;Landroid/support/place/music/TungstenGroupingService$OnRemoveRxFromGroup;Landroid/support/place/rpc/RpcErrorHandler;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # setter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mAssigningRxToGroup:Z
    invoke-static {v0, v1}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$4402(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Z)Z

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Assigning Rx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mReceiverId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to group: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mGroupId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    # getter for: Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->mGroupingService:Landroid/support/place/music/TungstenGroupingService;
    invoke-static {v0}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;->access$2000(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;)Landroid/support/place/music/TungstenGroupingService;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mGroupId:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$ToggleRxRunnable;->mReceiverId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p0, p0}, Landroid/support/place/music/TungstenGroupingService;->assignRxToGroup(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/music/TungstenGroupingService$OnAssignRxToGroup;Landroid/support/place/rpc/RpcErrorHandler;)V

    goto :goto_0
.end method
