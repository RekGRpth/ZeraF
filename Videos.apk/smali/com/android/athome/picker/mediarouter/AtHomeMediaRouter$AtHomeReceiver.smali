.class public Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;
.super Ljava/lang/Object;
.source "AtHomeMediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AtHomeReceiver"
.end annotation


# instance fields
.field private mConnectors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/place/connector/ConnectorInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceSerialNumber:Ljava/lang/String;

.field private mRoute:Ljava/lang/Object;

.field final synthetic this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;Ljava/util/List;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<",
            "Landroid/support/place/connector/ConnectorInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->this$0:Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->mRoute:Ljava/lang/Object;

    iput-object p3, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->mConnectors:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<",
            "Landroid/support/place/connector/ConnectorInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;-><init>(Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter;Ljava/lang/Object;Ljava/util/List;)V

    iput-object p4, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->mDeviceSerialNumber:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getConnectors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/place/connector/ConnectorInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->mConnectors:Ljava/util/List;

    return-object v0
.end method

.method public getDeviceSerialNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->mDeviceSerialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getRoute()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/mediarouter/AtHomeMediaRouter$AtHomeReceiver;->mRoute:Ljava/lang/Object;

    return-object v0
.end method
