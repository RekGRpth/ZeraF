.class public Lcom/android/athome/utils/NfcHelper;
.super Ljava/lang/Object;
.source "NfcHelper.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mDefaultUri:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mHasNdefMessage:Z

.field private mRegisteredCallbacks:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/athome/utils/NfcHelper;->mActivity:Landroid/app/Activity;

    new-instance v0, Lcom/android/athome/utils/NfcHelper$1;

    invoke-direct {v0, p0}, Lcom/android/athome/utils/NfcHelper$1;-><init>(Lcom/android/athome/utils/NfcHelper;)V

    iput-object v0, p0, Lcom/android/athome/utils/NfcHelper;->mHandler:Landroid/os/Handler;

    iput-object p2, p0, Lcom/android/athome/utils/NfcHelper;->mDefaultUri:Ljava/lang/String;

    return-void
.end method

.method private getPairingRecord()Landroid/nfc/NdefRecord;
    .locals 8

    const-string v2, "application/com.google.android.setupwarlock.SETUP"

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v3, Landroid/nfc/NdefRecord;

    const/4 v4, 0x2

    const-string v5, "US_ASCII"

    invoke-static {v5}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [B

    const-string v7, "US-ASCII"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    return-object v3

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 6
    .param p1    # Landroid/nfc/NfcEvent;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v1, p0, Lcom/android/athome/utils/NfcHelper;->mHasNdefMessage:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/athome/utils/NfcHelper;->getUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v1, Landroid/nfc/NdefMessage;

    new-array v2, v5, [Landroid/nfc/NdefRecord;

    invoke-direct {p0}, Lcom/android/athome/utils/NfcHelper;->getPairingRecord()Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-direct {v1, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/nfc/NdefMessage;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/nfc/NdefRecord;

    invoke-static {v0}, Landroid/nfc/NdefRecord;->createUri(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-direct {p0}, Lcom/android/athome/utils/NfcHelper;->getPairingRecord()Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-direct {v1, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0
.end method

.method public getUri()Ljava/lang/String;
    .locals 4

    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    const-string v2, " eng."

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "NfcHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not going to push URL on NFC.  Custom build: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/android/athome/utils/NfcHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "aah:remote_control_apk_url"

    invoke-static {v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/athome/utils/NfcHelper;->mDefaultUri:Ljava/lang/String;

    :cond_1
    const-string v1, "%buildid%"

    sget-object v2, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 4
    .param p1    # Landroid/nfc/NfcEvent;

    invoke-virtual {p0}, Lcom/android/athome/utils/NfcHelper;->onPause()V

    iget-object v0, p0, Lcom/android/athome/utils/NfcHelper;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3fd

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/utils/NfcHelper;->mHasNdefMessage:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/utils/NfcHelper;->mHasNdefMessage:Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 6

    const/4 v5, 0x1

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/android/athome/utils/NfcHelper;->mRegisteredCallbacks:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/utils/NfcHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/athome/utils/NfcHelper;->mActivity:Landroid/app/Activity;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, p0, v1, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/android/athome/utils/NfcHelper;->mActivity:Landroid/app/Activity;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, p0, v1, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    :goto_0
    iput-boolean v5, p0, Lcom/android/athome/utils/NfcHelper;->mRegisteredCallbacks:Z

    :cond_0
    iget-boolean v1, p0, Lcom/android/athome/utils/NfcHelper;->mHasNdefMessage:Z

    if-nez v1, :cond_1

    const-string v1, "NfcHelper"

    const-string v2, "Resuming."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v5, p0, Lcom/android/athome/utils/NfcHelper;->mHasNdefMessage:Z

    :cond_1
    return-void

    :cond_2
    const-string v1, "NfcHelper"

    const-string v2, "NFC wasn\'t ready, retry in a bit."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/athome/utils/NfcHelper;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3fd

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
