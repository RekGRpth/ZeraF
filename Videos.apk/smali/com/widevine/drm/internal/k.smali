.class public final Lcom/widevine/drm/internal/k;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:Ljava/net/URL;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Thread;

.field private e:Lcom/widevine/drm/internal/HTTPDecrypter;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/widevine/drm/internal/k;->a:I

    iput-object p3, p0, Lcom/widevine/drm/internal/k;->c:Ljava/lang/String;

    invoke-static {}, Lcom/widevine/drm/internal/HTTPDecrypter;->a()Lcom/widevine/drm/internal/HTTPDecrypter;

    move-result-object v0

    iput-object v0, p0, Lcom/widevine/drm/internal/k;->e:Lcom/widevine/drm/internal/HTTPDecrypter;

    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/widevine/drm/internal/k;->b:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/widevine/drm/internal/l;

    invoke-direct {v0, p0}, Lcom/widevine/drm/internal/l;-><init>(Lcom/widevine/drm/internal/k;)V

    iput-object v0, p0, Lcom/widevine/drm/internal/k;->d:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/widevine/drm/internal/k;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    :catch_0
    move-exception v0

    const-string v0, "Malformed Url (hr:hr)"

    iget-object v1, p0, Lcom/widevine/drm/internal/k;->e:Lcom/widevine/drm/internal/HTTPDecrypter;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToDrmServer:Lcom/widevine/drmapi/android/WVStatus;

    invoke-static {v2}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(Lcom/widevine/drmapi/android/WVStatus;)I

    move-result v2

    iget v3, p0, Lcom/widevine/drm/internal/k;->a:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(IILjava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/widevine/drm/internal/k;)Ljava/net/URL;
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/k;->b:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic b(Lcom/widevine/drm/internal/k;)Lcom/widevine/drm/internal/HTTPDecrypter;
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/k;->e:Lcom/widevine/drm/internal/HTTPDecrypter;

    return-object v0
.end method

.method static synthetic c(Lcom/widevine/drm/internal/k;)I
    .locals 1

    iget v0, p0, Lcom/widevine/drm/internal/k;->a:I

    return v0
.end method

.method static synthetic d(Lcom/widevine/drm/internal/k;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/widevine/drm/internal/k;->c:Ljava/lang/String;

    return-object v0
.end method
