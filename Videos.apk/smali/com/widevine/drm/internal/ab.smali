.class public final Lcom/widevine/drm/internal/ab;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:Z

.field private c:I

.field private d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/widevine/drm/internal/aa;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/widevine/drmapi/android/WVEventListener;


# direct methods
.method public constructor <init>(Lcom/widevine/drmapi/android/WVEventListener;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/widevine/drm/internal/ab;->a:Z

    iput-boolean v0, p0, Lcom/widevine/drm/internal/ab;->b:Z

    iput v0, p0, Lcom/widevine/drm/internal/ab;->c:I

    iput-object v1, p0, Lcom/widevine/drm/internal/ab;->d:Ljava/util/HashMap;

    iput-object v1, p0, Lcom/widevine/drm/internal/ab;->e:Lcom/widevine/drmapi/android/WVEventListener;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/widevine/drm/internal/ab;->d:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/widevine/drm/internal/ab;->e:Lcom/widevine/drmapi/android/WVEventListener;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/widevine/drm/internal/aa;)I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/widevine/drm/internal/ab;->a:Z

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/widevine/drm/internal/m;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/widevine/drm/internal/ac;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/widevine/drm/internal/ab;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, -0x2

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lcom/widevine/drm/internal/ab;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/widevine/drm/internal/ab;->c:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lcom/widevine/drm/internal/ab;->c:I

    :cond_2
    iget-object v0, p0, Lcom/widevine/drm/internal/ab;->d:Ljava/util/HashMap;

    iget v1, p0, Lcom/widevine/drm/internal/ab;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v0, "Overwriting task map entry"

    invoke-static {v0}, Lcom/widevine/drm/internal/p;->b(Ljava/lang/String;)V

    :cond_3
    iget v0, p0, Lcom/widevine/drm/internal/ab;->c:I

    invoke-virtual {p1, v0}, Lcom/widevine/drm/internal/aa;->a(I)V

    iget v0, p0, Lcom/widevine/drm/internal/ab;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/widevine/drm/internal/ab;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/widevine/drm/internal/ab;->d:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "No task map entry to remove"

    invoke-static {v0}, Lcom/widevine/drm/internal/p;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/widevine/drm/internal/ab;->b:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/widevine/drm/internal/ab;->e:Lcom/widevine/drmapi/android/WVEventListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/widevine/drm/internal/ab;->a:Z

    return v0
.end method

.method public final declared-synchronized d()Lcom/widevine/drmapi/android/WVEventListener;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/widevine/drm/internal/ab;->e:Lcom/widevine/drmapi/android/WVEventListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
