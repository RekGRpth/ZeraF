.class public final enum Lcom/widevine/drm/internal/x;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/widevine/drm/internal/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/widevine/drm/internal/x;

.field public static final enum b:Lcom/widevine/drm/internal/x;

.field public static final enum c:Lcom/widevine/drm/internal/x;

.field public static final enum d:Lcom/widevine/drm/internal/x;

.field public static final enum e:Lcom/widevine/drm/internal/x;

.field public static final enum f:Lcom/widevine/drm/internal/x;

.field public static final enum g:Lcom/widevine/drm/internal/x;

.field public static final enum h:Lcom/widevine/drm/internal/x;

.field public static final enum i:Lcom/widevine/drm/internal/x;

.field public static final enum j:Lcom/widevine/drm/internal/x;

.field public static final enum k:Lcom/widevine/drm/internal/x;

.field public static final enum l:Lcom/widevine/drm/internal/x;

.field public static final enum m:Lcom/widevine/drm/internal/x;

.field public static final enum n:Lcom/widevine/drm/internal/x;

.field public static final enum o:Lcom/widevine/drm/internal/x;

.field public static final enum p:Lcom/widevine/drm/internal/x;

.field public static final enum q:Lcom/widevine/drm/internal/x;

.field private static enum r:Lcom/widevine/drm/internal/x;

.field private static final synthetic s:[Lcom/widevine/drm/internal/x;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "Initialize"

    invoke-direct {v0, v1, v3}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->a:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "Terminate"

    invoke-direct {v0, v1, v4}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->b:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "SetCredentials"

    invoke-direct {v0, v1, v5}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->c:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "Play"

    invoke-direct {v0, v1, v6}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->d:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "Stop"

    invoke-direct {v0, v1, v7}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->e:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "Register"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->f:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "RegisterLicense"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->g:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "Unregister"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->h:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "NowOnlineAllFirst"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->i:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "NowOnlineAll"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->j:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "NowOnlineAllEndOfList"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->k:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "RequestLicense"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->l:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "QueryAll"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->m:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "QueryAllEndOfList"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->n:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "Query"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->o:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "SecureStore"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->p:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "SecureRetrieve"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->q:Lcom/widevine/drm/internal/x;

    new-instance v0, Lcom/widevine/drm/internal/x;

    const-string v1, "Unknown"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/widevine/drm/internal/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/widevine/drm/internal/x;->r:Lcom/widevine/drm/internal/x;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/widevine/drm/internal/x;

    sget-object v1, Lcom/widevine/drm/internal/x;->a:Lcom/widevine/drm/internal/x;

    aput-object v1, v0, v3

    sget-object v1, Lcom/widevine/drm/internal/x;->b:Lcom/widevine/drm/internal/x;

    aput-object v1, v0, v4

    sget-object v1, Lcom/widevine/drm/internal/x;->c:Lcom/widevine/drm/internal/x;

    aput-object v1, v0, v5

    sget-object v1, Lcom/widevine/drm/internal/x;->d:Lcom/widevine/drm/internal/x;

    aput-object v1, v0, v6

    sget-object v1, Lcom/widevine/drm/internal/x;->e:Lcom/widevine/drm/internal/x;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/widevine/drm/internal/x;->f:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/widevine/drm/internal/x;->g:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/widevine/drm/internal/x;->h:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/widevine/drm/internal/x;->i:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/widevine/drm/internal/x;->j:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/widevine/drm/internal/x;->k:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/widevine/drm/internal/x;->l:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/widevine/drm/internal/x;->m:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/widevine/drm/internal/x;->n:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/widevine/drm/internal/x;->o:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/widevine/drm/internal/x;->p:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/widevine/drm/internal/x;->q:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/widevine/drm/internal/x;->r:Lcom/widevine/drm/internal/x;

    aput-object v2, v0, v1

    sput-object v0, Lcom/widevine/drm/internal/x;->s:[Lcom/widevine/drm/internal/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/widevine/drm/internal/x;
    .locals 1

    packed-switch p0, :pswitch_data_0

    sget-object v0, Lcom/widevine/drm/internal/x;->r:Lcom/widevine/drm/internal/x;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/widevine/drm/internal/x;->a:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/widevine/drm/internal/x;->b:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/widevine/drm/internal/x;->c:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/widevine/drm/internal/x;->d:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/widevine/drm/internal/x;->e:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/widevine/drm/internal/x;->f:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/widevine/drm/internal/x;->g:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/widevine/drm/internal/x;->h:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/widevine/drm/internal/x;->i:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lcom/widevine/drm/internal/x;->j:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_a
    sget-object v0, Lcom/widevine/drm/internal/x;->k:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_b
    sget-object v0, Lcom/widevine/drm/internal/x;->l:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_c
    sget-object v0, Lcom/widevine/drm/internal/x;->m:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_d
    sget-object v0, Lcom/widevine/drm/internal/x;->n:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_e
    sget-object v0, Lcom/widevine/drm/internal/x;->o:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_f
    sget-object v0, Lcom/widevine/drm/internal/x;->p:Lcom/widevine/drm/internal/x;

    goto :goto_0

    :pswitch_10
    sget-object v0, Lcom/widevine/drm/internal/x;->q:Lcom/widevine/drm/internal/x;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/widevine/drm/internal/x;
    .locals 1

    const-class v0, Lcom/widevine/drm/internal/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/widevine/drm/internal/x;

    return-object v0
.end method

.method public static values()[Lcom/widevine/drm/internal/x;
    .locals 1

    sget-object v0, Lcom/widevine/drm/internal/x;->s:[Lcom/widevine/drm/internal/x;

    invoke-virtual {v0}, [Lcom/widevine/drm/internal/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/widevine/drm/internal/x;

    return-object v0
.end method
