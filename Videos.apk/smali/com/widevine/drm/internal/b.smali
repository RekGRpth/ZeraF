.class public final Lcom/widevine/drm/internal/b;
.super Ljava/lang/Object;


# static fields
.field private static p:Lcom/widevine/drmapi/android/WVEventListener;


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Lcom/widevine/drmapi/android/WVStatus;

.field private h:Lcom/widevine/drmapi/android/WVEvent;

.field private i:Lcom/widevine/drm/internal/x;

.field private j:J

.field private k:J

.field private l:J

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/widevine/drm/internal/x;Lcom/widevine/drmapi/android/WVStatus;)V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v2, p0, Lcom/widevine/drm/internal/b;->a:J

    iput-wide v2, p0, Lcom/widevine/drm/internal/b;->c:J

    iput-wide v2, p0, Lcom/widevine/drm/internal/b;->b:J

    iput-wide v2, p0, Lcom/widevine/drm/internal/b;->l:J

    iput-wide v2, p0, Lcom/widevine/drm/internal/b;->k:J

    iput-wide v2, p0, Lcom/widevine/drm/internal/b;->j:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/widevine/drm/internal/b;->f:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->n:Ljava/lang/String;

    iput-object p2, p0, Lcom/widevine/drm/internal/b;->g:Lcom/widevine/drmapi/android/WVStatus;

    iput-object p1, p0, Lcom/widevine/drm/internal/b;->i:Lcom/widevine/drm/internal/x;

    iput-boolean v1, p0, Lcom/widevine/drm/internal/b;->d:Z

    iput-boolean v1, p0, Lcom/widevine/drm/internal/b;->m:Z

    iput v1, p0, Lcom/widevine/drm/internal/b;->o:I

    sget-object v0, Lcom/widevine/drm/internal/c;->a:[I

    invoke-virtual {p1}, Lcom/widevine/drm/internal/x;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->NullEvent:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne p2, v0, :cond_0

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Initialized:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Terminated:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OutOfRange:Lcom/widevine/drmapi/android/WVStatus;

    if-ne p2, v0, :cond_2

    :cond_1
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Playing:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->PlayFailed:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Stopped:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Registered:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Unregistered:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne p2, v0, :cond_3

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseReceived:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->LicenseRequestFailed:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->QueryStatus:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->SecureStore:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->EndOfList:Lcom/widevine/drmapi/android/WVEvent;

    iput-object v0, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Lcom/widevine/drmapi/android/WVEventListener;)V
    .locals 0

    sput-object p0, Lcom/widevine/drm/internal/b;->p:Lcom/widevine/drmapi/android/WVEventListener;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sget-object v1, Lcom/widevine/drm/internal/c;->b:[I

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    sget-object v1, Lcom/widevine/drm/internal/c;->b:[I

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    :cond_1
    :goto_1
    sget-object v1, Lcom/widevine/drm/internal/c;->b:[I

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    :cond_2
    :goto_2
    sget-object v1, Lcom/widevine/drm/internal/c;->b:[I

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_3

    :cond_3
    :goto_3
    :pswitch_0
    sget-object v1, Lcom/widevine/drm/internal/c;->b:[I

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_4

    :cond_4
    :goto_4
    iget-object v1, p0, Lcom/widevine/drm/internal/b;->n:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/widevine/drm/internal/b;->g:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq v1, v2, :cond_5

    const-string v1, "WVErrorKey"

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    sget-object v1, Lcom/widevine/drm/internal/c;->b:[I

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_5

    :goto_5
    sget-object v1, Lcom/widevine/drm/internal/c;->b:[I

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    invoke-virtual {v2}, Lcom/widevine/drmapi/android/WVEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_6

    :goto_6
    :pswitch_1
    invoke-static {}, Lcom/widevine/drm/internal/s;->a()Lcom/widevine/drm/internal/s;

    move-result-object v1

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->i:Lcom/widevine/drm/internal/x;

    iget-object v3, p0, Lcom/widevine/drm/internal/b;->g:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v1, v2, v3}, Lcom/widevine/drm/internal/s;->a(Lcom/widevine/drm/internal/x;Lcom/widevine/drmapi/android/WVStatus;)V

    sget-object v1, Lcom/widevine/drm/internal/b;->p:Lcom/widevine/drmapi/android/WVEventListener;

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->h:Lcom/widevine/drmapi/android/WVEvent;

    invoke-interface {v1, v2, v0}, Lcom/widevine/drmapi/android/WVEventListener;->onEvent(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)Lcom/widevine/drmapi/android/WVStatus;

    return-void

    :pswitch_2
    iget-boolean v1, p0, Lcom/widevine/drm/internal/b;->d:Z

    if-eqz v1, :cond_0

    const-string v1, "WVLicenseDurationRemainingKey"

    iget-wide v2, p0, Lcom/widevine/drm/internal/b;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVPurchaseDurationRemainingKey"

    iget-wide v2, p0, Lcom/widevine/drm/internal/b;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVPlaybackElapsedTimeKey"

    iget-wide v2, p0, Lcom/widevine/drm/internal/b;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :pswitch_3
    iget-boolean v1, p0, Lcom/widevine/drm/internal/b;->m:Z

    if-eqz v1, :cond_1

    const-string v1, "WVSystemIDKey"

    iget-wide v2, p0, Lcom/widevine/drm/internal/b;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVAssetIDKey"

    iget-wide v2, p0, Lcom/widevine/drm/internal/b;->k:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "WVKeyIDKey"

    iget-wide v2, p0, Lcom/widevine/drm/internal/b;->l:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/widevine/drm/internal/b;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    const-string v1, "WVAssetPathKey"

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/widevine/drm/internal/b;->e:Ljava/lang/String;

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "WVAssetTypeKey"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_6
    const-string v1, "WVAssetTypeKey"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :pswitch_5
    const-string v1, "WVIsEncryptedKey"

    iget-boolean v2, p0, Lcom/widevine/drm/internal/b;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    :pswitch_6
    iget-boolean v1, p0, Lcom/widevine/drm/internal/b;->m:Z

    if-eqz v1, :cond_3

    const-string v1, "WVIsEncryptedKey"

    iget-boolean v2, p0, Lcom/widevine/drm/internal/b;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    :pswitch_7
    iget v1, p0, Lcom/widevine/drm/internal/b;->o:I

    if-lez v1, :cond_4

    iget v1, p0, Lcom/widevine/drm/internal/b;->o:I

    const/4 v2, 0x3

    if-gt v1, v2, :cond_4

    iget-object v1, p0, Lcom/widevine/drm/internal/b;->g:Lcom/widevine/drmapi/android/WVStatus;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-ne v1, v2, :cond_4

    const-string v1, "WVLicenseTypeKey"

    iget v2, p0, Lcom/widevine/drm/internal/b;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    :pswitch_8
    const-string v1, "WVStatusKey"

    iget-object v2, p0, Lcom/widevine/drm/internal/b;->g:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    :pswitch_9
    const-string v1, "WVVersionKey"

    const-string v2, "5.0.0.10051"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0xa
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_9
    .end packed-switch
.end method

.method public final a(I)V
    .locals 0

    iput p1, p0, Lcom/widevine/drm/internal/b;->o:I

    return-void
.end method

.method public final a(JJJ)V
    .locals 1

    iput-wide p1, p0, Lcom/widevine/drm/internal/b;->a:J

    iput-wide p3, p0, Lcom/widevine/drm/internal/b;->b:J

    iput-wide p5, p0, Lcom/widevine/drm/internal/b;->c:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/widevine/drm/internal/b;->d:Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/widevine/drm/internal/b;->e:Ljava/lang/String;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/widevine/drm/internal/b;->f:Z

    return-void
.end method

.method public final b(JJJ)V
    .locals 1

    iput-wide p1, p0, Lcom/widevine/drm/internal/b;->j:J

    iput-wide p3, p0, Lcom/widevine/drm/internal/b;->k:J

    iput-wide p5, p0, Lcom/widevine/drm/internal/b;->l:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/widevine/drm/internal/b;->m:Z

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/widevine/drm/internal/b;->n:Ljava/lang/String;

    return-void
.end method
