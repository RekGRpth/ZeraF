.class public abstract Landroid/support/place/beacon/IBeaconScanner$Stub;
.super Landroid/os/Binder;
.source "IBeaconScanner.java"

# interfaces
.implements Landroid/support/place/beacon/IBeaconScanner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/place/beacon/IBeaconScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/place/beacon/IBeaconScanner$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "android.support.place.beacon.IBeaconScanner"

    invoke-virtual {p0, p0, v0}, Landroid/support/place/beacon/IBeaconScanner$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/support/place/beacon/IBeaconScanner;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "android.support.place.beacon.IBeaconScanner"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/support/place/beacon/IBeaconScanner;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/support/place/beacon/IBeaconScanner;

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/support/place/beacon/IBeaconScanner$Stub$Proxy;

    invoke-direct {v0, p0}, Landroid/support/place/beacon/IBeaconScanner$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    :sswitch_0
    const-string v3, "android.support.place.beacon.IBeaconScanner"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v3, "android.support.place.beacon.IBeaconScanner"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/place/beacon/IBeaconScanner$Stub;->getBeacons()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    :sswitch_2
    const-string v3, "android.support.place.beacon.IBeaconScanner"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/place/beacon/IBeaconScanner$Stub;->scanForBeacons()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_3
    const-string v3, "android.support.place.beacon.IBeaconScanner"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/support/place/beacon/IBeaconScannerListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/support/place/beacon/IBeaconScannerListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/place/beacon/IBeaconScanner$Stub;->addListener(Landroid/support/place/beacon/IBeaconScannerListener;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_4
    const-string v3, "android.support.place.beacon.IBeaconScanner"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/support/place/beacon/IBeaconScannerListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/support/place/beacon/IBeaconScannerListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/place/beacon/IBeaconScanner$Stub;->removeListener(Landroid/support/place/beacon/IBeaconScannerListener;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
