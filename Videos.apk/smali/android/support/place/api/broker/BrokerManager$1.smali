.class Landroid/support/place/api/broker/BrokerManager$1;
.super Ljava/lang/Object;
.source "BrokerManager.java"

# interfaces
.implements Landroid/support/place/beacon/BeaconScanner$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/place/api/broker/BrokerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/place/api/broker/BrokerManager;


# direct methods
.method constructor <init>(Landroid/support/place/api/broker/BrokerManager;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBeaconsChanged(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/place/beacon/BeaconInfo;",
            ">;)V"
        }
    .end annotation

    iget-object v10, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;
    invoke-static {v10}, Landroid/support/place/api/broker/BrokerManager;->access$200(Landroid/support/place/api/broker/BrokerManager;)Landroid/support/place/connector/Broker;

    move-result-object v10

    if-nez v10, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v10, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mBeaconsCache:Ljava/util/List;
    invoke-static {v10}, Landroid/support/place/api/broker/BrokerManager;->access$300(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/List;

    move-result-object v10

    if-nez v10, :cond_2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/beacon/BeaconInfo;

    iget-object v10, v0, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {p0, v10}, Landroid/support/place/api/broker/BrokerManager$1;->onPlaceAdded(Landroid/support/place/connector/PlaceInfo;)V

    goto :goto_1

    :cond_1
    iget-object v10, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # setter for: Landroid/support/place/api/broker/BrokerManager;->mBeaconsCache:Ljava/util/List;
    invoke-static {v10, p1}, Landroid/support/place/api/broker/BrokerManager;->access$302(Landroid/support/place/api/broker/BrokerManager;Ljava/util/List;)Ljava/util/List;

    goto :goto_0

    :cond_2
    new-instance v8, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    invoke-direct {v8, v10}, Ljava/util/HashSet;-><init>(I)V

    new-instance v9, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    invoke-direct {v9, v10}, Ljava/util/HashMap;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/beacon/BeaconInfo;

    iget-object v10, v0, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v10}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v9, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    new-instance v2, Ljava/util/HashSet;

    iget-object v10, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mBeaconsCache:Ljava/util/List;
    invoke-static {v10}, Landroid/support/place/api/broker/BrokerManager;->access$300(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-direct {v2, v10}, Ljava/util/HashSet;-><init>(I)V

    new-instance v3, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    invoke-direct {v3, v10}, Ljava/util/HashMap;-><init>(I)V

    iget-object v10, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mBeaconsCache:Ljava/util/List;
    invoke-static {v10}, Landroid/support/place/api/broker/BrokerManager;->access$300(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/beacon/BeaconInfo;

    iget-object v10, v0, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v10}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v3, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v8}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1, v2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    invoke-interface {v2, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v8, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/beacon/BeaconInfo;

    iget-object v10, v0, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {p0, v10}, Landroid/support/place/api/broker/BrokerManager$1;->onPlaceRemoved(Landroid/support/place/connector/PlaceInfo;)V

    goto :goto_4

    :cond_5
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v9, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/beacon/BeaconInfo;

    iget-object v10, v0, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {p0, v10}, Landroid/support/place/api/broker/BrokerManager$1;->onPlaceAdded(Landroid/support/place/connector/PlaceInfo;)V

    goto :goto_5

    :cond_6
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/support/place/beacon/BeaconInfo;

    invoke-interface {v9, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/support/place/beacon/BeaconInfo;

    iget-object v10, v6, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v10}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v7, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v11}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    iget-object v10, v6, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v10}, Landroid/support/place/connector/PlaceInfo;->getMaster()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v10

    iget-object v11, v7, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v11}, Landroid/support/place/connector/PlaceInfo;->getMaster()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/support/place/rpc/EndpointInfo;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    iget-object v10, v6, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v10}, Landroid/support/place/connector/PlaceInfo;->getPlaceName()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v7, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v11}, Landroid/support/place/connector/PlaceInfo;->getPlaceName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    iget-object v10, v6, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    iget-object v11, v7, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v10, v11}, Landroid/support/place/connector/PlaceInfo;->hasSameExtras(Landroid/support/place/connector/PlaceInfo;)Z

    move-result v10

    if-nez v10, :cond_7

    :cond_8
    iget-object v10, v7, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {p0, v10}, Landroid/support/place/api/broker/BrokerManager$1;->onPlaceUpdated(Landroid/support/place/connector/PlaceInfo;)V

    goto :goto_6

    :cond_9
    iget-object v10, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # setter for: Landroid/support/place/api/broker/BrokerManager;->mBeaconsCache:Ljava/util/List;
    invoke-static {v10, p1}, Landroid/support/place/api/broker/BrokerManager;->access$302(Landroid/support/place/api/broker/BrokerManager;Ljava/util/List;)Ljava/util/List;

    goto/16 :goto_0
.end method

.method public onPlaceAdded(Landroid/support/place/connector/PlaceInfo;)V
    .locals 7
    .param p1    # Landroid/support/place/connector/PlaceInfo;

    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$000(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$000(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v5

    monitor-enter v5

    :try_start_1
    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v2, p1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onPlaceAdded(Landroid/support/place/connector/PlaceInfo;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4

    :cond_0
    return-void
.end method

.method public onPlaceRemoved(Landroid/support/place/connector/PlaceInfo;)V
    .locals 7
    .param p1    # Landroid/support/place/connector/PlaceInfo;

    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$000(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$000(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v5

    monitor-enter v5

    :try_start_1
    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v2, p1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onPlaceRemoved(Landroid/support/place/connector/PlaceInfo;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4

    :cond_0
    return-void
.end method

.method public onPlaceUpdated(Landroid/support/place/connector/PlaceInfo;)V
    .locals 7
    .param p1    # Landroid/support/place/connector/PlaceInfo;

    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$000(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$000(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v5

    monitor-enter v5

    :try_start_1
    iget-object v4, p0, Landroid/support/place/api/broker/BrokerManager$1;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v4}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v2, p1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onPlaceUpdated(Landroid/support/place/connector/PlaceInfo;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4

    :cond_0
    return-void
.end method
