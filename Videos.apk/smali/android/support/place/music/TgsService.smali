.class public Landroid/support/place/music/TgsService;
.super Ljava/lang/Object;
.source "TgsService.java"


# instance fields
.field private final mConnectorInfo:Landroid/support/place/connector/ConnectorInfo;

.field private final mDeviceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 3
    .param p1    # Landroid/support/place/rpc/RpcData;

    const-string v0, "deviceName"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/support/place/connector/ConnectorInfo;

    const-string v2, "connector"

    invoke-virtual {p1, v2}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/place/connector/ConnectorInfo;-><init>(Landroid/support/place/rpc/RpcData;)V

    invoke-direct {p0, v0, v1}, Landroid/support/place/music/TgsService;-><init>(Ljava/lang/String;Landroid/support/place/connector/ConnectorInfo;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/support/place/connector/ConnectorInfo;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/support/place/connector/ConnectorInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/music/TgsService;->mDeviceName:Ljava/lang/String;

    iput-object p2, p0, Landroid/support/place/music/TgsService;->mConnectorInfo:Landroid/support/place/connector/ConnectorInfo;

    return-void
.end method


# virtual methods
.method public getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TgsService;->mConnectorInfo:Landroid/support/place/connector/ConnectorInfo;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TgsService;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Landroid/support/place/music/TgsService;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TgsService("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 3
    .param p1    # Landroid/support/place/rpc/RpcData;

    const-string v1, "deviceName"

    iget-object v2, p0, Landroid/support/place/music/TgsService;->mDeviceName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v1, p0, Landroid/support/place/music/TgsService;->mConnectorInfo:Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v1, v0}, Landroid/support/place/connector/ConnectorInfo;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    const-string v1, "connector"

    invoke-virtual {p1, v1, v0}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    return-void
.end method
