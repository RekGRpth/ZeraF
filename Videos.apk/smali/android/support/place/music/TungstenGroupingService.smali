.class public Landroid/support/place/music/TungstenGroupingService;
.super Ljava/lang/Object;
.source "TungstenGroupingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/place/music/TungstenGroupingService$Presenter;,
        Landroid/support/place/music/TungstenGroupingService$Listener;,
        Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;,
        Landroid/support/place/music/TungstenGroupingService$OnGetRxVolumes;,
        Landroid/support/place/music/TungstenGroupingService$OnRemoveRxFromGroup;,
        Landroid/support/place/music/TungstenGroupingService$OnAssignRxToGroup;,
        Landroid/support/place/music/TungstenGroupingService$OnCreateGroup;,
        Landroid/support/place/music/TungstenGroupingService$OnGetGroupState;,
        Landroid/support/place/music/TungstenGroupingService$OnVersionCheck;
    }
.end annotation


# instance fields
.field private _broker:Landroid/support/place/connector/Broker;

.field private _endpoint:Landroid/support/place/rpc/EndpointInfo;

.field private _presenter:Landroid/support/place/music/TungstenGroupingService$Presenter;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 0
    .param p1    # Landroid/support/place/connector/Broker;
    .param p2    # Landroid/support/place/rpc/EndpointInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/music/TungstenGroupingService;->_broker:Landroid/support/place/connector/Broker;

    iput-object p2, p0, Landroid/support/place/music/TungstenGroupingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-void
.end method


# virtual methods
.method public adjustGroupVolume(Ljava/lang/String;ILandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "groupId"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "steps"

    invoke-virtual {v6, v0, p2}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenGroupingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "adjustGroupVolume"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public assignRxToGroup(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/music/TungstenGroupingService$OnAssignRxToGroup;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/support/place/music/TungstenGroupingService$OnAssignRxToGroup;
    .param p4    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "groupId"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "rxId"

    invoke-virtual {v6, v0, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenGroupingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "assignRxToGroup"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;

    const/4 v5, 0x3

    invoke-direct {v4, p0, v5, p3}, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenGroupingService;ILjava/lang/Object;)V

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public createGroup(Ljava/lang/String;Ljava/util/List;Landroid/support/place/music/TungstenGroupingService$OnCreateGroup;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p3    # Landroid/support/place/music/TungstenGroupingService$OnCreateGroup;
    .param p4    # Landroid/support/place/rpc/RpcErrorHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/support/place/music/TungstenGroupingService$OnCreateGroup;",
            "Landroid/support/place/rpc/RpcErrorHandler;",
            ")V"
        }
    .end annotation

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "groupId"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "rxIds"

    invoke-virtual {v6, v0, p2}, Landroid/support/place/rpc/RpcData;->putList(Ljava/lang/String;Ljava/util/List;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenGroupingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "createGroup"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;

    const/4 v5, 0x2

    invoke-direct {v4, p0, v5, p3}, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenGroupingService;ILjava/lang/Object;)V

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getGroupState(Landroid/support/place/music/TungstenGroupingService$OnGetGroupState;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Landroid/support/place/music/TungstenGroupingService$OnGetGroupState;
    .param p2    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenGroupingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getGroupState"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;

    const/4 v5, 0x1

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenGroupingService;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getRxVolumes(Landroid/support/place/music/TungstenGroupingService$OnGetRxVolumes;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Landroid/support/place/music/TungstenGroupingService$OnGetRxVolumes;
    .param p2    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenGroupingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getRxVolumes"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;

    const/4 v5, 0x6

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenGroupingService;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public removeRxFromGroup(Ljava/lang/String;Landroid/support/place/music/TungstenGroupingService$OnRemoveRxFromGroup;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/support/place/music/TungstenGroupingService$OnRemoveRxFromGroup;
    .param p3    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "rxId"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenGroupingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "removeRxFromGroup"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;

    const/4 v5, 0x4

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenGroupingService;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public startListening(Landroid/support/place/music/TungstenGroupingService$Listener;)V
    .locals 2
    .param p1    # Landroid/support/place/music/TungstenGroupingService$Listener;

    invoke-virtual {p0}, Landroid/support/place/music/TungstenGroupingService;->stopListening()V

    new-instance v0, Landroid/support/place/music/TungstenGroupingService$Presenter;

    iget-object v1, p0, Landroid/support/place/music/TungstenGroupingService;->_broker:Landroid/support/place/connector/Broker;

    invoke-direct {v0, p0, v1, p1}, Landroid/support/place/music/TungstenGroupingService$Presenter;-><init>(Landroid/support/place/music/TungstenGroupingService;Landroid/support/place/connector/Broker;Landroid/support/place/music/TungstenGroupingService$Listener;)V

    iput-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_presenter:Landroid/support/place/music/TungstenGroupingService$Presenter;

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_presenter:Landroid/support/place/music/TungstenGroupingService$Presenter;

    iget-object v1, p0, Landroid/support/place/music/TungstenGroupingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v1}, Landroid/support/place/music/TungstenGroupingService$Presenter;->startListening(Landroid/support/place/rpc/EndpointInfo;)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_presenter:Landroid/support/place/music/TungstenGroupingService$Presenter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_presenter:Landroid/support/place/music/TungstenGroupingService$Presenter;

    invoke-virtual {v0}, Landroid/support/place/music/TungstenGroupingService$Presenter;->stopListening()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_presenter:Landroid/support/place/music/TungstenGroupingService$Presenter;

    :cond_0
    return-void
.end method

.method public versionCheck(IILandroid/support/place/music/TungstenGroupingService$OnVersionCheck;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/support/place/music/TungstenGroupingService$OnVersionCheck;
    .param p4    # Landroid/support/place/rpc/RpcErrorHandler;

    new-instance v6, Landroid/support/place/rpc/RpcData;

    invoke-direct {v6}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "clientMinApiVersion"

    invoke-virtual {v6, v0, p1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "clientMaxApiVersion"

    invoke-virtual {v6, v0, p2}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Landroid/support/place/music/TungstenGroupingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenGroupingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "versionCheck"

    invoke-virtual {v6}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5, p3}, Landroid/support/place/music/TungstenGroupingService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenGroupingService;ILjava/lang/Object;)V

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method
