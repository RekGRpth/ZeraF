.class Landroid/support/place/connector/ConnectorRegistryRpc$Presenter;
.super Landroid/support/place/connector/EventListener;
.source "ConnectorRegistryRpc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/place/connector/ConnectorRegistryRpc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Presenter"
.end annotation


# instance fields
.field private _listener:Landroid/support/place/connector/ConnectorRegistryRpc$Listener;

.field final synthetic this$0:Landroid/support/place/connector/ConnectorRegistryRpc;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/ConnectorRegistryRpc;Landroid/support/place/connector/Broker;Landroid/support/place/connector/ConnectorRegistryRpc$Listener;)V
    .locals 0
    .param p2    # Landroid/support/place/connector/Broker;
    .param p3    # Landroid/support/place/connector/ConnectorRegistryRpc$Listener;

    iput-object p1, p0, Landroid/support/place/connector/ConnectorRegistryRpc$Presenter;->this$0:Landroid/support/place/connector/ConnectorRegistryRpc;

    invoke-direct {p0, p2, p3}, Landroid/support/place/connector/EventListener;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/connector/EventListener$Listener;)V

    iput-object p3, p0, Landroid/support/place/connector/ConnectorRegistryRpc$Presenter;->_listener:Landroid/support/place/connector/ConnectorRegistryRpc$Listener;

    return-void
.end method


# virtual methods
.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .param p3    # Landroid/support/place/rpc/RpcContext;
    .param p4    # Landroid/support/place/rpc/RpcError;

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const/4 v2, 0x0

    const-string v3, "onConnectorAdded"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "connector"

    sget-object v4, Landroid/support/place/connector/ConnectorInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v1, v3, v4}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    iget-object v3, p0, Landroid/support/place/connector/ConnectorRegistryRpc$Presenter;->_listener:Landroid/support/place/connector/ConnectorRegistryRpc$Listener;

    invoke-virtual {v3, v0, p3}, Landroid/support/place/connector/ConnectorRegistryRpc$Listener;->onConnectorAdded(Landroid/support/place/connector/ConnectorInfo;Landroid/support/place/rpc/RpcContext;)V

    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    const-string v3, "onConnectorRemoved"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "connector"

    sget-object v4, Landroid/support/place/connector/ConnectorInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v1, v3, v4}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    iget-object v3, p0, Landroid/support/place/connector/ConnectorRegistryRpc$Presenter;->_listener:Landroid/support/place/connector/ConnectorRegistryRpc$Listener;

    invoke-virtual {v3, v0, p3}, Landroid/support/place/connector/ConnectorRegistryRpc$Listener;->onConnectorRemoved(Landroid/support/place/connector/ConnectorInfo;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/EventListener;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v3

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
