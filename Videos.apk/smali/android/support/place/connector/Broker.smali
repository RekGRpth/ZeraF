.class public Landroid/support/place/connector/Broker;
.super Ljava/lang/Object;
.source "Broker.java"


# instance fields
.field mBrokerConnector:Landroid/support/place/rpc/EndpointInfo;

.field mConnection:Landroid/support/place/connector/IBrokerConnection;

.field private mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

.field final mContainer:Landroid/support/place/connector/ConnectorContainer;

.field private mCoordinator:Landroid/support/place/connector/coordinator/Coordinator;

.field private final mHandler:Landroid/os/Handler;

.field mPlace:Landroid/support/place/connector/PlaceInfo;

.field private mSecurityService:Landroid/support/place/connector/security/SecurityService;

.field mService:Landroid/support/place/connector/IBrokerService;


# direct methods
.method constructor <init>(Landroid/support/place/connector/IBrokerConnection;Landroid/support/place/connector/ConnectorContainer;Landroid/support/place/connector/IBrokerService;Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/rpc/EndpointInfo;Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/support/place/connector/IBrokerConnection;
    .param p2    # Landroid/support/place/connector/ConnectorContainer;
    .param p3    # Landroid/support/place/connector/IBrokerService;
    .param p4    # Landroid/support/place/rpc/EndpointInfo;
    .param p5    # Landroid/support/place/rpc/EndpointInfo;
    .param p6    # Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    iput-object p2, p0, Landroid/support/place/connector/Broker;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    iput-object p3, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    iput-object p4, p0, Landroid/support/place/connector/Broker;->mBrokerConnector:Landroid/support/place/rpc/EndpointInfo;

    iput-object p6, p0, Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Landroid/support/place/connector/Broker;)Landroid/os/Handler;
    .locals 1
    .param p0    # Landroid/support/place/connector/Broker;

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public checkCallingPermission(Landroid/support/place/rpc/RpcContext;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1    # Landroid/support/place/rpc/RpcContext;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v2, :cond_1

    const-string v2, "aah.Broker"

    const-string v3, "checkCallingPermission: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v2, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/rpc/RpcContext;->getCertificate()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, p2, v4, p3}, Landroid/support/place/connector/IBrokerService;->hasPermission(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public enforceCallingPermission(Landroid/support/place/rpc/RpcContext;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7
    .param p1    # Landroid/support/place/rpc/RpcContext;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    if-nez p3, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object v0, p3

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    invoke-virtual {p0, p1, p2, v3}, Landroid/support/place/connector/Broker;->checkCallingPermission(Landroid/support/place/rpc/RpcContext;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/lang/SecurityException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Incoming request not authorized for serviceType:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and permissions: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public getConnectorRegistry()Landroid/support/place/connector/ConnectorRegistry;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getPlace()Landroid/support/place/connector/PlaceInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    return-object v0
.end method

.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newConnectorInfo(Landroid/support/place/connector/Endpoint;)Landroid/support/place/connector/ConnectorInfo;
    .locals 3
    .param p1    # Landroid/support/place/connector/Endpoint;

    new-instance v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {p0}, Landroid/support/place/connector/Broker;->newEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/place/connector/ConnectorInfo;-><init>(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;)V

    return-object v0
.end method

.method public newEndpointInfo()Landroid/support/place/rpc/EndpointInfo;
    .locals 4

    new-instance v0, Landroid/support/place/rpc/EndpointInfo;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/Broker;->mBrokerConnector:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v2}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Landroid/support/place/connector/Broker;->mBrokerConnector:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v3}, Landroid/support/place/rpc/EndpointInfo;->getPort()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Landroid/support/place/rpc/EndpointInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public registerConnector(Landroid/support/place/connector/Endpoint;Landroid/support/place/connector/ConnectorInfo;)V
    .locals 3
    .param p1    # Landroid/support/place/connector/Endpoint;
    .param p2    # Landroid/support/place/connector/ConnectorInfo;

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "registerConnector only works for Brokers created in Containers"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v1, :cond_1

    const-string v1, "aah.Broker"

    const-string v2, "registerConnector: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/support/place/connector/BrokerDeadException;

    const-string v2, "Broker Disconnected"

    invoke-direct {v1, v2}, Landroid/support/place/connector/BrokerDeadException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->onStart()V

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->getIEndpoint()Landroid/support/place/rpc/IEndpointStub;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Landroid/support/place/connector/IBrokerService;->registerConnector(Landroid/support/place/rpc/IEndpointStub;Landroid/support/place/connector/ConnectorInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/support/place/connector/BrokerDeadException;

    invoke-direct {v1, v0}, Landroid/support/place/connector/BrokerDeadException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public registerEndpoint(Landroid/support/place/connector/Endpoint;Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;
    .locals 3
    .param p1    # Landroid/support/place/connector/Endpoint;
    .param p2    # Landroid/support/place/rpc/EndpointInfo;

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v1, :cond_0

    const-string v1, "aah.Broker"

    const-string v2, "registerEndpoint: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/support/place/connector/BrokerDeadException;

    const-string v2, "Broker Disconnected"

    invoke-direct {v1, v2}, Landroid/support/place/connector/BrokerDeadException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->onStart()V

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->getIEndpoint()Landroid/support/place/rpc/IEndpointStub;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Landroid/support/place/connector/IBrokerService;->registerEndpoint(Landroid/support/place/rpc/IEndpointStub;Landroid/support/place/rpc/EndpointInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p2

    :catch_0
    move-exception v0

    new-instance v1, Landroid/support/place/connector/BrokerDeadException;

    invoke-direct {v1, v0}, Landroid/support/place/connector/BrokerDeadException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7
    .param p1    # Landroid/support/place/rpc/EndpointInfo;
    .param p2    # Ljava/lang/String;
    .param p3    # [B
    .param p4    # Landroid/support/place/rpc/RpcResultHandler;
    .param p5    # Landroid/support/place/rpc/RpcErrorHandler;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;I)V

    return-void
.end method

.method public sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;I)V
    .locals 9
    .param p1    # Landroid/support/place/rpc/EndpointInfo;
    .param p2    # Ljava/lang/String;
    .param p3    # [B
    .param p4    # Landroid/support/place/rpc/RpcResultHandler;
    .param p5    # Landroid/support/place/rpc/RpcErrorHandler;
    .param p6    # I

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_1

    const-string v0, "aah.Broker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendRpc "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Trying to use a broker after calling "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "disconnect on the BrokerConnection that created it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Landroid/support/place/rpc/RpcError;

    invoke-direct {v8}, Landroid/support/place/rpc/RpcError;-><init>()V

    const/4 v0, 0x1

    iput v0, v8, Landroid/support/place/rpc/RpcError;->status:I

    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v8, v0}, Landroid/support/place/rpc/RpcError;->appendStackTrace(Ljava/lang/Throwable;)V

    invoke-interface {p5, v8}, Landroid/support/place/rpc/RpcErrorHandler;->onError(Landroid/support/place/rpc/RpcError;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x0

    if-nez p4, :cond_2

    if-eqz p5, :cond_3

    :cond_2
    new-instance v4, Landroid/support/place/connector/Broker$1;

    invoke-direct {v4, p0, p4, p5}, Landroid/support/place/connector/Broker$1;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_3
    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    iget-object v5, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    move-object v1, p2

    move-object v2, p1

    move-object v3, p3

    move v6, p6

    invoke-interface/range {v0 .. v6}, Landroid/support/place/connector/IBrokerService;->sendRequest(Ljava/lang/String;Landroid/support/place/rpc/EndpointInfo;[BLandroid/support/place/rpc/IRpcCallback;Landroid/support/place/connector/IBrokerConnection;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v7

    if-eqz p5, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;

    new-instance v1, Landroid/support/place/connector/Broker$2;

    invoke-direct {v1, p0, p5, v7}, Landroid/support/place/connector/Broker$2;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/RpcErrorHandler;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method setPlace(Landroid/support/place/connector/PlaceInfo;)V
    .locals 6
    .param p1    # Landroid/support/place/connector/PlaceInfo;

    const/4 v4, 0x0

    if-nez p1, :cond_2

    iput-object v4, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    iget-object v3, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    invoke-virtual {v3}, Landroid/support/place/connector/ConnectorRegistry;->stopRegistryListener()V

    :cond_0
    iput-object v4, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iput-object v4, p0, Landroid/support/place/connector/Broker;->mSecurityService:Landroid/support/place/connector/security/SecurityService;

    iput-object v4, p0, Landroid/support/place/connector/Broker;->mCoordinator:Landroid/support/place/connector/coordinator/Coordinator;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v3, p1}, Landroid/support/place/connector/PlaceInfo;->hasSameMaster(Landroid/support/place/connector/PlaceInfo;)Z

    move-result v3

    if-nez v3, :cond_5

    :cond_3
    const/4 v1, 0x1

    :goto_1
    iput-object p1, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getMaster()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getMaster()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/place/rpc/EndpointInfo;->getPort()I

    move-result v2

    iget-object v3, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v3, :cond_4

    iget-object v3, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    invoke-virtual {v3}, Landroid/support/place/connector/ConnectorRegistry;->stopRegistryListener()V

    :cond_4
    new-instance v3, Landroid/support/place/connector/ConnectorRegistry;

    invoke-direct {v3, p0}, Landroid/support/place/connector/ConnectorRegistry;-><init>(Landroid/support/place/connector/Broker;)V

    iput-object v3, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    new-instance v3, Landroid/support/place/connector/security/SecurityService;

    new-instance v4, Landroid/support/place/rpc/EndpointInfo;

    const-string v5, "_authService"

    invoke-direct {v4, v5, v0, v2}, Landroid/support/place/rpc/EndpointInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v3, p0, v4}, Landroid/support/place/connector/security/SecurityService;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V

    iput-object v3, p0, Landroid/support/place/connector/Broker;->mSecurityService:Landroid/support/place/connector/security/SecurityService;

    new-instance v3, Landroid/support/place/connector/coordinator/Coordinator;

    new-instance v4, Landroid/support/place/rpc/EndpointInfo;

    const-string v5, "_coordinator"

    invoke-direct {v4, v5, v0, v2}, Landroid/support/place/rpc/EndpointInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v3, p0, v4}, Landroid/support/place/connector/coordinator/Coordinator;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V

    iput-object v3, p0, Landroid/support/place/connector/Broker;->mCoordinator:Landroid/support/place/connector/coordinator/Coordinator;

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public unregisterConnector(Landroid/support/place/connector/Endpoint;)V
    .locals 3
    .param p1    # Landroid/support/place/connector/Endpoint;

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "unregisterConnector only works for Brokers created in Containers"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v1, :cond_1

    const-string v1, "aah.Broker"

    const-string v2, "unregisterConnector: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->onStop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    iget-object v1, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->getIEndpoint()Landroid/support/place/rpc/IEndpointStub;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/support/place/connector/IBrokerService;->unregisterConnector(Landroid/support/place/rpc/IEndpointStub;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "unregisterConnector: error communicating with BrokerService"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "Error invoking onStop (the object will be unregistered from the broker"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public unregisterEndpoint(Landroid/support/place/connector/Endpoint;)V
    .locals 3
    .param p1    # Landroid/support/place/connector/Endpoint;

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v1, :cond_0

    const-string v1, "aah.Broker"

    const-string v2, "unregisterEndpoint: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->onStop()V

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->getIEndpoint()Landroid/support/place/rpc/IEndpointStub;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/support/place/connector/IBrokerService;->unregisterEndpoint(Landroid/support/place/rpc/IEndpointStub;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "unregisterEndpoint: error communicating with BrokerService"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
