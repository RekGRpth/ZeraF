.class Landroid/support/place/connector/BrokerConnection$1;
.super Ljava/lang/Object;
.source "BrokerConnection.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/place/connector/BrokerConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/place/connector/BrokerConnection;


# direct methods
.method constructor <init>(Landroid/support/place/connector/BrokerConnection;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v3, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    invoke-virtual {v3, p2}, Landroid/support/place/connector/BrokerConnection;->getBrokerService(Landroid/os/IBinder;)Landroid/support/place/connector/IBrokerService;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v3, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iget-object v3, v3, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v3, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    new-instance v4, Landroid/support/place/connector/BrokerConnection$Callback;

    iget-object v5, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    invoke-direct {v4, v5}, Landroid/support/place/connector/BrokerConnection$Callback;-><init>(Landroid/support/place/connector/BrokerConnection;)V

    iput-object v4, v3, Landroid/support/place/connector/BrokerConnection;->mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BrokerConnection(pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " context="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iget-object v4, v4, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iget-object v3, v3, Landroid/support/place/connector/BrokerConnection;->mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

    invoke-interface {v0, v3, v1}, Landroid/support/place/connector/IBrokerService;->registerCallback(Landroid/support/place/connector/IBrokerConnection;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iput-object v0, v3, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    goto :goto_0

    :catch_0
    move-exception v2

    iget-object v3, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iget-object v3, v3, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    # invokes: Landroid/support/place/connector/BrokerConnection;->handleBrokerServiceDisconnected()V
    invoke-static {v0}, Landroid/support/place/connector/BrokerConnection;->access$100(Landroid/support/place/connector/BrokerConnection;)V

    return-void
.end method
