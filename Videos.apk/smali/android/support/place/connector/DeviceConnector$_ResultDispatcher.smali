.class final Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;
.super Ljava/lang/Object;
.source "DeviceConnector.java"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/place/connector/DeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "_ResultDispatcher"
.end annotation


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Landroid/support/place/connector/DeviceConnector;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V
    .locals 0
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iput-object p1, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->this$0:Landroid/support/place/connector/DeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public factoryReset([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnFactoryReset;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnFactoryReset;->onFactoryReset(Z)V

    :cond_0
    return-void
.end method

.method public getAdbState([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetAdbState;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetAdbState;->onGetAdbState(Z)V

    :cond_0
    return-void
.end method

.method public getAvailableUpdate([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetAvailableUpdate;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetAvailableUpdate;->onGetAvailableUpdate(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getBluetoothMac([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetBluetoothMac;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetBluetoothMac;->onGetBluetoothMac(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getBuildVersion([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetBuildVersion;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetBuildVersion;->onGetBuildVersion(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getDebugInfo([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetDebugInfo;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetDebugInfo;->onGetDebugInfo(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getDeviceName([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetDeviceName;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetDeviceName;->onGetDeviceName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getDeviceSerialNumber([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetDeviceSerialNumber;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetDeviceSerialNumber;->onGetDeviceSerialNumber(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getDeviceState([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetDeviceState;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetDeviceState;->onGetDeviceState(Landroid/support/place/rpc/RpcData;)V

    :cond_0
    return-void
.end method

.method public getDeviceVersion([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetDeviceVersion;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetDeviceVersion;->onGetDeviceVersion(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getManufacturerName([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetManufacturerName;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetManufacturerName;->onGetManufacturerName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getMaster([B)V
    .locals 4
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    sget-object v3, Landroid/support/place/rpc/EndpointInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v2, v3}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v1

    check-cast v1, Landroid/support/place/rpc/EndpointInfo;

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetMaster;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetMaster;->onGetMaster(Landroid/support/place/rpc/EndpointInfo;)V

    :cond_0
    return-void
.end method

.method public getModelName([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetModelName;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetModelName;->onGetModelName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getUpdateWindow([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnGetUpdateWindow;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnGetUpdateWindow;->onGetUpdateWindow(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public helloFromHub([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnHelloFromHub;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnHelloFromHub;->onHelloFromHub(Landroid/support/place/rpc/RpcData;)V

    :cond_0
    return-void
.end method

.method public onResult([B)V
    .locals 1
    .param p1    # [B

    iget v0, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->helloFromHub([B)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getMaster([B)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getBluetoothMac([B)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->setDeviceName([B)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getDeviceName([B)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getDeviceSerialNumber([B)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getDeviceVersion([B)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getBuildVersion([B)V

    goto :goto_0

    :pswitch_9
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getAvailableUpdate([B)V

    goto :goto_0

    :pswitch_a
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getDebugInfo([B)V

    goto :goto_0

    :pswitch_b
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->setAdbState([B)V

    goto :goto_0

    :pswitch_c
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getAdbState([B)V

    goto :goto_0

    :pswitch_d
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->setUpdateWindow([B)V

    goto :goto_0

    :pswitch_e
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getUpdateWindow([B)V

    goto :goto_0

    :pswitch_f
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getDeviceState([B)V

    goto :goto_0

    :pswitch_10
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getManufacturerName([B)V

    goto :goto_0

    :pswitch_11
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->getModelName([B)V

    goto :goto_0

    :pswitch_12
    invoke-virtual {p0, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->factoryReset([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_12
    .end packed-switch
.end method

.method public setAdbState([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnSetAdbState;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnSetAdbState;->onSetAdbState(Z)V

    :cond_0
    return-void
.end method

.method public setDeviceName([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnSetDeviceName;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnSetDeviceName;->onSetDeviceName(Z)V

    :cond_0
    return-void
.end method

.method public setUpdateWindow([B)V
    .locals 3
    .param p1    # [B

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "_result"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v2, Landroid/support/place/connector/DeviceConnector$OnSetUpdateWindow;

    invoke-interface {v2, v1}, Landroid/support/place/connector/DeviceConnector$OnSetUpdateWindow;->onSetUpdateWindow(Z)V

    :cond_0
    return-void
.end method
