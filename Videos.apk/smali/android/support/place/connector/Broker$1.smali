.class Landroid/support/place/connector/Broker$1;
.super Landroid/support/place/rpc/IRpcCallback$Stub;
.source "Broker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/place/connector/Broker;

.field final synthetic val$error:Landroid/support/place/rpc/RpcErrorHandler;

.field final synthetic val$result:Landroid/support/place/rpc/RpcResultHandler;


# direct methods
.method constructor <init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/Broker$1;->this$0:Landroid/support/place/connector/Broker;

    iput-object p2, p0, Landroid/support/place/connector/Broker$1;->val$result:Landroid/support/place/rpc/RpcResultHandler;

    iput-object p3, p0, Landroid/support/place/connector/Broker$1;->val$error:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-direct {p0}, Landroid/support/place/rpc/IRpcCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/support/place/rpc/RpcError;)V
    .locals 2
    .param p1    # Landroid/support/place/rpc/RpcError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Landroid/support/place/connector/Broker$1;->val$error:Landroid/support/place/rpc/RpcErrorHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/Broker$1;->this$0:Landroid/support/place/connector/Broker;

    # getter for: Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Landroid/support/place/connector/Broker;->access$100(Landroid/support/place/connector/Broker;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Landroid/support/place/connector/Broker$1$2;

    invoke-direct {v1, p0, p1}, Landroid/support/place/connector/Broker$1$2;-><init>(Landroid/support/place/connector/Broker$1;Landroid/support/place/rpc/RpcError;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onResponse([B)V
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Landroid/support/place/connector/Broker$1;->val$result:Landroid/support/place/rpc/RpcResultHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/Broker$1;->this$0:Landroid/support/place/connector/Broker;

    # getter for: Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Landroid/support/place/connector/Broker;->access$100(Landroid/support/place/connector/Broker;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Landroid/support/place/connector/Broker$1$1;

    invoke-direct {v1, p0, p1}, Landroid/support/place/connector/Broker$1$1;-><init>(Landroid/support/place/connector/Broker$1;[B)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
