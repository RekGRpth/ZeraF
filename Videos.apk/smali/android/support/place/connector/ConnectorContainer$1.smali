.class Landroid/support/place/connector/ConnectorContainer$1;
.super Ljava/lang/Object;
.source "ConnectorContainer.java"

# interfaces
.implements Landroid/support/place/connector/BrokerConnection$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/place/connector/ConnectorContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/place/connector/ConnectorContainer;


# direct methods
.method constructor <init>(Landroid/support/place/connector/ConnectorContainer;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBrokerConnected(Landroid/support/place/connector/Broker;)V
    .locals 2
    .param p1    # Landroid/support/place/connector/Broker;

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const/4 v1, 0x0

    # setter for: Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z
    invoke-static {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->access$002(Landroid/support/place/connector/ConnectorContainer;Z)Z

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    invoke-virtual {v0, p1}, Landroid/support/place/connector/ConnectorContainer;->onBrokerConnected(Landroid/support/place/connector/Broker;)V

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const-string v1, "onBrokerConnected"

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->enforceBaseCalled(Ljava/lang/String;)V

    return-void
.end method

.method public onBrokerDisconnected()V
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const/4 v1, 0x0

    # setter for: Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z
    invoke-static {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->access$002(Landroid/support/place/connector/ConnectorContainer;Z)Z

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorContainer;->onBrokerDisconnected()V

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const-string v1, "onBrokerDisconnected"

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->enforceBaseCalled(Ljava/lang/String;)V

    return-void
.end method

.method public onMasterChanged(Landroid/support/place/rpc/EndpointInfo;)V
    .locals 2
    .param p1    # Landroid/support/place/rpc/EndpointInfo;

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const/4 v1, 0x0

    # setter for: Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z
    invoke-static {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->access$002(Landroid/support/place/connector/ConnectorContainer;Z)Z

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    invoke-virtual {v0, p1}, Landroid/support/place/connector/ConnectorContainer;->onMasterChanged(Landroid/support/place/rpc/EndpointInfo;)V

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const-string v1, "onMasterChanged"

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->enforceBaseCalled(Ljava/lang/String;)V

    return-void
.end method

.method public onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V
    .locals 2
    .param p1    # Landroid/support/place/connector/PlaceInfo;

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const/4 v1, 0x0

    # setter for: Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z
    invoke-static {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->access$002(Landroid/support/place/connector/ConnectorContainer;Z)Z

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    invoke-virtual {v0, p1}, Landroid/support/place/connector/ConnectorContainer;->onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const-string v1, "onPlaceConnected"

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->enforceBaseCalled(Ljava/lang/String;)V

    return-void
.end method

.method public onPlaceDisconnected()V
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const/4 v1, 0x0

    # setter for: Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z
    invoke-static {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->access$002(Landroid/support/place/connector/ConnectorContainer;Z)Z

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorContainer;->onPlaceDisconnected()V

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const-string v1, "onPlaceDisconnected"

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->enforceBaseCalled(Ljava/lang/String;)V

    return-void
.end method

.method public onPlaceNameChanged(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const/4 v1, 0x0

    # setter for: Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z
    invoke-static {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->access$002(Landroid/support/place/connector/ConnectorContainer;Z)Z

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    invoke-virtual {v0, p1}, Landroid/support/place/connector/ConnectorContainer;->onPlaceNameChanged(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer$1;->this$0:Landroid/support/place/connector/ConnectorContainer;

    const-string v1, "onPlaceNameChanged"

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorContainer;->enforceBaseCalled(Ljava/lang/String;)V

    return-void
.end method
