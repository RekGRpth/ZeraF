.class public Lcom/google/mobile/googlenav/common/UserEventUtil;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static userEventLogContainsEventType([BLjava/util/Set;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Short;",
            ">;)Z"
        }
    .end annotation

    const-wide/16 v4, 0x8

    const-wide/16 v9, 0x2

    const/4 v8, 0x0

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readShort()S

    move-result v0

    const-wide/16 v2, 0x8

    invoke-virtual {v1, v2, v3}, Ljava/io/DataInputStream;->skip(J)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v8

    :goto_0
    return v0

    :cond_0
    move v2, v8

    :goto_1
    if-ge v2, v0, :cond_5

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readShort()S

    move-result v3

    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-wide/16 v3, 0x2

    invoke-virtual {v1, v3, v4}, Ljava/io/DataInputStream;->skip(J)J

    move-result-wide v3

    cmp-long v3, v3, v9

    if-eqz v3, :cond_2

    move v0, v8

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1, v4, v5}, Ljava/io/DataInputStream;->skip(J)J

    move-result-wide v4

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    move v0, v8

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1, v4, v5}, Ljava/io/DataInputStream;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v0, v8

    goto :goto_0

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "REQUEST"

    invoke-static {v1, v0}, Lcom/google/mobile/googlenav/common/ExceptionReporter;->logQuietThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v8

    goto :goto_0

    :cond_5
    move v0, v8

    goto :goto_0
.end method
