.class Lcom/google/android/street/Overlay$FadeAnimation;
.super Ljava/lang/Object;
.source "Overlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/Overlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FadeAnimation"
.end annotation


# instance fields
.field private mLabelOpacity:I

.field private mLabelState:I

.field private mLabelStateStartTime:J


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/google/android/street/Overlay$FadeAnimation;->reset(Z)V

    return-void
.end method


# virtual methods
.method public computeLabelOpacity(ZJ)J
    .locals 10
    .param p1    # Z
    .param p2    # J

    iget-wide v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    sub-long v0, p2, v6

    iget v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    packed-switch v6, :pswitch_data_0

    const-wide/16 v6, 0x0

    :goto_0
    return-wide v6

    :pswitch_0
    iget v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    const/high16 v7, 0x10000

    if-ne v6, v7, :cond_0

    const/4 v6, 0x1

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    const/high16 v6, 0x10000

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    iput-wide p2, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    const-wide/16 v6, 0x7d0

    add-long/2addr v6, p2

    goto :goto_0

    :cond_0
    const/4 v6, 0x4

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    const/4 v6, 0x0

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    iput-wide p2, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    const-wide/16 v6, 0x0

    goto :goto_0

    :pswitch_1
    if-eqz p1, :cond_1

    iput-wide p2, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    const-wide/16 v6, 0x7d0

    add-long/2addr v6, p2

    goto :goto_0

    :cond_1
    const-wide/16 v6, 0x7d0

    cmp-long v6, v0, v6

    if-gtz v6, :cond_2

    iget-wide v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    const-wide/16 v8, 0x7d0

    add-long/2addr v6, v8

    goto :goto_0

    :cond_2
    const-wide/16 v6, 0x7d0

    sub-long v2, v0, v6

    const-wide/16 v6, 0x190

    cmp-long v6, v2, v6

    if-gez v6, :cond_3

    const/4 v6, 0x3

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    sub-long v6, p2, v2

    iput-wide v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    const-wide/32 v6, 0x10000

    const-wide/16 v8, 0x190

    sub-long/2addr v8, v2

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x190

    div-long/2addr v6, v8

    long-to-int v6, v6

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    move-wide v6, p2

    goto :goto_0

    :cond_3
    const/4 v6, 0x4

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    const/4 v6, 0x0

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    iput-wide p2, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    const-wide/16 v6, 0x0

    goto :goto_0

    :pswitch_2
    const-wide/16 v6, 0x190

    sub-long/2addr v6, v0

    const-wide/16 v8, 0x0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    if-eqz p1, :cond_4

    const-wide/16 v6, 0xc8

    mul-long/2addr v6, v2

    const-wide/16 v8, 0x190

    div-long v4, v6, v8

    sub-long v6, p2, v4

    iput-wide v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    const/4 v6, 0x2

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    const-wide/32 v6, 0x10000

    mul-long/2addr v6, v4

    const-wide/16 v8, 0xc8

    div-long/2addr v6, v8

    long-to-int v6, v6

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    move-wide v6, p2

    goto/16 :goto_0

    :cond_4
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_5

    const-wide/32 v6, 0x10000

    mul-long/2addr v6, v2

    const-wide/16 v8, 0x190

    div-long/2addr v6, v8

    long-to-int v6, v6

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    move-wide v6, p2

    goto/16 :goto_0

    :cond_5
    const/4 v6, 0x4

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    const/4 v6, 0x0

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    iput-wide p2, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    const-wide/16 v6, 0x0

    goto/16 :goto_0

    :pswitch_3
    const-wide/16 v6, 0xc8

    sub-long/2addr v6, v0

    const-wide/16 v8, 0x0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_6

    const-wide/32 v6, 0x10000

    const-wide/16 v8, 0xc8

    sub-long/2addr v8, v2

    mul-long/2addr v6, v8

    const-wide/16 v8, 0xc8

    div-long/2addr v6, v8

    long-to-int v6, v6

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    move-wide v6, p2

    goto/16 :goto_0

    :cond_6
    const/4 v6, 0x1

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    const/high16 v6, 0x10000

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    iput-wide p2, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    const-wide/16 v6, 0x7d0

    add-long/2addr v6, p2

    goto/16 :goto_0

    :pswitch_4
    if-eqz p1, :cond_7

    const/4 v6, 0x2

    iput v6, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    iput-wide p2, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    move-wide v6, p2

    goto/16 :goto_0

    :cond_7
    const-wide/16 v6, 0x0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public getOpacity()I
    .locals 1

    iget v0, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    return v0
.end method

.method public isTransparent()Z
    .locals 1

    iget v0, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelStateStartTime:J

    iput v2, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelState:I

    if-eqz p1, :cond_0

    const/high16 v0, 0x10000

    :goto_0
    iput v0, p0, Lcom/google/android/street/Overlay$FadeAnimation;->mLabelOpacity:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method
