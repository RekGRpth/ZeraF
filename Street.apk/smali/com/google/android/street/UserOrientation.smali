.class Lcom/google/android/street/UserOrientation;
.super Ljava/lang/Object;
.source "UserOrientation.java"


# instance fields
.field private final mOrientation:[F

.field private final mRotationMatrix:[F

.field private final mTempMatrix:[F

.field private mTilt:F

.field private mUseRotationMatrix:Z

.field private mYaw:F

.field private mZoom:F


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v1, 0x10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/street/UserOrientation;->mTempMatrix:[F

    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/street/UserOrientation;->mOrientation:[F

    return-void

    :array_0
    .array-data 4
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
    .end array-data
.end method

.method public constructor <init>(FFF)V
    .locals 2
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/16 v1, 0x10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/street/UserOrientation;->mTempMatrix:[F

    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/street/UserOrientation;->mOrientation:[F

    iput p1, p0, Lcom/google/android/street/UserOrientation;->mYaw:F

    iput p2, p0, Lcom/google/android/street/UserOrientation;->mTilt:F

    iput p3, p0, Lcom/google/android/street/UserOrientation;->mZoom:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/UserOrientation;->mUseRotationMatrix:Z

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/street/UserOrientation;)V
    .locals 4
    .param p1    # Lcom/google/android/street/UserOrientation;

    const/16 v1, 0x10

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/street/UserOrientation;->mTempMatrix:[F

    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/street/UserOrientation;->mOrientation:[F

    iget v0, p1, Lcom/google/android/street/UserOrientation;->mYaw:F

    iput v0, p0, Lcom/google/android/street/UserOrientation;->mYaw:F

    iget v0, p1, Lcom/google/android/street/UserOrientation;->mTilt:F

    iput v0, p0, Lcom/google/android/street/UserOrientation;->mTilt:F

    iget v0, p1, Lcom/google/android/street/UserOrientation;->mZoom:F

    iput v0, p0, Lcom/google/android/street/UserOrientation;->mZoom:F

    iget-object v0, p1, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    iget-object v1, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    iget-object v2, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-boolean v0, p1, Lcom/google/android/street/UserOrientation;->mUseRotationMatrix:Z

    iput-boolean v0, p0, Lcom/google/android/street/UserOrientation;->mUseRotationMatrix:Z

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
    .end array-data
.end method


# virtual methods
.method public addZoom(FI)V
    .locals 4
    .param p1    # F
    .param p2    # I

    iget v1, p0, Lcom/google/android/street/UserOrientation;->mZoom:F

    add-float/2addr v1, p1

    const/4 v2, 0x0

    int-to-float v3, p2

    invoke-static {v1, v2, v3}, Lcom/google/android/street/StreetMath;->clamp(FFF)F

    move-result v0

    const v1, 0x3d4ccccd

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iput v0, p0, Lcom/google/android/street/UserOrientation;->mZoom:F

    return-void
.end method

.method public getPitchDegrees()F
    .locals 2

    iget v0, p0, Lcom/google/android/street/UserOrientation;->mTilt:F

    const/high16 v1, 0x3f000000

    sub-float/2addr v0, v1

    const/high16 v1, 0x43340000

    mul-float/2addr v0, v1

    return v0
.end method

.method public getRotationMatrix()[F
    .locals 11

    const/high16 v3, 0x3f800000

    const/4 v1, 0x0

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/google/android/street/UserOrientation;->mUseRotationMatrix:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    const/high16 v2, 0x42b40000

    iget v5, p0, Lcom/google/android/street/UserOrientation;->mTilt:F

    const/high16 v6, 0x43340000

    mul-float/2addr v5, v6

    sub-float/2addr v2, v5

    move v5, v4

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v5, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    iget v7, p0, Lcom/google/android/street/UserOrientation;->mYaw:F

    move v6, v1

    move v8, v4

    move v9, v3

    move v10, v4

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/UserOrientation;->mUseRotationMatrix:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    return-object v0
.end method

.method public getScale()F
    .locals 1

    iget v0, p0, Lcom/google/android/street/UserOrientation;->mZoom:F

    neg-float v0, v0

    invoke-static {v0}, Lcom/google/android/street/StreetMath;->exp2(F)F

    move-result v0

    return v0
.end method

.method public getTilt()F
    .locals 1

    iget v0, p0, Lcom/google/android/street/UserOrientation;->mTilt:F

    return v0
.end method

.method public getYaw()F
    .locals 1

    iget v0, p0, Lcom/google/android/street/UserOrientation;->mYaw:F

    return v0
.end method

.method public getZoom()F
    .locals 1

    iget v0, p0, Lcom/google/android/street/UserOrientation;->mZoom:F

    return v0
.end method

.method public setRotationMatrix([F)V
    .locals 7
    .param p1    # [F

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    iput-boolean v6, p0, Lcom/google/android/street/UserOrientation;->mUseRotationMatrix:Z

    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    iget-object v2, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    array-length v2, v2

    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mRotationMatrix:[F

    iget-object v2, p0, Lcom/google/android/street/UserOrientation;->mTempMatrix:[F

    iget-object v3, p0, Lcom/google/android/street/UserOrientation;->mTempMatrix:[F

    array-length v3, v3

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mTempMatrix:[F

    const/high16 v2, -0x3d4c0000

    const/high16 v3, 0x3f800000

    move v5, v4

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mTempMatrix:[F

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/street/UserOrientation;->mTempMatrix:[F

    invoke-static {v0, v6, v2, v3}, Landroid/hardware/SensorManager;->remapCoordinateSystem([FII[F)Z

    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mTempMatrix:[F

    iget-object v2, p0, Lcom/google/android/street/UserOrientation;->mOrientation:[F

    invoke-static {v0, v2}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mOrientation:[F

    aget v0, v0, v1

    invoke-static {v0}, Lcom/google/android/street/StreetMath;->radiansToDegrees(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/street/UserOrientation;->mYaw:F

    iget-object v0, p0, Lcom/google/android/street/UserOrientation;->mOrientation:[F

    aget v0, v0, v6

    invoke-static {v0}, Lcom/google/android/street/StreetMath;->radiansToNormalizedTilt(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/street/UserOrientation;->mTilt:F

    return-void
.end method

.method public setTilt(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/google/android/street/UserOrientation;->mTilt:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/UserOrientation;->mUseRotationMatrix:Z

    return-void
.end method

.method public setYaw(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/google/android/street/UserOrientation;->mYaw:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/UserOrientation;->mUseRotationMatrix:Z

    return-void
.end method

.method public setZoom(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/street/UserOrientation;->mZoom:F

    return-void
.end method
