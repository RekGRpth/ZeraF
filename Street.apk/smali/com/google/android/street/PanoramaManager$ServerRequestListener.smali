.class Lcom/google/android/street/PanoramaManager$ServerRequestListener;
.super Ljava/lang/Object;
.source "PanoramaManager.java"

# interfaces
.implements Lcom/google/android/street/PanoramaRequest$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/PanoramaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServerRequestListener"
.end annotation


# instance fields
.field private mCntConfigsReceived:I

.field private final mConfigListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

.field private final mPanoListener:Lcom/google/android/street/PanoramaManager$PanoFetchListener;

.field private final mRetrievalKey:Ljava/lang/Object;

.field final synthetic this$0:Lcom/google/android/street/PanoramaManager;


# direct methods
.method public constructor <init>(Lcom/google/android/street/PanoramaManager;Lcom/google/android/street/PanoramaManager$ConfigFetchListener;Lcom/google/android/street/PanoramaManager$PanoFetchListener;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Lcom/google/android/street/PanoramaManager$ConfigFetchListener;
    .param p3    # Lcom/google/android/street/PanoramaManager$PanoFetchListener;
    .param p4    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->this$0:Lcom/google/android/street/PanoramaManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mCntConfigsReceived:I

    iput-object p2, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mConfigListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    iput-object p3, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mPanoListener:Lcom/google/android/street/PanoramaManager$PanoFetchListener;

    iput-object p4, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mRetrievalKey:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public receivedDone(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mConfigListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mCntConfigsReceived:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mConfigListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, Lcom/google/android/street/PanoramaManager$ConfigFetchListener;->postPanoramaInfo(ZLcom/google/android/street/PanoramaConfig;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->this$0:Lcom/google/android/street/PanoramaManager;

    iget-object v1, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mRetrievalKey:Ljava/lang/Object;

    # invokes: Lcom/google/android/street/PanoramaManager;->doneRetrieving(Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lcom/google/android/street/PanoramaManager;->access$200(Lcom/google/android/street/PanoramaManager;Ljava/lang/Object;)V

    return-void
.end method

.method public receivedPanoramaConfig(Lcom/google/android/street/PanoramaConfig;Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 7
    .param p1    # Lcom/google/android/street/PanoramaConfig;
    .param p2    # Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;

    iget v5, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mCntConfigsReceived:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mCntConfigsReceived:I

    iget-object v5, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mConfigListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mConfigListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    const/4 v6, 0x0

    invoke-interface {v5, v6, p1}, Lcom/google/android/street/PanoramaManager$ConfigFetchListener;->postPanoramaInfo(ZLcom/google/android/street/PanoramaConfig;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/street/PanoramaConfig;->getPersistentKey()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-virtual {p2}, Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->this$0:Lcom/google/android/street/PanoramaManager;

    # getter for: Lcom/google/android/street/PanoramaManager;->mHttpCache:Lcom/google/android/street/HttpCache;
    invoke-static {v5}, Lcom/google/android/street/PanoramaManager;->access$100(Lcom/google/android/street/PanoramaManager;)Lcom/google/android/street/HttpCache;

    move-result-object v5

    invoke-virtual {v5, v0, v2}, Lcom/google/android/street/HttpCache;->saveToCache([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v5

    move-object v1, v5

    const-string v5, "PM failed to cache config"

    invoke-static {v5, v1}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v5

    move-object v1, v5

    const-string v5, "PM was interrupted caching config"

    invoke-static {v5}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public receivedPanoramaTile(Ljava/lang/String;IIII[B)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # [B

    new-instance v0, Lcom/google/android/street/PanoramaImageKey;

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/street/PanoramaImageKey;-><init>(Ljava/lang/String;IIII)V

    iget-object v1, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mPanoListener:Lcom/google/android/street/PanoramaManager$PanoFetchListener;

    if-eqz v1, :cond_0

    new-instance v8, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v8}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v8, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v8, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    iput-boolean v1, v8, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    const/4 v1, 0x0

    array-length v2, p6

    invoke-static {p6, v1, v2, v8}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->mPanoListener:Lcom/google/android/street/PanoramaManager$PanoFetchListener;

    invoke-interface {v1, v0, v7}, Lcom/google/android/street/PanoramaManager$PanoFetchListener;->postPanoramaTileImage(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/PanoramaManager$ServerRequestListener;->this$0:Lcom/google/android/street/PanoramaManager;

    # getter for: Lcom/google/android/street/PanoramaManager;->mHttpCache:Lcom/google/android/street/HttpCache;
    invoke-static {v1}, Lcom/google/android/street/PanoramaManager;->access$100(Lcom/google/android/street/PanoramaManager;)Lcom/google/android/street/HttpCache;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/street/PanoramaImageKey;->persistentCacheKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p6, v2}, Lcom/google/android/street/HttpCache;->saveToCache([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v1

    move-object v6, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PM failed to cache tile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v6, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PM was interrupted caching tile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method
