.class Lcom/google/android/street/Overlay$Polygon;
.super Ljava/lang/Object;
.source "Overlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/Overlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Polygon"
.end annotation


# instance fields
.field private final mFillIndexBuffer:Ljava/nio/ByteBuffer;

.field private final mFillIndexCount:I

.field private final mOutlineIndexBuffer:Ljava/nio/ByteBuffer;

.field private final mOutlineIndexCount:I

.field private final mVertexBuffer:Ljava/nio/FloatBuffer;


# direct methods
.method public constructor <init>([F)V
    .locals 1
    .param p1    # [F

    array-length v0, p1

    div-int/lit8 v0, v0, 0x3

    invoke-static {v0}, Lcom/google/android/street/Overlay$Polygon;->identityIndexArray(I)[B

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B)V

    return-void
.end method

.method public constructor <init>([F[B)V
    .locals 0
    .param p1    # [F
    .param p2    # [B

    invoke-direct {p0, p1, p2, p2}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B[B)V

    return-void
.end method

.method public constructor <init>([F[B[B)V
    .locals 5
    .param p1    # [F
    .param p2    # [B
    .param p3    # [B

    const/16 v4, 0x100

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    array-length v1, p1

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/street/Overlay$Polygon;->mVertexBuffer:Ljava/nio/FloatBuffer;

    iget-object v1, p0, Lcom/google/android/street/Overlay$Polygon;->mVertexBuffer:Ljava/nio/FloatBuffer;

    array-length v2, p1

    invoke-virtual {v1, p1, v3, v2}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    iget-object v1, p0, Lcom/google/android/street/Overlay$Polygon;->mVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    array-length v1, p2

    iput v1, p0, Lcom/google/android/street/Overlay$Polygon;->mFillIndexCount:I

    array-length v1, p3

    iput v1, p0, Lcom/google/android/street/Overlay$Polygon;->mOutlineIndexCount:I

    iget v1, p0, Lcom/google/android/street/Overlay$Polygon;->mFillIndexCount:I

    if-gt v1, v4, :cond_0

    iget v1, p0, Lcom/google/android/street/Overlay$Polygon;->mOutlineIndexCount:I

    if-le v1, v4, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Only up to 256 points"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {p2}, Lcom/google/android/street/Overlay$Polygon;->indexBufferHelper([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/street/Overlay$Polygon;->mFillIndexBuffer:Ljava/nio/ByteBuffer;

    invoke-static {p3}, Lcom/google/android/street/Overlay$Polygon;->indexBufferHelper([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/street/Overlay$Polygon;->mOutlineIndexBuffer:Ljava/nio/ByteBuffer;

    return-void
.end method

.method private checkVisible(IIII)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    if-ge p1, p3, :cond_0

    if-ge p2, p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static identityIndexArray(I)[B
    .locals 3
    .param p0    # I

    new-array v1, p0, [B

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p0, :cond_0

    int-to-byte v2, v0

    aput-byte v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static indexBufferHelper([B)Ljava/nio/ByteBuffer;
    .locals 5
    .param p0    # [B

    const/4 v4, 0x0

    array-length v1, p0

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p0, v4, v1}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    return-object v0
.end method


# virtual methods
.method draw(Ljavax/microedition/khronos/opengles/GL10;I)V
    .locals 4
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # I

    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    const/4 v0, 0x3

    const/16 v1, 0x1406

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/street/Overlay$Polygon;->mVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    const/16 v0, 0xde1

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    iget v0, p0, Lcom/google/android/street/Overlay$Polygon;->mFillIndexCount:I

    const/16 v1, 0x1401

    iget-object v2, p0, Lcom/google/android/street/Overlay$Polygon;->mFillIndexBuffer:Ljava/nio/ByteBuffer;

    invoke-interface {p1, p2, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawElements(IIILjava/nio/Buffer;)V

    return-void
.end method

.method drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    const/16 v4, 0xb20

    const/16 v0, 0xde1

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    const/4 v0, 0x3

    const/16 v1, 0x1406

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/street/Overlay$Polygon;->mVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/street/Overlay$Polygon;->mOutlineIndexCount:I

    const/16 v2, 0x1401

    iget-object v3, p0, Lcom/google/android/street/Overlay$Polygon;->mOutlineIndexBuffer:Ljava/nio/ByteBuffer;

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawElements(IIILjava/nio/Buffer;)V

    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    return-void
.end method

.method public getAABB(Lcom/google/android/street/Projector;[II[FII)Z
    .locals 18
    .param p1    # Lcom/google/android/street/Projector;
    .param p2    # [I
    .param p3    # I
    .param p4    # [F
    .param p5    # I
    .param p6    # I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay$Polygon;->mVertexBuffer:Ljava/nio/FloatBuffer;

    move-object v15, v0

    invoke-virtual {v15}, Ljava/nio/FloatBuffer;->limit()I

    move-result v15

    div-int/lit8 v7, v15, 0x3

    const v10, 0x7fffffff

    const/high16 v11, -0x80000000

    const v13, 0x7fffffff

    const/high16 v14, -0x80000000

    const/4 v8, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_2

    mul-int/lit8 v6, v5, 0x3

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay$Polygon;->mVertexBuffer:Ljava/nio/FloatBuffer;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move v1, v6

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v16

    aput v16, p4, v15

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay$Polygon;->mVertexBuffer:Ljava/nio/FloatBuffer;

    move-object/from16 v16, v0

    add-int/lit8 v17, v6, 0x1

    invoke-virtual/range {v16 .. v17}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v16

    aput v16, p4, v15

    const/4 v15, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay$Polygon;->mVertexBuffer:Ljava/nio/FloatBuffer;

    move-object/from16 v16, v0

    add-int/lit8 v17, v6, 0x2

    invoke-virtual/range {v16 .. v17}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v16

    aput v16, p4, v15

    const/4 v15, 0x3

    const/high16 v16, 0x3f800000

    aput v16, p4, v15

    const/4 v15, 0x0

    const/16 v16, 0x4

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move v2, v15

    move-object/from16 v3, p4

    move/from16 v4, v16

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/street/Projector;->project([FI[FI)V

    const/4 v15, 0x4

    aget v15, p4, v15

    float-to-int v9, v15

    const/4 v15, 0x5

    aget v15, p4, v15

    float-to-int v12, v15

    if-nez v8, :cond_0

    move-object/from16 v0, p0

    move v1, v9

    move v2, v12

    move/from16 v3, p5

    move/from16 v4, p6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/street/Overlay$Polygon;->checkVisible(IIII)Z

    move-result v15

    if-eqz v15, :cond_1

    :cond_0
    const/4 v15, 0x1

    move v8, v15

    :goto_1
    invoke-static {v10, v9}, Ljava/lang/Math;->min(II)I

    move-result v10

    invoke-static {v11, v9}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-static {v13, v12}, Ljava/lang/Math;->min(II)I

    move-result v13

    invoke-static {v14, v12}, Ljava/lang/Math;->max(II)I

    move-result v14

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v15, 0x0

    move v8, v15

    goto :goto_1

    :cond_2
    if-eqz v8, :cond_3

    aput v10, p2, p3

    add-int/lit8 v15, p3, 0x1

    aput v13, p2, v15

    add-int/lit8 v15, p3, 0x2

    aput v11, p2, v15

    add-int/lit8 v15, p3, 0x3

    aput v14, p2, v15

    :cond_3
    return v8
.end method
