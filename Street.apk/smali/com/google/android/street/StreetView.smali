.class Lcom/google/android/street/StreetView;
.super Landroid/view/SurfaceView;
.source "StreetView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Lcom/google/android/common/gesture/ScaleEventListener;
.implements Lcom/google/android/street/PanoramaManager$ConfigFetchListener;
.implements Lcom/google/android/street/Renderer$RenderStatusReceiver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/street/StreetView$OrientationSensorListener;,
        Lcom/google/android/street/StreetView$Status;,
        Lcom/google/android/street/StreetView$Flinger;
    }
.end annotation


# instance fields
.field private final clickToGoUiMode:I

.field private mAnchorPitch:F

.field private mAnchorYaw:F

.field private mAspectRatio:F

.field private mBadPanorama:Z

.field private mCurrentStatus:Lcom/google/android/street/StreetView$Status;

.field private mFlinger:Lcom/google/android/street/StreetView$Flinger;

.field private final mFlingerLock:Ljava/lang/Object;

.field private final mHandler:Landroid/os/Handler;

.field private mHighlighter:Lcom/google/android/street/Highlighter;

.field private mHorizontalTanRatio:F

.field private mInitialPanoId:Ljava/lang/String;

.field private mInitialPanoMapPoint:Lcom/google/android/street/MapPoint;

.field private mIsTablet:Z

.field private mLastDX:F

.field private mLastDY:F

.field private mNetworkUnavailableToast:Landroid/widget/Toast;

.field private mOldProgress:I

.field private mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

.field private mPanoramaConfigCache:Lcom/google/android/street/PanoramaConfigCache;

.field private mPanoramaLink:Lcom/google/android/street/PanoramaLink;

.field private mPanoramaManager:Lcom/google/android/street/PanoramaManager;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mRenderer:Lcom/google/android/street/Renderer;

.field private mRequestedFaceToLatLng:Lcom/google/android/street/MapPoint;

.field private mRequestedUserOrientation:Lcom/google/android/street/UserOrientation;

.field private mScreenDensity:F

.field private mSensorEnabled:Z

.field private final mSensorListener:Lcom/google/android/street/StreetView$OrientationSensorListener;

.field private mStatusOverride:Ljava/lang/String;

.field private mStreet:Lcom/google/android/street/Street;

.field private mStreetViewGestureController:Lcom/google/android/common/gesture/StreetViewGestureController;

.field private mTrackballGestureDetector:Lcom/google/android/street/TrackballGestureDetector;

.field private mUnzoomedFovH:F

.field private mUnzoomedFovV:F

.field private mUseSensorToControlView:Z

.field private final mUserActivityReporter:Ljava/lang/Runnable;

.field private mUserOrientation:Lcom/google/android/street/UserOrientation;

.field private mVerticalTanRatio:F

.field private mZoomButtonsController:Landroid/widget/ZoomButtonsController;

.field private mZoomButtonsEnabled:Z

.field private mZoomLevels:I

.field private panoCount:I

.field private timer:Lcom/google/android/street/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsEnabled:Z

    iput v2, p0, Lcom/google/android/street/StreetView;->clickToGoUiMode:I

    iput v2, p0, Lcom/google/android/street/StreetView;->panoCount:I

    iput-object v1, p0, Lcom/google/android/street/StreetView;->timer:Lcom/google/android/street/Timer;

    new-instance v0, Lcom/google/android/street/StreetView$1;

    invoke-direct {v0, p0}, Lcom/google/android/street/StreetView$1;-><init>(Lcom/google/android/street/StreetView;)V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mUserActivityReporter:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/street/StreetView$2;

    invoke-direct {v0, p0}, Lcom/google/android/street/StreetView$2;-><init>(Lcom/google/android/street/StreetView;)V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/street/StreetView$OrientationSensorListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/street/StreetView$OrientationSensorListener;-><init>(Lcom/google/android/street/StreetView;Lcom/google/android/street/StreetView$1;)V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mSensorListener:Lcom/google/android/street/StreetView$OrientationSensorListener;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mFlingerLock:Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mFlinger:Lcom/google/android/street/StreetView$Flinger;

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->commonConstructor(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/street/StreetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsEnabled:Z

    iput v2, p0, Lcom/google/android/street/StreetView;->clickToGoUiMode:I

    iput v2, p0, Lcom/google/android/street/StreetView;->panoCount:I

    iput-object v1, p0, Lcom/google/android/street/StreetView;->timer:Lcom/google/android/street/Timer;

    new-instance v0, Lcom/google/android/street/StreetView$1;

    invoke-direct {v0, p0}, Lcom/google/android/street/StreetView$1;-><init>(Lcom/google/android/street/StreetView;)V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mUserActivityReporter:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/street/StreetView$2;

    invoke-direct {v0, p0}, Lcom/google/android/street/StreetView$2;-><init>(Lcom/google/android/street/StreetView;)V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/street/StreetView$OrientationSensorListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/street/StreetView$OrientationSensorListener;-><init>(Lcom/google/android/street/StreetView;Lcom/google/android/street/StreetView$1;)V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mSensorListener:Lcom/google/android/street/StreetView$OrientationSensorListener;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mFlingerLock:Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mFlinger:Lcom/google/android/street/StreetView$Flinger;

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->commonConstructor(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/street/StreetView;)Landroid/os/PowerManager;
    .locals 1
    .param p0    # Lcom/google/android/street/StreetView;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPowerManager:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/street/StreetView;ZLcom/google/android/street/PanoramaConfig;)V
    .locals 0
    .param p0    # Lcom/google/android/street/StreetView;
    .param p1    # Z
    .param p2    # Lcom/google/android/street/PanoramaConfig;

    invoke-direct {p0, p1, p2}, Lcom/google/android/street/StreetView;->addPanoramaConfig(ZLcom/google/android/street/PanoramaConfig;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/street/StreetView;)Lcom/google/android/street/UserOrientation;
    .locals 1
    .param p0    # Lcom/google/android/street/StreetView;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/street/StreetView;)V
    .locals 0
    .param p0    # Lcom/google/android/street/StreetView;

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateRendererUserOrientation()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/street/StreetView;)V
    .locals 0
    .param p0    # Lcom/google/android/street/StreetView;

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateButtonsEnabled()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/street/StreetView;)Landroid/widget/ZoomButtonsController;
    .locals 1
    .param p0    # Lcom/google/android/street/StreetView;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/street/StreetView;F)V
    .locals 0
    .param p0    # Lcom/google/android/street/StreetView;
    .param p1    # F

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->smoothZoom(F)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/street/StreetView;I)V
    .locals 0
    .param p0    # Lcom/google/android/street/StreetView;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->doUpdateProgress(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/street/StreetView;I)V
    .locals 0
    .param p0    # Lcom/google/android/street/StreetView;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->doUpdateTransitionProgress(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/street/StreetView;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/street/StreetView;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mFlingerLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/street/StreetView;)Lcom/google/android/street/PanoramaConfig;
    .locals 1
    .param p0    # Lcom/google/android/street/StreetView;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/street/StreetView;)Lcom/google/android/street/Street;
    .locals 1
    .param p0    # Lcom/google/android/street/StreetView;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/street/StreetView;)Z
    .locals 1
    .param p0    # Lcom/google/android/street/StreetView;

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->okToAct()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/street/StreetView;)V
    .locals 0
    .param p0    # Lcom/google/android/street/StreetView;

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->reportUserActivity()V

    return-void
.end method

.method private addPanoramaConfig(ZLcom/google/android/street/PanoramaConfig;)V
    .locals 2

    if-eqz p1, :cond_1

    const-string v0, "SV panorama config request was interrupted"

    invoke-static {v0}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->invalidate()V

    return-void

    :cond_1
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SV received panorama "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-nez p2, :cond_3

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/street/StreetView;->mBadPanorama:Z

    invoke-direct {p0, p2}, Lcom/google/android/street/StreetView;->setPanoramaConfig(Lcom/google/android/street/PanoramaConfig;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private checkStatus()Ljava/lang/CharSequence;
    .locals 15

    const/4 v6, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->wasNetworkUp()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mNetworkUnavailableToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mNetworkUnavailableToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mNetworkUnavailableToast:Landroid/widget/Toast;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    iget-object v0, v0, Lcom/google/android/street/PanoramaLink;->mPanoId:Ljava/lang/String;

    move-object v2, v0

    :goto_1
    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mInitialPanoMapPoint:Lcom/google/android/street/MapPoint;

    if-eqz v0, :cond_9

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfigCache:Lcom/google/android/street/PanoramaConfigCache;

    invoke-virtual {v0, v2}, Lcom/google/android/street/PanoramaConfigCache;->get(Ljava/lang/String;)Lcom/google/android/street/PanoramaConfig;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-direct {p0, v1}, Lcom/google/android/street/StreetView;->setPanoramaConfig(Lcom/google/android/street/PanoramaConfig;)V

    iget-object v0, v1, Lcom/google/android/street/PanoramaConfig;->mRootImageKeys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    move v4, v3

    :goto_2
    if-ge v4, v2, :cond_5

    iget-object v5, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    iget-object v0, v1, Lcom/google/android/street/PanoramaConfig;->mRootImageKeys:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/street/PanoramaImageKey;

    sub-int v8, v2, v6

    if-ne v4, v8, :cond_4

    move v8, v6

    :goto_3
    invoke-virtual {v5, v7, v0, v8}, Lcom/google/android/street/PanoramaManager;->requestPanoramaTile(Lcom/google/android/street/PanoramaManager$PanoFetchListener;Lcom/google/android/street/PanoramaImageKey;Z)Z

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mNetworkUnavailableToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04000e

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mNetworkUnavailableToast:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mNetworkUnavailableToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mInitialPanoId:Ljava/lang/String;

    move-object v2, v0

    goto :goto_1

    :cond_4
    move v8, v3

    goto :goto_3

    :cond_5
    invoke-direct {p0}, Lcom/google/android/street/StreetView;->getStatusFromPanoramaConfig()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_4
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mInitialPanoMapPoint:Lcom/google/android/street/MapPoint;

    if-nez v2, :cond_8

    move v4, v6

    :goto_5
    invoke-virtual {v0, p0, v2, v1, v4}, Lcom/google/android/street/PanoramaManager;->requestPanoramaConfiguration(Lcom/google/android/street/PanoramaManager$ConfigFetchListener;Ljava/lang/String;Lcom/google/android/street/MapPoint;Z)Z

    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    move v4, v3

    move v5, v3

    move v7, v3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/street/PanoramaManager;->requestPanoramaTile(Lcom/google/android/street/PanoramaManager$PanoFetchListener;Ljava/lang/String;IIIIZ)Z

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-object v8, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    const/4 v13, 0x2

    move-object v9, v2

    move v10, v3

    move v11, v3

    move v12, v3

    move v14, v3

    invoke-virtual/range {v7 .. v14}, Lcom/google/android/street/PanoramaManager;->requestPanoramaTile(Lcom/google/android/street/PanoramaManager$PanoFetchListener;Ljava/lang/String;IIIIZ)Z

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-object v8, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    const/4 v13, 0x3

    move-object v9, v2

    move v10, v3

    move v11, v3

    move v12, v3

    move v14, v3

    invoke-virtual/range {v7 .. v14}, Lcom/google/android/street/PanoramaManager;->requestPanoramaTile(Lcom/google/android/street/PanoramaManager$PanoFetchListener;Ljava/lang/String;IIIIZ)Z

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-object v8, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    const/4 v13, 0x4

    move-object v9, v2

    move v10, v3

    move v11, v3

    move v12, v3

    move v14, v3

    invoke-virtual/range {v7 .. v14}, Lcom/google/android/street/PanoramaManager;->requestPanoramaTile(Lcom/google/android/street/PanoramaManager$PanoFetchListener;Ljava/lang/String;IIIIZ)Z

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-object v8, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    const/4 v13, 0x5

    move-object v9, v2

    move v10, v3

    move v11, v3

    move v12, v3

    move v14, v3

    invoke-virtual/range {v7 .. v14}, Lcom/google/android/street/PanoramaManager;->requestPanoramaTile(Lcom/google/android/street/PanoramaManager$PanoFetchListener;Ljava/lang/String;IIIIZ)Z

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-object v8, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    const/4 v13, 0x6

    move-object v9, v2

    move v10, v3

    move v11, v3

    move v12, v3

    move v14, v3

    invoke-virtual/range {v7 .. v14}, Lcom/google/android/street/PanoramaManager;->requestPanoramaTile(Lcom/google/android/street/PanoramaManager$PanoFetchListener;Ljava/lang/String;IIIIZ)Z

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget-object v8, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    const/4 v13, -0x1

    move-object v9, v2

    move v10, v3

    move v11, v3

    move v12, v3

    move v14, v6

    invoke-virtual/range {v7 .. v14}, Lcom/google/android/street/PanoramaManager;->requestPanoramaTile(Lcom/google/android/street/PanoramaManager$PanoFetchListener;Ljava/lang/String;IIIIZ)Z

    :cond_7
    invoke-direct {p0}, Lcom/google/android/street/StreetView;->getLoadingStatus()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_4

    :cond_8
    move v4, v3

    goto :goto_5

    :cond_9
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStatusOverride:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStatusOverride:Ljava/lang/String;

    goto :goto_4

    :cond_a
    invoke-direct {p0}, Lcom/google/android/street/StreetView;->getStatusFromPanoramaConfig()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_4

    :cond_b
    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mBadPanorama:Z

    if-eqz v0, :cond_c

    const v0, 0x7f04000b

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_4

    :cond_c
    const v0, 0x7f04000a

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_4
.end method

.method private clickToGo(FF)Z
    .locals 11
    .param p1    # F
    .param p2    # F

    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-nez v0, :cond_0

    move v0, v5

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0, p1, p2, v5}, Lcom/google/android/street/Renderer;->pixelToYawPitch(FFZ)[F

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    aget v3, v9, v5

    aget v4, v9, v10

    invoke-virtual {v0, v3, v4, v9}, Lcom/google/android/street/PanoramaConfig;->worldToVehicleYawPitch(FF[F)V

    const/4 v0, 0x2

    new-array v6, v0, [F

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0}, Lcom/google/android/street/DepthMap;->decompress()Z

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    aget v3, v9, v5

    aget v4, v9, v10

    invoke-virtual {v0, v3, v4, v6}, Lcom/google/android/street/DepthMap;->getPanoId(FF[F)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    move v0, v10

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mPanoId:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v5

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    aget v3, v6, v5

    aget v4, v6, v10

    invoke-virtual {v0, v3, v4, v6}, Lcom/google/android/street/PanoramaConfig;->vehicleToWorldYawPitch(FF[F)V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    aget v3, v9, v5

    aget v4, v9, v10

    invoke-virtual {v0, v3, v4}, Lcom/google/android/street/DepthMap;->getPlane(FF)Lcom/google/android/street/DepthMap$DepthMapPlane;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/street/DepthMap$DepthMapPlane;->isGround()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    aget v3, v6, v5

    invoke-static {v3}, Lcom/google/android/street/StreetMath;->normalizeUnitAngle(F)F

    move-result v3

    invoke-static {v3}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/street/UserOrientation;->setYaw(F)V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    const/high16 v3, 0x40000000

    aget v4, v6, v10

    invoke-static {v4}, Lcom/google/android/street/StreetMath;->normalizeUnitAngle(F)F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual {v0, v3}, Lcom/google/android/street/UserOrientation;->setTilt(F)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    aget v3, v9, v5

    aget v4, v9, v10

    invoke-virtual {v0, v3, v4}, Lcom/google/android/street/DepthMap;->getPanoPoint(FF)Lcom/google/android/street/DepthMap$Point;

    move-result-object v7

    new-instance v8, Lcom/google/android/street/Renderer$Transition;

    iget v0, v7, Lcom/google/android/street/DepthMap$Point;->x:F

    iget v3, v7, Lcom/google/android/street/DepthMap$Point;->y:F

    iget-object v4, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    const/16 v5, 0x3e8

    invoke-direct {v8, v0, v3, v4, v5}, Lcom/google/android/street/Renderer$Transition;-><init>(FFLcom/google/android/street/UserOrientation;I)V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v8, v0}, Lcom/google/android/street/Renderer$Transition;->detectCollision(Lcom/google/android/street/DepthMap;)Z

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0, v8}, Lcom/google/android/street/Renderer;->startTransition(Lcom/google/android/street/Renderer$Transition;)V

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/street/StreetView;->loadPanorama(Ljava/lang/String;Lcom/google/android/street/MapPoint;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/MapPoint;Lcom/google/android/street/PanoramaConfig;)V

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->invalidate()V

    move v0, v10

    goto/16 :goto_0
.end method

.method private commonConstructor(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/street/StreetView;->mScreenDensity:F

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SV ScreenDensity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/street/StreetView;->mScreenDensity:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", DPI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateDeviceOrientation()V

    new-instance v1, Lcom/google/android/street/UserOrientation;

    invoke-direct {v1}, Lcom/google/android/street/UserOrientation;-><init>()V

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    new-instance v1, Lcom/google/android/street/PanoramaConfigCache;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Lcom/google/android/street/PanoramaConfigCache;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfigCache:Lcom/google/android/street/PanoramaConfigCache;

    new-instance v1, Lcom/google/android/street/StreetView$Status;

    invoke-direct {v1}, Lcom/google/android/street/StreetView$Status;-><init>()V

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mCurrentStatus:Lcom/google/android/street/StreetView$Status;

    new-instance v1, Lcom/google/android/street/TrackballGestureDetector;

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2}, Lcom/google/android/street/TrackballGestureDetector;-><init>(Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mTrackballGestureDetector:Lcom/google/android/street/TrackballGestureDetector;

    new-instance v1, Lcom/google/android/common/gesture/StreetViewGestureController;

    invoke-direct {v1, p1, p0, p0, p0}, Lcom/google/android/common/gesture/StreetViewGestureController;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/view/GestureDetector$OnDoubleTapListener;Lcom/google/android/common/gesture/ScaleEventListener;)V

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mStreetViewGestureController:Lcom/google/android/common/gesture/StreetViewGestureController;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mStreetViewGestureController:Lcom/google/android/common/gesture/StreetViewGestureController;

    invoke-virtual {v1, v3}, Lcom/google/android/common/gesture/StreetViewGestureController;->setIsLongpressEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mStreetViewGestureController:Lcom/google/android/common/gesture/StreetViewGestureController;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/common/gesture/StreetViewGestureController;->setMultiTouchSupported(Landroid/content/pm/PackageManager;)V

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mStreetViewGestureController:Lcom/google/android/common/gesture/StreetViewGestureController;

    invoke-virtual {v1}, Lcom/google/android/common/gesture/StreetViewGestureController;->isMultiTouchSupported()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/street/StreetView;->mZoomButtonsEnabled:Z

    new-instance v1, Lcom/google/android/street/Highlighter;

    invoke-direct {v1}, Lcom/google/android/street/Highlighter;-><init>()V

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mHighlighter:Lcom/google/android/street/Highlighter;

    return-void

    :cond_0
    move v1, v3

    goto :goto_0
.end method

.method private createRenderer()V
    .locals 7

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mRenderer already exists"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/street/Renderer;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iget v2, p0, Lcom/google/android/street/StreetView;->mScreenDensity:F

    invoke-direct {v0, v1, v2}, Lcom/google/android/street/Renderer;-><init>(Lcom/google/android/street/PanoramaManager;F)V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mIsTablet:Z

    if-nez v0, :cond_1

    move v6, v4

    :goto_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    move-object v3, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/street/Renderer;->initialize(Landroid/content/Context;Landroid/view/SurfaceHolder;Lcom/google/android/street/Renderer$RenderStatusReceiver;ZZZ)V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->timer:Lcom/google/android/street/Timer;

    invoke-virtual {v0, v1}, Lcom/google/android/street/Renderer;->setTimer(Lcom/google/android/street/Timer;)V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mHighlighter:Lcom/google/android/street/Highlighter;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0, v1}, Lcom/google/android/street/Highlighter;->setRenderer(Lcom/google/android/street/Renderer;)V

    return-void

    :cond_1
    move v6, v5

    goto :goto_0
.end method

.method private doTilt(F)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget v0, v0, Lcom/google/android/street/PanoramaConfig;->mProjectionType:I

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v0}, Lcom/google/android/street/UserOrientation;->getScale()F

    move-result v0

    const/high16 v1, 0x3e000000

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget v1, v1, Lcom/google/android/street/PanoramaConfig;->mMaxVisiblePitchDeg:F

    invoke-static {v1}, Lcom/google/android/street/StreetView;->tiltDegToHalfTurns(F)F

    move-result v1

    add-float/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget v2, v2, Lcom/google/android/street/PanoramaConfig;->mMinVisiblePitchDeg:F

    invoke-static {v2}, Lcom/google/android/street/StreetView;->tiltDegToHalfTurns(F)F

    move-result v2

    sub-float/2addr v2, v0

    cmpl-float v3, v1, v2

    if-lez v3, :cond_0

    add-float/2addr v1, v2

    const/high16 v2, 0x3f000000

    mul-float/2addr v1, v2

    move v2, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getTilt()F

    move-result v3

    mul-float/2addr v0, p1

    add-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-static {v0, v2, v1}, Lcom/google/android/street/StreetMath;->clamp(FFF)F

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/android/street/UserOrientation;->setTilt(F)V

    return-void

    :cond_0
    move v4, v2

    move v2, v1

    move v1, v4

    goto :goto_0
.end method

.method private doUpdateProgress(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;

    invoke-virtual {v0, p1}, Lcom/google/android/street/Street;->reportProgress(I)V

    return-void
.end method

.method private doUpdateTransitionProgress(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;

    invoke-virtual {v0, p1}, Lcom/google/android/street/Street;->reportTransitionProgress(I)V

    return-void
.end method

.method private doYaw(F)V
    .locals 1
    .param p1    # F

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/street/StreetView;->doYawTilt(FF)V

    return-void
.end method

.method private doYawTilt(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v1}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v1

    const/high16 v2, 0x41200000

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getScale()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-static {v1}, Lcom/google/android/street/StreetMath;->normalizeDegrees(F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/street/UserOrientation;->setYaw(F)V

    invoke-direct {p0, p2}, Lcom/google/android/street/StreetView;->doTilt(F)V

    return-void
.end method

.method private doZoom(F)V
    .locals 3
    .param p1    # F

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    iget v1, p0, Lcom/google/android/street/StreetView;->mZoomLevels:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {v0, p1, v1}, Lcom/google/android/street/UserOrientation;->addZoom(FI)V

    return-void
.end method

.method private declared-synchronized enableSensor(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mSensorEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mSensorListener:Lcom/google/android/street/StreetView$OrientationSensorListener;

    invoke-virtual {v0}, Lcom/google/android/street/StreetView$OrientationSensorListener;->register()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/StreetView;->mSensorEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mSensorEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mSensorListener:Lcom/google/android/street/StreetView$OrientationSensorListener;

    invoke-virtual {v0}, Lcom/google/android/street/StreetView$OrientationSensorListener;->unregister()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/StreetView;->mSensorEnabled:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private finishTap(Landroid/view/MotionEvent;)V
    .locals 8

    const/16 v7, 0x14

    const/4 v6, -0x2

    const/high16 v5, 0x3f800000

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0}, Lcom/google/android/street/Renderer;->clearPancake()V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mHighlighter:Lcom/google/android/street/Highlighter;

    invoke-virtual {v0}, Lcom/google/android/street/Highlighter;->getCurrentlyPressedItem()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mHighlighter:Lcom/google/android/street/Highlighter;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/street/Highlighter;->up(FF)I

    move-result v1

    if-ne v1, v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04000c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne v0, v6, :cond_2

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-eqz v0, :cond_2

    const/high16 v0, 0x42860000

    iget v3, p0, Lcom/google/android/street/StreetView;->mScreenDensity:F

    mul-float/2addr v0, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v4, v0

    invoke-direct {p0, v3, v4}, Lcom/google/android/street/StreetView;->clickToGo(FF)Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-eqz v3, :cond_3

    invoke-direct {p0, v5}, Lcom/google/android/street/StreetView;->smoothZoom(F)V

    :cond_2
    :goto_1
    if-lt v1, v7, :cond_4

    sub-int v0, v1, v7

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v1, v1, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v1, v0}, Lcom/google/android/street/DepthMap;->getPanoId(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/street/StreetView;->loadPanorama(Ljava/lang/String;Lcom/google/android/street/MapPoint;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/MapPoint;Lcom/google/android/street/PanoramaConfig;)V

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->invalidate()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float v0, v4, v0

    invoke-direct {p0, v5, v3, v0}, Lcom/google/android/street/StreetView;->smoothZoom(FFF)V

    goto :goto_1

    :cond_4
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0, v1}, Lcom/google/android/street/Renderer;->getPanoramaLink(I)Lcom/google/android/street/PanoramaLink;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->goTo(Lcom/google/android/street/PanoramaLink;)V

    goto :goto_0
.end method

.method private formatStreetAddress(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    if-nez p1, :cond_1

    if-nez p2, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    move-object v0, p1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04000d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getClosestLink(F)Lcom/google/android/street/PanoramaLink;
    .locals 2
    .param p1    # F

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v1, p1}, Lcom/google/android/street/PanoramaConfig;->getClosestLink(F)Lcom/google/android/street/PanoramaLink;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private getLoadingStatus()Ljava/lang/CharSequence;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->wasNetworkUp()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f040006

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private getStatus(Lcom/google/android/street/StreetView$Status;)V
    .locals 2
    .param p1    # Lcom/google/android/street/StreetView$Status;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->wasNetworkUp()Z

    move-result v0

    iput-boolean v0, p1, Lcom/google/android/street/StreetView$Status;->mGotNetworkConnection:Z

    iput-boolean v1, p1, Lcom/google/android/street/StreetView$Status;->mGotPanoramaConfig:Z

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mBadPanorama:Z

    iput-boolean v0, p1, Lcom/google/android/street/StreetView$Status;->mBadPanorama:Z

    iput-boolean v1, p1, Lcom/google/android/street/StreetView$Status;->mThrottling:Z

    iput-boolean v1, p1, Lcom/google/android/street/StreetView$Status;->mDisabled:Z

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/google/android/street/StreetView$Status;->mGotPanoramaConfig:Z

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v0}, Lcom/google/android/street/PanoramaConfig;->someRequestsWillBeDenied()Z

    move-result v0

    iput-boolean v0, p1, Lcom/google/android/street/StreetView$Status;->mThrottling:Z

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-boolean v0, v0, Lcom/google/android/street/PanoramaConfig;->mDisabled:Z

    iput-boolean v0, p1, Lcom/google/android/street/StreetView$Status;->mDisabled:Z

    :cond_0
    return-void
.end method

.method private getStatusFromPanoramaConfig()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-boolean v0, v0, Lcom/google/android/street/PanoramaConfig;->mDisabled:Z

    if-eqz v0, :cond_1

    const v0, 0x7f040007

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v0}, Lcom/google/android/street/PanoramaConfig;->serviceTemporarilyUnavailable()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f040008

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mStreetRange:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v1, v1, Lcom/google/android/street/PanoramaConfig;->mText:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/StreetView;->formatStreetAddress(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getText(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;

    invoke-virtual {v0, p1}, Lcom/google/android/street/Street;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private goTo(Lcom/google/android/street/PanoramaLink;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/android/street/PanoramaLink;->mPanoId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/street/PanoramaLink;->mPanoId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040007

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const-string v0, "Panorama step"

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-static {v0, v1}, Lcom/google/android/street/Street;->noteStartFrame(Ljava/lang/String;Lcom/google/android/street/PanoramaConfig;)V

    iput-boolean v3, p0, Lcom/google/android/street/StreetView;->mBadPanorama:Z

    iput-object v2, p0, Lcom/google/android/street/StreetView;->mInitialPanoId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/street/StreetView;->mInitialPanoMapPoint:Lcom/google/android/street/MapPoint;

    iput-object p1, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SV step to panorama "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    iget-object v1, v1, Lcom/google/android/street/PanoramaLink;->mPanoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/street/StreetView;->mStatusOverride:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->clearDirectionsArrowParams()V

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->invalidate()V

    goto :goto_0
.end method

.method private nextZoomLevel()F
    .locals 3

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v1}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v0

    iget v1, p0, Lcom/google/android/street/StreetView;->mZoomLevels:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    const/high16 v1, 0x3f800000

    :goto_0
    return v1

    :cond_0
    neg-float v1, v0

    goto :goto_0
.end method

.method private okToAct()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onIndirectUp(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->finishTap(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private reportUserActivity()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserActivityReporter:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/street/StreetView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private final send(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, p1, p2, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private final send(IIILjava/lang/Object;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mHandler:Landroid/os/Handler;

    invoke-static {v1, p1, p2, p3, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setPanoramaConfig(Lcom/google/android/street/PanoramaConfig;)V
    .locals 3
    .param p1    # Lcom/google/android/street/PanoramaConfig;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0}, Lcom/google/android/street/DepthMap;->decompress()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/street/Renderer;->setPanoramaConfig(Lcom/google/android/street/PanoramaConfig;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfigCache:Lcom/google/android/street/PanoramaConfigCache;

    invoke-virtual {v0, p1}, Lcom/google/android/street/PanoramaConfigCache;->insert(Lcom/google/android/street/PanoramaConfig;)V

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->initUserOrientation()V

    iput-object v2, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    iput-object v2, p0, Lcom/google/android/street/StreetView;->mInitialPanoId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/street/StreetView;->mInitialPanoMapPoint:Lcom/google/android/street/MapPoint;

    :cond_2
    return-void
.end method

.method private setTangentRatios(FF)V
    .locals 6
    .param p1    # F
    .param p2    # F

    const/high16 v5, 0x40000000

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v3

    invoke-static {v3}, Lcom/google/android/street/StreetMath;->degreesToUnit(F)F

    move-result v1

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getTilt()F

    move-result v3

    div-float v0, v3, v5

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getScale()F

    move-result v2

    iput p1, p0, Lcom/google/android/street/StreetView;->mAnchorYaw:F

    iput p2, p0, Lcom/google/android/street/StreetView;->mAnchorPitch:F

    invoke-static {v1, p1}, Lcom/google/android/street/StreetMath;->angleSubtractUnit(FF)F

    move-result v3

    invoke-static {v3}, Lcom/google/android/street/StreetMath;->tanUnit(F)F

    move-result v3

    iget v4, p0, Lcom/google/android/street/StreetView;->mUnzoomedFovH:F

    div-float/2addr v4, v5

    invoke-static {v4}, Lcom/google/android/street/StreetMath;->degreesToUnit(F)F

    move-result v4

    mul-float/2addr v4, v2

    invoke-static {v4}, Lcom/google/android/street/StreetMath;->tanUnit(F)F

    move-result v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/street/StreetView;->mHorizontalTanRatio:F

    invoke-static {v0, p2}, Lcom/google/android/street/StreetMath;->angleSubtractUnit(FF)F

    move-result v3

    invoke-static {v3}, Lcom/google/android/street/StreetMath;->tanUnit(F)F

    move-result v3

    iget v4, p0, Lcom/google/android/street/StreetView;->mUnzoomedFovV:F

    div-float/2addr v4, v5

    invoke-static {v4}, Lcom/google/android/street/StreetMath;->degreesToUnit(F)F

    move-result v4

    mul-float/2addr v4, v2

    invoke-static {v4}, Lcom/google/android/street/StreetMath;->tanUnit(F)F

    move-result v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/street/StreetView;->mVerticalTanRatio:F

    return-void
.end method

.method private showZoomController(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0, p1}, Landroid/widget/ZoomButtonsController;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    goto :goto_0
.end method

.method private smoothZoom(F)V
    .locals 5
    .param p1    # F

    const/4 v0, 0x5

    const/high16 v3, 0x40a00000

    div-float v1, p1, v3

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x5

    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/street/StreetView;->zoom(F)V

    monitor-enter p0

    const-wide/16 v3, 0x1e

    :try_start_0
    invoke-virtual {p0, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    monitor-exit p0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :catch_0
    move-exception v3

    goto :goto_1

    :cond_0
    return-void
.end method

.method private smoothZoom(FFF)V
    .locals 6
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v4, p2, p3, v5}, Lcom/google/android/street/Renderer;->pixelToYawPitch(FFZ)[F

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x7

    const/high16 v4, 0x40e00000

    div-float v3, p1, v4

    aget v4, v1, v5

    const/4 v5, 0x1

    aget v5, v1, v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/street/StreetView;->setTangentRatios(FF)V

    const/4 v2, 0x0

    :goto_0
    const/4 v4, 0x7

    if-ge v2, v4, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/street/StreetView;->doZoom(F)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateOrientationForZoom()V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateRendererAndButtons()V

    const/4 v4, 0x6

    if-ge v2, v4, :cond_2

    monitor-enter p0

    const-wide/16 v4, 0x1e

    :try_start_0
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    monitor-exit p0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method private startFlinger(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mFlingerLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/street/StreetView;->mFlinger:Lcom/google/android/street/StreetView$Flinger;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mFlinger:Lcom/google/android/street/StreetView$Flinger;

    invoke-virtual {v2}, Lcom/google/android/street/StreetView$Flinger;->stop()V

    :cond_0
    new-instance v2, Lcom/google/android/street/StreetView$Flinger;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/street/StreetView$Flinger;-><init>(Lcom/google/android/street/StreetView;FF)V

    iput-object v2, p0, Lcom/google/android/street/StreetView;->mFlinger:Lcom/google/android/street/StreetView$Flinger;

    new-instance v0, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mFlinger:Lcom/google/android/street/StreetView$Flinger;

    const-string v3, "Flinger"

    invoke-direct {v0, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static tiltDegToHalfTurns(F)F
    .locals 2

    const/high16 v0, 0x3f000000

    const v1, 0x3bb60b61

    mul-float/2addr v1, p0

    sub-float/2addr v0, v1

    return v0
.end method

.method private updateButtonsEnabled()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v1}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    iget v2, p0, Lcom/google/android/street/StreetView;->mZoomLevels:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/ZoomButtonsController;->setZoomInEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_2

    move v2, v3

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/ZoomButtonsController;->setZoomOutEnabled(Z)V

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1

    :cond_2
    move v2, v4

    goto :goto_2
.end method

.method private updateDeviceOrientation()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateZoomLevels()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/street/StreetView;->zoom(F)V

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    :cond_0
    return-void
.end method

.method private updateOrientationForZoom()V
    .locals 7

    const/high16 v6, 0x40000000

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getScale()F

    move-result v2

    iget v3, p0, Lcom/google/android/street/StreetView;->mAnchorYaw:F

    iget v4, p0, Lcom/google/android/street/StreetView;->mHorizontalTanRatio:F

    iget v5, p0, Lcom/google/android/street/StreetView;->mUnzoomedFovH:F

    div-float/2addr v5, v6

    invoke-static {v5}, Lcom/google/android/street/StreetMath;->degreesToUnit(F)F

    move-result v5

    mul-float/2addr v5, v2

    invoke-static {v5}, Lcom/google/android/street/StreetMath;->tanUnit(F)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Lcom/google/android/street/StreetMath;->atanUnit(F)F

    move-result v4

    add-float v1, v3, v4

    iget v3, p0, Lcom/google/android/street/StreetView;->mAnchorPitch:F

    iget v4, p0, Lcom/google/android/street/StreetView;->mVerticalTanRatio:F

    iget v5, p0, Lcom/google/android/street/StreetView;->mUnzoomedFovV:F

    div-float/2addr v5, v6

    invoke-static {v5}, Lcom/google/android/street/StreetMath;->degreesToUnit(F)F

    move-result v5

    mul-float/2addr v5, v2

    invoke-static {v5}, Lcom/google/android/street/StreetMath;->tanUnit(F)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Lcom/google/android/street/StreetMath;->atanUnit(F)F

    move-result v4

    add-float v0, v3, v4

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-static {v1}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/street/UserOrientation;->setYaw(F)V

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    mul-float v4, v6, v0

    invoke-virtual {v3, v4}, Lcom/google/android/street/UserOrientation;->setTilt(F)V

    return-void
.end method

.method private updateRendererAndButtons()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateRendererUserOrientation()V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateButtonsEnabled()V

    return-void
.end method

.method private updateRendererUserOrientation()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    new-instance v1, Lcom/google/android/street/UserOrientation;

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-direct {v1, v2}, Lcom/google/android/street/UserOrientation;-><init>(Lcom/google/android/street/UserOrientation;)V

    invoke-virtual {v0, v1}, Lcom/google/android/street/Renderer;->setUserOrientation(Lcom/google/android/street/UserOrientation;)V

    :cond_0
    return-void
.end method

.method private updateStatusText()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->checkStatus()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;

    invoke-virtual {v2, v1}, Lcom/google/android/street/Street;->setStatusText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mCurrentStatus:Lcom/google/android/street/StreetView$Status;

    invoke-direct {p0, v2}, Lcom/google/android/street/StreetView;->getStatus(Lcom/google/android/street/StreetView$Status;)V

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mCurrentStatus:Lcom/google/android/street/StreetView$Status;

    invoke-virtual {v2}, Lcom/google/android/street/StreetView$Status;->getProgress()I

    move-result v0

    iget v2, p0, Lcom/google/android/street/StreetView;->mOldProgress:I

    if-eq v0, v2, :cond_0

    iput v0, p0, Lcom/google/android/street/StreetView;->mOldProgress:I

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;

    invoke-virtual {v2, v0}, Lcom/google/android/street/Street;->reportProgress(I)V

    :cond_0
    return-void
.end method

.method private updateZoomLevels()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/street/StreetView;->mAspectRatio:F

    iget v0, p0, Lcom/google/android/street/StreetView;->mAspectRatio:F

    invoke-static {v0}, Lcom/google/android/street/Renderer;->getUnzoomedVerticalFov(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/street/StreetView;->mUnzoomedFovV:F

    iget v0, p0, Lcom/google/android/street/StreetView;->mAspectRatio:F

    invoke-static {v0}, Lcom/google/android/street/Renderer;->getUnzoomedHorizontalFov(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/street/StreetView;->mUnzoomedFovH:F

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget v0, v0, Lcom/google/android/street/PanoramaConfig;->mImageHeight:I

    int-to-float v0, v0

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/street/StreetView;->mUnzoomedFovV:F

    mul-float/2addr v0, v1

    const v1, 0x3bb60b61

    mul-float/2addr v0, v1

    invoke-static {v0}, Lcom/google/android/street/StreetMath;->log2(F)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget v1, v1, Lcom/google/android/street/PanoramaConfig;->mNumZoomLevels:I

    const/4 v2, 0x0

    float-to-int v0, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/street/StreetView;->mZoomLevels:I

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    iget v1, p0, Lcom/google/android/street/StreetView;->mZoomLevels:I

    invoke-virtual {v0, v1}, Lcom/google/android/street/Renderer;->setZoomLevels(I)V

    :cond_0
    return-void
.end method

.method private wasNetworkUp()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;

    invoke-virtual {v0}, Lcom/google/android/street/Street;->wasNetworkAvailable()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public clearDirectionsArrowParams()V
    .locals 2

    const/high16 v1, -0x40800000

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/street/Renderer;->setDirectionsArrowParams(FF)V

    return-void
.end method

.method public clearStatusOverride()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStatusOverride:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mStatusOverride:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateStatusText()V

    :cond_0
    return-void
.end method

.method public getPanoramaConfig()Lcom/google/android/street/PanoramaConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    return-object v0
.end method

.method public getUserOrientation()Lcom/google/android/street/UserOrientation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRequestedUserOrientation:Lcom/google/android/street/UserOrientation;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    goto :goto_0
.end method

.method public goForward()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v1}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/street/StreetView;->getClosestLink(F)Lcom/google/android/street/PanoramaLink;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->goTo(Lcom/google/android/street/PanoramaLink;)V

    :cond_0
    return-void
.end method

.method public handleScaleEvent(Lcom/google/android/common/gesture/ScaleEvent;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {p1}, Lcom/google/android/common/gesture/ScaleEvent;->getFocusX()F

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/common/gesture/ScaleEvent;->getFocusY()F

    move-result v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/street/Renderer;->pixelToYawPitch(FFZ)[F

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    aget v1, v0, v3

    aget v0, v0, v4

    invoke-direct {p0, v1, v0}, Lcom/google/android/street/StreetView;->setTangentRatios(FF)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/common/gesture/ScaleEvent;->getCurrentSpan()F

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/common/gesture/ScaleEvent;->getPreviousSpan()F

    move-result v1

    sub-float/2addr v0, v1

    const/high16 v1, 0x43340000

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/street/StreetView;->mScreenDensity:F

    div-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->doZoom(F)V

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateOrientationForZoom()V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateRendererAndButtons()V

    move v0, v4

    goto :goto_0
.end method

.method public handleTwoFingerTap()V
    .locals 1

    const/high16 v0, -0x40800000

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->smoothZoom(F)V

    return-void
.end method

.method public hasPanorama()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initUserOrientation()V
    .locals 12

    const/4 v11, 0x0

    const/high16 v10, 0x43340000

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRequestedUserOrientation:Lcom/google/android/street/UserOrientation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRequestedUserOrientation:Lcom/google/android/street/UserOrientation;

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    iput-object v11, p0, Lcom/google/android/street/StreetView;->mRequestedUserOrientation:Lcom/google/android/street/UserOrientation;

    :goto_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRequestedFaceToLatLng:Lcom/google/android/street/MapPoint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mLatLng:Lcom/google/android/street/MapPoint;

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mRequestedFaceToLatLng:Lcom/google/android/street/MapPoint;

    invoke-virtual {v0, v1}, Lcom/google/android/street/MapPoint;->angleTo(Lcom/google/android/street/MapPoint;)F

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SV adjusting face: pano-latlng="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v2, v2, Lcom/google/android/street/PanoramaConfig;->mLatLng:Lcom/google/android/street/MapPoint;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " face-latlng="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mRequestedFaceToLatLng:Lcom/google/android/street/MapPoint;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " yaw="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    iput-object v11, p0, Lcom/google/android/street/StreetView;->mRequestedFaceToLatLng:Lcom/google/android/street/MapPoint;

    new-instance v1, Lcom/google/android/street/UserOrientation;

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v2}, Lcom/google/android/street/UserOrientation;->getTilt()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/street/UserOrientation;-><init>(FFF)V

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateZoomLevels()V

    invoke-direct {p0, v9, v9}, Lcom/google/android/street/StreetView;->doYawTilt(FF)V

    invoke-virtual {p0, v9}, Lcom/google/android/street/StreetView;->zoom(F)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    if-eqz v0, :cond_4

    const/high16 v0, 0x7f800000

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    iget v1, v1, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v2, v2, Lcom/google/android/street/PanoramaConfig;->mLinks:[Lcom/google/android/street/PanoramaLink;

    if-eqz v2, :cond_6

    array-length v3, v2

    const/4 v4, 0x0

    move v5, v0

    move v0, v4

    move v4, v9

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v6, v2, v0

    iget v6, v6, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    sub-float/2addr v6, v1

    add-float/2addr v6, v10

    const v7, 0x3b360b61

    mul-float/2addr v7, v6

    invoke-static {v7}, Landroid/util/FloatMath;->floor(F)F

    move-result v7

    const/high16 v8, 0x43b40000

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    sub-float/2addr v6, v10

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const/high16 v8, 0x41c80000

    cmpg-float v8, v7, v8

    if-gtz v8, :cond_2

    cmpg-float v8, v7, v5

    if-gtz v8, :cond_2

    move v4, v6

    move v5, v7

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v4

    :goto_2
    iget-object v1, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v1}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v1

    add-float/2addr v0, v1

    :goto_3
    new-instance v1, Lcom/google/android/street/UserOrientation;

    const/high16 v2, 0x3f000000

    iget-object v3, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/street/UserOrientation;-><init>(FFF)V

    iput-object v1, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget v0, v0, Lcom/google/android/street/PanoramaConfig;->mPanoYawDeg:F

    goto :goto_3

    :cond_5
    move v0, v9

    goto :goto_3

    :cond_6
    move v0, v9

    goto :goto_2
.end method

.method public initialize(Lcom/google/android/street/Street;Lcom/google/android/street/PanoramaManager;Z)V
    .locals 3
    .param p1    # Lcom/google/android/street/Street;
    .param p2    # Lcom/google/android/street/PanoramaManager;
    .param p3    # Z

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;

    iput-object p2, p0, Lcom/google/android/street/StreetView;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iput-boolean p3, p0, Lcom/google/android/street/StreetView;->mIsTablet:Z

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->createRenderer()V

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsEnabled:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/ZoomButtonsController;

    invoke-direct {v0, p0}, Landroid/widget/ZoomButtonsController;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    new-instance v1, Lcom/google/android/street/StreetView$3;

    invoke-direct {v1, p0}, Lcom/google/android/street/StreetView$3;-><init>(Lcom/google/android/street/StreetView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setOnZoomListener(Landroid/widget/ZoomButtonsController$OnZoomListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Lcom/google/android/street/Street;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.google.android.street.StreetView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "useSensorToControlView"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->enableSensor(Z)V

    return-void
.end method

.method public invalidate()V
    .locals 1

    invoke-super {p0}, Landroid/view/SurfaceView;->invalidate()V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateStatusText()V

    :cond_0
    return-void
.end method

.method public loadPanorama(Ljava/lang/String;Lcom/google/android/street/MapPoint;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/MapPoint;Lcom/google/android/street/PanoramaConfig;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/street/MapPoint;
    .param p3    # Lcom/google/android/street/UserOrientation;
    .param p4    # Lcom/google/android/street/MapPoint;
    .param p5    # Lcom/google/android/street/PanoramaConfig;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/StreetView;->mBadPanorama:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    iput-object p1, p0, Lcom/google/android/street/StreetView;->mInitialPanoId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/street/StreetView;->mInitialPanoMapPoint:Lcom/google/android/street/MapPoint;

    iput-object p3, p0, Lcom/google/android/street/StreetView;->mRequestedUserOrientation:Lcom/google/android/street/UserOrientation;

    iput-object p4, p0, Lcom/google/android/street/StreetView;->mRequestedFaceToLatLng:Lcom/google/android/street/MapPoint;

    invoke-direct {p0, p5}, Lcom/google/android/street/StreetView;->setPanoramaConfig(Lcom/google/android/street/PanoramaConfig;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    :cond_0
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x1

    const/high16 v2, 0x3f800000

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/StreetView;->clickToGo(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v0}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v0

    add-float/2addr v0, v2

    iget v1, p0, Lcom/google/android/street/StreetView;->mZoomLevels:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/google/android/street/StreetView;->smoothZoom(F)V

    :goto_1
    move v0, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/street/StreetView;->smoothZoom(FFF)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v0}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v0

    neg-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->smoothZoom(F)V

    goto :goto_1
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mFlingerLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/StreetView;->mFlinger:Lcom/google/android/street/StreetView$Flinger;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/street/StreetView;->mFlinger:Lcom/google/android/street/StreetView$Flinger;

    invoke-virtual {v1}, Lcom/google/android/street/StreetView$Flinger;->stop()V

    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0, v3}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/StreetView;->mHighlighter:Lcom/google/android/street/Highlighter;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Highlighter;->down(FF)Z

    return v3

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v5, 0x1

    const/high16 v4, 0x40200000

    const/high16 v3, -0x3fe00000

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mHighlighter:Lcom/google/android/street/Highlighter;

    invoke-virtual {v2}, Lcom/google/android/street/Highlighter;->isTracking()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-eqz v2, :cond_1

    move v2, v5

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/google/android/street/StreetView;->mLastDX:F

    invoke-static {v2, v3, v4}, Lcom/google/android/street/StreetMath;->clamp(FFF)F

    move-result v0

    iget v2, p0, Lcom/google/android/street/StreetView;->mLastDY:F

    invoke-static {v2, v3, v4}, Lcom/google/android/street/StreetMath;->clamp(FFF)F

    move-result v1

    const v2, 0x3f4ccccd

    mul-float/2addr v2, v0

    const/high16 v3, 0x3f000000

    mul-float/2addr v3, v1

    invoke-direct {p0, v2, v3}, Lcom/google/android/street/StreetView;->startFlinger(FF)V

    move v2, v5

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000

    const/high16 v5, -0x40800000

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->okToAct()Z

    move-result v2

    iget-boolean v4, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-nez v4, :cond_2

    const/4 v4, 0x1

    move v1, v4

    :goto_0
    const/4 v3, 0x0

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :cond_0
    :goto_1
    if-eqz v0, :cond_3

    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v4, v7}, Lcom/google/android/street/Renderer;->setMotionUse(I)V

    :cond_1
    :goto_2
    return v0

    :cond_2
    move v1, v7

    goto :goto_0

    :sswitch_0
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0, v6}, Lcom/google/android/street/StreetView;->tilt(F)V

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_2
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0, v5}, Lcom/google/android/street/StreetView;->tilt(F)V

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_3
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0, v5}, Lcom/google/android/street/StreetView;->yaw(F)V

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_4
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0, v6}, Lcom/google/android/street/StreetView;->yaw(F)V

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_5
    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->nextZoomLevel()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/street/StreetView;->zoom(F)V

    goto :goto_1

    :sswitch_6
    if-eqz v2, :cond_0

    invoke-virtual {p0, v5}, Lcom/google/android/street/StreetView;->zoom(F)V

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/google/android/street/StreetView;->tilt(F)V

    goto :goto_1

    :sswitch_7
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->initUserOrientation()V

    goto :goto_1

    :sswitch_8
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->toggleCompassMode()V

    goto :goto_1

    :cond_3
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
        0x15 -> :sswitch_3
        0x16 -> :sswitch_4
        0x17 -> :sswitch_5
        0x1f -> :sswitch_8
        0x23 -> :sswitch_6
        0x2d -> :sswitch_7
        0x30 -> :sswitch_5
        0x3e -> :sswitch_6
    .end sparse-switch
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0}, Lcom/google/android/street/Renderer;->renderingPause()V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->enableSensor(Z)V

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0}, Lcom/google/android/street/Renderer;->renderingResume()V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->enableSensor(Z)V

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v7, 0x1

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->okToAct()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v7

    :goto_0
    return v2

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/street/StreetView;->mZoomButtonsEnabled:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v2, v7}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/street/StreetView;->mHighlighter:Lcom/google/android/street/Highlighter;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/street/Highlighter;->move(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mHighlighter:Lcom/google/android/street/Highlighter;

    invoke-virtual {v2}, Lcom/google/android/street/Highlighter;->getCurrentlyPressedItem()I

    move-result v2

    const/4 v3, -0x2

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    const/high16 v5, 0x42860000

    iget v6, p0, Lcom/google/android/street/StreetView;->mScreenDensity:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/street/Renderer;->setPancake(FF)V

    :cond_2
    move v2, v7

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-eqz v2, :cond_4

    move v2, v7

    goto :goto_0

    :cond_4
    const v0, 0x3ca3d70a

    const v1, -0x43dc28f6

    const v2, 0x3ca3d70a

    mul-float/2addr v2, p3

    iput v2, p0, Lcom/google/android/street/StreetView;->mLastDX:F

    const v2, -0x43dc28f6

    mul-float/2addr v2, p4

    iput v2, p0, Lcom/google/android/street/StreetView;->mLastDY:F

    iget v2, p0, Lcom/google/android/street/StreetView;->mUnzoomedFovH:F

    mul-float/2addr v2, p3

    const/high16 v3, 0x41200000

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/street/StreetView;->mUnzoomedFovV:F

    mul-float/2addr v3, p4

    const/high16 v4, -0x3e4c0000

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/street/StreetView;->yawTilt(FF)V

    move v2, v7

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->finishTap(Landroid/view/MotionEvent;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/SurfaceView;->onSizeChanged(IIII)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateDeviceOrientation()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->okToAct()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0, v1}, Lcom/google/android/street/Renderer;->setMotionUse(I)V

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mStreetViewGestureController:Lcom/google/android/common/gesture/StreetViewGestureController;

    invoke-virtual {v0, p1}, Lcom/google/android/common/gesture/StreetViewGestureController;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->onIndirectUp(Landroid/view/MotionEvent;)V

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    const/4 v8, 0x0

    const v11, 0x3e4ccccd

    const/4 v10, 0x0

    const/4 v9, 0x1

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->okToAct()Z

    move-result v7

    if-nez v7, :cond_0

    move v7, v9

    :goto_0
    return v7

    :cond_0
    iget-boolean v7, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-nez v7, :cond_2

    move v2, v9

    :goto_1
    iget-object v7, p0, Lcom/google/android/street/StreetView;->mTrackballGestureDetector:Lcom/google/android/street/TrackballGestureDetector;

    invoke-virtual {v7, p1}, Lcom/google/android/street/TrackballGestureDetector;->analyze(Landroid/view/MotionEvent;)V

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v7, v8}, Lcom/google/android/street/Renderer;->setMotionUse(I)V

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mTrackballGestureDetector:Lcom/google/android/street/TrackballGestureDetector;

    invoke-virtual {v7}, Lcom/google/android/street/TrackballGestureDetector;->isScroll()Z

    move-result v7

    if-eqz v7, :cond_3

    if-eqz v2, :cond_3

    const/high16 v0, 0x40a00000

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    const/high16 v8, 0x40a00000

    mul-float v5, v7, v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    const/high16 v8, -0x3fe00000

    mul-float v3, v7, v8

    cmpl-float v7, v5, v10

    if-nez v7, :cond_1

    cmpl-float v7, v3, v10

    if-eqz v7, :cond_4

    :cond_1
    mul-float v6, v11, v5

    mul-float v4, v11, v3

    const/4 v1, 0x0

    :goto_2
    const/4 v7, 0x5

    if-ge v1, v7, :cond_4

    invoke-direct {p0, v6, v4}, Lcom/google/android/street/StreetView;->doYawTilt(FF)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateRendererUserOrientation()V

    monitor-enter p0

    const-wide/16 v7, 0x1e

    :try_start_0
    invoke-virtual {p0, v7, v8}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_3
    :try_start_1
    monitor-exit p0

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    move v2, v8

    goto :goto_1

    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    :cond_3
    iget-object v7, p0, Lcom/google/android/street/StreetView;->mTrackballGestureDetector:Lcom/google/android/street/TrackballGestureDetector;

    invoke-virtual {v7}, Lcom/google/android/street/TrackballGestureDetector;->isTap()Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v7}, Lcom/google/android/street/UserOrientation;->getZoom()F

    move-result v7

    cmpl-float v7, v7, v10

    if-nez v7, :cond_5

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->goForward()V

    :cond_4
    :goto_4
    move v7, v9

    goto :goto_0

    :cond_5
    iget-boolean v7, p0, Lcom/google/android/street/StreetView;->mZoomButtonsEnabled:Z

    if-eqz v7, :cond_4

    invoke-direct {p0, v9}, Lcom/google/android/street/StreetView;->showZoomController(Z)V

    goto :goto_4

    :catch_0
    move-exception v7

    goto :goto_3
.end method

.method public postPanoramaInfo(ZLcom/google/android/street/PanoramaConfig;)V
    .locals 2
    .param p1    # Z
    .param p2    # Lcom/google/android/street/PanoramaConfig;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0, v1, p2}, Lcom/google/android/street/StreetView;->send(IIILjava/lang/Object;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public reloadPanorama()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaLink:Lcom/google/android/street/PanoramaLink;

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mInitialPanoId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/street/StreetView;->mInitialPanoMapPoint:Lcom/google/android/street/MapPoint;

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->setPanoramaConfig(Lcom/google/android/street/PanoramaConfig;)V

    return-void
.end method

.method public reportRenderStatusProgress(I)V
    .locals 7
    .param p1    # I

    const-wide v1, 0x409f400000000000L

    const-wide v3, 0x3fe999999999999aL

    int-to-double v5, p1

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-int v0, v1

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/street/StreetView;->send(II)V

    return-void
.end method

.method public reportTransitionProgress(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/google/android/street/StreetView;->send(II)V

    return-void
.end method

.method public setDirectionsArrowParams(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/street/StreetView;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/street/Renderer;->setDirectionsArrowParams(FF)V

    return-void
.end method

.method public setStatusOverride(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/street/StreetView;->mStatusOverride:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateStatusText()V

    return-void
.end method

.method public setTimer(Lcom/google/android/street/Timer;)V
    .locals 0
    .param p1    # Lcom/google/android/street/Timer;

    return-void
.end method

.method public declared-synchronized tilt(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->doTilt(F)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateRendererUserOrientation()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toggleCompassMode()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/street/StreetView;->toggleCompassMode(ZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggleCompassMode(ZZ)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-eqz p2, :cond_1

    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    if-eqz v0, :cond_2

    const v0, 0x7f040004

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    invoke-direct {p0, v0}, Lcom/google/android/street/StreetView;->enableSensor(Z)V

    invoke-virtual {p0}, Lcom/google/android/street/StreetView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.google.android.street.StreetView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "useSensorToControlView"

    iget-boolean v2, p0, Lcom/google/android/street/StreetView;->mUseSensorToControlView:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_2
    const v0, 0x7f040005

    goto :goto_1
.end method

.method public declared-synchronized yaw(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->doYaw(F)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateRendererUserOrientation()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized yawTilt(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/street/StreetView;->doYawTilt(FF)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateRendererUserOrientation()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public zoom(F)V
    .locals 0
    .param p1    # F

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView;->doZoom(F)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView;->updateRendererAndButtons()V

    return-void
.end method
