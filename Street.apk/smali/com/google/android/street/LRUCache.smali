.class public Lcom/google/android/street/LRUCache;
.super Ljava/lang/Object;
.source "LRUCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/street/LRUCache$LRUCacheEntry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key:",
        "Ljava/lang/Object;",
        "Value:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TKey;",
            "Lcom/google/android/street/LRUCache$LRUCacheEntry",
            "<TKey;TValue;>;>;"
        }
    .end annotation
.end field

.field protected final mMaxEntries:I

.field private mOldest:Lcom/google/android/street/LRUCache$LRUCacheEntry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/street/LRUCache$LRUCacheEntry",
            "<TKey;TValue;>;"
        }
    .end annotation
.end field

.field private mYoungest:Lcom/google/android/street/LRUCache$LRUCacheEntry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/street/LRUCache$LRUCacheEntry",
            "<TKey;TValue;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/LRUCache;->mMap:Ljava/util/HashMap;

    iput p1, p0, Lcom/google/android/street/LRUCache;->mMaxEntries:I

    return-void
.end method

.method private link(Lcom/google/android/street/LRUCache$LRUCacheEntry;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/street/LRUCache$LRUCacheEntry",
            "<TKey;TValue;>;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/street/LRUCache;->mYoungest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    if-nez v1, :cond_0

    iput-object p1, p0, Lcom/google/android/street/LRUCache;->mOldest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    iput-object p1, p0, Lcom/google/android/street/LRUCache;->mYoungest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/LRUCache;->mYoungest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    iput-object v0, p1, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mOlder:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    iput-object p1, v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mYounger:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    iput-object p1, p0, Lcom/google/android/street/LRUCache;->mYoungest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    goto :goto_0
.end method

.method private unlink(Lcom/google/android/street/LRUCache$LRUCacheEntry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/street/LRUCache$LRUCacheEntry",
            "<TKey;TValue;>;)V"
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p1, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mOlder:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    iget-object v1, p1, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mYounger:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    if-eqz v0, :cond_0

    iput-object v1, v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mYounger:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    :cond_0
    if-eqz v1, :cond_1

    iput-object v0, v1, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mOlder:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    :cond_1
    iput-object v2, p1, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mOlder:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    iput-object v2, p1, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mYounger:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    iget-object v2, p0, Lcom/google/android/street/LRUCache;->mOldest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    if-ne v2, p1, :cond_2

    iput-object v1, p0, Lcom/google/android/street/LRUCache;->mOldest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    :cond_2
    iget-object v2, p0, Lcom/google/android/street/LRUCache;->mYoungest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    if-ne v2, p1, :cond_3

    iput-object v0, p0, Lcom/google/android/street/LRUCache;->mYoungest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    :cond_3
    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/street/LRUCache;->trimTo(I)V

    return-void
.end method

.method protected ensureSpaceForInsertion()V
    .locals 2

    iget v0, p0, Lcom/google/android/street/LRUCache;->mMaxEntries:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/street/LRUCache;->trimTo(I)V

    return-void
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)TValue;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/street/LRUCache;->mMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/street/LRUCache;->unlink(Lcom/google/android/street/LRUCache$LRUCacheEntry;)V

    invoke-direct {p0, v0}, Lcom/google/android/street/LRUCache;->link(Lcom/google/android/street/LRUCache$LRUCacheEntry;)V

    iget-object v1, v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mValue:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final insert(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;TValue;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/street/LRUCache;->mMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/street/LRUCache$LRUCacheEntry;

    if-nez v1, :cond_0

    iget v2, p0, Lcom/google/android/street/LRUCache;->mMaxEntries:I

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/google/android/street/LRUCache;->trimTo(I)V

    :cond_0
    new-instance v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;

    invoke-direct {v0}, Lcom/google/android/street/LRUCache$LRUCacheEntry;-><init>()V

    iput-object p2, v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mValue:Ljava/lang/Object;

    iput-object p1, v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mKey:Ljava/lang/Object;

    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/street/LRUCache;->unlink(Lcom/google/android/street/LRUCache$LRUCacheEntry;)V

    iget-object v2, v1, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mValue:Ljava/lang/Object;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/street/LRUCache;->onEject(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v2, v1, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mValue:Ljava/lang/Object;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/street/LRUCache;->onSuperseded(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/street/LRUCache;->mMap:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mKey:Ljava/lang/Object;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/google/android/street/LRUCache;->link(Lcom/google/android/street/LRUCache$LRUCacheEntry;)V

    return-void
.end method

.method protected onEject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;TValue;)V"
        }
    .end annotation

    return-void
.end method

.method protected onSuperseded(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;TValue;)V"
        }
    .end annotation

    return-void
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)TValue;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/street/LRUCache;->mMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/street/LRUCache;->unlink(Lcom/google/android/street/LRUCache$LRUCacheEntry;)V

    iget-object v1, v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mValue:Ljava/lang/Object;

    invoke-virtual {p0, p1, v1}, Lcom/google/android/street/LRUCache;->onEject(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mValue:Ljava/lang/Object;

    goto :goto_0
.end method

.method public trimTo(I)V
    .locals 3
    .param p1    # I

    :goto_0
    iget-object v2, p0, Lcom/google/android/street/LRUCache;->mMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-le v2, p1, :cond_0

    iget-object v2, p0, Lcom/google/android/street/LRUCache;->mOldest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    iget-object v0, v2, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mKey:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/street/LRUCache;->mOldest:Lcom/google/android/street/LRUCache$LRUCacheEntry;

    iget-object v2, v2, Lcom/google/android/street/LRUCache$LRUCacheEntry;->mKey:Ljava/lang/Object;

    invoke-virtual {p0, v2}, Lcom/google/android/street/LRUCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/street/LRUCache;->onSuperseded(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method
