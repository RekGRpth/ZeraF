.class public Lcom/google/android/common/datarequest/ConfigAndDrdUtil;
.super Ljava/lang/Object;
.source "ConfigAndDrdUtil.java"


# static fields
.field private static sAppVersionName:Ljava/lang/String;

.field private static sIsInitialized:Z

.field private static sLogTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "DRD"

    sput-object v0, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sLogTag:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sIsInitialized:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized cleanupConfigAndDrd()V
    .locals 3

    const-class v1, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;

    monitor-enter v1

    :try_start_0
    sget-boolean v2, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sIsInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->getInstance()Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->stop()V

    invoke-static {}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->clearInstance()V

    :cond_1
    const/4 v2, 0x0

    sput-boolean v2, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sIsInitialized:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method private static createDataRequestDispatcher(Landroid/content/Context;Ljava/lang/String;)Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const-string v1, "http://mobilemaps.clients.google.com/glm/mmap"

    invoke-static {}, Lcom/google/mobile/googlenav/common/Config;->getPlatformID()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->getAppVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/google/mobile/googlenav/common/Config;->getDistributionChannel()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->createInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "logging_id2"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->setAndroidLoggingId2(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->setGzipEnabled(Z)V

    const-string v1, "SYSTEM"

    invoke-virtual {v0, v1}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->setAndroidSignature(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->setApplicationName(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v0, v1}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->setScreenPixelDensity(I)V

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v1, v2}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->setMaxNetworkErrorRetryTimeout(J)V

    return-object v0
.end method

.method public static declared-synchronized getAppVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sAppVersionName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    invoke-static {p0}, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->getPackageInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v2, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sAppVersionName:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    sget-object v2, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sAppVersionName:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v2

    :catch_0
    move-exception v2

    move-object v0, v2

    :try_start_3
    sget-object v2, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sLogTag:Ljava/lang/String;

    const-string v3, "Couldn\'t get the PackageInfo"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v2, "1.5.0"

    sput-object v2, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sAppVersionName:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public static getPackageInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;
    .locals 3
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized setupConfigAndDrd(Landroid/content/Context;Ljava/lang/String;Lcom/google/mobile/googlenav/datarequest/DataRequestListener;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/mobile/googlenav/datarequest/DataRequestListener;

    const-class v1, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;

    monitor-enter v1

    :try_start_0
    sget-boolean v2, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sIsInitialized:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->updateConfig()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    sput-object p1, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sLogTag:Ljava/lang/String;

    invoke-static {}, Lcom/google/mobile/googlenav/common/Config;->getInstance()Lcom/google/mobile/googlenav/common/Config;

    move-result-object v2

    if-nez v2, :cond_3

    new-instance v2, Lcom/google/mobile/googlenav/common/Config;

    invoke-direct {v2, p0}, Lcom/google/mobile/googlenav/common/Config;-><init>(Landroid/content/Context;)V

    invoke-static {v2}, Lcom/google/mobile/googlenav/common/Config;->setConfig(Lcom/google/mobile/googlenav/common/Config;)V

    :goto_1
    invoke-static {}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->getInstance()Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {p0, p1}, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->createDataRequestDispatcher(Landroid/content/Context;Ljava/lang/String;)Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;

    move-result-object v0

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v0, p2}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->addDataRequestListener(Lcom/google/mobile/googlenav/datarequest/DataRequestListener;)V

    :cond_2
    invoke-virtual {v0}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->start()V

    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->sIsInitialized:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1

    throw v2

    :cond_3
    :try_start_2
    invoke-static {}, Lcom/google/android/common/datarequest/ConfigAndDrdUtil;->updateConfig()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private static updateConfig()V
    .locals 2

    invoke-static {}, Lcom/google/mobile/googlenav/common/Config;->getInstance()Lcom/google/mobile/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/mobile/googlenav/common/Config;->getInstance()Lcom/google/mobile/googlenav/common/Config;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/mobile/googlenav/common/Config;->initLocale(Ljava/util/Locale;)V

    :cond_0
    return-void
.end method
