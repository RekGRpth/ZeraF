.class final Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;
.super Landroid/os/Handler;
.source "PhoneStatesMgrService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/CellConnService/PhoneStatesMgrService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;


# direct methods
.method private constructor <init>(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/CellConnService/PhoneStatesMgrService;
    .param p2    # Lcom/mediatek/CellConnService/PhoneStatesMgrService$1;

    invoke-direct {p0, p1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;-><init>(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1    # Landroid/os/Message;

    const/16 v9, 0x192

    const/16 v8, 0x132

    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage: process incoming message"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$800(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$800(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "PhoneStatesMgrService"

    const-string v4, "handleMessage no item to handle"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v3

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$800(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage mICallBackList the first item is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_1
    invoke-virtual {v1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v0

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eq v2, v0, :cond_2

    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage: the handleSlot is not match the msg.args"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage: check radio off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)[I

    move-result-object v2

    aget v2, v2, v0

    if-eq v5, v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;I)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage: radio is off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)[I

    move-result-object v2

    aput v5, v2, v0

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2, v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$102(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Z)Z

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x191

    invoke-static {v2, v1, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)[I

    move-result-object v2

    const/4 v3, 0x2

    aput v3, v2, v0

    :pswitch_2
    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage: check PIN1 state"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)[I

    move-result-object v3

    aget v3, v3, v0

    if-eq v2, v3, :cond_5

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-virtual {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->pinRequest(I)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_5
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$100(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2, v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$102(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Z)Z

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2, v1, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_6
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x1f5

    invoke-static {v2, v1, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1600(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_7
    :pswitch_3
    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage: check PUK1 state"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-virtual {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->pukRequest(I)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1700(Lcom/mediatek/CellConnService/PhoneStatesMgrService;I)I

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage PUK no retry"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x196

    invoke-static {v2, v1, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_8
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)[I

    move-result-object v3

    aget v3, v3, v0

    if-eq v2, v3, :cond_9

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-virtual {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->pukRequest(I)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_9
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$100(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2, v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$102(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Z)Z

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2, v1, v9}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_a
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x1f6

    invoke-static {v2, v1, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1600(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_b
    :pswitch_4
    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage: check SIM-ME lock state"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-virtual {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->simMELockRequest(I)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1800(Lcom/mediatek/CellConnService/PhoneStatesMgrService;I)I

    move-result v2

    if-nez v2, :cond_c

    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage SIM-ME lock no retry"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x197

    invoke-static {v2, v1, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_c
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)[I

    move-result-object v3

    aget v3, v3, v0

    if-eq v2, v3, :cond_d

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-virtual {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->simMELockRequest(I)Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_d
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$100(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2, v5}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$102(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Z)Z

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x193

    invoke-static {v2, v1, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x1f7

    invoke-static {v2, v1, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1600(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_f
    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage check SIM-ME lock phone state is normal"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)[I

    move-result-object v2

    aput v6, v2, v0

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x12e

    invoke-static {v2, v0, v3, v7}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;III)V

    goto/16 :goto_0

    :pswitch_5
    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage: check MSG_ID_CHECKFDN state"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-virtual {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->fdnRequest(I)Z

    move-result v2

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x194

    invoke-static {v2, v1, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)[I

    move-result-object v2

    aget v3, v2, v0

    const v4, -0xf001

    and-int/2addr v3, v4

    aput v3, v2, v0

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x130

    invoke-static {v2, v0, v3, v6}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;III)V

    goto/16 :goto_0

    :pswitch_6
    const-string v2, "PhoneStatesMgrService"

    const-string v3, "handleMessage: check roaming"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-virtual {v2, v0}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->roamingRequest(I)Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    const/16 v3, 0x195

    invoke-static {v2, v1, v3}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1500(Lcom/mediatek/CellConnService/PhoneStatesMgrService;Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;I)V

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v2}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$700(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :pswitch_7
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-virtual {v1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v3

    invoke-static {v2, v3, v8, v7}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;III)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v2, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-virtual {v1}, Lcom/mediatek/CellConnService/PhoneStatesMgrService$RequestItem;->getReqSlot()I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/CellConnService/PhoneStatesMgrService$ServiceHandler;->this$0:Lcom/mediatek/CellConnService/PhoneStatesMgrService;

    invoke-static {v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$700(Lcom/mediatek/CellConnService/PhoneStatesMgrService;)I

    move-result v4

    invoke-static {v2, v3, v8, v4}, Lcom/mediatek/CellConnService/PhoneStatesMgrService;->access$1400(Lcom/mediatek/CellConnService/PhoneStatesMgrService;III)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
