.class public Lcom/android/dreams/basic/PreviewStubActivity;
.super Landroid/app/Activity;
.source "PreviewStubActivity.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field private mRenderer:Lcom/android/dreams/basic/ColorsGLRenderer;

.field private mRendererHandler:Landroid/os/Handler;

.field private mRendererHandlerThread:Landroid/os/HandlerThread;

.field private mTextureView:Landroid/view/TextureView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/dreams/basic/PreviewStubActivity;)Lcom/android/dreams/basic/ColorsGLRenderer;
    .locals 1
    .param p0    # Lcom/android/dreams/basic/PreviewStubActivity;

    iget-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRenderer:Lcom/android/dreams/basic/ColorsGLRenderer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/dreams/basic/PreviewStubActivity;Lcom/android/dreams/basic/ColorsGLRenderer;)Lcom/android/dreams/basic/ColorsGLRenderer;
    .locals 0
    .param p0    # Lcom/android/dreams/basic/PreviewStubActivity;
    .param p1    # Lcom/android/dreams/basic/ColorsGLRenderer;

    iput-object p1, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRenderer:Lcom/android/dreams/basic/ColorsGLRenderer;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/dreams/basic/PreviewStubActivity;)Landroid/os/HandlerThread;
    .locals 1
    .param p0    # Lcom/android/dreams/basic/PreviewStubActivity;

    iget-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandlerThread:Landroid/os/HandlerThread;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/view/TextureView;

    invoke-direct {v0, p0}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mTextureView:Landroid/view/TextureView;

    iget-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iget-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandlerThread:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandlerThread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandler:Landroid/os/Handler;

    :cond_0
    iget-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 4
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    const-string v0, "onSurfaceTextureAvailable(%s, %d, %d)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/android/dreams/basic/Colors;->LOG(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/dreams/basic/PreviewStubActivity$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/dreams/basic/PreviewStubActivity$1;-><init>(Lcom/android/dreams/basic/PreviewStubActivity;Landroid/graphics/SurfaceTexture;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 5
    .param p1    # Landroid/graphics/SurfaceTexture;

    const/4 v4, 0x1

    const-string v1, "onSurfaceTextureDestroyed(%s)"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/android/dreams/basic/Colors;->LOG(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/dreams/basic/PreviewStubActivity$3;

    invoke-direct {v2, p0}, Lcom/android/dreams/basic/PreviewStubActivity$3;-><init>(Lcom/android/dreams/basic/PreviewStubActivity;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    iget-object v1, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 4
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    const-string v0, "onSurfaceTextureSizeChanged(%s, %d, %d)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/android/dreams/basic/Colors;->LOG(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/dreams/basic/PreviewStubActivity;->mRendererHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/dreams/basic/PreviewStubActivity$2;

    invoke-direct {v1, p0, p2, p3}, Lcom/android/dreams/basic/PreviewStubActivity$2;-><init>(Lcom/android/dreams/basic/PreviewStubActivity;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 3
    .param p1    # Landroid/graphics/SurfaceTexture;

    const-string v0, "onSurfaceTextureUpdated(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/android/dreams/basic/Colors;->LOG(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
