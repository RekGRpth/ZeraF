.class public Ljava/util/concurrent/ForkJoinWorkerThread;
.super Ljava/lang/Thread;
.source "ForkJoinWorkerThread.java"


# static fields
.field private static final ABASE:J

.field private static final ASHIFT:I

.field private static final INITIAL_QUEUE_CAPACITY:I = 0x2000

.field private static final MAXIMUM_QUEUE_CAPACITY:I = 0x1000000

.field private static final MAX_HELP:I = 0x10

.field private static final SMASK:I = 0xffff

.field private static final UNSAFE:Lsun/misc/Unsafe;


# instance fields
.field currentJoin:Ljava/util/concurrent/ForkJoinTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation
.end field

.field currentSteal:Ljava/util/concurrent/ForkJoinTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation
.end field

.field volatile eventCount:I

.field final locallyFifo:Z

.field nextWait:I

.field volatile parked:Z

.field final pool:Ljava/util/concurrent/ForkJoinPool;

.field final poolIndex:I

.field queue:[Ljava/util/concurrent/ForkJoinTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation
.end field

.field volatile queueBase:I

.field queueTop:I

.field seed:I

.field stealCount:I

.field stealHint:I

.field volatile terminate:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    :try_start_0
    invoke-static {}, Lsun/misc/Unsafe;->getUnsafe()Lsun/misc/Unsafe;

    move-result-object v3

    sput-object v3, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    const-class v0, [Ljava/util/concurrent/ForkJoinTask;

    sget-object v3, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v3, v0}, Lsun/misc/Unsafe;->arrayBaseOffset(Ljava/lang/Class;)I

    move-result v3

    int-to-long v3, v3

    sput-wide v3, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    sget-object v3, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v3, v0}, Lsun/misc/Unsafe;->arrayIndexScale(Ljava/lang/Class;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    add-int/lit8 v3, v2, -0x1

    and-int/2addr v3, v2

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/Error;

    const-string v4, "data type scale not a power of two"

    invoke-direct {v3, v4}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_0
    move-exception v1

    new-instance v3, Ljava/lang/Error;

    invoke-direct {v3, v1}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v3

    rsub-int/lit8 v3, v3, 0x1f

    sput v3, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    return-void
.end method

.method protected constructor <init>(Ljava/util/concurrent/ForkJoinPool;)V
    .locals 4
    .param p1    # Ljava/util/concurrent/ForkJoinPool;

    invoke-virtual {p1}, Ljava/util/concurrent/ForkJoinPool;->nextWorkerName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    invoke-virtual {p1, p0}, Ljava/util/concurrent/ForkJoinPool;->registerWorker(Ljava/util/concurrent/ForkJoinWorkerThread;)I

    move-result v0

    iput v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->poolIndex:I

    xor-int/lit8 v2, v0, -0x1

    const v3, 0xffff

    and-int/2addr v2, v3

    iput v2, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    iget-boolean v2, p1, Ljava/util/concurrent/ForkJoinPool;->locallyFifo:Z

    iput-boolean v2, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->locallyFifo:Z

    iget-object v1, p1, Ljava/util/concurrent/ForkJoinPool;->ueh:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/Thread;->setDaemon(Z)V

    return-void
.end method

.method private static final casSlotNull([Ljava/util/concurrent/ForkJoinTask;ILjava/util/concurrent/ForkJoinTask;)Z
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;I",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)Z"
        }
    .end annotation

    sget-object v0, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    sget v1, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int v1, p1, v1

    int-to-long v1, v1

    sget-wide v3, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long v2, v1, v3

    const/4 v5, 0x0

    move-object v1, p0

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private growQueue()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v2, :cond_0

    array-length v1, v2

    shl-int/lit8 v11, v1, 0x1

    :goto_0
    const/high16 v1, 0x1000000

    if-le v11, v1, :cond_1

    new-instance v1, Ljava/util/concurrent/RejectedExecutionException;

    const-string v6, "Queue capacity exceeded"

    invoke-direct {v1, v6}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/16 v11, 0x2000

    goto :goto_0

    :cond_1
    const/16 v1, 0x2000

    if-ge v11, v1, :cond_2

    const/16 v11, 0x2000

    :cond_2
    new-array v10, v11, [Ljava/util/concurrent/ForkJoinTask;

    move-object/from16 v0, p0

    iput-object v10, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    add-int/lit8 v8, v11, -0x1

    move-object/from16 v0, p0

    iget v12, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eqz v2, :cond_4

    array-length v1, v2

    add-int/lit8 v9, v1, -0x1

    if-ltz v9, :cond_4

    move-object/from16 v0, p0

    iget v7, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    :goto_1
    if-eq v7, v12, :cond_4

    and-int v1, v7, v9

    sget v6, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int/2addr v1, v6

    int-to-long v13, v1

    sget-wide v15, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long v3, v13, v15

    sget-object v1, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v1, v2, v3, v4}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_3

    sget-object v1, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    and-int v6, v7, v8

    sget v13, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int/2addr v6, v13

    int-to-long v13, v6

    sget-wide v15, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long/2addr v13, v15

    invoke-virtual {v1, v10, v13, v14, v5}, Lsun/misc/Unsafe;->putObjectVolatile(Ljava/lang/Object;JLjava/lang/Object;)V

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method

.method private helpJoinTask(Ljava/util/concurrent/ForkJoinTask;)Z
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)Z"
        }
    .end annotation

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    iget v2, v2, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    const v7, 0xffff

    and-int v13, v2, v7

    move-object/from16 v0, p0

    iget-object v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    iget-object v0, v2, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    move-object/from16 v0, v19

    array-length v2, v0

    if-le v2, v13, :cond_3

    move-object/from16 v0, p1

    iget v2, v0, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v2, :cond_3

    const/16 v12, 0x10

    move-object/from16 v16, p1

    move-object/from16 v17, p0

    :goto_0
    move-object/from16 v0, v17

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->stealHint:I

    and-int/2addr v2, v13

    aget-object v18, v19, v2

    if-eqz v18, :cond_0

    move-object/from16 v0, v18

    iget-object v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    move-object/from16 v0, v16

    if-eq v2, v0, :cond_2

    :cond_0
    const/4 v11, 0x0

    :cond_1
    aget-object v18, v19, v11

    if-eqz v18, :cond_4

    move-object/from16 v0, v18

    iget-object v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    move-object/from16 v0, v16

    if-ne v2, v0, :cond_4

    move-object/from16 v0, v17

    iput v11, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->stealHint:I

    :cond_2
    :goto_1
    move-object/from16 v0, p1

    iget v2, v0, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-gez v2, :cond_5

    :cond_3
    :goto_2
    return v9

    :cond_4
    add-int/lit8 v11, v11, 0x1

    if-le v11, v13, :cond_1

    goto :goto_2

    :cond_5
    move-object/from16 v0, v18

    iget v8, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    move-object/from16 v0, v18

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eq v8, v2, :cond_6

    move-object/from16 v0, v18

    iget-object v3, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v3, :cond_6

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    and-int v10, v2, v8

    if-gez v10, :cond_7

    :cond_6
    move-object/from16 v0, v18

    iget-object v14, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentJoin:Ljava/util/concurrent/ForkJoinTask;

    add-int/lit8 v12, v12, -0x1

    if-lez v12, :cond_3

    move-object/from16 v0, v16

    iget v2, v0, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v2, :cond_3

    if-eqz v14, :cond_3

    move-object/from16 v0, v16

    if-eq v14, v0, :cond_3

    move-object/from16 v16, v14

    move-object/from16 v17, v18

    goto :goto_0

    :cond_7
    sget v2, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int v2, v10, v2

    int-to-long v0, v2

    move-wide/from16 v20, v0

    sget-wide v22, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long v4, v20, v22

    aget-object v6, v3, v10

    move-object/from16 v0, v16

    iget v2, v0, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v2, :cond_3

    if-eqz v6, :cond_2

    move-object/from16 v0, v18

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-ne v2, v8, :cond_2

    sget-object v2, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v2, v8, 0x1

    move-object/from16 v0, v18

    iput v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->poolIndex:I

    move-object/from16 v0, v18

    iput v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->stealHint:I

    move-object/from16 v0, p0

    iget-object v15, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    move-object/from16 v0, p0

    iput-object v6, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    invoke-virtual {v6}, Ljava/util/concurrent/ForkJoinTask;->doExec()V

    move-object/from16 v0, p0

    iput-object v15, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    const/4 v9, 0x1

    goto :goto_1
.end method

.method private localHelpJoinTask(Ljava/util/concurrent/ForkJoinTask;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)Z"
        }
    .end annotation

    iget v7, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-eq v7, v0, :cond_1

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v7, v7, -0x1

    and-int v6, v0, v7

    if-ltz v6, :cond_1

    aget-object v4, v1, v6

    if-eqz v4, :cond_1

    if-eq v4, p1, :cond_0

    iget v0, v4, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    sget v2, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int v2, v6, v2

    int-to-long v2, v2

    sget-wide v8, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long/2addr v2, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput v7, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    invoke-virtual {v4}, Ljava/util/concurrent/ForkJoinTask;->doExec()V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private nextSeed()I
    .locals 2

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->seed:I

    shl-int/lit8 v1, v0, 0xd

    xor-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x11

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x5

    xor-int/2addr v0, v1

    iput v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->seed:I

    return v0
.end method

.method private popTask()Ljava/util/concurrent/ForkJoinTask;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_1

    array-length v0, v1

    add-int/lit8 v7, v0, -0x1

    if-ltz v7, :cond_1

    :cond_0
    iget v8, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-eq v8, v0, :cond_1

    add-int/lit8 v8, v8, -0x1

    and-int v6, v7, v8

    sget v0, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int v0, v6, v0

    int-to-long v9, v0

    sget-wide v11, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long v2, v9, v11

    aget-object v4, v1, v6

    if-nez v4, :cond_2

    :cond_1
    move-object v4, v5

    :goto_0
    return-object v4

    :cond_2
    sget-object v0, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v8, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    goto :goto_0
.end method

.method private tryDeqAndExec(Ljava/util/concurrent/ForkJoinTask;)I
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)I"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    iget v2, v2, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    const v6, 0xffff

    and-int v11, v2, v6

    move-object/from16 v0, p0

    iget-object v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    iget-object v14, v2, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v14, :cond_0

    array-length v2, v14

    if-le v2, v11, :cond_0

    move-object/from16 v0, p1

    iget v2, v0, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v2, :cond_0

    const/4 v10, 0x0

    :goto_0
    if-gt v10, v11, :cond_0

    aget-object v13, v14, v10

    if-eqz v13, :cond_1

    iget v8, v13, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    iget v2, v13, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eq v8, v2, :cond_1

    iget-object v3, v13, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v3, :cond_1

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    and-int v9, v2, v8

    if-ltz v9, :cond_1

    aget-object v2, v3, v9

    move-object/from16 v0, p1

    if-ne v2, v0, :cond_1

    sget v2, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int v2, v9, v2

    int-to-long v6, v2

    sget-wide v15, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long v4, v6, v15

    iget v2, v13, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-ne v2, v8, :cond_0

    sget-object v2, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    const/4 v7, 0x0

    move-object/from16 v6, p1

    invoke-virtual/range {v2 .. v7}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, v8, 0x1

    iput v2, v13, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->poolIndex:I

    iput v2, v13, Ljava/util/concurrent/ForkJoinWorkerThread;->stealHint:I

    move-object/from16 v0, p0

    iget-object v12, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    invoke-virtual/range {p1 .. p1}, Ljava/util/concurrent/ForkJoinTask;->doExec()V

    move-object/from16 v0, p0

    iput-object v12, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    :cond_0
    move-object/from16 v0, p1

    iget v2, v0, Ljava/util/concurrent/ForkJoinTask;->status:I

    return v2

    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0
.end method

.method private static final writeSlot([Ljava/util/concurrent/ForkJoinTask;ILjava/util/concurrent/ForkJoinTask;)V
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;I",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)V"
        }
    .end annotation

    sget-object v0, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    sget v1, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int v1, p1, v1

    int-to-long v1, v1

    sget-wide v3, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long/2addr v1, v3

    invoke-virtual {v0, p0, v1, v2, p2}, Lsun/misc/Unsafe;->putObjectVolatile(Ljava/lang/Object;JLjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method final cancelTasks()V
    .locals 5

    iget-object v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentJoin:Ljava/util/concurrent/ForkJoinTask;

    if-eqz v0, :cond_0

    iget v3, v0, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v3, :cond_0

    invoke-virtual {v0}, Ljava/util/concurrent/ForkJoinTask;->cancelIgnoringExceptions()V

    :cond_0
    iget-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_1

    iget v3, v1, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v3, :cond_1

    invoke-virtual {v1}, Ljava/util/concurrent/ForkJoinTask;->cancelIgnoringExceptions()V

    :cond_1
    :goto_0
    iget v3, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    iget v4, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eq v3, v4, :cond_2

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->deqTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/concurrent/ForkJoinTask;->cancelIgnoringExceptions()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method final deqTask()Ljava/util/concurrent/ForkJoinTask;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation

    const/4 v5, 0x0

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v6, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-eq v0, v6, :cond_0

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_0

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    and-int v7, v0, v6

    if-ltz v7, :cond_0

    aget-object v4, v1, v7

    if-eqz v4, :cond_0

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-ne v0, v6, :cond_0

    sget-object v0, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    sget v2, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int v2, v7, v2

    int-to-long v2, v2

    sget-wide v8, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long/2addr v2, v8

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v6, 0x1

    iput v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    :goto_0
    return-object v4

    :cond_0
    move-object v4, v5

    goto :goto_0
.end method

.method final drainTasksTo(Ljava/util/Collection;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<-",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;>;)I"
        }
    .end annotation

    const/4 v0, 0x0

    :cond_0
    :goto_0
    iget v2, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    iget v3, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eq v2, v3, :cond_1

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->deqTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method final execTask(Ljava/util/concurrent/ForkJoinTask;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)V"
        }
    .end annotation

    iput-object p1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/concurrent/ForkJoinTask;->doExec()V

    :cond_0
    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->stealCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->stealCount:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    return-void

    :cond_1
    iget-boolean v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->locallyFifo:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->locallyDeqTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object p1

    :goto_1
    goto :goto_0

    :cond_2
    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->popTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object p1

    goto :goto_1
.end method

.method final getEstimatedSurplusTaskCount()I
    .locals 2

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    invoke-virtual {v1}, Ljava/util/concurrent/ForkJoinPool;->idlePerActive()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getPool()Ljava/util/concurrent/ForkJoinPool;
    .locals 1

    iget-object v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    return-object v0
.end method

.method public getPoolIndex()I
    .locals 1

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->poolIndex:I

    return v0
.end method

.method final getQueueSize()I
    .locals 2

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    sub-int/2addr v0, v1

    return v0
.end method

.method final helpQuiescePool()V
    .locals 15

    const/4 v14, -0x1

    const/4 v13, 0x1

    const/4 v0, 0x1

    iget-object v4, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    iget-object v3, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    invoke-virtual {v3, v13}, Ljava/util/concurrent/ForkJoinPool;->addQuiescerCount(I)V

    :cond_0
    :goto_0
    iget-object v10, v3, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    const/4 v8, 0x0

    iget v11, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v12, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-eq v11, v12, :cond_3

    move-object v8, p0

    :cond_1
    :goto_1
    if-eqz v8, :cond_7

    if-nez v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {v3, v13}, Ljava/util/concurrent/ForkJoinPool;->addActiveCount(I)V

    :cond_2
    if-eq v8, p0, :cond_5

    invoke-virtual {v8}, Ljava/util/concurrent/ForkJoinWorkerThread;->deqTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v7

    :goto_2
    if-eqz v7, :cond_0

    iput-object v7, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    invoke-virtual {v7}, Ljava/util/concurrent/ForkJoinTask;->doExec()V

    iput-object v4, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentSteal:Ljava/util/concurrent/ForkJoinTask;

    goto :goto_0

    :cond_3
    if-eqz v10, :cond_1

    array-length v2, v10

    if-le v2, v13, :cond_1

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->nextSeed()I

    move-result v5

    shl-int/lit8 v6, v2, 0x1

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v6, :cond_1

    add-int v11, v1, v5

    add-int/lit8 v12, v2, -0x1

    and-int/2addr v11, v12

    aget-object v9, v10, v11

    if-eqz v9, :cond_4

    iget v11, v9, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    iget v12, v9, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eq v11, v12, :cond_4

    move-object v8, v9

    goto :goto_1

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    iget-boolean v11, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->locallyFifo:Z

    if-eqz v11, :cond_6

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->locallyDeqTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v7

    goto :goto_2

    :cond_6
    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->popTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v7

    goto :goto_2

    :cond_7
    if-eqz v0, :cond_8

    const/4 v0, 0x0

    invoke-virtual {v3, v14}, Ljava/util/concurrent/ForkJoinPool;->addActiveCount(I)V

    :cond_8
    invoke-virtual {v3}, Ljava/util/concurrent/ForkJoinPool;->isQuiescent()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {v3, v13}, Ljava/util/concurrent/ForkJoinPool;->addActiveCount(I)V

    invoke-virtual {v3, v14}, Ljava/util/concurrent/ForkJoinPool;->addQuiescerCount(I)V

    return-void
.end method

.method final joinTask(Ljava/util/concurrent/ForkJoinTask;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)I"
        }
    .end annotation

    iget-object v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentJoin:Ljava/util/concurrent/ForkJoinTask;

    iput-object p1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentJoin:Ljava/util/concurrent/ForkJoinTask;

    const/16 v1, 0x10

    :cond_0
    :goto_0
    iget v2, p1, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-gez v2, :cond_1

    iput-object v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->currentJoin:Ljava/util/concurrent/ForkJoinTask;

    return v2

    :cond_1
    if-lez v1, :cond_5

    iget v3, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v4, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-eq v3, v4, :cond_2

    invoke-direct {p0, p1}, Ljava/util/concurrent/ForkJoinWorkerThread;->localHelpJoinTask(Ljava/util/concurrent/ForkJoinTask;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, p1}, Ljava/util/concurrent/ForkJoinWorkerThread;->tryDeqAndExec(Ljava/util/concurrent/ForkJoinTask;)I

    move-result v3

    if-ltz v3, :cond_0

    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1}, Ljava/util/concurrent/ForkJoinWorkerThread;->helpJoinTask(Ljava/util/concurrent/ForkJoinTask;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v1, 0x10

    :goto_1
    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_5
    const/16 v1, 0x10

    iget-object v3, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    invoke-virtual {v3, p1}, Ljava/util/concurrent/ForkJoinPool;->tryAwaitJoin(Ljava/util/concurrent/ForkJoinTask;)V

    goto :goto_0
.end method

.method final locallyDeqTask()Ljava/util/concurrent/ForkJoinTask;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_1

    array-length v0, v1

    add-int/lit8 v8, v0, -0x1

    if-ltz v8, :cond_1

    :cond_0
    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v6, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-eq v0, v6, :cond_1

    and-int v7, v8, v6

    aget-object v4, v1, v7

    if-eqz v4, :cond_0

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-ne v0, v6, :cond_0

    sget-object v0, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    sget v2, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int v2, v7, v2

    int-to-long v2, v2

    sget-wide v9, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long/2addr v2, v9

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v6, 0x1

    iput v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    :goto_0
    return-object v4

    :cond_1
    move-object v4, v5

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    const/16 v1, 0x2000

    new-array v1, v1, [Ljava/util/concurrent/ForkJoinTask;

    iput-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->workerSeedGenerator:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->seed:I

    return-void
.end method

.method protected onTermination(Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # Ljava/lang/Throwable;

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->terminate:Z

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->cancelTasks()V

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    invoke-virtual {v1, p0, p1}, Ljava/util/concurrent/ForkJoinPool;->deregisterWorker(Ljava/util/concurrent/ForkJoinWorkerThread;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    :goto_0
    invoke-static {p1}, Llibcore/util/SneakyThrow;->sneakyThrow(Ljava/lang/Throwable;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    if-nez p1, :cond_1

    move-object p1, v0

    :cond_1
    if-eqz p1, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz p1, :cond_2

    invoke-static {p1}, Llibcore/util/SneakyThrow;->sneakyThrow(Ljava/lang/Throwable;)V

    :cond_2
    throw v1
.end method

.method final peekTask()Ljava/util/concurrent/ForkJoinTask;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation

    iget-object v2, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v2, :cond_0

    array-length v3, v2

    add-int/lit8 v1, v3, -0x1

    if-gez v1, :cond_1

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_1
    iget-boolean v3, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->locallyFifo:Z

    if-eqz v3, :cond_2

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    :goto_1
    and-int v3, v0, v1

    aget-object v3, v2, v3

    goto :goto_0

    :cond_2
    iget v3, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    add-int/lit8 v0, v3, -0x1

    goto :goto_1
.end method

.method final pollLocalTask()Ljava/util/concurrent/ForkJoinTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation

    iget-boolean v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->locallyFifo:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->locallyDeqTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->popTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    goto :goto_0
.end method

.method final pollTask()Ljava/util/concurrent/ForkJoinTask;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->pollLocalTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v5

    if-nez v5, :cond_0

    iget-object v8, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    iget-object v7, v8, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    if-nez v7, :cond_1

    :cond_0
    move-object v8, v5

    :goto_0
    return-object v8

    :cond_1
    array-length v2, v7

    shl-int/lit8 v4, v2, 0x1

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->nextSeed()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    add-int/lit8 v0, v1, 0x1

    add-int v8, v1, v3

    add-int/lit8 v9, v2, -0x1

    and-int/2addr v8, v9

    aget-object v6, v7, v8

    if-eqz v6, :cond_3

    iget v8, v6, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    iget v9, v6, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eq v8, v9, :cond_3

    iget-object v8, v6, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v8, :cond_3

    invoke-virtual {v6}, Ljava/util/concurrent/ForkJoinWorkerThread;->deqTask()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v5

    if-eqz v5, :cond_2

    move-object v8, v5

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :cond_3
    move v1, v0

    goto :goto_1

    :cond_4
    const/4 v8, 0x0

    goto :goto_0
.end method

.method final pushTask(Ljava/util/concurrent/ForkJoinTask;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)V"
        }
    .end annotation

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_0

    iget v2, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    array-length v5, v1

    add-int/lit8 v0, v5, -0x1

    and-int v5, v2, v0

    sget v6, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int/2addr v5, v6

    int-to-long v5, v5

    sget-wide v7, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long v3, v5, v7

    sget-object v5, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v5, v1, v3, v4, p1}, Lsun/misc/Unsafe;->putOrderedObject(Ljava/lang/Object;JLjava/lang/Object;)V

    add-int/lit8 v5, v2, 0x1

    iput v5, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v5, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    sub-int/2addr v2, v5

    const/4 v5, 0x2

    if-gt v2, v5, :cond_1

    iget-object v5, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    invoke-virtual {v5}, Ljava/util/concurrent/ForkJoinPool;->signalWork()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne v2, v0, :cond_0

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->growQueue()V

    goto :goto_0
.end method

.method public run()V
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinWorkerThread;->onStart()V

    iget-object v2, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    invoke-virtual {v2, p0}, Ljava/util/concurrent/ForkJoinPool;->work(Ljava/util/concurrent/ForkJoinWorkerThread;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-virtual {p0, v1}, Ljava/util/concurrent/ForkJoinWorkerThread;->onTermination(Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception v0

    move-object v1, v0

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-virtual {p0, v1}, Ljava/util/concurrent/ForkJoinWorkerThread;->onTermination(Ljava/lang/Throwable;)V

    throw v2
.end method

.method final unpushTask(Ljava/util/concurrent/ForkJoinTask;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)Z"
        }
    .end annotation

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_0

    iget v6, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    iget v0, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-eq v6, v0, :cond_0

    sget-object v0, Ljava/util/concurrent/ForkJoinWorkerThread;->UNSAFE:Lsun/misc/Unsafe;

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v6, v6, -0x1

    and-int/2addr v2, v6

    sget v3, Ljava/util/concurrent/ForkJoinWorkerThread;->ASHIFT:I

    shl-int/2addr v2, v3

    int-to-long v2, v2

    sget-wide v4, Ljava/util/concurrent/ForkJoinWorkerThread;->ABASE:J

    add-long/2addr v2, v4

    const/4 v5, 0x0

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v6, p0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
