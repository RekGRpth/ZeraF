.class final Ljava/util/concurrent/ConcurrentHashMap$Segment;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source "ConcurrentHashMap.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljava/util/concurrent/ConcurrentHashMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Segment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/locks/ReentrantLock;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final MAX_SCAN_RETRIES:I

.field private static final serialVersionUID:J = 0x1f364c905893293dL


# instance fields
.field transient count:I

.field final loadFactor:F

.field transient modCount:I

.field volatile transient table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/concurrent/ConcurrentHashMap$HashEntry",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field transient threshold:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    if-le v1, v0, :cond_0

    const/16 v0, 0x40

    :cond_0
    sput v0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->MAX_SCAN_RETRIES:I

    return-void
.end method

.method constructor <init>(FI[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V
    .locals 0
    .param p1    # F
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FI[",
            "Ljava/util/concurrent/ConcurrentHashMap$HashEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput p1, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->loadFactor:F

    iput p2, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->threshold:I

    iput-object p3, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    return-void
.end method

.method private rehash(Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap$HashEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v16, v0

    shl-int/lit8 v12, v16, 0x1

    int-to-float v0, v12

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->loadFactor:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Ljava/util/concurrent/ConcurrentHashMap$Segment;->threshold:I

    new-array v13, v12, [Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    check-cast v13, [Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    add-int/lit8 v19, v12, -0x1

    const/4 v5, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v5, v0, :cond_4

    aget-object v3, v17, v5

    if-eqz v3, :cond_0

    iget-object v14, v3, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    iget v0, v3, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    move/from16 v21, v0

    and-int v6, v21, v19

    if-nez v14, :cond_1

    aput-object v3, v13, v6

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    move-object v10, v3

    move v9, v6

    move-object v8, v14

    :goto_1
    if-eqz v8, :cond_3

    iget v0, v8, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    move/from16 v21, v0

    and-int v7, v21, v19

    if-eq v7, v9, :cond_2

    move v9, v7

    move-object v10, v8

    :cond_2
    iget-object v8, v8, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    goto :goto_1

    :cond_3
    aput-object v10, v13, v9

    move-object/from16 v18, v3

    :goto_2
    move-object/from16 v0, v18

    if-eq v0, v10, :cond_0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    iget v4, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    and-int v7, v4, v19

    aget-object v11, v13, v7

    new-instance v21, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-object/from16 v0, v18

    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-direct {v0, v4, v1, v2, v11}, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;-><init>(ILjava/lang/Object;Ljava/lang/Object;Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    aput-object v21, v13, v7

    move-object/from16 v0, v18

    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-object/from16 v18, v0

    goto :goto_2

    :cond_4
    move-object/from16 v0, p1

    iget v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    move/from16 v21, v0

    and-int v15, v21, v19

    aget-object v21, v13, v15

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->setNext(Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    aput-object p1, v13, v15

    move-object/from16 v0, p0

    iput-object v13, v0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    return-void
.end method

.method private scanAndLock(Ljava/lang/Object;I)V
    .locals 5
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-static {p0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->entryForHash(Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v2

    move-object v0, v2

    const/4 v3, -0x1

    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v4

    if-nez v4, :cond_4

    if-gez v3, :cond_3

    if-eqz v0, :cond_1

    iget-object v4, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    goto :goto_0

    :cond_3
    add-int/lit8 v3, v3, 0x1

    sget v4, Ljava/util/concurrent/ConcurrentHashMap$Segment;->MAX_SCAN_RETRIES:I

    if-le v3, v4, :cond_5

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :cond_4
    return-void

    :cond_5
    and-int/lit8 v4, v3, 0x1

    if-nez v4, :cond_0

    invoke-static {p0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->entryForHash(Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v1

    if-eq v1, v2, :cond_0

    move-object v2, v1

    move-object v0, v1

    const/4 v3, -0x1

    goto :goto_0
.end method

.method private scanAndLockForPut(Ljava/lang/Object;ILjava/lang/Object;)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;
    .locals 6
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;)",
            "Ljava/util/concurrent/ConcurrentHashMap$HashEntry",
            "<TK;TV;>;"
        }
    .end annotation

    invoke-static {p0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->entryForHash(Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v2

    move-object v0, v2

    const/4 v3, 0x0

    const/4 v4, -0x1

    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v5

    if-nez v5, :cond_5

    if-gez v4, :cond_4

    if-nez v0, :cond_2

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    const/4 v5, 0x0

    invoke-direct {v3, p2, p1, p3, v5}, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;-><init>(ILjava/lang/Object;Ljava/lang/Object;Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    iget-object v5, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    goto :goto_0

    :cond_4
    add-int/lit8 v4, v4, 0x1

    sget v5, Ljava/util/concurrent/ConcurrentHashMap$Segment;->MAX_SCAN_RETRIES:I

    if-le v4, v5, :cond_6

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :cond_5
    return-object v3

    :cond_6
    and-int/lit8 v5, v4, 0x1

    if-nez v5, :cond_0

    invoke-static {p0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->entryForHash(Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v1

    if-eq v1, v2, :cond_0

    move-object v2, v1

    move-object v0, v1

    const/4 v4, -0x1

    goto :goto_0
.end method


# virtual methods
.method final clear()V
    .locals 3

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v1, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->setEntryAt([Ljava/util/concurrent/ConcurrentHashMap$HashEntry;ILjava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget v2, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    const/4 v2, 0x0

    iput v2, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v2

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2
.end method

.method final put(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 11
    .param p2    # I
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;Z)TV;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v5, 0x0

    :goto_0
    :try_start_0
    iget-object v8, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    array-length v9, v8

    add-int/lit8 v9, v9, -0x1

    and-int v3, v9, p2

    invoke-static {v8, v3}, Ljava/util/concurrent/ConcurrentHashMap;->entryAt([Ljava/util/concurrent/ConcurrentHashMap$HashEntry;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v2

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_4

    iget-object v4, v1, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    if-eq v4, p1, :cond_0

    iget v9, v1, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    if-ne v9, p2, :cond_3

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    :cond_0
    iget-object v7, v1, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    if-nez p4, :cond_1

    iput-object p3, v1, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    iget v9, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_2
    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v7

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->scanAndLockForPut(Ljava/lang/Object;ILjava/lang/Object;)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v5

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v1, v1, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    goto :goto_1

    :cond_4
    if-eqz v5, :cond_5

    invoke-virtual {v5, v2}, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->setNext(Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    :goto_3
    iget v9, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->count:I

    add-int/lit8 v0, v9, 0x1

    iget v9, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->threshold:I

    if-le v0, v9, :cond_6

    array-length v9, v8

    const/high16 v10, 0x40000000

    if-ge v9, v10, :cond_6

    invoke-direct {p0, v5}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->rehash(Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    :goto_4
    iget v9, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    iput v0, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->count:I

    const/4 v7, 0x0

    goto :goto_2

    :cond_5
    new-instance v6, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    invoke-direct {v6, p2, p1, p3, v2}, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;-><init>(ILjava/lang/Object;Ljava/lang/Object;Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    move-object v5, v6

    goto :goto_3

    :cond_6
    invoke-static {v8, v3, v5}, Ljava/util/concurrent/ConcurrentHashMap;->setEntryAt([Ljava/util/concurrent/ConcurrentHashMap$HashEntry;ILjava/util/concurrent/ConcurrentHashMap$HashEntry;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v9

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v9
.end method

.method final remove(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->scanAndLock(Ljava/lang/Object;I)V

    :cond_0
    const/4 v4, 0x0

    :try_start_0
    iget-object v6, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    array-length v8, v6

    add-int/lit8 v8, v8, -0x1

    and-int v1, v8, p2

    invoke-static {v6, v1}, Ljava/util/concurrent/ConcurrentHashMap;->entryAt([Ljava/util/concurrent/ConcurrentHashMap$HashEntry;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v0

    const/4 v5, 0x0

    :goto_0
    if-eqz v0, :cond_3

    iget-object v3, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    iget-object v2, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    if-eq v2, p1, :cond_1

    iget v8, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    if-ne v8, p2, :cond_5

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_1
    iget-object v7, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    if-eqz p3, :cond_2

    if-eq p3, v7, :cond_2

    invoke-virtual {p3, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_2
    if-nez v5, :cond_4

    invoke-static {v6, v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->setEntryAt([Ljava/util/concurrent/ConcurrentHashMap$HashEntry;ILjava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    :goto_1
    iget v8, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    iget v8, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->count:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v4, v7

    :cond_3
    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v4

    :cond_4
    :try_start_1
    invoke-virtual {v5, v3}, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->setNext(Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v8

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v8

    :cond_5
    move-object v5, v0

    move-object v0, v3

    goto :goto_0
.end method

.method final replace(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;)TV;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->scanAndLock(Ljava/lang/Object;I)V

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->entryForHash(Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    if-eq v1, p1, :cond_1

    iget v3, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    if-ne v3, p2, :cond_3

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    iget-object v2, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    iput-object p3, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    iget v3, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v2

    :cond_3
    :try_start_1
    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v3
.end method

.method final replace(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 4
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;TV;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->scanAndLock(Ljava/lang/Object;I)V

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->entryForHash(Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    if-eq v1, p1, :cond_1

    iget v3, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    if-ne v3, p2, :cond_3

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    iget-object v3, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    invoke-virtual {p3, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput-object p4, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    iget v3, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x1

    :cond_2
    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v2

    :cond_3
    :try_start_1
    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v3
.end method
