.class public interface abstract Lcom/mediatek/providers/contacts/SimCardUtils$SimType;
.super Ljava/lang/Object;
.source "SimCardUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/providers/contacts/SimCardUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SimType"
.end annotation


# static fields
.field public static final SIM_TYPE_SIM:I = 0x0

.field public static final SIM_TYPE_SIM_TAG:Ljava/lang/String; = "SIM"

.field public static final SIM_TYPE_UIM:I = 0x2

.field public static final SIM_TYPE_UIM_TAG:Ljava/lang/String; = "UIM"

.field public static final SIM_TYPE_USIM:I = 0x1

.field public static final SIM_TYPE_USIM_TAG:Ljava/lang/String; = "USIM"
