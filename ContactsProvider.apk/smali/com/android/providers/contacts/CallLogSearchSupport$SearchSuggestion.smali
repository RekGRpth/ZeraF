.class Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;
.super Ljava/lang/Object;
.source "CallLogSearchSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/CallLogSearchSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchSuggestion"
.end annotation


# instance fields
.field callNumberLabel:Ljava/lang/String;

.field callsId:J

.field calls_raw_contacts_id:I

.field date:J

.field icon1:Ljava/lang/String;

.field icon2:Ljava/lang/String;

.field isSdnContact:I

.field isVTCall:I

.field private mIcon01:Ljava/lang/String;

.field private mIcon02:Ljava/lang/String;

.field private mIcon03:Ljava/lang/String;

.field private mIcon04:Ljava/lang/String;

.field number:Ljava/lang/String;

.field photo_uri:Ljava/lang/String;

.field processed:Z

.field slotId:I

.field sortKey:Ljava/lang/String;

.field text1:Ljava/lang/String;

.field text2:Ljava/lang/String;

.field type:I


# direct methods
.method public constructor <init>(J)V
    .locals 2
    .param p1    # J

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->slotId:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->isSdnContact:I

    iput-object v1, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon01:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon02:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon03:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon04:Ljava/lang/String;

    iput-wide p1, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->callsId:J

    return-void
.end method

.method private addColumnValue(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "_id"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->callsId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "suggest_text_1"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->text1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v0, "suggest_text_2"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->text2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v0, "suggest_icon_1"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const-string v0, "suggest_icon_2"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const-string v0, "suggest_intent_data_id"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-wide v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->callsId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    const-string v0, "suggest_shortcut_id"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-wide v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->callsId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid column name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getIcon(II)Ljava/lang/String;
    .locals 11
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    const-string v7, "CallLogSearchSupport"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "slotId is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    iget-object v7, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon01:Ljava/lang/String;

    if-eqz v7, :cond_0

    const/4 v7, 0x1

    if-ge p2, v7, :cond_0

    iget-object v7, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon01:Ljava/lang/String;

    :goto_0
    return-object v7

    :cond_0
    const/4 v7, 0x1

    if-ne p1, v7, :cond_1

    iget-object v7, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon02:Ljava/lang/String;

    if-eqz v7, :cond_1

    const/4 v7, 0x1

    if-ge p2, v7, :cond_1

    iget-object v7, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon02:Ljava/lang/String;

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    iget-object v7, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon03:Ljava/lang/String;

    if-eqz v7, :cond_2

    if-lez p2, :cond_2

    iget-object v7, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon03:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v7, 0x1

    if-ne p1, v7, :cond_3

    iget-object v7, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon04:Ljava/lang/String;

    if-eqz v7, :cond_3

    if-lez p2, :cond_3

    iget-object v7, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon04:Ljava/lang/String;

    goto :goto_0

    :cond_3
    if-ltz p1, :cond_12

    const-string v7, "CallLogSearchSupport"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[processIcon] mSlot = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, -0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Lcom/android/providers/contacts/CallLogSearchSupport;->access$000()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, p1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v7, "CallLogSearchSupport"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "beforInfor : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " | afterInfor : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " | TIME : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v9, v0, v2

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v6, :cond_4

    iget v4, v6, Landroid/provider/Telephony$SIMInfo;->mColor:I

    :cond_4
    const-string v7, "CallLogSearchSupport"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[processIcon] i = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-lez p2, :cond_a

    if-nez v4, :cond_6

    const v7, 0x7f020006

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    :goto_1
    if-nez p1, :cond_f

    const/4 v7, 0x1

    if-ge p2, v7, :cond_f

    iput-object v5, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon01:Ljava/lang/String;

    :cond_5
    :goto_2
    move-object v7, v5

    goto/16 :goto_0

    :cond_6
    const/4 v7, 0x1

    if-ne v4, v7, :cond_7

    const v7, 0x7f020008

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_7
    const/4 v7, 0x2

    if-ne v4, v7, :cond_8

    const v7, 0x7f020007

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_8
    const/4 v7, 0x3

    if-ne v4, v7, :cond_9

    const v7, 0x7f020009

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_9
    const v7, 0x7f020001

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_a
    if-nez v4, :cond_b

    const v7, 0x7f02000a

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_b
    const/4 v7, 0x1

    if-ne v4, v7, :cond_c

    const v7, 0x7f02000c

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_c
    const/4 v7, 0x2

    if-ne v4, v7, :cond_d

    const v7, 0x7f02000b

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_d
    const/4 v7, 0x3

    if-ne v4, v7, :cond_e

    const v7, 0x7f02000d

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_e
    const v7, 0x7f020001

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_f
    const/4 v7, 0x1

    if-ne p1, v7, :cond_10

    const/4 v7, 0x1

    if-ge p2, v7, :cond_10

    iput-object v5, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon02:Ljava/lang/String;

    goto :goto_2

    :cond_10
    if-nez p1, :cond_11

    if-lez p2, :cond_11

    iput-object v5, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon03:Ljava/lang/String;

    goto :goto_2

    :cond_11
    const/4 v7, 0x1

    if-ne p1, v7, :cond_5

    if-lez p2, :cond_5

    iput-object v5, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->mIcon04:Ljava/lang/String;

    goto :goto_2

    :cond_12
    const v7, 0x10802c0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method private process()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->processed:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "CallLogSearchSupport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "photo_uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->photo_uri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->photo_uri:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->photo_uri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon1:Ljava/lang/String;

    :goto_1
    iget v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->type:I

    packed-switch v0, :pswitch_data_0

    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->processed:Z

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->calls_raw_contacts_id:I

    if-nez v0, :cond_2

    const v0, 0x10802c0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon1:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->slotId:I

    iget v1, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->isSdnContact:I

    invoke-direct {p0, v0, v1}, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->getIcon(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon1:Ljava/lang/String;

    goto :goto_1

    :pswitch_0
    iget v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->isVTCall:I

    if-nez v0, :cond_3

    const v0, 0x7f02000e

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon2:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const v0, 0x7f020011

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon2:Ljava/lang/String;

    goto :goto_2

    :pswitch_1
    iget v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->isVTCall:I

    if-nez v0, :cond_4

    const v0, 0x7f020010

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon2:Ljava/lang/String;

    goto :goto_2

    :cond_4
    const v0, 0x7f020013

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon2:Ljava/lang/String;

    goto :goto_2

    :pswitch_2
    iget v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->isVTCall:I

    if-nez v0, :cond_5

    const v0, 0x7f02000f

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon2:Ljava/lang/String;

    goto :goto_2

    :cond_5
    const v0, 0x7f020012

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon2:Ljava/lang/String;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public asList([Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .param p1    # [Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->process()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_1

    iget-wide v2, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->callsId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->text1:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->text2:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon1:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->icon2:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v2, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->callsId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v2, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->callsId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget-object v2, p1, v0

    invoke-direct {p0, v1, v2}, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->addColumnValue(Ljava/util/ArrayList;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getSortKey()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->sortKey:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->process()V

    iget-wide v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->date:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->sortKey:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/CallLogSearchSupport$SearchSuggestion;->sortKey:Ljava/lang/String;

    return-object v0
.end method
