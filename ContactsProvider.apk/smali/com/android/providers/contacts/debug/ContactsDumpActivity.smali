.class public Lcom/android/providers/contacts/debug/ContactsDumpActivity;
.super Landroid/app/Activity;
.source "ContactsDumpActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/debug/ContactsDumpActivity$1;,
        Lcom/android/providers/contacts/debug/ContactsDumpActivity$DumpDbTask;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mCancelButton:Landroid/widget/Button;

.field private mConfirmButton:Landroid/widget/Button;

.field private mDeleteButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "ContactsDumpActivity"

    sput-object v0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/providers/contacts/debug/ContactsDumpActivity;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/android/providers/contacts/debug/ContactsDumpActivity;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->emailFile(Landroid/net/Uri;)V

    return-void
.end method

.method private cleanup()V
    .locals 0

    invoke-static {p0}, Lcom/android/providers/contacts/debug/DataExporter;->removeDumpFiles(Landroid/content/Context;)V

    return-void
.end method

.method private emailFile(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;

    sget-object v1, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->TAG:Ljava/lang/String;

    const-string v2, "Drafting email"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.SUBJECT"

    const v2, 0x7f040012

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    const v2, 0x7f040013

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "application/zip"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const v1, 0x7f040011

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private updateDeleteButton()V
    .locals 2

    iget-object v0, p0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->mDeleteButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/android/providers/contacts/debug/DataExporter;->dumpFileExists(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->updateDeleteButton()V

    iget-object v0, p0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->mConfirmButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->mConfirmButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    new-instance v0, Lcom/android/providers/contacts/debug/ContactsDumpActivity$DumpDbTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/providers/contacts/debug/ContactsDumpActivity$DumpDbTask;-><init>(Lcom/android/providers/contacts/debug/ContactsDumpActivity;Lcom/android/providers/contacts/debug/ContactsDumpActivity$1;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->cleanup()V

    invoke-direct {p0}, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->updateDeleteButton()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f050001
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x1080027

    invoke-virtual {v0, v2, v1}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    const v0, 0x7f050001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->mConfirmButton:Landroid/widget/Button;

    const v0, 0x7f050003

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->mCancelButton:Landroid/widget/Button;

    const v0, 0x7f050002

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->mDeleteButton:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/android/providers/contacts/debug/ContactsDumpActivity;->updateDeleteButton()V

    return-void
.end method
