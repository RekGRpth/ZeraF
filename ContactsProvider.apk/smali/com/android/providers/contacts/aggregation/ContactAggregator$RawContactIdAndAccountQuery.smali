.class final Lcom/android/providers/contacts/aggregation/ContactAggregator$RawContactIdAndAccountQuery;
.super Ljava/lang/Object;
.source "ContactAggregator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/aggregation/ContactAggregator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RawContactIdAndAccountQuery"
.end annotation


# static fields
.field public static final ACCOUNT_ID:I = 0x1

.field public static final COLUMNS:[Ljava/lang/String;

.field public static final CONTACT_ID:I = 0x0

.field public static final SELECTION:Ljava/lang/String; = "_id=?"

.field public static final TABLE:Ljava/lang/String; = "raw_contacts"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "account_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/contacts/aggregation/ContactAggregator$RawContactIdAndAccountQuery;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
