.class public Lcom/android/providers/contacts/util/TypedUriMatcherImpl;
.super Ljava/lang/Object;
.source "TypedUriMatcherImpl.java"

# interfaces
.implements Lcom/android/providers/contacts/util/TypedUriMatcher;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/android/providers/contacts/util/UriType;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/android/providers/contacts/util/TypedUriMatcher",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mAuthority:Ljava/lang/String;

.field private final mNoMatchUriType:Lcom/android/providers/contacts/util/UriType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mUriMatcher:Landroid/content/UriMatcher;

.field private final mValues:[Lcom/android/providers/contacts/util/UriType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;[Lcom/android/providers/contacts/util/UriType;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[TT;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->mAuthority:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->mValues:[Lcom/android/providers/contacts/util/UriType;

    new-instance v6, Landroid/content/UriMatcher;

    const/4 v7, -0x1

    invoke-direct {v6, v7}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v6, p0, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->mUriMatcher:Landroid/content/UriMatcher;

    const/4 v1, 0x0

    move-object v0, p2

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v5, v0, v2

    invoke-interface {v5}, Lcom/android/providers/contacts/util/UriType;->path()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v4, v5}, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->addUriType(Ljava/lang/String;Lcom/android/providers/contacts/util/UriType;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    move-object v1, v5

    goto :goto_1

    :cond_1
    iput-object v1, p0, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->mNoMatchUriType:Lcom/android/providers/contacts/util/UriType;

    return-void
.end method

.method private addUriType(Ljava/lang/String;Lcom/android/providers/contacts/util/UriType;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->mAuthority:Ljava/lang/String;

    invoke-interface {p2}, Lcom/android/providers/contacts/util/UriType;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public match(Landroid/net/Uri;)Lcom/android/providers/contacts/util/UriType;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")TT;"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->mNoMatchUriType:Lcom/android/providers/contacts/util/UriType;

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/android/providers/contacts/util/TypedUriMatcherImpl;->mValues:[Lcom/android/providers/contacts/util/UriType;

    aget-object v1, v1, v0

    goto :goto_0
.end method
