.class public Lcom/android/providers/contacts/ContactsTransaction;
.super Ljava/lang/Object;
.source "ContactsTransaction.java"


# instance fields
.field private final mBatch:Z

.field private final mDatabaseTagMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ">;"
        }
    .end annotation
.end field

.field private final mDatabasesForTransaction:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDirty:Z

.field private mYieldFailed:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/android/providers/contacts/ContactsTransaction;->mBatch:Z

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mIsDirty:Z

    return-void
.end method

.method private getTagForDb(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    iget-object v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public finish(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mBatch:Z

    if-eqz v2, :cond_0

    if-eqz p1, :cond_4

    :cond_0
    iget-object v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    iget-boolean v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mYieldFailed:Z

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isDbLockedByCurrentThread()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mIsDirty:Z

    :cond_4
    return-void
.end method

.method public getDbForTag(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public hasDbInTransaction(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isBatch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mBatch:Z

    return v0
.end method

.method public isDirty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mIsDirty:Z

    return v0
.end method

.method public markDirty()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mIsDirty:Z

    return-void
.end method

.method public markSuccessful(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mBatch:Z

    if-eqz v2, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public markYieldFailed()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mYieldFailed:Z

    return-void
.end method

.method public removeDbForTag(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public startTransactionForDb(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/database/sqlite/SQLiteTransactionListener;)V
    .locals 2
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/database/sqlite/SQLiteTransactionListener;

    invoke-virtual {p0, p2}, Lcom/android/providers/contacts/ContactsTransaction;->hasDbInTransaction(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_1

    invoke-virtual {p1, p3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    goto :goto_0
.end method
