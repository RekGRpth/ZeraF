.class Lcom/google/android/common/GoogleWebContentHelper$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "GoogleWebContentHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/common/GoogleWebContentHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/common/GoogleWebContentHelper;


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/common/GoogleWebContentHelper$MyWebViewClient;->this$0:Lcom/google/android/common/GoogleWebContentHelper;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/common/GoogleWebContentHelper;->handleWebViewCompletion(Z)V
    invoke-static {v0, v1}, Lcom/google/android/common/GoogleWebContentHelper;->access$100(Lcom/google/android/common/GoogleWebContentHelper;Z)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/common/GoogleWebContentHelper$MyWebViewClient;->this$0:Lcom/google/android/common/GoogleWebContentHelper;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/common/GoogleWebContentHelper;->handleWebViewCompletion(Z)V
    invoke-static {v0, v1}, Lcom/google/android/common/GoogleWebContentHelper;->access$100(Lcom/google/android/common/GoogleWebContentHelper;Z)V

    return-void
.end method

.method public onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/webkit/HttpAuthHandler;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/common/GoogleWebContentHelper$MyWebViewClient;->this$0:Lcom/google/android/common/GoogleWebContentHelper;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/common/GoogleWebContentHelper;->handleWebViewCompletion(Z)V
    invoke-static {v0, v1}, Lcom/google/android/common/GoogleWebContentHelper;->access$100(Lcom/google/android/common/GoogleWebContentHelper;Z)V

    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/webkit/SslErrorHandler;
    .param p3    # Landroid/net/http/SslError;

    iget-object v0, p0, Lcom/google/android/common/GoogleWebContentHelper$MyWebViewClient;->this$0:Lcom/google/android/common/GoogleWebContentHelper;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/common/GoogleWebContentHelper;->handleWebViewCompletion(Z)V
    invoke-static {v0, v1}, Lcom/google/android/common/GoogleWebContentHelper;->access$100(Lcom/google/android/common/GoogleWebContentHelper;Z)V

    return-void
.end method

.method public onTooManyRedirects(Landroid/webkit/WebView;Landroid/os/Message;Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/os/Message;
    .param p3    # Landroid/os/Message;

    iget-object v0, p0, Lcom/google/android/common/GoogleWebContentHelper$MyWebViewClient;->this$0:Lcom/google/android/common/GoogleWebContentHelper;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/common/GoogleWebContentHelper;->handleWebViewCompletion(Z)V
    invoke-static {v0, v1}, Lcom/google/android/common/GoogleWebContentHelper;->access$100(Lcom/google/android/common/GoogleWebContentHelper;Z)V

    return-void
.end method
