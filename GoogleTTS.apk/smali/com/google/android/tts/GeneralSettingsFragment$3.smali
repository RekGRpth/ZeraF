.class Lcom/google/android/tts/GeneralSettingsFragment$3;
.super Landroid/webkit/WebViewClient;
.source "GeneralSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/GeneralSettingsFragment;->showPageOfText(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/tts/GeneralSettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/tts/GeneralSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/tts/GeneralSettingsFragment$3;->this$0:Lcom/google/android/tts/GeneralSettingsFragment;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment$3;->this$0:Lcom/google/android/tts/GeneralSettingsFragment;

    # getter for: Lcom/google/android/tts/GeneralSettingsFragment;->mSpinnerDlg:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/google/android/tts/GeneralSettingsFragment;->access$300(Lcom/google/android/tts/GeneralSettingsFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment$3;->this$0:Lcom/google/android/tts/GeneralSettingsFragment;

    # getter for: Lcom/google/android/tts/GeneralSettingsFragment;->mSpinnerDlg:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/google/android/tts/GeneralSettingsFragment;->access$300(Lcom/google/android/tts/GeneralSettingsFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment$3;->this$0:Lcom/google/android/tts/GeneralSettingsFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/tts/GeneralSettingsFragment;->mSpinnerDlg:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/google/android/tts/GeneralSettingsFragment;->access$302(Lcom/google/android/tts/GeneralSettingsFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment$3;->this$0:Lcom/google/android/tts/GeneralSettingsFragment;

    # getter for: Lcom/google/android/tts/GeneralSettingsFragment;->mTextDlg:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/google/android/tts/GeneralSettingsFragment;->access$400(Lcom/google/android/tts/GeneralSettingsFragment;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/tts/GeneralSettingsFragment$3;->this$0:Lcom/google/android/tts/GeneralSettingsFragment;

    # getter for: Lcom/google/android/tts/GeneralSettingsFragment;->mTextDlg:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/google/android/tts/GeneralSettingsFragment;->access$400(Lcom/google/android/tts/GeneralSettingsFragment;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_1
    return-void
.end method
