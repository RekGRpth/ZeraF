.class public final Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "VoiceMetadataProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voicepack/VoiceMetadataProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Metadata"
.end annotation


# instance fields
.field private cachedSize:I

.field private description_:Ljava/lang/String;

.field private hasDescription:Z

.field private hasLocale:Z

.field private hasRevision:Z

.field private hasSizeKb:Z

.field private hasUrl:Z

.field private locale_:Ljava/lang/String;

.field private revision_:I

.field private sizeKb_:I

.field private url_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->locale_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->url_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->revision_:I

    iput v1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->sizeKb_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->description_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->cachedSize:I

    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    invoke-direct {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->cachedSize:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->locale_:Ljava/lang/String;

    return-object v0
.end method

.method public getRevision()I
    .locals 1

    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->revision_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasLocale()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasRevision()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getRevision()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasSizeKb()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getSizeKb()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasDescription()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->cachedSize:I

    return v0
.end method

.method public getSizeKb()I
    .locals 1

    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->sizeKb_:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasDescription:Z

    return v0
.end method

.method public hasLocale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasLocale:Z

    return v0
.end method

.method public hasRevision()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasRevision:Z

    return v0
.end method

.method public hasSizeKb()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasSizeKb:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->setLocale(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->setUrl(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->setRevision(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->setSizeKb(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->setDescription(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    move-result-object v0

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasDescription:Z

    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->description_:Ljava/lang/String;

    return-object p0
.end method

.method public setLocale(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasLocale:Z

    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->locale_:Ljava/lang/String;

    return-object p0
.end method

.method public setRevision(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasRevision:Z

    iput p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->revision_:I

    return-object p0
.end method

.method public setSizeKb(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasSizeKb:Z

    iput p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->sizeKb_:I

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasUrl:Z

    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->url_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasLocale()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasRevision()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getRevision()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasSizeKb()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getSizeKb()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    return-void
.end method
