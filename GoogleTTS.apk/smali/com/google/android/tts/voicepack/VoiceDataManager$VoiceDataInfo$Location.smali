.class final enum Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;
.super Ljava/lang/Enum;
.source "VoiceDataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Location"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

.field public static final enum APK:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

.field public static final enum DATA:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    const-string v1, "APK"

    invoke-direct {v0, v1, v2}, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;->APK:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    new-instance v0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    const-string v1, "DATA"

    invoke-direct {v0, v1, v3}, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;->DATA:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    sget-object v1, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;->APK:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;->DATA:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;->$VALUES:[Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;
    .locals 1

    const-class v0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    return-object v0
.end method

.method public static values()[Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;
    .locals 1

    sget-object v0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;->$VALUES:[Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    invoke-virtual {v0}, [Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    return-object v0
.end method
