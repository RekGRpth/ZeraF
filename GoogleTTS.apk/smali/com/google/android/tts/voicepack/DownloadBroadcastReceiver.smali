.class public Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DownloadBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private openDownloadsActivity(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v4, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p1}, Lcom/google/android/tts/GoogleTtsApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/GoogleTtsApplication;

    move-result-object v0

    const-string v4, "extra_download_id"

    const-wide/16 v5, -0x1

    invoke-virtual {p2, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v3

    new-instance v4, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;

    invoke-direct {v4}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;-><init>()V

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;

    const/4 v6, 0x0

    new-instance v7, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;-><init>(Lcom/google/android/tts/GoogleTtsApplication;JLandroid/content/BroadcastReceiver$PendingResult;)V

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/voicepack/DownloadBroadcastReceiver;->openDownloadsActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
