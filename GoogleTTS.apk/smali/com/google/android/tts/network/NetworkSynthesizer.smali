.class public Lcom/google/android/tts/network/NetworkSynthesizer;
.super Ljava/lang/Object;
.source "NetworkSynthesizer.java"

# interfaces
.implements Lcom/google/android/tts/Synthesizer;


# instance fields
.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mCurrentLocale:[Ljava/lang/String;

.field private mFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/android/tts/network/ByteArrayHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final mHttpHelper:Lcom/google/android/tts/network/HttpHelper;

.field private final mLangCountryHelper:Lcom/google/android/tts/network/NetworkLangCountryHelper;

.field private mService:Ljava/util/concurrent/ExecutorService;

.field private final mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

.field private final mStateLock:Ljava/lang/Object;

.field private mStopRequested:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/tts/network/NetworkLangCountryHelper;Lcom/google/android/common/base/Function;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/tts/network/NetworkLangCountryHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/tts/network/NetworkLangCountryHelper;",
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    iput-object p2, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mLangCountryHelper:Lcom/google/android/tts/network/NetworkLangCountryHelper;

    new-instance v0, Lcom/google/android/tts/network/HttpHelper;

    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mLangCountryHelper:Lcom/google/android/tts/network/NetworkLangCountryHelper;

    invoke-direct {v0, p3, v1}, Lcom/google/android/tts/network/HttpHelper;-><init>(Lcom/google/android/common/base/Function;Lcom/google/android/tts/network/NetworkLangCountryHelper;)V

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mHttpHelper:Lcom/google/android/tts/network/HttpHelper;

    new-instance v0, Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-direct {v0}, Lcom/google/android/tts/network/BufferedSpeexDecoder;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-void
.end method

.method private decodeAndWriteData([BLcom/google/android/tts/GoogleTtsRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 9
    .param p1    # [B
    .param p2    # Lcom/google/android/tts/GoogleTtsRequest;
    .param p3    # Landroid/speech/tts/SynthesisCallback;

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    sget-object v6, Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;->WB:Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;

    const/16 v7, 0xa

    invoke-virtual {v5, v6, v7}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->startSynthesis(Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;I)V

    iget-object v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-boolean v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    monitor-exit v6

    :goto_0
    return-void

    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p2}, Lcom/google/android/tts/GoogleTtsRequest;->setServed()V

    sget-object v5, Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;->WB:Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;

    invoke-virtual {v5}, Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;->getRate()I

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-interface {p3, v5, v6, v7}, Landroid/speech/tts/SynthesisCallback;->start(III)I

    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-virtual {v5}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->getOutputBufferSize()I

    move-result v5

    new-array v0, v5, [B

    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-virtual {v5}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->getInputBufferSize()I

    move-result v4

    const/4 v3, 0x0

    :goto_1
    array-length v5, p1

    if-ge v3, v5, :cond_2

    array-length v5, p1

    sub-int/2addr v5, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-virtual {v5, p1, v3, v2, v0}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->getNextChunk([BII[B)I

    move-result v1

    if-lez v1, :cond_1

    add-int/2addr v3, v2

    invoke-interface {p3, v0, v8, v1}, Landroid/speech/tts/SynthesisCallback;->audioAvailable([BII)I

    goto :goto_1

    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    :cond_1
    const-string v5, "NetworkSynthesizer"

    const-string v6, "Error decoding speex stream, bytesRead < 0"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-virtual {v5}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->endSynthesis()V

    invoke-interface {p3}, Landroid/speech/tts/SynthesisCallback;->done()I

    goto :goto_0
.end method


# virtual methods
.method public getLanguage()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mCurrentLocale:[Ljava/lang/String;

    return-object v0
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mLangCountryHelper:Lcom/google/android/tts/network/NetworkLangCountryHelper;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/tts/network/NetworkLangCountryHelper;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public onClose()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mService:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0x5

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NetworkSynthesizer"

    const-string v2, "Network tasks did not terminate within timeout."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "NetworkSynthesizer"

    const-string v2, "Thread interrupted while waiting for tasks to complete."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onInit()V
    .locals 1

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mService:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mLangCountryHelper:Lcom/google/android/tts/network/NetworkLangCountryHelper;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/tts/network/NetworkLangCountryHelper;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    new-array v1, v5, [Ljava/lang/String;

    aput-object p1, v1, v2

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, ""

    aput-object v2, v1, v4

    iput-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mCurrentLocale:[Ljava/lang/String;

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-ne v0, v3, :cond_0

    new-array v1, v5, [Ljava/lang/String;

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    const-string v2, ""

    aput-object v2, v1, v4

    iput-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mCurrentLocale:[Ljava/lang/String;

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onSynthesize(Lcom/google/android/tts/GoogleTtsRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 12
    .param p1    # Lcom/google/android/tts/GoogleTtsRequest;
    .param p2    # Landroid/speech/tts/SynthesisCallback;

    iget-object v8, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v8

    const/4 v7, 0x0

    :try_start_0
    iput-boolean v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getTimeout()I

    move-result v7

    int-to-long v1, v7

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getCountry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/google/android/tts/network/NetworkSynthesizer;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x2

    if-ne v7, v8, :cond_0

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->setServed()V

    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    :goto_0
    return-void

    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    :cond_0
    iget-object v8, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v8

    :try_start_2
    iget-boolean v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    if-eqz v7, :cond_1

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    monitor-exit v8

    goto :goto_0

    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v7

    :cond_1
    :try_start_3
    iget-object v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mService:Ljava/util/concurrent/ExecutorService;

    new-instance v9, Lcom/google/android/tts/network/NetworkFetchTask;

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getTimeout()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mHttpHelper:Lcom/google/android/tts/network/HttpHelper;

    invoke-direct {v9, p1, v10, p2, v11}, Lcom/google/android/tts/network/NetworkFetchTask;-><init>(Lcom/google/android/tts/GoogleTtsRequest;ILandroid/speech/tts/SynthesisCallback;Lcom/google/android/tts/network/HttpHelper;)V

    invoke-interface {v7, v9}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v4, 0x0

    :try_start_4
    iget-object v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v7, v1, v2, v8}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/tts/network/ByteArrayHolder;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_4 .. :try_end_4} :catch_3

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/google/android/tts/network/ByteArrayHolder;->get()[B

    move-result-object v7

    invoke-direct {p0, v7, p1, p2}, Lcom/google/android/tts/network/NetworkSynthesizer;->decodeAndWriteData([BLcom/google/android/tts/GoogleTtsRequest;Landroid/speech/tts/SynthesisCallback;)V

    goto :goto_0

    :catch_0
    move-exception v5

    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto :goto_0

    :catch_1
    move-exception v3

    const-string v7, "NetworkSynthesizer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "Error executing network fetch : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto :goto_0

    :catch_2
    move-exception v6

    const-string v7, "NetworkSynthesizer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "Timed out executing network fetch : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Ljava/util/concurrent/Future;->cancel(Z)Z

    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    iget-object v8, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v8

    const/4 v7, 0x0

    :try_start_5
    iput-boolean v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->setServed()V

    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto/16 :goto_0

    :catchall_2
    move-exception v7

    :try_start_6
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v7

    :cond_2
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto/16 :goto_0
.end method
