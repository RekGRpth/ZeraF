.class public Lcom/google/android/tts/local/PattsSynthesizer;
.super Ljava/lang/Object;
.source "PattsSynthesizer.java"

# interfaces
.implements Lcom/google/android/tts/Synthesizer;


# instance fields
.field private mBaseDir:Ljava/lang/String;

.field private final mConfig:Lcom/google/android/tts/TtsConfig;

.field private final mContext:Landroid/content/Context;

.field private final mLangCountryHelper:Lcom/google/android/tts/local/LangCountryHelper;

.field private final mProcessor:Lcom/google/android/tts/local/TextPreprocessor;

.field private volatile mStopRequested:Z

.field private final mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

.field private mSynthesisBuf:Ljava/nio/ByteBuffer;

.field private final mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/tts/TtsConfig;Lcom/google/android/tts/local/LangCountryHelper;Lcom/google/android/tts/voicepack/VoiceDataManager;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/tts/TtsConfig;
    .param p3    # Lcom/google/android/tts/local/LangCountryHelper;
    .param p4    # Lcom/google/android/tts/voicepack/VoiceDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;-><init>(Landroid/content/res/AssetManager;)V

    iput-object v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    iput-object p3, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mLangCountryHelper:Lcom/google/android/tts/local/LangCountryHelper;

    iput-object p4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;

    new-instance v0, Lcom/google/android/tts/local/TextPreprocessor;

    invoke-direct {v0}, Lcom/google/android/tts/local/TextPreprocessor;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mProcessor:Lcom/google/android/tts/local/TextPreprocessor;

    iput-object p1, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mConfig:Lcom/google/android/tts/TtsConfig;

    return-void
.end method

.method private static clip(III)I
    .locals 0
    .param p0    # I
    .param p1    # I
    .param p2    # I

    if-le p0, p2, :cond_0

    :goto_0
    return p2

    :cond_0
    if-ge p0, p1, :cond_1

    move p2, p1

    goto :goto_0

    :cond_1
    move p2, p0

    goto :goto_0
.end method

.method private destroySynthesizer()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v0}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v0}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->shutdown()V

    :cond_0
    return-void
.end method

.method private getBaseDir(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/local/PattsSynthesizer;->getLocaleKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLocaleFromBaseDir(Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x3

    new-array v3, v4, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v3, v7

    const-string v4, ""

    aput-object v4, v3, v5

    const-string v4, ""

    aput-object v4, v3, v6

    const-string v2, "([A-Za-z]+)-([A-Za-z]+).*"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v4

    if-ne v4, v6, :cond_0

    invoke-virtual {v0, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    :cond_0
    return-object v3
.end method

.method private getLocaleKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initializeSynthesizer(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    iget-object v5, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v5}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v5}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->shutdown()V

    :cond_0
    iget-object v5, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/tts/local/PattsSynthesizer;->getLocaleFromBaseDir(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v4, v5

    const/4 v6, 0x1

    aget-object v6, v4, v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/tts/local/PattsSynthesizer;->isLocaleInApk(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v5, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "voices"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mConfig:Lcom/google/android/tts/TtsConfig;

    invoke-interface {v7}, Lcom/google/android/tts/TtsConfig;->getProjectFileVariant()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->initialize(Ljava/io/InputStream;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    if-nez v2, :cond_1

    const-string v5, "PattsSynthesizer"

    const-string v6, "Failed to load synth data."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    new-instance v6, Ljava/io/FileInputStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;

    invoke-virtual {v8}, Lcom/google/android/tts/voicepack/VoiceDataManager;->getPattsDataPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mConfig:Lcom/google/android/tts/TtsConfig;

    invoke-interface {v8}, Lcom/google/android/tts/TtsConfig;->getProjectFileVariant()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;

    invoke-virtual {v7}, Lcom/google/android/tts/voicepack/VoiceDataManager;->getPattsDataPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->initialize(Ljava/io/InputStream;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v5, "PattsSynthesizer"

    const-string v6, "Unable to open project file."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isLocaleInApk(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/local/PattsSynthesizer;->getLocaleKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;

    invoke-virtual {v1, v0}, Lcom/google/android/tts/voicepack/VoiceDataManager;->isApkLocale(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method static normalizePitch(I)F
    .locals 2
    .param p0    # I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    const/16 v1, 0x1f4

    invoke-static {p0, v0, v1}, Lcom/google/android/tts/local/PattsSynthesizer;->clip(III)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000

    div-float/2addr v0, v1

    return v0
.end method

.method static normalizeSpeechRate(I)F
    .locals 5
    .param p0    # I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/16 v4, 0x64

    const/high16 v3, 0x40e00000

    const/16 v1, 0x14

    const/16 v2, 0x12c

    invoke-static {p0, v1, v2}, Lcom/google/android/tts/local/PattsSynthesizer;->clip(III)I

    move-result v0

    if-ne v0, v4, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    if-le v0, v4, :cond_1

    const/high16 v1, 0x40600000

    int-to-float v2, v0

    mul-float/2addr v2, v3

    const/high16 v3, 0x43480000

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    goto :goto_0

    :cond_1
    const/high16 v1, 0x410c0000

    int-to-float v2, v0

    mul-float/2addr v2, v3

    const/high16 v3, 0x42a00000

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    goto :goto_0
.end method

.method private synthesizeSentence(Ljava/lang/String;Lcom/google/speech/patts/engine/api/SentenceControls;Landroid/speech/tts/SynthesisCallback;)Z
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/speech/patts/engine/api/SentenceControls;
    .param p3    # Landroid/speech/tts/SynthesisCallback;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v3}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "PattsSynthesizer"

    const-string v4, "SynthesizeSentence requested before engine initialized."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p3}, Landroid/speech/tts/SynthesisCallback;->error()V

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v3, p1, p2}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->initBufferedSynthesis(Ljava/lang/String;Lcom/google/speech/patts/engine/api/SentenceControls;)Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-interface {p3}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    iget-object v3, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    iget-object v4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0, v4}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->synthesizeToDirectBuffer(Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;Ljava/nio/ByteBuffer;)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-interface {p3, v3, v2, v1}, Landroid/speech/tts/SynthesisCallback;->audioAvailable([BII)I

    invoke-virtual {v0}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->isInProgress()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mStopRequested:Z

    if-eqz v3, :cond_3

    :cond_4
    invoke-virtual {v0}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->destroy()V

    iget-boolean v3, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mStopRequested:Z

    if-nez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private synthesizeSilence(JLandroid/speech/tts/SynthesisCallback;)Z
    .locals 7
    .param p1    # J
    .param p3    # Landroid/speech/tts/SynthesisCallback;

    const-wide/16 v0, 0x2710

    cmp-long v5, p1, v0

    if-lez v5, :cond_1

    :goto_0
    const/high16 v5, 0x447a0000

    iget-object v6, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v6}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->getSampleRate()I

    move-result v6

    int-to-float v6, v6

    div-float v2, v5, v6

    long-to-float v5, v0

    div-float/2addr v5, v2

    const/high16 v6, 0x40000000

    mul-float/2addr v5, v6

    float-to-int v3, v5

    if-lez v3, :cond_0

    new-array v4, v3, [B

    const/4 v5, 0x0

    array-length v6, v4

    invoke-interface {p3, v4, v5, v6}, Landroid/speech/tts/SynthesisCallback;->audioAvailable([BII)I

    :cond_0
    const/4 v5, 0x1

    return v5

    :cond_1
    move-wide v0, p1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getLanguage()[Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mBaseDir:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mBaseDir:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/tts/local/PattsSynthesizer;->getLocaleFromBaseDir(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mLangCountryHelper:Lcom/google/android/tts/local/LangCountryHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/local/LangCountryHelper;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public declared-synchronized onClose()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/tts/local/PattsSynthesizer;->destroySynthesizer()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onInit()V
    .locals 1

    invoke-static {}, Lcom/google/speech/patts/api/android/StaticJniLoader;->loadJni()Z

    const/16 v0, 0x400

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    return-void
.end method

.method public declared-synchronized onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v3, -0x2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/tts/local/PattsSynthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mLangCountryHelper:Lcom/google/android/tts/local/LangCountryHelper;

    invoke-virtual {v4, p1}, Lcom/google/android/tts/local/LangCountryHelper;->getAlternateCountry(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v1, -0x2

    :cond_0
    :goto_0
    if-ne v1, v3, :cond_3

    :cond_1
    :goto_1
    monitor-exit p0

    return v1

    :cond_2
    :try_start_1
    invoke-virtual {p0, p1, v0}, Lcom/google/android/tts/local/PattsSynthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eq v1, v3, :cond_0

    const-string v4, "PattsSynthesizer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onLoadLanguage : Falling back to Alternate Country: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for langauage "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object p2, v0

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/local/PattsSynthesizer;->getBaseDir(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mBaseDir:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v4}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_4
    invoke-direct {p0}, Lcom/google/android/tts/local/PattsSynthesizer;->destroySynthesizer()V

    iget-object v4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/local/PattsSynthesizer;->isLocaleInApk(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->enableDataInApk(Z)V

    invoke-direct {p0, v2}, Lcom/google/android/tts/local/PattsSynthesizer;->initializeSynthesizer(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v4}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "PattsSynthesizer"

    const-string v5, "onLoadLanguage : Synthesizer failed to initialize"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v3

    goto :goto_1

    :cond_5
    iput-object v2, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mBaseDir:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public onStop()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mStopRequested:Z

    return-void
.end method

.method public onSynthesize(Lcom/google/android/tts/GoogleTtsRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 1
    .param p1    # Lcom/google/android/tts/GoogleTtsRequest;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Landroid/speech/tts/SynthesisCallback;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mStopRequested:Z

    invoke-virtual {p0, p1, p2}, Lcom/google/android/tts/local/PattsSynthesizer;->synthesizeBuffered(Lcom/google/android/tts/GoogleTtsRequest;Landroid/speech/tts/SynthesisCallback;)V

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->setServed()V

    return-void
.end method

.method declared-synchronized synthesizeBuffered(Lcom/google/android/tts/GoogleTtsRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 10
    .param p1    # Lcom/google/android/tts/GoogleTtsRequest;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Landroid/speech/tts/SynthesisCallback;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v9, 0x2

    const/4 v8, 0x1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/tts/local/PattsSynthesizer;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x2

    if-ne v4, v5, :cond_0

    const-string v4, "PattsSynthesizer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Synthesis request for unsupported locale: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getCountry()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v4}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "PattsSynthesizer"

    const-string v5, "Synthesis requested before engine intialized: "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_1
    :try_start_2
    new-instance v0, Lcom/google/speech/patts/engine/api/SentenceControls;

    invoke-direct {v0}, Lcom/google/speech/patts/engine/api/SentenceControls;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getSpeechRate()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/tts/local/PattsSynthesizer;->normalizeSpeechRate(I)F

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/speech/patts/engine/api/SentenceControls;->setSpeakingRate(F)V

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getPitch()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/tts/local/PattsSynthesizer;->normalizePitch(I)F

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/speech/patts/engine/api/SentenceControls;->setPitchMultFactor(F)V

    iget-object v4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mProcessor:Lcom/google/android/tts/local/TextPreprocessor;

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/tts/GoogleTtsRequest;->getCountry()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/tts/local/TextPreprocessor;->process(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/tts/local/PattsSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v4}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->getSampleRate()I

    move-result v4

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-interface {p2, v4, v5, v6}, Landroid/speech/tts/SynthesisCallback;->start(III)I

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/tts/local/Utterance;

    iget v4, v2, Lcom/google/android/tts/local/Utterance;->mType:I

    if-ne v4, v8, :cond_4

    iget-object v4, v2, Lcom/google/android/tts/local/Utterance;->mText:Ljava/lang/String;

    invoke-direct {p0, v4, v0, p2}, Lcom/google/android/tts/local/PattsSynthesizer;->synthesizeSentence(Ljava/lang/String;Lcom/google/speech/patts/engine/api/SentenceControls;Landroid/speech/tts/SynthesisCallback;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_3
    :goto_1
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I

    goto :goto_0

    :cond_4
    iget v4, v2, Lcom/google/android/tts/local/Utterance;->mType:I

    if-ne v4, v9, :cond_2

    iget-wide v4, v2, Lcom/google/android/tts/local/Utterance;->mDurationMs:J

    invoke-direct {p0, v4, v5, p2}, Lcom/google/android/tts/local/PattsSynthesizer;->synthesizeSilence(JLandroid/speech/tts/SynthesisCallback;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-nez v4, :cond_2

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PattsSynthesizer["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
