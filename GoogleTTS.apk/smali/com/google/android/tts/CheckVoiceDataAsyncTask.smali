.class public Lcom/google/android/tts/CheckVoiceDataAsyncTask;
.super Landroid/os/AsyncTask;
.source "CheckVoiceDataAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/app/Activity;",
        "Ljava/lang/Void;",
        "Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private static filter(Ljava/util/ArrayList;Ljava/util/Set;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/app/Activity;)Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;
    .locals 24
    .param p1    # [Landroid/app/Activity;

    const/16 v22, 0x0

    aget-object v10, p1, v22

    invoke-static {v10}, Lcom/google/android/tts/GoogleTtsApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/GoogleTtsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDataManager()Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v21

    invoke-virtual {v3}, Lcom/google/android/tts/GoogleTtsApplication;->getMetadataManager()Lcom/google/android/tts/voicepack/MetadataManager;

    move-result-object v15

    invoke-virtual {v3}, Lcom/google/android/tts/GoogleTtsApplication;->getLangCountryHelper()Lcom/google/android/tts/local/LangCountryHelper;

    move-result-object v11

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/tts/voicepack/VoiceDataManager;->updateAvailableLangs()V

    invoke-virtual {v11}, Lcom/google/android/tts/local/LangCountryHelper;->updateLocaleList()V

    invoke-virtual {v11}, Lcom/google/android/tts/local/LangCountryHelper;->getLanguages()[Ljava/lang/String;

    move-result-object v4

    array-length v14, v4

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v14, :cond_0

    aget-object v13, v4, v12

    invoke-interface {v6, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v15}, Lcom/google/android/tts/voicepack/MetadataManager;->forceEvictionAndUpdate()V

    invoke-virtual {v15}, Lcom/google/android/tts/voicepack/MetadataManager;->getDownloadableLocales()Ljava/util/Set;

    move-result-object v7

    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v22

    move/from16 v0, v22

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_1

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v10}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v22

    const-string v23, "checkVoiceDataFor"

    invoke-virtual/range {v22 .. v23}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_3

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9, v8}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v5, v9}, Lcom/google/android/tts/CheckVoiceDataAsyncTask;->filter(Ljava/util/ArrayList;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v0, v9}, Lcom/google/android/tts/CheckVoiceDataAsyncTask;->filter(Ljava/util/ArrayList;Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v20

    :cond_3
    new-instance v18, Landroid/content/Intent;

    invoke-direct/range {v18 .. v18}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v22

    if-lez v22, :cond_4

    const-string v22, "availableVoices"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_4
    const/16 v17, 0x1

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v22

    if-nez v22, :cond_5

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/tts/voicepack/VoiceDataManager;->hasDownloadedLocales()Z

    move-result v22

    if-eqz v22, :cond_5

    const-string v22, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    const-string v22, "unavailableVoices"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    new-instance v16, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;-><init>()V

    move-object/from16 v0, v16

    iput-object v10, v0, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;->checkVoiceDataActivity:Landroid/app/Activity;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;->outputIntent:Landroid/content/Intent;

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;->returnCode:I

    return-object v16
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/google/android/tts/CheckVoiceDataAsyncTask;->doInBackground([Landroid/app/Activity;)Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;)V
    .locals 3
    .param p1    # Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;

    iget-object v0, p1, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;->checkVoiceDataActivity:Landroid/app/Activity;

    iget v1, p1, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;->returnCode:I

    iget-object v2, p1, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;->outputIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p1, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;->checkVoiceDataActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;

    invoke-virtual {p0, p1}, Lcom/google/android/tts/CheckVoiceDataAsyncTask;->onPostExecute(Lcom/google/android/tts/CheckVoiceDataAsyncTask$Output;)V

    return-void
.end method
