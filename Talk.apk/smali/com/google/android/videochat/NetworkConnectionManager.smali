.class Lcom/google/android/videochat/NetworkConnectionManager;
.super Ljava/lang/Object;
.source "NetworkConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;
    }
.end annotation


# instance fields
.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mMobileHipriRunnable:Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;

.field private mRequestHipri:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videochat/NetworkConnectionManager;)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/NetworkConnectionManager;

    invoke-direct {p0}, Lcom/google/android/videochat/NetworkConnectionManager;->requestMobileHipriNetwork()V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "vclib:NetworkConnectionManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[NetConMgr] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private requestMobileHipriNetwork()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mRequestHipri:Z

    if-nez v1, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/4 v2, 0x0

    const-string v3, "enableHIPRI"

    invoke-virtual {v1, v2, v3}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>> requestMobileHipriNetwork: enableHIPRI result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videochat/NetworkConnectionManager;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mMobileHipriRunnable:Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;

    invoke-direct {v1, p0}, Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;-><init>(Lcom/google/android/videochat/NetworkConnectionManager;)V

    iput-object v1, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mMobileHipriRunnable:Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;

    :cond_1
    iget-object v1, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mMobileHipriRunnable:Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;

    const-wide/16 v3, 0x61a8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public startUsingMobileHipriIfOnMobileNetwork()V
    .locals 5

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startUsingMobileHipriIfOnMobileNetwork: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/videochat/NetworkConnectionManager;->log(Ljava/lang/String;)V

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    :sswitch_0
    iget-object v2, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "gtalk_use_hipri"

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mRequestHipri:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/videochat/NetworkConnectionManager;->requestMobileHipriNetwork()V

    goto :goto_1

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method public stopUsingMobileHipri()V
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "stopUsingMobileHipri: mRequestHipri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mRequestHipri:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videochat/NetworkConnectionManager;->log(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mRequestHipri:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mRequestHipri:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mMobileHipriRunnable:Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mMobileHipriRunnable:Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videochat/NetworkConnectionManager;->mMobileHipriRunnable:Lcom/google/android/videochat/NetworkConnectionManager$MobileHipriRunnable;

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
