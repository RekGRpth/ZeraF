.class public Lcom/google/android/videochat/RemoteRenderer;
.super Lcom/google/android/videochat/Renderer;
.source "RemoteRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videochat/RemoteRenderer$1;,
        Lcom/google/android/videochat/RemoteRenderer$Stats;,
        Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/google/android/videochat/Renderer$RendererThreadCallback;

.field private mFirstStatsUpdateTime:J

.field private mHaveSeenFirstFrame:Z

.field private mLastStatsUpdateTime:J

.field private mOutputTextureName:I

.field mStats:Lcom/google/android/videochat/util/CircularArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videochat/util/CircularArray",
            "<",
            "Lcom/google/android/videochat/RemoteRenderer$Stats;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/videochat/RendererManager;Lcom/google/android/videochat/Renderer$RendererThreadCallback;)V
    .locals 3
    .param p1    # Lcom/google/android/videochat/RendererManager;
    .param p2    # Lcom/google/android/videochat/Renderer$RendererThreadCallback;

    invoke-direct {p0}, Lcom/google/android/videochat/Renderer;-><init>()V

    new-instance v0, Lcom/google/android/videochat/util/CircularArray;

    const/16 v1, 0x78

    invoke-direct {v0, v1}, Lcom/google/android/videochat/util/CircularArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mStats:Lcom/google/android/videochat/util/CircularArray;

    iput-object p1, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    iput-object p2, p0, Lcom/google/android/videochat/RemoteRenderer;->mCallback:Lcom/google/android/videochat/Renderer$RendererThreadCallback;

    iget-object v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/RendererManager;->instantiateRenderer(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    const-string v0, "vclib:RemoteRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "construct "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videochat/util/LogUtil;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videochat/RendererManager;->registerRendererForStats(Lcom/google/android/videochat/Renderer;)V

    return-void
.end method


# virtual methods
.method public drawTexture(Lcom/google/android/videochat/Renderer$DrawInputParams;Lcom/google/android/videochat/Renderer$DrawOutputParams;)V
    .locals 9
    .param p1    # Lcom/google/android/videochat/Renderer$DrawInputParams;
    .param p2    # Lcom/google/android/videochat/Renderer$DrawOutputParams;

    const/4 v8, 0x0

    move-object v0, p2

    check-cast v0, Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;

    iget-object v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    iget v5, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    invoke-virtual {v4, v5, v8, p2}, Lcom/google/android/videochat/RendererManager;->renderFrame(ILjava/lang/Object;Ljava/lang/Object;)V

    iget-boolean v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mHaveSeenFirstFrame:Z

    if-nez v4, :cond_1

    iget-boolean v4, v0, Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;->updatedTexture:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mCallback:Lcom/google/android/videochat/Renderer$RendererThreadCallback;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mCallback:Lcom/google/android/videochat/Renderer$RendererThreadCallback;

    invoke-interface {v4}, Lcom/google/android/videochat/Renderer$RendererThreadCallback;->onInitialFrame()V

    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mHaveSeenFirstFrame:Z

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mLastStatsUpdateTime:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x2710

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    new-instance v1, Lcom/google/android/videochat/RemoteRenderer$Stats;

    invoke-direct {v1, p0, v8}, Lcom/google/android/videochat/RemoteRenderer$Stats;-><init>(Lcom/google/android/videochat/RemoteRenderer;Lcom/google/android/videochat/RemoteRenderer$1;)V

    iput-wide v2, v1, Lcom/google/android/videochat/RemoteRenderer$Stats;->time:J

    iget-object v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    iget v5, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    const-string v6, "yuv_dropped"

    invoke-virtual {v4, v5, v6}, Lcom/google/android/videochat/RendererManager;->getIntParam(ILjava/lang/String;)I

    move-result v4

    iput v4, v1, Lcom/google/android/videochat/RemoteRenderer$Stats;->dropped:I

    iget-object v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    iget v5, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    const-string v6, "yuv_pushed"

    invoke-virtual {v4, v5, v6}, Lcom/google/android/videochat/RendererManager;->getIntParam(ILjava/lang/String;)I

    move-result v4

    iput v4, v1, Lcom/google/android/videochat/RemoteRenderer$Stats;->pushed:I

    iget-object v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    iget v5, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    const-string v6, "yuv_rendered"

    invoke-virtual {v4, v5, v6}, Lcom/google/android/videochat/RendererManager;->getIntParam(ILjava/lang/String;)I

    move-result v4

    iput v4, v1, Lcom/google/android/videochat/RemoteRenderer$Stats;->renderered:I

    iget-object v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    iget v5, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    const-string v6, "sub_renderedframes"

    invoke-virtual {v4, v5, v6}, Lcom/google/android/videochat/RendererManager;->getIntParam(ILjava/lang/String;)I

    move-result v4

    iput v4, v1, Lcom/google/android/videochat/RemoteRenderer$Stats;->rendererCalls:I

    iget-object v5, p0, Lcom/google/android/videochat/RemoteRenderer;->mStats:Lcom/google/android/videochat/util/CircularArray;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/videochat/RemoteRenderer;->mStats:Lcom/google/android/videochat/util/CircularArray;

    invoke-virtual {v4, v1}, Lcom/google/android/videochat/util/CircularArray;->add(Ljava/lang/Object;)V

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-wide v2, p0, Lcom/google/android/videochat/RemoteRenderer;->mLastStatsUpdateTime:J

    :cond_2
    return-void

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 12
    .param p1    # Ljava/io/PrintWriter;

    const-string v6, "Remote Renderer -- dropped, pushed, new frames renderered, rendered"

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/videochat/RemoteRenderer$Stats;

    const/4 v6, 0x0

    invoke-direct {v4, p0, v6}, Lcom/google/android/videochat/RemoteRenderer$Stats;-><init>(Lcom/google/android/videochat/RemoteRenderer;Lcom/google/android/videochat/RemoteRenderer$1;)V

    iget-wide v6, p0, Lcom/google/android/videochat/RemoteRenderer;->mFirstStatsUpdateTime:J

    iput-wide v6, v4, Lcom/google/android/videochat/RemoteRenderer$Stats;->time:J

    iget-object v7, p0, Lcom/google/android/videochat/RemoteRenderer;->mStats:Lcom/google/android/videochat/util/CircularArray;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/google/android/videochat/RemoteRenderer;->mStats:Lcom/google/android/videochat/util/CircularArray;

    invoke-virtual {v6}, Lcom/google/android/videochat/util/CircularArray;->count()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    iget-object v6, p0, Lcom/google/android/videochat/RemoteRenderer;->mStats:Lcom/google/android/videochat/util/CircularArray;

    invoke-virtual {v6, v3}, Lcom/google/android/videochat/util/CircularArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/videochat/RemoteRenderer$Stats;

    new-instance v1, Ljava/util/Date;

    iget-wide v8, v5, Lcom/google/android/videochat/RemoteRenderer$Stats;->time:J

    invoke-direct {v1, v8, v9}, Ljava/util/Date;-><init>(J)V

    iget-wide v8, v5, Lcom/google/android/videochat/RemoteRenderer$Stats;->time:J

    iget-wide v10, v4, Lcom/google/android/videochat/RemoteRenderer$Stats;->time:J

    sub-long/2addr v8, v10

    long-to-float v6, v8

    const/high16 v8, 0x447a0000

    div-float v2, v6, v8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " -- "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, v5, Lcom/google/android/videochat/RemoteRenderer$Stats;->dropped:I

    iget v9, v4, Lcom/google/android/videochat/RemoteRenderer$Stats;->dropped:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    div-float/2addr v8, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, v5, Lcom/google/android/videochat/RemoteRenderer$Stats;->pushed:I

    iget v9, v4, Lcom/google/android/videochat/RemoteRenderer$Stats;->pushed:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    div-float/2addr v8, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, v5, Lcom/google/android/videochat/RemoteRenderer$Stats;->renderered:I

    iget v9, v4, Lcom/google/android/videochat/RemoteRenderer$Stats;->renderered:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    div-float/2addr v8, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, v5, Lcom/google/android/videochat/RemoteRenderer$Stats;->rendererCalls:I

    iget v9, v4, Lcom/google/android/videochat/RemoteRenderer$Stats;->rendererCalls:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    div-float/2addr v8, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move-object v4, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v7

    return-void

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method public getOutputTextureName()I
    .locals 1

    iget v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mOutputTextureName:I

    return v0
.end method

.method public initializeGLContext()V
    .locals 3

    const-string v0, "vclib:RemoteRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeGLContext "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videochat/util/LogUtil;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    iget v1, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/RendererManager;->initializeGLContext(I)Z

    iget-object v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    iget v1, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    const-string v2, "sub_outtex"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videochat/RendererManager;->getIntParam(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mOutputTextureName:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mHaveSeenFirstFrame:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mFirstStatsUpdateTime:J

    iput-wide v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mLastStatsUpdateTime:J

    return-void
.end method

.method public release()V
    .locals 3

    const-string v0, "vclib:RemoteRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "release "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videochat/util/LogUtil;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videochat/RendererManager;->unregisterRendererForStats(Lcom/google/android/videochat/Renderer;)V

    iget-object v0, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererManager:Lcom/google/android/videochat/RendererManager;

    iget v1, p0, Lcom/google/android/videochat/RemoteRenderer;->mRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/RendererManager;->releaseRenderer(I)V

    return-void
.end method
