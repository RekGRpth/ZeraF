.class public Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;
.super Ljava/lang/Object;
.source "JingleAndGingleStanzaParser.java"


# instance fields
.field public mId:Ljava/lang/String;

.field private mIsJingle:Z

.field public mRawStanza:Ljava/lang/String;

.field public mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->parseStanzaString(Ljava/lang/String;)Z

    return-void
.end method

.method public static makeParser(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;
    .locals 5
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const-string v3, "vclib:JingleAndGingleStanzaParser"

    const-string v4, "Couldn\'t parse stanza"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private parseJingleAttributes(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 6
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    if-eqz v2, :cond_0

    if-nez v4, :cond_3

    :cond_0
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    if-nez v2, :cond_2

    const-string v5, "sid"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mId:Ljava/lang/String;

    const/4 v2, 0x1

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    if-nez v4, :cond_1

    const-string v5, "action"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mType:Ljava/lang/String;

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method private parseSessionAttributes(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 6
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    if-eqz v2, :cond_0

    if-nez v4, :cond_3

    :cond_0
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    if-nez v2, :cond_2

    const-string v5, "id"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mId:Ljava/lang/String;

    const/4 v2, 0x1

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    if-nez v4, :cond_1

    const-string v5, "type"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mType:Ljava/lang/String;

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method private parseStanzaString(Ljava/lang/String;)Z
    .locals 12
    .param p1    # Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v9, 0x0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mRawStanza:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mRawStanza:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->makeParser(Ljava/lang/String;)Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    :goto_0
    if-eq v2, v8, :cond_0

    const/4 v10, 0x2

    if-ne v2, v10, :cond_2

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v1

    const-string v10, "session"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const-string v10, "http://www.google.com/session"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-direct {p0, v6}, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->parseSessionAttributes(Lorg/xmlpull/v1/XmlPullParser;)V

    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mIsJingle:Z

    :cond_0
    :goto_1
    return v8

    :cond_1
    const-string v10, "jingle"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v10, "urn:xmpp:jingle:1"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-direct {p0, v6}, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->parseJingleAttributes(Lorg/xmlpull/v1/XmlPullParser;)V

    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mIsJingle:Z
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_1

    :catch_0
    move-exception v7

    const-string v8, "vclib:JingleAndGingleStanzaParser"

    const-string v10, "Couldn\'t parse stanza"

    invoke-static {v8, v10, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v8, v9

    goto :goto_1

    :cond_2
    :try_start_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result v2

    goto :goto_0

    :catch_1
    move-exception v5

    const-string v8, "vclib:JingleAndGingleStanzaParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Couldn\'t parse the stanza "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v8, v9

    goto :goto_1

    :catch_2
    move-exception v4

    const-string v8, "vclib:JingleAndGingleStanzaParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Couldn\'t parse the sessionId "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v8, v9

    goto :goto_1

    :catch_3
    move-exception v3

    const-string v8, "vclib:JingleAndGingleStanzaParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Couldn\'t parse the stanza "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v8, v9

    goto :goto_1
.end method


# virtual methods
.method public isJingle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mIsJingle:Z

    return v0
.end method

.method public isTypeInitiate()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->isJingle()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "session-initiate"

    iget-object v1, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "initiate"

    iget-object v1, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isTypeTerminate()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->isJingle()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "session-terminate"

    iget-object v1, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "terminate"

    iget-object v1, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isValidJingleOrGingleStanza()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videochat/util/JingleAndGingleStanzaParser;->mType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
