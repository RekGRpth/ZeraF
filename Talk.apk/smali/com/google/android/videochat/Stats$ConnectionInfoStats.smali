.class public Lcom/google/android/videochat/Stats$ConnectionInfoStats;
.super Lcom/google/android/videochat/Stats;
.source "Stats.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videochat/Stats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConnectionInfoStats"
.end annotation


# instance fields
.field public flags:I

.field public localAddress:Ljava/lang/String;

.field public localProtocol:Ljava/lang/String;

.field public localType:Ljava/lang/String;

.field public mediaType:I

.field public receivedBitrate:I

.field public receivedBytes:I

.field public remoteAddress:Ljava/lang/String;

.field public remoteProtocol:Ljava/lang/String;

.field public remoteType:Ljava/lang/String;

.field public rtt:I

.field public sentBitrate:I

.field public sentBytes:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIII)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # I
    .param p12    # I
    .param p13    # I

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/videochat/Stats;-><init>(I)V

    iput p1, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->mediaType:I

    iput-object p2, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->localType:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->localAddress:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->localProtocol:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->remoteType:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->remoteAddress:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->remoteProtocol:Ljava/lang/String;

    iput p8, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->receivedBytes:I

    iput p9, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->receivedBitrate:I

    iput p10, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->sentBytes:I

    iput p11, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->sentBitrate:I

    iput p12, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->rtt:I

    iput p13, p0, Lcom/google/android/videochat/Stats$ConnectionInfoStats;->flags:I

    return-void
.end method
