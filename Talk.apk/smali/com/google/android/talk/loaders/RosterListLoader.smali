.class public Lcom/google/android/talk/loaders/RosterListLoader;
.super Landroid/content/CursorLoader;
.source "RosterListLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/loaders/RosterListLoader$2;,
        Lcom/google/android/talk/loaders/RosterListLoader$SortMode;,
        Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;
    }
.end annotation


# static fields
.field static final PROJECTION:[Ljava/lang/String;

.field static final PROJECTION_NOAVATAR:[Ljava/lang/String;

.field static final mSelectionArgs:[Ljava/lang/String;

.field static final sSearchSelection:Ljava/lang/String;


# instance fields
.field private mAccountId:J

.field private mConstraint:Ljava/lang/String;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContentObserverRegistered:Z

.field private mFilterMode:Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

.field protected mLogLevel:I

.field private mOnFinishedLoading:Ljava/lang/Runnable;

.field private mProjection:[Ljava/lang/String;

.field private mSortMode:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

.field private mTablet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x3

    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "username"

    aput-object v1, v0, v5

    const-string v1, "nickname"

    aput-object v1, v0, v6

    const-string v1, "account"

    aput-object v1, v0, v3

    const-string v1, "type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "subscriptionStatus"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "subscriptionType"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "qc"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mode"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "client_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "groupchat"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "last_unread_message"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "last_message_date"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "is_active"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "avatars_hash"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "avatars_data"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "cap"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "chats._id as ChatsTableId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/talk/loaders/RosterListLoader;->PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "username"

    aput-object v1, v0, v5

    const-string v1, "nickname"

    aput-object v1, v0, v6

    const-string v1, "account"

    aput-object v1, v0, v3

    const-string v1, "type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "subscriptionStatus"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "subscriptionType"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "qc"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mode"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "client_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "groupchat"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "last_unread_message"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "last_message_date"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "is_active"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "avatars_hash"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "cap"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "chats._id as ChatsTableId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/talk/loaders/RosterListLoader;->PROJECTION_NOAVATAR:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/talk/loaders/RosterListLoader;->mSelectionArgs:[Ljava/lang/String;

    const-string v0, "(%s LIKE ?) OR (%s LIKE ?) OR (%s LIKE ?)"

    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "username"

    aput-object v2, v1, v4

    const-string v2, "nickname"

    aput-object v2, v1, v5

    const-string v2, "nickname"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/talk/loaders/RosterListLoader;->sSearchSelection:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    sget-object v2, Lcom/google/android/gsf/TalkContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/talk/loaders/RosterListLoader;->PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mLogLevel:I

    new-instance v0, Lcom/google/android/talk/loaders/RosterListLoader$1;

    invoke-direct {v0, p0, v4}, Lcom/google/android/talk/loaders/RosterListLoader$1;-><init>(Lcom/google/android/talk/loaders/RosterListLoader;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mContentObserver:Landroid/database/ContentObserver;

    sget-object v0, Lcom/google/android/talk/loaders/RosterListLoader;->PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mProjection:[Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/talk/ActivityUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mTablet:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;Ljava/lang/Runnable;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # J
    .param p4    # Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;
    .param p5    # Lcom/google/android/talk/loaders/RosterListLoader$SortMode;
    .param p6    # Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/google/android/talk/loaders/RosterListLoader;-><init>(Landroid/content/Context;)V

    iput-wide p2, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mAccountId:J

    iput-object p4, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mFilterMode:Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    iput-object p5, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mSortMode:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    iput-object p6, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mOnFinishedLoading:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/talk/loaders/RosterListLoader;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/loaders/RosterListLoader;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/talk/loaders/RosterListLoader;->logd(Ljava/lang/String;)V

    return-void
.end method

.method private computeSelectionString(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Ljava/lang/String;J)Ljava/lang/String;
    .locals 8
    .param p1    # Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader$2;->$SwitchMap$com$google$android$talk$loaders$RosterListLoader$FilterMode:[I

    invoke-virtual {p1}, Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/talk/loaders/RosterListLoader;->setSelectionArgs([Ljava/lang/String;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader;->mSelectionArgs:[Ljava/lang/String;

    aput-object v1, v2, v5

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader;->mSelectionArgs:[Ljava/lang/String;

    aput-object v1, v2, v6

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader;->mSelectionArgs:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%% "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader;->mSelectionArgs:[Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/talk/loaders/RosterListLoader;->setSelectionArgs([Ljava/lang/String;)V

    const-string v2, "%s AND (%s)"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v5

    sget-object v4, Lcom/google/android/talk/loaders/RosterListLoader;->sSearchSelection:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    :pswitch_0
    iget-boolean v2, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mTablet:Z

    if-eqz v2, :cond_1

    const-string v2, "account=%s AND type!=3 AND subscriptionType>=4 AND (qc=1 OR last_message_date NOTNULL OR subscriptionType=5)"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v2, "account=%s AND type!=3 AND subscriptionType>=4 AND (qc=1 OR last_message_date!=0 OR subscriptionType=5)"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mTablet:Z

    if-eqz v2, :cond_2

    const-string v2, "account=%s AND type!=3 AND subscriptionType>=4 AND (type!=4 OR last_message_date NOTNULL)"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    const-string v2, "account=%s AND type!=3 AND subscriptionType>=4 AND (type!=4 OR last_message_date!=0)"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_2
    const-string v2, "account=%s AND type!=3 AND subscriptionType>=4"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static computeSortOrderString(Lcom/google/android/talk/loaders/RosterListLoader$SortMode;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    sget-object v0, Lcom/google/android/talk/loaders/RosterListLoader$2;->$SwitchMap$com$google$android$talk$loaders$RosterListLoader$SortMode:[I

    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "subscriptionType DESC, (is_active != 0) DESC, mode DESC, nickname COLLATE UNICODE ASC"

    goto :goto_0

    :pswitch_1
    const-string v0, "subscriptionType DESC, (is_active != 0) DESC, nickname COLLATE UNICODE ASC"

    goto :goto_0

    :pswitch_2
    const-string v0, "subscriptionType DESC, (is_active != 0) DESC, chats.last_message_date DESC, nickname COLLATE UNICODE ASC"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private logd(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x3

    const-string v1, "talk"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[RosterListLoader] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/talk/TalkApp;->LOG(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private logv(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x2

    const-string v1, "talk"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[RosterListLoader] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/talk/TalkApp;->LOG(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public deliverResult(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    const-string v0, "onLoadComplete"

    invoke-direct {p0, v0}, Lcom/google/android/talk/loaders/RosterListLoader;->logv(Ljava/lang/String;)V

    if-nez p1, :cond_1

    const-string v0, "talk"

    const-string v1, "Query failed. Try again without the avatar column."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/android/talk/loaders/RosterListLoader;->PROJECTION_NOAVATAR:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/loaders/RosterListLoader;->setProjection([Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->forceLoad()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Landroid/content/CursorLoader;->deliverResult(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mOnFinishedLoading:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mOnFinishedLoading:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/google/android/talk/loaders/RosterListLoader;->deliverResult(Landroid/database/Cursor;)V

    return-void
.end method

.method public onContentChanged()V
    .locals 0

    return-void
.end method

.method protected onForceLoad()V
    .locals 1

    const-string v0, "forceLoad"

    invoke-direct {p0, v0}, Lcom/google/android/talk/loaders/RosterListLoader;->logd(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->prepareSelectionClause()V

    invoke-super {p0}, Landroid/content/CursorLoader;->onForceLoad()V

    return-void
.end method

.method protected onStartLoading()V
    .locals 5

    const/4 v4, 0x1

    iget-boolean v1, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mContentObserverRegistered:Z

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "##### startLoading: register content observer for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/gsf/TalkContract$Contacts;->CONTENT_URI_CHAT_CONTACTS:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/talk/loaders/RosterListLoader;->logd(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/gsf/TalkContract$Contacts;->CONTENT_URI_CHAT_CONTACTS:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iput-boolean v4, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mContentObserverRegistered:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->prepareSelectionClause()V

    iget v1, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mLogLevel:I

    if-lt v1, v4, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "startQuery(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "projection: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mProjection:[Ljava/lang/String;

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader;->PROJECTION:[Ljava/lang/String;

    if-ne v1, v2, :cond_2

    const-string v1, "full"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "selection: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->getSelection()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/talk/loaders/RosterListLoader;->logd(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->cancelLoad()Z

    invoke-super {p0}, Landroid/content/CursorLoader;->onStartLoading()V

    return-void

    :cond_2
    const-string v1, "no avatar"

    goto :goto_0
.end method

.method protected onStopLoading()V
    .locals 2

    invoke-super {p0}, Landroid/content/CursorLoader;->onStopLoading()V

    iget-boolean v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mContentObserverRegistered:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "##### stopLoading: unregister content observer for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/TalkContract$Contacts;->CONTENT_URI_CHAT_CONTACTS:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/talk/loaders/RosterListLoader;->logd(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mContentObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected prepareSelectionClause()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mFilterMode:Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    iget-object v1, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mConstraint:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mAccountId:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/talk/loaders/RosterListLoader;->computeSelectionString(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/talk/loaders/RosterListLoader;->setSelection(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mSortMode:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    invoke-static {v0}, Lcom/google/android/talk/loaders/RosterListLoader;->computeSortOrderString(Lcom/google/android/talk/loaders/RosterListLoader$SortMode;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/talk/loaders/RosterListLoader;->setSortOrder(Ljava/lang/String;)V

    return-void
.end method

.method public resetProjection()V
    .locals 1

    sget-object v0, Lcom/google/android/talk/loaders/RosterListLoader;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/loaders/RosterListLoader;->setProjection([Ljava/lang/String;)V

    return-void
.end method

.method public setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;)V
    .locals 1
    .param p1    # Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;
    .param p2    # Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/talk/loaders/RosterListLoader;->setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->forceLoad()V

    return-void
.end method

.method public setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;
    .param p2    # Lcom/google/android/talk/loaders/RosterListLoader$SortMode;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mConstraint:Ljava/lang/String;

    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mFilterMode:Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mSortMode:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    if-eq p2, v0, :cond_1

    :cond_0
    iput-object p3, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mConstraint:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mFilterMode:Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    iput-object p2, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mSortMode:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->forceLoad()V

    :cond_1
    return-void
.end method

.method public switchAccounts(J)V
    .locals 2
    .param p1    # J

    iget-wide v0, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mAccountId:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/talk/loaders/RosterListLoader;->mAccountId:J

    invoke-virtual {p0}, Lcom/google/android/talk/loaders/RosterListLoader;->forceLoad()V

    :cond_0
    return-void
.end method
