.class public Lcom/google/android/talk/videochat/IncomingCallRinger;
.super Ljava/lang/Object;
.source "IncomingCallRinger.java"

# interfaces
.implements Lcom/google/android/talk/videochat/CallRinger;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mRingCount:I

.field private mRingDelayRunnable:Ljava/lang/Runnable;

.field private final mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

.field private mRinging:Z

.field private mVibrateWhen:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;J)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Lcom/google/android/talk/videochat/IncomingCallRinger$1;

    invoke-direct {v2, p0}, Lcom/google/android/talk/videochat/IncomingCallRinger$1;-><init>(Lcom/google/android/talk/videochat/IncomingCallRinger;)V

    iput-object v2, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRingDelayRunnable:Ljava/lang/Runnable;

    new-instance v2, Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    invoke-direct {v2, p1}, Lcom/google/android/talk/videochat/NotificationRingtonePlayer;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    iput-object p2, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mHandler:Landroid/os/Handler;

    invoke-static {}, Lcom/google/android/talk/SettingsCache;->getInstance()Lcom/google/android/talk/SettingsCache;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Lcom/google/android/talk/SettingsCache;->getSettingsMap(J)Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getVideoVibrateWhen()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mVibrateWhen:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    iget-object v3, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mVibrateWhen:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/talk/videochat/NotificationRingtonePlayer;->setVibrateWhen(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    invoke-virtual {v0}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getVideoRingtoneURI()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/talk/videochat/NotificationRingtonePlayer;->setCustomRingtoneUri(Landroid/net/Uri;)V

    const-string v2, "phone"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/google/android/talk/videochat/NotificationRingtonePlayer;->setStreamType(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/talk/videochat/NotificationRingtonePlayer;->setStreamType(I)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/talk/videochat/IncomingCallRinger;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/videochat/IncomingCallRinger;

    invoke-direct {p0}, Lcom/google/android/talk/videochat/IncomingCallRinger;->ringAndRepeat()V

    return-void
.end method

.method private ringAndRepeat()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinging:Z

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRingCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRingCount:I

    iget-object v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/NotificationRingtonePlayer;->ring()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ringAndRepeat: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRingCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/IncomingCallRinger;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRingDelayRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[IncomingCallRinger] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public startRing()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRingCount:I

    iget-object v1, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinging:Z

    invoke-direct {p0}, Lcom/google/android/talk/videochat/IncomingCallRinger;->ringAndRepeat()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stopRing()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    monitor-enter v1

    :try_start_0
    const-string v0, "stopRing"

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/IncomingCallRinger;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinger:Lcom/google/android/talk/videochat/NotificationRingtonePlayer;

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/NotificationRingtonePlayer;->stopRing()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRinging:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/talk/videochat/IncomingCallRinger;->mRingDelayRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
