.class Lcom/google/android/talk/AvatarCache$WorkItem;
.super Ljava/lang/Object;
.source "AvatarCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/AvatarCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WorkItem"
.end annotation


# instance fields
.field mAccountId:J

.field mAvatarData:[B

.field mAvatarHash:Ljava/lang/String;

.field mAvatarLoadedMessage:Landroid/os/Message;

.field mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

.field mUsername:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/talk/AvatarCache;


# direct methods
.method constructor <init>(Lcom/google/android/talk/AvatarCache;Ljava/lang/String;[BLjava/lang/String;JLcom/google/android/talk/AvatarCache$BitmapCache;Landroid/os/Message;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # [B
    .param p4    # Ljava/lang/String;
    .param p5    # J
    .param p7    # Lcom/google/android/talk/AvatarCache$BitmapCache;
    .param p8    # Landroid/os/Message;

    iput-object p1, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->this$0:Lcom/google/android/talk/AvatarCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->mAvatarData:[B

    iput-object p4, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->mAvatarHash:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->mUsername:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->mAccountId:J

    iput-object p7, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    if-eqz p8, :cond_0

    iget-object v0, p8, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p8, Landroid/os/Message;->obj:Ljava/lang/Object;

    :cond_0
    iput-object p8, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->mAvatarLoadedMessage:Landroid/os/Message;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Lcom/google/android/talk/AvatarCache$WorkItem;

    iget-object v1, v0, Lcom/google/android/talk/AvatarCache$WorkItem;->mUsername:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->mUsername:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/google/android/talk/AvatarCache$WorkItem;->mAccountId:J

    iget-wide v3, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->mAccountId:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/AvatarCache$WorkItem;->mUsername:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
