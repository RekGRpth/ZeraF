.class public Lcom/google/android/talk/NetworkProgressMonitor;
.super Ljava/lang/Object;
.source "NetworkProgressMonitor.java"

# interfaces
.implements Lcom/google/android/talk/IProgressMonitor;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mDoneRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/Runnable;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/talk/NetworkProgressMonitor;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/talk/NetworkProgressMonitor;->mDoneRunnable:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public beginTask(Ljava/lang/CharSequence;I)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/talk/NetworkProgressMonitor;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFeatureInt(II)V

    return-void
.end method

.method public done()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/talk/NetworkProgressMonitor;->mDoneRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/NetworkProgressMonitor;->mDoneRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/NetworkProgressMonitor;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFeatureInt(II)V

    return-void
.end method
