.class public Lcom/google/android/talk/DrawerLayout;
.super Landroid/view/ViewGroup;
.source "DrawerLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/DrawerLayout$LayoutParams;,
        Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;
    }
.end annotation


# instance fields
.field private mAnimationCount:I

.field private mAnimationRunning:Z

.field private mAnimationStartTime:J

.field private mAnimator:Landroid/animation/ObjectAnimator;

.field private mContractAnimationDuration:J

.field private mDefaultToExpandedMode:Z

.field private mDividerWidth:I

.field private mExpandAnimationDuration:J

.field private mExpandStateChangedListener:Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;

.field private mExpandedState:Z

.field private mLeftView:Landroid/view/View;

.field private mResizeLeftViewDuringAnimation:Z

.field private mRightView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/talk/DrawerLayout;->initializeFromAttributes(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p2}, Lcom/google/android/talk/DrawerLayout;->initializeFromAttributes(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p2}, Lcom/google/android/talk/DrawerLayout;->initializeFromAttributes(Landroid/util/AttributeSet;)V

    return-void
.end method

.method private animate(JII)V
    .locals 4
    .param p1    # J
    .param p3    # I
    .param p4    # I

    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    const-string v0, "animate"

    new-array v1, v1, [I

    aput p3, v1, v2

    aput p4, v1, v3

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iput v2, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationCount:I

    iput-boolean v3, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationRunning:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationStartTime:J

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimator:Landroid/animation/ObjectAnimator;

    new-array v1, v1, [I

    aput p3, v1, v2

    aput p4, v1, v3

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    goto :goto_0
.end method

.method private computeWidthTarget()I
    .locals 2

    iget-object v1, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/google/android/talk/DrawerLayout;->getLayoutParams(Landroid/view/View;)Lcom/google/android/talk/DrawerLayout$LayoutParams;

    move-result-object v0

    iget v1, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->contractedWidth:I

    return v1
.end method

.method private getLayoutParams(Landroid/view/View;)Lcom/google/android/talk/DrawerLayout$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;

    return-object v0
.end method

.method private initializeFromAttributes(Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/util/AttributeSet;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/talk/DrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/talk/R$styleable;->DrawerLayout:[I

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/google/android/talk/DrawerLayout;->mExpandAnimationDuration:J

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/google/android/talk/DrawerLayout;->mContractAnimationDuration:J

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/talk/DrawerLayout;->mExpandedState:Z

    iput-boolean v1, p0, Lcom/google/android/talk/DrawerLayout;->mDefaultToExpandedMode:Z

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/talk/DrawerLayout;->mResizeLeftViewDuringAnimation:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private logV(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "DrawerLayout"

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private onExpandStateChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mExpandStateChangedListener:Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mExpandStateChangedListener:Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;

    invoke-interface {v0, p0}, Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;->onExpandStateCompleted(Lcom/google/android/talk/DrawerLayout;)V

    :cond_0
    return-void
.end method

.method private onExpandStateChanging(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mExpandStateChangedListener:Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mExpandStateChangedListener:Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;->onExpandStateStarted(Lcom/google/android/talk/DrawerLayout;Z)V

    :cond_0
    return-void
.end method

.method private setAnimate(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    iget v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationCount:I

    return-void
.end method

.method private setDividerWidth(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/talk/DrawerLayout;->mDividerWidth:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/talk/DrawerLayout;->mDividerWidth:I

    invoke-virtual {p0}, Lcom/google/android/talk/DrawerLayout;->requestLayout()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/util/AttributeSet;

    invoke-virtual {p0, p1}, Lcom/google/android/talk/DrawerLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/talk/DrawerLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/talk/DrawerLayout$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/talk/DrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/talk/DrawerLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public gotoDefaultExpandedState()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/talk/DrawerLayout;->mDefaultToExpandedMode:Z

    invoke-virtual {p0, v0}, Lcom/google/android/talk/DrawerLayout;->setExpanded(Z)V

    return-void
.end method

.method public isExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/talk/DrawerLayout;->mExpandedState:Z

    return v0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 3
    .param p1    # Landroid/animation/Animator;

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 7
    .param p1    # Landroid/animation/Animator;

    const/4 v6, 0x0

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "animation frames count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationStartTime:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/talk/DrawerLayout;->logV(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/talk/DrawerLayout;->onExpandStateChanged()V

    iput-boolean v5, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationRunning:Z

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v0, v5, v6}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    invoke-virtual {v0, v5, v6}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1    # Landroid/animation/Animator;

    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationCount:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationStartTime:J

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/google/android/talk/DrawerLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "DrawerLayout must have exactly two children"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/talk/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/talk/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    const v1, 0x7f100025

    invoke-virtual {p0, v1}, Lcom/google/android/talk/DrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/talk/DrawerLayout$1;

    invoke-direct {v1, p0}, Lcom/google/android/talk/DrawerLayout$1;-><init>(Lcom/google/android/talk/DrawerLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/talk/DrawerLayout;->onExpandStateChanged()V

    iget-object v1, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/google/android/talk/DrawerLayout;->getLayoutParams(Landroid/view/View;)Lcom/google/android/talk/DrawerLayout$LayoutParams;

    move-result-object v1

    iget v1, v1, Lcom/google/android/talk/DrawerLayout$LayoutParams;->contractedWidth:I

    invoke-direct {p0, v1}, Lcom/google/android/talk/DrawerLayout;->setDividerWidth(I)Z

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v4, v8, v8, v1, v0}, Landroid/view/View;->layout(IIII)V

    iget-object v4, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/talk/DrawerLayout$LayoutParams;

    iget-object v4, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    iget v5, p0, Lcom/google/android/talk/DrawerLayout;->mDividerWidth:I

    iget v6, v2, Lcom/google/android/talk/DrawerLayout$LayoutParams;->leftMargin:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/talk/DrawerLayout;->mDividerWidth:I

    add-int/2addr v6, v3

    iget v7, v2, Lcom/google/android/talk/DrawerLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v6, v7

    invoke-virtual {v4, v5, v8, v6, v0}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public onMeasure(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    const/high16 v11, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    iget-object v9, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-direct {p0, v9}, Lcom/google/android/talk/DrawerLayout;->getLayoutParams(Landroid/view/View;)Lcom/google/android/talk/DrawerLayout$LayoutParams;

    move-result-object v0

    iget-object v9, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    invoke-direct {p0, v9}, Lcom/google/android/talk/DrawerLayout;->getLayoutParams(Landroid/view/View;)Lcom/google/android/talk/DrawerLayout$LayoutParams;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/talk/DrawerLayout;->getWidth()I

    move-result v9

    if-eq v4, v9, :cond_2

    iget v9, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    if-nez v9, :cond_1

    iget v1, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->weight:F

    iget v6, v5, Lcom/google/android/talk/DrawerLayout$LayoutParams;->weight:F

    add-float v8, v1, v6

    const/4 v9, 0x0

    cmpl-float v9, v8, v9

    if-nez v9, :cond_0

    const/high16 v8, 0x3f800000

    :cond_0
    div-float v9, v1, v8

    int-to-float v10, v4

    mul-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    :cond_1
    iget v9, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    iget v10, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v9, v10

    iput v9, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    iget-boolean v9, p0, Lcom/google/android/talk/DrawerLayout;->mDefaultToExpandedMode:Z

    if-eqz v9, :cond_3

    iget v9, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    sub-int v9, v4, v9

    iput v9, v5, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    :goto_0
    iget v9, v5, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    iget v10, v5, Lcom/google/android/talk/DrawerLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v9, v10

    iput v9, v5, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    invoke-direct {p0}, Lcom/google/android/talk/DrawerLayout;->computeWidthTarget()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/google/android/talk/DrawerLayout;->setDividerWidth(I)Z

    :cond_2
    iget-boolean v9, p0, Lcom/google/android/talk/DrawerLayout;->mResizeLeftViewDuringAnimation:Z

    if-eqz v9, :cond_4

    iget v2, p0, Lcom/google/android/talk/DrawerLayout;->mDividerWidth:I

    :goto_1
    or-int v3, v2, v11

    iget-object v9, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v9, v3, p2}, Landroid/view/View;->measure(II)V

    iget-object v9, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    or-int v7, v9, v11

    iget-object v9, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    invoke-virtual {v9, v7, p2}, Landroid/view/View;->measure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v10

    invoke-virtual {p0, v9, v10}, Lcom/google/android/talk/DrawerLayout;->setMeasuredDimension(II)V

    return-void

    :cond_3
    iget v9, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->contractedWidth:I

    sub-int v9, v4, v9

    iput v9, v5, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    goto :goto_0

    :cond_4
    iget-boolean v9, p0, Lcom/google/android/talk/DrawerLayout;->mExpandedState:Z

    if-nez v9, :cond_5

    iget-boolean v9, p0, Lcom/google/android/talk/DrawerLayout;->mAnimationRunning:Z

    if-nez v9, :cond_5

    iget v2, v0, Lcom/google/android/talk/DrawerLayout$LayoutParams;->contractedWidth:I

    goto :goto_1

    :cond_5
    iget-object v9, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v2, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_1
.end method

.method public setDefaultToExpanded(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/DrawerLayout;->mDefaultToExpandedMode:Z

    invoke-virtual {p0}, Lcom/google/android/talk/DrawerLayout;->requestLayout()V

    return-void
.end method

.method public setExpanded(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/talk/DrawerLayout;->setExpandedState(ZZ)V

    return-void
.end method

.method public setExpandedState(ZZ)V
    .locals 4
    .param p1    # Z
    .param p2    # Z

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/google/android/talk/DrawerLayout;->mExpandedState:Z

    if-ne v1, p1, :cond_0

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/talk/DrawerLayout;->mExpandedState:Z

    invoke-direct {p0, p1}, Lcom/google/android/talk/DrawerLayout;->onExpandStateChanging(Z)V

    iget-object v1, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/google/android/talk/DrawerLayout;->getLayoutParams(Landroid/view/View;)Lcom/google/android/talk/DrawerLayout$LayoutParams;

    move-result-object v1

    iget v1, v1, Lcom/google/android/talk/DrawerLayout$LayoutParams;->width:I

    iget-object v2, p0, Lcom/google/android/talk/DrawerLayout;->mLeftView:Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/google/android/talk/DrawerLayout;->getLayoutParams(Landroid/view/View;)Lcom/google/android/talk/DrawerLayout$LayoutParams;

    move-result-object v2

    iget v2, v2, Lcom/google/android/talk/DrawerLayout$LayoutParams;->contractedWidth:I

    sub-int v0, v1, v2

    if-nez p2, :cond_2

    iget-object v1, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    if-eqz p1, :cond_1

    iget-wide v1, p0, Lcom/google/android/talk/DrawerLayout;->mExpandAnimationDuration:J

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/google/android/talk/DrawerLayout;->animate(JII)V

    goto :goto_0

    :cond_1
    iget-wide v1, p0, Lcom/google/android/talk/DrawerLayout;->mContractAnimationDuration:J

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/talk/DrawerLayout;->animate(JII)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/talk/DrawerLayout;->computeWidthTarget()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/talk/DrawerLayout;->setDividerWidth(I)Z

    iget-object v2, p0, Lcom/google/android/talk/DrawerLayout;->mRightView:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/talk/DrawerLayout;->mExpandedState:Z

    if-eqz v1, :cond_3

    int-to-float v1, v0

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setOnExpandStateChangedListener(Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;

    iput-object p1, p0, Lcom/google/android/talk/DrawerLayout;->mExpandStateChangedListener:Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/google/android/talk/DrawerLayout$ExpandStateChangedListener;->onExpandStateCompleted(Lcom/google/android/talk/DrawerLayout;)V

    :cond_0
    return-void
.end method
