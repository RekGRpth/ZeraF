.class public Lcom/google/android/talk/fragments/SettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;
    }
.end annotation


# static fields
.field private static final KEY_BLOCKED_BUDDIES:Ljava/lang/String; = "pref_gtalk_blocked_buddies_key"

.field private static final KEY_CLEAR_HISTORY:Ljava/lang/String; = "pref_gtalk_clear_history_key"

.field private static final KEY_MANAGE_ACCOUNT:Ljava/lang/String; = "pref_gtalk_manage_account_key"

.field private static final KEY_MOBILE_INDICATOR:Ljava/lang/String; = "gtalk-mobile-indicator"

.field private static final KEY_SHOW_AWAY_ON_IDLE:Ljava/lang/String; = "gtalk-show-away-on-idle"

.field private static final KEY_SHOW_FRIEND_NOTIFICATIONS:Ljava/lang/String; = "gtalk-show-friend-notifications"

.field private static final KEY_TERMS:Ljava/lang/String; = "gtalk-terms-key"

.field private static final KEY_TEXT_GROUP:Ljava/lang/String; = "text_notification_group"

.field private static final KEY_TEXT_NOTIFICATIONS:Ljava/lang/String; = "gtalk-enable-notifications"

.field private static final KEY_TEXT_RINGTONE:Ljava/lang/String; = "gtalk-text-ringtone"

.field private static final KEY_TEXT_VIBRATE_WHEN:Ljava/lang/String; = "gtalk-vibrate-when"

.field private static final KEY_VIDEO_GROUP:Ljava/lang/String; = "video_notification_group"

.field private static final KEY_VIDEO_IMAGE_STABILIZATION:Ljava/lang/String; = "gtalk-video-image-image-stabilization"

.field private static final KEY_VIDEO_NOTIFICATIONS:Ljava/lang/String; = "gtalk-video-enable-notifications"

.field private static final KEY_VIDEO_RINGTONE:Ljava/lang/String; = "gtalk-video-ringtone"

.field private static final KEY_VIDEO_VIBRATE_WHEN:Ljava/lang/String; = "gtalk-video-vibrate-when"

.field private static final VIDEO_IMAGE_STABILIZATION_FACE_TRACKING_INDEX:I = 0x3

.field private static final VIDEO_IMAGE_STABILIZATION_LOW_INDEX:I = 0x1

.field private static final VIDEO_IMAGE_STABILIZATION_MEDIUM_INDEX:I = 0x2

.field private static final VIDEO_IMAGE_STABILIZATION_OFF_INDEX:I


# instance fields
.field private mAccountId:J

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private adjustImageStabilizationSummary(Landroid/preference/ListPreference;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/preference/ListPreference;
    .param p2    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "off"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0c0089

    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(I)V

    return-void

    :cond_0
    const-string v1, "low"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f0c008a

    goto :goto_0

    :cond_1
    const-string v1, "medium"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, 0x7f0c008b

    goto :goto_0

    :cond_2
    const-string v1, "virtual"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v0, 0x7f0c008c

    goto :goto_0

    :cond_3
    const v0, 0x7f0c008b

    goto :goto_0
.end method

.method private adjustTextNotificationSummary(Landroid/preference/ListPreference;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/preference/ListPreference;
    .param p2    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "popup"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f0c0080

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(I)V

    return-void

    :cond_1
    const-string v1, "statusbar"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, 0x7f0c0081

    goto :goto_0

    :cond_2
    const-string v1, "off"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0c007f

    goto :goto_0
.end method

.method private adjustVideoNotificationSummary(Landroid/preference/ListPreference;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/preference/ListPreference;
    .param p2    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "popup"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f0c0085

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(I)V

    return-void

    :cond_1
    const-string v1, "statusbar"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0c0086

    goto :goto_0
.end method

.method private registerListeners()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string v3, "gtalk-enable-notifications"

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "gtalk-video-enable-notifications"

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    const-string v3, "gtalk-video-image-image-stabilization"

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_1
    const-string v3, "gtalk-text-ringtone"

    invoke-virtual {p0, v3}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/google/android/talk/ImRingtonePreference;

    invoke-virtual {v2, p0}, Lcom/google/android/talk/ImRingtonePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "gtalk-video-ringtone"

    invoke-virtual {p0, v3}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/google/android/talk/ImRingtonePreference;

    if-eqz v2, :cond_2

    invoke-virtual {v2, p0}, Lcom/google/android/talk/ImRingtonePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_2
    return-void
.end method

.method private setInitialValues()V
    .locals 24

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/talk/fragments/SettingsFragment;->mAccountId:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-nez v20, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/talk/SettingsCache;->getInstance()Lcom/google/android/talk/SettingsCache;

    move-result-object v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/talk/fragments/SettingsFragment;->mAccountId:J

    move-wide/from16 v21, v0

    invoke-virtual/range {v20 .. v22}, Lcom/google/android/talk/SettingsCache;->getSettingsMap(J)Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    move-result-object v13

    const-string v20, "gtalk-mobile-indicator"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/CheckBoxPreference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/talk/fragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getShowMobileIndicator(Landroid/content/Context;)Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string v20, "gtalk-enable-notifications"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/google/android/talk/DependentListPreference;

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getTextNotification()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/google/android/talk/DependentListPreference;->setValue(Ljava/lang/String;)V

    const-string v20, "off"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/google/android/talk/DependentListPreference;->setDisabledEntry(Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getTextNotification()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v4, v13, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->adjustTextNotificationSummary(Landroid/preference/ListPreference;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;)V

    const-string v20, "gtalk-video-enable-notifications"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Landroid/preference/ListPreference;

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getVideoNotification()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getVideoNotification()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v8, v13, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->adjustVideoNotificationSummary(Landroid/preference/ListPreference;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/fragments/SettingsFragment;->mVibrator:Landroid/os/Vibrator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v6

    const-string v20, "gtalk-vibrate-when"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/CheckBoxPreference;

    if-eqz v6, :cond_2

    const-string v20, "always"

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getTextVibrateWhen()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :goto_1
    invoke-static {}, Lcom/google/android/talk/FeatureManager;->videoChatEnabled()Z

    move-result v17

    invoke-static {}, Lcom/google/android/talk/FeatureManager;->audioChatEnabled()Z

    move-result v2

    if-nez v17, :cond_3

    if-nez v2, :cond_3

    const-string v20, "video_notification_group"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    check-cast v18, Landroid/preference/PreferenceGroup;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/talk/fragments/SettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_2
    const-string v20, "gtalk-show-away-on-idle"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getShowAwayOnIdle()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string v20, "gtalk-show-friend-notifications"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getNotifyFriendInvitation()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string v20, "gtalk-text-ringtone"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v12

    check-cast v12, Lcom/google/android/talk/ImRingtonePreference;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/talk/fragments/SettingsFragment;->mAccountId:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v12, v0, v1}, Lcom/google/android/talk/ImRingtonePreference;->setAccountId(J)V

    const-string v20, "ringtone"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lcom/google/android/talk/ImRingtonePreference;->setIMRingtoneType(Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getTextRingtoneURI()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v12}, Lcom/google/android/talk/fragments/SettingsFragment;->setRingtoneSummary(Ljava/lang/String;Lcom/google/android/talk/ImRingtonePreference;)V

    const-string v20, "gtalk-video-ringtone"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v12

    check-cast v12, Lcom/google/android/talk/ImRingtonePreference;

    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/talk/fragments/SettingsFragment;->mAccountId:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v12, v0, v1}, Lcom/google/android/talk/ImRingtonePreference;->setAccountId(J)V

    const-string v20, "ringtone-video"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lcom/google/android/talk/ImRingtonePreference;->setIMRingtoneType(Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getVideoRingtoneURI()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v12}, Lcom/google/android/talk/fragments/SettingsFragment;->setRingtoneSummary(Ljava/lang/String;Lcom/google/android/talk/ImRingtonePreference;)V

    goto/16 :goto_0

    :cond_2
    const-string v20, "text_notification_group"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    check-cast v14, Landroid/preference/PreferenceCategory;

    invoke-virtual {v14, v11}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    :cond_3
    const-string v20, "gtalk-video-vibrate-when"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/CheckBoxPreference;

    if-eqz v6, :cond_5

    const-string v20, "always"

    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getVideoVibrateWhen()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :goto_3
    const-string v20, "gtalk-video-image-image-stabilization"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Landroid/preference/ListPreference;

    const/16 v20, 0x1

    invoke-static/range {v20 .. v20}, Lcom/google/android/talk/videochat/EffectsController;->isEffectAvailable(I)Z

    move-result v7

    const/16 v20, 0x2

    invoke-static/range {v20 .. v20}, Lcom/google/android/talk/videochat/EffectsController;->isEffectAvailable(I)Z

    move-result v19

    if-eqz v17, :cond_4

    if-nez v7, :cond_6

    if-nez v19, :cond_6

    :cond_4
    const-string v20, "video_notification_group"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v16

    check-cast v16, Landroid/preference/PreferenceCategory;

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2

    :cond_5
    const-string v20, "video_notification_group"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v16

    check-cast v16, Landroid/preference/PreferenceCategory;

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3

    :cond_6
    if-nez v19, :cond_9

    invoke-virtual {v8}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v5

    const/16 v20, 0x0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v20

    check-cast v20, [Ljava/lang/CharSequence;

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {v8}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v15

    const/16 v20, 0x0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v15, v0, v1}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v20

    check-cast v20, [Ljava/lang/CharSequence;

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    :cond_7
    :goto_4
    invoke-virtual {v13}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getVideoImageStabilization()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_8

    invoke-static {}, Lcom/google/android/talk/FeatureManager;->getDefaultVideoChatEffect()Ljava/lang/String;

    move-result-object v3

    :cond_8
    invoke-virtual {v8, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v13, v3}, Lcom/google/android/talk/fragments/SettingsFragment;->adjustImageStabilizationSummary(Landroid/preference/ListPreference;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    if-nez v7, :cond_7

    invoke-virtual {v8}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v5

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v9, v0, [Ljava/lang/CharSequence;

    const/16 v20, 0x0

    const/16 v21, 0x0

    aget-object v21, v5, v21

    aput-object v21, v9, v20

    const/16 v20, 0x1

    const/16 v21, 0x3

    aget-object v21, v5, v21

    aput-object v21, v9, v20

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {v8}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v15

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v10, v0, [Ljava/lang/CharSequence;

    const/16 v20, 0x0

    const/16 v21, 0x0

    aget-object v21, v15, v21

    aput-object v21, v10, v20

    const/16 v20, 0x1

    const/16 v21, 0x3

    aget-object v21, v15, v21

    aput-object v21, v10, v20

    invoke-virtual {v8, v10}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method private setRingtoneSummary(Ljava/lang/String;Lcom/google/android/talk/ImRingtonePreference;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/talk/ImRingtonePreference;

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v3

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v1, :cond_1

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v2

    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {v2, v0}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {p2, v3}, Lcom/google/android/talk/ImRingtonePreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v2, v3

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0083

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "vibrator"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    iput-object v2, p0, Lcom/google/android/talk/fragments/SettingsFragment;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "accountId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/talk/fragments/SettingsFragment;->mAccountId:J

    const/high16 v2, 0x7f060000

    invoke-virtual {p0, v2}, Lcom/google/android/talk/fragments/SettingsFragment;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SettingsFragment;->setInitialValues()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    instance-of v3, p1, Landroid/preference/ListPreference;

    if-nez v3, :cond_0

    instance-of v3, p1, Lcom/google/android/talk/ImRingtonePreference;

    if-eqz v3, :cond_2

    :cond_0
    invoke-static {}, Lcom/google/android/talk/SettingsCache;->getInstance()Lcom/google/android/talk/SettingsCache;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/talk/fragments/SettingsFragment;->mAccountId:J

    invoke-virtual {v3, v4, v5}, Lcom/google/android/talk/SettingsCache;->getSettingsMap(J)Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    move-result-object v2

    const-string v3, "gtalk-enable-notifications"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v3, p2

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setTextNotification(Ljava/lang/String;)V

    check-cast p1, Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p1, v2, p2}, Lcom/google/android/talk/fragments/SettingsFragment;->adjustTextNotificationSummary(Landroid/preference/ListPreference;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;)V

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    return v1

    :cond_3
    const-string v3, "gtalk-video-enable-notifications"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v3, p2

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setVideoNotification(Ljava/lang/String;)V

    check-cast p1, Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p1, v2, p2}, Lcom/google/android/talk/fragments/SettingsFragment;->adjustVideoNotificationSummary(Landroid/preference/ListPreference;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v3, "gtalk-video-image-image-stabilization"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setVideoImageStabilization(Ljava/lang/String;)V

    check-cast p1, Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p1, v2, p2}, Lcom/google/android/talk/fragments/SettingsFragment;->adjustImageStabilizationSummary(Landroid/preference/ListPreference;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v3, "gtalk-text-ringtone"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setTextRingtoneURI(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    check-cast p1, Lcom/google/android/talk/ImRingtonePreference;

    invoke-direct {p0, v3, p1}, Lcom/google/android/talk/fragments/SettingsFragment;->setRingtoneSummary(Ljava/lang/String;Lcom/google/android/talk/ImRingtonePreference;)V

    goto :goto_0

    :cond_6
    const-string v3, "gtalk-video-ringtone"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setVideoRingtoneURI(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    check-cast p1, Lcom/google/android/talk/ImRingtonePreference;

    invoke-direct {p0, v3, p1}, Lcom/google/android/talk/fragments/SettingsFragment;->setRingtoneSummary(Ljava/lang/String;Lcom/google/android/talk/ImRingtonePreference;)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 11
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    const/4 v7, 0x1

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/google/android/talk/SettingsCache;->getInstance()Lcom/google/android/talk/SettingsCache;

    move-result-object v6

    iget-wide v8, p0, Lcom/google/android/talk/fragments/SettingsFragment;->mAccountId:J

    invoke-virtual {v6, v8, v9}, Lcom/google/android/talk/SettingsCache;->getSettingsMap(J)Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    move-result-object v4

    instance-of v6, p2, Landroid/preference/CheckBoxPreference;

    if-eqz v6, :cond_7

    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v5

    const-string v6, "gtalk-mobile-indicator"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v4, v5}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setShowMobileIndicator(Z)V

    :cond_0
    :goto_0
    move v6, v7

    :goto_1
    return v6

    :cond_1
    const-string v6, "gtalk-show-away-on-idle"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v4, v5}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setShowAwayOnIdle(Z)V

    goto :goto_0

    :cond_2
    const-string v6, "gtalk-show-friend-notifications"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v4, v5}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setNotifyFriendInvitation(Z)V

    goto :goto_0

    :cond_3
    const-string v6, "gtalk-vibrate-when"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    if-eqz v5, :cond_4

    const-string v6, "always"

    :goto_2
    invoke-virtual {v4, v6}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setTextVibrateWhen(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v6, "never"

    goto :goto_2

    :cond_5
    const-string v6, "gtalk-video-vibrate-when"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v5, :cond_6

    const-string v6, "always"

    :goto_3
    invoke-virtual {v4, v6}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setVideoVibrateWhen(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v6, "never"

    goto :goto_3

    :cond_7
    const-string v6, "pref_gtalk_clear_history_key"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    new-instance v0, Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;

    invoke-direct {v0}, Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v8, "dialog"

    invoke-virtual {v0, v6, v8}, Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    move v6, v7

    goto :goto_1

    :cond_8
    const-string v6, "pref_gtalk_manage_account_key"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.settings.SYNC_SETTINGS"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "authorities"

    new-array v8, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "gmail-ls"

    aput-object v10, v8, v9

    invoke-virtual {v2, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v6, 0x10000000

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/talk/fragments/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    move v6, v7

    goto :goto_1

    :cond_9
    const-string v6, "pref_gtalk_blocked_buddies_key"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {p2}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v6, "accountId"

    iget-wide v7, p0, Lcom/google/android/talk/fragments/SettingsFragment;->mAccountId:J

    invoke-virtual {v1, v6, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_a
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v6

    goto/16 :goto_1

    :cond_b
    const-string v6, "gtalk-terms-key"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/talk/HelpUtils;->showTermsOfService(Landroid/content/Context;)V

    move v6, v7

    goto/16 :goto_1
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SettingsFragment;->registerListeners()V

    return-void
.end method
