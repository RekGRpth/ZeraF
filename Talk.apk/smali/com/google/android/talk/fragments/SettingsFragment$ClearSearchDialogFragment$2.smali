.class Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment$2;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment$2;->this$0:Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment$2;->this$0:Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;

    invoke-virtual {v2}, Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/talk/TalkApp;->getApplication(Landroid/app/Activity;)Lcom/google/android/talk/TalkApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/talk/TalkApp;->getRecentSuggestions()Landroid/provider/SearchRecentSuggestions;

    move-result-object v2

    invoke-virtual {v2}, Landroid/provider/SearchRecentSuggestions;->clearHistory()V

    iget-object v2, p0, Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment$2;->this$0:Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;

    invoke-virtual {v2}, Lcom/google/android/talk/fragments/SettingsFragment$ClearSearchDialogFragment;->dismiss()V

    :cond_0
    return-void
.end method
