.class public Lcom/google/android/talk/fragments/ChatScreenFragment;
.super Landroid/app/Fragment;
.source "ChatScreenFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/fragments/ChatScreenFragment$18;,
        Lcom/google/android/talk/fragments/ChatScreenFragment$ChatHost;,
        Lcom/google/android/talk/fragments/ChatScreenFragment$GroupChatConverted;,
        Lcom/google/android/talk/fragments/ChatScreenFragment$GalleryAdapter;,
        Lcom/google/android/talk/fragments/ChatScreenFragment$SimpleActiveChats;,
        Lcom/google/android/talk/fragments/ChatScreenFragment$ViewPagerActiveChats;,
        Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;,
        Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;,
        Lcom/google/android/talk/fragments/ChatScreenFragment$ChatScreenHost;,
        Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;
    }
.end annotation


# static fields
.field private static CHATS_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAccountId:J

.field private mActivity:Landroid/app/Activity;

.field private mApp:Lcom/google/android/talk/TalkApp;

.field private mAudioDevices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videochat/CallSession$AudioDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mCallStateClient:Lcom/google/android/videochat/CallStateClient;

.field private mChatHost:Lcom/google/android/talk/fragments/ChatScreenFragment$ChatHost;

.field private mChatInputField:Landroid/widget/EditText;

.field private mChatList:Lcom/google/android/talk/util/ChatList;

.field private mColorMaker:Lcom/google/android/talk/util/ChatColorMaker;

.field private mContactIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mController:Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;

.field private mCreated:Z

.field private mFromStatusBarNotify:Z

.field private mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

.field private mGroupChatEnabled:Z

.field private final mHandler:Landroid/os/Handler;

.field private mInputContainer:Landroid/view/View;

.field private volatile mInvitePending:Z

.field private mInviteRunnable:Ljava/lang/Runnable;

.field private mMessageBar:Landroid/view/ViewGroup;

.field private mNeedToHandleNewIntent:Z

.field private mQueryCompleteRunnable:Ljava/lang/Runnable;

.field private mResources:Landroid/content/res/Resources;

.field private mRoot:Landroid/view/View;

.field private mSelfClientType:I

.field private mSendButton:Landroid/view/View;

.field private mService:Lcom/google/android/gtalkservice/IGTalkService;

.field private mStartVoiceChatRequest:Z

.field private mStarted:Z

.field private mTabletMode:Z

.field private mTargetContact:Ljava/lang/String;

.field private final mWaitForServiceTasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "is_active"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/talk/fragments/ChatScreenFragment;->CHATS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mContactIdMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mWaitForServiceTasks:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mSelfClientType:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAudioDevices:Ljava/util/Set;

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$1;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatHost:Lcom/google/android/talk/fragments/ChatScreenFragment$ChatHost;

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$2;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mQueryCompleteRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mContactIdMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mWaitForServiceTasks:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mSelfClientType:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAudioDevices:Ljava/util/Set;

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$1;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatHost:Lcom/google/android/talk/fragments/ChatScreenFragment$ChatHost;

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$2;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mQueryCompleteRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/talk/fragments/ChatScreenFragment;)Lcom/google/android/videochat/CallStateClient;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/talk/fragments/ChatScreenFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/talk/fragments/ChatScreenFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/talk/fragments/ChatScreenFragment;)Landroid/view/ViewGroup;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mMessageBar:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/talk/fragments/ChatScreenFragment;)Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mController:Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/talk/fragments/ChatScreenFragment;)Lcom/google/android/talk/ChatView;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/talk/fragments/ChatScreenFragment;Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->foreachChat(Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/talk/fragments/ChatScreenFragment;Ljava/lang/String;J)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/talk/fragments/ChatScreenFragment;->pickChat(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/talk/fragments/ChatScreenFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->hideKeyboard()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/talk/fragments/ChatScreenFragment;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/talk/fragments/ChatScreenFragment;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mContactIdMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/talk/fragments/ChatScreenFragment;)J
    .locals 2
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-wide v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    return-wide v0
.end method

.method static synthetic access$2200(Lcom/google/android/talk/fragments/ChatScreenFragment;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mWaitForServiceTasks:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/talk/fragments/ChatScreenFragment;)Lcom/google/android/gtalkservice/IGTalkService;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/talk/fragments/ChatScreenFragment;)Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/talk/fragments/ChatScreenFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->sendMessage()V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/talk/fragments/ChatScreenFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mSendButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/android/talk/fragments/ChatScreenFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->userActionDetected()V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/talk/fragments/ChatScreenFragment;Ljava/lang/String;J)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/talk/fragments/ChatScreenFragment;->ensureChatInDb(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/talk/fragments/ChatScreenFragment;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/talk/fragments/ChatScreenFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/google/android/talk/fragments/ChatScreenFragment;Ljava/util/Set;)Ljava/util/Set;
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAudioDevices:Ljava/util/Set;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/talk/fragments/ChatScreenFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/google/android/talk/fragments/ChatScreenFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInvitePending:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/google/android/talk/fragments/ChatScreenFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInvitePending:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/talk/fragments/ChatScreenFragment;Ljava/lang/String;JZ)Lcom/google/android/gtalkservice/IChatSession;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Z

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/talk/fragments/ChatScreenFragment;->checkChatSession(Ljava/lang/String;JZ)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/talk/fragments/ChatScreenFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStartVoiceChatRequest:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/talk/fragments/ChatScreenFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStartVoiceChatRequest:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/talk/fragments/ChatScreenFragment;)Lcom/google/android/talk/util/ChatList;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/talk/fragments/ChatScreenFragment;Lcom/google/android/talk/util/ChatList;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;
    .param p1    # Lcom/google/android/talk/util/ChatList;

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->hintValue(Lcom/google/android/talk/util/ChatList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/talk/fragments/ChatScreenFragment;)Lcom/google/android/talk/fragments/ChatScreenFragment$ChatHost;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/ChatScreenFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatHost:Lcom/google/android/talk/fragments/ChatScreenFragment$ChatHost;

    return-object v0
.end method

.method private checkChatSession(Ljava/lang/String;JZ)Lcom/google/android/gtalkservice/IChatSession;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Z

    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkChatSession "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ensureChatsInDb "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->logv(Ljava/lang/String;)V

    :cond_0
    iget-object v9, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mWaitForServiceTasks:Ljava/util/List;

    monitor-enter v9

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    if-nez v0, :cond_2

    iget-object v10, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mWaitForServiceTasks:Ljava/util/List;

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$7;

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/talk/fragments/ChatScreenFragment$7;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;Ljava/lang/String;JZ)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v6, 0x0

    monitor-exit v9

    :cond_1
    :goto_0
    return-object v6

    :cond_2
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    invoke-interface {v0, p2, p3}, Lcom/google/android/gtalkservice/IGTalkService;->getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;

    move-result-object v8

    if-nez v8, :cond_3

    const-string v0, "talk"

    const-string v1, "checkChatSession: null imSession, bail!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/talk/AccountSelectionActivity;->startAccountSelectionActivity(Landroid/app/Activity;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v6, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    :try_start_3
    invoke-interface {v8, p1}, Lcom/google/android/gtalkservice/IImSession;->getChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v6

    if-nez v6, :cond_4

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v0}, Lcom/google/android/talk/util/ChatList;->suspendRequery()V

    invoke-interface {v8, p1}, Lcom/google/android/gtalkservice/IImSession;->createChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkChatSession created for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " chatSession="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v0}, Lcom/google/android/talk/util/ChatList;->resumeRequery()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v7

    const-string v0, "talk"

    const-string v1, "checkChatSession caught "

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/talk/AccountSelectionActivity;->startAccountSelectionActivity(Landroid/app/Activity;)V

    const/4 v6, 0x0

    goto :goto_0

    :cond_4
    if-eqz p4, :cond_1

    :try_start_4
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/talk/fragments/ChatScreenFragment$8;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/talk/fragments/ChatScreenFragment$8;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;Ljava/lang/String;J)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0
.end method

.method private configureMuteUnmuteButton(Landroid/view/MenuItem;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eqz p2, :cond_0

    const v0, 0x7f0c0104

    :goto_0
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    if-eqz p2, :cond_1

    const v0, 0x7f020063

    :goto_1
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    return-void

    :cond_0
    const v0, 0x7f0c0103

    goto :goto_0

    :cond_1
    const v0, 0x7f020065

    goto :goto_1
.end method

.method private createChatList(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    new-instance v0, Lcom/google/android/talk/util/ChatList;

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    iget-wide v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mQueryCompleteRunnable:Ljava/lang/Runnable;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/talk/util/ChatList;-><init>(Landroid/app/Activity;JLjava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    return-void
.end method

.method private dismissAllChatNotifications(J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v0}, Lcom/google/android/talk/TalkApp;->getGTalkService()Lcom/google/android/gtalkservice/IGTalkService;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "talk"

    const-string v1, "dismissChatNotification: no GTalkService object found!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-string v1, "dismissChatNotification for all"

    invoke-direct {p0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->log(Ljava/lang/String;)V

    invoke-interface {v0, p1, p2}, Lcom/google/android/gtalkservice/IGTalkService;->dismissNotificationsForAccount(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/talk/ActivityUtils;->dismissPopupNotification(Landroid/app/Activity;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "talk"

    const-string v2, "dismissChatNotification: caught "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private enableOrDisable(Landroid/view/MenuItem;Lcom/google/android/videochat/CallSession$AudioDevice;)V
    .locals 2
    .param p1    # Landroid/view/MenuItem;
    .param p2    # Lcom/google/android/videochat/CallSession$AudioDevice;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAudioDevices:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method private ensureChatInDb(Ljava/lang/String;J)V
    .locals 18
    .param p1    # Ljava/lang/String;
    .param p2    # J

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contact_id"

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " in (select _id from contacts where "

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "username"

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=? AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "account"

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/gsf/TalkContract$Chats;->CONTENT_URI:Landroid/net/Uri;

    sget-object v5, Lcom/google/android/talk/fragments/ChatScreenFragment;->CHATS_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v7, v8

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    const-wide/16 v11, 0x0

    const/4 v15, 0x0

    const-wide/16 v13, 0x0

    if-eqz v10, :cond_1

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v11, v4

    const/4 v4, 0x2

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    const/4 v15, 0x1

    :goto_0
    const/4 v4, 0x0

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v13

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v4, v11, v4

    if-nez v4, :cond_4

    move-object/from16 v0, p1

    move-wide/from16 v1, p2

    invoke-static {v3, v0, v1, v2}, Lcom/google/android/talk/DatabaseUtils;->getIdForContact(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v11

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ensureChatInDb: create a chat for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", contactId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/talk/fragments/ChatScreenFragment;->log(Ljava/lang/String;)V

    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "contact_id"

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "last_message_date"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "last_unread_message"

    const/4 v4, 0x0

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gsf/TalkContract$Chats;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v15, 0x0

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_4
    if-nez v15, :cond_2

    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "is_active"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v4, Lcom/google/android/gsf/TalkContract$Chats;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "contact_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, v16

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method private foreachChat(Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;)V
    .locals 1
    .param p1    # Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    invoke-interface {v0, p1}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->foreachChatView(Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;)V

    :cond_0
    return-void
.end method

.method private getActiveChat()Lcom/google/android/talk/ChatView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->getSelectedChatView()Lcom/google/android/talk/ChatView;

    move-result-object v0

    goto :goto_0
.end method

.method private hideKeyboard()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method private hintValue(Lcom/google/android/talk/util/ChatList;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/google/android/talk/util/ChatList;

    invoke-virtual {p1}, Lcom/google/android/talk/util/ChatList;->isGroupChat()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0c0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/talk/util/ChatList;->getNickName()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_1

    invoke-static {v0}, Lcom/google/android/talk/StringUtils;->parseAbbreviatedAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static isChatScreenIntent(Landroid/content/Intent;)Z
    .locals 3
    .param p0    # Landroid/content/Intent;

    const/4 v1, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v2, "from"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 3

    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatScreenFragment] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private logv(Ljava/lang/String;)V
    .locals 3

    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ChatScreenFragment] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onGroupChatApproval(Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/talk/AccountSelectionActivity;->startAccountSelectionActivity(Landroid/app/Activity;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "room"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v1, v1, Lcom/google/android/talk/TalkApp;->mGroupChatInvitations:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "approval"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "from"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->leaveChat()V

    goto :goto_0
.end method

.method private pickChat(Ljava/lang/String;J)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const/4 v6, -0x1

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "from"

    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "accountId"

    invoke-virtual {v1, v4, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v4}, Lcom/google/android/talk/util/ChatList;->isClosed()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v4}, Lcom/google/android/talk/util/ChatList;->getCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_4

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v4, v2}, Lcom/google/android/talk/util/ChatList;->moveToPosition(I)V

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v4}, Lcom/google/android/talk/util/ChatList;->getAccountId()J

    move-result-wide v4

    cmp-long v4, v4, p2

    if-eqz v4, :cond_3

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v4}, Lcom/google/android/talk/util/ChatList;->getUsername()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v3, v2

    :cond_4
    iput-object p1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    if-eqz p1, :cond_5

    if-ne v3, v6, :cond_6

    const/4 v4, 0x1

    :goto_2
    invoke-direct {p0, p1, p2, p3, v4}, Lcom/google/android/talk/fragments/ChatScreenFragment;->checkChatSession(Ljava/lang/String;JZ)Lcom/google/android/gtalkservice/IChatSession;

    if-eq v3, v6, :cond_5

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    new-instance v5, Lcom/google/android/talk/fragments/ChatScreenFragment$GalleryAdapter;

    iget-object v6, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-direct {v5, p0, v6}, Lcom/google/android/talk/fragments/ChatScreenFragment$GalleryAdapter;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;Lcom/google/android/talk/util/ChatList;)V

    invoke-interface {v4, v5}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->setAdapter(Lcom/google/android/talk/fragments/ChatScreenFragment$GalleryAdapter;)V

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    invoke-interface {v4, v3}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->setSelection(I)V

    :cond_5
    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInviteRunnable:Ljava/lang/Runnable;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInviteRunnable:Ljava/lang/Runnable;

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInviteRunnable:Ljava/lang/Runnable;

    goto :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private resolveIntent()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_notify"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mFromStatusBarNotify:Z

    const-string v1, "vc"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStartVoiceChatRequest:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resolveIntent: fromNotify="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mFromStatusBarNotify:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startVoice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStartVoiceChatRequest:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->log(Ljava/lang/String;)V

    return-void
.end method

.method private restartKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    goto :goto_0
.end method

.method private sendMessage()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/talk/ChatView;->sendMessage(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/method/TextKeyListener;->clear(Landroid/text/Editable;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->restartKeyboard()V

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    iget-boolean v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTabletMode:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v2, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->hideKeyboard()V

    goto :goto_0
.end method

.method private setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)V
    .locals 7
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v0, -0x1

    if-eq p4, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mApp:Lcom/google/android/talk/TalkApp;

    const/4 v1, 0x0

    invoke-virtual {v0, p4, v1}, Lcom/google/android/talk/TalkApp;->getConnectionTypeIndicator(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    :goto_0
    iput p4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mSelfClientType:I

    packed-switch p4, :pswitch_data_0

    :goto_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mController:Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;->setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;IILandroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c00fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private setupMessageBar()V
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    const/high16 v6, 0x3fc00000

    const/4 v5, 0x3

    const/4 v4, 0x2

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mRoot:Landroid/view/View;

    const v3, 0x7f100034

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v7}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v4, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v5, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    invoke-virtual {v0, v4, v8, v9}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    invoke-virtual {v0, v5, v8, v9}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2, v6}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v4, v2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2, v6}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v5, v2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    iput-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mMessageBar:Landroid/view/ViewGroup;

    goto :goto_0
.end method

.method private start()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStarted:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStarted:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v0}, Lcom/google/android/talk/TalkApp;->ensureServiceBound()V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->registerForServiceStateChanged()V

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$10;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$10;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->foreachChat(Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v0}, Lcom/google/android/talk/util/ChatList;->requery()V

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$11;

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment$11;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    invoke-virtual {v0}, Lcom/google/android/videochat/CallStateClient;->startListening()V

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mNeedToHandleNewIntent:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mNeedToHandleNewIntent:Z

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    iget-wide v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/talk/fragments/ChatScreenFragment;->pickChat(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private userActionDetected()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/talk/ChatView;->handleUnreadMessages(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public addToChat()V
    .locals 5

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGroupChatEnabled:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "show groupchat invite"

    invoke-direct {p0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->log(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    const-class v3, Lcom/google/android/talk/GroupChatInviteeList;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "accountId"

    invoke-virtual {v0}, Lcom/google/android/talk/ChatView;->getAccountId()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "from"

    invoke-virtual {v0}, Lcom/google/android/talk/ChatView;->getMucParticipants()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method clearChat()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/talk/ChatView;->clearChat()V

    goto :goto_0
.end method

.method public currentChatAccount()J
    .locals 3

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/talk/ChatView;->getAccountId()J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method public currentChatUser()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/talk/ChatView;->getContact()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public leaveChat()V
    .locals 5

    const/4 v0, 0x1

    const-string v1, "leaveChat"

    invoke-direct {p0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v2}, Lcom/google/android/talk/util/ChatList;->getCount()I

    move-result v2

    if-le v2, v0, :cond_4

    new-instance v3, Lcom/google/android/talk/fragments/ChatScreenFragment$17;

    invoke-direct {v3, p0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment$17;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;Lcom/google/android/talk/ChatView;)V

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    invoke-interface {v4}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->getSelectedItemPosition()I

    move-result v4

    add-int/lit8 v2, v2, -0x1

    if-ge v4, v2, :cond_1

    :goto_1
    iget-boolean v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTabletMode:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    if-eqz v0, :cond_2

    add-int/lit8 v0, v4, 0x1

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/talk/util/ChatList;->moveToPosition(I)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v0}, Lcom/google/android/talk/util/ChatList;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {v1}, Lcom/google/android/talk/ChatView;->leaveChat()V

    invoke-virtual {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->onNewIntent(Landroid/content/Intent;)V

    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v4, -0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    invoke-interface {v1, v0, v3}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->selectNext(ZLjava/lang/Runnable;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mController:Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;->onLastChatClosed()V

    invoke-virtual {v1}, Lcom/google/android/talk/ChatView;->leaveChat()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v1, "accountId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v0}, Lcom/google/android/talk/util/ChatList;->requery()V

    goto :goto_0
.end method

.method public leaveChat(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/talk/ChatView;->getContact()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->leaveChat()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v2}, Lcom/google/android/talk/util/ChatList;->requery()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    const/4 v6, 0x0

    const/4 v7, 0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "invitee"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/util/Formatter;

    invoke-direct {v1, v0}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0c003b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountId"

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$15;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/talk/fragments/ChatScreenFragment$15;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;Ljava/lang/String;JLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInviteRunnable:Ljava/lang/Runnable;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInviteRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInviteRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iput-object v6, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInviteRunnable:Ljava/lang/Runnable;

    goto :goto_0

    :pswitch_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->onGroupChatApproval(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    move-object v0, v6

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    move-object v0, p1

    check-cast v0, Lcom/google/android/talk/fragments/ChatScreenFragment$ChatScreenHost;

    iput-object p1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/ChatScreenFragment$ChatScreenHost;->getChatScreenController()Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mController:Lcom/google/android/talk/fragments/ChatScreenFragment$Controller;

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mResources:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gtalk_allow_group_chat"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGroupChatEnabled:Z

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInputContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInputContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x1

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->resolveIntent()V

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/talk/ActivityUtils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTabletMode:Z

    invoke-virtual {p0, v6}, Lcom/google/android/talk/fragments/ChatScreenFragment;->setHasOptionsMenu(Z)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "accountId"

    const-wide/16 v3, -0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    iget-wide v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    check-cast v0, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    iput-wide v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    :cond_0
    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/talk/TalkApp;->getApplication(Landroid/app/Activity;)Lcom/google/android/talk/TalkApp;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "from"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/talk/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    iput-boolean v6, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mCreated:Z

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    const v0, 0x7f0f0004

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTabletMode:Z

    if-eqz v1, :cond_1

    const v1, 0x7f040013

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mRoot:Landroid/view/View;

    :goto_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f10004c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->setupInputFieldAndSendButton(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->setupMessageBar()V

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f100033

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-boolean v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTabletMode:Z

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/talk/fragments/ChatScreenFragment$SimpleActiveChats;

    invoke-direct {v1, p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment$SimpleActiveChats;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    :goto_1
    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mRoot:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->createChatList(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->isHidden()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->start()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mRoot:Landroid/view/View;

    return-object v1

    :cond_1
    const v1, 0x7f040014

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mRoot:Landroid/view/View;

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/talk/fragments/ChatScreenFragment$ViewPagerActiveChats;

    invoke-direct {v1, p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment$ViewPagerActiveChats;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$14;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$14;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->foreachChat(Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;)V

    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->stop()V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->hideKeyboard()V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    invoke-interface {v1, p1}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->onHiddenChanged(Z)V

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->start()V

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 6

    const-wide/16 v4, 0x0

    invoke-static {p1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->isChatScreenIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->resolveIntent()V

    const-string v0, "from"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/talk/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    const-string v0, "accountId"

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_2

    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "got intent with account "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; contact is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    iput-wide v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStarted:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mNeedToHandleNewIntent:Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mNeedToHandleNewIntent:Z

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    iget-wide v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/talk/fragments/ChatScreenFragment;->pickChat(Ljava/lang/String;J)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/talk/ChatView;->dismissChatNotification()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    invoke-virtual {v0}, Lcom/google/android/videochat/CallStateClient;->requestUpdate()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatHost:Lcom/google/android/talk/fragments/ChatScreenFragment$ChatHost;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/talk/fragments/ChatScreenFragment$ChatHost;->handleMenuItem(Lcom/google/android/talk/ChatView;Landroid/view/MenuItem;I)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mContactIdMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$13;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$13;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->foreachChat(Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 13

    const v0, 0x7f1000bd

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f1000be

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f100059

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f10005a

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const v4, 0x7f10005d

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const v5, 0x7f10005e

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const v6, 0x7f1000bf

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    const v7, 0x7f1000c3

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v8, 0x0

    invoke-interface {v1, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v8, 0x0

    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGroupChatEnabled:Z

    invoke-interface {v7, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/talk/ChatView;->getVideoChatState()Lcom/google/android/talk/ChatView$VideoChatState;

    move-result-object v8

    invoke-virtual {v2}, Lcom/google/android/talk/ChatView;->getCapabilities()I

    move-result v9

    invoke-static {v9}, Lcom/google/android/talk/ActivityUtils;->isVideoChatCapable(I)Z

    move-result v10

    invoke-static {v9}, Lcom/google/android/talk/ActivityUtils;->isAudioChatCapable(I)Z

    move-result v9

    sget-object v11, Lcom/google/android/talk/fragments/ChatScreenFragment$18;->$SwitchMap$com$google$android$talk$ChatView$VideoChatState:[I

    invoke-virtual {v8}, Lcom/google/android/talk/ChatView$VideoChatState;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    :cond_2
    :goto_1
    const v0, 0x7f1000c1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/google/android/talk/ChatView;->isOffTheRecord()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f0c004d

    :goto_2
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :cond_3
    const v0, 0x7f1000b4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    sget-object v1, Lcom/google/android/videochat/CallSession$AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/videochat/CallSession$AudioDevice;

    invoke-direct {p0, v0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->enableOrDisable(Landroid/view/MenuItem;Lcom/google/android/videochat/CallSession$AudioDevice;)V

    const v0, 0x7f1000b1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    sget-object v1, Lcom/google/android/videochat/CallSession$AudioDevice;->SPEAKERPHONE:Lcom/google/android/videochat/CallSession$AudioDevice;

    invoke-direct {p0, v0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->enableOrDisable(Landroid/view/MenuItem;Lcom/google/android/videochat/CallSession$AudioDevice;)V

    const v0, 0x7f1000b3

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    sget-object v1, Lcom/google/android/videochat/CallSession$AudioDevice;->EARPIECE:Lcom/google/android/videochat/CallSession$AudioDevice;

    invoke-direct {p0, v0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->enableOrDisable(Landroid/view/MenuItem;Lcom/google/android/videochat/CallSession$AudioDevice;)V

    const v0, 0x7f1000b2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    sget-object v1, Lcom/google/android/videochat/CallSession$AudioDevice;->WIRED_HEADSET:Lcom/google/android/videochat/CallSession$AudioDevice;

    invoke-direct {p0, v0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->enableOrDisable(Landroid/view/MenuItem;Lcom/google/android/videochat/CallSession$AudioDevice;)V

    invoke-virtual {v2}, Lcom/google/android/talk/ChatView;->getSelectedAudioDevice()Lcom/google/android/videochat/CallState$AudioDeviceState;

    move-result-object v1

    const/4 v0, 0x0

    sget-object v2, Lcom/google/android/talk/fragments/ChatScreenFragment$18;->$SwitchMap$com$google$android$videochat$CallState$AudioDeviceState:[I

    invoke-virtual {v1}, Lcom/google/android/videochat/CallState$AudioDeviceState;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    :goto_3
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_0
    if-eqz v10, :cond_4

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_4
    if-eqz v9, :cond_2

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x1

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget-object v0, Lcom/google/android/talk/ChatView$VideoChatState;->VOICE_MUTED:Lcom/google/android/talk/ChatView$VideoChatState;

    if-ne v8, v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    invoke-direct {p0, v4, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->configureMuteUnmuteButton(Landroid/view/MenuItem;Z)V

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    const v0, 0x7f0c004e

    goto :goto_2

    :pswitch_3
    const v0, 0x7f1000b4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_3

    :pswitch_4
    const v0, 0x7f1000b1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_3

    :pswitch_5
    const v0, 0x7f1000b3

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_3

    :pswitch_6
    const v0, 0x7f1000b2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$12;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$12;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->foreachChat(Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mColorMaker:Lcom/google/android/talk/util/ChatColorMaker;

    invoke-virtual {v1, p1}, Lcom/google/android/talk/util/ChatColorMaker;->freeze(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/talk/ChatView;->saveState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->start()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->stop()V

    return-void
.end method

.method public registerForServiceStateChanged()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/talk/fragments/ChatScreenFragment$9;

    invoke-direct {v2, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$9;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/talk/TalkApp;->addServiceAvailableCallback(Landroid/os/Handler;Lcom/google/android/talk/ServiceAvailableRunnable;)V

    return-void
.end method

.method public serviceStateChanged(Lcom/google/android/gtalkservice/IGTalkService;)V
    .locals 3

    if-eqz p1, :cond_4

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mWaitForServiceTasks:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mWaitForServiceTasks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mWaitForServiceTasks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-instance v0, Lcom/google/android/talk/fragments/ChatScreenFragment$16;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$16;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->foreachChat(Lcom/google/android/talk/fragments/ChatScreenFragment$ViewRunnable;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/talk/ChatView;->addInvitationListener()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/talk/ChatView;->handleUnreadMessages(Z)V

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mFromStatusBarNotify:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mFromStatusBarNotify:Z

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/talk/ChatView;->getAccountId()J

    move-result-wide v0

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment;->dismissAllChatNotifications(J)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-wide v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    goto :goto_1

    :cond_4
    const-string v0, "talk"

    const-string v1, "serviceStateChanged: service disconnected, finish!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/talk/AccountSelectionActivity;->startAccountSelectionActivity(Landroid/app/Activity;)V

    goto :goto_2
.end method

.method public setupInputFieldAndSendButton(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInputContainer:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInputContainer:Landroid/view/View;

    const v4, 0x7f10004e

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mSendButton:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mInputContainer:Landroid/view/View;

    const v4, 0x7f10004d

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/talk/util/ChatColorMaker;

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getLinkTextColors()Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/talk/util/ChatColorMaker;-><init>(II)V

    iput-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mColorMaker:Lcom/google/android/talk/util/ChatColorMaker;

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mSendButton:Landroid/view/View;

    new-instance v4, Lcom/google/android/talk/fragments/ChatScreenFragment$3;

    invoke-direct {v4, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$3;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    new-instance v4, Lcom/google/android/talk/fragments/ChatScreenFragment$4;

    invoke-direct {v4, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$4;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    new-instance v4, Lcom/google/android/talk/fragments/ChatScreenFragment$5;

    invoke-direct {v4, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$5;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    new-instance v4, Lcom/google/android/talk/fragments/ChatScreenFragment$6;

    invoke-direct {v4, p0}, Lcom/google/android/talk/fragments/ChatScreenFragment$6;-><init>(Lcom/google/android/talk/fragments/ChatScreenFragment;)V

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->isHidden()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    :cond_0
    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/16 v4, 0x12

    invoke-interface {v0, p0, v3, v2, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    iget-object v4, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mSendButton:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v4, v2}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStarted:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mStarted:Z

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->stop()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    invoke-virtual {v0}, Lcom/google/android/talk/util/ChatList;->closeCursor()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mGallery:Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;

    invoke-interface {v0, v1}, Lcom/google/android/talk/fragments/ChatScreenFragment$ActiveChats;->setAdapter(Lcom/google/android/talk/fragments/ChatScreenFragment$GalleryAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->unregisterForServiceStateChanged()V

    iput-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    invoke-virtual {v0}, Lcom/google/android/videochat/CallStateClient;->stopListening()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->hideKeyboard()V

    goto :goto_0
.end method

.method public switchAccounts()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mCreated:Z

    if-nez v0, :cond_0

    const-string v0, "switchAccounts: fragment\'s onCreate not called yet"

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->stop()V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/talk/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mTargetContact:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_1

    const-string v0, "switchAccounts: mActivity is NULL"

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    iput-wide v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mChatList:Lcom/google/android/talk/util/ChatList;

    iget-wide v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mAccountId:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/talk/util/ChatList;->changeAccount(J)Z

    :cond_2
    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->start()V

    goto :goto_0
.end method

.method public toggleOtr()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/talk/fragments/ChatScreenFragment;->getActiveChat()Lcom/google/android/talk/ChatView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/talk/ChatView;->isOffTheRecord()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/talk/ChatView;->goOffTheRecord(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public unregisterForServiceStateChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v1, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/google/android/talk/TalkApp;->removeServiceAvailableCallback(Landroid/os/Handler;)V

    return-void
.end method

.method public updateTitle(Landroid/view/View;)V
    .locals 12
    .param p1    # Landroid/view/View;

    const/4 v6, 0x0

    const/4 v4, -0x1

    instance-of v0, p1, Lcom/google/android/talk/ChatView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object v10, p1

    check-cast v10, Lcom/google/android/talk/ChatView;

    if-nez v10, :cond_1

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/talk/fragments/ChatScreenFragment;->setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)V

    goto :goto_0

    :cond_1
    invoke-virtual {v10}, Lcom/google/android/talk/ChatView;->isGroupChat()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, Lcom/google/android/talk/ChatView;->getNickname()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/talk/fragments/ChatScreenFragment;->setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)V

    goto :goto_0

    :cond_2
    invoke-virtual {v10}, Lcom/google/android/talk/ChatView;->getNickname()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10}, Lcom/google/android/talk/ChatView;->getStatus()I

    move-result v8

    invoke-virtual {v10}, Lcom/google/android/talk/ChatView;->getCapabilities()I

    move-result v9

    invoke-virtual {v10}, Lcom/google/android/talk/ChatView;->getClientType()I

    move-result v7

    invoke-virtual {v10}, Lcom/google/android/talk/ChatView;->getContact()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/talk/fragments/ChatScreenFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v0, v4}, Lcom/google/android/talk/TalkApp;->shouldHideRemoteJid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v4, ""

    :cond_3
    invoke-virtual {v10}, Lcom/google/android/talk/ChatView;->getNickname()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Lcom/google/android/talk/ChatView;->getCustomStatus()Ljava/lang/String;

    move-result-object v6

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/talk/fragments/ChatScreenFragment;->setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)V

    goto :goto_0
.end method
