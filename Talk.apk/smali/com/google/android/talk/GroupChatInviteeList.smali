.class public Lcom/google/android/talk/GroupChatInviteeList;
.super Lcom/google/android/talk/GtalkServiceActivity;
.source "GroupChatInviteeList.java"


# instance fields
.field private mAccountId:J

.field private mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

.field private mApp:Lcom/google/android/talk/TalkApp;

.field mController:Lcom/google/android/talk/fragments/GroupChatInviteeListFragment$Controller;

.field mList:Lcom/google/android/talk/fragments/GroupChatInviteeListFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/talk/GtalkServiceActivity;-><init>()V

    new-instance v0, Lcom/google/android/talk/GroupChatInviteeList$2;

    invoke-direct {v0, p0}, Lcom/google/android/talk/GroupChatInviteeList$2;-><init>(Lcom/google/android/talk/GroupChatInviteeList;)V

    iput-object v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mController:Lcom/google/android/talk/fragments/GroupChatInviteeListFragment$Controller;

    return-void
.end method

.method private getParticipants(Landroid/content/Intent;)[Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Intent;

    const-string v2, "from"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mParticipants.length = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/talk/GroupChatInviteeList;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mParticipants["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/talk/GroupChatInviteeList;->log(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private initAccount(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mAccountId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/GroupChatInviteeList;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountId"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mAccountId:J

    iget-wide v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mAccountId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "accountId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mAccountId:J

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mApp:Lcom/google/android/talk/TalkApp;

    iget-wide v1, p0, Lcom/google/android/talk/GroupChatInviteeList;->mAccountId:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/talk/TalkApp;->getAccountInfo(J)Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    iget-object v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/talk/GroupChatInviteeList;->finish()V

    :cond_1
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[GroupChatInviteeList] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private registerOnSessionCreatedListeners()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mList:Lcom/google/android/talk/fragments/GroupChatInviteeListFragment;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/GroupChatInviteeList;->addOnSessionCreatedListener(Lcom/google/android/talk/GtalkServiceActivity$OnImSessionCreatedListener;)V

    return-void
.end method


# virtual methods
.method protected getAccountId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/talk/GroupChatInviteeList;->mAccountId:J

    return-wide v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x4

    invoke-static {p0}, Lcom/google/android/talk/ActivityUtils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    const v3, 0x1030119

    invoke-virtual {p0, v3}, Lcom/google/android/talk/GroupChatInviteeList;->setTheme(I)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/talk/GtalkServiceActivity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f040020

    invoke-virtual {p0, v3}, Lcom/google/android/talk/GroupChatInviteeList;->setContentView(I)V

    invoke-static {p0}, Lcom/google/android/talk/TalkApp;->getApplication(Landroid/app/Activity;)Lcom/google/android/talk/TalkApp;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/talk/GroupChatInviteeList;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-direct {p0, p1}, Lcom/google/android/talk/GroupChatInviteeList;->initAccount(Landroid/os/Bundle;)V

    new-instance v3, Lcom/google/android/talk/fragments/GroupChatInviteeListFragment;

    iget-object v4, p0, Lcom/google/android/talk/GroupChatInviteeList;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    iget-object v5, p0, Lcom/google/android/talk/GroupChatInviteeList;->mController:Lcom/google/android/talk/fragments/GroupChatInviteeListFragment$Controller;

    invoke-virtual {p0}, Lcom/google/android/talk/GroupChatInviteeList;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/talk/GroupChatInviteeList;->getParticipants(Landroid/content/Intent;)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/talk/fragments/GroupChatInviteeListFragment;-><init>(Lcom/google/android/talk/TalkApp$AccountInfo;Lcom/google/android/talk/fragments/GroupChatInviteeListFragment$Controller;[Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/talk/GroupChatInviteeList;->mList:Lcom/google/android/talk/fragments/GroupChatInviteeListFragment;

    invoke-direct {p0}, Lcom/google/android/talk/GroupChatInviteeList;->registerOnSessionCreatedListeners()V

    invoke-virtual {p0}, Lcom/google/android/talk/GroupChatInviteeList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f100027

    iget-object v4, p0, Lcom/google/android/talk/GroupChatInviteeList;->mList:Lcom/google/android/talk/fragments/GroupChatInviteeListFragment;

    invoke-virtual {v2, v3, v4}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    const v3, 0x7f100015

    invoke-virtual {p0, v3}, Lcom/google/android/talk/GroupChatInviteeList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v3, Lcom/google/android/talk/GroupChatInviteeList$1;

    invoke-direct {v3, p0}, Lcom/google/android/talk/GroupChatInviteeList$1;-><init>(Lcom/google/android/talk/GroupChatInviteeList;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/talk/GroupChatInviteeList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v7, v7}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    :cond_2
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/talk/GroupChatInviteeList;->onBackPressed()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onSearchRequested()Z
    .locals 3

    const/4 v2, 0x0

    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/google/android/talk/GroupChatInviteeList;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/GroupChatInviteeList;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_0
    return v2
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/talk/GtalkServiceActivity;->onStop()V

    invoke-virtual {p0}, Lcom/google/android/talk/GroupChatInviteeList;->finish()V

    return-void
.end method
