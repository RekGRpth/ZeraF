.class public Lcom/google/android/talk/AlertNotificationFullScreenActivity;
.super Landroid/app/Activity;
.source "AlertNotificationFullScreenActivity.java"


# instance fields
.field protected mAccountId:J

.field private mAvatar:Landroid/graphics/Bitmap;

.field private mAvatarView:Landroid/widget/ImageView;

.field private mBuilder:Landroid/app/AlertDialog$Builder;

.field private mCallSession:Lcom/google/android/videochat/CallSession;

.field private mCancelBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mDialog:Landroid/app/Dialog;

.field private mDialogRootView:Landroid/view/View;

.field protected mExistingCall:Lcom/google/android/videochat/CallState;

.field protected mExistingCallNickName:Ljava/lang/String;

.field protected mIsGroupChatInvite:Z

.field protected mIsIncomingCall:Z

.field protected mIsVideo:Z

.field protected mMessage:Ljava/lang/String;

.field private mMessage1View:Landroid/widget/TextView;

.field private mMessage2View:Landroid/widget/TextView;

.field protected mNickName:Ljava/lang/String;

.field protected mRemoteJid:Ljava/lang/String;

.field private mRingerForceStopped:Z

.field protected mShowChatIntent:Landroid/content/Intent;

.field private mShowDialog:Z

.field private final mShowDialogLock:Ljava/lang/Object;

.field protected mTimestamp:J

.field private mVcBinder:Lcom/google/android/videochat/VideoChatServiceBinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowDialog:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowDialogLock:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->dismissDialogAndFinish()V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/talk/AlertNotificationFullScreenActivity;Lcom/google/android/videochat/CallSession;)Lcom/google/android/videochat/CallSession;
    .locals 0
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;
    .param p1    # Lcom/google/android/videochat/CallSession;

    iput-object p1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mCallSession:Lcom/google/android/videochat/CallSession;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->loadContactInfoAndShowDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;

    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mRingerForceStopped:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/talk/AlertNotificationFullScreenActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mRingerForceStopped:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/talk/AlertNotificationFullScreenActivity;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0, p1, p2}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->dumpIntent(Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/talk/AlertNotificationFullScreenActivity;JLjava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;
    .param p1    # J
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->declineCall(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->dismissNotificationAndFinish()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->showDialog()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mAvatar:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/talk/AlertNotificationFullScreenActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mAvatar:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/google/android/talk/AlertNotificationFullScreenActivity;

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mAvatarView:Landroid/widget/ImageView;

    return-object v0
.end method

.method private bindToVideoChatServiceAndShowDialog()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mVcBinder:Lcom/google/android/videochat/VideoChatServiceBinder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/videochat/VideoChatServiceBinder;

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/talk/videochat/VideoChatOutputReceiver;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/videochat/VideoChatServiceBinder;-><init>(Landroid/content/Context;Landroid/content/ComponentName;)V

    iput-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mVcBinder:Lcom/google/android/videochat/VideoChatServiceBinder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mVcBinder:Lcom/google/android/videochat/VideoChatServiceBinder;

    new-instance v1, Lcom/google/android/talk/AlertNotificationFullScreenActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity$2;-><init>(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/VideoChatServiceBinder;->bind(Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;)V

    return-void
.end method

.method private createViews()V
    .locals 3

    new-instance v1, Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x3

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04001f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialogRootView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialogRootView:Landroid/view/View;

    const v2, 0x7f10000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mAvatarView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialogRootView:Landroid/view/View;

    const v2, 0x7f10004a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mMessage1View:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialogRootView:Landroid/view/View;

    const v2, 0x7f10004b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mMessage2View:Landroid/widget/TextView;

    return-void
.end method

.method private declineCall(JLjava/lang/String;)V
    .locals 1
    .param p1    # J
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mCallSession:Lcom/google/android/videochat/CallSession;

    invoke-virtual {v0, p3}, Lcom/google/android/videochat/CallSession;->declineIncomingCall(Ljava/lang/String;)Z

    return-void
.end method

.method private dismissDialog()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowDialogLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialog:Landroid/app/Dialog;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowDialog:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private dismissDialogAndFinish()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mCancelBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mCancelBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mCancelBroadcastReceiver:Landroid/content/BroadcastReceiver;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->dismissDialog()V

    invoke-virtual {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->finish()V

    :cond_1
    return-void
.end method

.method private dismissNotificationAndFinish()V
    .locals 6

    iget-boolean v4, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    if-eqz v4, :cond_0

    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.google.android.talk.CANCEL_NOTIFICATION"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->sendBroadcast(Landroid/content/Intent;)V

    :goto_0
    const-string v4, "talk"

    const-string v5, "##### [AlertNotificationFullScreen] dismissNotificationAndFinish: dismiss dialog"

    invoke-static {v4, v5}, Lcom/google/android/talk/TalkApp;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->dismissDialogAndFinish()V

    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/talk/TalkApp;->getApplication(Landroid/app/Activity;)Lcom/google/android/talk/TalkApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/talk/TalkApp;->getGTalkService()Lcom/google/android/gtalkservice/IGTalkService;

    move-result-object v3

    if-nez v3, :cond_1

    const-string v4, "talk"

    const-string v5, "dismissNotificationAndFinish: no GTalkService object found!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mRemoteJid:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/talk/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v4, 0x1

    :try_start_0
    invoke-interface {v3, v0, v4, v5}, Lcom/google/android/gtalkservice/IGTalkService;->dismissNotificationFor(Ljava/lang/String;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "talk"

    const-string v5, "dismissNotificationAndFinish: caught "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private dumpIntent(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Intent;

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " EXTRA_INTENT_FROM_ADDRESS: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "from"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " EXTRA_INTENT_ACCOUNT_ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "accountId"

    const-wide/16 v3, 0x0

    invoke-virtual {p2, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " EXTRA_INTENT_USERNAME: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "username"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private loadContactInfoAndShowDialog()V
    .locals 13

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mRemoteJid:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/talk/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mExistingCall:Lcom/google/android/videochat/CallState;

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/talk/ContactInfoQuery;

    iget-wide v2, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mAccountId:J

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mExistingCall:Lcom/google/android/videochat/CallState;

    iget-object v4, v1, Lcom/google/android/videochat/CallState;->remoteBareJid:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/talk/ContactInfoQuery;-><init>(Landroid/content/Context;JLjava/lang/String;Landroid/database/ContentObserver;Z)V

    new-instance v1, Lcom/google/android/talk/AlertNotificationFullScreenActivity$6;

    invoke-direct {v1, p0, v0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity$6;-><init>(Lcom/google/android/talk/AlertNotificationFullScreenActivity;Lcom/google/android/talk/ContactInfoQuery;)V

    invoke-virtual {v0, v1}, Lcom/google/android/talk/ContactInfoQuery;->setContactInfoCallback(Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;)V

    :goto_0
    new-instance v6, Lcom/google/android/talk/ContactInfoQuery;

    iget-wide v8, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mAccountId:J

    const/4 v12, 0x1

    move-object v7, p0

    move-object v11, v5

    invoke-direct/range {v6 .. v12}, Lcom/google/android/talk/ContactInfoQuery;-><init>(Landroid/content/Context;JLjava/lang/String;Landroid/database/ContentObserver;Z)V

    new-instance v1, Lcom/google/android/talk/AlertNotificationFullScreenActivity$7;

    invoke-direct {v1, p0, v6, v0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity$7;-><init>(Lcom/google/android/talk/AlertNotificationFullScreenActivity;Lcom/google/android/talk/ContactInfoQuery;Lcom/google/android/talk/ContactInfoQuery;)V

    invoke-virtual {v6, v1}, Lcom/google/android/talk/ContactInfoQuery;->setContactInfoCallback(Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;)V

    invoke-virtual {v6}, Lcom/google/android/talk/ContactInfoQuery;->startQueryForContactInfo()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeDialog()Landroid/app/Dialog;
    .locals 5

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mBuilder:Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialogRootView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->getOnClickListener()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->setPositiveButtonString(Landroid/app/AlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->setNegativeButtonString(Landroid/app/AlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0, v1}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->setCancelCallbackListener(Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setInverseBackgroundForced(Z)V

    new-instance v3, Lcom/google/android/talk/AlertNotificationFullScreenActivity$3;

    invoke-direct {v3, p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity$3;-><init>(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-object v0
.end method

.method private resolveIntent()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->parseIntent(Landroid/content/Intent;)V

    return-void
.end method

.method private showDialog()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowDialogLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowDialog:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->setTitle(Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->setMessageLine1()V

    invoke-virtual {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->setMessageLine2()V

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->makeDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialog:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private startVideochatActivity()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsVideo:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mAccountId:J

    iget-object v2, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mRemoteJid:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/talk/videochat/VideoChatActivity;->startActivityCallInProgress(Landroid/content/Context;JLjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-wide v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mAccountId:J

    iget-object v2, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mRemoteJid:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/talk/BuddyListCombo;->startChatScreenActivity(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method acceptCall(JLjava/lang/String;)V
    .locals 3
    .param p1    # J
    .param p3    # Ljava/lang/String;

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Incoming call alert] acceptCall for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mCallSession:Lcom/google/android/videochat/CallSession;

    invoke-virtual {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/talk/videochat/TalkOngoingNotificationFactory;->getInstance(Landroid/content/Context;)Lcom/google/android/videochat/OngoingNotificationFactory;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, Lcom/google/android/videochat/CallSession;->acceptIncomingCall(Ljava/lang/String;Lcom/google/android/videochat/OngoingNotificationFactory;)Z

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->startVideochatActivity()V

    return-void
.end method

.method protected getOnClickListener()Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    new-instance v0, Lcom/google/android/talk/AlertNotificationFullScreenActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity$4;-><init>(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)V

    return-object v0
.end method

.method protected handleNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const-string v1, "incoming_call"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "talk"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "incomingCall: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/talk/TalkApp;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->parseIntent(Landroid/content/Intent;)V

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->dismissDialog()V

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->createViews()V

    iget-object v2, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowDialogLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowDialog:Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->bindToVideoChatServiceAndShowDialog()V

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->loadContactInfoAndShowDialog()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->resolveIntent()V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->requestWindowFeature(I)Z

    iget-boolean v2, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "screen_off"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const v2, 0x200081

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    :cond_0
    new-instance v2, Lcom/google/android/talk/AlertNotificationFullScreenActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity$1;-><init>(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)V

    iput-object v2, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mCancelBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.google.android.talk.CANCEL_NOTIFICATION"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.talk.CANCEL_POPUP_NOTIFICATION"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mCancelBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->createViews()V

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->handleNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->bindToVideoChatServiceAndShowDialog()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->loadContactInfoAndShowDialog()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mVcBinder:Lcom/google/android/videochat/VideoChatServiceBinder;

    invoke-virtual {v0}, Lcom/google/android/videochat/VideoChatServiceBinder;->unbind()V

    :cond_0
    return-void
.end method

.method protected parseIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x0

    const-string v0, "from"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mRemoteJid:Ljava/lang/String;

    const-string v0, "accountId"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mAccountId:J

    const-string v0, "message"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mMessage:Ljava/lang/String;

    const-string v0, "android.intent.extra.INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowChatIntent:Landroid/content/Intent;

    const-string v0, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mTimestamp:J

    const-string v0, "incoming_call"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowChatIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mShowChatIntent:Landroid/content/Intent;

    const-string v1, "is_muc"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsGroupChatInvite:Z

    const-string v0, "parseIntent"

    invoke-direct {p0, v0, p1}, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->dumpIntent(Ljava/lang/String;Landroid/content/Intent;)V

    :goto_0
    const-string v0, "isvideo"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsVideo:Z

    const-string v0, "iscollision"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/videochat/CallState;

    iput-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mExistingCall:Lcom/google/android/videochat/CallState;

    return-void

    :cond_0
    iput-boolean v3, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsGroupChatInvite:Z

    goto :goto_0
.end method

.method protected setCancelCallbackListener(Landroid/app/AlertDialog$Builder;)V
    .locals 1
    .param p1    # Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/google/android/talk/AlertNotificationFullScreenActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/talk/AlertNotificationFullScreenActivity$5;-><init>(Lcom/google/android/talk/AlertNotificationFullScreenActivity;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method protected setMessageLine1()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mMessage1View:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mNickName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mRemoteJid:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/talk/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mNickName:Ljava/lang/String;

    goto :goto_0
.end method

.method protected setMessageLine2()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mMessage2View:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mMessage2View:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setNegativeButtonString(Landroid/app/AlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/app/AlertDialog$Builder;
    .param p2    # Landroid/content/DialogInterface$OnClickListener;

    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0c00c0

    :goto_0
    invoke-virtual {p1, v0, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void

    :cond_0
    const v0, 0x7f0c00c8

    goto :goto_0
.end method

.method protected setPositiveButtonString(Landroid/app/AlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/app/AlertDialog$Builder;
    .param p2    # Landroid/content/DialogInterface$OnClickListener;

    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0c006c

    :goto_0
    invoke-virtual {p1, v0, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void

    :cond_0
    const v0, 0x7f0c00c9

    goto :goto_0
.end method

.method protected setTitle(Landroid/app/AlertDialog$Builder;)V
    .locals 1
    .param p1    # Landroid/app/AlertDialog$Builder;

    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsIncomingCall:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsVideo:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0c00ba

    :goto_0
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    :goto_1
    return-void

    :cond_0
    const v0, 0x7f0c00bc

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/talk/AlertNotificationFullScreenActivity;->mIsGroupChatInvite:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0c00ca

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    :cond_2
    const v0, 0x7f0c00c7

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method
