.class public final Lcom/adobe/xmp/impl/ISO8601Converter;
.super Ljava/lang/Object;
.source "ISO8601Converter.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/adobe/xmp/XMPDateTime;
    .locals 1
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/xmp/XMPException;
        }
    .end annotation

    new-instance v0, Lcom/adobe/xmp/impl/XMPDateTimeImpl;

    invoke-direct {v0}, Lcom/adobe/xmp/impl/XMPDateTimeImpl;-><init>()V

    invoke-static {p0, v0}, Lcom/adobe/xmp/impl/ISO8601Converter;->parse(Ljava/lang/String;Lcom/adobe/xmp/XMPDateTime;)Lcom/adobe/xmp/XMPDateTime;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/String;Lcom/adobe/xmp/XMPDateTime;)Lcom/adobe/xmp/XMPDateTime;
    .locals 11
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/adobe/xmp/XMPDateTime;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/xmp/XMPException;
        }
    .end annotation

    invoke-static {p0}, Lcom/adobe/xmp/impl/ParameterAsserts;->assertNotNull(Ljava/lang/Object;)V

    new-instance v1, Lcom/adobe/xmp/impl/ParseState;

    invoke-direct {v1, p0}, Lcom/adobe/xmp/impl/ParseState;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/adobe/xmp/impl/ParseState;->ch(I)C

    move-result v8

    const/16 v9, 0x54

    if-eq v8, v9, :cond_1

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->length()I

    move-result v8

    const/4 v9, 0x2

    if-lt v8, v9, :cond_0

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Lcom/adobe/xmp/impl/ParseState;->ch(I)C

    move-result v8

    const/16 v9, 0x3a

    if-eq v8, v9, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->length()I

    move-result v8

    const/4 v9, 0x3

    if-lt v8, v9, :cond_3

    const/4 v8, 0x2

    invoke-virtual {v1, v8}, Lcom/adobe/xmp/impl/ParseState;->ch(I)C

    move-result v8

    const/16 v9, 0x3a

    if-ne v8, v9, :cond_3

    :cond_1
    const/4 v3, 0x1

    :goto_0
    if-nez v3, :cond_b

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/adobe/xmp/impl/ParseState;->ch(I)C

    move-result v8

    const/16 v9, 0x2d

    if-ne v8, v9, :cond_2

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    :cond_2
    const-string v8, "Invalid year in date string"

    const/16 v9, 0x270f

    invoke-virtual {v1, v8, v9}, Lcom/adobe/xmp/impl/ParseState;->gatherInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2d

    if-eq v8, v9, :cond_4

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, after year"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_3
    const/4 v3, 0x0

    goto :goto_0

    :cond_4
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/adobe/xmp/impl/ParseState;->ch(I)C

    move-result v8

    const/16 v9, 0x2d

    if-ne v8, v9, :cond_5

    neg-int v7, v7

    :cond_5
    invoke-interface {p1, v7}, Lcom/adobe/xmp/XMPDateTime;->setYear(I)V

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-nez v8, :cond_7

    :cond_6
    return-object p1

    :cond_7
    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    const-string v8, "Invalid month in date string"

    const/16 v9, 0xc

    invoke-virtual {v1, v8, v9}, Lcom/adobe/xmp/impl/ParseState;->gatherInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2d

    if-eq v8, v9, :cond_8

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, after month"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_8
    invoke-interface {p1, v7}, Lcom/adobe/xmp/XMPDateTime;->setMonth(I)V

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    const-string v8, "Invalid day in date string"

    const/16 v9, 0x1f

    invoke-virtual {v1, v8, v9}, Lcom/adobe/xmp/impl/ParseState;->gatherInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x54

    if-eq v8, v9, :cond_9

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, after day"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_9
    invoke-interface {p1, v7}, Lcom/adobe/xmp/XMPDateTime;->setDay(I)V

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    :goto_1
    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x54

    if-ne v8, v9, :cond_c

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    :cond_a
    const-string v8, "Invalid hour in date string"

    const/16 v9, 0x17

    invoke-virtual {v1, v8, v9}, Lcom/adobe/xmp/impl/ParseState;->gatherInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x3a

    if-eq v8, v9, :cond_d

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, after hour"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_b
    const/4 v8, 0x1

    invoke-interface {p1, v8}, Lcom/adobe/xmp/XMPDateTime;->setMonth(I)V

    const/4 v8, 0x1

    invoke-interface {p1, v8}, Lcom/adobe/xmp/XMPDateTime;->setDay(I)V

    goto :goto_1

    :cond_c
    if-nez v3, :cond_a

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, missing \'T\' after date"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_d
    invoke-interface {p1, v7}, Lcom/adobe/xmp/XMPDateTime;->setHour(I)V

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    const-string v8, "Invalid minute in date string"

    const/16 v9, 0x3b

    invoke-virtual {v1, v8, v9}, Lcom/adobe/xmp/impl/ParseState;->gatherInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_e

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x3a

    if-eq v8, v9, :cond_e

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x5a

    if-eq v8, v9, :cond_e

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2b

    if-eq v8, v9, :cond_e

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2d

    if-eq v8, v9, :cond_e

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, after minute"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_e
    invoke-interface {p1, v7}, Lcom/adobe/xmp/XMPDateTime;->setMinute(I)V

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x3a

    if-ne v8, v9, :cond_13

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    const-string v8, "Invalid whole seconds in date string"

    const/16 v9, 0x3b

    invoke-virtual {v1, v8, v9}, Lcom/adobe/xmp/impl/ParseState;->gatherInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_f

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2e

    if-eq v8, v9, :cond_f

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x5a

    if-eq v8, v9, :cond_f

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2b

    if-eq v8, v9, :cond_f

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2d

    if-eq v8, v9, :cond_f

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, after whole seconds"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_f
    invoke-interface {p1, v7}, Lcom/adobe/xmp/XMPDateTime;->setSecond(I)V

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2e

    if-ne v8, v9, :cond_13

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->pos()I

    move-result v0

    const-string v8, "Invalid fractional seconds in date string"

    const v9, 0x3b9ac9ff

    invoke-virtual {v1, v8, v9}, Lcom/adobe/xmp/impl/ParseState;->gatherInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x5a

    if-eq v8, v9, :cond_10

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2b

    if-eq v8, v9, :cond_10

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2d

    if-eq v8, v9, :cond_10

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, after fractional second"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_10
    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->pos()I

    move-result v8

    sub-int v0, v8, v0

    :goto_2
    const/16 v8, 0x9

    if-le v0, v8, :cond_11

    div-int/lit8 v7, v7, 0xa

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_11
    :goto_3
    const/16 v8, 0x9

    if-ge v0, v8, :cond_12

    mul-int/lit8 v7, v7, 0xa

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_12
    invoke-interface {p1, v7}, Lcom/adobe/xmp/XMPDateTime;->setNanoSecond(I)V

    :cond_13
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x5a

    if-ne v8, v9, :cond_15

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    :cond_14
    :goto_4
    mul-int/lit16 v8, v4, 0xe10

    mul-int/lit16 v8, v8, 0x3e8

    mul-int/lit8 v9, v5, 0x3c

    mul-int/lit16 v9, v9, 0x3e8

    add-int/2addr v8, v9

    mul-int v2, v8, v6

    new-instance v8, Ljava/util/SimpleTimeZone;

    const-string v9, ""

    invoke-direct {v8, v2, v9}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v8}, Lcom/adobe/xmp/XMPDateTime;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, extra chars at end"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_15
    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_14

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2b

    if-ne v8, v9, :cond_16

    const/4 v6, 0x1

    :goto_5
    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    const-string v8, "Invalid time zone hour in date string"

    const/16 v9, 0x17

    invoke-virtual {v1, v8, v9}, Lcom/adobe/xmp/impl/ParseState;->gatherInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x3a

    if-eq v8, v9, :cond_18

    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Invalid date string, after time zone hour"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_16
    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->ch()C

    move-result v8

    const/16 v9, 0x2d

    if-ne v8, v9, :cond_17

    const/4 v6, -0x1

    goto :goto_5

    :cond_17
    new-instance v8, Lcom/adobe/xmp/XMPException;

    const-string v9, "Time zone must begin with \'Z\', \'+\', or \'-\'"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10}, Lcom/adobe/xmp/XMPException;-><init>(Ljava/lang/String;I)V

    throw v8

    :cond_18
    invoke-virtual {v1}, Lcom/adobe/xmp/impl/ParseState;->skip()V

    const-string v8, "Invalid time zone minute in date string"

    const/16 v9, 0x3b

    invoke-virtual {v1, v8, v9}, Lcom/adobe/xmp/impl/ParseState;->gatherInt(Ljava/lang/String;I)I

    move-result v5

    goto :goto_4
.end method

.method public static render(Lcom/adobe/xmp/XMPDateTime;)Ljava/lang/String;
    .locals 15
    .param p0    # Lcom/adobe/xmp/XMPDateTime;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v1, Ljava/text/DecimalFormat;

    const-string v9, "0000"

    new-instance v10, Ljava/text/DecimalFormatSymbols;

    sget-object v11, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v10, v11}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v1, v9, v10}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getYear()I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v1, v9, v10}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getMonth()I

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    :goto_0
    return-object v9

    :cond_0
    const-string v9, "\'-\'00"

    invoke-virtual {v1, v9}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getMonth()I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v1, v9, v10}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getDay()I

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    :cond_1
    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getDay()I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v1, v9, v10}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getHour()I

    move-result v9

    if-nez v9, :cond_2

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getMinute()I

    move-result v9

    if-nez v9, :cond_2

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getSecond()I

    move-result v9

    if-nez v9, :cond_2

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getNanoSecond()I

    move-result v9

    if-nez v9, :cond_2

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v9

    if-eqz v9, :cond_5

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v9

    if-eqz v9, :cond_5

    :cond_2
    const/16 v9, 0x54

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v9, "00"

    invoke-virtual {v1, v9}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getHour()I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v1, v9, v10}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v9, 0x3a

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getMinute()I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v1, v9, v10}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getSecond()I

    move-result v9

    if-nez v9, :cond_3

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getNanoSecond()I

    move-result v9

    if-eqz v9, :cond_4

    :cond_3
    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getSecond()I

    move-result v9

    int-to-double v9, v9

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getNanoSecond()I

    move-result v11

    int-to-double v11, v11

    const-wide v13, 0x41cdcd6500000000L

    div-double/2addr v11, v13

    add-double v3, v9, v11

    const-string v9, ":00.#########"

    invoke-virtual {v1, v9}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_4
    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v9

    if-eqz v9, :cond_5

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getCalendar()Ljava/util/Calendar;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-interface {p0}, Lcom/adobe/xmp/XMPDateTime;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    if-nez v2, :cond_6

    const/16 v9, 0x5a

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_5
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    :cond_6
    const v9, 0x36ee80

    div-int v5, v2, v9

    const v9, 0x36ee80

    rem-int v9, v2, v9

    const v10, 0xea60

    div-int/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v8

    const-string v9, "+00;-00"

    invoke-virtual {v1, v9}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    int-to-long v9, v5

    invoke-virtual {v1, v9, v10}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v9, ":00"

    invoke-virtual {v1, v9}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    int-to-long v9, v8

    invoke-virtual {v1, v9, v10}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method
