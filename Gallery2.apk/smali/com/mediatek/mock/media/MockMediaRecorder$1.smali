.class Lcom/mediatek/mock/media/MockMediaRecorder$1;
.super Ljava/lang/Object;
.source "MockMediaRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mock/media/MockMediaRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mock/media/MockMediaRecorder;


# direct methods
.method constructor <init>(Lcom/mediatek/mock/media/MockMediaRecorder;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mock/media/MockMediaRecorder$1;->this$0:Lcom/mediatek/mock/media/MockMediaRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const-string v7, "MockMediaRecorder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Saving path = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/mock/media/MockMediaRecorder$1;->this$0:Lcom/mediatek/mock/media/MockMediaRecorder;

    invoke-static {v9}, Lcom/mediatek/mock/media/MockMediaRecorder;->access$000(Lcom/mediatek/mock/media/MockMediaRecorder;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/mediatek/mock/media/MockMediaRecorder$1;->this$0:Lcom/mediatek/mock/media/MockMediaRecorder;

    invoke-static {v7}, Lcom/mediatek/mock/media/MockMediaRecorder;->access$000(Lcom/mediatek/mock/media/MockMediaRecorder;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/mediatek/mock/media/MockMediaRecorder$1;->this$0:Lcom/mediatek/mock/media/MockMediaRecorder;

    invoke-static {v7}, Lcom/mediatek/mock/media/MockMediaRecorder;->access$100(Lcom/mediatek/mock/media/MockMediaRecorder;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070006

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    const/4 v4, 0x0

    const/16 v7, 0x400

    new-array v0, v7, [B

    const/4 v6, -0x1

    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    iget-object v7, p0, Lcom/mediatek/mock/media/MockMediaRecorder$1;->this$0:Lcom/mediatek/mock/media/MockMediaRecorder;

    invoke-static {v7}, Lcom/mediatek/mock/media/MockMediaRecorder;->access$000(Lcom/mediatek/mock/media/MockMediaRecorder;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v4, v5

    :goto_1
    :try_start_2
    const-string v7, "MockMediaRecorder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/mock/media/MockMediaRecorder$1;->this$0:Lcom/mediatek/mock/media/MockMediaRecorder;

    invoke-static {v9}, Lcom/mediatek/mock/media/MockMediaRecorder;->access$000(Lcom/mediatek/mock/media/MockMediaRecorder;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " not found!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_1
    :goto_2
    return-void

    :cond_2
    if-eqz v2, :cond_3

    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    :cond_4
    :goto_3
    move-object v4, v5

    goto :goto_2

    :catch_1
    move-exception v3

    :goto_4
    :try_start_5
    const-string v7, "MockMediaRecorder"

    const-string v8, "read blank.jpg in raw reault in error"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_5

    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_5
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_2

    :catch_2
    move-exception v1

    const-string v7, "MockMediaRecorder"

    const-string v8, "close inputStream fail"

    :goto_5
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catchall_0
    move-exception v7

    :goto_6
    if-eqz v2, :cond_6

    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_6
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :cond_7
    :goto_7
    throw v7

    :cond_8
    iget-object v7, p0, Lcom/mediatek/mock/media/MockMediaRecorder$1;->this$0:Lcom/mediatek/mock/media/MockMediaRecorder;

    invoke-static {v7}, Lcom/mediatek/mock/media/MockMediaRecorder;->access$200(Lcom/mediatek/mock/media/MockMediaRecorder;)Ljava/io/FileDescriptor;

    move-result-object v7

    if-eqz v7, :cond_1

    const-string v7, "MockMediaRecorder"

    const-string v8, "mFd != null"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_3
    move-exception v1

    const-string v8, "MockMediaRecorder"

    const-string v9, "close inputStream fail"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :catch_4
    move-exception v1

    const-string v7, "MockMediaRecorder"

    const-string v8, "close inputStream fail"

    goto :goto_5

    :catch_5
    move-exception v1

    const-string v7, "MockMediaRecorder"

    const-string v8, "close inputStream fail"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catchall_1
    move-exception v7

    move-object v4, v5

    goto :goto_6

    :catch_6
    move-exception v3

    move-object v4, v5

    goto :goto_4

    :catch_7
    move-exception v1

    goto/16 :goto_1
.end method
