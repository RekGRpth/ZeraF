.class public Lcom/mediatek/mock/hardware/CaptureThread;
.super Ljava/lang/Thread;
.source "CaptureThread.java"


# static fields
.field private static final CAPTURE_TIME:I = 0xc8

.field private static final IDLE_TIME:I = 0x4e20

.field private static final TAG:Ljava/lang/String; = "CaptureThread"

.field private static sPictureCount:I


# instance fields
.field private mActCapture:Z

.field private mCameraSound:Landroid/media/MediaActionSound;

.field private mCapNum:I

.field private mContext:Landroid/content/Context;

.field private mCurrentCount:I

.field private mHandler:Landroid/os/Handler;

.field private mQuit:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/mediatek/mock/hardware/CaptureThread;->sPictureCount:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;Landroid/media/MediaActionSound;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/media/MediaActionSound;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput v0, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCapNum:I

    iput v0, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    iput-boolean v0, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mActCapture:Z

    iput-boolean v0, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mQuit:Z

    iput-object p1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mHandler:Landroid/os/Handler;

    iput-object p2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCameraSound:Landroid/media/MediaActionSound;

    return-void
.end method

.method private onPictureCreate()[B
    .locals 9

    const/16 v8, 0x400

    iget-object v6, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mContext:Landroid/content/Context;

    if-nez v6, :cond_0

    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_0
    sget v6, Lcom/mediatek/mock/hardware/CaptureThread;->sPictureCount:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/mediatek/mock/hardware/CaptureThread;->sPictureCount:I

    sget v6, Lcom/mediatek/mock/hardware/CaptureThread;->sPictureCount:I

    rem-int/lit8 v6, v6, 0x2

    sput v6, Lcom/mediatek/mock/hardware/CaptureThread;->sPictureCount:I

    sget v6, Lcom/mediatek/mock/hardware/CaptureThread;->sPictureCount:I

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070001

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    :goto_1
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-array v0, v8, [B

    const/4 v5, -0x1

    :goto_2
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    const/4 v6, 0x0

    invoke-virtual {v4, v0, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v3

    :try_start_1
    const-string v6, "CaptureThread"

    const-string v7, "read blank.jpg in raw reault in error"

    invoke-static {v6, v7}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070005

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    goto :goto_1

    :catchall_0
    move-exception v6

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    throw v6

    :catch_1
    move-exception v1

    const-string v7, "CaptureThread"

    const-string v8, "close inputStream fail"

    invoke-static {v7, v8}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catch_2
    move-exception v1

    const-string v6, "CaptureThread"

    const-string v7, "close inputStream fail"

    :goto_5
    invoke-static {v6, v7}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_2
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_3

    :catch_3
    move-exception v1

    const-string v6, "CaptureThread"

    const-string v7, "close inputStream fail"

    goto :goto_5
.end method


# virtual methods
.method public declared-synchronized cancelCapture()V
    .locals 5

    monitor-enter p0

    :try_start_0
    const-string v1, "CaptureThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancelCapture, CurrentCount = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mActCapture:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mHandler:Landroid/os/Handler;

    const/high16 v2, 0x40000000

    const/4 v3, 0x6

    iget v4, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mActCapture:Z

    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized capture()V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "CaptureThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "capture, CurrentCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mActCapture:Z

    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public doCapture()V
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mHandler:Landroid/os/Handler;

    if-nez v2, :cond_2

    :cond_0
    const-string v2, "CaptureThread"

    const-string v3, "doCapture not ready"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x100

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0xc8

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCapNum:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const-string v2, "CaptureThread"

    const-string v3, "play shutter sound"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCameraSound:Landroid/media/MediaActionSound;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/MediaActionSound;->play(I)V

    :cond_1
    return-void

    :cond_2
    const-string v2, "CaptureThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doCapture, CurrentCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/mock/hardware/CaptureThread;->onPictureCreate()[B

    move-result-object v0

    goto :goto_0
.end method

.method public declared-synchronized quit()V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "CaptureThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "quit, CurrentCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mActCapture:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mQuit:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 4

    :goto_0
    iget-boolean v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mActCapture:Z

    if-eqz v1, :cond_0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/mock/hardware/CaptureThread;->doCapture()V

    iget v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    iget v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    iget v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCapNum:I

    if-ne v1, v2, :cond_1

    const-string v1, "CaptureThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reach count break, CurrentCount = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mActCapture:Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-enter p0

    :try_start_1
    iget-boolean v1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mQuit:Z

    if-eqz v1, :cond_2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :cond_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    const-string v1, "CaptureThread"

    const-string v2, "Into capture time"

    invoke-static {v1, v2}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v1, 0xc8

    invoke-static {v1, v2}, Lcom/mediatek/mock/hardware/CaptureThread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    :cond_2
    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    const-string v1, "CaptureThread"

    const-string v2, "Into Idle time"

    invoke-static {v1, v2}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v1, 0x4e20

    invoke-static {v1, v2}, Lcom/mediatek/mock/hardware/CaptureThread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catchall_1
    move-exception v1

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v1
.end method

.method public declared-synchronized setCaptureNum(I)V
    .locals 3
    .param p1    # I

    monitor-enter p0

    :try_start_0
    const-string v0, "CaptureThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCaptureNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CurrentCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCurrentCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iput p1, p0, Lcom/mediatek/mock/hardware/CaptureThread;->mCapNum:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
