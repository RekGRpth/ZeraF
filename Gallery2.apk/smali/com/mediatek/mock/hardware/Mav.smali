.class public Lcom/mediatek/mock/hardware/Mav;
.super Ljava/lang/Thread;
.source "Mav.java"


# static fields
.field private static final CAPTURE_INTERVAL:I = 0x64

.field private static final TAG:Ljava/lang/String; = "Mav"


# instance fields
.field private mCapturePath:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCurrentNum:I

.field private mHandler:Landroid/os/Handler;

.field private mInCapture:Z

.field private mMavCaptureNum:I

.field private mMerge:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 2
    .param p1    # Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/16 v0, 0xf

    iput v0, p0, Lcom/mediatek/mock/hardware/Mav;->mMavCaptureNum:I

    iput v1, p0, Lcom/mediatek/mock/hardware/Mav;->mCurrentNum:I

    iput-boolean v1, p0, Lcom/mediatek/mock/hardware/Mav;->mInCapture:Z

    iput-object p1, p0, Lcom/mediatek/mock/hardware/Mav;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private onPictureCreate()V
    .locals 10

    iget-object v7, p0, Lcom/mediatek/mock/hardware/Mav;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070003

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    const/4 v4, 0x0

    const/16 v7, 0x400

    new-array v0, v7, [B

    const/4 v6, -0x1

    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    iget-object v7, p0, Lcom/mediatek/mock/hardware/Mav;->mCapturePath:Ljava/lang/String;

    invoke-direct {v5, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v4, v5

    :goto_1
    :try_start_2
    const-string v7, "Mav"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/mock/hardware/Mav;->mCapturePath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " not found!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_1
    :goto_2
    return-void

    :cond_2
    if-eqz v2, :cond_3

    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    :cond_4
    :goto_3
    move-object v4, v5

    goto :goto_2

    :catch_1
    move-exception v3

    :goto_4
    :try_start_5
    const-string v7, "Mav"

    const-string v8, "read blank.jpg in raw reault in error"

    invoke-static {v7, v8}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_5

    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_5
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_2

    :catch_2
    move-exception v1

    const-string v7, "Mav"

    const-string v8, "close inputStream fail"

    :goto_5
    invoke-static {v7, v8}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catchall_0
    move-exception v7

    :goto_6
    if-eqz v2, :cond_6

    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_6
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :cond_7
    :goto_7
    throw v7

    :catch_3
    move-exception v1

    const-string v8, "Mav"

    const-string v9, "close inputStream fail"

    invoke-static {v8, v9}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :catch_4
    move-exception v1

    const-string v7, "Mav"

    const-string v8, "close inputStream fail"

    goto :goto_5

    :catch_5
    move-exception v1

    const-string v7, "Mav"

    const-string v8, "close inputStream fail"

    invoke-static {v7, v8}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catchall_1
    move-exception v7

    move-object v4, v5

    goto :goto_6

    :catch_6
    move-exception v3

    move-object v4, v5

    goto :goto_4

    :catch_7
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 3

    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/mediatek/mock/hardware/Mav;->mInCapture:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/mediatek/mock/hardware/Mav;->mCurrentNum:I

    iget v2, p0, Lcom/mediatek/mock/hardware/Mav;->mMavCaptureNum:I

    if-ge v1, v2, :cond_1

    const-wide/16 v1, 0x64

    :try_start_0
    invoke-static {v1, v2}, Lcom/mediatek/mock/hardware/Mav;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-boolean v1, p0, Lcom/mediatek/mock/hardware/Mav;->mInCapture:Z

    if-nez v1, :cond_3

    :cond_1
    iget-boolean v1, p0, Lcom/mediatek/mock/hardware/Mav;->mMerge:Z

    if-eqz v1, :cond_4

    const-string v1, "Mav"

    const-string v2, "Save mpo file"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/mock/hardware/Mav;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/mediatek/mock/hardware/Mav;->onPictureCreate()V

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/mock/hardware/Mav;->sendFrameMsg()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/mock/hardware/Mav;->mMerge:Z

    :goto_2
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Mav"

    const-string v2, "get Notify"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/mediatek/mock/hardware/Mav;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/mock/hardware/Mav;->sendFrameMsg()V

    goto :goto_0

    :cond_4
    const-string v1, "Mav"

    const-string v2, "clear frame buff"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public sendFrameMsg()V
    .locals 5

    iget-object v1, p0, Lcom/mediatek/mock/hardware/Mav;->mHandler:Landroid/os/Handler;

    const/high16 v2, 0x40000000

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/mock/hardware/Mav;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public setCapturePath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mock/hardware/Mav;->mCapturePath:Ljava/lang/String;

    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/mock/hardware/Mav;->mContext:Landroid/content/Context;

    return-void
.end method

.method public declared-synchronized startMAV(I)V
    .locals 2
    .param p1    # I

    monitor-enter p0

    :try_start_0
    const-string v0, "Mav"

    const-string v1, "startMav"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/mediatek/mock/hardware/Mav;->mMavCaptureNum:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/mock/hardware/Mav;->mInCapture:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopMAV(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    const-string v1, "Mav"

    const-string v2, "stopMav"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/mock/hardware/Mav;->mInCapture:Z

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/mediatek/mock/hardware/Mav;->mMerge:Z

    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
