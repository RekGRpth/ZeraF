.class public Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;
.super Lcom/mediatek/gallery3d/ext/ActivityHooker;
.source "ActivityHookerGroup.java"


# instance fields
.field private mHooks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/gallery3d/ext/IActivityHooker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addHooker(Lcom/mediatek/gallery3d/ext/IActivityHooker;)Z
    .locals 1
    .param p1    # Lcom/mediatek/gallery3d/ext/IActivityHooker;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getHooker(I)Lcom/mediatek/gallery3d/ext/IActivityHooker;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    return-object v0
.end method

.method public init(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/content/Intent;

    invoke-super {p0, p1, p2}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->init(Landroid/app/Activity;Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v0, p1, p2}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->init(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onCreate(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v0, p1}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->onCreate(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v1, p1}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    if-nez v0, :cond_0

    move v0, v3

    goto :goto_0

    :cond_1
    return v0
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onDestroy()V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->onDestroy()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v1, p1}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    if-nez v0, :cond_0

    move v0, v3

    goto :goto_0

    :cond_1
    return v0
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onPause()V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->onPause()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v1, p1}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    if-nez v0, :cond_0

    move v0, v3

    goto :goto_0

    :cond_1
    return v0
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onResume()V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->onResume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onStart()V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->onStart()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3

    invoke-super {p0}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->onStop()V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->onStop()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public removeHooker(Lcom/mediatek/gallery3d/ext/IActivityHooker;)Z
    .locals 1
    .param p1    # Lcom/mediatek/gallery3d/ext/IActivityHooker;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;

    invoke-super {p0, p1, p2}, Lcom/mediatek/gallery3d/ext/ActivityHooker;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/ext/IActivityHooker;

    invoke-interface {v0, p1, p2}, Lcom/mediatek/gallery3d/ext/IActivityHooker;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/ext/ActivityHookerGroup;->mHooks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
