.class public Lcom/mediatek/gallery3d/util/MediatekMMProfile;
.super Ljava/lang/Object;
.source "MediatekMMProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;
    }
.end annotation


# static fields
.field private static EVENT_ALBUMSETPAGE_ACTIVITY:I

.field private static EVENT_ALBUMSETPAGE_DECODE_SCREENNAIL_JOB:I

.field private static EVENT_ALBUMSETPAGE_DECODE_SCREENNAIL_LISTENER:I

.field private static EVENT_ALBUMSETPAGE_DECODE_SCREENNAIL_SUBMIT:I

.field private static EVENT_ALBUMSETPAGE_ON_CREATE:I

.field private static EVENT_ALBUMSETPAGE_ON_RESUME:I

.field private static EVENT_ALBUMSETPAGE_RELOAD_DATA:I

.field private static EVENT_DRAW_SCREEN_NAIL:I

.field private static EVENT_FRAME_AVAILABLE:I

.field private static EVENT_FRAME_DRAW_AVAILABLE:I

.field private static EVENT_GALLERY_ACTIVITY:I

.field private static EVENT_GALLERY_DATALOADER:I

.field private static EVENT_GALLERY_ON_CREATE:I

.field private static EVENT_GALLERY_ON_DESTROY:I

.field private static EVENT_GALLERY_ON_PAUSE:I

.field private static EVENT_GALLERY_ON_RESUME:I

.field private static EVENT_GALLERY_ROOT:I

.field private static EVENT_GALLERY_START_UP:I

.field private static EVENT_GALLERY_VTNP_ONDRAWFRAME:I

.field private static EVENT_GLROOTVIEW:I

.field private static EVENT_GLROOTVIEW_ONDRAWFRAME:I

.field private static EVENT_GLROOTVIEW_REQUEST_RENDER:I

.field private static EVENT_PHOTOPAGE_ACTIVITY:I

.field private static EVENT_PHOTOPAGE_DECODE_SCREENNAIL_JOB:I

.field private static EVENT_PHOTOPAGE_DECODE_SCREENNAIL_LISTENER:I

.field private static EVENT_PHOTOPAGE_DECODE_SCREENNAIL_SUBMIT:I

.field private static EVENT_PHOTOPAGE_FIRE_DATA_CHANGE:I

.field private static EVENT_PHOTOPAGE_GET_UPDATE_INFO:I

.field private static EVENT_PHOTOPAGE_ON_CREATE:I

.field private static EVENT_PHOTOPAGE_ON_RESUME:I

.field private static EVENT_PHOTOPAGE_RELOAD_DATA:I

.field private static EVENT_PHOTOPAGE_UPDATE_CONTENT:I

.field private static EVENT_PHOTOPAGE_UPDATE_IMAGE_CACHE:I

.field private static EVENT_PHOTOPAGE_UPDATE_IMAGE_REQUEST:I

.field private static EVENT_PHOTOPAGE_UPDATE_SLIDING_WINDOW:I

.field private static EVENT_PHOTOPAGE_UPDATE_TILE_PROVIDER:I

.field private static NAME_ALBUMSETPAGE_ACTIVITY:Ljava/lang/String;

.field private static NAME_ALBUMSETPAGE_DECODE_SCREENNAIL_JOB:Ljava/lang/String;

.field private static NAME_ALBUMSETPAGE_DECODE_SCREENNAIL_LISTENER:Ljava/lang/String;

.field private static NAME_ALBUMSETPAGE_DECODE_SCREENNAIL_SUBMIT:Ljava/lang/String;

.field private static NAME_ALBUMSETPAGE_ON_CREATE:Ljava/lang/String;

.field private static NAME_ALBUMSETPAGE_ON_RESUME:Ljava/lang/String;

.field private static NAME_ALBUMSETPAGE_RELOAD_DATA:Ljava/lang/String;

.field private static NAME_DRAW_SCREEN_NAIL:Ljava/lang/String;

.field private static NAME_FIRST_FRAME_AVAILABLE:Ljava/lang/String;

.field private static NAME_FRAME_AVAILABLE:Ljava/lang/String;

.field private static NAME_GALLERY_ACTIVITY:Ljava/lang/String;

.field private static NAME_GALLERY_DATALOADER:Ljava/lang/String;

.field private static NAME_GALLERY_ON_CREATE:Ljava/lang/String;

.field private static NAME_GALLERY_ON_DESTROY:Ljava/lang/String;

.field private static NAME_GALLERY_ON_PAUSE:Ljava/lang/String;

.field private static NAME_GALLERY_ON_RESUME:Ljava/lang/String;

.field private static NAME_GALLERY_ROOT:Ljava/lang/String;

.field private static NAME_GALLERY_START_UP:Ljava/lang/String;

.field private static NAME_GALLERY_VTNP_ONDRAWFRAME:Ljava/lang/String;

.field private static NAME_GLROOTVIEW:Ljava/lang/String;

.field private static NAME_GLROOTVIEW_ONDRAWFRAME:Ljava/lang/String;

.field private static NAME_GLROOTVIEW_REQUEST_RENDER:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_ACTIVITY:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_DECODE_SCREENNAIL_JOB:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_DECODE_SCREENNAIL_LISTENER:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_DECODE_SCREENNAIL_SUBMIT:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_FIRE_DATA_CHANGE:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_GET_UPDATE_INFO:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_ON_CREATE:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_ON_RESUME:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_RELOAD_DATA:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_UPDATE_CONTENT:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_UPDATE_IMAGE_CACHE:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_UPDATE_IMAGE_REQUEST:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_UPDATE_SLIDING_WINDOW:Ljava/lang/String;

.field private static NAME_PHOTOPAGE_UPDATE_TILE_PROVIDER:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "GALLERYApp"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ROOT:Ljava/lang/String;

    const-string v0, "Gallery2VideoThumbnailPlayback"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_VTNP_ONDRAWFRAME:Ljava/lang/String;

    const-string v0, "FrameAvailable"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_FRAME_AVAILABLE:Ljava/lang/String;

    const-string v0, "FirstFrameAvailable"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_FIRST_FRAME_AVAILABLE:Ljava/lang/String;

    const-string v0, "DrawScreenNail"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_DRAW_SCREEN_NAIL:Ljava/lang/String;

    const-string v0, "GLRootView"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GLROOTVIEW:Ljava/lang/String;

    const-string v0, "GLRootViewRequestRender"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GLROOTVIEW_REQUEST_RENDER:Ljava/lang/String;

    const-string v0, "GLRootViewOnDrawFrame"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GLROOTVIEW_ONDRAWFRAME:Ljava/lang/String;

    const-string v0, "GalleryActivity"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ACTIVITY:Ljava/lang/String;

    const-string v0, "GalleryOnCreate"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ON_CREATE:Ljava/lang/String;

    const-string v0, "GalleryOnResume"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ON_RESUME:Ljava/lang/String;

    const-string v0, "GalleryOnPause"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ON_PAUSE:Ljava/lang/String;

    const-string v0, "GalleryOnDestroy"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ON_DESTROY:Ljava/lang/String;

    const-string v0, "GalleryStartUp"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_START_UP:Ljava/lang/String;

    const-string v0, "AlbumSetPageActivity"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_ACTIVITY:Ljava/lang/String;

    const-string v0, "AlbumSetPageOnCreate"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_ON_CREATE:Ljava/lang/String;

    const-string v0, "AlbumSetPageOnResume"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_ON_RESUME:Ljava/lang/String;

    const-string v0, "PhotoPageActivity"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_ACTIVITY:Ljava/lang/String;

    const-string v0, "PhotoPageOnCreate"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_ON_CREATE:Ljava/lang/String;

    const-string v0, "PhotoPageOnResume"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_ON_RESUME:Ljava/lang/String;

    const-string v0, "GalleryDataLoader"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_DATALOADER:Ljava/lang/String;

    const-string v0, "PhotoPageReloadData"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_RELOAD_DATA:Ljava/lang/String;

    const-string v0, "PhotoPageGetUpdateInfo"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_GET_UPDATE_INFO:Ljava/lang/String;

    const-string v0, "PhotoPageUpdateContent"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_CONTENT:Ljava/lang/String;

    const-string v0, "PhotoPageUpdateSlidingWindow"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_SLIDING_WINDOW:Ljava/lang/String;

    const-string v0, "PhotoPageUpdateImageCache"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_IMAGE_CACHE:Ljava/lang/String;

    const-string v0, "PhotoPageUpdateTileProvider"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_TILE_PROVIDER:Ljava/lang/String;

    const-string v0, "PhotoPageUpdateImageRequest"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_IMAGE_REQUEST:Ljava/lang/String;

    const-string v0, "PhotoPageFireDataChange"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_FIRE_DATA_CHANGE:Ljava/lang/String;

    const-string v0, "PhotoPageDecodeScreenNailSubmit"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_DECODE_SCREENNAIL_SUBMIT:Ljava/lang/String;

    const-string v0, "PhotoPageDecodeScreenNailJob"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_DECODE_SCREENNAIL_JOB:Ljava/lang/String;

    const-string v0, "PhotoPageDecodeScreenNailListener"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_DECODE_SCREENNAIL_LISTENER:Ljava/lang/String;

    const-string v0, "AlbumSetPageReloadData"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_RELOAD_DATA:Ljava/lang/String;

    const-string v0, "AlbumSetPageDecodeScreenNailSubmit"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_DECODE_SCREENNAIL_SUBMIT:Ljava/lang/String;

    const-string v0, "AlbumSetPageDecodeScreenNailJob"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_DECODE_SCREENNAIL_JOB:Ljava/lang/String;

    const-string v0, "AlbumSetPageDecodeScreenNailListener"

    sput-object v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_DECODE_SCREENNAIL_LISTENER:Ljava/lang/String;

    const/4 v0, 0x1

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ROOT:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ROOT:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ROOT:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_VTNP_ONDRAWFRAME:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_VTNP_ONDRAWFRAME:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_VTNP_ONDRAWFRAME:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_FRAME_AVAILABLE:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_FRAME_AVAILABLE:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_VTNP_ONDRAWFRAME:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_FIRST_FRAME_AVAILABLE:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_FRAME_DRAW_AVAILABLE:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_VTNP_ONDRAWFRAME:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_DRAW_SCREEN_NAIL:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_DRAW_SCREEN_NAIL:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ROOT:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GLROOTVIEW:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GLROOTVIEW:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GLROOTVIEW:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GLROOTVIEW_REQUEST_RENDER:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GLROOTVIEW_REQUEST_RENDER:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GLROOTVIEW:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GLROOTVIEW_ONDRAWFRAME:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GLROOTVIEW_ONDRAWFRAME:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ROOT:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ACTIVITY:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ACTIVITY:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ACTIVITY:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ON_CREATE:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_CREATE:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ACTIVITY:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ON_RESUME:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_RESUME:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ACTIVITY:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ON_PAUSE:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_PAUSE:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ACTIVITY:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_ON_DESTROY:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_DESTROY:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ACTIVITY:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_START_UP:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_START_UP:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ROOT:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_ACTIVITY:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_ACTIVITY:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_ACTIVITY:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_ON_CREATE:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_ON_CREATE:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_ACTIVITY:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_ON_RESUME:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_ON_RESUME:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ROOT:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_ACTIVITY:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_ACTIVITY:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_ACTIVITY:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_ON_CREATE:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_ON_CREATE:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_ACTIVITY:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_ON_RESUME:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_ON_RESUME:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ROOT:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_GALLERY_DATALOADER:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_RELOAD_DATA:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_RELOAD_DATA:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_GET_UPDATE_INFO:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_GET_UPDATE_INFO:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_CONTENT:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_CONTENT:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_SLIDING_WINDOW:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_SLIDING_WINDOW:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_IMAGE_CACHE:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_IMAGE_CACHE:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_TILE_PROVIDER:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_TILE_PROVIDER:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_UPDATE_IMAGE_REQUEST:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_IMAGE_REQUEST:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_FIRE_DATA_CHANGE:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_FIRE_DATA_CHANGE:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_DECODE_SCREENNAIL_SUBMIT:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_DECODE_SCREENNAIL_SUBMIT:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_DECODE_SCREENNAIL_JOB:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_DECODE_SCREENNAIL_JOB:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_PHOTOPAGE_DECODE_SCREENNAIL_LISTENER:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_DECODE_SCREENNAIL_LISTENER:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_RELOAD_DATA:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_RELOAD_DATA:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_DECODE_SCREENNAIL_SUBMIT:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_DECODE_SCREENNAIL_SUBMIT:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_DECODE_SCREENNAIL_JOB:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_DECODE_SCREENNAIL_JOB:I

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_DATALOADER:I

    sget-object v1, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->NAME_ALBUMSETPAGE_DECODE_SCREENNAIL_LISTENER:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_DECODE_SCREENNAIL_LISTENER:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static startProfileAlbumSetPageDecodeScreenNailJob()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_DECODE_SCREENNAIL_JOB:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileAlbumSetPageOnCreate()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_ON_CREATE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileAlbumSetPageOnResume()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_ON_RESUME:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileAlbumSetPageReloadData()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_RELOAD_DATA:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileDrawScreenNail()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_DRAW_SCREEN_NAIL:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileFirstFrameAvailable()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_FRAME_DRAW_AVAILABLE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileGALLERYOnDestroy()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_DESTROY:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileGALLERYOnPause()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_PAUSE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileGalleryOnCreate()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_CREATE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileGalleryOnResume()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_RESUME:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileGalleryStartUp()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_START_UP:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfileOnDrawFrame()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GLROOTVIEW_ONDRAWFRAME:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageDecodeScreenNailJob()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_DECODE_SCREENNAIL_JOB:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageDecodeScreenNailListener()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_DECODE_SCREENNAIL_LISTENER:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageFireDataChange()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_FIRE_DATA_CHANGE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageGetUpdateInfo()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_GET_UPDATE_INFO:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageOnCreate()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_ON_CREATE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageOnResume()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_ON_RESUME:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageReloadData()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_RELOAD_DATA:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageUpdateContent()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_CONTENT:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageUpdateImageCache()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_IMAGE_CACHE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageUpdateImageRequest()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_IMAGE_REQUEST:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageUpdateSlidingWindow()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_SLIDING_WINDOW:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static startProfilePhotoPageUpdateTileProvider()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_TILE_PROVIDER:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileAlbumSetPageDecodeScreenNailJob()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_DECODE_SCREENNAIL_JOB:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileAlbumSetPageOnCreate()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_ON_CREATE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileAlbumSetPageOnResume()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_ON_RESUME:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileAlbumSetPageReloadData()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_RELOAD_DATA:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileDrawScreenNail()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_DRAW_SCREEN_NAIL:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileFirstFrameAvailable()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_FRAME_DRAW_AVAILABLE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileGALLERYOnDestroy()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_DESTROY:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileGALLERYOnPause()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_PAUSE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileGalleryOnCreate()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_CREATE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileGalleryOnResume()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_ON_RESUME:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileGalleryStartUp()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GALLERY_START_UP:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfileOnDrawFrame()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GLROOTVIEW_ONDRAWFRAME:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageDecodeScreenNailJob()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_DECODE_SCREENNAIL_JOB:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageDecodeScreenNailListener()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_DECODE_SCREENNAIL_LISTENER:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageFireDataChange()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_FIRE_DATA_CHANGE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageGetUpdateInfo()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_GET_UPDATE_INFO:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageOnCreate()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_ON_CREATE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageOnResume()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_ON_RESUME:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageReloadData()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_RELOAD_DATA:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageUpdateContent()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_CONTENT:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageUpdateImageCache()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_IMAGE_CACHE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageUpdateImageRequest()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_IMAGE_REQUEST:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageUpdateSlidingWindow()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_SLIDING_WINDOW:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static stopProfilePhotoPageUpdateTileProvider()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_UPDATE_TILE_PROVIDER:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static triggerAlbumSetPageDecodeScreenNail()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_ALBUMSETPAGE_DECODE_SCREENNAIL_SUBMIT:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static triggerFirstFrameAvailable()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_FRAME_DRAW_AVAILABLE:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static triggerFrameAvailable()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_FRAME_AVAILABLE:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static triggerGLRootViewRequest()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_GLROOTVIEW_REQUEST_RENDER:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method

.method public static triggerPhotoPageDecodeScreenNail()V
    .locals 2

    sget v0, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->EVENT_PHOTOPAGE_DECODE_SCREENNAIL_SUBMIT:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MediatekMMProfile$MMProfileWrapper;->MMProfileLog(II)V

    return-void
.end method
