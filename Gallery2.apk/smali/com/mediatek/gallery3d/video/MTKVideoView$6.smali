.class Lcom/mediatek/gallery3d/video/MTKVideoView$6;
.super Ljava/lang/Object;
.source "MTKVideoView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gallery3d/video/MTKVideoView;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/video/MTKVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 7
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "Gallery2/VideoPlayer/MTKVideoView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "surfaceChanged("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/gallery3d/ui/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "Gallery2/VideoPlayer/MTKVideoView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "surfaceChanged() mMediaPlayer="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$4200(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/media/MediaPlayer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mTargetState="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$4300(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mVideoWidth="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$4400(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mVideoHeight="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$4500(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/gallery3d/ui/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v4, p3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$4602(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    iget-object v4, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v4, p4}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$4702(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    iget-object v4, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v4}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$4800(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v4}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$4900(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v4

    if-ne v4, p3, :cond_3

    iget-object v4, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v4}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$5000(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v4

    if-ne v4, p4, :cond_3

    move v0, v2

    :goto_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$5100(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/media/MediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$5200(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget-object v3, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$5300(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    :cond_0
    const-string v2, "Gallery2/VideoPlayer/MTKVideoView"

    const-string v3, "surfaceChanged() start()"

    invoke-static {v2, v3}, Lcom/android/gallery3d/ui/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->start()V

    :cond_1
    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "Gallery2/VideoPlayer/MTKVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceCreated("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/ui/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v0, p1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$5402(Lcom/mediatek/gallery3d/video/MTKVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->openVideo()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "Gallery2/VideoPlayer/MTKVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceDestroyed("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/ui/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$5502(Lcom/mediatek/gallery3d/video/MTKVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$5600(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$5700(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$6;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->release(Z)V

    return-void
.end method
