.class Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;
.super Ljava/lang/Object;
.source "VideoThumbnailPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VideoThumbnailPlayInfo"
.end annotation


# instance fields
.field public final mediaPlayer:Landroid/media/MediaPlayer;

.field public final renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

.field public surface:Landroid/view/Surface;

.field final synthetic this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

.field public thumnailPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)V
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;

    invoke-direct {v1, p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;)V

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->thumnailPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->acquireSurfaceTexture()V

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, v0}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->surface:Landroid/view/Surface;

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v3, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->surface:Landroid/view/Surface;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$2;

    invoke-direct {v2, p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$2;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    return-void
.end method


# virtual methods
.method public recycle()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    iput-boolean v2, v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isWorking:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->thumnailPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    iput-boolean v2, v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isReadyForRender:Z

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Gallery2/VideoThumbnailPlayer"

    const-string v2, "thumbnail is released by pausing, give up recycling once again"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
