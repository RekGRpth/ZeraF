.class Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;
.super Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;
.source "VideoThumbnailPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;->this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;-><init>()V

    return-void
.end method


# virtual methods
.method public onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 2
    .param p1    # Landroid/graphics/SurfaceTexture;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;->this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iget-object v0, v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    iget-boolean v0, v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isWorking:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;->this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iget-object v0, v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->access$000(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;->render(Landroid/graphics/SurfaceTexture;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;->this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iget-object v1, v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasNewFrame:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;->this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iget-object v0, v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->access$100(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;->this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iget-object v1, v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    invoke-interface {v0, v1}, Lcom/android/gallery3d/ui/GLRoot;->addOnGLIdleListener(Lcom/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$1;->this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iget-object v0, v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->access$000(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;->antiRender(Landroid/graphics/SurfaceTexture;)V

    goto :goto_0
.end method
