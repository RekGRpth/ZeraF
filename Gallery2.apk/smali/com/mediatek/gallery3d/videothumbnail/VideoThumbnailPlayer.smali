.class public Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;
.super Ljava/lang/Object;
.source "VideoThumbnailPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;,
        Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final INIT_POOL_SIZE:I = 0x10

.field private static final MAX_PLAYER_COUNT:I = 0x10

.field private static final TAG:Ljava/lang/String; = "Gallery2/VideoThumbnailPlayer"


# instance fields
.field private final mCheckOuts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mCheckOutsLock:Ljava/lang/Object;

.field private mGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

.field private volatile mIsWorking:Z

.field private final mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/media/MediaPlayer;",
            "Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mMapLock:Ljava/lang/Object;

.field private final mOnFrameAvailableListener:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;

.field private volatile mPlayerCount:I

.field private final mPool:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mPoolLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ILcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;Lcom/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;
    .param p3    # Lcom/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPoolLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOutsLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mMapLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPool:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOuts:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mMap:Ljava/util/Map;

    iput-object p2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mOnFrameAvailableListener:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;

    iput-object p3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;Lcom/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;
    .param p2    # Lcom/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v0, 0x10

    invoke-direct {p0, v0, p1, p2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;-><init>(ILcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;Lcom/android/gallery3d/app/AbstractGalleryActivity;)V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mOnFrameAvailableListener:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)Lcom/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;Landroid/media/MediaPlayer;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;
    .param p1    # Landroid/media/MediaPlayer;

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->closeThumbnailByPlayer(Landroid/media/MediaPlayer;)Z

    move-result v0

    return v0
.end method

.method private closeThumbnailByPlayer(Landroid/media/MediaPlayer;)Z
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mMapLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->thumnailPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->getPlayInfoFromPath(Ljava/lang/String;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOutsLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOuts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->recycle()V

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPoolLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPool:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    const/4 v1, 0x1

    goto :goto_0

    :catchall_1
    move-exception v1

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1

    :catchall_2
    move-exception v1

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v1
.end method

.method public static create(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;Lcom/android/gallery3d/app/AbstractGalleryActivity;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;
    .param p1    # Lcom/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;Lcom/android/gallery3d/app/AbstractGalleryActivity;)V

    return-object v0
.end method

.method private getPlayInfoFromPath(Ljava/lang/String;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOutsLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mIsWorking:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOuts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iget-object v2, v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->thumnailPath:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    monitor-exit v3

    :goto_0
    return-object v1

    :cond_1
    monitor-exit v3

    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private onOpenException(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;
    .param p2    # Ljava/lang/Exception;

    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    iget-object v0, p1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->thumnailPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPoolLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPool:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "Gallery2/VideoThumbnailPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "happens when openning video thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private releaseThumbnailPlay(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;)V
    .locals 1
    .param p1    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    invoke-virtual {p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->recycle()V

    iget-object v0, p1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iget-object v0, p1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->surface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    iget-object v0, p1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->releaseSurfaceTexture()V

    return-void
.end method


# virtual methods
.method public closeThumbnail(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->getPlayInfoFromPath(Ljava/lang/String;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->closeThumbnailByPlayer(Landroid/media/MediaPlayer;)Z

    move-result v1

    goto :goto_0
.end method

.method public getPlayingThumbnailCount()I
    .locals 2

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOutsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOuts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getPlayingThumbnails(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object v3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOutsLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOuts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iget-object v2, v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->thumnailPath:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public isThumbnailPlaying(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->getPlayInfoFromPath(Ljava/lang/String;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openThumbnail(Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPoolLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v7, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mMapLock:Ljava/lang/Object;

    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v8, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mIsWorking:Z

    if-nez v8, :cond_0

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    return v4

    :cond_0
    :try_start_3
    iget-object v8, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPool:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_1

    iget-object v8, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPool:Ljava/util/List;

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    :goto_1
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    sget-boolean v6, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->$assertionsDisabled:Z

    if-nez v6, :cond_4

    if-nez v3, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    :cond_1
    :try_start_5
    iget v8, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPlayerCount:I

    const/16 v9, 0x10

    if-ne v8, v9, :cond_2

    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    monitor-exit v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move v4, v5

    goto :goto_0

    :cond_2
    :try_start_7
    const-string v8, "Gallery2/VideoThumbnailPlayer"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Gallery2/VideoThumbnailPlayer"

    const-string v9, "create new player in the pool"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    invoke-direct {v3, p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)V

    iget-object v8, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mMap:Ljava/util/Map;

    iget-object v9, v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v8, v9, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v8, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPlayerCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPlayerCount:I

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v4

    :catchall_1
    move-exception v4

    monitor-exit v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v4

    :cond_4
    iput-object p1, v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->thumnailPath:Ljava/lang/String;

    iget-object v2, v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->mediaPlayer:Landroid/media/MediaPlayer;

    move-object v1, v3

    :try_start_9
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->reset()V

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/media/MediaPlayer;->setLooping(Z)V
    :try_end_9
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_9} :catch_1

    :try_start_a
    invoke-virtual {v2, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_a} :catch_1

    :try_start_b
    iget-object v6, v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    const/4 v7, 0x1

    iput-boolean v7, v6, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isWorking:Z
    :try_end_b
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_b} :catch_1

    iget-object v5, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOutsLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_c
    iget-boolean v6, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mIsWorking:Z

    if-eqz v6, :cond_5

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    iget-object v6, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOuts:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    monitor-exit v5

    goto :goto_0

    :catchall_2
    move-exception v4

    monitor-exit v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    throw v4

    :catch_0
    move-exception v0

    :try_start_d
    invoke-direct {p0, v3, v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->onOpenException(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;Ljava/lang/Exception;)V
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v4, "Gallery2/VideoThumbnailPlayer"

    const-string v6, "thumbnail is released by pausing, give up openning"

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    goto/16 :goto_0

    :cond_5
    :try_start_e
    invoke-direct {p0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->releaseThumbnailPlay(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto :goto_2
.end method

.method public pause()V
    .locals 4

    iget-object v3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOutsLock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mIsWorking:Z

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mCheckOuts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPoolLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPool:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    iget-object v3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mMapLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    invoke-direct {p0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->releaseThumbnailPlay(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    :catchall_2
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v2

    :cond_0
    :try_start_5
    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    const/16 v2, 0x10

    iput v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mPlayerCount:I

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-static {}, Ljava/lang/System;->gc()V

    return-void
.end method

.method public renderThumbnail(Ljava/lang/String;Lcom/android/gallery3d/ui/GLCanvas;II)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->getPlayInfoFromPath(Ljava/lang/String;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    :try_start_0
    iget-object v3, v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    iget-boolean v3, v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isReadyForRender:Z

    if-eqz v3, :cond_0

    iget-object v2, v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->renderTarget:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;

    invoke-virtual {v2, p2, p3, p4}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->draw(Lcom/android/gallery3d/ui/GLCanvas;II)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method public resume()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->mIsWorking:Z

    return-void
.end method
