.class public Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailFeatureOption;
.super Ljava/lang/Object;
.source "VideoThumbnailFeatureOption.java"


# static fields
.field public static final OPTION_ENABLE_THIS_FEATRUE:Z = false

.field static final OPTION_MONITOR_LOADING:Z = false

.field static final OPTION_PREPARE_ASYNC:Z = false

.field static final OPTION_RENDER_TYPE:I = 0x2

.field static final OPTION_RENDER_TYPE_CROP_CENTER:I = 0x2

.field static final OPTION_RENDER_TYPE_KEEP_ASPECT_RATIO:I = 0x1

.field static final OPTION_RENDER_TYPE_REGARDLESS_OF_ASPECT_RATIO:I = 0x0

.field static final OPTION_TRANSCODE_BEFORE_PLAY:Z = true


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
