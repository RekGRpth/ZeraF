.class Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$2;
.super Ljava/lang/Object;
.source "VideoThumbnailPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

.field final synthetic val$this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$2;->this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iput-object p2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$2;->val$this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const-string v0, "Gallery2/VideoThumbnailPlayer"

    const-string v1, "error happened in video thumbnail\'s internal player. \n\tmay triggered by video deletion"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo$2;->this$1:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;

    iget-object v0, v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$VideoThumbnailPlayInfo;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-static {v0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->access$200(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;Landroid/media/MediaPlayer;)Z

    const/4 v0, 0x0

    return v0
.end method
