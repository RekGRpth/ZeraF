.class public final Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;
.super Ljava/lang/Object;
.source "VideoThumbnailMaker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;
    }
.end annotation


# static fields
.field private static final DYNAMIC_CACHE_FILE_POSTFIX:Ljava/lang/String; = ".dthumb"

.field private static final ENCODE_HEIGHT:I = 0xf0

.field private static final ENCODE_WIDTH:I = 0x140

.field private static final MAX_THUMBNAIL_DURATION:I = 0x1f40

.field private static final MIN_STORAGE_SPACE:I = 0x300000

.field private static final TAG:Ljava/lang/String; = "Gallery2/VideoThumbnailMaker"

.field private static final TRANSCODE_CANCEL:I = 0x2711

.field private static final TRANSCODING_BIT_RATE:I = 0x40000

.field private static final TRANSCODING_FRAME_RATE:I = 0xa

.field private static sCurrentHandle:Ljava/util/concurrent/atomic/AtomicLong;

.field private static sDirector:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

.field private static sGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

.field private static volatile sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, -0x1

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sCurrentHandle:Ljava/util/concurrent/atomic/AtomicLong;

    sput-object v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    sput-object v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    sput-object v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sDirector:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/gallery3d/data/LocalVideo;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/data/LocalVideo;

    invoke-static {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->handleVideo(Lcom/android/gallery3d/data/LocalVideo;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/gallery3d/data/LocalVideo;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/LocalVideo;

    invoke-static {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->needGenDynThumb(Lcom/android/gallery3d/data/LocalVideo;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200()Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sCurrentHandle:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method

.method static synthetic access$300()Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;
    .locals 1

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    return-object v0
.end method

.method public static cancelPendingTranscode()V
    .locals 1

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->access$600(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;)V

    :cond_0
    return-void
.end method

.method private static doTranscode(Lcom/android/gallery3d/data/LocalVideo;)I
    .locals 31
    .param p0    # Lcom/android/gallery3d/data/LocalVideo;

    new-instance v26, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/gallery3d/data/LocalMediaItem;->width:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/gallery3d/data/LocalMediaItem;->height:I

    move-object/from16 v0, v26

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Rect;->height()I

    move-result v3

    const/16 v4, 0x140

    const/16 v5, 0xf0

    invoke-static {v2, v3, v4, v5}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->getTargetRect(IIII)Landroid/graphics/Rect;

    move-result-object v27

    const-string v2, "Gallery2/VideoThumbnailMaker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "srcRect: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " targetRect: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/gallery3d/data/LocalVideo;->durationInSec:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v0, v2

    move-wide/from16 v21, v0

    const-wide/16 v2, 0x3

    div-long v10, v21, v2

    const-wide/16 v2, 0x1f40

    add-long/2addr v2, v10

    move-wide/from16 v0, v21

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v12

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1f40

    sub-long v4, v12, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v25, 0x2711

    :cond_0
    :goto_0
    return v25

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    move-object/from16 v28, v0

    const/4 v2, 0x0

    const/16 v3, 0x2f

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->isStorageSafeForTranscoding(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "Gallery2/VideoThumbnailMaker"

    const-string v3, "storage available in this volume is not enough! stop transcoding"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v25, -0x7fffffff

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    const/16 v3, 0x2f

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    new-instance v19, Ljava/io/File;

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->mkdir()Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "Gallery2/VideoThumbnailMaker"

    const-string v3, "exception when creating cache container!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v25, -0x7fffffff

    goto :goto_0

    :cond_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v25, 0x2711

    goto :goto_0

    :cond_4
    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v29, v0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v23, v0

    const-string v2, "Gallery2/VideoThumbnailMaker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "start transcoding: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", target width = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v29

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", target height = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v23

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "Gallery2/VideoThumbnailMaker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "starttime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", endtime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sCurrentHandle:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Lcom/mediatek/transcode/VideoTranscode;->init()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    sget-object v2, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sCurrentHandle:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".tmp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-long v8, v8

    const-wide/32 v14, 0x40000

    const-wide/16 v16, 0xa

    invoke-static/range {v2 .. v17}, Lcom/mediatek/transcode/VideoTranscode;->transcodeAdv(JLjava/lang/String;Ljava/lang/String;JJJJJJ)I

    move-result v25

    const-string v2, "Gallery2/VideoThumbnailMaker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "end transcoding: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sCurrentHandle:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/mediatek/transcode/VideoTranscode;->deinit(J)V

    sget-object v2, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sCurrentHandle:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v3, -0x1

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v25, 0x2711

    goto/16 :goto_0
.end method

.method private static getDynamicThumbnailPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ".dthumb/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".dthumb"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ".dthumb/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".dthumb"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto :goto_0
.end method

.method private static getTargetRect(IIII)Landroid/graphics/Rect;
    .locals 7
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x0

    if-le p0, p2, :cond_0

    if-gt p1, p3, :cond_1

    :cond_0
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v6, v6, p0, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    :goto_0
    return-object v4

    :cond_1
    int-to-float v4, p0

    int-to-float v5, p1

    div-float v1, v4, v5

    int-to-float v4, p2

    int-to-float v5, p3

    div-float v0, v4, v5

    cmpg-float v4, v1, v0

    if-gez v4, :cond_3

    move v3, p2

    mul-int v4, v3, p1

    div-int v2, v4, p0

    :cond_2
    :goto_1
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v6, v6, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    :cond_3
    move v2, p3

    mul-int v4, v2, p0

    div-int v3, v4, p1

    rem-int/lit8 v4, v3, 0x10

    if-eqz v4, :cond_2

    add-int/lit8 v4, v3, -0xf

    shr-int/lit8 v4, v4, 0x4

    shl-int/lit8 v3, v4, 0x4

    mul-int v4, v3, p1

    div-int v2, v4, p0

    goto :goto_1
.end method

.method private static declared-synchronized handleVideo(Lcom/android/gallery3d/data/LocalVideo;)V
    .locals 7
    .param p0    # Lcom/android/gallery3d/data/LocalVideo;

    const-class v4, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;

    monitor-enter v4

    :try_start_0
    invoke-static {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->doTranscode(Lcom/android/gallery3d/data/LocalVideo;)I

    move-result v1

    const-string v3, "Gallery2/VideoThumbnailMaker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "transcode result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0x2711

    if-ne v1, v3, :cond_0

    const/4 v3, 0x0

    iput v3, p0, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v4

    return-void

    :cond_0
    :try_start_1
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ".tmp"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    if-nez v1, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/io/File;

    iget-object v5, p0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    :cond_1
    const-string v3, "Gallery2/VideoThumbnailMaker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "recrified transcode result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_2

    const/4 v3, 0x2

    iput v3, p0, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I

    sget-object v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sDirector:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->pumpLiveThumbnails()V

    sget-object v3, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/gallery3d/ui/GLRoot;->requestRender()V

    const-string v3, "Gallery2/VideoThumbnailMaker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "then request render: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    :cond_2
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_3
    const/4 v3, 0x3

    iput v3, p0, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public static isStorageSafeForTranscoding(Ljava/lang/String;)Z
    .locals 9
    .param p0    # Ljava/lang/String;

    const/4 v4, 0x0

    :try_start_0
    new-instance v3, Landroid/os/StatFs;

    invoke-direct {v3, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    int-to-long v7, v7

    mul-long v1, v5, v7

    const-string v5, "Gallery2/VideoThumbnailMaker"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "storage available in this volume is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/32 v5, 0x300000

    cmp-long v5, v1, v5

    if-gez v5, :cond_0

    :goto_0
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v5, "Gallery2/VideoThumbnailMaker"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private static needGenDynThumb(Lcom/android/gallery3d/data/LocalVideo;)Z
    .locals 8
    .param p0    # Lcom/android/gallery3d/data/LocalVideo;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/gallery3d/data/LocalMediaItem;->isDrm()Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz v1, :cond_0

    iget v6, p0, Lcom/android/gallery3d/data/LocalMediaItem;->width:I

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/android/gallery3d/data/LocalMediaItem;->height:I

    if-nez v6, :cond_1

    :cond_0
    const/4 v5, 0x3

    iput v5, p0, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    :goto_0
    return v4

    :cond_1
    new-instance v3, Landroid/graphics/Rect;

    iget v6, p0, Lcom/android/gallery3d/data/LocalMediaItem;->width:I

    iget v7, p0, Lcom/android/gallery3d/data/LocalMediaItem;->height:I

    invoke-direct {v3, v4, v4, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->getDynamicThumbnailPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x2

    iput v5, p0, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I

    iput-object v2, p0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iput v5, p0, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I

    iput-object v2, p0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    move v4, v5

    goto :goto_0
.end method

.method public static pause()V
    .locals 1

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->access$500(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    :cond_0
    return-void
.end method

.method public static requestThumbnail(Lcom/android/gallery3d/data/LocalVideo;)V
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/LocalVideo;

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    invoke-static {v0, p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->access$400(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;Lcom/android/gallery3d/data/LocalVideo;)V

    :cond_0
    return-void
.end method

.method public static setDirector(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    sput-object p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sDirector:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    return-void
.end method

.method public static setGalleryActivity(Lcom/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/AbstractGalleryActivity;

    sput-object p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    return-void
.end method

.method public static start()V
    .locals 2

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    const-string v1, "transcode proxy"

    invoke-direct {v0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    sget-object v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->sHandler:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method
