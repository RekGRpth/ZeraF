.class public Lcom/android/gallery3d/picasasource/PicasaSource;
.super Lcom/android/gallery3d/data/MediaSource;
.source "PicasaSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/picasasource/PicasaSource$EmptyAlbumSet;
    }
.end annotation


# static fields
.field public static final ALBUM_PATH:Lcom/android/gallery3d/data/Path;

.field private static final IMAGE_MEDIA_ID:I = 0x1

.field private static final MAP_BATCH_COUNT:I = 0x64

.field private static final NO_MATCH:I = -0x1

.field private static final PICASA_ALBUMSET:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Gallery2/PicasaSource"


# instance fields
.field private mApplication:Lcom/android/gallery3d/app/GalleryApp;

.field private mMatcher:Lcom/android/gallery3d/data/PathMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "/picasa/all"

    invoke-static {v0}, Lcom/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/android/gallery3d/picasasource/PicasaSource;->ALBUM_PATH:Lcom/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1    # Lcom/android/gallery3d/app/GalleryApp;

    const/4 v2, 0x0

    const-string v0, "picasa"

    invoke-direct {p0, v0}, Lcom/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/gallery3d/picasasource/PicasaSource;->mApplication:Lcom/android/gallery3d/app/GalleryApp;

    new-instance v0, Lcom/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/picasasource/PicasaSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    iget-object v0, p0, Lcom/android/gallery3d/picasasource/PicasaSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/all"

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/gallery3d/picasasource/PicasaSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/image"

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/gallery3d/picasasource/PicasaSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/video"

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    return-void
.end method

.method public static extractExifValues(Lcom/android/gallery3d/data/MediaObject;Lcom/android/gallery3d/exif/ExifData;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/data/MediaObject;
    .param p1    # Lcom/android/gallery3d/exif/ExifData;

    return-void
.end method

.method public static getContentType(Lcom/android/gallery3d/data/MediaObject;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getDateTaken(Lcom/android/gallery3d/data/MediaObject;)J
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getFaceItem(Landroid/content/Context;Lcom/android/gallery3d/data/MediaItem;I)Lcom/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/gallery3d/data/MediaItem;
    .param p2    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getImageSize(Lcom/android/gallery3d/data/MediaObject;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getImageTitle(Lcom/android/gallery3d/data/MediaObject;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getLatitude(Lcom/android/gallery3d/data/MediaObject;)D
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getLongitude(Lcom/android/gallery3d/data/MediaObject;)D
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getPicasaId(Lcom/android/gallery3d/data/MediaObject;)J
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getRotation(Lcom/android/gallery3d/data/MediaObject;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getUserAccount(Landroid/content/Context;Lcom/android/gallery3d/data/MediaObject;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/gallery3d/data/MediaObject;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getVersionCheckDialog(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 1
    .param p0    # Landroid/app/Activity;

    const/4 v0, 0x0

    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    return-void
.end method

.method public static isPicasaImage(Lcom/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/data/MediaObject;

    const/4 v0, 0x0

    return v0
.end method

.method public static onPackageAdded(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public static onPackageChanged(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public static onPackageRemoved(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public static openFile(Landroid/content/Context;Lcom/android/gallery3d/data/MediaObject;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/gallery3d/data/MediaObject;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static requestSync(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    return-void
.end method

.method public static showSignInReminder(Landroid/app/Activity;)V
    .locals 0
    .param p0    # Landroid/app/Activity;

    return-void
.end method


# virtual methods
.method public createMediaObject(Lcom/android/gallery3d/data/Path;)Lcom/android/gallery3d/data/MediaObject;
    .locals 3
    .param p1    # Lcom/android/gallery3d/data/Path;

    iget-object v0, p0, Lcom/android/gallery3d/picasasource/PicasaSource;->mMatcher:Lcom/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/data/PathMatcher;->match(Lcom/android/gallery3d/data/Path;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bad path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lcom/android/gallery3d/picasasource/PicasaSource$EmptyAlbumSet;

    invoke-static {}, Lcom/android/gallery3d/data/MediaObject;->nextVersionNumber()J

    move-result-wide v1

    invoke-direct {v0, p1, v1, v2}, Lcom/android/gallery3d/picasasource/PicasaSource$EmptyAlbumSet;-><init>(Lcom/android/gallery3d/data/Path;J)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
