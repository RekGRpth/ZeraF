.class public Lcom/android/gallery3d/ui/ManageCacheDrawer;
.super Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;
.source "ManageCacheDrawer.java"


# instance fields
.field private final mCachePinMargin:I

.field private final mCachePinSize:I

.field private final mCachingText:Lcom/android/gallery3d/ui/StringTexture;

.field private final mCheckedItem:Lcom/android/gallery3d/ui/ResourceTexture;

.field private final mLocalAlbumIcon:Lcom/android/gallery3d/ui/ResourceTexture;

.field private final mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

.field private final mUnCheckedItem:Lcom/android/gallery3d/ui/ResourceTexture;


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/app/AbstractGalleryActivity;Lcom/android/gallery3d/ui/SelectionManager;Lcom/android/gallery3d/ui/SlotView;Lcom/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;II)V
    .locals 8
    .param p1    # Lcom/android/gallery3d/app/AbstractGalleryActivity;
    .param p2    # Lcom/android/gallery3d/ui/SelectionManager;
    .param p3    # Lcom/android/gallery3d/ui/SlotView;
    .param p4    # Lcom/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;
    .param p5    # I
    .param p6    # I

    invoke-virtual {p1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;-><init>(Lcom/android/gallery3d/app/AbstractGalleryActivity;Lcom/android/gallery3d/ui/SelectionManager;Lcom/android/gallery3d/ui/SlotView;Lcom/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;I)V

    move-object v7, p1

    new-instance v0, Lcom/android/gallery3d/ui/ResourceTexture;

    const v1, 0x7f02001d

    invoke-direct {v0, v7, v1}, Lcom/android/gallery3d/ui/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCheckedItem:Lcom/android/gallery3d/ui/ResourceTexture;

    new-instance v0, Lcom/android/gallery3d/ui/ResourceTexture;

    const v1, 0x7f02001c

    invoke-direct {v0, v7, v1}, Lcom/android/gallery3d/ui/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mUnCheckedItem:Lcom/android/gallery3d/ui/ResourceTexture;

    new-instance v0, Lcom/android/gallery3d/ui/ResourceTexture;

    const v1, 0x7f02001b

    invoke-direct {v0, v7, v1}, Lcom/android/gallery3d/ui/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mLocalAlbumIcon:Lcom/android/gallery3d/ui/ResourceTexture;

    const v0, 0x7f0c0267

    invoke-virtual {v7, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/high16 v0, 0x41400000

    const/4 v1, -0x1

    invoke-static {v6, v0, v1}, Lcom/android/gallery3d/ui/StringTexture;->newInstance(Ljava/lang/String;FI)Lcom/android/gallery3d/ui/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCachingText:Lcom/android/gallery3d/ui/StringTexture;

    iput-object p2, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    iput p5, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCachePinSize:I

    iput p6, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCachePinMargin:I

    return-void
.end method

.method private drawCachingPin(Lcom/android/gallery3d/ui/GLCanvas;Lcom/android/gallery3d/data/Path;IZZII)V
    .locals 9
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # Lcom/android/gallery3d/data/Path;
    .param p3    # I
    .param p4    # Z
    .param p5    # Z
    .param p6    # I
    .param p7    # I

    invoke-static {p3}, Lcom/android/gallery3d/ui/ManageCacheDrawer;->isLocal(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mLocalAlbumIcon:Lcom/android/gallery3d/ui/ResourceTexture;

    :goto_0
    iget v4, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCachePinSize:I

    iget v7, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCachePinMargin:I

    sub-int v1, p6, v7

    sub-int v2, v1, v4

    sub-int v3, p7, v4

    move-object v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/gallery3d/ui/ResourceTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    if-eqz p4, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCachingText:Lcom/android/gallery3d/ui/StringTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/UploadedTexture;->getWidth()I

    move-result v8

    iget-object v1, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCachingText:Lcom/android/gallery3d/ui/StringTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/UploadedTexture;->getHeight()I

    move-result v6

    iget-object v1, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCachingText:Lcom/android/gallery3d/ui/StringTexture;

    sub-int v2, p6, v8

    div-int/lit8 v2, v2, 0x2

    sub-int v3, p7, v6

    invoke-virtual {v1, p1, v2, v3}, Lcom/android/gallery3d/ui/BasicTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;II)V

    :cond_0
    return-void

    :cond_1
    if-eqz p5, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mCheckedItem:Lcom/android/gallery3d/ui/ResourceTexture;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mUnCheckedItem:Lcom/android/gallery3d/ui/ResourceTexture;

    goto :goto_0
.end method

.method private static isLocal(I)Z
    .locals 1
    .param p0    # I

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public renderSlot(Lcom/android/gallery3d/ui/GLCanvas;IIII)I
    .locals 20
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSetSlidingWindow;

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/android/gallery3d/ui/AlbumSetSlidingWindow;->get(I)Lcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    move-result-object v13

    iget v4, v13, Lcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->cacheFlag:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    const/16 v19, 0x1

    :goto_0
    if-eqz v19, :cond_4

    iget v4, v13, Lcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->cacheStatus:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_4

    const/4 v8, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/gallery3d/ui/ManageCacheDrawer;->mSelectionManager:Lcom/android/gallery3d/ui/SelectionManager;

    iget-object v5, v13, Lcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->setPath:Lcom/android/gallery3d/data/Path;

    invoke-virtual {v4, v5}, Lcom/android/gallery3d/ui/SelectionManager;->isItemSelected(Lcom/android/gallery3d/data/Path;)Z

    move-result v18

    xor-int v9, v19, v18

    iget v4, v13, Lcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->sourceType:I

    invoke-static {v4}, Lcom/android/gallery3d/ui/ManageCacheDrawer;->isLocal(I)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v9, :cond_5

    :cond_0
    const/16 v16, 0x1

    :goto_2
    const/16 v17, 0x0

    if-nez v16, :cond_1

    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Lcom/android/gallery3d/ui/GLCanvas;->save(I)V

    const v4, 0x3f19999a

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Lcom/android/gallery3d/ui/GLCanvas;->multiplyAlpha(F)V

    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v13, v2, v3}, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;->renderContent(Lcom/android/gallery3d/ui/GLCanvas;Lcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;II)I

    move-result v4

    or-int v17, v17, v4

    if-nez v16, :cond_2

    invoke-interface/range {p1 .. p1}, Lcom/android/gallery3d/ui/GLCanvas;->restore()V

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v13, v2, v3}, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;->renderLabel(Lcom/android/gallery3d/ui/GLCanvas;Lcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;II)I

    move-result v4

    or-int v17, v17, v4

    iget-object v6, v13, Lcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->setPath:Lcom/android/gallery3d/data/Path;

    iget v7, v13, Lcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->sourceType:I

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v10, p4

    move/from16 v11, p5

    invoke-direct/range {v4 .. v11}, Lcom/android/gallery3d/ui/ManageCacheDrawer;->drawCachingPin(Lcom/android/gallery3d/ui/GLCanvas;Lcom/android/gallery3d/data/Path;IZZII)V

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move/from16 v12, p2

    move/from16 v14, p4

    move/from16 v15, p5

    invoke-virtual/range {v10 .. v15}, Lcom/android/gallery3d/ui/AlbumSetSlotRenderer;->renderOverlay(Lcom/android/gallery3d/ui/GLCanvas;ILcom/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;II)I

    move-result v4

    or-int v17, v17, v4

    return v17

    :cond_3
    const/16 v19, 0x0

    goto :goto_0

    :cond_4
    const/4 v8, 0x0

    goto :goto_1

    :cond_5
    const/16 v16, 0x0

    goto :goto_2
.end method
