.class Lcom/android/gallery3d/ui/MultiLineTexture;
.super Lcom/android/gallery3d/ui/CanvasTexture;
.source "MultiLineTexture.java"


# instance fields
.field private final mLayout:Landroid/text/Layout;


# direct methods
.method private constructor <init>(Landroid/text/Layout;)V
    .locals 2
    .param p1    # Landroid/text/Layout;

    invoke-virtual {p1}, Landroid/text/Layout;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/gallery3d/ui/CanvasTexture;-><init>(II)V

    iput-object p1, p0, Lcom/android/gallery3d/ui/MultiLineTexture;->mLayout:Landroid/text/Layout;

    return-void
.end method

.method public static newInstance(Ljava/lang/String;IFILandroid/text/Layout$Alignment;)Lcom/android/gallery3d/ui/MultiLineTexture;
    .locals 12
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # F
    .param p3    # I
    .param p4    # Landroid/text/Layout$Alignment;

    invoke-static {p2, p3}, Lcom/android/gallery3d/ui/StringTexture;->getDefaultPaint(FI)Landroid/text/TextPaint;

    move-result-object v4

    new-instance v0, Landroid/text/StaticLayout;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    move v5, p1

    move-object/from16 v6, p4

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    new-instance v1, Lcom/android/gallery3d/ui/MultiLineTexture;

    invoke-direct {v1, v0}, Lcom/android/gallery3d/ui/MultiLineTexture;-><init>(Landroid/text/Layout;)V

    return-object v1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/gallery3d/ui/MultiLineTexture;->mLayout:Landroid/text/Layout;

    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method
