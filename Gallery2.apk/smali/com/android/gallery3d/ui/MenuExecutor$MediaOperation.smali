.class Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;
.super Ljava/lang/Object;
.source "MenuExecutor.java"

# interfaces
.implements Lcom/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/ui/MenuExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private final mListener:Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;

.field private final mOperation:I

.field final synthetic this$0:Lcom/android/gallery3d/ui/MenuExecutor;


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/ui/MenuExecutor;ILjava/util/ArrayList;Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    .locals 0
    .param p2    # I
    .param p4    # Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/gallery3d/data/Path;",
            ">;",
            "Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mOperation:I

    iput-object p3, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mItems:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mListener:Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;

    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->run(Lcom/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 13
    .param p1    # Lcom/android/gallery3d/util/ThreadPool$JobContext;

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v10}, Lcom/android/gallery3d/ui/MenuExecutor;->access$400(Lcom/android/gallery3d/ui/MenuExecutor;)Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/android/gallery3d/data/DataManager;

    move-result-object v5

    const/4 v6, 0x1

    iget v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mOperation:I

    const v11, 0x7f0b016f

    if-ne v10, v11, :cond_4

    move v3, v8

    :goto_0
    :try_start_0
    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    iget-object v11, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mListener:Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;

    invoke-static {v10, v11}, Lcom/android/gallery3d/ui/MenuExecutor;->access$600(Lcom/android/gallery3d/ui/MenuExecutor;Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/data/Path;

    invoke-interface {p1}, Lcom/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v10

    if-nez v10, :cond_0

    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v10}, Lcom/android/gallery3d/ui/MenuExecutor;->access$000(Lcom/android/gallery3d/ui/MenuExecutor;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    if-eqz v10, :cond_5

    :cond_0
    const/4 v6, 0x3

    :cond_1
    if-eqz v3, :cond_2

    invoke-static {}, Lcom/android/gallery3d/data/ClusterAlbumSet;->getClusterDeleteOperation()Z

    move-result v4

    invoke-static {v9}, Lcom/android/gallery3d/data/ClusterAlbumSet;->setClusterDeleteOperation(Z)V

    if-eqz v4, :cond_2

    const-string v10, "Gallery2/MenuExecutor"

    const-string v11, "deleting cluster complete, force reload all!"

    invoke-static {v10, v11}, Lcom/mediatek/gallery3d/util/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Lcom/android/gallery3d/data/DataManager;->forceRefreshAll()V

    :cond_2
    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v10}, Lcom/android/gallery3d/ui/MenuExecutor;->access$400(Lcom/android/gallery3d/ui/MenuExecutor;)Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->hasPausedActivity()Z

    move-result v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v10}, Lcom/android/gallery3d/ui/MenuExecutor;->access$900(Lcom/android/gallery3d/ui/MenuExecutor;)Z

    move-result v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v10}, Lcom/android/gallery3d/ui/MenuExecutor;->access$400(Lcom/android/gallery3d/ui/MenuExecutor;)Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v10

    invoke-virtual {v10, v8}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->setHideToast(Z)V

    :cond_3
    iget-object v8, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v8, v9}, Lcom/android/gallery3d/ui/MenuExecutor;->access$002(Lcom/android/gallery3d/ui/MenuExecutor;Z)Z

    iget-object v8, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v8, v9}, Lcom/android/gallery3d/ui/MenuExecutor;->access$902(Lcom/android/gallery3d/ui/MenuExecutor;Z)Z

    iget-object v8, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    iget-object v9, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mListener:Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;

    :goto_2
    invoke-static {v8, v6, v9}, Lcom/android/gallery3d/ui/MenuExecutor;->access$1000(Lcom/android/gallery3d/ui/MenuExecutor;ILcom/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    const/4 v8, 0x0

    return-object v8

    :cond_4
    move v3, v9

    goto :goto_0

    :cond_5
    if-eqz v3, :cond_6

    :try_start_1
    const-string v10, "cluster"

    invoke-virtual {v1}, Lcom/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const-string v10, "Gallery2/MenuExecutor"

    const-string v11, "deleting cluster, use special logic for culster object!"

    invoke-static {v10, v11}, Lcom/mediatek/gallery3d/util/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x1

    invoke-static {v10}, Lcom/android/gallery3d/data/ClusterAlbumSet;->setClusterDeleteOperation(Z)V

    :cond_6
    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    iget v11, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mOperation:I

    invoke-static {v10, v5, p1, v11, v1}, Lcom/android/gallery3d/ui/MenuExecutor;->access$700(Lcom/android/gallery3d/ui/MenuExecutor;Lcom/android/gallery3d/data/DataManager;Lcom/android/gallery3d/util/ThreadPool$JobContext;ILcom/android/gallery3d/data/Path;)Z

    move-result v10

    if-nez v10, :cond_7

    const/4 v6, 0x2

    :cond_7
    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    add-int/lit8 v2, v2, 0x1

    iget-object v11, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mListener:Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;

    invoke-static {v10, v2, v11}, Lcom/android/gallery3d/ui/MenuExecutor;->access$800(Lcom/android/gallery3d/ui/MenuExecutor;ILcom/android/gallery3d/ui/MenuExecutor$ProgressListener;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    :catch_0
    move-exception v7

    :try_start_2
    const-string v10, "Gallery2/MenuExecutor"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "failed to execute operation "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mOperation:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/gallery3d/ui/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v3, :cond_8

    invoke-static {}, Lcom/android/gallery3d/data/ClusterAlbumSet;->getClusterDeleteOperation()Z

    move-result v4

    invoke-static {v9}, Lcom/android/gallery3d/data/ClusterAlbumSet;->setClusterDeleteOperation(Z)V

    if-eqz v4, :cond_8

    const-string v10, "Gallery2/MenuExecutor"

    const-string v11, "deleting cluster complete, force reload all!"

    invoke-static {v10, v11}, Lcom/mediatek/gallery3d/util/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Lcom/android/gallery3d/data/DataManager;->forceRefreshAll()V

    :cond_8
    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v10}, Lcom/android/gallery3d/ui/MenuExecutor;->access$400(Lcom/android/gallery3d/ui/MenuExecutor;)Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->hasPausedActivity()Z

    move-result v10

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v10}, Lcom/android/gallery3d/ui/MenuExecutor;->access$900(Lcom/android/gallery3d/ui/MenuExecutor;)Z

    move-result v10

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v10}, Lcom/android/gallery3d/ui/MenuExecutor;->access$400(Lcom/android/gallery3d/ui/MenuExecutor;)Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v10

    invoke-virtual {v10, v8}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->setHideToast(Z)V

    :cond_9
    iget-object v8, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v8, v9}, Lcom/android/gallery3d/ui/MenuExecutor;->access$002(Lcom/android/gallery3d/ui/MenuExecutor;Z)Z

    iget-object v8, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v8, v9}, Lcom/android/gallery3d/ui/MenuExecutor;->access$902(Lcom/android/gallery3d/ui/MenuExecutor;Z)Z

    iget-object v8, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    iget-object v9, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mListener:Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;

    goto/16 :goto_2

    :catchall_0
    move-exception v10

    if-eqz v3, :cond_a

    invoke-static {}, Lcom/android/gallery3d/data/ClusterAlbumSet;->getClusterDeleteOperation()Z

    move-result v4

    invoke-static {v9}, Lcom/android/gallery3d/data/ClusterAlbumSet;->setClusterDeleteOperation(Z)V

    if-eqz v4, :cond_a

    const-string v11, "Gallery2/MenuExecutor"

    const-string v12, "deleting cluster complete, force reload all!"

    invoke-static {v11, v12}, Lcom/mediatek/gallery3d/util/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Lcom/android/gallery3d/data/DataManager;->forceRefreshAll()V

    :cond_a
    iget-object v11, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v11}, Lcom/android/gallery3d/ui/MenuExecutor;->access$400(Lcom/android/gallery3d/ui/MenuExecutor;)Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->hasPausedActivity()Z

    move-result v11

    if-eqz v11, :cond_b

    iget-object v11, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v11}, Lcom/android/gallery3d/ui/MenuExecutor;->access$900(Lcom/android/gallery3d/ui/MenuExecutor;)Z

    move-result v11

    if-eqz v11, :cond_b

    iget-object v11, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v11}, Lcom/android/gallery3d/ui/MenuExecutor;->access$400(Lcom/android/gallery3d/ui/MenuExecutor;)Lcom/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v11

    invoke-virtual {v11, v8}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->setHideToast(Z)V

    :cond_b
    iget-object v8, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v8, v9}, Lcom/android/gallery3d/ui/MenuExecutor;->access$002(Lcom/android/gallery3d/ui/MenuExecutor;Z)Z

    iget-object v8, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    invoke-static {v8, v9}, Lcom/android/gallery3d/ui/MenuExecutor;->access$902(Lcom/android/gallery3d/ui/MenuExecutor;Z)Z

    iget-object v8, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->this$0:Lcom/android/gallery3d/ui/MenuExecutor;

    iget-object v9, p0, Lcom/android/gallery3d/ui/MenuExecutor$MediaOperation;->mListener:Lcom/android/gallery3d/ui/MenuExecutor$ProgressListener;

    invoke-static {v8, v6, v9}, Lcom/android/gallery3d/ui/MenuExecutor;->access$1000(Lcom/android/gallery3d/ui/MenuExecutor;ILcom/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    throw v10
.end method
