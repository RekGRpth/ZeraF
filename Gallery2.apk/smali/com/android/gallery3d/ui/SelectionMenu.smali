.class public Lcom/android/gallery3d/ui/SelectionMenu;
.super Ljava/lang/Object;
.source "SelectionMenu.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "Gallery2/SelectionMenu"


# instance fields
.field private final mButton:Landroid/widget/Button;

.field private final mContext:Landroid/content/Context;

.field private final mPopupList:Lcom/android/gallery3d/ui/PopupList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Button;Lcom/android/gallery3d/ui/PopupList$OnPopupItemClickListener;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/Button;
    .param p3    # Lcom/android/gallery3d/ui/PopupList$OnPopupItemClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mButton:Landroid/widget/Button;

    new-instance v0, Lcom/android/gallery3d/ui/PopupList;

    iget-object v1, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mButton:Landroid/widget/Button;

    invoke-direct {v0, p1, v1}, Lcom/android/gallery3d/ui/PopupList;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mPopupList:Lcom/android/gallery3d/ui/PopupList;

    iget-object v0, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mPopupList:Lcom/android/gallery3d/ui/PopupList;

    const v1, 0x7f0b0004

    const v2, 0x7f0c025a

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/ui/PopupList;->addItem(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mPopupList:Lcom/android/gallery3d/ui/PopupList;

    invoke-virtual {v0, p3}, Lcom/android/gallery3d/ui/PopupList;->setOnPopupItemClickListener(Lcom/android/gallery3d/ui/PopupList$OnPopupItemClickListener;)V

    iget-object v0, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mPopupList:Lcom/android/gallery3d/ui/PopupList;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/PopupList;->show()V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updateSelectAllMode(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mPopupList:Lcom/android/gallery3d/ui/PopupList;

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/ui/PopupList;->findItem(I)Lcom/android/gallery3d/ui/PopupList$Item;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/ui/SelectionMenu;->mContext:Landroid/content/Context;

    if-eqz p1, :cond_1

    const v1, 0x7f0c025b

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/ui/PopupList$Item;->setTitle(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const v1, 0x7f0c025a

    goto :goto_0
.end method
