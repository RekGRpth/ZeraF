.class public Lcom/android/gallery3d/ui/CaptureAnimation;
.super Ljava/lang/Object;
.source "CaptureAnimation.java"


# static fields
.field private static final ZOOM_DELTA:F = 0.2f

.field private static final ZOOM_IN_BEGIN:F = 0.8f

.field private static final sSlideInterpolator:Landroid/view/animation/Interpolator;

.field private static final sZoomInInterpolator:Landroid/view/animation/Interpolator;

.field private static final sZoomOutInterpolator:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/android/gallery3d/ui/CaptureAnimation;->sZoomOutInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Lcom/android/gallery3d/ui/CaptureAnimation;->sZoomInInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/android/gallery3d/ui/CaptureAnimation;->sSlideInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculateScale(F)F
    .locals 6
    .param p0    # F

    const/high16 v5, 0x40000000

    const/high16 v3, 0x3f000000

    const v4, 0x3e4ccccd

    cmpg-float v1, p0, v3

    if-gtz v1, :cond_0

    const/high16 v1, 0x3f800000

    sget-object v2, Lcom/android/gallery3d/ui/CaptureAnimation;->sZoomOutInterpolator:Landroid/view/animation/Interpolator;

    mul-float v3, p0, v5

    invoke-interface {v2, v3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v2, v4

    sub-float v0, v1, v2

    :goto_0
    return v0

    :cond_0
    const v1, 0x3f4ccccd

    sget-object v2, Lcom/android/gallery3d/ui/CaptureAnimation;->sZoomInInterpolator:Landroid/view/animation/Interpolator;

    sub-float v3, p0, v3

    mul-float/2addr v3, v5

    invoke-interface {v2, v3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v2, v4

    add-float v0, v1, v2

    goto :goto_0
.end method

.method public static calculateSlide(F)F
    .locals 1
    .param p0    # F

    sget-object v0, Lcom/android/gallery3d/ui/CaptureAnimation;->sSlideInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method
