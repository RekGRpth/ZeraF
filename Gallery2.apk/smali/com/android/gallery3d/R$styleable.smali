.class public final Lcom/android/gallery3d/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CameraPreference:[I

.field public static final CameraPreference_title:I = 0x0

.field public static final CenteredLinearLayout:[I

.field public static final CenteredLinearLayout_max_width:I = 0x0

.field public static final IconIndicator:[I

.field public static final IconIndicator_icons:I = 0x0

.field public static final IconIndicator_modes:I = 0x1

.field public static final IconListPreference:[I

.field public static final IconListPreference_icons:I = 0x0

.field public static final IconListPreference_images:I = 0x3

.field public static final IconListPreference_largeIcons:I = 0x2

.field public static final IconListPreference_singleIcon:I = 0x1

.field public static final ImageButtonTitle:[I

.field public static final ImageButtonTitle_android_text:I = 0x1

.field public static final ImageButtonTitle_android_textColor:I = 0x0

.field public static final ListPreference:[I

.field public static final ListPreference_defaultValue:I = 0x1

.field public static final ListPreference_entries:I = 0x3

.field public static final ListPreference_entryValues:I = 0x2

.field public static final ListPreference_key:I = 0x0

.field public static final MaxLinearLayout:[I

.field public static final MaxLinearLayout_maxHeight:I = 0x0

.field public static final MaxLinearLayout_maxWidth:I = 0x1

.field public static final Theme_GalleryBase:[I

.field public static final Theme_GalleryBase_listPreferredItemHeightSmall:I = 0x0

.field public static final Theme_GalleryBase_switchStyle:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    new-array v0, v4, [I

    const/high16 v1, 0x7f010000

    aput v1, v0, v3

    sput-object v0, Lcom/android/gallery3d/R$styleable;->CameraPreference:[I

    new-array v0, v4, [I

    const v1, 0x7f01000e

    aput v1, v0, v3

    sput-object v0, Lcom/android/gallery3d/R$styleable;->CenteredLinearLayout:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/gallery3d/R$styleable;->IconIndicator:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/gallery3d/R$styleable;->IconListPreference:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/gallery3d/R$styleable;->ImageButtonTitle:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/gallery3d/R$styleable;->ListPreference:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/gallery3d/R$styleable;->MaxLinearLayout:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/gallery3d/R$styleable;->Theme_GalleryBase:[I

    return-void

    :array_0
    .array-data 4
        0x7f010005
        0x7f010006
    .end array-data

    :array_1
    .array-data 4
        0x7f010005
        0x7f010007
        0x7f010008
        0x7f010009
    .end array-data

    :array_2
    .array-data 4
        0x1010098
        0x101014f
    .end array-data

    :array_3
    .array-data 4
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
    .end array-data

    :array_4
    .array-data 4
        0x7f01000a
        0x7f01000b
    .end array-data

    :array_5
    .array-data 4
        0x7f01000c
        0x7f01000d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
