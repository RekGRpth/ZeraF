.class public Lcom/android/gallery3d/anim/StateTransitionAnimation;
.super Lcom/android/gallery3d/anim/Animation;
.source "StateTransitionAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/anim/StateTransitionAnimation$1;,
        Lcom/android/gallery3d/anim/StateTransitionAnimation$Transition;,
        Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;
    }
.end annotation


# instance fields
.field private mCurrentBackgroundAlpha:F

.field private mCurrentBackgroundScale:F

.field private mCurrentContentAlpha:F

.field private mCurrentContentScale:F

.field private mCurrentOverlayAlpha:F

.field private mCurrentOverlayScale:F

.field private mOldScreenTexture:Lcom/android/gallery3d/ui/RawTexture;

.field private final mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;Lcom/android/gallery3d/ui/RawTexture;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;
    .param p2    # Lcom/android/gallery3d/ui/RawTexture;

    invoke-direct {p0}, Lcom/android/gallery3d/anim/Animation;-><init>()V

    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget-object v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v0, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->duration:I

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/anim/Animation;->setDuration(I)V

    iget-object v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget-object v0, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/anim/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iput-object p2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mOldScreenTexture:Lcom/android/gallery3d/ui/RawTexture;

    invoke-static {}, Lcom/android/gallery3d/ui/TiledScreenNail;->disableDrawPlaceholder()V

    return-void

    :cond_0
    sget-object p1, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/android/gallery3d/anim/StateTransitionAnimation$Transition;Lcom/android/gallery3d/ui/RawTexture;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/anim/StateTransitionAnimation$Transition;
    .param p2    # Lcom/android/gallery3d/ui/RawTexture;

    invoke-static {p1}, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->access$000(Lcom/android/gallery3d/anim/StateTransitionAnimation$Transition;)Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/android/gallery3d/anim/StateTransitionAnimation;-><init>(Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;Lcom/android/gallery3d/ui/RawTexture;)V

    return-void
.end method

.method private applyOldTexture(Lcom/android/gallery3d/ui/GLView;Lcom/android/gallery3d/ui/GLCanvas;FFZ)V
    .locals 5
    .param p1    # Lcom/android/gallery3d/ui/GLView;
    .param p2    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p3    # F
    .param p4    # F
    .param p5    # Z

    iget-object v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mOldScreenTexture:Lcom/android/gallery3d/ui/RawTexture;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p5, :cond_1

    invoke-virtual {p1}, Lcom/android/gallery3d/ui/GLView;->getBackgroundColor()[F

    move-result-object v2

    invoke-interface {p2, v2}, Lcom/android/gallery3d/ui/GLCanvas;->clearBuffer([F)V

    :cond_1
    invoke-interface {p2}, Lcom/android/gallery3d/ui/GLCanvas;->save()V

    invoke-interface {p2, p3}, Lcom/android/gallery3d/ui/GLCanvas;->setAlpha(F)V

    invoke-virtual {p1}, Lcom/android/gallery3d/ui/GLView;->getWidth()I

    move-result v2

    div-int/lit8 v0, v2, 0x2

    invoke-virtual {p1}, Lcom/android/gallery3d/ui/GLView;->getHeight()I

    move-result v2

    div-int/lit8 v1, v2, 0x2

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-interface {p2, v2, v3}, Lcom/android/gallery3d/ui/GLCanvas;->translate(FF)V

    const/high16 v2, 0x3f800000

    invoke-interface {p2, p4, p4, v2}, Lcom/android/gallery3d/ui/GLCanvas;->scale(FFF)V

    iget-object v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mOldScreenTexture:Lcom/android/gallery3d/ui/RawTexture;

    neg-int v3, v0

    neg-int v4, v1

    invoke-virtual {v2, p2, v3, v4}, Lcom/android/gallery3d/ui/RawTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;II)V

    invoke-interface {p2}, Lcom/android/gallery3d/ui/GLCanvas;->restore()V

    goto :goto_0
.end method


# virtual methods
.method public applyBackground(Lcom/android/gallery3d/ui/GLView;Lcom/android/gallery3d/ui/GLCanvas;)V
    .locals 6
    .param p1    # Lcom/android/gallery3d/ui/GLView;
    .param p2    # Lcom/android/gallery3d/ui/GLCanvas;

    iget v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentBackgroundAlpha:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v3, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentBackgroundAlpha:F

    iget v4, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentBackgroundScale:F

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/anim/StateTransitionAnimation;->applyOldTexture(Lcom/android/gallery3d/ui/GLView;Lcom/android/gallery3d/ui/GLCanvas;FFZ)V

    :cond_0
    return-void
.end method

.method public applyContentTransform(Lcom/android/gallery3d/ui/GLView;Lcom/android/gallery3d/ui/GLCanvas;)V
    .locals 5
    .param p1    # Lcom/android/gallery3d/ui/GLView;
    .param p2    # Lcom/android/gallery3d/ui/GLCanvas;

    invoke-virtual {p1}, Lcom/android/gallery3d/ui/GLView;->getWidth()I

    move-result v2

    div-int/lit8 v0, v2, 0x2

    invoke-virtual {p1}, Lcom/android/gallery3d/ui/GLView;->getHeight()I

    move-result v2

    div-int/lit8 v1, v2, 0x2

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-interface {p2, v2, v3}, Lcom/android/gallery3d/ui/GLCanvas;->translate(FF)V

    iget v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentContentScale:F

    iget v3, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentContentScale:F

    const/high16 v4, 0x3f800000

    invoke-interface {p2, v2, v3, v4}, Lcom/android/gallery3d/ui/GLCanvas;->scale(FFF)V

    neg-int v2, v0

    int-to-float v2, v2

    neg-int v3, v1

    int-to-float v3, v3

    invoke-interface {p2, v2, v3}, Lcom/android/gallery3d/ui/GLCanvas;->translate(FF)V

    iget v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentContentAlpha:F

    invoke-interface {p2, v2}, Lcom/android/gallery3d/ui/GLCanvas;->setAlpha(F)V

    return-void
.end method

.method public applyOverlay(Lcom/android/gallery3d/ui/GLView;Lcom/android/gallery3d/ui/GLCanvas;)V
    .locals 6
    .param p1    # Lcom/android/gallery3d/ui/GLView;
    .param p2    # Lcom/android/gallery3d/ui/GLCanvas;

    iget v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentOverlayAlpha:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v3, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentOverlayAlpha:F

    iget v4, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentOverlayScale:F

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/anim/StateTransitionAnimation;->applyOldTexture(Lcom/android/gallery3d/ui/GLView;Lcom/android/gallery3d/ui/GLCanvas;FFZ)V

    :cond_0
    return-void
.end method

.method public calculate(J)Z
    .locals 2
    .param p1    # J

    invoke-super {p0, p1, p2}, Lcom/android/gallery3d/anim/Animation;->calculate(J)Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/gallery3d/anim/Animation;->isActive()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mOldScreenTexture:Lcom/android/gallery3d/ui/RawTexture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mOldScreenTexture:Lcom/android/gallery3d/ui/RawTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/RawTexture;->recycle()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mOldScreenTexture:Lcom/android/gallery3d/ui/RawTexture;

    :cond_0
    invoke-static {}, Lcom/android/gallery3d/ui/TiledScreenNail;->enableDrawPlaceholder()V

    :cond_1
    return v0
.end method

.method protected onCalculate(F)V
    .locals 3
    .param p1    # F

    iget-object v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v0, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentScaleFrom:F

    iget-object v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v1, v1, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentScaleTo:F

    iget-object v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v2, v2, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentScaleFrom:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentContentScale:F

    iget-object v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v0, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentAlphaFrom:F

    iget-object v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v1, v1, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentAlphaTo:F

    iget-object v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v2, v2, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentAlphaFrom:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentContentAlpha:F

    iget-object v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v0, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundAlphaFrom:F

    iget-object v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v1, v1, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundAlphaTo:F

    iget-object v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v2, v2, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundAlphaFrom:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentBackgroundAlpha:F

    iget-object v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v0, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundScaleFrom:F

    iget-object v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v1, v1, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundScaleTo:F

    iget-object v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v2, v2, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundScaleFrom:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentBackgroundScale:F

    iget-object v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v0, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayScaleFrom:F

    iget-object v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v1, v1, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayScaleTo:F

    iget-object v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v2, v2, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayScaleFrom:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentOverlayScale:F

    iget-object v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v0, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayAlphaFrom:F

    iget-object v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v1, v1, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayAlphaTo:F

    iget-object v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mTransitionSpec:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iget v2, v2, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayAlphaFrom:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation;->mCurrentOverlayAlpha:F

    return-void
.end method
