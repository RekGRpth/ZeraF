.class public Lcom/android/gallery3d/filtershow/presets/ImagePreset;
.super Ljava/lang/Object;
.source "ImagePreset.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/filtershow/presets/ImagePreset$FullRotate;
    }
.end annotation


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/ImagePreset"


# instance fields
.field private mDoApplyFilters:Z

.field private mDoApplyGeometry:Z

.field private mEndPoint:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

.field protected mFilters:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/android/gallery3d/filtershow/filters/ImageFilter;",
            ">;"
        }
    .end annotation
.end field

.field public final mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

.field private mHistoryName:Ljava/lang/String;

.field private mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

.field private mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

.field protected mIsFxPreset:Z

.field private mIsHighQuality:Z

.field protected mName:Ljava/lang/String;

.field private mScaleFactor:F


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mEndPoint:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mScaleFactor:F

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsHighQuality:Z

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    const-string v0, "Original"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mName:Ljava/lang/String;

    const-string v0, "Original"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mHistoryName:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsFxPreset:Z

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyFilters:Z

    new-instance v0, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setup()V

    return-void
.end method

.method public constructor <init>(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mEndPoint:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iput-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    const/high16 v3, 0x3f800000

    iput v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mScaleFactor:F

    iput-boolean v5, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsHighQuality:Z

    iput-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    const-string v3, "Original"

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mName:Ljava/lang/String;

    const-string v3, "Original"

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mHistoryName:Ljava/lang/String;

    iput-boolean v5, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsFxPreset:Z

    iput-boolean v6, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    iput-boolean v6, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyFilters:Z

    new-instance v3, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;-><init>()V

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    :try_start_0
    iget-object v3, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v3

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget-object v3, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    iget-object v3, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->add(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "Gallery2/ImagePreset"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception trying to clone: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->name()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->historyName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mHistoryName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->isFx()Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsFxPreset:Z

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->getImageLoader()Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    move-result-object v3

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    iget-object v4, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v3, v4}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->set(Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/gallery3d/filtershow/presets/ImagePreset;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;-><init>(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    if-eqz p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setHistoryName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mEndPoint:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mScaleFactor:F

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsHighQuality:Z

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    const-string v0, "Original"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mName:Ljava/lang/String;

    const-string v0, "Original"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mHistoryName:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsFxPreset:Z

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyFilters:Z

    new-instance v0, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setHistoryName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setup()V

    return-void
.end method

.method private setBorder(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    return-void
.end method


# virtual methods
.method public add(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V
    .locals 6
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    const/4 v5, 0x2

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getFilterType()B

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setHistoryName(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setBorder(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V

    :cond_0
    :goto_0
    invoke-virtual {p1, p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getFilterType()B

    move-result v3

    if-ne v3, v5, :cond_5

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getFilterType()B

    move-result v2

    if-eqz v0, :cond_3

    const/4 v3, 0x4

    if-eq v2, v3, :cond_3

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    if-ne v2, v5, :cond_2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3, v1, p1}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setHistoryName(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setHistoryName(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setHistoryName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public apply(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;

    move-object v0, p1

    iget-boolean v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    iget v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mScaleFactor:F

    iget-boolean v5, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsHighQuality:Z

    invoke-virtual {v3, p1, v4, v5}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    iget-boolean v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyFilters:Z

    if-eqz v3, :cond_1

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iget v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mScaleFactor:F

    iget-boolean v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsHighQuality:Z

    invoke-virtual {v1, v0, v3, v4}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iget v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mScaleFactor:F

    iget-boolean v5, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsHighQuality:Z

    invoke-virtual {v3, v0, v4, v5}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_2
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mEndPoint:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mEndPoint:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateFilteredImage(Landroid/graphics/Bitmap;)V

    :cond_3
    return-object v0
.end method

.method public fillImageStateAdapter(Lcom/android/gallery3d/filtershow/ImageStateAdapter;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/ImageStateAdapter;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->clear()V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {p1, v0}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getFilter(Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getImageLoader()Lcom/android/gallery3d/filtershow/cache/ImageLoader;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    return-object v0
.end method

.method public getScaleFactor()F
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mScaleFactor:F

    return v0
.end method

.method public hasModifications()Z
    .locals 4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->isNil()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->hasModifications()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->isNil()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public historyName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mHistoryName:Ljava/lang/String;

    return-object v0
.end method

.method public isFx()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsFxPreset:Z

    return v0
.end method

.method public isHighQuality()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsHighQuality:Z

    return v0
.end method

.method public isPanoramaSafe()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->isNil()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->hasModifications()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getFilterType()B

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->isNil()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_3
    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getFilterType()B

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_2

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->isNil()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_4
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->getFilter(Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public same(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)Z
    .locals 6
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-eq v4, v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    iget-boolean v5, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    if-ne v4, v5, :cond_0

    iget-boolean v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    iget-object v5, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v4, v5}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_2
    iget-boolean v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iget-object v5, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    if-ne v4, v5, :cond_0

    :cond_3
    iget-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iget-object v5, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageBorder:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v4, v5}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->same(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_4
    iget-boolean v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyFilters:Z

    iget-boolean v5, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyFilters:Z

    if-eq v4, v5, :cond_5

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-gtz v4, :cond_0

    iget-object v4, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-gtz v4, :cond_0

    :cond_5
    iget-boolean v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyFilters:Z

    if-eqz v4, :cond_6

    iget-boolean v4, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyFilters:Z

    if-eqz v4, :cond_6

    const/4 v2, 0x0

    :goto_1
    iget-object v4, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v2, v4, :cond_6

    iget-object v4, p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->same(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public setDoApplyFilters(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyFilters:Z

    return-void
.end method

.method public setDoApplyGeometry(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mDoApplyGeometry:Z

    return-void
.end method

.method public setEndpoint(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mEndPoint:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    return-void
.end method

.method public setGeometry(Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->set(Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;)V

    return-void
.end method

.method public setHistoryName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mHistoryName:Ljava/lang/String;

    return-void
.end method

.method public setImageLoader(Lcom/android/gallery3d/filtershow/cache/ImageLoader;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    return-void
.end method

.method public setIsFx(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsFxPreset:Z

    return-void
.end method

.method public setIsHighQuality(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mIsHighQuality:Z

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mName:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mHistoryName:Ljava/lang/String;

    return-void
.end method

.method public setScaleFactor(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mScaleFactor:F

    return-void
.end method

.method public setup()V
    .locals 0

    return-void
.end method
