.class public Lcom/android/gallery3d/filtershow/presets/ImagePresetBWBlue;
.super Lcom/android/gallery3d/filtershow/presets/ImagePreset;
.source "ImagePresetBWBlue.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;-><init>()V

    return-void
.end method


# virtual methods
.method public name()Ljava/lang/String;
    .locals 1

    const-string v0, "B&W - Blue"

    return-object v0
.end method

.method public setup()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    new-instance v1, Lcom/android/gallery3d/filtershow/filters/ImageFilterBWBlue;

    invoke-direct {v1}, Lcom/android/gallery3d/filtershow/filters/ImageFilterBWBlue;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method
