.class public Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;
.super Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;
.source "ImageZoom.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/ImageZoom"

.field private static mMaxSize:F


# instance fields
.field private mTouchDown:Z

.field private mZoomBounds:Landroid/graphics/Rect;

.field private mZoomedIn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, 0x44000000

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mMaxSize:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mTouchDown:Z

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomedIn:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomBounds:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mTouchDown:Z

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomedIn:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomBounds:Landroid/graphics/Rect;

    return-void
.end method

.method public static setZoomedSize(F)V
    .locals 0
    .param p0    # F

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mMaxSize:F

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomedIn:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->onTouchDown(FF)V

    :goto_0
    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomedIn:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomedIn:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->onTouchUp()V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v9, 0x43870000

    const/high16 v8, 0x42b40000

    const/high16 v7, 0x3f800000

    const/high16 v6, -0x40800000

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->drawBackground(Landroid/graphics/Canvas;)V

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomedIn:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mTouchDown:Z

    if-eqz v2, :cond_4

    :cond_0
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v3

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomBounds:Landroid/graphics/Rect;

    const/4 v5, 0x0

    invoke-virtual {v2, p0, v3, v4, v5}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->getScaleOneImageForPreset(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;Lcom/android/gallery3d/filtershow/presets/ImagePreset;Landroid/graphics/Rect;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomedIn:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mTouchDown:Z

    if-eqz v2, :cond_2

    :cond_1
    invoke-static {}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->getZoomOrientation()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->showControls()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSliderController:Lcom/android/gallery3d/filtershow/ui/SliderController;

    invoke-virtual {v2, p1}, Lcom/android/gallery3d/filtershow/ui/SliderController;->onDraw(Landroid/graphics/Canvas;)V

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->drawToast(Landroid/graphics/Canvas;)V

    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->requestFilteredImages()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->getFilteredImage()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v8, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v9, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v8, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    invoke-virtual {p1, v7, v6}, Landroid/graphics/Canvas;->scale(FF)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v9, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    invoke-virtual {p1, v7, v6}, Landroid/graphics/Canvas;->scale(FF)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchDown(FF)V
    .locals 13
    .param p1    # F
    .param p2    # F

    const/4 v1, 0x1

    invoke-super {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->onTouchDown(FF)V

    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomedIn:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mTouchDown:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mTouchDown:Z

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v2

    iget-object v0, v2, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->getOriginalBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->getOriginalBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual/range {v0 .. v5}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->getOriginalToScreen(ZFFFF)Landroid/graphics/Matrix;

    move-result-object v9

    const/4 v2, 0x2

    new-array v10, v2, [F

    const/4 v2, 0x0

    aput p1, v10, v2

    aput p2, v10, v1

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v9, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    invoke-virtual {v6, v10}, Landroid/graphics/Matrix;->mapPoints([F)V

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v11, v1, v2

    sget v7, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mMaxSize:F

    mul-float v8, v11, v7

    new-instance v12, Landroid/graphics/RectF;

    iget v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    sub-float/2addr v1, v8

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    sub-float/2addr v2, v7

    iget v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    add-float/2addr v3, v8

    iget v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    add-float/2addr v4, v8

    invoke-direct {v12, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v6, v12}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    invoke-virtual {v12}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    sub-float/2addr v1, v8

    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    sub-float/2addr v2, v7

    invoke-virtual {v12}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    add-float/2addr v3, v8

    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    add-float/2addr v4, v7

    invoke-virtual {v12, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    new-instance v1, Landroid/graphics/Rect;

    iget v2, v12, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v12, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v12, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v5, v12, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomBounds:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto/16 :goto_0
.end method

.method public onTouchUp()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mTouchDown:Z

    return-void
.end method

.method public resetParameter()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->resetParameter()V

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mZoomedIn:Z

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageZoom;->mTouchDown:Z

    return-void
.end method
