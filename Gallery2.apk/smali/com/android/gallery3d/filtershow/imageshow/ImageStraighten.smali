.class public Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;
.super Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;
.source "ImageStraighten.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/ImageStraighten"

.field private static final gPaint:Landroid/graphics/Paint;


# instance fields
.field private mAngle:F

.field private mBaseAngle:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->gPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mBaseAngle:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mAngle:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mBaseAngle:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mAngle:F

    return-void
.end method

.method private computeValue()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getCurrentTouchAngle()F

    move-result v0

    iget v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mBaseAngle:F

    sub-float/2addr v1, v0

    const/high16 v2, 0x43b40000

    rem-float/2addr v1, v2

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mAngle:F

    const/high16 v1, -0x3dcc0000

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mAngle:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mAngle:F

    const/high16 v1, 0x42340000

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mAngle:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mAngle:F

    return-void
.end method

.method private setCropToStraighten()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalStraighten()F

    move-result v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->getUntranslatedStraightenCropBounds(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setLocalCropBounds(Landroid/graphics/RectF;)V

    return-void
.end method


# virtual methods
.method protected drawShape(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 20
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Bitmap;

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->drawTransformed(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)Landroid/graphics/RectF;

    invoke-virtual/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->straightenBounds()Landroid/graphics/RectF;

    move-result-object v14

    new-instance v18, Landroid/graphics/Path;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Path;-><init>()V

    sget-object v3, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->gPaint:Landroid/graphics/Paint;

    const/16 v5, 0xff

    const/16 v6, 0xff

    const/16 v8, 0xff

    const/16 v9, 0xff

    invoke-virtual {v3, v5, v6, v8, v9}, Landroid/graphics/Paint;->setARGB(IIII)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->gPaint:Landroid/graphics/Paint;

    const/high16 v5, 0x40400000

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->gPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalDisplayBounds()Landroid/graphics/RectF;

    move-result-object v15

    invoke-virtual {v15}, Landroid/graphics/RectF;->width()F

    move-result v11

    invoke-virtual {v15}, Landroid/graphics/RectF;->height()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mMode:Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry$MODES;

    sget-object v5, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry$MODES;->MOVE:Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry$MODES;

    if-ne v3, v5, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    const/16 v17, 0x10

    move/from16 v0, v17

    int-to-float v3, v0

    div-float v19, v11, v3

    const/4 v4, 0x0

    const/16 v16, 0x1

    :goto_0
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_0

    move/from16 v0, v16

    int-to-float v3, v0

    mul-float v4, v3, v19

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->gPaint:Landroid/graphics/Paint;

    const/16 v5, 0x3c

    const/16 v6, 0xff

    const/16 v8, 0xff

    const/16 v9, 0xff

    invoke-virtual {v3, v5, v6, v8, v9}, Landroid/graphics/Paint;->setARGB(IIII)V

    const/4 v5, 0x0

    sget-object v8, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    move v6, v4

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/4 v9, 0x0

    sget-object v13, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v8, p1

    move v10, v4

    move v12, v4

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    return-void
.end method

.method protected gainedVisibility()V
    .locals 0

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->setCropToStraighten()V

    return-void
.end method

.method protected getLocalValue()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalStraighten()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c0198

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected lostVisibility()V
    .locals 0

    return-void
.end method

.method public onNewValue(I)V
    .locals 3
    .param p1    # I

    int-to-float v0, p1

    const/high16 v1, -0x3dcc0000

    const/high16 v2, 0x42340000

    invoke-static {v0, v1, v2}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMath;->clamp(FFF)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setLocalStraighten(F)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->getPanelController()Lcom/android/gallery3d/filtershow/PanelController;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->getPanelController()Lcom/android/gallery3d/filtershow/PanelController;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalStraighten()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/PanelController;->onNewValue(I)V

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method protected setActionDown(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    invoke-super {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setActionDown(FF)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalStraighten()F

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mAngle:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mBaseAngle:F

    return-void
.end method

.method protected setActionMove(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    invoke-super {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setActionMove(FF)V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->computeValue()V

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->mAngle:F

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setLocalStraighten(F)V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->setCropToStraighten()V

    return-void
.end method

.method protected setActionUp()V
    .locals 0

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setActionUp()V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageStraighten;->setCropToStraighten()V

    return-void
.end method
