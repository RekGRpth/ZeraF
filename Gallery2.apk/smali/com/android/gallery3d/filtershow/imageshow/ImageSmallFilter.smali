.class public Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;
.super Lcom/android/gallery3d/filtershow/imageshow/ImageShow;
.source "ImageSmallFilter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/ImageSmallFilter"

.field protected static mBackgroundColor:I

.field protected static mMargin:I

.field protected static mTextMargin:I


# instance fields
.field private mController:Lcom/android/gallery3d/filtershow/FilterShowActivity;

.field protected mImageFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

.field protected mIsSelected:Z

.field private mNullFilter:Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;

.field protected final mPaint:Landroid/graphics/Paint;

.field protected final mSelectedBackgroundColor:I

.field private mSetBorder:Z

.field private mShowTitle:Z

.field protected final mTextColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xc

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mMargin:I

    const/16 v0, 0x8

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextMargin:I

    const v0, -0xffff01

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mBackgroundColor:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mController:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mImageFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mShowTitle:Z

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mSetBorder:Z

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mPaint:Landroid/graphics/Paint;

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mIsSelected:Z

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mSelectedBackgroundColor:I

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextColor:I

    invoke-virtual {p0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mController:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mImageFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mShowTitle:Z

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mSetBorder:Z

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mPaint:Landroid/graphics/Paint;

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mIsSelected:Z

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mSelectedBackgroundColor:I

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextColor:I

    invoke-virtual {p0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static setDefaultBackgroundColor(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mBackgroundColor:I

    return-void
.end method

.method public static setMargin(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mMargin:I

    return-void
.end method

.method public static setTextMargin(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextMargin:I

    return-void
.end method


# virtual methods
.method public drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Landroid/graphics/Rect;

    const/high16 v7, 0x40000000

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-le v1, v0, :cond_1

    move v2, v0

    sub-int v6, v1, v2

    int-to-float v6, v6

    div-float/2addr v6, v7

    float-to-int v4, v6

    const/4 v5, 0x0

    :goto_0
    new-instance v3, Landroid/graphics/Rect;

    add-int v6, v4, v2

    add-int v7, v5, v2

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v3, p3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    return-void

    :cond_1
    move v2, v1

    const/4 v4, 0x0

    sub-int v6, v0, v2

    int-to-float v6, v6

    div-float/2addr v6, v7

    float-to-int v5, v6

    goto :goto_0
.end method

.method public getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mController:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mImageFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mIsSelected:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mNullFilter:Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mNullFilter:Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->onClick(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mController:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mImageFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mSetBorder:Z

    invoke-virtual {v0, p0, v1, v2}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->useImageFilter(Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;Lcom/android/gallery3d/filtershow/filters/ImageFilter;Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mIsSelected:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mNullFilter:Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mNullFilter:Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->onClick(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mController:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {v0, p0, v1}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->useImagePreset(Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;

    const/4 v11, -0x1

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->requestFilteredImages()V

    sget v0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mBackgroundColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mImageFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    sget v0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextSize:I

    sget v1, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextPadding:I

    mul-int/lit8 v1, v1, 0x2

    add-int v7, v0, v1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v8

    const/high16 v1, 0x40000000

    div-float/2addr v0, v1

    float-to-int v9, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v10

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mIsSelected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v1, 0x0

    sget v0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mMargin:I

    int-to-float v2, v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    sget v4, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mMargin:I

    add-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    new-instance v6, Landroid/graphics/Rect;

    sget v0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mMargin:I

    sget v1, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mMargin:I

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    sget v3, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mMargin:I

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-direct {v6, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getFilteredImage()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v6}, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mPaint:Landroid/graphics/Paint;

    sget v1, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mImageFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getName()Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v9

    sget v2, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextMargin:I

    sub-int v2, v10, v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sget v3, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextSize:I

    sget v4, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mTextPadding:I

    add-int v0, v3, v4

    sub-int v3, v1, v0

    invoke-virtual {p0, v3, v1}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public setBorder(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mSetBorder:Z

    return-void
.end method

.method public setController(Lcom/android/gallery3d/filtershow/FilterShowActivity;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/FilterShowActivity;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mController:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    return-void
.end method

.method public setImageFilter(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mImageFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    new-instance v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p1, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mImageFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->add(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V

    return-void
.end method

.method public setNulfilter(Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mNullFilter:Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;

    return-void
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mIsSelected:Z

    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mIsSelected:Z

    return-void
.end method

.method public setShowTitle(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mShowTitle:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public showControls()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public showHires()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public showTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->mShowTitle:Z

    return v0
.end method

.method public updateGeometryFlags()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public updateImagePresets(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageSmallFilter;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    if-nez v0, :cond_0

    :cond_0
    return-void
.end method
