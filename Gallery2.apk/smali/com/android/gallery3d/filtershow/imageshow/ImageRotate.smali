.class public Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;
.super Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;
.source "ImageRotate.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/ImageRotate"

.field private static final gPaint:Landroid/graphics/Paint;


# instance fields
.field private mAngle:F

.field private mBaseAngle:F

.field private final mSnapToNinety:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->gPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mBaseAngle:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mAngle:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mSnapToNinety:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mBaseAngle:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mAngle:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mSnapToNinety:Z

    return-void
.end method

.method private computeValue()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getCurrentTouchAngle()F

    move-result v0

    iget v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mBaseAngle:F

    sub-float/2addr v1, v0

    const/high16 v2, 0x43b40000

    rem-float/2addr v1, v2

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mAngle:F

    return-void
.end method


# virtual methods
.method protected drawShape(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    const/16 v1, 0xff

    sget-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    sget-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    sget-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/Paint;->setARGB(IIII)V

    sget-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->drawTransformedCropped(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    return-void
.end method

.method protected getLocalValue()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v0

    invoke-static {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->snappedAngle(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->constrainedRotation(F)I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c019a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setActionDown(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    invoke-super {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setActionDown(FF)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mAngle:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mBaseAngle:F

    return-void
.end method

.method protected setActionMove(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    invoke-super {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setActionMove(FF)V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->computeValue()V

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mAngle:F

    const/high16 v1, 0x43b40000

    rem-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setLocalRotation(F)V

    return-void
.end method

.method protected setActionUp()V
    .locals 2

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setActionUp()V

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->mAngle:F

    const/high16 v1, 0x43b40000

    rem-float/2addr v0, v1

    invoke-static {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageRotate;->snappedAngle(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setLocalRotation(F)V

    return-void
.end method
